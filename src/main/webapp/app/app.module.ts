import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MisbeSharedModule } from 'app/shared/shared.module';
import { MisbeCoreModule } from 'app/core/core.module';
import { MisbeAppRoutingModule } from './app-routing.module';
import { MisbeHomeModule } from './home/home.module';
import { MisbeEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    MisbeSharedModule,
    MisbeCoreModule,
    MisbeHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MisbeEntityModule,
    MisbeAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class MisbeAppModule {}
