import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBookingServiceMember, BookingServiceMember } from 'app/shared/model/booking-service-member.model';
import { BookingServiceMemberService } from './booking-service-member.service';
import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from 'app/entities/booking-status/booking-status.service';
import { IJobAssignment } from 'app/shared/model/job-assignment.model';
import { JobAssignmentService } from 'app/entities/job-assignment/job-assignment.service';
import { IBookingServices } from 'app/shared/model/booking-services.model';
import { BookingServicesService } from 'app/entities/booking-services/booking-services.service';

type SelectableEntity = IBookingStatus | IJobAssignment | IBookingServices;

@Component({
  selector: 'jhi-booking-service-member-update',
  templateUrl: './booking-service-member-update.component.html',
})
export class BookingServiceMemberUpdateComponent implements OnInit {
  isSaving = false;
  bookingstatusids: IBookingStatus[] = [];
  jobassignmentids: IJobAssignment[] = [];
  bookingservices: IBookingServices[] = [];

  editForm = this.fb.group({
    id: [],
    userId: [],
    productPriceRule: [],
    skuId: [],
    price: [],
    remark: [],
    startTime: [],
    endTime: [],
    imageUpload: [],
    bookingDetail: [],
    createdBy: [],
    updatedBy: [],
    createdAt: [],
    updatedAt: [],
    bookingStatusId: [],
    jobAssignmentId: [],
    bookingServices: [],
  });

  constructor(
    protected bookingServiceMemberService: BookingServiceMemberService,
    protected bookingStatusService: BookingStatusService,
    protected jobAssignmentService: JobAssignmentService,
    protected bookingServicesService: BookingServicesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingServiceMember }) => {
      if (!bookingServiceMember.id) {
        const today = moment().startOf('day');
        bookingServiceMember.startTime = today;
        bookingServiceMember.endTime = today;
        bookingServiceMember.createdBy = today;
        bookingServiceMember.updatedBy = today;
        bookingServiceMember.createdAt = today;
        bookingServiceMember.updatedAt = today;
      }

      this.updateForm(bookingServiceMember);

      this.bookingStatusService
        .query({ filter: 'bookingservicemember-is-null' })
        .pipe(
          map((res: HttpResponse<IBookingStatus[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBookingStatus[]) => {
          if (!bookingServiceMember.bookingStatusId || !bookingServiceMember.bookingStatusId.id) {
            this.bookingstatusids = resBody;
          } else {
            this.bookingStatusService
              .find(bookingServiceMember.bookingStatusId.id)
              .pipe(
                map((subRes: HttpResponse<IBookingStatus>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBookingStatus[]) => (this.bookingstatusids = concatRes));
          }
        });

      this.jobAssignmentService
        .query({ filter: 'bookingservicemember-is-null' })
        .pipe(
          map((res: HttpResponse<IJobAssignment[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IJobAssignment[]) => {
          if (!bookingServiceMember.jobAssignmentId || !bookingServiceMember.jobAssignmentId.id) {
            this.jobassignmentids = resBody;
          } else {
            this.jobAssignmentService
              .find(bookingServiceMember.jobAssignmentId.id)
              .pipe(
                map((subRes: HttpResponse<IJobAssignment>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IJobAssignment[]) => (this.jobassignmentids = concatRes));
          }
        });

      this.bookingServicesService.query().subscribe((res: HttpResponse<IBookingServices[]>) => (this.bookingservices = res.body || []));
    });
  }

  updateForm(bookingServiceMember: IBookingServiceMember): void {
    this.editForm.patchValue({
      id: bookingServiceMember.id,
      userId: bookingServiceMember.userId,
      productPriceRule: bookingServiceMember.productPriceRule,
      skuId: bookingServiceMember.skuId,
      price: bookingServiceMember.price,
      remark: bookingServiceMember.remark,
      startTime: bookingServiceMember.startTime ? bookingServiceMember.startTime.format(DATE_TIME_FORMAT) : null,
      endTime: bookingServiceMember.endTime ? bookingServiceMember.endTime.format(DATE_TIME_FORMAT) : null,
      imageUpload: bookingServiceMember.imageUpload,
      bookingDetail: bookingServiceMember.bookingDetail,
      createdBy: bookingServiceMember.createdBy ? bookingServiceMember.createdBy.format(DATE_TIME_FORMAT) : null,
      updatedBy: bookingServiceMember.updatedBy ? bookingServiceMember.updatedBy.format(DATE_TIME_FORMAT) : null,
      createdAt: bookingServiceMember.createdAt ? bookingServiceMember.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: bookingServiceMember.updatedAt ? bookingServiceMember.updatedAt.format(DATE_TIME_FORMAT) : null,
      bookingStatusId: bookingServiceMember.bookingStatusId,
      jobAssignmentId: bookingServiceMember.jobAssignmentId,
      bookingServices: bookingServiceMember.bookingServices,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookingServiceMember = this.createFromForm();
    if (bookingServiceMember.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingServiceMemberService.update(bookingServiceMember));
    } else {
      this.subscribeToSaveResponse(this.bookingServiceMemberService.create(bookingServiceMember));
    }
  }

  private createFromForm(): IBookingServiceMember {
    return {
      ...new BookingServiceMember(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      productPriceRule: this.editForm.get(['productPriceRule'])!.value,
      skuId: this.editForm.get(['skuId'])!.value,
      price: this.editForm.get(['price'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      startTime: this.editForm.get(['startTime'])!.value ? moment(this.editForm.get(['startTime'])!.value, DATE_TIME_FORMAT) : undefined,
      endTime: this.editForm.get(['endTime'])!.value ? moment(this.editForm.get(['endTime'])!.value, DATE_TIME_FORMAT) : undefined,
      imageUpload: this.editForm.get(['imageUpload'])!.value,
      bookingDetail: this.editForm.get(['bookingDetail'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value ? moment(this.editForm.get(['createdBy'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value ? moment(this.editForm.get(['updatedBy'])!.value, DATE_TIME_FORMAT) : undefined,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
      bookingStatusId: this.editForm.get(['bookingStatusId'])!.value,
      jobAssignmentId: this.editForm.get(['jobAssignmentId'])!.value,
      bookingServices: this.editForm.get(['bookingServices'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookingServiceMember>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
