import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';
import { BookingServiceMemberService } from './booking-service-member.service';

@Component({
  templateUrl: './booking-service-member-delete-dialog.component.html',
})
export class BookingServiceMemberDeleteDialogComponent {
  bookingServiceMember?: IBookingServiceMember;

  constructor(
    protected bookingServiceMemberService: BookingServiceMemberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.bookingServiceMemberService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bookingServiceMemberListModification');
      this.activeModal.close();
    });
  }
}
