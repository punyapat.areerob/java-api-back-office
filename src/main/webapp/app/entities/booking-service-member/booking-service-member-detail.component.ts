import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';

@Component({
  selector: 'jhi-booking-service-member-detail',
  templateUrl: './booking-service-member-detail.component.html',
})
export class BookingServiceMemberDetailComponent implements OnInit {
  bookingServiceMember: IBookingServiceMember | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingServiceMember }) => (this.bookingServiceMember = bookingServiceMember));
  }

  previousState(): void {
    window.history.back();
  }
}
