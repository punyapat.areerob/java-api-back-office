import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';
import { BookingServiceMemberService } from './booking-service-member.service';
import { BookingServiceMemberDeleteDialogComponent } from './booking-service-member-delete-dialog.component';

@Component({
  selector: 'jhi-booking-service-member',
  templateUrl: './booking-service-member.component.html',
})
export class BookingServiceMemberComponent implements OnInit, OnDestroy {
  bookingServiceMembers?: IBookingServiceMember[];
  eventSubscriber?: Subscription;

  constructor(
    protected bookingServiceMemberService: BookingServiceMemberService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.bookingServiceMemberService
      .query()
      .subscribe((res: HttpResponse<IBookingServiceMember[]>) => (this.bookingServiceMembers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBookingServiceMembers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBookingServiceMember): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBookingServiceMembers(): void {
    this.eventSubscriber = this.eventManager.subscribe('bookingServiceMemberListModification', () => this.loadAll());
  }

  delete(bookingServiceMember: IBookingServiceMember): void {
    const modalRef = this.modalService.open(BookingServiceMemberDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bookingServiceMember = bookingServiceMember;
  }
}
