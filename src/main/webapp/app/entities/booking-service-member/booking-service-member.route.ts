import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBookingServiceMember, BookingServiceMember } from 'app/shared/model/booking-service-member.model';
import { BookingServiceMemberService } from './booking-service-member.service';
import { BookingServiceMemberComponent } from './booking-service-member.component';
import { BookingServiceMemberDetailComponent } from './booking-service-member-detail.component';
import { BookingServiceMemberUpdateComponent } from './booking-service-member-update.component';

@Injectable({ providedIn: 'root' })
export class BookingServiceMemberResolve implements Resolve<IBookingServiceMember> {
  constructor(private service: BookingServiceMemberService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookingServiceMember> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bookingServiceMember: HttpResponse<BookingServiceMember>) => {
          if (bookingServiceMember.body) {
            return of(bookingServiceMember.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookingServiceMember());
  }
}

export const bookingServiceMemberRoute: Routes = [
  {
    path: '',
    component: BookingServiceMemberComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServiceMembers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookingServiceMemberDetailComponent,
    resolve: {
      bookingServiceMember: BookingServiceMemberResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServiceMembers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookingServiceMemberUpdateComponent,
    resolve: {
      bookingServiceMember: BookingServiceMemberResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServiceMembers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookingServiceMemberUpdateComponent,
    resolve: {
      bookingServiceMember: BookingServiceMemberResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServiceMembers',
    },
    canActivate: [UserRouteAccessService],
  },
];
