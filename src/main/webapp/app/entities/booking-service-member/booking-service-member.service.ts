import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';

type EntityResponseType = HttpResponse<IBookingServiceMember>;
type EntityArrayResponseType = HttpResponse<IBookingServiceMember[]>;

@Injectable({ providedIn: 'root' })
export class BookingServiceMemberService {
  public resourceUrl = SERVER_API_URL + 'api/booking-service-members';

  constructor(protected http: HttpClient) {}

  create(bookingServiceMember: IBookingServiceMember): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingServiceMember);
    return this.http
      .post<IBookingServiceMember>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bookingServiceMember: IBookingServiceMember): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingServiceMember);
    return this.http
      .put<IBookingServiceMember>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IBookingServiceMember>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBookingServiceMember[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(bookingServiceMember: IBookingServiceMember): IBookingServiceMember {
    const copy: IBookingServiceMember = Object.assign({}, bookingServiceMember, {
      startTime:
        bookingServiceMember.startTime && bookingServiceMember.startTime.isValid() ? bookingServiceMember.startTime.toJSON() : undefined,
      endTime: bookingServiceMember.endTime && bookingServiceMember.endTime.isValid() ? bookingServiceMember.endTime.toJSON() : undefined,
      createdBy:
        bookingServiceMember.createdBy && bookingServiceMember.createdBy.isValid() ? bookingServiceMember.createdBy.toJSON() : undefined,
      updatedBy:
        bookingServiceMember.updatedBy && bookingServiceMember.updatedBy.isValid() ? bookingServiceMember.updatedBy.toJSON() : undefined,
      createdAt:
        bookingServiceMember.createdAt && bookingServiceMember.createdAt.isValid() ? bookingServiceMember.createdAt.toJSON() : undefined,
      updatedAt:
        bookingServiceMember.updatedAt && bookingServiceMember.updatedAt.isValid() ? bookingServiceMember.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startTime = res.body.startTime ? moment(res.body.startTime) : undefined;
      res.body.endTime = res.body.endTime ? moment(res.body.endTime) : undefined;
      res.body.createdBy = res.body.createdBy ? moment(res.body.createdBy) : undefined;
      res.body.updatedBy = res.body.updatedBy ? moment(res.body.updatedBy) : undefined;
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bookingServiceMember: IBookingServiceMember) => {
        bookingServiceMember.startTime = bookingServiceMember.startTime ? moment(bookingServiceMember.startTime) : undefined;
        bookingServiceMember.endTime = bookingServiceMember.endTime ? moment(bookingServiceMember.endTime) : undefined;
        bookingServiceMember.createdBy = bookingServiceMember.createdBy ? moment(bookingServiceMember.createdBy) : undefined;
        bookingServiceMember.updatedBy = bookingServiceMember.updatedBy ? moment(bookingServiceMember.updatedBy) : undefined;
        bookingServiceMember.createdAt = bookingServiceMember.createdAt ? moment(bookingServiceMember.createdAt) : undefined;
        bookingServiceMember.updatedAt = bookingServiceMember.updatedAt ? moment(bookingServiceMember.updatedAt) : undefined;
      });
    }
    return res;
  }
}
