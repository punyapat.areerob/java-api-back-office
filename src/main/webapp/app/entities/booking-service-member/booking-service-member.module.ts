import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BookingServiceMemberComponent } from './booking-service-member.component';
import { BookingServiceMemberDetailComponent } from './booking-service-member-detail.component';
import { BookingServiceMemberUpdateComponent } from './booking-service-member-update.component';
import { BookingServiceMemberDeleteDialogComponent } from './booking-service-member-delete-dialog.component';
import { bookingServiceMemberRoute } from './booking-service-member.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(bookingServiceMemberRoute)],
  declarations: [
    BookingServiceMemberComponent,
    BookingServiceMemberDetailComponent,
    BookingServiceMemberUpdateComponent,
    BookingServiceMemberDeleteDialogComponent,
  ],
  entryComponents: [BookingServiceMemberDeleteDialogComponent],
})
export class MisbeBookingServiceMemberModule {}
