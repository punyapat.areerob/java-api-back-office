import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaseInstance, CaseInstance } from 'app/shared/model/case-instance.model';
import { CaseInstanceService } from './case-instance.service';
import { CaseInstanceComponent } from './case-instance.component';
import { CaseInstanceDetailComponent } from './case-instance-detail.component';
import { CaseInstanceUpdateComponent } from './case-instance-update.component';

@Injectable({ providedIn: 'root' })
export class CaseInstanceResolve implements Resolve<ICaseInstance> {
  constructor(private service: CaseInstanceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaseInstance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caseInstance: HttpResponse<CaseInstance>) => {
          if (caseInstance.body) {
            return of(caseInstance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaseInstance());
  }
}

export const caseInstanceRoute: Routes = [
  {
    path: '',
    component: CaseInstanceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaseInstanceDetailComponent,
    resolve: {
      caseInstance: CaseInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaseInstanceUpdateComponent,
    resolve: {
      caseInstance: CaseInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaseInstanceUpdateComponent,
    resolve: {
      caseInstance: CaseInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseInstances',
    },
    canActivate: [UserRouteAccessService],
  },
];
