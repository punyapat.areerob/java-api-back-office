import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseInstance } from 'app/shared/model/case-instance.model';
import { CaseInstanceService } from './case-instance.service';

@Component({
  templateUrl: './case-instance-delete-dialog.component.html',
})
export class CaseInstanceDeleteDialogComponent {
  caseInstance?: ICaseInstance;

  constructor(
    protected caseInstanceService: CaseInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseInstanceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseInstanceListModification');
      this.activeModal.close();
    });
  }
}
