import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseInstanceComponent } from './case-instance.component';
import { CaseInstanceDetailComponent } from './case-instance-detail.component';
import { CaseInstanceUpdateComponent } from './case-instance-update.component';
import { CaseInstanceDeleteDialogComponent } from './case-instance-delete-dialog.component';
import { caseInstanceRoute } from './case-instance.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseInstanceRoute)],
  declarations: [CaseInstanceComponent, CaseInstanceDetailComponent, CaseInstanceUpdateComponent, CaseInstanceDeleteDialogComponent],
  entryComponents: [CaseInstanceDeleteDialogComponent],
})
export class MisbeCaseInstanceModule {}
