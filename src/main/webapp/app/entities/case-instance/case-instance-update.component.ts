import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICaseInstance, CaseInstance } from 'app/shared/model/case-instance.model';
import { CaseInstanceService } from './case-instance.service';

@Component({
  selector: 'jhi-case-instance-update',
  templateUrl: './case-instance-update.component.html',
})
export class CaseInstanceUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    caseNo: [],
    caseId: [],
    currentStepId: [],
    remark: [],
    memberId: [],
    caseDataId: [],
    stepStatus: [],
    createdBy: [],
    updatedBy: [],
    createdAt: [],
    updatedAt: [],
  });

  constructor(protected caseInstanceService: CaseInstanceService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseInstance }) => {
      if (!caseInstance.id) {
        const today = moment().startOf('day');
        caseInstance.createdAt = today;
        caseInstance.updatedAt = today;
      }

      this.updateForm(caseInstance);
    });
  }

  updateForm(caseInstance: ICaseInstance): void {
    this.editForm.patchValue({
      id: caseInstance.id,
      caseNo: caseInstance.caseNo,
      caseId: caseInstance.caseId,
      currentStepId: caseInstance.currentStepId,
      remark: caseInstance.remark,
      memberId: caseInstance.memberId,
      caseDataId: caseInstance.caseDataId,
      stepStatus: caseInstance.stepStatus,
      createdBy: caseInstance.createdBy,
      updatedBy: caseInstance.updatedBy,
      createdAt: caseInstance.createdAt ? caseInstance.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: caseInstance.updatedAt ? caseInstance.updatedAt.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseInstance = this.createFromForm();
    if (caseInstance.id !== undefined) {
      this.subscribeToSaveResponse(this.caseInstanceService.update(caseInstance));
    } else {
      this.subscribeToSaveResponse(this.caseInstanceService.create(caseInstance));
    }
  }

  private createFromForm(): ICaseInstance {
    return {
      ...new CaseInstance(),
      id: this.editForm.get(['id'])!.value,
      caseNo: this.editForm.get(['caseNo'])!.value,
      caseId: this.editForm.get(['caseId'])!.value,
      currentStepId: this.editForm.get(['currentStepId'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      memberId: this.editForm.get(['memberId'])!.value,
      caseDataId: this.editForm.get(['caseDataId'])!.value,
      stepStatus: this.editForm.get(['stepStatus'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseInstance>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
