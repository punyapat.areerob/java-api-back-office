import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseInstance } from 'app/shared/model/case-instance.model';

@Component({
  selector: 'jhi-case-instance-detail',
  templateUrl: './case-instance-detail.component.html',
})
export class CaseInstanceDetailComponent implements OnInit {
  caseInstance: ICaseInstance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseInstance }) => (this.caseInstance = caseInstance));
  }

  previousState(): void {
    window.history.back();
  }
}
