import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICaseInstance } from 'app/shared/model/case-instance.model';

type EntityResponseType = HttpResponse<ICaseInstance>;
type EntityArrayResponseType = HttpResponse<ICaseInstance[]>;

@Injectable({ providedIn: 'root' })
export class CaseInstanceService {
  public resourceUrl = SERVER_API_URL + 'api/case-instances';

  constructor(protected http: HttpClient) {}

  create(caseInstance: ICaseInstance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caseInstance);
    return this.http
      .post<ICaseInstance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(caseInstance: ICaseInstance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caseInstance);
    return this.http
      .put<ICaseInstance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<ICaseInstance>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICaseInstance[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(caseInstance: ICaseInstance): ICaseInstance {
    const copy: ICaseInstance = Object.assign({}, caseInstance, {
      createdAt: caseInstance.createdAt && caseInstance.createdAt.isValid() ? caseInstance.createdAt.toJSON() : undefined,
      updatedAt: caseInstance.updatedAt && caseInstance.updatedAt.isValid() ? caseInstance.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((caseInstance: ICaseInstance) => {
        caseInstance.createdAt = caseInstance.createdAt ? moment(caseInstance.createdAt) : undefined;
        caseInstance.updatedAt = caseInstance.updatedAt ? moment(caseInstance.updatedAt) : undefined;
      });
    }
    return res;
  }
}
