import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseInstance } from 'app/shared/model/case-instance.model';
import { CaseInstanceService } from './case-instance.service';
import { CaseInstanceDeleteDialogComponent } from './case-instance-delete-dialog.component';

@Component({
  selector: 'jhi-case-instance',
  templateUrl: './case-instance.component.html',
})
export class CaseInstanceComponent implements OnInit, OnDestroy {
  caseInstances?: ICaseInstance[];
  eventSubscriber?: Subscription;

  constructor(
    protected caseInstanceService: CaseInstanceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.caseInstanceService.query().subscribe((res: HttpResponse<ICaseInstance[]>) => (this.caseInstances = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseInstance): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseInstanceListModification', () => this.loadAll());
  }

  delete(caseInstance: ICaseInstance): void {
    const modalRef = this.modalService.open(CaseInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseInstance = caseInstance;
  }
}
