import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBloodType, BloodType } from 'app/shared/model/blood-type.model';
import { BloodTypeService } from './blood-type.service';

@Component({
  selector: 'jhi-blood-type-update',
  templateUrl: './blood-type-update.component.html',
})
export class BloodTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(protected bloodTypeService: BloodTypeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bloodType }) => {
      this.updateForm(bloodType);
    });
  }

  updateForm(bloodType: IBloodType): void {
    this.editForm.patchValue({
      id: bloodType.id,
      name: bloodType.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bloodType = this.createFromForm();
    if (bloodType.id !== undefined) {
      this.subscribeToSaveResponse(this.bloodTypeService.update(bloodType));
    } else {
      this.subscribeToSaveResponse(this.bloodTypeService.create(bloodType));
    }
  }

  private createFromForm(): IBloodType {
    return {
      ...new BloodType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBloodType>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
