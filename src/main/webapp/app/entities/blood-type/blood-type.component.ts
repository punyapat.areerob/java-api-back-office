import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBloodType } from 'app/shared/model/blood-type.model';
import { BloodTypeService } from './blood-type.service';
import { BloodTypeDeleteDialogComponent } from './blood-type-delete-dialog.component';

@Component({
  selector: 'jhi-blood-type',
  templateUrl: './blood-type.component.html',
})
export class BloodTypeComponent implements OnInit, OnDestroy {
  bloodTypes?: IBloodType[];
  eventSubscriber?: Subscription;

  constructor(protected bloodTypeService: BloodTypeService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.bloodTypeService.query().subscribe((res: HttpResponse<IBloodType[]>) => (this.bloodTypes = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBloodTypes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBloodType): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBloodTypes(): void {
    this.eventSubscriber = this.eventManager.subscribe('bloodTypeListModification', () => this.loadAll());
  }

  delete(bloodType: IBloodType): void {
    const modalRef = this.modalService.open(BloodTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bloodType = bloodType;
  }
}
