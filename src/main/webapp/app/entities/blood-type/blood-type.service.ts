import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBloodType } from 'app/shared/model/blood-type.model';

type EntityResponseType = HttpResponse<IBloodType>;
type EntityArrayResponseType = HttpResponse<IBloodType[]>;

@Injectable({ providedIn: 'root' })
export class BloodTypeService {
  public resourceUrl = SERVER_API_URL + 'api/blood-types';

  constructor(protected http: HttpClient) {}

  create(bloodType: IBloodType): Observable<EntityResponseType> {
    return this.http.post<IBloodType>(this.resourceUrl, bloodType, { observe: 'response' });
  }

  update(bloodType: IBloodType): Observable<EntityResponseType> {
    return this.http.put<IBloodType>(this.resourceUrl, bloodType, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IBloodType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBloodType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
