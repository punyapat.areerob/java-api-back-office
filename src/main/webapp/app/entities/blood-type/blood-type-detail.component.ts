import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBloodType } from 'app/shared/model/blood-type.model';

@Component({
  selector: 'jhi-blood-type-detail',
  templateUrl: './blood-type-detail.component.html',
})
export class BloodTypeDetailComponent implements OnInit {
  bloodType: IBloodType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bloodType }) => (this.bloodType = bloodType));
  }

  previousState(): void {
    window.history.back();
  }
}
