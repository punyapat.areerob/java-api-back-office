import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBloodType, BloodType } from 'app/shared/model/blood-type.model';
import { BloodTypeService } from './blood-type.service';
import { BloodTypeComponent } from './blood-type.component';
import { BloodTypeDetailComponent } from './blood-type-detail.component';
import { BloodTypeUpdateComponent } from './blood-type-update.component';

@Injectable({ providedIn: 'root' })
export class BloodTypeResolve implements Resolve<IBloodType> {
  constructor(private service: BloodTypeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBloodType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bloodType: HttpResponse<BloodType>) => {
          if (bloodType.body) {
            return of(bloodType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BloodType());
  }
}

export const bloodTypeRoute: Routes = [
  {
    path: '',
    component: BloodTypeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BloodTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BloodTypeDetailComponent,
    resolve: {
      bloodType: BloodTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BloodTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BloodTypeUpdateComponent,
    resolve: {
      bloodType: BloodTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BloodTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BloodTypeUpdateComponent,
    resolve: {
      bloodType: BloodTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BloodTypes',
    },
    canActivate: [UserRouteAccessService],
  },
];
