import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBloodType } from 'app/shared/model/blood-type.model';
import { BloodTypeService } from './blood-type.service';

@Component({
  templateUrl: './blood-type-delete-dialog.component.html',
})
export class BloodTypeDeleteDialogComponent {
  bloodType?: IBloodType;

  constructor(protected bloodTypeService: BloodTypeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.bloodTypeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bloodTypeListModification');
      this.activeModal.close();
    });
  }
}
