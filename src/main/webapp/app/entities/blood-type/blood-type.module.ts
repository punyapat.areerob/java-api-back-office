import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BloodTypeComponent } from './blood-type.component';
import { BloodTypeDetailComponent } from './blood-type-detail.component';
import { BloodTypeUpdateComponent } from './blood-type-update.component';
import { BloodTypeDeleteDialogComponent } from './blood-type-delete-dialog.component';
import { bloodTypeRoute } from './blood-type.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(bloodTypeRoute)],
  declarations: [BloodTypeComponent, BloodTypeDetailComponent, BloodTypeUpdateComponent, BloodTypeDeleteDialogComponent],
  entryComponents: [BloodTypeDeleteDialogComponent],
})
export class MisbeBloodTypeModule {}
