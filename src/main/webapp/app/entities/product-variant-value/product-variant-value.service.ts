import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductVariantValue } from 'app/shared/model/product-variant-value.model';

type EntityResponseType = HttpResponse<IProductVariantValue>;
type EntityArrayResponseType = HttpResponse<IProductVariantValue[]>;

@Injectable({ providedIn: 'root' })
export class ProductVariantValueService {
  public resourceUrl = SERVER_API_URL + 'api/product-variant-values';

  constructor(protected http: HttpClient) {}

  create(productVariantValue: IProductVariantValue): Observable<EntityResponseType> {
    return this.http.post<IProductVariantValue>(this.resourceUrl, productVariantValue, { observe: 'response' });
  }

  update(productVariantValue: IProductVariantValue): Observable<EntityResponseType> {
    return this.http.put<IProductVariantValue>(this.resourceUrl, productVariantValue, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IProductVariantValue>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductVariantValue[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
