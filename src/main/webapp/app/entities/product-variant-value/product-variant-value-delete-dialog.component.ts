import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductVariantValue } from 'app/shared/model/product-variant-value.model';
import { ProductVariantValueService } from './product-variant-value.service';

@Component({
  templateUrl: './product-variant-value-delete-dialog.component.html',
})
export class ProductVariantValueDeleteDialogComponent {
  productVariantValue?: IProductVariantValue;

  constructor(
    protected productVariantValueService: ProductVariantValueService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.productVariantValueService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productVariantValueListModification');
      this.activeModal.close();
    });
  }
}
