import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductVariantValue } from 'app/shared/model/product-variant-value.model';
import { ProductVariantValueService } from './product-variant-value.service';
import { ProductVariantValueDeleteDialogComponent } from './product-variant-value-delete-dialog.component';

@Component({
  selector: 'jhi-product-variant-value',
  templateUrl: './product-variant-value.component.html',
})
export class ProductVariantValueComponent implements OnInit, OnDestroy {
  productVariantValues?: IProductVariantValue[];
  eventSubscriber?: Subscription;

  constructor(
    protected productVariantValueService: ProductVariantValueService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.productVariantValueService
      .query()
      .subscribe((res: HttpResponse<IProductVariantValue[]>) => (this.productVariantValues = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductVariantValues();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductVariantValue): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductVariantValues(): void {
    this.eventSubscriber = this.eventManager.subscribe('productVariantValueListModification', () => this.loadAll());
  }

  delete(productVariantValue: IProductVariantValue): void {
    const modalRef = this.modalService.open(ProductVariantValueDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productVariantValue = productVariantValue;
  }
}
