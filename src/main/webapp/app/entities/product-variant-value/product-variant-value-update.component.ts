import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductVariantValue, ProductVariantValue } from 'app/shared/model/product-variant-value.model';
import { ProductVariantValueService } from './product-variant-value.service';

@Component({
  selector: 'jhi-product-variant-value-update',
  templateUrl: './product-variant-value-update.component.html',
})
export class ProductVariantValueUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    productVariantGroupId: [],
    name: [],
    size: [],
    status: [],
    active: [],
  });

  constructor(
    protected productVariantValueService: ProductVariantValueService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productVariantValue }) => {
      this.updateForm(productVariantValue);
    });
  }

  updateForm(productVariantValue: IProductVariantValue): void {
    this.editForm.patchValue({
      id: productVariantValue.id,
      productVariantGroupId: productVariantValue.productVariantGroupId,
      name: productVariantValue.name,
      size: productVariantValue.size,
      status: productVariantValue.status,
      active: productVariantValue.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productVariantValue = this.createFromForm();
    if (productVariantValue.id !== undefined) {
      this.subscribeToSaveResponse(this.productVariantValueService.update(productVariantValue));
    } else {
      this.subscribeToSaveResponse(this.productVariantValueService.create(productVariantValue));
    }
  }

  private createFromForm(): IProductVariantValue {
    return {
      ...new ProductVariantValue(),
      id: this.editForm.get(['id'])!.value,
      productVariantGroupId: this.editForm.get(['productVariantGroupId'])!.value,
      name: this.editForm.get(['name'])!.value,
      size: this.editForm.get(['size'])!.value,
      status: this.editForm.get(['status'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductVariantValue>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
