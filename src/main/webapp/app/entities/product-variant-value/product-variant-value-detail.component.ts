import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductVariantValue } from 'app/shared/model/product-variant-value.model';

@Component({
  selector: 'jhi-product-variant-value-detail',
  templateUrl: './product-variant-value-detail.component.html',
})
export class ProductVariantValueDetailComponent implements OnInit {
  productVariantValue: IProductVariantValue | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productVariantValue }) => (this.productVariantValue = productVariantValue));
  }

  previousState(): void {
    window.history.back();
  }
}
