import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ProductVariantValueComponent } from './product-variant-value.component';
import { ProductVariantValueDetailComponent } from './product-variant-value-detail.component';
import { ProductVariantValueUpdateComponent } from './product-variant-value-update.component';
import { ProductVariantValueDeleteDialogComponent } from './product-variant-value-delete-dialog.component';
import { productVariantValueRoute } from './product-variant-value.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(productVariantValueRoute)],
  declarations: [
    ProductVariantValueComponent,
    ProductVariantValueDetailComponent,
    ProductVariantValueUpdateComponent,
    ProductVariantValueDeleteDialogComponent,
  ],
  entryComponents: [ProductVariantValueDeleteDialogComponent],
})
export class MisbeProductVariantValueModule {}
