import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductVariantValue, ProductVariantValue } from 'app/shared/model/product-variant-value.model';
import { ProductVariantValueService } from './product-variant-value.service';
import { ProductVariantValueComponent } from './product-variant-value.component';
import { ProductVariantValueDetailComponent } from './product-variant-value-detail.component';
import { ProductVariantValueUpdateComponent } from './product-variant-value-update.component';

@Injectable({ providedIn: 'root' })
export class ProductVariantValueResolve implements Resolve<IProductVariantValue> {
  constructor(private service: ProductVariantValueService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductVariantValue> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productVariantValue: HttpResponse<ProductVariantValue>) => {
          if (productVariantValue.body) {
            return of(productVariantValue.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductVariantValue());
  }
}

export const productVariantValueRoute: Routes = [
  {
    path: '',
    component: ProductVariantValueComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductVariantValues',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductVariantValueDetailComponent,
    resolve: {
      productVariantValue: ProductVariantValueResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductVariantValues',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductVariantValueUpdateComponent,
    resolve: {
      productVariantValue: ProductVariantValueResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductVariantValues',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductVariantValueUpdateComponent,
    resolve: {
      productVariantValue: ProductVariantValueResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductVariantValues',
    },
    canActivate: [UserRouteAccessService],
  },
];
