import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { StepOwnerComponent } from './step-owner.component';
import { StepOwnerDetailComponent } from './step-owner-detail.component';
import { StepOwnerUpdateComponent } from './step-owner-update.component';
import { StepOwnerDeleteDialogComponent } from './step-owner-delete-dialog.component';
import { stepOwnerRoute } from './step-owner.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(stepOwnerRoute)],
  declarations: [StepOwnerComponent, StepOwnerDetailComponent, StepOwnerUpdateComponent, StepOwnerDeleteDialogComponent],
  entryComponents: [StepOwnerDeleteDialogComponent],
})
export class MisbeStepOwnerModule {}
