import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStepOwner } from 'app/shared/model/step-owner.model';
import { StepOwnerService } from './step-owner.service';

@Component({
  templateUrl: './step-owner-delete-dialog.component.html',
})
export class StepOwnerDeleteDialogComponent {
  stepOwner?: IStepOwner;

  constructor(protected stepOwnerService: StepOwnerService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.stepOwnerService.delete(id).subscribe(() => {
      this.eventManager.broadcast('stepOwnerListModification');
      this.activeModal.close();
    });
  }
}
