import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStepOwner } from 'app/shared/model/step-owner.model';
import { StepOwnerService } from './step-owner.service';
import { StepOwnerDeleteDialogComponent } from './step-owner-delete-dialog.component';

@Component({
  selector: 'jhi-step-owner',
  templateUrl: './step-owner.component.html',
})
export class StepOwnerComponent implements OnInit, OnDestroy {
  stepOwners?: IStepOwner[];
  eventSubscriber?: Subscription;

  constructor(protected stepOwnerService: StepOwnerService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.stepOwnerService.query().subscribe((res: HttpResponse<IStepOwner[]>) => (this.stepOwners = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInStepOwners();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IStepOwner): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInStepOwners(): void {
    this.eventSubscriber = this.eventManager.subscribe('stepOwnerListModification', () => this.loadAll());
  }

  delete(stepOwner: IStepOwner): void {
    const modalRef = this.modalService.open(StepOwnerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.stepOwner = stepOwner;
  }
}
