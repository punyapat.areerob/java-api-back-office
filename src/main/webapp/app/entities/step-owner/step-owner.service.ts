import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStepOwner } from 'app/shared/model/step-owner.model';

type EntityResponseType = HttpResponse<IStepOwner>;
type EntityArrayResponseType = HttpResponse<IStepOwner[]>;

@Injectable({ providedIn: 'root' })
export class StepOwnerService {
  public resourceUrl = SERVER_API_URL + 'api/step-owners';

  constructor(protected http: HttpClient) {}

  create(stepOwner: IStepOwner): Observable<EntityResponseType> {
    return this.http.post<IStepOwner>(this.resourceUrl, stepOwner, { observe: 'response' });
  }

  update(stepOwner: IStepOwner): Observable<EntityResponseType> {
    return this.http.put<IStepOwner>(this.resourceUrl, stepOwner, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IStepOwner>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IStepOwner[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
