import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IStepOwner } from 'app/shared/model/step-owner.model';

@Component({
  selector: 'jhi-step-owner-detail',
  templateUrl: './step-owner-detail.component.html',
})
export class StepOwnerDetailComponent implements OnInit {
  stepOwner: IStepOwner | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ stepOwner }) => (this.stepOwner = stepOwner));
  }

  previousState(): void {
    window.history.back();
  }
}
