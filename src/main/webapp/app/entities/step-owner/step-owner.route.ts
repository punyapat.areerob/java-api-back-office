import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IStepOwner, StepOwner } from 'app/shared/model/step-owner.model';
import { StepOwnerService } from './step-owner.service';
import { StepOwnerComponent } from './step-owner.component';
import { StepOwnerDetailComponent } from './step-owner-detail.component';
import { StepOwnerUpdateComponent } from './step-owner-update.component';

@Injectable({ providedIn: 'root' })
export class StepOwnerResolve implements Resolve<IStepOwner> {
  constructor(private service: StepOwnerService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStepOwner> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((stepOwner: HttpResponse<StepOwner>) => {
          if (stepOwner.body) {
            return of(stepOwner.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new StepOwner());
  }
}

export const stepOwnerRoute: Routes = [
  {
    path: '',
    component: StepOwnerComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'StepOwners',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: StepOwnerDetailComponent,
    resolve: {
      stepOwner: StepOwnerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'StepOwners',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: StepOwnerUpdateComponent,
    resolve: {
      stepOwner: StepOwnerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'StepOwners',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: StepOwnerUpdateComponent,
    resolve: {
      stepOwner: StepOwnerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'StepOwners',
    },
    canActivate: [UserRouteAccessService],
  },
];
