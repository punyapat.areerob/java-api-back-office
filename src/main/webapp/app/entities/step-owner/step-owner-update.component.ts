import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IStepOwner, StepOwner } from 'app/shared/model/step-owner.model';
import { StepOwnerService } from './step-owner.service';

@Component({
  selector: 'jhi-step-owner-update',
  templateUrl: './step-owner-update.component.html',
})
export class StepOwnerUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    staffId: [],
    givenName: [],
    middleName: [],
    surName: [],
    email: [],
  });

  constructor(protected stepOwnerService: StepOwnerService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ stepOwner }) => {
      this.updateForm(stepOwner);
    });
  }

  updateForm(stepOwner: IStepOwner): void {
    this.editForm.patchValue({
      id: stepOwner.id,
      staffId: stepOwner.staffId,
      givenName: stepOwner.givenName,
      middleName: stepOwner.middleName,
      surName: stepOwner.surName,
      email: stepOwner.email,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const stepOwner = this.createFromForm();
    if (stepOwner.id !== undefined) {
      this.subscribeToSaveResponse(this.stepOwnerService.update(stepOwner));
    } else {
      this.subscribeToSaveResponse(this.stepOwnerService.create(stepOwner));
    }
  }

  private createFromForm(): IStepOwner {
    return {
      ...new StepOwner(),
      id: this.editForm.get(['id'])!.value,
      staffId: this.editForm.get(['staffId'])!.value,
      givenName: this.editForm.get(['givenName'])!.value,
      middleName: this.editForm.get(['middleName'])!.value,
      surName: this.editForm.get(['surName'])!.value,
      email: this.editForm.get(['email'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStepOwner>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
