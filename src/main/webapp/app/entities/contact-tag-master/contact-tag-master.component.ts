import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IContactTagMaster } from 'app/shared/model/contact-tag-master.model';
import { ContactTagMasterService } from './contact-tag-master.service';
import { ContactTagMasterDeleteDialogComponent } from './contact-tag-master-delete-dialog.component';

@Component({
  selector: 'jhi-contact-tag-master',
  templateUrl: './contact-tag-master.component.html',
})
export class ContactTagMasterComponent implements OnInit, OnDestroy {
  contactTagMasters?: IContactTagMaster[];
  eventSubscriber?: Subscription;

  constructor(
    protected contactTagMasterService: ContactTagMasterService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.contactTagMasterService.query().subscribe((res: HttpResponse<IContactTagMaster[]>) => (this.contactTagMasters = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInContactTagMasters();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IContactTagMaster): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInContactTagMasters(): void {
    this.eventSubscriber = this.eventManager.subscribe('contactTagMasterListModification', () => this.loadAll());
  }

  delete(contactTagMaster: IContactTagMaster): void {
    const modalRef = this.modalService.open(ContactTagMasterDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.contactTagMaster = contactTagMaster;
  }
}
