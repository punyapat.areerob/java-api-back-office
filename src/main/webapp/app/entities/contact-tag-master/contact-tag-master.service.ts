import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IContactTagMaster } from 'app/shared/model/contact-tag-master.model';

type EntityResponseType = HttpResponse<IContactTagMaster>;
type EntityArrayResponseType = HttpResponse<IContactTagMaster[]>;

@Injectable({ providedIn: 'root' })
export class ContactTagMasterService {
  public resourceUrl = SERVER_API_URL + 'api/contact-tag-masters';

  constructor(protected http: HttpClient) {}

  create(contactTagMaster: IContactTagMaster): Observable<EntityResponseType> {
    return this.http.post<IContactTagMaster>(this.resourceUrl, contactTagMaster, { observe: 'response' });
  }

  update(contactTagMaster: IContactTagMaster): Observable<EntityResponseType> {
    return this.http.put<IContactTagMaster>(this.resourceUrl, contactTagMaster, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IContactTagMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IContactTagMaster[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
