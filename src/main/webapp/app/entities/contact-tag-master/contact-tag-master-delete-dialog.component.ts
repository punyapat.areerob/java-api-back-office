import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContactTagMaster } from 'app/shared/model/contact-tag-master.model';
import { ContactTagMasterService } from './contact-tag-master.service';

@Component({
  templateUrl: './contact-tag-master-delete-dialog.component.html',
})
export class ContactTagMasterDeleteDialogComponent {
  contactTagMaster?: IContactTagMaster;

  constructor(
    protected contactTagMasterService: ContactTagMasterService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.contactTagMasterService.delete(id).subscribe(() => {
      this.eventManager.broadcast('contactTagMasterListModification');
      this.activeModal.close();
    });
  }
}
