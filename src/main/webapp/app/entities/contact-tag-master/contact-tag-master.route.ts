import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IContactTagMaster, ContactTagMaster } from 'app/shared/model/contact-tag-master.model';
import { ContactTagMasterService } from './contact-tag-master.service';
import { ContactTagMasterComponent } from './contact-tag-master.component';
import { ContactTagMasterDetailComponent } from './contact-tag-master-detail.component';
import { ContactTagMasterUpdateComponent } from './contact-tag-master-update.component';

@Injectable({ providedIn: 'root' })
export class ContactTagMasterResolve implements Resolve<IContactTagMaster> {
  constructor(private service: ContactTagMasterService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IContactTagMaster> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((contactTagMaster: HttpResponse<ContactTagMaster>) => {
          if (contactTagMaster.body) {
            return of(contactTagMaster.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ContactTagMaster());
  }
}

export const contactTagMasterRoute: Routes = [
  {
    path: '',
    component: ContactTagMasterComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ContactTagMasters',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ContactTagMasterDetailComponent,
    resolve: {
      contactTagMaster: ContactTagMasterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ContactTagMasters',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ContactTagMasterUpdateComponent,
    resolve: {
      contactTagMaster: ContactTagMasterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ContactTagMasters',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ContactTagMasterUpdateComponent,
    resolve: {
      contactTagMaster: ContactTagMasterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ContactTagMasters',
    },
    canActivate: [UserRouteAccessService],
  },
];
