import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContactTagMaster } from 'app/shared/model/contact-tag-master.model';

@Component({
  selector: 'jhi-contact-tag-master-detail',
  templateUrl: './contact-tag-master-detail.component.html',
})
export class ContactTagMasterDetailComponent implements OnInit {
  contactTagMaster: IContactTagMaster | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ contactTagMaster }) => (this.contactTagMaster = contactTagMaster));
  }

  previousState(): void {
    window.history.back();
  }
}
