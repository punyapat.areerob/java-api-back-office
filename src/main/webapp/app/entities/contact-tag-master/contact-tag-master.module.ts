import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ContactTagMasterComponent } from './contact-tag-master.component';
import { ContactTagMasterDetailComponent } from './contact-tag-master-detail.component';
import { ContactTagMasterUpdateComponent } from './contact-tag-master-update.component';
import { ContactTagMasterDeleteDialogComponent } from './contact-tag-master-delete-dialog.component';
import { contactTagMasterRoute } from './contact-tag-master.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(contactTagMasterRoute)],
  declarations: [
    ContactTagMasterComponent,
    ContactTagMasterDetailComponent,
    ContactTagMasterUpdateComponent,
    ContactTagMasterDeleteDialogComponent,
  ],
  entryComponents: [ContactTagMasterDeleteDialogComponent],
})
export class MisbeContactTagMasterModule {}
