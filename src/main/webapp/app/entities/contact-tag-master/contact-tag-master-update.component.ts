import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IContactTagMaster, ContactTagMaster } from 'app/shared/model/contact-tag-master.model';
import { ContactTagMasterService } from './contact-tag-master.service';

@Component({
  selector: 'jhi-contact-tag-master-update',
  templateUrl: './contact-tag-master-update.component.html',
})
export class ContactTagMasterUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    color: [],
    createBy: [],
    active: [],
  });

  constructor(
    protected contactTagMasterService: ContactTagMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ contactTagMaster }) => {
      this.updateForm(contactTagMaster);
    });
  }

  updateForm(contactTagMaster: IContactTagMaster): void {
    this.editForm.patchValue({
      id: contactTagMaster.id,
      name: contactTagMaster.name,
      color: contactTagMaster.color,
      createBy: contactTagMaster.createBy,
      active: contactTagMaster.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const contactTagMaster = this.createFromForm();
    if (contactTagMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.contactTagMasterService.update(contactTagMaster));
    } else {
      this.subscribeToSaveResponse(this.contactTagMasterService.create(contactTagMaster));
    }
  }

  private createFromForm(): IContactTagMaster {
    return {
      ...new ContactTagMaster(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      color: this.editForm.get(['color'])!.value,
      createBy: this.editForm.get(['createBy'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContactTagMaster>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
