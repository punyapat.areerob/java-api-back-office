import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEmailTemplateMeta } from 'app/shared/model/email-template-meta.model';
import { EmailTemplateMetaService } from './email-template-meta.service';

@Component({
  templateUrl: './email-template-meta-delete-dialog.component.html',
})
export class EmailTemplateMetaDeleteDialogComponent {
  emailTemplateMeta?: IEmailTemplateMeta;

  constructor(
    protected emailTemplateMetaService: EmailTemplateMetaService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.emailTemplateMetaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('emailTemplateMetaListModification');
      this.activeModal.close();
    });
  }
}
