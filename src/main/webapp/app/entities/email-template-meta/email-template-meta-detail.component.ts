import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmailTemplateMeta } from 'app/shared/model/email-template-meta.model';

@Component({
  selector: 'jhi-email-template-meta-detail',
  templateUrl: './email-template-meta-detail.component.html',
})
export class EmailTemplateMetaDetailComponent implements OnInit {
  emailTemplateMeta: IEmailTemplateMeta | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailTemplateMeta }) => (this.emailTemplateMeta = emailTemplateMeta));
  }

  previousState(): void {
    window.history.back();
  }
}
