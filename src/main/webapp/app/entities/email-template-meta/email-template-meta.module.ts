import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { EmailTemplateMetaComponent } from './email-template-meta.component';
import { EmailTemplateMetaDetailComponent } from './email-template-meta-detail.component';
import { EmailTemplateMetaUpdateComponent } from './email-template-meta-update.component';
import { EmailTemplateMetaDeleteDialogComponent } from './email-template-meta-delete-dialog.component';
import { emailTemplateMetaRoute } from './email-template-meta.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(emailTemplateMetaRoute)],
  declarations: [
    EmailTemplateMetaComponent,
    EmailTemplateMetaDetailComponent,
    EmailTemplateMetaUpdateComponent,
    EmailTemplateMetaDeleteDialogComponent,
  ],
  entryComponents: [EmailTemplateMetaDeleteDialogComponent],
})
export class MisbeEmailTemplateMetaModule {}
