import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEmailTemplateMeta, EmailTemplateMeta } from 'app/shared/model/email-template-meta.model';
import { EmailTemplateMetaService } from './email-template-meta.service';
import { EmailTemplateMetaComponent } from './email-template-meta.component';
import { EmailTemplateMetaDetailComponent } from './email-template-meta-detail.component';
import { EmailTemplateMetaUpdateComponent } from './email-template-meta-update.component';

@Injectable({ providedIn: 'root' })
export class EmailTemplateMetaResolve implements Resolve<IEmailTemplateMeta> {
  constructor(private service: EmailTemplateMetaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEmailTemplateMeta> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((emailTemplateMeta: HttpResponse<EmailTemplateMeta>) => {
          if (emailTemplateMeta.body) {
            return of(emailTemplateMeta.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EmailTemplateMeta());
  }
}

export const emailTemplateMetaRoute: Routes = [
  {
    path: '',
    component: EmailTemplateMetaComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplateMetas',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmailTemplateMetaDetailComponent,
    resolve: {
      emailTemplateMeta: EmailTemplateMetaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplateMetas',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmailTemplateMetaUpdateComponent,
    resolve: {
      emailTemplateMeta: EmailTemplateMetaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplateMetas',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmailTemplateMetaUpdateComponent,
    resolve: {
      emailTemplateMeta: EmailTemplateMetaResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplateMetas',
    },
    canActivate: [UserRouteAccessService],
  },
];
