import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmailTemplateMeta } from 'app/shared/model/email-template-meta.model';
import { EmailTemplateMetaService } from './email-template-meta.service';
import { EmailTemplateMetaDeleteDialogComponent } from './email-template-meta-delete-dialog.component';

@Component({
  selector: 'jhi-email-template-meta',
  templateUrl: './email-template-meta.component.html',
})
export class EmailTemplateMetaComponent implements OnInit, OnDestroy {
  emailTemplateMetas?: IEmailTemplateMeta[];
  eventSubscriber?: Subscription;

  constructor(
    protected emailTemplateMetaService: EmailTemplateMetaService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.emailTemplateMetaService
      .query()
      .subscribe((res: HttpResponse<IEmailTemplateMeta[]>) => (this.emailTemplateMetas = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEmailTemplateMetas();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IEmailTemplateMeta): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEmailTemplateMetas(): void {
    this.eventSubscriber = this.eventManager.subscribe('emailTemplateMetaListModification', () => this.loadAll());
  }

  delete(emailTemplateMeta: IEmailTemplateMeta): void {
    const modalRef = this.modalService.open(EmailTemplateMetaDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.emailTemplateMeta = emailTemplateMeta;
  }
}
