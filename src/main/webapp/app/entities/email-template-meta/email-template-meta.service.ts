import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEmailTemplateMeta } from 'app/shared/model/email-template-meta.model';

type EntityResponseType = HttpResponse<IEmailTemplateMeta>;
type EntityArrayResponseType = HttpResponse<IEmailTemplateMeta[]>;

@Injectable({ providedIn: 'root' })
export class EmailTemplateMetaService {
  public resourceUrl = SERVER_API_URL + 'api/email-template-metas';

  constructor(protected http: HttpClient) {}

  create(emailTemplateMeta: IEmailTemplateMeta): Observable<EntityResponseType> {
    return this.http.post<IEmailTemplateMeta>(this.resourceUrl, emailTemplateMeta, { observe: 'response' });
  }

  update(emailTemplateMeta: IEmailTemplateMeta): Observable<EntityResponseType> {
    return this.http.put<IEmailTemplateMeta>(this.resourceUrl, emailTemplateMeta, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IEmailTemplateMeta>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmailTemplateMeta[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
