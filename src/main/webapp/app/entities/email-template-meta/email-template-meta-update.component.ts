import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEmailTemplateMeta, EmailTemplateMeta } from 'app/shared/model/email-template-meta.model';
import { EmailTemplateMetaService } from './email-template-meta.service';

@Component({
  selector: 'jhi-email-template-meta-update',
  templateUrl: './email-template-meta-update.component.html',
})
export class EmailTemplateMetaUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(
    protected emailTemplateMetaService: EmailTemplateMetaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailTemplateMeta }) => {
      this.updateForm(emailTemplateMeta);
    });
  }

  updateForm(emailTemplateMeta: IEmailTemplateMeta): void {
    this.editForm.patchValue({
      id: emailTemplateMeta.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const emailTemplateMeta = this.createFromForm();
    if (emailTemplateMeta.id !== undefined) {
      this.subscribeToSaveResponse(this.emailTemplateMetaService.update(emailTemplateMeta));
    } else {
      this.subscribeToSaveResponse(this.emailTemplateMetaService.create(emailTemplateMeta));
    }
  }

  private createFromForm(): IEmailTemplateMeta {
    return {
      ...new EmailTemplateMeta(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmailTemplateMeta>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
