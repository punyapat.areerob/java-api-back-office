import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IActionInstance, ActionInstance } from 'app/shared/model/action-instance.model';
import { ActionInstanceService } from './action-instance.service';
import { ActionInstanceComponent } from './action-instance.component';
import { ActionInstanceDetailComponent } from './action-instance-detail.component';
import { ActionInstanceUpdateComponent } from './action-instance-update.component';

@Injectable({ providedIn: 'root' })
export class ActionInstanceResolve implements Resolve<IActionInstance> {
  constructor(private service: ActionInstanceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IActionInstance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((actionInstance: HttpResponse<ActionInstance>) => {
          if (actionInstance.body) {
            return of(actionInstance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ActionInstance());
  }
}

export const actionInstanceRoute: Routes = [
  {
    path: '',
    component: ActionInstanceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ActionInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ActionInstanceDetailComponent,
    resolve: {
      actionInstance: ActionInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ActionInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ActionInstanceUpdateComponent,
    resolve: {
      actionInstance: ActionInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ActionInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ActionInstanceUpdateComponent,
    resolve: {
      actionInstance: ActionInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ActionInstances',
    },
    canActivate: [UserRouteAccessService],
  },
];
