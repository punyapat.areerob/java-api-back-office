import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ActionInstanceComponent } from './action-instance.component';
import { ActionInstanceDetailComponent } from './action-instance-detail.component';
import { ActionInstanceUpdateComponent } from './action-instance-update.component';
import { ActionInstanceDeleteDialogComponent } from './action-instance-delete-dialog.component';
import { actionInstanceRoute } from './action-instance.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(actionInstanceRoute)],
  declarations: [
    ActionInstanceComponent,
    ActionInstanceDetailComponent,
    ActionInstanceUpdateComponent,
    ActionInstanceDeleteDialogComponent,
  ],
  entryComponents: [ActionInstanceDeleteDialogComponent],
})
export class MisbeActionInstanceModule {}
