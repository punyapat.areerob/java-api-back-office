import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IActionInstance } from 'app/shared/model/action-instance.model';
import { ActionInstanceService } from './action-instance.service';

@Component({
  templateUrl: './action-instance-delete-dialog.component.html',
})
export class ActionInstanceDeleteDialogComponent {
  actionInstance?: IActionInstance;

  constructor(
    protected actionInstanceService: ActionInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.actionInstanceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('actionInstanceListModification');
      this.activeModal.close();
    });
  }
}
