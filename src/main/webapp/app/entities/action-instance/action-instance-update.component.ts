import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IActionInstance, ActionInstance } from 'app/shared/model/action-instance.model';
import { ActionInstanceService } from './action-instance.service';
import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from 'app/entities/case-step-instance/case-step-instance.service';

@Component({
  selector: 'jhi-action-instance-update',
  templateUrl: './action-instance-update.component.html',
})
export class ActionInstanceUpdateComponent implements OnInit {
  isSaving = false;
  casestepinstances: ICaseStepInstance[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    next: [],
    type: [],
    caseStepInstance: [],
  });

  constructor(
    protected actionInstanceService: ActionInstanceService,
    protected caseStepInstanceService: CaseStepInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ actionInstance }) => {
      this.updateForm(actionInstance);

      this.caseStepInstanceService.query().subscribe((res: HttpResponse<ICaseStepInstance[]>) => (this.casestepinstances = res.body || []));
    });
  }

  updateForm(actionInstance: IActionInstance): void {
    this.editForm.patchValue({
      id: actionInstance.id,
      name: actionInstance.name,
      next: actionInstance.next,
      type: actionInstance.type,
      caseStepInstance: actionInstance.caseStepInstance,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const actionInstance = this.createFromForm();
    if (actionInstance.id !== undefined) {
      this.subscribeToSaveResponse(this.actionInstanceService.update(actionInstance));
    } else {
      this.subscribeToSaveResponse(this.actionInstanceService.create(actionInstance));
    }
  }

  private createFromForm(): IActionInstance {
    return {
      ...new ActionInstance(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      next: this.editForm.get(['next'])!.value,
      type: this.editForm.get(['type'])!.value,
      caseStepInstance: this.editForm.get(['caseStepInstance'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IActionInstance>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICaseStepInstance): any {
    return item.id;
  }
}
