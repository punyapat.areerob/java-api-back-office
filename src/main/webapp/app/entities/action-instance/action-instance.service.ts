import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IActionInstance } from 'app/shared/model/action-instance.model';

type EntityResponseType = HttpResponse<IActionInstance>;
type EntityArrayResponseType = HttpResponse<IActionInstance[]>;

@Injectable({ providedIn: 'root' })
export class ActionInstanceService {
  public resourceUrl = SERVER_API_URL + 'api/action-instances';

  constructor(protected http: HttpClient) {}

  create(actionInstance: IActionInstance): Observable<EntityResponseType> {
    return this.http.post<IActionInstance>(this.resourceUrl, actionInstance, { observe: 'response' });
  }

  update(actionInstance: IActionInstance): Observable<EntityResponseType> {
    return this.http.put<IActionInstance>(this.resourceUrl, actionInstance, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IActionInstance>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IActionInstance[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
