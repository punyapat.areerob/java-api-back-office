import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IActionInstance } from 'app/shared/model/action-instance.model';

@Component({
  selector: 'jhi-action-instance-detail',
  templateUrl: './action-instance-detail.component.html',
})
export class ActionInstanceDetailComponent implements OnInit {
  actionInstance: IActionInstance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ actionInstance }) => (this.actionInstance = actionInstance));
  }

  previousState(): void {
    window.history.back();
  }
}
