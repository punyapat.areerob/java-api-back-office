import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IActionInstance } from 'app/shared/model/action-instance.model';
import { ActionInstanceService } from './action-instance.service';
import { ActionInstanceDeleteDialogComponent } from './action-instance-delete-dialog.component';

@Component({
  selector: 'jhi-action-instance',
  templateUrl: './action-instance.component.html',
})
export class ActionInstanceComponent implements OnInit, OnDestroy {
  actionInstances?: IActionInstance[];
  eventSubscriber?: Subscription;

  constructor(
    protected actionInstanceService: ActionInstanceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.actionInstanceService.query().subscribe((res: HttpResponse<IActionInstance[]>) => (this.actionInstances = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInActionInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IActionInstance): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInActionInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('actionInstanceListModification', () => this.loadAll());
  }

  delete(actionInstance: IActionInstance): void {
    const modalRef = this.modalService.open(ActionInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.actionInstance = actionInstance;
  }
}
