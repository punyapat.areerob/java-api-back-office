import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAgentCommissionRate, AgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';
import { AgentCommissionRateService } from './agent-commission-rate.service';
import { AgentCommissionRateComponent } from './agent-commission-rate.component';
import { AgentCommissionRateDetailComponent } from './agent-commission-rate-detail.component';
import { AgentCommissionRateUpdateComponent } from './agent-commission-rate-update.component';

@Injectable({ providedIn: 'root' })
export class AgentCommissionRateResolve implements Resolve<IAgentCommissionRate> {
  constructor(private service: AgentCommissionRateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAgentCommissionRate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((agentCommissionRate: HttpResponse<AgentCommissionRate>) => {
          if (agentCommissionRate.body) {
            return of(agentCommissionRate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AgentCommissionRate());
  }
}

export const agentCommissionRateRoute: Routes = [
  {
    path: '',
    component: AgentCommissionRateComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AgentCommissionRateDetailComponent,
    resolve: {
      agentCommissionRate: AgentCommissionRateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AgentCommissionRateUpdateComponent,
    resolve: {
      agentCommissionRate: AgentCommissionRateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AgentCommissionRateUpdateComponent,
    resolve: {
      agentCommissionRate: AgentCommissionRateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRates',
    },
    canActivate: [UserRouteAccessService],
  },
];
