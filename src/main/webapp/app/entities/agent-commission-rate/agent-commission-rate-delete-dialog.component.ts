import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';
import { AgentCommissionRateService } from './agent-commission-rate.service';

@Component({
  templateUrl: './agent-commission-rate-delete-dialog.component.html',
})
export class AgentCommissionRateDeleteDialogComponent {
  agentCommissionRate?: IAgentCommissionRate;

  constructor(
    protected agentCommissionRateService: AgentCommissionRateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.agentCommissionRateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('agentCommissionRateListModification');
      this.activeModal.close();
    });
  }
}
