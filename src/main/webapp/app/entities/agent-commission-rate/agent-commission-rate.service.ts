import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';

type EntityResponseType = HttpResponse<IAgentCommissionRate>;
type EntityArrayResponseType = HttpResponse<IAgentCommissionRate[]>;

@Injectable({ providedIn: 'root' })
export class AgentCommissionRateService {
  public resourceUrl = SERVER_API_URL + 'api/agent-commission-rates';

  constructor(protected http: HttpClient) {}

  create(agentCommissionRate: IAgentCommissionRate): Observable<EntityResponseType> {
    return this.http.post<IAgentCommissionRate>(this.resourceUrl, agentCommissionRate, { observe: 'response' });
  }

  update(agentCommissionRate: IAgentCommissionRate): Observable<EntityResponseType> {
    return this.http.put<IAgentCommissionRate>(this.resourceUrl, agentCommissionRate, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IAgentCommissionRate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentCommissionRate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
