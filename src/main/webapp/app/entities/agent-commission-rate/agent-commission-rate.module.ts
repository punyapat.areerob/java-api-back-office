import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { AgentCommissionRateComponent } from './agent-commission-rate.component';
import { AgentCommissionRateDetailComponent } from './agent-commission-rate-detail.component';
import { AgentCommissionRateUpdateComponent } from './agent-commission-rate-update.component';
import { AgentCommissionRateDeleteDialogComponent } from './agent-commission-rate-delete-dialog.component';
import { agentCommissionRateRoute } from './agent-commission-rate.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(agentCommissionRateRoute)],
  declarations: [
    AgentCommissionRateComponent,
    AgentCommissionRateDetailComponent,
    AgentCommissionRateUpdateComponent,
    AgentCommissionRateDeleteDialogComponent,
  ],
  entryComponents: [AgentCommissionRateDeleteDialogComponent],
})
export class MisbeAgentCommissionRateModule {}
