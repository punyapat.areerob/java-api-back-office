import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAgentCommissionRate, AgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';
import { AgentCommissionRateService } from './agent-commission-rate.service';

@Component({
  selector: 'jhi-agent-commission-rate-update',
  templateUrl: './agent-commission-rate-update.component.html',
})
export class AgentCommissionRateUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    constraint: [],
    commissionRate: [],
    type: [],
    comment: [],
  });

  constructor(
    protected agentCommissionRateService: AgentCommissionRateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentCommissionRate }) => {
      this.updateForm(agentCommissionRate);
    });
  }

  updateForm(agentCommissionRate: IAgentCommissionRate): void {
    this.editForm.patchValue({
      id: agentCommissionRate.id,
      constraint: agentCommissionRate.constraint,
      commissionRate: agentCommissionRate.commissionRate,
      type: agentCommissionRate.type,
      comment: agentCommissionRate.comment,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agentCommissionRate = this.createFromForm();
    if (agentCommissionRate.id !== undefined) {
      this.subscribeToSaveResponse(this.agentCommissionRateService.update(agentCommissionRate));
    } else {
      this.subscribeToSaveResponse(this.agentCommissionRateService.create(agentCommissionRate));
    }
  }

  private createFromForm(): IAgentCommissionRate {
    return {
      ...new AgentCommissionRate(),
      id: this.editForm.get(['id'])!.value,
      constraint: this.editForm.get(['constraint'])!.value,
      commissionRate: this.editForm.get(['commissionRate'])!.value,
      type: this.editForm.get(['type'])!.value,
      comment: this.editForm.get(['comment'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentCommissionRate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
