import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';
import { AgentCommissionRateService } from './agent-commission-rate.service';
import { AgentCommissionRateDeleteDialogComponent } from './agent-commission-rate-delete-dialog.component';

@Component({
  selector: 'jhi-agent-commission-rate',
  templateUrl: './agent-commission-rate.component.html',
})
export class AgentCommissionRateComponent implements OnInit, OnDestroy {
  agentCommissionRates?: IAgentCommissionRate[];
  eventSubscriber?: Subscription;

  constructor(
    protected agentCommissionRateService: AgentCommissionRateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.agentCommissionRateService
      .query()
      .subscribe((res: HttpResponse<IAgentCommissionRate[]>) => (this.agentCommissionRates = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAgentCommissionRates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAgentCommissionRate): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAgentCommissionRates(): void {
    this.eventSubscriber = this.eventManager.subscribe('agentCommissionRateListModification', () => this.loadAll());
  }

  delete(agentCommissionRate: IAgentCommissionRate): void {
    const modalRef = this.modalService.open(AgentCommissionRateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.agentCommissionRate = agentCommissionRate;
  }
}
