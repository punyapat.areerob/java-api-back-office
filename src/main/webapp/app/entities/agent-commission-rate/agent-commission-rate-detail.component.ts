import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';

@Component({
  selector: 'jhi-agent-commission-rate-detail',
  templateUrl: './agent-commission-rate-detail.component.html',
})
export class AgentCommissionRateDetailComponent implements OnInit {
  agentCommissionRate: IAgentCommissionRate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentCommissionRate }) => (this.agentCommissionRate = agentCommissionRate));
  }

  previousState(): void {
    window.history.back();
  }
}
