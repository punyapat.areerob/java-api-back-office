import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductVariantGroup } from 'app/shared/model/product-variant-group.model';
import { ProductVariantGroupService } from './product-variant-group.service';
import { ProductVariantGroupDeleteDialogComponent } from './product-variant-group-delete-dialog.component';

@Component({
  selector: 'jhi-product-variant-group',
  templateUrl: './product-variant-group.component.html',
})
export class ProductVariantGroupComponent implements OnInit, OnDestroy {
  productVariantGroups?: IProductVariantGroup[];
  eventSubscriber?: Subscription;

  constructor(
    protected productVariantGroupService: ProductVariantGroupService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.productVariantGroupService
      .query()
      .subscribe((res: HttpResponse<IProductVariantGroup[]>) => (this.productVariantGroups = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductVariantGroups();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductVariantGroup): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductVariantGroups(): void {
    this.eventSubscriber = this.eventManager.subscribe('productVariantGroupListModification', () => this.loadAll());
  }

  delete(productVariantGroup: IProductVariantGroup): void {
    const modalRef = this.modalService.open(ProductVariantGroupDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productVariantGroup = productVariantGroup;
  }
}
