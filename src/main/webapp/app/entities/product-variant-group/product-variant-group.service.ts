import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductVariantGroup } from 'app/shared/model/product-variant-group.model';

type EntityResponseType = HttpResponse<IProductVariantGroup>;
type EntityArrayResponseType = HttpResponse<IProductVariantGroup[]>;

@Injectable({ providedIn: 'root' })
export class ProductVariantGroupService {
  public resourceUrl = SERVER_API_URL + 'api/product-variant-groups';

  constructor(protected http: HttpClient) {}

  create(productVariantGroup: IProductVariantGroup): Observable<EntityResponseType> {
    return this.http.post<IProductVariantGroup>(this.resourceUrl, productVariantGroup, { observe: 'response' });
  }

  update(productVariantGroup: IProductVariantGroup): Observable<EntityResponseType> {
    return this.http.put<IProductVariantGroup>(this.resourceUrl, productVariantGroup, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IProductVariantGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductVariantGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
