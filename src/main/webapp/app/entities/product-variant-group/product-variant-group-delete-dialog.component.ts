import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductVariantGroup } from 'app/shared/model/product-variant-group.model';
import { ProductVariantGroupService } from './product-variant-group.service';

@Component({
  templateUrl: './product-variant-group-delete-dialog.component.html',
})
export class ProductVariantGroupDeleteDialogComponent {
  productVariantGroup?: IProductVariantGroup;

  constructor(
    protected productVariantGroupService: ProductVariantGroupService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.productVariantGroupService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productVariantGroupListModification');
      this.activeModal.close();
    });
  }
}
