import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductVariantGroup, ProductVariantGroup } from 'app/shared/model/product-variant-group.model';
import { ProductVariantGroupService } from './product-variant-group.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';

@Component({
  selector: 'jhi-product-variant-group-update',
  templateUrl: './product-variant-group-update.component.html',
})
export class ProductVariantGroupUpdateComponent implements OnInit {
  isSaving = false;
  products: IProduct[] = [];

  editForm = this.fb.group({
    id: [],
    shopId: [],
    name: [],
    type: [],
    images: [],
    active: [],
    product: [],
  });

  constructor(
    protected productVariantGroupService: ProductVariantGroupService,
    protected productService: ProductService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productVariantGroup }) => {
      this.updateForm(productVariantGroup);

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));
    });
  }

  updateForm(productVariantGroup: IProductVariantGroup): void {
    this.editForm.patchValue({
      id: productVariantGroup.id,
      shopId: productVariantGroup.shopId,
      name: productVariantGroup.name,
      type: productVariantGroup.type,
      images: productVariantGroup.images,
      active: productVariantGroup.active,
      product: productVariantGroup.product,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productVariantGroup = this.createFromForm();
    if (productVariantGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.productVariantGroupService.update(productVariantGroup));
    } else {
      this.subscribeToSaveResponse(this.productVariantGroupService.create(productVariantGroup));
    }
  }

  private createFromForm(): IProductVariantGroup {
    return {
      ...new ProductVariantGroup(),
      id: this.editForm.get(['id'])!.value,
      shopId: this.editForm.get(['shopId'])!.value,
      name: this.editForm.get(['name'])!.value,
      type: this.editForm.get(['type'])!.value,
      images: this.editForm.get(['images'])!.value,
      active: this.editForm.get(['active'])!.value,
      product: this.editForm.get(['product'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductVariantGroup>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProduct): any {
    return item.id;
  }
}
