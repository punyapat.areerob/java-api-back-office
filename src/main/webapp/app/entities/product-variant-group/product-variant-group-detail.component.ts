import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductVariantGroup } from 'app/shared/model/product-variant-group.model';

@Component({
  selector: 'jhi-product-variant-group-detail',
  templateUrl: './product-variant-group-detail.component.html',
})
export class ProductVariantGroupDetailComponent implements OnInit {
  productVariantGroup: IProductVariantGroup | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productVariantGroup }) => (this.productVariantGroup = productVariantGroup));
  }

  previousState(): void {
    window.history.back();
  }
}
