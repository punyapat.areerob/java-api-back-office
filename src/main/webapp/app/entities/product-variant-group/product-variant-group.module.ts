import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ProductVariantGroupComponent } from './product-variant-group.component';
import { ProductVariantGroupDetailComponent } from './product-variant-group-detail.component';
import { ProductVariantGroupUpdateComponent } from './product-variant-group-update.component';
import { ProductVariantGroupDeleteDialogComponent } from './product-variant-group-delete-dialog.component';
import { productVariantGroupRoute } from './product-variant-group.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(productVariantGroupRoute)],
  declarations: [
    ProductVariantGroupComponent,
    ProductVariantGroupDetailComponent,
    ProductVariantGroupUpdateComponent,
    ProductVariantGroupDeleteDialogComponent,
  ],
  entryComponents: [ProductVariantGroupDeleteDialogComponent],
})
export class MisbeProductVariantGroupModule {}
