import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVendorContract } from 'app/shared/model/vendor-contract.model';
import { VendorContractService } from './vendor-contract.service';

@Component({
  templateUrl: './vendor-contract-delete-dialog.component.html',
})
export class VendorContractDeleteDialogComponent {
  vendorContract?: IVendorContract;

  constructor(
    protected vendorContractService: VendorContractService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.vendorContractService.delete(id).subscribe(() => {
      this.eventManager.broadcast('vendorContractListModification');
      this.activeModal.close();
    });
  }
}
