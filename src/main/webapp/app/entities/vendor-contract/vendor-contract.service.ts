import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVendorContract } from 'app/shared/model/vendor-contract.model';

type EntityResponseType = HttpResponse<IVendorContract>;
type EntityArrayResponseType = HttpResponse<IVendorContract[]>;

@Injectable({ providedIn: 'root' })
export class VendorContractService {
  public resourceUrl = SERVER_API_URL + 'api/vendor-contracts';

  constructor(protected http: HttpClient) {}

  create(vendorContract: IVendorContract): Observable<EntityResponseType> {
    return this.http.post<IVendorContract>(this.resourceUrl, vendorContract, { observe: 'response' });
  }

  update(vendorContract: IVendorContract): Observable<EntityResponseType> {
    return this.http.put<IVendorContract>(this.resourceUrl, vendorContract, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IVendorContract>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVendorContract[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
