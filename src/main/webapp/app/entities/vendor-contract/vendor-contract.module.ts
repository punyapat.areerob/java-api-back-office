import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { VendorContractComponent } from './vendor-contract.component';
import { VendorContractDetailComponent } from './vendor-contract-detail.component';
import { VendorContractUpdateComponent } from './vendor-contract-update.component';
import { VendorContractDeleteDialogComponent } from './vendor-contract-delete-dialog.component';
import { vendorContractRoute } from './vendor-contract.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(vendorContractRoute)],
  declarations: [
    VendorContractComponent,
    VendorContractDetailComponent,
    VendorContractUpdateComponent,
    VendorContractDeleteDialogComponent,
  ],
  entryComponents: [VendorContractDeleteDialogComponent],
})
export class MisbeVendorContractModule {}
