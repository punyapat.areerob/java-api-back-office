import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVendorContract, VendorContract } from 'app/shared/model/vendor-contract.model';
import { VendorContractService } from './vendor-contract.service';

@Component({
  selector: 'jhi-vendor-contract-update',
  templateUrl: './vendor-contract-update.component.html',
})
export class VendorContractUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    vendorId: [],
    vendorContractStartDate: [],
    vendorContractEndDate: [],
    vendorContractFile: [],
    remark: [],
    status: [],
  });

  constructor(protected vendorContractService: VendorContractService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vendorContract }) => {
      this.updateForm(vendorContract);
    });
  }

  updateForm(vendorContract: IVendorContract): void {
    this.editForm.patchValue({
      id: vendorContract.id,
      name: vendorContract.name,
      vendorId: vendorContract.vendorId,
      vendorContractStartDate: vendorContract.vendorContractStartDate,
      vendorContractEndDate: vendorContract.vendorContractEndDate,
      vendorContractFile: vendorContract.vendorContractFile,
      remark: vendorContract.remark,
      status: vendorContract.status,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vendorContract = this.createFromForm();
    if (vendorContract.id !== undefined) {
      this.subscribeToSaveResponse(this.vendorContractService.update(vendorContract));
    } else {
      this.subscribeToSaveResponse(this.vendorContractService.create(vendorContract));
    }
  }

  private createFromForm(): IVendorContract {
    return {
      ...new VendorContract(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      vendorId: this.editForm.get(['vendorId'])!.value,
      vendorContractStartDate: this.editForm.get(['vendorContractStartDate'])!.value,
      vendorContractEndDate: this.editForm.get(['vendorContractEndDate'])!.value,
      vendorContractFile: this.editForm.get(['vendorContractFile'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVendorContract>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
