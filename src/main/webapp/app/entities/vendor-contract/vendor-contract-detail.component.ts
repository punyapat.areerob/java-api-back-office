import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVendorContract } from 'app/shared/model/vendor-contract.model';

@Component({
  selector: 'jhi-vendor-contract-detail',
  templateUrl: './vendor-contract-detail.component.html',
})
export class VendorContractDetailComponent implements OnInit {
  vendorContract: IVendorContract | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vendorContract }) => (this.vendorContract = vendorContract));
  }

  previousState(): void {
    window.history.back();
  }
}
