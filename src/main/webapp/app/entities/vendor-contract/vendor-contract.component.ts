import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVendorContract } from 'app/shared/model/vendor-contract.model';
import { VendorContractService } from './vendor-contract.service';
import { VendorContractDeleteDialogComponent } from './vendor-contract-delete-dialog.component';

@Component({
  selector: 'jhi-vendor-contract',
  templateUrl: './vendor-contract.component.html',
})
export class VendorContractComponent implements OnInit, OnDestroy {
  vendorContracts?: IVendorContract[];
  eventSubscriber?: Subscription;

  constructor(
    protected vendorContractService: VendorContractService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.vendorContractService.query().subscribe((res: HttpResponse<IVendorContract[]>) => (this.vendorContracts = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInVendorContracts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IVendorContract): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInVendorContracts(): void {
    this.eventSubscriber = this.eventManager.subscribe('vendorContractListModification', () => this.loadAll());
  }

  delete(vendorContract: IVendorContract): void {
    const modalRef = this.modalService.open(VendorContractDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.vendorContract = vendorContract;
  }
}
