import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVendorContract, VendorContract } from 'app/shared/model/vendor-contract.model';
import { VendorContractService } from './vendor-contract.service';
import { VendorContractComponent } from './vendor-contract.component';
import { VendorContractDetailComponent } from './vendor-contract-detail.component';
import { VendorContractUpdateComponent } from './vendor-contract-update.component';

@Injectable({ providedIn: 'root' })
export class VendorContractResolve implements Resolve<IVendorContract> {
  constructor(private service: VendorContractService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVendorContract> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((vendorContract: HttpResponse<VendorContract>) => {
          if (vendorContract.body) {
            return of(vendorContract.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VendorContract());
  }
}

export const vendorContractRoute: Routes = [
  {
    path: '',
    component: VendorContractComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorContracts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VendorContractDetailComponent,
    resolve: {
      vendorContract: VendorContractResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorContracts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VendorContractUpdateComponent,
    resolve: {
      vendorContract: VendorContractResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorContracts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VendorContractUpdateComponent,
    resolve: {
      vendorContract: VendorContractResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorContracts',
    },
    canActivate: [UserRouteAccessService],
  },
];
