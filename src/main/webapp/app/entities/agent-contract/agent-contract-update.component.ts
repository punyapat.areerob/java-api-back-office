import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAgentContract, AgentContract } from 'app/shared/model/agent-contract.model';
import { AgentContractService } from './agent-contract.service';

@Component({
  selector: 'jhi-agent-contract-update',
  templateUrl: './agent-contract-update.component.html',
})
export class AgentContractUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentId: [],
    commissionId: [],
    start: [],
    end: [],
  });

  constructor(protected agentContractService: AgentContractService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentContract }) => {
      if (!agentContract.id) {
        const today = moment().startOf('day');
        agentContract.start = today;
        agentContract.end = today;
      }

      this.updateForm(agentContract);
    });
  }

  updateForm(agentContract: IAgentContract): void {
    this.editForm.patchValue({
      id: agentContract.id,
      agentId: agentContract.agentId,
      commissionId: agentContract.commissionId,
      start: agentContract.start ? agentContract.start.format(DATE_TIME_FORMAT) : null,
      end: agentContract.end ? agentContract.end.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agentContract = this.createFromForm();
    if (agentContract.id !== undefined) {
      this.subscribeToSaveResponse(this.agentContractService.update(agentContract));
    } else {
      this.subscribeToSaveResponse(this.agentContractService.create(agentContract));
    }
  }

  private createFromForm(): IAgentContract {
    return {
      ...new AgentContract(),
      id: this.editForm.get(['id'])!.value,
      agentId: this.editForm.get(['agentId'])!.value,
      commissionId: this.editForm.get(['commissionId'])!.value,
      start: this.editForm.get(['start'])!.value ? moment(this.editForm.get(['start'])!.value, DATE_TIME_FORMAT) : undefined,
      end: this.editForm.get(['end'])!.value ? moment(this.editForm.get(['end'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentContract>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
