import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentContract } from 'app/shared/model/agent-contract.model';

@Component({
  selector: 'jhi-agent-contract-detail',
  templateUrl: './agent-contract-detail.component.html',
})
export class AgentContractDetailComponent implements OnInit {
  agentContract: IAgentContract | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentContract }) => (this.agentContract = agentContract));
  }

  previousState(): void {
    window.history.back();
  }
}
