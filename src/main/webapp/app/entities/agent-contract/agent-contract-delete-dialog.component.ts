import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentContract } from 'app/shared/model/agent-contract.model';
import { AgentContractService } from './agent-contract.service';

@Component({
  templateUrl: './agent-contract-delete-dialog.component.html',
})
export class AgentContractDeleteDialogComponent {
  agentContract?: IAgentContract;

  constructor(
    protected agentContractService: AgentContractService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.agentContractService.delete(id).subscribe(() => {
      this.eventManager.broadcast('agentContractListModification');
      this.activeModal.close();
    });
  }
}
