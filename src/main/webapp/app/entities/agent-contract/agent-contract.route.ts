import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAgentContract, AgentContract } from 'app/shared/model/agent-contract.model';
import { AgentContractService } from './agent-contract.service';
import { AgentContractComponent } from './agent-contract.component';
import { AgentContractDetailComponent } from './agent-contract-detail.component';
import { AgentContractUpdateComponent } from './agent-contract-update.component';

@Injectable({ providedIn: 'root' })
export class AgentContractResolve implements Resolve<IAgentContract> {
  constructor(private service: AgentContractService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAgentContract> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((agentContract: HttpResponse<AgentContract>) => {
          if (agentContract.body) {
            return of(agentContract.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AgentContract());
  }
}

export const agentContractRoute: Routes = [
  {
    path: '',
    component: AgentContractComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentContracts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AgentContractDetailComponent,
    resolve: {
      agentContract: AgentContractResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentContracts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AgentContractUpdateComponent,
    resolve: {
      agentContract: AgentContractResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentContracts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AgentContractUpdateComponent,
    resolve: {
      agentContract: AgentContractResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentContracts',
    },
    canActivate: [UserRouteAccessService],
  },
];
