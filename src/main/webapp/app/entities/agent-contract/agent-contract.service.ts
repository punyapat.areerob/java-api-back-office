import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentContract } from 'app/shared/model/agent-contract.model';

type EntityResponseType = HttpResponse<IAgentContract>;
type EntityArrayResponseType = HttpResponse<IAgentContract[]>;

@Injectable({ providedIn: 'root' })
export class AgentContractService {
  public resourceUrl = SERVER_API_URL + 'api/agent-contracts';

  constructor(protected http: HttpClient) {}

  create(agentContract: IAgentContract): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agentContract);
    return this.http
      .post<IAgentContract>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(agentContract: IAgentContract): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agentContract);
    return this.http
      .put<IAgentContract>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IAgentContract>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAgentContract[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(agentContract: IAgentContract): IAgentContract {
    const copy: IAgentContract = Object.assign({}, agentContract, {
      start: agentContract.start && agentContract.start.isValid() ? agentContract.start.toJSON() : undefined,
      end: agentContract.end && agentContract.end.isValid() ? agentContract.end.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.start = res.body.start ? moment(res.body.start) : undefined;
      res.body.end = res.body.end ? moment(res.body.end) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((agentContract: IAgentContract) => {
        agentContract.start = agentContract.start ? moment(agentContract.start) : undefined;
        agentContract.end = agentContract.end ? moment(agentContract.end) : undefined;
      });
    }
    return res;
  }
}
