import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAgentContract } from 'app/shared/model/agent-contract.model';
import { AgentContractService } from './agent-contract.service';
import { AgentContractDeleteDialogComponent } from './agent-contract-delete-dialog.component';

@Component({
  selector: 'jhi-agent-contract',
  templateUrl: './agent-contract.component.html',
})
export class AgentContractComponent implements OnInit, OnDestroy {
  agentContracts?: IAgentContract[];
  eventSubscriber?: Subscription;

  constructor(
    protected agentContractService: AgentContractService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.agentContractService.query().subscribe((res: HttpResponse<IAgentContract[]>) => (this.agentContracts = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAgentContracts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAgentContract): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAgentContracts(): void {
    this.eventSubscriber = this.eventManager.subscribe('agentContractListModification', () => this.loadAll());
  }

  delete(agentContract: IAgentContract): void {
    const modalRef = this.modalService.open(AgentContractDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.agentContract = agentContract;
  }
}
