import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { AgentContractComponent } from './agent-contract.component';
import { AgentContractDetailComponent } from './agent-contract-detail.component';
import { AgentContractUpdateComponent } from './agent-contract-update.component';
import { AgentContractDeleteDialogComponent } from './agent-contract-delete-dialog.component';
import { agentContractRoute } from './agent-contract.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(agentContractRoute)],
  declarations: [AgentContractComponent, AgentContractDetailComponent, AgentContractUpdateComponent, AgentContractDeleteDialogComponent],
  entryComponents: [AgentContractDeleteDialogComponent],
})
export class MisbeAgentContractModule {}
