import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IShop, Shop } from 'app/shared/model/shop.model';
import { ShopService } from './shop.service';
import { IShopBillInfo } from 'app/shared/model/shop-bill-info.model';
import { ShopBillInfoService } from 'app/entities/shop-bill-info/shop-bill-info.service';

@Component({
  selector: 'jhi-shop-update',
  templateUrl: './shop-update.component.html',
})
export class ShopUpdateComponent implements OnInit {
  isSaving = false;
  billinfos: IShopBillInfo[] = [];
  openTimeDp: any;
  closeTimeDp: any;

  editForm = this.fb.group({
    id: [],
    name: [],
    vendorId: [],
    email: [],
    openTime: [],
    closeTime: [],
    mobile: [],
    address: [],
    subDistrict: [],
    district: [],
    province: [],
    country: [],
    postCode: [],
    latitude: [],
    longitude: [],
    active: [],
    billInfo: [],
  });

  constructor(
    protected shopService: ShopService,
    protected shopBillInfoService: ShopBillInfoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shop }) => {
      this.updateForm(shop);

      this.shopBillInfoService
        .query({ filter: 'shop-is-null' })
        .pipe(
          map((res: HttpResponse<IShopBillInfo[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IShopBillInfo[]) => {
          if (!shop.billInfo || !shop.billInfo.id) {
            this.billinfos = resBody;
          } else {
            this.shopBillInfoService
              .find(shop.billInfo.id)
              .pipe(
                map((subRes: HttpResponse<IShopBillInfo>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IShopBillInfo[]) => (this.billinfos = concatRes));
          }
        });
    });
  }

  updateForm(shop: IShop): void {
    this.editForm.patchValue({
      id: shop.id,
      name: shop.name,
      vendorId: shop.vendorId,
      email: shop.email,
      openTime: shop.openTime,
      closeTime: shop.closeTime,
      mobile: shop.mobile,
      address: shop.address,
      subDistrict: shop.subDistrict,
      district: shop.district,
      province: shop.province,
      country: shop.country,
      postCode: shop.postCode,
      latitude: shop.latitude,
      longitude: shop.longitude,
      active: shop.active,
      billInfo: shop.billInfo,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shop = this.createFromForm();
    if (shop.id !== undefined) {
      this.subscribeToSaveResponse(this.shopService.update(shop));
    } else {
      this.subscribeToSaveResponse(this.shopService.create(shop));
    }
  }

  private createFromForm(): IShop {
    return {
      ...new Shop(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      vendorId: this.editForm.get(['vendorId'])!.value,
      email: this.editForm.get(['email'])!.value,
      openTime: this.editForm.get(['openTime'])!.value,
      closeTime: this.editForm.get(['closeTime'])!.value,
      mobile: this.editForm.get(['mobile'])!.value,
      address: this.editForm.get(['address'])!.value,
      subDistrict: this.editForm.get(['subDistrict'])!.value,
      district: this.editForm.get(['district'])!.value,
      province: this.editForm.get(['province'])!.value,
      country: this.editForm.get(['country'])!.value,
      postCode: this.editForm.get(['postCode'])!.value,
      latitude: this.editForm.get(['latitude'])!.value,
      longitude: this.editForm.get(['longitude'])!.value,
      active: this.editForm.get(['active'])!.value,
      billInfo: this.editForm.get(['billInfo'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShop>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IShopBillInfo): any {
    return item.id;
  }
}
