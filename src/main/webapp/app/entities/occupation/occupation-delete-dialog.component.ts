import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOccupation } from 'app/shared/model/occupation.model';
import { OccupationService } from './occupation.service';

@Component({
  templateUrl: './occupation-delete-dialog.component.html',
})
export class OccupationDeleteDialogComponent {
  occupation?: IOccupation;

  constructor(
    protected occupationService: OccupationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.occupationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('occupationListModification');
      this.activeModal.close();
    });
  }
}
