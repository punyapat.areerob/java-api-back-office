import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOccupation, Occupation } from 'app/shared/model/occupation.model';
import { OccupationService } from './occupation.service';
import { OccupationComponent } from './occupation.component';
import { OccupationDetailComponent } from './occupation-detail.component';
import { OccupationUpdateComponent } from './occupation-update.component';

@Injectable({ providedIn: 'root' })
export class OccupationResolve implements Resolve<IOccupation> {
  constructor(private service: OccupationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOccupation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((occupation: HttpResponse<Occupation>) => {
          if (occupation.body) {
            return of(occupation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Occupation());
  }
}

export const occupationRoute: Routes = [
  {
    path: '',
    component: OccupationComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Occupations',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OccupationDetailComponent,
    resolve: {
      occupation: OccupationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Occupations',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OccupationUpdateComponent,
    resolve: {
      occupation: OccupationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Occupations',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OccupationUpdateComponent,
    resolve: {
      occupation: OccupationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Occupations',
    },
    canActivate: [UserRouteAccessService],
  },
];
