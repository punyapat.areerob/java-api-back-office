import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { OccupationComponent } from './occupation.component';
import { OccupationDetailComponent } from './occupation-detail.component';
import { OccupationUpdateComponent } from './occupation-update.component';
import { OccupationDeleteDialogComponent } from './occupation-delete-dialog.component';
import { occupationRoute } from './occupation.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(occupationRoute)],
  declarations: [OccupationComponent, OccupationDetailComponent, OccupationUpdateComponent, OccupationDeleteDialogComponent],
  entryComponents: [OccupationDeleteDialogComponent],
})
export class MisbeOccupationModule {}
