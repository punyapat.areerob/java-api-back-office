import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOccupation } from 'app/shared/model/occupation.model';
import { OccupationService } from './occupation.service';
import { OccupationDeleteDialogComponent } from './occupation-delete-dialog.component';

@Component({
  selector: 'jhi-occupation',
  templateUrl: './occupation.component.html',
})
export class OccupationComponent implements OnInit, OnDestroy {
  occupations?: IOccupation[];
  eventSubscriber?: Subscription;

  constructor(protected occupationService: OccupationService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.occupationService.query().subscribe((res: HttpResponse<IOccupation[]>) => (this.occupations = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOccupations();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOccupation): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOccupations(): void {
    this.eventSubscriber = this.eventManager.subscribe('occupationListModification', () => this.loadAll());
  }

  delete(occupation: IOccupation): void {
    const modalRef = this.modalService.open(OccupationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.occupation = occupation;
  }
}
