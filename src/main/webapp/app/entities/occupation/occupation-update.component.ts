import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOccupation, Occupation } from 'app/shared/model/occupation.model';
import { OccupationService } from './occupation.service';

@Component({
  selector: 'jhi-occupation-update',
  templateUrl: './occupation-update.component.html',
})
export class OccupationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(protected occupationService: OccupationService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ occupation }) => {
      this.updateForm(occupation);
    });
  }

  updateForm(occupation: IOccupation): void {
    this.editForm.patchValue({
      id: occupation.id,
      name: occupation.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const occupation = this.createFromForm();
    if (occupation.id !== undefined) {
      this.subscribeToSaveResponse(this.occupationService.update(occupation));
    } else {
      this.subscribeToSaveResponse(this.occupationService.create(occupation));
    }
  }

  private createFromForm(): IOccupation {
    return {
      ...new Occupation(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOccupation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
