import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOccupation } from 'app/shared/model/occupation.model';

@Component({
  selector: 'jhi-occupation-detail',
  templateUrl: './occupation-detail.component.html',
})
export class OccupationDetailComponent implements OnInit {
  occupation: IOccupation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ occupation }) => (this.occupation = occupation));
  }

  previousState(): void {
    window.history.back();
  }
}
