import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOccupation } from 'app/shared/model/occupation.model';

type EntityResponseType = HttpResponse<IOccupation>;
type EntityArrayResponseType = HttpResponse<IOccupation[]>;

@Injectable({ providedIn: 'root' })
export class OccupationService {
  public resourceUrl = SERVER_API_URL + 'api/occupations';

  constructor(protected http: HttpClient) {}

  create(occupation: IOccupation): Observable<EntityResponseType> {
    return this.http.post<IOccupation>(this.resourceUrl, occupation, { observe: 'response' });
  }

  update(occupation: IOccupation): Observable<EntityResponseType> {
    return this.http.put<IOccupation>(this.resourceUrl, occupation, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IOccupation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOccupation[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
