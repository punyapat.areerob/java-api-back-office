import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBookingServices, BookingServices } from 'app/shared/model/booking-services.model';
import { BookingServicesService } from './booking-services.service';
import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from 'app/entities/booking-status/booking-status.service';
import { IBooking } from 'app/shared/model/booking.model';
import { BookingService } from 'app/entities/booking/booking.service';

type SelectableEntity = IBookingStatus | IBooking;

@Component({
  selector: 'jhi-booking-services-update',
  templateUrl: './booking-services-update.component.html',
})
export class BookingServicesUpdateComponent implements OnInit {
  isSaving = false;
  bookingstatusids: IBookingStatus[] = [];
  bookings: IBooking[] = [];

  editForm = this.fb.group({
    id: [],
    shopCategoryId: [],
    shopId: [],
    remark: [],
    isActive: [],
    serviceName: [],
    serviceId: [],
    fromLatitude: [],
    fromLongitude: [],
    toLatitude: [],
    toLongitude: [],
    createdBy: [],
    updatedBy: [],
    createdAt: [],
    updatedAt: [],
    bookingStatusId: [],
    booking: [],
  });

  constructor(
    protected bookingServicesService: BookingServicesService,
    protected bookingStatusService: BookingStatusService,
    protected bookingService: BookingService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingServices }) => {
      if (!bookingServices.id) {
        const today = moment().startOf('day');
        bookingServices.createdBy = today;
        bookingServices.updatedBy = today;
        bookingServices.createdAt = today;
        bookingServices.updatedAt = today;
      }

      this.updateForm(bookingServices);

      this.bookingStatusService
        .query({ filter: 'bookingservices-is-null' })
        .pipe(
          map((res: HttpResponse<IBookingStatus[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBookingStatus[]) => {
          if (!bookingServices.bookingStatusId || !bookingServices.bookingStatusId.id) {
            this.bookingstatusids = resBody;
          } else {
            this.bookingStatusService
              .find(bookingServices.bookingStatusId.id)
              .pipe(
                map((subRes: HttpResponse<IBookingStatus>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBookingStatus[]) => (this.bookingstatusids = concatRes));
          }
        });

      this.bookingService.query().subscribe((res: HttpResponse<IBooking[]>) => (this.bookings = res.body || []));
    });
  }

  updateForm(bookingServices: IBookingServices): void {
    this.editForm.patchValue({
      id: bookingServices.id,
      shopCategoryId: bookingServices.shopCategoryId,
      shopId: bookingServices.shopId,
      remark: bookingServices.remark,
      isActive: bookingServices.isActive,
      serviceName: bookingServices.serviceName,
      serviceId: bookingServices.serviceId,
      fromLatitude: bookingServices.fromLatitude,
      fromLongitude: bookingServices.fromLongitude,
      toLatitude: bookingServices.toLatitude,
      toLongitude: bookingServices.toLongitude,
      createdBy: bookingServices.createdBy ? bookingServices.createdBy.format(DATE_TIME_FORMAT) : null,
      updatedBy: bookingServices.updatedBy ? bookingServices.updatedBy.format(DATE_TIME_FORMAT) : null,
      createdAt: bookingServices.createdAt ? bookingServices.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: bookingServices.updatedAt ? bookingServices.updatedAt.format(DATE_TIME_FORMAT) : null,
      bookingStatusId: bookingServices.bookingStatusId,
      booking: bookingServices.booking,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookingServices = this.createFromForm();
    if (bookingServices.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingServicesService.update(bookingServices));
    } else {
      this.subscribeToSaveResponse(this.bookingServicesService.create(bookingServices));
    }
  }

  private createFromForm(): IBookingServices {
    return {
      ...new BookingServices(),
      id: this.editForm.get(['id'])!.value,
      shopCategoryId: this.editForm.get(['shopCategoryId'])!.value,
      shopId: this.editForm.get(['shopId'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      isActive: this.editForm.get(['isActive'])!.value,
      serviceName: this.editForm.get(['serviceName'])!.value,
      serviceId: this.editForm.get(['serviceId'])!.value,
      fromLatitude: this.editForm.get(['fromLatitude'])!.value,
      fromLongitude: this.editForm.get(['fromLongitude'])!.value,
      toLatitude: this.editForm.get(['toLatitude'])!.value,
      toLongitude: this.editForm.get(['toLongitude'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value ? moment(this.editForm.get(['createdBy'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value ? moment(this.editForm.get(['updatedBy'])!.value, DATE_TIME_FORMAT) : undefined,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
      bookingStatusId: this.editForm.get(['bookingStatusId'])!.value,
      booking: this.editForm.get(['booking'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookingServices>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
