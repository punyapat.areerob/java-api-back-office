import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookingServices } from 'app/shared/model/booking-services.model';
import { BookingServicesService } from './booking-services.service';
import { BookingServicesDeleteDialogComponent } from './booking-services-delete-dialog.component';

@Component({
  selector: 'jhi-booking-services',
  templateUrl: './booking-services.component.html',
})
export class BookingServicesComponent implements OnInit, OnDestroy {
  bookingServices?: IBookingServices[];
  eventSubscriber?: Subscription;

  constructor(
    protected bookingServicesService: BookingServicesService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.bookingServicesService.query().subscribe((res: HttpResponse<IBookingServices[]>) => (this.bookingServices = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBookingServices();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBookingServices): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBookingServices(): void {
    this.eventSubscriber = this.eventManager.subscribe('bookingServicesListModification', () => this.loadAll());
  }

  delete(bookingServices: IBookingServices): void {
    const modalRef = this.modalService.open(BookingServicesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bookingServices = bookingServices;
  }
}
