import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBookingServices, BookingServices } from 'app/shared/model/booking-services.model';
import { BookingServicesService } from './booking-services.service';
import { BookingServicesComponent } from './booking-services.component';
import { BookingServicesDetailComponent } from './booking-services-detail.component';
import { BookingServicesUpdateComponent } from './booking-services-update.component';

@Injectable({ providedIn: 'root' })
export class BookingServicesResolve implements Resolve<IBookingServices> {
  constructor(private service: BookingServicesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookingServices> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bookingServices: HttpResponse<BookingServices>) => {
          if (bookingServices.body) {
            return of(bookingServices.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookingServices());
  }
}

export const bookingServicesRoute: Routes = [
  {
    path: '',
    component: BookingServicesComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServices',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookingServicesDetailComponent,
    resolve: {
      bookingServices: BookingServicesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServices',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookingServicesUpdateComponent,
    resolve: {
      bookingServices: BookingServicesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServices',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookingServicesUpdateComponent,
    resolve: {
      bookingServices: BookingServicesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingServices',
    },
    canActivate: [UserRouteAccessService],
  },
];
