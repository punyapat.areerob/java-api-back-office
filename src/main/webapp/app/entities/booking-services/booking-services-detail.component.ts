import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBookingServices } from 'app/shared/model/booking-services.model';

@Component({
  selector: 'jhi-booking-services-detail',
  templateUrl: './booking-services-detail.component.html',
})
export class BookingServicesDetailComponent implements OnInit {
  bookingServices: IBookingServices | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingServices }) => (this.bookingServices = bookingServices));
  }

  previousState(): void {
    window.history.back();
  }
}
