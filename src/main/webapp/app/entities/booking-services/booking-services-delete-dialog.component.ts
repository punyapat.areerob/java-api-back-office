import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBookingServices } from 'app/shared/model/booking-services.model';
import { BookingServicesService } from './booking-services.service';

@Component({
  templateUrl: './booking-services-delete-dialog.component.html',
})
export class BookingServicesDeleteDialogComponent {
  bookingServices?: IBookingServices;

  constructor(
    protected bookingServicesService: BookingServicesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.bookingServicesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bookingServicesListModification');
      this.activeModal.close();
    });
  }
}
