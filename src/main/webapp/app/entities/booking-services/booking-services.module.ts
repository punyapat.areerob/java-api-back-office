import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BookingServicesComponent } from './booking-services.component';
import { BookingServicesDetailComponent } from './booking-services-detail.component';
import { BookingServicesUpdateComponent } from './booking-services-update.component';
import { BookingServicesDeleteDialogComponent } from './booking-services-delete-dialog.component';
import { bookingServicesRoute } from './booking-services.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(bookingServicesRoute)],
  declarations: [
    BookingServicesComponent,
    BookingServicesDetailComponent,
    BookingServicesUpdateComponent,
    BookingServicesDeleteDialogComponent,
  ],
  entryComponents: [BookingServicesDeleteDialogComponent],
})
export class MisbeBookingServicesModule {}
