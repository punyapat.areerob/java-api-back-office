import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBookingServices } from 'app/shared/model/booking-services.model';

type EntityResponseType = HttpResponse<IBookingServices>;
type EntityArrayResponseType = HttpResponse<IBookingServices[]>;

@Injectable({ providedIn: 'root' })
export class BookingServicesService {
  public resourceUrl = SERVER_API_URL + 'api/booking-services';

  constructor(protected http: HttpClient) {}

  create(bookingServices: IBookingServices): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingServices);
    return this.http
      .post<IBookingServices>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bookingServices: IBookingServices): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingServices);
    return this.http
      .put<IBookingServices>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IBookingServices>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBookingServices[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(bookingServices: IBookingServices): IBookingServices {
    const copy: IBookingServices = Object.assign({}, bookingServices, {
      createdBy: bookingServices.createdBy && bookingServices.createdBy.isValid() ? bookingServices.createdBy.toJSON() : undefined,
      updatedBy: bookingServices.updatedBy && bookingServices.updatedBy.isValid() ? bookingServices.updatedBy.toJSON() : undefined,
      createdAt: bookingServices.createdAt && bookingServices.createdAt.isValid() ? bookingServices.createdAt.toJSON() : undefined,
      updatedAt: bookingServices.updatedAt && bookingServices.updatedAt.isValid() ? bookingServices.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdBy = res.body.createdBy ? moment(res.body.createdBy) : undefined;
      res.body.updatedBy = res.body.updatedBy ? moment(res.body.updatedBy) : undefined;
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bookingServices: IBookingServices) => {
        bookingServices.createdBy = bookingServices.createdBy ? moment(bookingServices.createdBy) : undefined;
        bookingServices.updatedBy = bookingServices.updatedBy ? moment(bookingServices.updatedBy) : undefined;
        bookingServices.createdAt = bookingServices.createdAt ? moment(bookingServices.createdAt) : undefined;
        bookingServices.updatedAt = bookingServices.updatedAt ? moment(bookingServices.updatedAt) : undefined;
      });
    }
    return res;
  }
}
