import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaseStep, CaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from './case-step.service';
import { CaseStepComponent } from './case-step.component';
import { CaseStepDetailComponent } from './case-step-detail.component';
import { CaseStepUpdateComponent } from './case-step-update.component';

@Injectable({ providedIn: 'root' })
export class CaseStepResolve implements Resolve<ICaseStep> {
  constructor(private service: CaseStepService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaseStep> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caseStep: HttpResponse<CaseStep>) => {
          if (caseStep.body) {
            return of(caseStep.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaseStep());
  }
}

export const caseStepRoute: Routes = [
  {
    path: '',
    component: CaseStepComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseSteps',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaseStepDetailComponent,
    resolve: {
      caseStep: CaseStepResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseSteps',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaseStepUpdateComponent,
    resolve: {
      caseStep: CaseStepResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseSteps',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaseStepUpdateComponent,
    resolve: {
      caseStep: CaseStepResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseSteps',
    },
    canActivate: [UserRouteAccessService],
  },
];
