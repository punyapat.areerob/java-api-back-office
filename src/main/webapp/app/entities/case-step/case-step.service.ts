import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICaseStep } from 'app/shared/model/case-step.model';

type EntityResponseType = HttpResponse<ICaseStep>;
type EntityArrayResponseType = HttpResponse<ICaseStep[]>;

@Injectable({ providedIn: 'root' })
export class CaseStepService {
  public resourceUrl = SERVER_API_URL + 'api/case-steps';

  constructor(protected http: HttpClient) {}

  create(caseStep: ICaseStep): Observable<EntityResponseType> {
    return this.http.post<ICaseStep>(this.resourceUrl, caseStep, { observe: 'response' });
  }

  update(caseStep: ICaseStep): Observable<EntityResponseType> {
    return this.http.put<ICaseStep>(this.resourceUrl, caseStep, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ICaseStep>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICaseStep[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
