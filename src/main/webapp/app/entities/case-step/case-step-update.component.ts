import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICaseStep, CaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from './case-step.service';
import { ITeam } from 'app/shared/model/team.model';
import { TeamService } from 'app/entities/team/team.service';

@Component({
  selector: 'jhi-case-step-update',
  templateUrl: './case-step-update.component.html',
})
export class CaseStepUpdateComponent implements OnInit {
  isSaving = false;
  teams: ITeam[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    caseId: [],
    begin: [],
    team: [],
  });

  constructor(
    protected caseStepService: CaseStepService,
    protected teamService: TeamService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseStep }) => {
      this.updateForm(caseStep);

      this.teamService
        .query({ filter: 'casestep-is-null' })
        .pipe(
          map((res: HttpResponse<ITeam[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ITeam[]) => {
          if (!caseStep.team || !caseStep.team.id) {
            this.teams = resBody;
          } else {
            this.teamService
              .find(caseStep.team.id)
              .pipe(
                map((subRes: HttpResponse<ITeam>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ITeam[]) => (this.teams = concatRes));
          }
        });
    });
  }

  updateForm(caseStep: ICaseStep): void {
    this.editForm.patchValue({
      id: caseStep.id,
      name: caseStep.name,
      caseId: caseStep.caseId,
      begin: caseStep.begin,
      team: caseStep.team,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseStep = this.createFromForm();
    if (caseStep.id !== undefined) {
      this.subscribeToSaveResponse(this.caseStepService.update(caseStep));
    } else {
      this.subscribeToSaveResponse(this.caseStepService.create(caseStep));
    }
  }

  private createFromForm(): ICaseStep {
    return {
      ...new CaseStep(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      caseId: this.editForm.get(['caseId'])!.value,
      begin: this.editForm.get(['begin'])!.value,
      team: this.editForm.get(['team'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseStep>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITeam): any {
    return item.id;
  }
}
