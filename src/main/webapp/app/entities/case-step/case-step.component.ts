import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from './case-step.service';
import { CaseStepDeleteDialogComponent } from './case-step-delete-dialog.component';

@Component({
  selector: 'jhi-case-step',
  templateUrl: './case-step.component.html',
})
export class CaseStepComponent implements OnInit, OnDestroy {
  caseSteps?: ICaseStep[];
  eventSubscriber?: Subscription;

  constructor(protected caseStepService: CaseStepService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.caseStepService.query().subscribe((res: HttpResponse<ICaseStep[]>) => (this.caseSteps = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseSteps();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseStep): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseSteps(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseStepListModification', () => this.loadAll());
  }

  delete(caseStep: ICaseStep): void {
    const modalRef = this.modalService.open(CaseStepDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseStep = caseStep;
  }
}
