import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseStepComponent } from './case-step.component';
import { CaseStepDetailComponent } from './case-step-detail.component';
import { CaseStepUpdateComponent } from './case-step-update.component';
import { CaseStepDeleteDialogComponent } from './case-step-delete-dialog.component';
import { caseStepRoute } from './case-step.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseStepRoute)],
  declarations: [CaseStepComponent, CaseStepDetailComponent, CaseStepUpdateComponent, CaseStepDeleteDialogComponent],
  entryComponents: [CaseStepDeleteDialogComponent],
})
export class MisbeCaseStepModule {}
