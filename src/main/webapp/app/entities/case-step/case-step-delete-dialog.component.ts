import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from './case-step.service';

@Component({
  templateUrl: './case-step-delete-dialog.component.html',
})
export class CaseStepDeleteDialogComponent {
  caseStep?: ICaseStep;

  constructor(protected caseStepService: CaseStepService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseStepService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseStepListModification');
      this.activeModal.close();
    });
  }
}
