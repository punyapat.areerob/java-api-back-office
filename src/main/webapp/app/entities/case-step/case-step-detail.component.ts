import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseStep } from 'app/shared/model/case-step.model';

@Component({
  selector: 'jhi-case-step-detail',
  templateUrl: './case-step-detail.component.html',
})
export class CaseStepDetailComponent implements OnInit {
  caseStep: ICaseStep | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseStep }) => (this.caseStep = caseStep));
  }

  previousState(): void {
    window.history.back();
  }
}
