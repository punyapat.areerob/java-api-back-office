import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBookingGuest, BookingGuest } from 'app/shared/model/booking-guest.model';
import { BookingGuestService } from './booking-guest.service';
import { BookingGuestComponent } from './booking-guest.component';
import { BookingGuestDetailComponent } from './booking-guest-detail.component';
import { BookingGuestUpdateComponent } from './booking-guest-update.component';

@Injectable({ providedIn: 'root' })
export class BookingGuestResolve implements Resolve<IBookingGuest> {
  constructor(private service: BookingGuestService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookingGuest> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bookingGuest: HttpResponse<BookingGuest>) => {
          if (bookingGuest.body) {
            return of(bookingGuest.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookingGuest());
  }
}

export const bookingGuestRoute: Routes = [
  {
    path: '',
    component: BookingGuestComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingGuests',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookingGuestDetailComponent,
    resolve: {
      bookingGuest: BookingGuestResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingGuests',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookingGuestUpdateComponent,
    resolve: {
      bookingGuest: BookingGuestResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingGuests',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookingGuestUpdateComponent,
    resolve: {
      bookingGuest: BookingGuestResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingGuests',
    },
    canActivate: [UserRouteAccessService],
  },
];
