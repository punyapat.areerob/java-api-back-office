import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBookingGuest } from 'app/shared/model/booking-guest.model';
import { BookingGuestService } from './booking-guest.service';

@Component({
  templateUrl: './booking-guest-delete-dialog.component.html',
})
export class BookingGuestDeleteDialogComponent {
  bookingGuest?: IBookingGuest;

  constructor(
    protected bookingGuestService: BookingGuestService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.bookingGuestService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bookingGuestListModification');
      this.activeModal.close();
    });
  }
}
