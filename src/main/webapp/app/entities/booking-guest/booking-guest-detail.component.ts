import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBookingGuest } from 'app/shared/model/booking-guest.model';

@Component({
  selector: 'jhi-booking-guest-detail',
  templateUrl: './booking-guest-detail.component.html',
})
export class BookingGuestDetailComponent implements OnInit {
  bookingGuest: IBookingGuest | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingGuest }) => (this.bookingGuest = bookingGuest));
  }

  previousState(): void {
    window.history.back();
  }
}
