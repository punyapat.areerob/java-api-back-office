import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookingGuest } from 'app/shared/model/booking-guest.model';
import { BookingGuestService } from './booking-guest.service';
import { BookingGuestDeleteDialogComponent } from './booking-guest-delete-dialog.component';

@Component({
  selector: 'jhi-booking-guest',
  templateUrl: './booking-guest.component.html',
})
export class BookingGuestComponent implements OnInit, OnDestroy {
  bookingGuests?: IBookingGuest[];
  eventSubscriber?: Subscription;

  constructor(
    protected bookingGuestService: BookingGuestService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.bookingGuestService.query().subscribe((res: HttpResponse<IBookingGuest[]>) => (this.bookingGuests = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBookingGuests();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBookingGuest): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBookingGuests(): void {
    this.eventSubscriber = this.eventManager.subscribe('bookingGuestListModification', () => this.loadAll());
  }

  delete(bookingGuest: IBookingGuest): void {
    const modalRef = this.modalService.open(BookingGuestDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bookingGuest = bookingGuest;
  }
}
