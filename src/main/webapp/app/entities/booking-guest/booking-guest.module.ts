import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BookingGuestComponent } from './booking-guest.component';
import { BookingGuestDetailComponent } from './booking-guest-detail.component';
import { BookingGuestUpdateComponent } from './booking-guest-update.component';
import { BookingGuestDeleteDialogComponent } from './booking-guest-delete-dialog.component';
import { bookingGuestRoute } from './booking-guest.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(bookingGuestRoute)],
  declarations: [BookingGuestComponent, BookingGuestDetailComponent, BookingGuestUpdateComponent, BookingGuestDeleteDialogComponent],
  entryComponents: [BookingGuestDeleteDialogComponent],
})
export class MisbeBookingGuestModule {}
