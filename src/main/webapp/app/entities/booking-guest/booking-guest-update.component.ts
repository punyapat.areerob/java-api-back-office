import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBookingGuest, BookingGuest } from 'app/shared/model/booking-guest.model';
import { BookingGuestService } from './booking-guest.service';
import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';
import { BookingServiceMemberService } from 'app/entities/booking-service-member/booking-service-member.service';

@Component({
  selector: 'jhi-booking-guest-update',
  templateUrl: './booking-guest-update.component.html',
})
export class BookingGuestUpdateComponent implements OnInit {
  isSaving = false;
  bookingservicemembers: IBookingServiceMember[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    amount: [],
    createdAt: [],
    updatedAt: [],
    bookingServiceMember: [],
  });

  constructor(
    protected bookingGuestService: BookingGuestService,
    protected bookingServiceMemberService: BookingServiceMemberService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingGuest }) => {
      if (!bookingGuest.id) {
        const today = moment().startOf('day');
        bookingGuest.createdAt = today;
        bookingGuest.updatedAt = today;
      }

      this.updateForm(bookingGuest);

      this.bookingServiceMemberService
        .query()
        .subscribe((res: HttpResponse<IBookingServiceMember[]>) => (this.bookingservicemembers = res.body || []));
    });
  }

  updateForm(bookingGuest: IBookingGuest): void {
    this.editForm.patchValue({
      id: bookingGuest.id,
      name: bookingGuest.name,
      amount: bookingGuest.amount,
      createdAt: bookingGuest.createdAt ? bookingGuest.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: bookingGuest.updatedAt ? bookingGuest.updatedAt.format(DATE_TIME_FORMAT) : null,
      bookingServiceMember: bookingGuest.bookingServiceMember,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookingGuest = this.createFromForm();
    if (bookingGuest.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingGuestService.update(bookingGuest));
    } else {
      this.subscribeToSaveResponse(this.bookingGuestService.create(bookingGuest));
    }
  }

  private createFromForm(): IBookingGuest {
    return {
      ...new BookingGuest(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
      bookingServiceMember: this.editForm.get(['bookingServiceMember'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookingGuest>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBookingServiceMember): any {
    return item.id;
  }
}
