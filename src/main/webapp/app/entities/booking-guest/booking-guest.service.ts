import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBookingGuest } from 'app/shared/model/booking-guest.model';

type EntityResponseType = HttpResponse<IBookingGuest>;
type EntityArrayResponseType = HttpResponse<IBookingGuest[]>;

@Injectable({ providedIn: 'root' })
export class BookingGuestService {
  public resourceUrl = SERVER_API_URL + 'api/booking-guests';

  constructor(protected http: HttpClient) {}

  create(bookingGuest: IBookingGuest): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingGuest);
    return this.http
      .post<IBookingGuest>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bookingGuest: IBookingGuest): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingGuest);
    return this.http
      .put<IBookingGuest>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IBookingGuest>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBookingGuest[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(bookingGuest: IBookingGuest): IBookingGuest {
    const copy: IBookingGuest = Object.assign({}, bookingGuest, {
      createdAt: bookingGuest.createdAt && bookingGuest.createdAt.isValid() ? bookingGuest.createdAt.toJSON() : undefined,
      updatedAt: bookingGuest.updatedAt && bookingGuest.updatedAt.isValid() ? bookingGuest.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bookingGuest: IBookingGuest) => {
        bookingGuest.createdAt = bookingGuest.createdAt ? moment(bookingGuest.createdAt) : undefined;
        bookingGuest.updatedAt = bookingGuest.updatedAt ? moment(bookingGuest.updatedAt) : undefined;
      });
    }
    return res;
  }
}
