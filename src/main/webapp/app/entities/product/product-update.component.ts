import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProduct, Product } from 'app/shared/model/product.model';
import { ProductService } from './product.service';
import { IService } from 'app/shared/model/service.model';
import { ServiceService } from 'app/entities/service/service.service';
import { ISubProductCategory } from 'app/shared/model/sub-product-category.model';
import { SubProductCategoryService } from 'app/entities/sub-product-category/sub-product-category.service';

type SelectableEntity = IService | ISubProductCategory;

@Component({
  selector: 'jhi-product-update',
  templateUrl: './product-update.component.html',
})
export class ProductUpdateComponent implements OnInit {
  isSaving = false;
  services: IService[] = [];
  subproductcategories: ISubProductCategory[] = [];

  editForm = this.fb.group({
    id: [],
    shopId: [],
    name: [],
    description: [],
    policy: [],
    images: [],
    active: [],
    service: [],
    subProductCategory: [],
  });

  constructor(
    protected productService: ProductService,
    protected serviceService: ServiceService,
    protected subProductCategoryService: SubProductCategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ product }) => {
      this.updateForm(product);

      this.serviceService.query().subscribe((res: HttpResponse<IService[]>) => (this.services = res.body || []));

      this.subProductCategoryService
        .query()
        .subscribe((res: HttpResponse<ISubProductCategory[]>) => (this.subproductcategories = res.body || []));
    });
  }

  updateForm(product: IProduct): void {
    this.editForm.patchValue({
      id: product.id,
      shopId: product.shopId,
      name: product.name,
      description: product.description,
      policy: product.policy,
      images: product.images,
      active: product.active,
      service: product.service,
      subProductCategory: product.subProductCategory,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const product = this.createFromForm();
    if (product.id !== undefined) {
      this.subscribeToSaveResponse(this.productService.update(product));
    } else {
      this.subscribeToSaveResponse(this.productService.create(product));
    }
  }

  private createFromForm(): IProduct {
    return {
      ...new Product(),
      id: this.editForm.get(['id'])!.value,
      shopId: this.editForm.get(['shopId'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      policy: this.editForm.get(['policy'])!.value,
      images: this.editForm.get(['images'])!.value,
      active: this.editForm.get(['active'])!.value,
      service: this.editForm.get(['service'])!.value,
      subProductCategory: this.editForm.get(['subProductCategory'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
