import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJobAssignment } from 'app/shared/model/job-assignment.model';
import { JobAssignmentService } from './job-assignment.service';
import { JobAssignmentDeleteDialogComponent } from './job-assignment-delete-dialog.component';

@Component({
  selector: 'jhi-job-assignment',
  templateUrl: './job-assignment.component.html',
})
export class JobAssignmentComponent implements OnInit, OnDestroy {
  jobAssignments?: IJobAssignment[];
  eventSubscriber?: Subscription;

  constructor(
    protected jobAssignmentService: JobAssignmentService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.jobAssignmentService.query().subscribe((res: HttpResponse<IJobAssignment[]>) => (this.jobAssignments = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJobAssignments();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJobAssignment): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJobAssignments(): void {
    this.eventSubscriber = this.eventManager.subscribe('jobAssignmentListModification', () => this.loadAll());
  }

  delete(jobAssignment: IJobAssignment): void {
    const modalRef = this.modalService.open(JobAssignmentDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jobAssignment = jobAssignment;
  }
}
