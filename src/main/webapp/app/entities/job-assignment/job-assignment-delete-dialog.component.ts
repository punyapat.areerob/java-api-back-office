import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJobAssignment } from 'app/shared/model/job-assignment.model';
import { JobAssignmentService } from './job-assignment.service';

@Component({
  templateUrl: './job-assignment-delete-dialog.component.html',
})
export class JobAssignmentDeleteDialogComponent {
  jobAssignment?: IJobAssignment;

  constructor(
    protected jobAssignmentService: JobAssignmentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.jobAssignmentService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jobAssignmentListModification');
      this.activeModal.close();
    });
  }
}
