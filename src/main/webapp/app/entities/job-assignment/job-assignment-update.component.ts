import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IJobAssignment, JobAssignment } from 'app/shared/model/job-assignment.model';
import { JobAssignmentService } from './job-assignment.service';

@Component({
  selector: 'jhi-job-assignment-update',
  templateUrl: './job-assignment-update.component.html',
})
export class JobAssignmentUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    imageUrl: [],
    createdAt: [],
    updatedAt: [],
  });

  constructor(protected jobAssignmentService: JobAssignmentService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobAssignment }) => {
      if (!jobAssignment.id) {
        const today = moment().startOf('day');
        jobAssignment.createdAt = today;
        jobAssignment.updatedAt = today;
      }

      this.updateForm(jobAssignment);
    });
  }

  updateForm(jobAssignment: IJobAssignment): void {
    this.editForm.patchValue({
      id: jobAssignment.id,
      imageUrl: jobAssignment.imageUrl,
      createdAt: jobAssignment.createdAt ? jobAssignment.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: jobAssignment.updatedAt ? jobAssignment.updatedAt.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jobAssignment = this.createFromForm();
    if (jobAssignment.id !== undefined) {
      this.subscribeToSaveResponse(this.jobAssignmentService.update(jobAssignment));
    } else {
      this.subscribeToSaveResponse(this.jobAssignmentService.create(jobAssignment));
    }
  }

  private createFromForm(): IJobAssignment {
    return {
      ...new JobAssignment(),
      id: this.editForm.get(['id'])!.value,
      imageUrl: this.editForm.get(['imageUrl'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJobAssignment>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
