import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJobAssignment } from 'app/shared/model/job-assignment.model';

@Component({
  selector: 'jhi-job-assignment-detail',
  templateUrl: './job-assignment-detail.component.html',
})
export class JobAssignmentDetailComponent implements OnInit {
  jobAssignment: IJobAssignment | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobAssignment }) => (this.jobAssignment = jobAssignment));
  }

  previousState(): void {
    window.history.back();
  }
}
