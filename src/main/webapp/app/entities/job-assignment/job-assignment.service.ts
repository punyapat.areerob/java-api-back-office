import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJobAssignment } from 'app/shared/model/job-assignment.model';

type EntityResponseType = HttpResponse<IJobAssignment>;
type EntityArrayResponseType = HttpResponse<IJobAssignment[]>;

@Injectable({ providedIn: 'root' })
export class JobAssignmentService {
  public resourceUrl = SERVER_API_URL + 'api/job-assignments';

  constructor(protected http: HttpClient) {}

  create(jobAssignment: IJobAssignment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(jobAssignment);
    return this.http
      .post<IJobAssignment>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(jobAssignment: IJobAssignment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(jobAssignment);
    return this.http
      .put<IJobAssignment>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IJobAssignment>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IJobAssignment[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(jobAssignment: IJobAssignment): IJobAssignment {
    const copy: IJobAssignment = Object.assign({}, jobAssignment, {
      createdAt: jobAssignment.createdAt && jobAssignment.createdAt.isValid() ? jobAssignment.createdAt.toJSON() : undefined,
      updatedAt: jobAssignment.updatedAt && jobAssignment.updatedAt.isValid() ? jobAssignment.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((jobAssignment: IJobAssignment) => {
        jobAssignment.createdAt = jobAssignment.createdAt ? moment(jobAssignment.createdAt) : undefined;
        jobAssignment.updatedAt = jobAssignment.updatedAt ? moment(jobAssignment.updatedAt) : undefined;
      });
    }
    return res;
  }
}
