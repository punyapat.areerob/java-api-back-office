import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJobAssignment, JobAssignment } from 'app/shared/model/job-assignment.model';
import { JobAssignmentService } from './job-assignment.service';
import { JobAssignmentComponent } from './job-assignment.component';
import { JobAssignmentDetailComponent } from './job-assignment-detail.component';
import { JobAssignmentUpdateComponent } from './job-assignment-update.component';

@Injectable({ providedIn: 'root' })
export class JobAssignmentResolve implements Resolve<IJobAssignment> {
  constructor(private service: JobAssignmentService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJobAssignment> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jobAssignment: HttpResponse<JobAssignment>) => {
          if (jobAssignment.body) {
            return of(jobAssignment.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new JobAssignment());
  }
}

export const jobAssignmentRoute: Routes = [
  {
    path: '',
    component: JobAssignmentComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobAssignments',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JobAssignmentDetailComponent,
    resolve: {
      jobAssignment: JobAssignmentResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobAssignments',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JobAssignmentUpdateComponent,
    resolve: {
      jobAssignment: JobAssignmentResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobAssignments',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JobAssignmentUpdateComponent,
    resolve: {
      jobAssignment: JobAssignmentResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobAssignments',
    },
    canActivate: [UserRouteAccessService],
  },
];
