import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { JobAssignmentComponent } from './job-assignment.component';
import { JobAssignmentDetailComponent } from './job-assignment-detail.component';
import { JobAssignmentUpdateComponent } from './job-assignment-update.component';
import { JobAssignmentDeleteDialogComponent } from './job-assignment-delete-dialog.component';
import { jobAssignmentRoute } from './job-assignment.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(jobAssignmentRoute)],
  declarations: [JobAssignmentComponent, JobAssignmentDetailComponent, JobAssignmentUpdateComponent, JobAssignmentDeleteDialogComponent],
  entryComponents: [JobAssignmentDeleteDialogComponent],
})
export class MisbeJobAssignmentModule {}
