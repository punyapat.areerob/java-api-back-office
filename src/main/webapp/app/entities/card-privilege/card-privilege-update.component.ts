import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICardPrivilege, CardPrivilege } from 'app/shared/model/card-privilege.model';
import { CardPrivilegeService } from './card-privilege.service';

@Component({
  selector: 'jhi-card-privilege-update',
  templateUrl: './card-privilege-update.component.html',
})
export class CardPrivilegeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cardId: [],
    cardName: [],
    privilegeId: [],
    privilegeName: [],
    quota: [],
    validityPeriod: [],
    active: [],
  });

  constructor(protected cardPrivilegeService: CardPrivilegeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cardPrivilege }) => {
      this.updateForm(cardPrivilege);
    });
  }

  updateForm(cardPrivilege: ICardPrivilege): void {
    this.editForm.patchValue({
      id: cardPrivilege.id,
      cardId: cardPrivilege.cardId,
      cardName: cardPrivilege.cardName,
      privilegeId: cardPrivilege.privilegeId,
      privilegeName: cardPrivilege.privilegeName,
      quota: cardPrivilege.quota,
      validityPeriod: cardPrivilege.validityPeriod,
      active: cardPrivilege.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cardPrivilege = this.createFromForm();
    if (cardPrivilege.id !== undefined) {
      this.subscribeToSaveResponse(this.cardPrivilegeService.update(cardPrivilege));
    } else {
      this.subscribeToSaveResponse(this.cardPrivilegeService.create(cardPrivilege));
    }
  }

  private createFromForm(): ICardPrivilege {
    return {
      ...new CardPrivilege(),
      id: this.editForm.get(['id'])!.value,
      cardId: this.editForm.get(['cardId'])!.value,
      cardName: this.editForm.get(['cardName'])!.value,
      privilegeId: this.editForm.get(['privilegeId'])!.value,
      privilegeName: this.editForm.get(['privilegeName'])!.value,
      quota: this.editForm.get(['quota'])!.value,
      validityPeriod: this.editForm.get(['validityPeriod'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICardPrivilege>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
