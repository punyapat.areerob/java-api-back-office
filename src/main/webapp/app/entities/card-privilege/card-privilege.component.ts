import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICardPrivilege } from 'app/shared/model/card-privilege.model';
import { CardPrivilegeService } from './card-privilege.service';
import { CardPrivilegeDeleteDialogComponent } from './card-privilege-delete-dialog.component';

@Component({
  selector: 'jhi-card-privilege',
  templateUrl: './card-privilege.component.html',
})
export class CardPrivilegeComponent implements OnInit, OnDestroy {
  cardPrivileges?: ICardPrivilege[];
  eventSubscriber?: Subscription;

  constructor(
    protected cardPrivilegeService: CardPrivilegeService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.cardPrivilegeService.query().subscribe((res: HttpResponse<ICardPrivilege[]>) => (this.cardPrivileges = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCardPrivileges();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICardPrivilege): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCardPrivileges(): void {
    this.eventSubscriber = this.eventManager.subscribe('cardPrivilegeListModification', () => this.loadAll());
  }

  delete(cardPrivilege: ICardPrivilege): void {
    const modalRef = this.modalService.open(CardPrivilegeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cardPrivilege = cardPrivilege;
  }
}
