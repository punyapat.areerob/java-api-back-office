import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CardPrivilegeComponent } from './card-privilege.component';
import { CardPrivilegeDetailComponent } from './card-privilege-detail.component';
import { CardPrivilegeUpdateComponent } from './card-privilege-update.component';
import { CardPrivilegeDeleteDialogComponent } from './card-privilege-delete-dialog.component';
import { cardPrivilegeRoute } from './card-privilege.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(cardPrivilegeRoute)],
  declarations: [CardPrivilegeComponent, CardPrivilegeDetailComponent, CardPrivilegeUpdateComponent, CardPrivilegeDeleteDialogComponent],
  entryComponents: [CardPrivilegeDeleteDialogComponent],
})
export class MisbeCardPrivilegeModule {}
