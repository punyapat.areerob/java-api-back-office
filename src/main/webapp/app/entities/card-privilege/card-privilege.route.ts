import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICardPrivilege, CardPrivilege } from 'app/shared/model/card-privilege.model';
import { CardPrivilegeService } from './card-privilege.service';
import { CardPrivilegeComponent } from './card-privilege.component';
import { CardPrivilegeDetailComponent } from './card-privilege-detail.component';
import { CardPrivilegeUpdateComponent } from './card-privilege-update.component';

@Injectable({ providedIn: 'root' })
export class CardPrivilegeResolve implements Resolve<ICardPrivilege> {
  constructor(private service: CardPrivilegeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICardPrivilege> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cardPrivilege: HttpResponse<CardPrivilege>) => {
          if (cardPrivilege.body) {
            return of(cardPrivilege.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CardPrivilege());
  }
}

export const cardPrivilegeRoute: Routes = [
  {
    path: '',
    component: CardPrivilegeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CardPrivileges',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CardPrivilegeDetailComponent,
    resolve: {
      cardPrivilege: CardPrivilegeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CardPrivileges',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CardPrivilegeUpdateComponent,
    resolve: {
      cardPrivilege: CardPrivilegeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CardPrivileges',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CardPrivilegeUpdateComponent,
    resolve: {
      cardPrivilege: CardPrivilegeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CardPrivileges',
    },
    canActivate: [UserRouteAccessService],
  },
];
