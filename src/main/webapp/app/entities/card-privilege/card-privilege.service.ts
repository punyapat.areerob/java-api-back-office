import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICardPrivilege } from 'app/shared/model/card-privilege.model';

type EntityResponseType = HttpResponse<ICardPrivilege>;
type EntityArrayResponseType = HttpResponse<ICardPrivilege[]>;

@Injectable({ providedIn: 'root' })
export class CardPrivilegeService {
  public resourceUrl = SERVER_API_URL + 'api/card-privileges';

  constructor(protected http: HttpClient) {}

  create(cardPrivilege: ICardPrivilege): Observable<EntityResponseType> {
    return this.http.post<ICardPrivilege>(this.resourceUrl, cardPrivilege, { observe: 'response' });
  }

  update(cardPrivilege: ICardPrivilege): Observable<EntityResponseType> {
    return this.http.put<ICardPrivilege>(this.resourceUrl, cardPrivilege, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ICardPrivilege>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICardPrivilege[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
