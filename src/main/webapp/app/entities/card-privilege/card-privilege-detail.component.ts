import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICardPrivilege } from 'app/shared/model/card-privilege.model';

@Component({
  selector: 'jhi-card-privilege-detail',
  templateUrl: './card-privilege-detail.component.html',
})
export class CardPrivilegeDetailComponent implements OnInit {
  cardPrivilege: ICardPrivilege | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cardPrivilege }) => (this.cardPrivilege = cardPrivilege));
  }

  previousState(): void {
    window.history.back();
  }
}
