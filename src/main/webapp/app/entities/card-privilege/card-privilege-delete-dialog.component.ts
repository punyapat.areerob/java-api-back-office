import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICardPrivilege } from 'app/shared/model/card-privilege.model';
import { CardPrivilegeService } from './card-privilege.service';

@Component({
  templateUrl: './card-privilege-delete-dialog.component.html',
})
export class CardPrivilegeDeleteDialogComponent {
  cardPrivilege?: ICardPrivilege;

  constructor(
    protected cardPrivilegeService: CardPrivilegeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.cardPrivilegeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cardPrivilegeListModification');
      this.activeModal.close();
    });
  }
}
