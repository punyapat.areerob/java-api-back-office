import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAction, Action } from 'app/shared/model/action.model';
import { ActionService } from './action.service';
import { ICaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from 'app/entities/case-step/case-step.service';

@Component({
  selector: 'jhi-action-update',
  templateUrl: './action-update.component.html',
})
export class ActionUpdateComponent implements OnInit {
  isSaving = false;
  casesteps: ICaseStep[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    next: [],
    type: [],
    caseStep: [],
  });

  constructor(
    protected actionService: ActionService,
    protected caseStepService: CaseStepService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ action }) => {
      this.updateForm(action);

      this.caseStepService.query().subscribe((res: HttpResponse<ICaseStep[]>) => (this.casesteps = res.body || []));
    });
  }

  updateForm(action: IAction): void {
    this.editForm.patchValue({
      id: action.id,
      name: action.name,
      next: action.next,
      type: action.type,
      caseStep: action.caseStep,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const action = this.createFromForm();
    if (action.id !== undefined) {
      this.subscribeToSaveResponse(this.actionService.update(action));
    } else {
      this.subscribeToSaveResponse(this.actionService.create(action));
    }
  }

  private createFromForm(): IAction {
    return {
      ...new Action(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      next: this.editForm.get(['next'])!.value,
      type: this.editForm.get(['type'])!.value,
      caseStep: this.editForm.get(['caseStep'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAction>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICaseStep): any {
    return item.id;
  }
}
