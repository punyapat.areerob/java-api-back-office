import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJobDescription } from 'app/shared/model/job-description.model';

@Component({
  selector: 'jhi-job-description-detail',
  templateUrl: './job-description-detail.component.html',
})
export class JobDescriptionDetailComponent implements OnInit {
  jobDescription: IJobDescription | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobDescription }) => (this.jobDescription = jobDescription));
  }

  previousState(): void {
    window.history.back();
  }
}
