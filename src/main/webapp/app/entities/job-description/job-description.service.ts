import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJobDescription } from 'app/shared/model/job-description.model';

type EntityResponseType = HttpResponse<IJobDescription>;
type EntityArrayResponseType = HttpResponse<IJobDescription[]>;

@Injectable({ providedIn: 'root' })
export class JobDescriptionService {
  public resourceUrl = SERVER_API_URL + 'api/job-descriptions';

  constructor(protected http: HttpClient) {}

  create(jobDescription: IJobDescription): Observable<EntityResponseType> {
    return this.http.post<IJobDescription>(this.resourceUrl, jobDescription, { observe: 'response' });
  }

  update(jobDescription: IJobDescription): Observable<EntityResponseType> {
    return this.http.put<IJobDescription>(this.resourceUrl, jobDescription, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IJobDescription>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJobDescription[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
