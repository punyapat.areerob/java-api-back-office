import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJobDescription } from 'app/shared/model/job-description.model';
import { JobDescriptionService } from './job-description.service';

@Component({
  templateUrl: './job-description-delete-dialog.component.html',
})
export class JobDescriptionDeleteDialogComponent {
  jobDescription?: IJobDescription;

  constructor(
    protected jobDescriptionService: JobDescriptionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.jobDescriptionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jobDescriptionListModification');
      this.activeModal.close();
    });
  }
}
