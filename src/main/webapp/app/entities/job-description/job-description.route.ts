import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJobDescription, JobDescription } from 'app/shared/model/job-description.model';
import { JobDescriptionService } from './job-description.service';
import { JobDescriptionComponent } from './job-description.component';
import { JobDescriptionDetailComponent } from './job-description-detail.component';
import { JobDescriptionUpdateComponent } from './job-description-update.component';

@Injectable({ providedIn: 'root' })
export class JobDescriptionResolve implements Resolve<IJobDescription> {
  constructor(private service: JobDescriptionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJobDescription> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jobDescription: HttpResponse<JobDescription>) => {
          if (jobDescription.body) {
            return of(jobDescription.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new JobDescription());
  }
}

export const jobDescriptionRoute: Routes = [
  {
    path: '',
    component: JobDescriptionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobDescriptions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JobDescriptionDetailComponent,
    resolve: {
      jobDescription: JobDescriptionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobDescriptions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JobDescriptionUpdateComponent,
    resolve: {
      jobDescription: JobDescriptionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobDescriptions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JobDescriptionUpdateComponent,
    resolve: {
      jobDescription: JobDescriptionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobDescriptions',
    },
    canActivate: [UserRouteAccessService],
  },
];
