import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IJobDescription, JobDescription } from 'app/shared/model/job-description.model';
import { JobDescriptionService } from './job-description.service';

@Component({
  selector: 'jhi-job-description-update',
  templateUrl: './job-description-update.component.html',
})
export class JobDescriptionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    teamId: [null, [Validators.required]],
    teamName: [null, [Validators.required]],
    jobTitle: [null, [Validators.required]],
    lineOfWork: [null, [Validators.required]],
    department: [null, [Validators.required]],
    educationBackground: [null, [Validators.required]],
    salary: [null, [Validators.required]],
    location: [null, [Validators.required]],
    summary: [null, [Validators.required]],
    mainResponsibilities: [null, [Validators.required]],
    jobDescription: [null, [Validators.required]],
    jobQualifications: [null, [Validators.required]],
    profilePhoto: [],
    militaryPass: [],
    enLanguageScore: [],
    educationCertificate: [],
    driverLicense: [],
    chLanguageScore: [],
    educationQualification: [],
    citizenID: [],
    jpLanguageScore: [],
    workPermit: [],
    marriageCertificate: [],
    houseRegistry: [],
    birthCertificate: [],
    remark: [],
  });

  constructor(protected jobDescriptionService: JobDescriptionService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobDescription }) => {
      this.updateForm(jobDescription);
    });
  }

  updateForm(jobDescription: IJobDescription): void {
    this.editForm.patchValue({
      id: jobDescription.id,
      name: jobDescription.name,
      teamId: jobDescription.teamId,
      teamName: jobDescription.teamName,
      jobTitle: jobDescription.jobTitle,
      lineOfWork: jobDescription.lineOfWork,
      department: jobDescription.department,
      educationBackground: jobDescription.educationBackground,
      salary: jobDescription.salary,
      location: jobDescription.location,
      summary: jobDescription.summary,
      mainResponsibilities: jobDescription.mainResponsibilities,
      jobDescription: jobDescription.jobDescription,
      jobQualifications: jobDescription.jobQualifications,
      profilePhoto: jobDescription.profilePhoto,
      militaryPass: jobDescription.militaryPass,
      enLanguageScore: jobDescription.enLanguageScore,
      educationCertificate: jobDescription.educationCertificate,
      driverLicense: jobDescription.driverLicense,
      chLanguageScore: jobDescription.chLanguageScore,
      educationQualification: jobDescription.educationQualification,
      citizenID: jobDescription.citizenID,
      jpLanguageScore: jobDescription.jpLanguageScore,
      workPermit: jobDescription.workPermit,
      marriageCertificate: jobDescription.marriageCertificate,
      houseRegistry: jobDescription.houseRegistry,
      birthCertificate: jobDescription.birthCertificate,
      remark: jobDescription.remark,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jobDescription = this.createFromForm();
    if (jobDescription.id !== undefined) {
      this.subscribeToSaveResponse(this.jobDescriptionService.update(jobDescription));
    } else {
      this.subscribeToSaveResponse(this.jobDescriptionService.create(jobDescription));
    }
  }

  private createFromForm(): IJobDescription {
    return {
      ...new JobDescription(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      teamId: this.editForm.get(['teamId'])!.value,
      teamName: this.editForm.get(['teamName'])!.value,
      jobTitle: this.editForm.get(['jobTitle'])!.value,
      lineOfWork: this.editForm.get(['lineOfWork'])!.value,
      department: this.editForm.get(['department'])!.value,
      educationBackground: this.editForm.get(['educationBackground'])!.value,
      salary: this.editForm.get(['salary'])!.value,
      location: this.editForm.get(['location'])!.value,
      summary: this.editForm.get(['summary'])!.value,
      mainResponsibilities: this.editForm.get(['mainResponsibilities'])!.value,
      jobDescription: this.editForm.get(['jobDescription'])!.value,
      jobQualifications: this.editForm.get(['jobQualifications'])!.value,
      profilePhoto: this.editForm.get(['profilePhoto'])!.value,
      militaryPass: this.editForm.get(['militaryPass'])!.value,
      enLanguageScore: this.editForm.get(['enLanguageScore'])!.value,
      educationCertificate: this.editForm.get(['educationCertificate'])!.value,
      driverLicense: this.editForm.get(['driverLicense'])!.value,
      chLanguageScore: this.editForm.get(['chLanguageScore'])!.value,
      educationQualification: this.editForm.get(['educationQualification'])!.value,
      citizenID: this.editForm.get(['citizenID'])!.value,
      jpLanguageScore: this.editForm.get(['jpLanguageScore'])!.value,
      workPermit: this.editForm.get(['workPermit'])!.value,
      marriageCertificate: this.editForm.get(['marriageCertificate'])!.value,
      houseRegistry: this.editForm.get(['houseRegistry'])!.value,
      birthCertificate: this.editForm.get(['birthCertificate'])!.value,
      remark: this.editForm.get(['remark'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJobDescription>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
