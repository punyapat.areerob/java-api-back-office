import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJobDescription } from 'app/shared/model/job-description.model';
import { JobDescriptionService } from './job-description.service';
import { JobDescriptionDeleteDialogComponent } from './job-description-delete-dialog.component';

@Component({
  selector: 'jhi-job-description',
  templateUrl: './job-description.component.html',
})
export class JobDescriptionComponent implements OnInit, OnDestroy {
  jobDescriptions?: IJobDescription[];
  eventSubscriber?: Subscription;

  constructor(
    protected jobDescriptionService: JobDescriptionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.jobDescriptionService.query().subscribe((res: HttpResponse<IJobDescription[]>) => (this.jobDescriptions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJobDescriptions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJobDescription): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJobDescriptions(): void {
    this.eventSubscriber = this.eventManager.subscribe('jobDescriptionListModification', () => this.loadAll());
  }

  delete(jobDescription: IJobDescription): void {
    const modalRef = this.modalService.open(JobDescriptionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jobDescription = jobDescription;
  }
}
