import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { JobDescriptionComponent } from './job-description.component';
import { JobDescriptionDetailComponent } from './job-description-detail.component';
import { JobDescriptionUpdateComponent } from './job-description-update.component';
import { JobDescriptionDeleteDialogComponent } from './job-description-delete-dialog.component';
import { jobDescriptionRoute } from './job-description.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(jobDescriptionRoute)],
  declarations: [
    JobDescriptionComponent,
    JobDescriptionDetailComponent,
    JobDescriptionUpdateComponent,
    JobDescriptionDeleteDialogComponent,
  ],
  entryComponents: [JobDescriptionDeleteDialogComponent],
})
export class MisbeJobDescriptionModule {}
