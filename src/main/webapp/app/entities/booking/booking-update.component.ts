import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBooking, Booking } from 'app/shared/model/booking.model';
import { BookingService } from './booking.service';
import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from 'app/entities/booking-status/booking-status.service';

@Component({
  selector: 'jhi-booking-update',
  templateUrl: './booking-update.component.html',
})
export class BookingUpdateComponent implements OnInit {
  isSaving = false;
  bookingstatuses: IBookingStatus[] = [];

  editForm = this.fb.group({
    id: [],
    remark: [],
    createdBy: [],
    updatedBy: [],
    createdAt: [],
    updatedAt: [],
    bookingStatus: [],
  });

  constructor(
    protected bookingService: BookingService,
    protected bookingStatusService: BookingStatusService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ booking }) => {
      if (!booking.id) {
        const today = moment().startOf('day');
        booking.createdBy = today;
        booking.updatedBy = today;
        booking.createdAt = today;
        booking.updatedAt = today;
      }

      this.updateForm(booking);

      this.bookingStatusService
        .query({ filter: 'booking-is-null' })
        .pipe(
          map((res: HttpResponse<IBookingStatus[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBookingStatus[]) => {
          if (!booking.bookingStatus || !booking.bookingStatus.id) {
            this.bookingstatuses = resBody;
          } else {
            this.bookingStatusService
              .find(booking.bookingStatus.id)
              .pipe(
                map((subRes: HttpResponse<IBookingStatus>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBookingStatus[]) => (this.bookingstatuses = concatRes));
          }
        });
    });
  }

  updateForm(booking: IBooking): void {
    this.editForm.patchValue({
      id: booking.id,
      remark: booking.remark,
      createdBy: booking.createdBy ? booking.createdBy.format(DATE_TIME_FORMAT) : null,
      updatedBy: booking.updatedBy ? booking.updatedBy.format(DATE_TIME_FORMAT) : null,
      createdAt: booking.createdAt ? booking.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: booking.updatedAt ? booking.updatedAt.format(DATE_TIME_FORMAT) : null,
      bookingStatus: booking.bookingStatus,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const booking = this.createFromForm();
    if (booking.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingService.update(booking));
    } else {
      this.subscribeToSaveResponse(this.bookingService.create(booking));
    }
  }

  private createFromForm(): IBooking {
    return {
      ...new Booking(),
      id: this.editForm.get(['id'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value ? moment(this.editForm.get(['createdBy'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value ? moment(this.editForm.get(['updatedBy'])!.value, DATE_TIME_FORMAT) : undefined,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
      bookingStatus: this.editForm.get(['bookingStatus'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBooking>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBookingStatus): any {
    return item.id;
  }
}
