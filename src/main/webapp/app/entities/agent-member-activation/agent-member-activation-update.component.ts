import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAgentMemberActivation, AgentMemberActivation } from 'app/shared/model/agent-member-activation.model';
import { AgentMemberActivationService } from './agent-member-activation.service';

@Component({
  selector: 'jhi-agent-member-activation-update',
  templateUrl: './agent-member-activation-update.component.html',
})
export class AgentMemberActivationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentId: [],
    invoice: [],
    date: [],
    activate: [],
    remark: [],
    currency: [],
    money: [],
  });

  constructor(
    protected agentMemberActivationService: AgentMemberActivationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentMemberActivation }) => {
      if (!agentMemberActivation.id) {
        const today = moment().startOf('day');
        agentMemberActivation.date = today;
        agentMemberActivation.activate = today;
      }

      this.updateForm(agentMemberActivation);
    });
  }

  updateForm(agentMemberActivation: IAgentMemberActivation): void {
    this.editForm.patchValue({
      id: agentMemberActivation.id,
      agentId: agentMemberActivation.agentId,
      invoice: agentMemberActivation.invoice,
      date: agentMemberActivation.date ? agentMemberActivation.date.format(DATE_TIME_FORMAT) : null,
      activate: agentMemberActivation.activate ? agentMemberActivation.activate.format(DATE_TIME_FORMAT) : null,
      remark: agentMemberActivation.remark,
      currency: agentMemberActivation.currency,
      money: agentMemberActivation.money,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agentMemberActivation = this.createFromForm();
    if (agentMemberActivation.id !== undefined) {
      this.subscribeToSaveResponse(this.agentMemberActivationService.update(agentMemberActivation));
    } else {
      this.subscribeToSaveResponse(this.agentMemberActivationService.create(agentMemberActivation));
    }
  }

  private createFromForm(): IAgentMemberActivation {
    return {
      ...new AgentMemberActivation(),
      id: this.editForm.get(['id'])!.value,
      agentId: this.editForm.get(['agentId'])!.value,
      invoice: this.editForm.get(['invoice'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      activate: this.editForm.get(['activate'])!.value ? moment(this.editForm.get(['activate'])!.value, DATE_TIME_FORMAT) : undefined,
      remark: this.editForm.get(['remark'])!.value,
      currency: this.editForm.get(['currency'])!.value,
      money: this.editForm.get(['money'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentMemberActivation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
