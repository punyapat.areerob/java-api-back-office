import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentMemberActivation } from 'app/shared/model/agent-member-activation.model';

@Component({
  selector: 'jhi-agent-member-activation-detail',
  templateUrl: './agent-member-activation-detail.component.html',
})
export class AgentMemberActivationDetailComponent implements OnInit {
  agentMemberActivation: IAgentMemberActivation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentMemberActivation }) => (this.agentMemberActivation = agentMemberActivation));
  }

  previousState(): void {
    window.history.back();
  }
}
