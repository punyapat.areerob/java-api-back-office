import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentMemberActivation } from 'app/shared/model/agent-member-activation.model';
import { AgentMemberActivationService } from './agent-member-activation.service';

@Component({
  templateUrl: './agent-member-activation-delete-dialog.component.html',
})
export class AgentMemberActivationDeleteDialogComponent {
  agentMemberActivation?: IAgentMemberActivation;

  constructor(
    protected agentMemberActivationService: AgentMemberActivationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.agentMemberActivationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('agentMemberActivationListModification');
      this.activeModal.close();
    });
  }
}
