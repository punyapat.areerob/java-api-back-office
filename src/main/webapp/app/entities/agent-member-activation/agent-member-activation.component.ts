import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAgentMemberActivation } from 'app/shared/model/agent-member-activation.model';
import { AgentMemberActivationService } from './agent-member-activation.service';
import { AgentMemberActivationDeleteDialogComponent } from './agent-member-activation-delete-dialog.component';

@Component({
  selector: 'jhi-agent-member-activation',
  templateUrl: './agent-member-activation.component.html',
})
export class AgentMemberActivationComponent implements OnInit, OnDestroy {
  agentMemberActivations?: IAgentMemberActivation[];
  eventSubscriber?: Subscription;

  constructor(
    protected agentMemberActivationService: AgentMemberActivationService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.agentMemberActivationService
      .query()
      .subscribe((res: HttpResponse<IAgentMemberActivation[]>) => (this.agentMemberActivations = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAgentMemberActivations();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAgentMemberActivation): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAgentMemberActivations(): void {
    this.eventSubscriber = this.eventManager.subscribe('agentMemberActivationListModification', () => this.loadAll());
  }

  delete(agentMemberActivation: IAgentMemberActivation): void {
    const modalRef = this.modalService.open(AgentMemberActivationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.agentMemberActivation = agentMemberActivation;
  }
}
