import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentMemberActivation } from 'app/shared/model/agent-member-activation.model';

type EntityResponseType = HttpResponse<IAgentMemberActivation>;
type EntityArrayResponseType = HttpResponse<IAgentMemberActivation[]>;

@Injectable({ providedIn: 'root' })
export class AgentMemberActivationService {
  public resourceUrl = SERVER_API_URL + 'api/agent-member-activations';

  constructor(protected http: HttpClient) {}

  create(agentMemberActivation: IAgentMemberActivation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agentMemberActivation);
    return this.http
      .post<IAgentMemberActivation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(agentMemberActivation: IAgentMemberActivation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agentMemberActivation);
    return this.http
      .put<IAgentMemberActivation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IAgentMemberActivation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAgentMemberActivation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(agentMemberActivation: IAgentMemberActivation): IAgentMemberActivation {
    const copy: IAgentMemberActivation = Object.assign({}, agentMemberActivation, {
      date: agentMemberActivation.date && agentMemberActivation.date.isValid() ? agentMemberActivation.date.toJSON() : undefined,
      activate:
        agentMemberActivation.activate && agentMemberActivation.activate.isValid() ? agentMemberActivation.activate.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
      res.body.activate = res.body.activate ? moment(res.body.activate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((agentMemberActivation: IAgentMemberActivation) => {
        agentMemberActivation.date = agentMemberActivation.date ? moment(agentMemberActivation.date) : undefined;
        agentMemberActivation.activate = agentMemberActivation.activate ? moment(agentMemberActivation.activate) : undefined;
      });
    }
    return res;
  }
}
