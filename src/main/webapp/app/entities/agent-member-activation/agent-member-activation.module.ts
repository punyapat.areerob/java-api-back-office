import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { AgentMemberActivationComponent } from './agent-member-activation.component';
import { AgentMemberActivationDetailComponent } from './agent-member-activation-detail.component';
import { AgentMemberActivationUpdateComponent } from './agent-member-activation-update.component';
import { AgentMemberActivationDeleteDialogComponent } from './agent-member-activation-delete-dialog.component';
import { agentMemberActivationRoute } from './agent-member-activation.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(agentMemberActivationRoute)],
  declarations: [
    AgentMemberActivationComponent,
    AgentMemberActivationDetailComponent,
    AgentMemberActivationUpdateComponent,
    AgentMemberActivationDeleteDialogComponent,
  ],
  entryComponents: [AgentMemberActivationDeleteDialogComponent],
})
export class MisbeAgentMemberActivationModule {}
