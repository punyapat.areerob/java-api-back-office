import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAgentMemberActivation, AgentMemberActivation } from 'app/shared/model/agent-member-activation.model';
import { AgentMemberActivationService } from './agent-member-activation.service';
import { AgentMemberActivationComponent } from './agent-member-activation.component';
import { AgentMemberActivationDetailComponent } from './agent-member-activation-detail.component';
import { AgentMemberActivationUpdateComponent } from './agent-member-activation-update.component';

@Injectable({ providedIn: 'root' })
export class AgentMemberActivationResolve implements Resolve<IAgentMemberActivation> {
  constructor(private service: AgentMemberActivationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAgentMemberActivation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((agentMemberActivation: HttpResponse<AgentMemberActivation>) => {
          if (agentMemberActivation.body) {
            return of(agentMemberActivation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AgentMemberActivation());
  }
}

export const agentMemberActivationRoute: Routes = [
  {
    path: '',
    component: AgentMemberActivationComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentMemberActivations',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AgentMemberActivationDetailComponent,
    resolve: {
      agentMemberActivation: AgentMemberActivationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentMemberActivations',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AgentMemberActivationUpdateComponent,
    resolve: {
      agentMemberActivation: AgentMemberActivationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentMemberActivations',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AgentMemberActivationUpdateComponent,
    resolve: {
      agentMemberActivation: AgentMemberActivationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentMemberActivations',
    },
    canActivate: [UserRouteAccessService],
  },
];
