import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMemberRemarkTag, MemberRemarkTag } from 'app/shared/model/member-remark-tag.model';
import { MemberRemarkTagService } from './member-remark-tag.service';
import { MemberRemarkTagComponent } from './member-remark-tag.component';
import { MemberRemarkTagDetailComponent } from './member-remark-tag-detail.component';
import { MemberRemarkTagUpdateComponent } from './member-remark-tag-update.component';

@Injectable({ providedIn: 'root' })
export class MemberRemarkTagResolve implements Resolve<IMemberRemarkTag> {
  constructor(private service: MemberRemarkTagService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMemberRemarkTag> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((memberRemarkTag: HttpResponse<MemberRemarkTag>) => {
          if (memberRemarkTag.body) {
            return of(memberRemarkTag.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MemberRemarkTag());
  }
}

export const memberRemarkTagRoute: Routes = [
  {
    path: '',
    component: MemberRemarkTagComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkTags',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MemberRemarkTagDetailComponent,
    resolve: {
      memberRemarkTag: MemberRemarkTagResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkTags',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MemberRemarkTagUpdateComponent,
    resolve: {
      memberRemarkTag: MemberRemarkTagResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkTags',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MemberRemarkTagUpdateComponent,
    resolve: {
      memberRemarkTag: MemberRemarkTagResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkTags',
    },
    canActivate: [UserRouteAccessService],
  },
];
