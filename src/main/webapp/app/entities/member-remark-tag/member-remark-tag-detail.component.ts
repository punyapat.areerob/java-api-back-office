import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMemberRemarkTag } from 'app/shared/model/member-remark-tag.model';

@Component({
  selector: 'jhi-member-remark-tag-detail',
  templateUrl: './member-remark-tag-detail.component.html',
})
export class MemberRemarkTagDetailComponent implements OnInit {
  memberRemarkTag: IMemberRemarkTag | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ memberRemarkTag }) => (this.memberRemarkTag = memberRemarkTag));
  }

  previousState(): void {
    window.history.back();
  }
}
