import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMemberRemarkTag } from 'app/shared/model/member-remark-tag.model';
import { MemberRemarkTagService } from './member-remark-tag.service';

@Component({
  templateUrl: './member-remark-tag-delete-dialog.component.html',
})
export class MemberRemarkTagDeleteDialogComponent {
  memberRemarkTag?: IMemberRemarkTag;

  constructor(
    protected memberRemarkTagService: MemberRemarkTagService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.memberRemarkTagService.delete(id).subscribe(() => {
      this.eventManager.broadcast('memberRemarkTagListModification');
      this.activeModal.close();
    });
  }
}
