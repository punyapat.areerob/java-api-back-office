import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMemberRemarkTag, MemberRemarkTag } from 'app/shared/model/member-remark-tag.model';
import { MemberRemarkTagService } from './member-remark-tag.service';

@Component({
  selector: 'jhi-member-remark-tag-update',
  templateUrl: './member-remark-tag-update.component.html',
})
export class MemberRemarkTagUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    active: [],
  });

  constructor(
    protected memberRemarkTagService: MemberRemarkTagService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ memberRemarkTag }) => {
      this.updateForm(memberRemarkTag);
    });
  }

  updateForm(memberRemarkTag: IMemberRemarkTag): void {
    this.editForm.patchValue({
      id: memberRemarkTag.id,
      name: memberRemarkTag.name,
      active: memberRemarkTag.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const memberRemarkTag = this.createFromForm();
    if (memberRemarkTag.id !== undefined) {
      this.subscribeToSaveResponse(this.memberRemarkTagService.update(memberRemarkTag));
    } else {
      this.subscribeToSaveResponse(this.memberRemarkTagService.create(memberRemarkTag));
    }
  }

  private createFromForm(): IMemberRemarkTag {
    return {
      ...new MemberRemarkTag(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMemberRemarkTag>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
