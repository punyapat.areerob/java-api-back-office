import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMemberRemarkTag } from 'app/shared/model/member-remark-tag.model';

type EntityResponseType = HttpResponse<IMemberRemarkTag>;
type EntityArrayResponseType = HttpResponse<IMemberRemarkTag[]>;

@Injectable({ providedIn: 'root' })
export class MemberRemarkTagService {
  public resourceUrl = SERVER_API_URL + 'api/member-remark-tags';

  constructor(protected http: HttpClient) {}

  create(memberRemarkTag: IMemberRemarkTag): Observable<EntityResponseType> {
    return this.http.post<IMemberRemarkTag>(this.resourceUrl, memberRemarkTag, { observe: 'response' });
  }

  update(memberRemarkTag: IMemberRemarkTag): Observable<EntityResponseType> {
    return this.http.put<IMemberRemarkTag>(this.resourceUrl, memberRemarkTag, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IMemberRemarkTag>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMemberRemarkTag[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
