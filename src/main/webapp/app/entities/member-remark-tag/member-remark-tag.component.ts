import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMemberRemarkTag } from 'app/shared/model/member-remark-tag.model';
import { MemberRemarkTagService } from './member-remark-tag.service';
import { MemberRemarkTagDeleteDialogComponent } from './member-remark-tag-delete-dialog.component';

@Component({
  selector: 'jhi-member-remark-tag',
  templateUrl: './member-remark-tag.component.html',
})
export class MemberRemarkTagComponent implements OnInit, OnDestroy {
  memberRemarkTags?: IMemberRemarkTag[];
  eventSubscriber?: Subscription;

  constructor(
    protected memberRemarkTagService: MemberRemarkTagService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.memberRemarkTagService.query().subscribe((res: HttpResponse<IMemberRemarkTag[]>) => (this.memberRemarkTags = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMemberRemarkTags();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMemberRemarkTag): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMemberRemarkTags(): void {
    this.eventSubscriber = this.eventManager.subscribe('memberRemarkTagListModification', () => this.loadAll());
  }

  delete(memberRemarkTag: IMemberRemarkTag): void {
    const modalRef = this.modalService.open(MemberRemarkTagDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.memberRemarkTag = memberRemarkTag;
  }
}
