import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { MemberRemarkTagComponent } from './member-remark-tag.component';
import { MemberRemarkTagDetailComponent } from './member-remark-tag-detail.component';
import { MemberRemarkTagUpdateComponent } from './member-remark-tag-update.component';
import { MemberRemarkTagDeleteDialogComponent } from './member-remark-tag-delete-dialog.component';
import { memberRemarkTagRoute } from './member-remark-tag.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(memberRemarkTagRoute)],
  declarations: [
    MemberRemarkTagComponent,
    MemberRemarkTagDetailComponent,
    MemberRemarkTagUpdateComponent,
    MemberRemarkTagDeleteDialogComponent,
  ],
  entryComponents: [MemberRemarkTagDeleteDialogComponent],
})
export class MisbeMemberRemarkTagModule {}
