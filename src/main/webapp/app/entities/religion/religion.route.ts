import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IReligion, Religion } from 'app/shared/model/religion.model';
import { ReligionService } from './religion.service';
import { ReligionComponent } from './religion.component';
import { ReligionDetailComponent } from './religion-detail.component';
import { ReligionUpdateComponent } from './religion-update.component';

@Injectable({ providedIn: 'root' })
export class ReligionResolve implements Resolve<IReligion> {
  constructor(private service: ReligionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IReligion> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((religion: HttpResponse<Religion>) => {
          if (religion.body) {
            return of(religion.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Religion());
  }
}

export const religionRoute: Routes = [
  {
    path: '',
    component: ReligionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Religions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ReligionDetailComponent,
    resolve: {
      religion: ReligionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Religions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ReligionUpdateComponent,
    resolve: {
      religion: ReligionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Religions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ReligionUpdateComponent,
    resolve: {
      religion: ReligionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Religions',
    },
    canActivate: [UserRouteAccessService],
  },
];
