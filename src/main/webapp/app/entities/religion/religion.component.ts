import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IReligion } from 'app/shared/model/religion.model';
import { ReligionService } from './religion.service';
import { ReligionDeleteDialogComponent } from './religion-delete-dialog.component';

@Component({
  selector: 'jhi-religion',
  templateUrl: './religion.component.html',
})
export class ReligionComponent implements OnInit, OnDestroy {
  religions?: IReligion[];
  eventSubscriber?: Subscription;

  constructor(protected religionService: ReligionService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.religionService.query().subscribe((res: HttpResponse<IReligion[]>) => (this.religions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInReligions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IReligion): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInReligions(): void {
    this.eventSubscriber = this.eventManager.subscribe('religionListModification', () => this.loadAll());
  }

  delete(religion: IReligion): void {
    const modalRef = this.modalService.open(ReligionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.religion = religion;
  }
}
