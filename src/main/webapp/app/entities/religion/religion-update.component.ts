import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IReligion, Religion } from 'app/shared/model/religion.model';
import { ReligionService } from './religion.service';

@Component({
  selector: 'jhi-religion-update',
  templateUrl: './religion-update.component.html',
})
export class ReligionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(protected religionService: ReligionService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ religion }) => {
      this.updateForm(religion);
    });
  }

  updateForm(religion: IReligion): void {
    this.editForm.patchValue({
      id: religion.id,
      name: religion.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const religion = this.createFromForm();
    if (religion.id !== undefined) {
      this.subscribeToSaveResponse(this.religionService.update(religion));
    } else {
      this.subscribeToSaveResponse(this.religionService.create(religion));
    }
  }

  private createFromForm(): IReligion {
    return {
      ...new Religion(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReligion>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
