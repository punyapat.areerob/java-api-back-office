import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IReligion } from 'app/shared/model/religion.model';
import { ReligionService } from './religion.service';

@Component({
  templateUrl: './religion-delete-dialog.component.html',
})
export class ReligionDeleteDialogComponent {
  religion?: IReligion;

  constructor(protected religionService: ReligionService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.religionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('religionListModification');
      this.activeModal.close();
    });
  }
}
