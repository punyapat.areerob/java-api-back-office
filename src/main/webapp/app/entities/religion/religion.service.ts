import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IReligion } from 'app/shared/model/religion.model';

type EntityResponseType = HttpResponse<IReligion>;
type EntityArrayResponseType = HttpResponse<IReligion[]>;

@Injectable({ providedIn: 'root' })
export class ReligionService {
  public resourceUrl = SERVER_API_URL + 'api/religions';

  constructor(protected http: HttpClient) {}

  create(religion: IReligion): Observable<EntityResponseType> {
    return this.http.post<IReligion>(this.resourceUrl, religion, { observe: 'response' });
  }

  update(religion: IReligion): Observable<EntityResponseType> {
    return this.http.put<IReligion>(this.resourceUrl, religion, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IReligion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IReligion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
