import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IReligion } from 'app/shared/model/religion.model';

@Component({
  selector: 'jhi-religion-detail',
  templateUrl: './religion-detail.component.html',
})
export class ReligionDetailComponent implements OnInit {
  religion: IReligion | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ religion }) => (this.religion = religion));
  }

  previousState(): void {
    window.history.back();
  }
}
