import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ReligionComponent } from './religion.component';
import { ReligionDetailComponent } from './religion-detail.component';
import { ReligionUpdateComponent } from './religion-update.component';
import { ReligionDeleteDialogComponent } from './religion-delete-dialog.component';
import { religionRoute } from './religion.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(religionRoute)],
  declarations: [ReligionComponent, ReligionDetailComponent, ReligionUpdateComponent, ReligionDeleteDialogComponent],
  entryComponents: [ReligionDeleteDialogComponent],
})
export class MisbeReligionModule {}
