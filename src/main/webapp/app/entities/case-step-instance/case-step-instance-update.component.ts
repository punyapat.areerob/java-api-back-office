import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICaseStepInstance, CaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from './case-step-instance.service';
import { ITeam } from 'app/shared/model/team.model';
import { TeamService } from 'app/entities/team/team.service';
import { IStepOwner } from 'app/shared/model/step-owner.model';
import { StepOwnerService } from 'app/entities/step-owner/step-owner.service';
import { ICaseStepState } from 'app/shared/model/case-step-state.model';
import { CaseStepStateService } from 'app/entities/case-step-state/case-step-state.service';

type SelectableEntity = ITeam | IStepOwner | ICaseStepState;

@Component({
  selector: 'jhi-case-step-instance-update',
  templateUrl: './case-step-instance-update.component.html',
})
export class CaseStepInstanceUpdateComponent implements OnInit {
  isSaving = false;
  teams: ITeam[] = [];
  stepowners: IStepOwner[] = [];
  casestepstates: ICaseStepState[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    stepId: [],
    caseInstanceId: [],
    begin: [],
    team: [],
    stepOwner: [],
    state: [],
  });

  constructor(
    protected caseStepInstanceService: CaseStepInstanceService,
    protected teamService: TeamService,
    protected stepOwnerService: StepOwnerService,
    protected caseStepStateService: CaseStepStateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseStepInstance }) => {
      this.updateForm(caseStepInstance);

      this.teamService
        .query({ filter: 'casestepinstance-is-null' })
        .pipe(
          map((res: HttpResponse<ITeam[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ITeam[]) => {
          if (!caseStepInstance.team || !caseStepInstance.team.id) {
            this.teams = resBody;
          } else {
            this.teamService
              .find(caseStepInstance.team.id)
              .pipe(
                map((subRes: HttpResponse<ITeam>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ITeam[]) => (this.teams = concatRes));
          }
        });

      this.stepOwnerService
        .query({ filter: 'casestepinstance-is-null' })
        .pipe(
          map((res: HttpResponse<IStepOwner[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IStepOwner[]) => {
          if (!caseStepInstance.stepOwner || !caseStepInstance.stepOwner.id) {
            this.stepowners = resBody;
          } else {
            this.stepOwnerService
              .find(caseStepInstance.stepOwner.id)
              .pipe(
                map((subRes: HttpResponse<IStepOwner>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IStepOwner[]) => (this.stepowners = concatRes));
          }
        });

      this.caseStepStateService.query().subscribe((res: HttpResponse<ICaseStepState[]>) => (this.casestepstates = res.body || []));
    });
  }

  updateForm(caseStepInstance: ICaseStepInstance): void {
    this.editForm.patchValue({
      id: caseStepInstance.id,
      name: caseStepInstance.name,
      stepId: caseStepInstance.stepId,
      caseInstanceId: caseStepInstance.caseInstanceId,
      begin: caseStepInstance.begin,
      team: caseStepInstance.team,
      stepOwner: caseStepInstance.stepOwner,
      state: caseStepInstance.state,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseStepInstance = this.createFromForm();
    if (caseStepInstance.id !== undefined) {
      this.subscribeToSaveResponse(this.caseStepInstanceService.update(caseStepInstance));
    } else {
      this.subscribeToSaveResponse(this.caseStepInstanceService.create(caseStepInstance));
    }
  }

  private createFromForm(): ICaseStepInstance {
    return {
      ...new CaseStepInstance(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      stepId: this.editForm.get(['stepId'])!.value,
      caseInstanceId: this.editForm.get(['caseInstanceId'])!.value,
      begin: this.editForm.get(['begin'])!.value,
      team: this.editForm.get(['team'])!.value,
      stepOwner: this.editForm.get(['stepOwner'])!.value,
      state: this.editForm.get(['state'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseStepInstance>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
