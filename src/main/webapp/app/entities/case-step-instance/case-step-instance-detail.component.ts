import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';

@Component({
  selector: 'jhi-case-step-instance-detail',
  templateUrl: './case-step-instance-detail.component.html',
})
export class CaseStepInstanceDetailComponent implements OnInit {
  caseStepInstance: ICaseStepInstance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseStepInstance }) => (this.caseStepInstance = caseStepInstance));
  }

  previousState(): void {
    window.history.back();
  }
}
