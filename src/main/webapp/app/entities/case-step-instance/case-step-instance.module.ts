import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseStepInstanceComponent } from './case-step-instance.component';
import { CaseStepInstanceDetailComponent } from './case-step-instance-detail.component';
import { CaseStepInstanceUpdateComponent } from './case-step-instance-update.component';
import { CaseStepInstanceDeleteDialogComponent } from './case-step-instance-delete-dialog.component';
import { caseStepInstanceRoute } from './case-step-instance.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseStepInstanceRoute)],
  declarations: [
    CaseStepInstanceComponent,
    CaseStepInstanceDetailComponent,
    CaseStepInstanceUpdateComponent,
    CaseStepInstanceDeleteDialogComponent,
  ],
  entryComponents: [CaseStepInstanceDeleteDialogComponent],
})
export class MisbeCaseStepInstanceModule {}
