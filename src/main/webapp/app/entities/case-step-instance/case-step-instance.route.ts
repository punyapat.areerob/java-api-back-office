import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaseStepInstance, CaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from './case-step-instance.service';
import { CaseStepInstanceComponent } from './case-step-instance.component';
import { CaseStepInstanceDetailComponent } from './case-step-instance-detail.component';
import { CaseStepInstanceUpdateComponent } from './case-step-instance-update.component';

@Injectable({ providedIn: 'root' })
export class CaseStepInstanceResolve implements Resolve<ICaseStepInstance> {
  constructor(private service: CaseStepInstanceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaseStepInstance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caseStepInstance: HttpResponse<CaseStepInstance>) => {
          if (caseStepInstance.body) {
            return of(caseStepInstance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaseStepInstance());
  }
}

export const caseStepInstanceRoute: Routes = [
  {
    path: '',
    component: CaseStepInstanceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaseStepInstanceDetailComponent,
    resolve: {
      caseStepInstance: CaseStepInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaseStepInstanceUpdateComponent,
    resolve: {
      caseStepInstance: CaseStepInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaseStepInstanceUpdateComponent,
    resolve: {
      caseStepInstance: CaseStepInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepInstances',
    },
    canActivate: [UserRouteAccessService],
  },
];
