import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from './case-step-instance.service';
import { CaseStepInstanceDeleteDialogComponent } from './case-step-instance-delete-dialog.component';

@Component({
  selector: 'jhi-case-step-instance',
  templateUrl: './case-step-instance.component.html',
})
export class CaseStepInstanceComponent implements OnInit, OnDestroy {
  caseStepInstances?: ICaseStepInstance[];
  eventSubscriber?: Subscription;

  constructor(
    protected caseStepInstanceService: CaseStepInstanceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.caseStepInstanceService.query().subscribe((res: HttpResponse<ICaseStepInstance[]>) => (this.caseStepInstances = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseStepInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseStepInstance): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseStepInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseStepInstanceListModification', () => this.loadAll());
  }

  delete(caseStepInstance: ICaseStepInstance): void {
    const modalRef = this.modalService.open(CaseStepInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseStepInstance = caseStepInstance;
  }
}
