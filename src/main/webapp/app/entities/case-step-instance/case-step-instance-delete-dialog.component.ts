import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from './case-step-instance.service';

@Component({
  templateUrl: './case-step-instance-delete-dialog.component.html',
})
export class CaseStepInstanceDeleteDialogComponent {
  caseStepInstance?: ICaseStepInstance;

  constructor(
    protected caseStepInstanceService: CaseStepInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseStepInstanceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseStepInstanceListModification');
      this.activeModal.close();
    });
  }
}
