import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVendorsContact, VendorsContact } from 'app/shared/model/vendors-contact.model';
import { VendorsContactService } from './vendors-contact.service';
import { IVendor } from 'app/shared/model/vendor.model';
import { VendorService } from 'app/entities/vendor/vendor.service';
import { IShop } from 'app/shared/model/shop.model';
import { ShopService } from 'app/entities/shop/shop.service';

type SelectableEntity = IVendor | IShop;

@Component({
  selector: 'jhi-vendors-contact-update',
  templateUrl: './vendors-contact-update.component.html',
})
export class VendorsContactUpdateComponent implements OnInit {
  isSaving = false;
  vendors: IVendor[] = [];
  shops: IShop[] = [];

  editForm = this.fb.group({
    id: [],
    title: [],
    firstName: [],
    lastName: [],
    mobile: [],
    email: [],
    businessTitle: [],
    contactTagId: [],
    contactTagName: [],
    active: [],
    vendor: [],
    shop: [],
  });

  constructor(
    protected vendorsContactService: VendorsContactService,
    protected vendorService: VendorService,
    protected shopService: ShopService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vendorsContact }) => {
      this.updateForm(vendorsContact);

      this.vendorService.query().subscribe((res: HttpResponse<IVendor[]>) => (this.vendors = res.body || []));

      this.shopService.query().subscribe((res: HttpResponse<IShop[]>) => (this.shops = res.body || []));
    });
  }

  updateForm(vendorsContact: IVendorsContact): void {
    this.editForm.patchValue({
      id: vendorsContact.id,
      title: vendorsContact.title,
      firstName: vendorsContact.firstName,
      lastName: vendorsContact.lastName,
      mobile: vendorsContact.mobile,
      email: vendorsContact.email,
      businessTitle: vendorsContact.businessTitle,
      contactTagId: vendorsContact.contactTagId,
      contactTagName: vendorsContact.contactTagName,
      active: vendorsContact.active,
      vendor: vendorsContact.vendor,
      shop: vendorsContact.shop,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const vendorsContact = this.createFromForm();
    if (vendorsContact.id !== undefined) {
      this.subscribeToSaveResponse(this.vendorsContactService.update(vendorsContact));
    } else {
      this.subscribeToSaveResponse(this.vendorsContactService.create(vendorsContact));
    }
  }

  private createFromForm(): IVendorsContact {
    return {
      ...new VendorsContact(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      mobile: this.editForm.get(['mobile'])!.value,
      email: this.editForm.get(['email'])!.value,
      businessTitle: this.editForm.get(['businessTitle'])!.value,
      contactTagId: this.editForm.get(['contactTagId'])!.value,
      contactTagName: this.editForm.get(['contactTagName'])!.value,
      active: this.editForm.get(['active'])!.value,
      vendor: this.editForm.get(['vendor'])!.value,
      shop: this.editForm.get(['shop'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVendorsContact>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
