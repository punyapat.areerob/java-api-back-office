import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVendorsContact } from 'app/shared/model/vendors-contact.model';

type EntityResponseType = HttpResponse<IVendorsContact>;
type EntityArrayResponseType = HttpResponse<IVendorsContact[]>;

@Injectable({ providedIn: 'root' })
export class VendorsContactService {
  public resourceUrl = SERVER_API_URL + 'api/vendors-contacts';

  constructor(protected http: HttpClient) {}

  create(vendorsContact: IVendorsContact): Observable<EntityResponseType> {
    return this.http.post<IVendorsContact>(this.resourceUrl, vendorsContact, { observe: 'response' });
  }

  update(vendorsContact: IVendorsContact): Observable<EntityResponseType> {
    return this.http.put<IVendorsContact>(this.resourceUrl, vendorsContact, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IVendorsContact>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVendorsContact[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
