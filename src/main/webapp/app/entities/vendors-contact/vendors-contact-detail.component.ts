import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVendorsContact } from 'app/shared/model/vendors-contact.model';

@Component({
  selector: 'jhi-vendors-contact-detail',
  templateUrl: './vendors-contact-detail.component.html',
})
export class VendorsContactDetailComponent implements OnInit {
  vendorsContact: IVendorsContact | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ vendorsContact }) => (this.vendorsContact = vendorsContact));
  }

  previousState(): void {
    window.history.back();
  }
}
