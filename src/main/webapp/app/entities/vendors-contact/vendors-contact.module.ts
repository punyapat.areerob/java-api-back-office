import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { VendorsContactComponent } from './vendors-contact.component';
import { VendorsContactDetailComponent } from './vendors-contact-detail.component';
import { VendorsContactUpdateComponent } from './vendors-contact-update.component';
import { VendorsContactDeleteDialogComponent } from './vendors-contact-delete-dialog.component';
import { vendorsContactRoute } from './vendors-contact.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(vendorsContactRoute)],
  declarations: [
    VendorsContactComponent,
    VendorsContactDetailComponent,
    VendorsContactUpdateComponent,
    VendorsContactDeleteDialogComponent,
  ],
  entryComponents: [VendorsContactDeleteDialogComponent],
})
export class MisbeVendorsContactModule {}
