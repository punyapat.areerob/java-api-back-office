import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVendorsContact, VendorsContact } from 'app/shared/model/vendors-contact.model';
import { VendorsContactService } from './vendors-contact.service';
import { VendorsContactComponent } from './vendors-contact.component';
import { VendorsContactDetailComponent } from './vendors-contact-detail.component';
import { VendorsContactUpdateComponent } from './vendors-contact-update.component';

@Injectable({ providedIn: 'root' })
export class VendorsContactResolve implements Resolve<IVendorsContact> {
  constructor(private service: VendorsContactService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVendorsContact> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((vendorsContact: HttpResponse<VendorsContact>) => {
          if (vendorsContact.body) {
            return of(vendorsContact.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VendorsContact());
  }
}

export const vendorsContactRoute: Routes = [
  {
    path: '',
    component: VendorsContactComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorsContacts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VendorsContactDetailComponent,
    resolve: {
      vendorsContact: VendorsContactResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorsContacts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VendorsContactUpdateComponent,
    resolve: {
      vendorsContact: VendorsContactResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorsContacts',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VendorsContactUpdateComponent,
    resolve: {
      vendorsContact: VendorsContactResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VendorsContacts',
    },
    canActivate: [UserRouteAccessService],
  },
];
