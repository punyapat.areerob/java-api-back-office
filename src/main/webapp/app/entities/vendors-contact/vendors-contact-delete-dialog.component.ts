import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVendorsContact } from 'app/shared/model/vendors-contact.model';
import { VendorsContactService } from './vendors-contact.service';

@Component({
  templateUrl: './vendors-contact-delete-dialog.component.html',
})
export class VendorsContactDeleteDialogComponent {
  vendorsContact?: IVendorsContact;

  constructor(
    protected vendorsContactService: VendorsContactService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.vendorsContactService.delete(id).subscribe(() => {
      this.eventManager.broadcast('vendorsContactListModification');
      this.activeModal.close();
    });
  }
}
