import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVendorsContact } from 'app/shared/model/vendors-contact.model';
import { VendorsContactService } from './vendors-contact.service';
import { VendorsContactDeleteDialogComponent } from './vendors-contact-delete-dialog.component';

@Component({
  selector: 'jhi-vendors-contact',
  templateUrl: './vendors-contact.component.html',
})
export class VendorsContactComponent implements OnInit, OnDestroy {
  vendorsContacts?: IVendorsContact[];
  eventSubscriber?: Subscription;

  constructor(
    protected vendorsContactService: VendorsContactService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.vendorsContactService.query().subscribe((res: HttpResponse<IVendorsContact[]>) => (this.vendorsContacts = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInVendorsContacts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IVendorsContact): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInVendorsContacts(): void {
    this.eventSubscriber = this.eventManager.subscribe('vendorsContactListModification', () => this.loadAll());
  }

  delete(vendorsContact: IVendorsContact): void {
    const modalRef = this.modalService.open(VendorsContactDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.vendorsContact = vendorsContact;
  }
}
