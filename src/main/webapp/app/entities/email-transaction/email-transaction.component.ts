import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmailTransaction } from 'app/shared/model/email-transaction.model';
import { EmailTransactionService } from './email-transaction.service';
import { EmailTransactionDeleteDialogComponent } from './email-transaction-delete-dialog.component';

@Component({
  selector: 'jhi-email-transaction',
  templateUrl: './email-transaction.component.html',
})
export class EmailTransactionComponent implements OnInit, OnDestroy {
  emailTransactions?: IEmailTransaction[];
  eventSubscriber?: Subscription;

  constructor(
    protected emailTransactionService: EmailTransactionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.emailTransactionService.query().subscribe((res: HttpResponse<IEmailTransaction[]>) => (this.emailTransactions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEmailTransactions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IEmailTransaction): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEmailTransactions(): void {
    this.eventSubscriber = this.eventManager.subscribe('emailTransactionListModification', () => this.loadAll());
  }

  delete(emailTransaction: IEmailTransaction): void {
    const modalRef = this.modalService.open(EmailTransactionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.emailTransaction = emailTransaction;
  }
}
