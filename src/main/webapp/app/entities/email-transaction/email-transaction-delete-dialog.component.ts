import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEmailTransaction } from 'app/shared/model/email-transaction.model';
import { EmailTransactionService } from './email-transaction.service';

@Component({
  templateUrl: './email-transaction-delete-dialog.component.html',
})
export class EmailTransactionDeleteDialogComponent {
  emailTransaction?: IEmailTransaction;

  constructor(
    protected emailTransactionService: EmailTransactionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.emailTransactionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('emailTransactionListModification');
      this.activeModal.close();
    });
  }
}
