import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { EmailTransactionComponent } from './email-transaction.component';
import { EmailTransactionDetailComponent } from './email-transaction-detail.component';
import { EmailTransactionUpdateComponent } from './email-transaction-update.component';
import { EmailTransactionDeleteDialogComponent } from './email-transaction-delete-dialog.component';
import { emailTransactionRoute } from './email-transaction.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(emailTransactionRoute)],
  declarations: [
    EmailTransactionComponent,
    EmailTransactionDetailComponent,
    EmailTransactionUpdateComponent,
    EmailTransactionDeleteDialogComponent,
  ],
  entryComponents: [EmailTransactionDeleteDialogComponent],
})
export class MisbeEmailTransactionModule {}
