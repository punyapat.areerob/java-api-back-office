import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmailTransaction } from 'app/shared/model/email-transaction.model';

@Component({
  selector: 'jhi-email-transaction-detail',
  templateUrl: './email-transaction-detail.component.html',
})
export class EmailTransactionDetailComponent implements OnInit {
  emailTransaction: IEmailTransaction | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailTransaction }) => (this.emailTransaction = emailTransaction));
  }

  previousState(): void {
    window.history.back();
  }
}
