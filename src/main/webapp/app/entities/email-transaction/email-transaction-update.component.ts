import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IEmailTransaction, EmailTransaction } from 'app/shared/model/email-transaction.model';
import { EmailTransactionService } from './email-transaction.service';

@Component({
  selector: 'jhi-email-transaction-update',
  templateUrl: './email-transaction-update.component.html',
})
export class EmailTransactionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    templateName: [],
    to: [],
    subject: [],
    body: [],
    sendDate: [],
    status: [],
    error: [],
  });

  constructor(
    protected emailTransactionService: EmailTransactionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailTransaction }) => {
      if (!emailTransaction.id) {
        const today = moment().startOf('day');
        emailTransaction.sendDate = today;
      }

      this.updateForm(emailTransaction);
    });
  }

  updateForm(emailTransaction: IEmailTransaction): void {
    this.editForm.patchValue({
      id: emailTransaction.id,
      templateName: emailTransaction.templateName,
      to: emailTransaction.to,
      subject: emailTransaction.subject,
      body: emailTransaction.body,
      sendDate: emailTransaction.sendDate ? emailTransaction.sendDate.format(DATE_TIME_FORMAT) : null,
      status: emailTransaction.status,
      error: emailTransaction.error,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const emailTransaction = this.createFromForm();
    if (emailTransaction.id !== undefined) {
      this.subscribeToSaveResponse(this.emailTransactionService.update(emailTransaction));
    } else {
      this.subscribeToSaveResponse(this.emailTransactionService.create(emailTransaction));
    }
  }

  private createFromForm(): IEmailTransaction {
    return {
      ...new EmailTransaction(),
      id: this.editForm.get(['id'])!.value,
      templateName: this.editForm.get(['templateName'])!.value,
      to: this.editForm.get(['to'])!.value,
      subject: this.editForm.get(['subject'])!.value,
      body: this.editForm.get(['body'])!.value,
      sendDate: this.editForm.get(['sendDate'])!.value ? moment(this.editForm.get(['sendDate'])!.value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status'])!.value,
      error: this.editForm.get(['error'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmailTransaction>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
