import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEmailTransaction, EmailTransaction } from 'app/shared/model/email-transaction.model';
import { EmailTransactionService } from './email-transaction.service';
import { EmailTransactionComponent } from './email-transaction.component';
import { EmailTransactionDetailComponent } from './email-transaction-detail.component';
import { EmailTransactionUpdateComponent } from './email-transaction-update.component';

@Injectable({ providedIn: 'root' })
export class EmailTransactionResolve implements Resolve<IEmailTransaction> {
  constructor(private service: EmailTransactionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEmailTransaction> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((emailTransaction: HttpResponse<EmailTransaction>) => {
          if (emailTransaction.body) {
            return of(emailTransaction.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EmailTransaction());
  }
}

export const emailTransactionRoute: Routes = [
  {
    path: '',
    component: EmailTransactionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTransactions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmailTransactionDetailComponent,
    resolve: {
      emailTransaction: EmailTransactionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTransactions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmailTransactionUpdateComponent,
    resolve: {
      emailTransaction: EmailTransactionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTransactions',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmailTransactionUpdateComponent,
    resolve: {
      emailTransaction: EmailTransactionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTransactions',
    },
    canActivate: [UserRouteAccessService],
  },
];
