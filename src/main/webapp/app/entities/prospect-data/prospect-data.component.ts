import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProspectData } from 'app/shared/model/prospect-data.model';
import { ProspectDataService } from './prospect-data.service';
import { ProspectDataDeleteDialogComponent } from './prospect-data-delete-dialog.component';

@Component({
  selector: 'jhi-prospect-data',
  templateUrl: './prospect-data.component.html',
})
export class ProspectDataComponent implements OnInit, OnDestroy {
  prospectData?: IProspectData[];
  eventSubscriber?: Subscription;

  constructor(
    protected prospectDataService: ProspectDataService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.prospectDataService.query().subscribe((res: HttpResponse<IProspectData[]>) => (this.prospectData = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProspectData();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProspectData): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProspectData(): void {
    this.eventSubscriber = this.eventManager.subscribe('prospectDataListModification', () => this.loadAll());
  }

  delete(prospectData: IProspectData): void {
    const modalRef = this.modalService.open(ProspectDataDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.prospectData = prospectData;
  }
}
