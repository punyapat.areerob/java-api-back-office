import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ProspectDataComponent } from './prospect-data.component';
import { ProspectDataDetailComponent } from './prospect-data-detail.component';
import { ProspectDataUpdateComponent } from './prospect-data-update.component';
import { ProspectDataDeleteDialogComponent } from './prospect-data-delete-dialog.component';
import { prospectDataRoute } from './prospect-data.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(prospectDataRoute)],
  declarations: [ProspectDataComponent, ProspectDataDetailComponent, ProspectDataUpdateComponent, ProspectDataDeleteDialogComponent],
  entryComponents: [ProspectDataDeleteDialogComponent],
})
export class MisbeProspectDataModule {}
