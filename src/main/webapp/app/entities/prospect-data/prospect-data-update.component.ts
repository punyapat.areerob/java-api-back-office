import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProspectData, ProspectData } from 'app/shared/model/prospect-data.model';
import { ProspectDataService } from './prospect-data.service';

@Component({
  selector: 'jhi-prospect-data-update',
  templateUrl: './prospect-data-update.component.html',
})
export class ProspectDataUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    title: [],
    givenName: [],
    middleName: [],
    surName: [],
    areaCode: [],
    contactNo: [],
    passportNo: [],
    passportExpireDate: [],
    passportDateOfBirth: [],
    agreePda: [],
    allowContactMe: [],
  });

  constructor(protected prospectDataService: ProspectDataService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prospectData }) => {
      this.updateForm(prospectData);
    });
  }

  updateForm(prospectData: IProspectData): void {
    this.editForm.patchValue({
      id: prospectData.id,
      title: prospectData.title,
      givenName: prospectData.givenName,
      middleName: prospectData.middleName,
      surName: prospectData.surName,
      areaCode: prospectData.areaCode,
      contactNo: prospectData.contactNo,
      passportNo: prospectData.passportNo,
      passportExpireDate: prospectData.passportExpireDate,
      passportDateOfBirth: prospectData.passportDateOfBirth,
      agreePda: prospectData.agreePda,
      allowContactMe: prospectData.allowContactMe,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const prospectData = this.createFromForm();
    if (prospectData.id !== undefined) {
      this.subscribeToSaveResponse(this.prospectDataService.update(prospectData));
    } else {
      this.subscribeToSaveResponse(this.prospectDataService.create(prospectData));
    }
  }

  private createFromForm(): IProspectData {
    return {
      ...new ProspectData(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      givenName: this.editForm.get(['givenName'])!.value,
      middleName: this.editForm.get(['middleName'])!.value,
      surName: this.editForm.get(['surName'])!.value,
      areaCode: this.editForm.get(['areaCode'])!.value,
      contactNo: this.editForm.get(['contactNo'])!.value,
      passportNo: this.editForm.get(['passportNo'])!.value,
      passportExpireDate: this.editForm.get(['passportExpireDate'])!.value,
      passportDateOfBirth: this.editForm.get(['passportDateOfBirth'])!.value,
      agreePda: this.editForm.get(['agreePda'])!.value,
      allowContactMe: this.editForm.get(['allowContactMe'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProspectData>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
