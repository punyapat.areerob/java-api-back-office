import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProspectData } from 'app/shared/model/prospect-data.model';
import { ProspectDataService } from './prospect-data.service';

@Component({
  templateUrl: './prospect-data-delete-dialog.component.html',
})
export class ProspectDataDeleteDialogComponent {
  prospectData?: IProspectData;

  constructor(
    protected prospectDataService: ProspectDataService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.prospectDataService.delete(id).subscribe(() => {
      this.eventManager.broadcast('prospectDataListModification');
      this.activeModal.close();
    });
  }
}
