import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProspectData } from 'app/shared/model/prospect-data.model';

type EntityResponseType = HttpResponse<IProspectData>;
type EntityArrayResponseType = HttpResponse<IProspectData[]>;

@Injectable({ providedIn: 'root' })
export class ProspectDataService {
  public resourceUrl = SERVER_API_URL + 'api/prospect-data';

  constructor(protected http: HttpClient) {}

  create(prospectData: IProspectData): Observable<EntityResponseType> {
    return this.http.post<IProspectData>(this.resourceUrl, prospectData, { observe: 'response' });
  }

  update(prospectData: IProspectData): Observable<EntityResponseType> {
    return this.http.put<IProspectData>(this.resourceUrl, prospectData, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IProspectData>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProspectData[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
