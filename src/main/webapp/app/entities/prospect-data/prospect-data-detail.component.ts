import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProspectData } from 'app/shared/model/prospect-data.model';

@Component({
  selector: 'jhi-prospect-data-detail',
  templateUrl: './prospect-data-detail.component.html',
})
export class ProspectDataDetailComponent implements OnInit {
  prospectData: IProspectData | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ prospectData }) => (this.prospectData = prospectData));
  }

  previousState(): void {
    window.history.back();
  }
}
