import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProspectData, ProspectData } from 'app/shared/model/prospect-data.model';
import { ProspectDataService } from './prospect-data.service';
import { ProspectDataComponent } from './prospect-data.component';
import { ProspectDataDetailComponent } from './prospect-data-detail.component';
import { ProspectDataUpdateComponent } from './prospect-data-update.component';

@Injectable({ providedIn: 'root' })
export class ProspectDataResolve implements Resolve<IProspectData> {
  constructor(private service: ProspectDataService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProspectData> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((prospectData: HttpResponse<ProspectData>) => {
          if (prospectData.body) {
            return of(prospectData.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProspectData());
  }
}

export const prospectDataRoute: Routes = [
  {
    path: '',
    component: ProspectDataComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProspectData',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProspectDataDetailComponent,
    resolve: {
      prospectData: ProspectDataResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProspectData',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProspectDataUpdateComponent,
    resolve: {
      prospectData: ProspectDataResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProspectData',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProspectDataUpdateComponent,
    resolve: {
      prospectData: ProspectDataResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProspectData',
    },
    canActivate: [UserRouteAccessService],
  },
];
