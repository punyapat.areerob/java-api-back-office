import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BookingStatusComponent } from './booking-status.component';
import { BookingStatusDetailComponent } from './booking-status-detail.component';
import { BookingStatusUpdateComponent } from './booking-status-update.component';
import { BookingStatusDeleteDialogComponent } from './booking-status-delete-dialog.component';
import { bookingStatusRoute } from './booking-status.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(bookingStatusRoute)],
  declarations: [BookingStatusComponent, BookingStatusDetailComponent, BookingStatusUpdateComponent, BookingStatusDeleteDialogComponent],
  entryComponents: [BookingStatusDeleteDialogComponent],
})
export class MisbeBookingStatusModule {}
