import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBookingStatus, BookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from './booking-status.service';
import { BookingStatusComponent } from './booking-status.component';
import { BookingStatusDetailComponent } from './booking-status-detail.component';
import { BookingStatusUpdateComponent } from './booking-status-update.component';

@Injectable({ providedIn: 'root' })
export class BookingStatusResolve implements Resolve<IBookingStatus> {
  constructor(private service: BookingStatusService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookingStatus> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bookingStatus: HttpResponse<BookingStatus>) => {
          if (bookingStatus.body) {
            return of(bookingStatus.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookingStatus());
  }
}

export const bookingStatusRoute: Routes = [
  {
    path: '',
    component: BookingStatusComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingStatuses',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookingStatusDetailComponent,
    resolve: {
      bookingStatus: BookingStatusResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingStatuses',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookingStatusUpdateComponent,
    resolve: {
      bookingStatus: BookingStatusResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingStatuses',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookingStatusUpdateComponent,
    resolve: {
      bookingStatus: BookingStatusResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BookingStatuses',
    },
    canActivate: [UserRouteAccessService],
  },
];
