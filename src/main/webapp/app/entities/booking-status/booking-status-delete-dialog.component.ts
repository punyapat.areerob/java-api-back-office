import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from './booking-status.service';

@Component({
  templateUrl: './booking-status-delete-dialog.component.html',
})
export class BookingStatusDeleteDialogComponent {
  bookingStatus?: IBookingStatus;

  constructor(
    protected bookingStatusService: BookingStatusService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.bookingStatusService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bookingStatusListModification');
      this.activeModal.close();
    });
  }
}
