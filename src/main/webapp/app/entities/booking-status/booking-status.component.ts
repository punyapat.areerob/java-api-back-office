import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from './booking-status.service';
import { BookingStatusDeleteDialogComponent } from './booking-status-delete-dialog.component';

@Component({
  selector: 'jhi-booking-status',
  templateUrl: './booking-status.component.html',
})
export class BookingStatusComponent implements OnInit, OnDestroy {
  bookingStatuses?: IBookingStatus[];
  eventSubscriber?: Subscription;

  constructor(
    protected bookingStatusService: BookingStatusService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.bookingStatusService.query().subscribe((res: HttpResponse<IBookingStatus[]>) => (this.bookingStatuses = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBookingStatuses();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBookingStatus): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBookingStatuses(): void {
    this.eventSubscriber = this.eventManager.subscribe('bookingStatusListModification', () => this.loadAll());
  }

  delete(bookingStatus: IBookingStatus): void {
    const modalRef = this.modalService.open(BookingStatusDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.bookingStatus = bookingStatus;
  }
}
