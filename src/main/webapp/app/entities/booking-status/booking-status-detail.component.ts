import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBookingStatus } from 'app/shared/model/booking-status.model';

@Component({
  selector: 'jhi-booking-status-detail',
  templateUrl: './booking-status-detail.component.html',
})
export class BookingStatusDetailComponent implements OnInit {
  bookingStatus: IBookingStatus | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingStatus }) => (this.bookingStatus = bookingStatus));
  }

  previousState(): void {
    window.history.back();
  }
}
