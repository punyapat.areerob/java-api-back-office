import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBookingStatus } from 'app/shared/model/booking-status.model';

type EntityResponseType = HttpResponse<IBookingStatus>;
type EntityArrayResponseType = HttpResponse<IBookingStatus[]>;

@Injectable({ providedIn: 'root' })
export class BookingStatusService {
  public resourceUrl = SERVER_API_URL + 'api/booking-statuses';

  constructor(protected http: HttpClient) {}

  create(bookingStatus: IBookingStatus): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingStatus);
    return this.http
      .post<IBookingStatus>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bookingStatus: IBookingStatus): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bookingStatus);
    return this.http
      .put<IBookingStatus>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IBookingStatus>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBookingStatus[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(bookingStatus: IBookingStatus): IBookingStatus {
    const copy: IBookingStatus = Object.assign({}, bookingStatus, {
      createdAt: bookingStatus.createdAt && bookingStatus.createdAt.isValid() ? bookingStatus.createdAt.toJSON() : undefined,
      updatedAt: bookingStatus.updatedAt && bookingStatus.updatedAt.isValid() ? bookingStatus.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bookingStatus: IBookingStatus) => {
        bookingStatus.createdAt = bookingStatus.createdAt ? moment(bookingStatus.createdAt) : undefined;
        bookingStatus.updatedAt = bookingStatus.updatedAt ? moment(bookingStatus.updatedAt) : undefined;
      });
    }
    return res;
  }
}
