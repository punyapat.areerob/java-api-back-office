import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBookingStatus, BookingStatus } from 'app/shared/model/booking-status.model';
import { BookingStatusService } from './booking-status.service';

@Component({
  selector: 'jhi-booking-status-update',
  templateUrl: './booking-status-update.component.html',
})
export class BookingStatusUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    createdAt: [],
    updatedAt: [],
  });

  constructor(protected bookingStatusService: BookingStatusService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookingStatus }) => {
      if (!bookingStatus.id) {
        const today = moment().startOf('day');
        bookingStatus.createdAt = today;
        bookingStatus.updatedAt = today;
      }

      this.updateForm(bookingStatus);
    });
  }

  updateForm(bookingStatus: IBookingStatus): void {
    this.editForm.patchValue({
      id: bookingStatus.id,
      name: bookingStatus.name,
      createdAt: bookingStatus.createdAt ? bookingStatus.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: bookingStatus.updatedAt ? bookingStatus.updatedAt.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookingStatus = this.createFromForm();
    if (bookingStatus.id !== undefined) {
      this.subscribeToSaveResponse(this.bookingStatusService.update(bookingStatus));
    } else {
      this.subscribeToSaveResponse(this.bookingStatusService.create(bookingStatus));
    }
  }

  private createFromForm(): IBookingStatus {
    return {
      ...new BookingStatus(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookingStatus>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
