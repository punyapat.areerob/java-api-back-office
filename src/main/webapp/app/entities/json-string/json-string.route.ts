import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJsonString, JsonString } from 'app/shared/model/json-string.model';
import { JsonStringService } from './json-string.service';
import { JsonStringComponent } from './json-string.component';
import { JsonStringDetailComponent } from './json-string-detail.component';
import { JsonStringUpdateComponent } from './json-string-update.component';

@Injectable({ providedIn: 'root' })
export class JsonStringResolve implements Resolve<IJsonString> {
  constructor(private service: JsonStringService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJsonString> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jsonString: HttpResponse<JsonString>) => {
          if (jsonString.body) {
            return of(jsonString.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new JsonString());
  }
}

export const jsonStringRoute: Routes = [
  {
    path: '',
    component: JsonStringComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonStrings',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JsonStringDetailComponent,
    resolve: {
      jsonString: JsonStringResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonStrings',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JsonStringUpdateComponent,
    resolve: {
      jsonString: JsonStringResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonStrings',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JsonStringUpdateComponent,
    resolve: {
      jsonString: JsonStringResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonStrings',
    },
    canActivate: [UserRouteAccessService],
  },
];
