import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJsonString } from 'app/shared/model/json-string.model';
import { JsonStringService } from './json-string.service';
import { JsonStringDeleteDialogComponent } from './json-string-delete-dialog.component';

@Component({
  selector: 'jhi-json-string',
  templateUrl: './json-string.component.html',
})
export class JsonStringComponent implements OnInit, OnDestroy {
  jsonStrings?: IJsonString[];
  eventSubscriber?: Subscription;

  constructor(protected jsonStringService: JsonStringService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.jsonStringService.query().subscribe((res: HttpResponse<IJsonString[]>) => (this.jsonStrings = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJsonStrings();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJsonString): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJsonStrings(): void {
    this.eventSubscriber = this.eventManager.subscribe('jsonStringListModification', () => this.loadAll());
  }

  delete(jsonString: IJsonString): void {
    const modalRef = this.modalService.open(JsonStringDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jsonString = jsonString;
  }
}
