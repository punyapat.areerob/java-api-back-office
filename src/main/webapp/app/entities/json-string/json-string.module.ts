import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { JsonStringComponent } from './json-string.component';
import { JsonStringDetailComponent } from './json-string-detail.component';
import { JsonStringUpdateComponent } from './json-string-update.component';
import { JsonStringDeleteDialogComponent } from './json-string-delete-dialog.component';
import { jsonStringRoute } from './json-string.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(jsonStringRoute)],
  declarations: [JsonStringComponent, JsonStringDetailComponent, JsonStringUpdateComponent, JsonStringDeleteDialogComponent],
  entryComponents: [JsonStringDeleteDialogComponent],
})
export class MisbeJsonStringModule {}
