import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IJsonString, JsonString } from 'app/shared/model/json-string.model';
import { JsonStringService } from './json-string.service';

@Component({
  selector: 'jhi-json-string-update',
  templateUrl: './json-string-update.component.html',
})
export class JsonStringUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    key: [],
    value: [],
  });

  constructor(protected jsonStringService: JsonStringService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jsonString }) => {
      this.updateForm(jsonString);
    });
  }

  updateForm(jsonString: IJsonString): void {
    this.editForm.patchValue({
      id: jsonString.id,
      key: jsonString.key,
      value: jsonString.value,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jsonString = this.createFromForm();
    if (jsonString.id !== undefined) {
      this.subscribeToSaveResponse(this.jsonStringService.update(jsonString));
    } else {
      this.subscribeToSaveResponse(this.jsonStringService.create(jsonString));
    }
  }

  private createFromForm(): IJsonString {
    return {
      ...new JsonString(),
      id: this.editForm.get(['id'])!.value,
      key: this.editForm.get(['key'])!.value,
      value: this.editForm.get(['value'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJsonString>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
