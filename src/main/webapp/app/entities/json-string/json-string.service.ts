import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJsonString } from 'app/shared/model/json-string.model';

type EntityResponseType = HttpResponse<IJsonString>;
type EntityArrayResponseType = HttpResponse<IJsonString[]>;

@Injectable({ providedIn: 'root' })
export class JsonStringService {
  public resourceUrl = SERVER_API_URL + 'api/json-strings';

  constructor(protected http: HttpClient) {}

  create(jsonString: IJsonString): Observable<EntityResponseType> {
    return this.http.post<IJsonString>(this.resourceUrl, jsonString, { observe: 'response' });
  }

  update(jsonString: IJsonString): Observable<EntityResponseType> {
    return this.http.put<IJsonString>(this.resourceUrl, jsonString, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IJsonString>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJsonString[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
