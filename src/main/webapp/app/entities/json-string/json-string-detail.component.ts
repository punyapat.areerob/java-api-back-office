import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJsonString } from 'app/shared/model/json-string.model';

@Component({
  selector: 'jhi-json-string-detail',
  templateUrl: './json-string-detail.component.html',
})
export class JsonStringDetailComponent implements OnInit {
  jsonString: IJsonString | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jsonString }) => (this.jsonString = jsonString));
  }

  previousState(): void {
    window.history.back();
  }
}
