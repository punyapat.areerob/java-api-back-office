import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJsonString } from 'app/shared/model/json-string.model';
import { JsonStringService } from './json-string.service';

@Component({
  templateUrl: './json-string-delete-dialog.component.html',
})
export class JsonStringDeleteDialogComponent {
  jsonString?: IJsonString;

  constructor(
    protected jsonStringService: JsonStringService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.jsonStringService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jsonStringListModification');
      this.activeModal.close();
    });
  }
}
