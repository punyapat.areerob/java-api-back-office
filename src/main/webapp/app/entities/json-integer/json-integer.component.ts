import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJsonInteger } from 'app/shared/model/json-integer.model';
import { JsonIntegerService } from './json-integer.service';
import { JsonIntegerDeleteDialogComponent } from './json-integer-delete-dialog.component';

@Component({
  selector: 'jhi-json-integer',
  templateUrl: './json-integer.component.html',
})
export class JsonIntegerComponent implements OnInit, OnDestroy {
  jsonIntegers?: IJsonInteger[];
  eventSubscriber?: Subscription;

  constructor(
    protected jsonIntegerService: JsonIntegerService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.jsonIntegerService.query().subscribe((res: HttpResponse<IJsonInteger[]>) => (this.jsonIntegers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJsonIntegers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJsonInteger): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJsonIntegers(): void {
    this.eventSubscriber = this.eventManager.subscribe('jsonIntegerListModification', () => this.loadAll());
  }

  delete(jsonInteger: IJsonInteger): void {
    const modalRef = this.modalService.open(JsonIntegerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jsonInteger = jsonInteger;
  }
}
