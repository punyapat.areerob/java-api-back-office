import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJsonInteger } from 'app/shared/model/json-integer.model';

@Component({
  selector: 'jhi-json-integer-detail',
  templateUrl: './json-integer-detail.component.html',
})
export class JsonIntegerDetailComponent implements OnInit {
  jsonInteger: IJsonInteger | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jsonInteger }) => (this.jsonInteger = jsonInteger));
  }

  previousState(): void {
    window.history.back();
  }
}
