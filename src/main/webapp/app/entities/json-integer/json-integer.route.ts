import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJsonInteger, JsonInteger } from 'app/shared/model/json-integer.model';
import { JsonIntegerService } from './json-integer.service';
import { JsonIntegerComponent } from './json-integer.component';
import { JsonIntegerDetailComponent } from './json-integer-detail.component';
import { JsonIntegerUpdateComponent } from './json-integer-update.component';

@Injectable({ providedIn: 'root' })
export class JsonIntegerResolve implements Resolve<IJsonInteger> {
  constructor(private service: JsonIntegerService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJsonInteger> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jsonInteger: HttpResponse<JsonInteger>) => {
          if (jsonInteger.body) {
            return of(jsonInteger.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new JsonInteger());
  }
}

export const jsonIntegerRoute: Routes = [
  {
    path: '',
    component: JsonIntegerComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonIntegers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JsonIntegerDetailComponent,
    resolve: {
      jsonInteger: JsonIntegerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonIntegers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JsonIntegerUpdateComponent,
    resolve: {
      jsonInteger: JsonIntegerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonIntegers',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JsonIntegerUpdateComponent,
    resolve: {
      jsonInteger: JsonIntegerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JsonIntegers',
    },
    canActivate: [UserRouteAccessService],
  },
];
