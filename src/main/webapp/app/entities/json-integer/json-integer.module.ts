import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { JsonIntegerComponent } from './json-integer.component';
import { JsonIntegerDetailComponent } from './json-integer-detail.component';
import { JsonIntegerUpdateComponent } from './json-integer-update.component';
import { JsonIntegerDeleteDialogComponent } from './json-integer-delete-dialog.component';
import { jsonIntegerRoute } from './json-integer.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(jsonIntegerRoute)],
  declarations: [JsonIntegerComponent, JsonIntegerDetailComponent, JsonIntegerUpdateComponent, JsonIntegerDeleteDialogComponent],
  entryComponents: [JsonIntegerDeleteDialogComponent],
})
export class MisbeJsonIntegerModule {}
