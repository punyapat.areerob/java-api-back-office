import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJsonInteger } from 'app/shared/model/json-integer.model';

type EntityResponseType = HttpResponse<IJsonInteger>;
type EntityArrayResponseType = HttpResponse<IJsonInteger[]>;

@Injectable({ providedIn: 'root' })
export class JsonIntegerService {
  public resourceUrl = SERVER_API_URL + 'api/json-integers';

  constructor(protected http: HttpClient) {}

  create(jsonInteger: IJsonInteger): Observable<EntityResponseType> {
    return this.http.post<IJsonInteger>(this.resourceUrl, jsonInteger, { observe: 'response' });
  }

  update(jsonInteger: IJsonInteger): Observable<EntityResponseType> {
    return this.http.put<IJsonInteger>(this.resourceUrl, jsonInteger, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IJsonInteger>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJsonInteger[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
