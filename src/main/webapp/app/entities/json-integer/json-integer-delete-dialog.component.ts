import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJsonInteger } from 'app/shared/model/json-integer.model';
import { JsonIntegerService } from './json-integer.service';

@Component({
  templateUrl: './json-integer-delete-dialog.component.html',
})
export class JsonIntegerDeleteDialogComponent {
  jsonInteger?: IJsonInteger;

  constructor(
    protected jsonIntegerService: JsonIntegerService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.jsonIntegerService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jsonIntegerListModification');
      this.activeModal.close();
    });
  }
}
