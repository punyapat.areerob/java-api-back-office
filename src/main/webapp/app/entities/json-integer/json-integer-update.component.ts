import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IJsonInteger, JsonInteger } from 'app/shared/model/json-integer.model';
import { JsonIntegerService } from './json-integer.service';

@Component({
  selector: 'jhi-json-integer-update',
  templateUrl: './json-integer-update.component.html',
})
export class JsonIntegerUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    key: [],
    value: [],
  });

  constructor(protected jsonIntegerService: JsonIntegerService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jsonInteger }) => {
      this.updateForm(jsonInteger);
    });
  }

  updateForm(jsonInteger: IJsonInteger): void {
    this.editForm.patchValue({
      id: jsonInteger.id,
      key: jsonInteger.key,
      value: jsonInteger.value,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jsonInteger = this.createFromForm();
    if (jsonInteger.id !== undefined) {
      this.subscribeToSaveResponse(this.jsonIntegerService.update(jsonInteger));
    } else {
      this.subscribeToSaveResponse(this.jsonIntegerService.create(jsonInteger));
    }
  }

  private createFromForm(): IJsonInteger {
    return {
      ...new JsonInteger(),
      id: this.editForm.get(['id'])!.value,
      key: this.editForm.get(['key'])!.value,
      value: this.editForm.get(['value'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJsonInteger>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
