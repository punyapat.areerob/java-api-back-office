import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJobPositions } from 'app/shared/model/job-positions.model';

@Component({
  selector: 'jhi-job-positions-detail',
  templateUrl: './job-positions-detail.component.html',
})
export class JobPositionsDetailComponent implements OnInit {
  jobPositions: IJobPositions | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobPositions }) => (this.jobPositions = jobPositions));
  }

  previousState(): void {
    window.history.back();
  }
}
