import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJobPositions } from 'app/shared/model/job-positions.model';

type EntityResponseType = HttpResponse<IJobPositions>;
type EntityArrayResponseType = HttpResponse<IJobPositions[]>;

@Injectable({ providedIn: 'root' })
export class JobPositionsService {
  public resourceUrl = SERVER_API_URL + 'api/job-positions';

  constructor(protected http: HttpClient) {}

  create(jobPositions: IJobPositions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(jobPositions);
    return this.http
      .post<IJobPositions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(jobPositions: IJobPositions): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(jobPositions);
    return this.http
      .put<IJobPositions>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IJobPositions>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IJobPositions[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(jobPositions: IJobPositions): IJobPositions {
    const copy: IJobPositions = Object.assign({}, jobPositions, {
      from: jobPositions.from && jobPositions.from.isValid() ? jobPositions.from.toJSON() : undefined,
      to: jobPositions.to && jobPositions.to.isValid() ? jobPositions.to.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.from = res.body.from ? moment(res.body.from) : undefined;
      res.body.to = res.body.to ? moment(res.body.to) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((jobPositions: IJobPositions) => {
        jobPositions.from = jobPositions.from ? moment(jobPositions.from) : undefined;
        jobPositions.to = jobPositions.to ? moment(jobPositions.to) : undefined;
      });
    }
    return res;
  }
}
