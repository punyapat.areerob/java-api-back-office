import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IJobPositions, JobPositions } from 'app/shared/model/job-positions.model';
import { JobPositionsService } from './job-positions.service';

@Component({
  selector: 'jhi-job-positions-update',
  templateUrl: './job-positions-update.component.html',
})
export class JobPositionsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    teamId: [null, [Validators.required]],
    teamName: [null, [Validators.required]],
    jobDescriptionTemplateId: [],
    jobDescriptionTemplateName: [],
    jobTitle: [null, [Validators.required]],
    lineOfWork: [null, [Validators.required]],
    department: [null, [Validators.required]],
    educationBackground: [null, [Validators.required]],
    salary: [null, [Validators.required]],
    location: [null, [Validators.required]],
    summary: [null, [Validators.required]],
    mainReponsibilities: [null, [Validators.required]],
    jobDescription: [null, [Validators.required]],
    jobQualifications: [null, [Validators.required]],
    profilePhoto: [],
    militaryPass: [],
    enLanguageScore: [],
    educationCertificate: [],
    driverLicense: [],
    chLanguageScore: [],
    educationQualification: [],
    citizenID: [],
    jpLanguageScore: [],
    workPermit: [],
    marriageCertificate: [],
    houseRegistry: [],
    birthCertificate: [],
    from: [null, [Validators.required]],
    to: [null, [Validators.required]],
    status: [],
  });

  constructor(protected jobPositionsService: JobPositionsService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobPositions }) => {
      if (!jobPositions.id) {
        const today = moment().startOf('day');
        jobPositions.from = today;
        jobPositions.to = today;
      }

      this.updateForm(jobPositions);
    });
  }

  updateForm(jobPositions: IJobPositions): void {
    this.editForm.patchValue({
      id: jobPositions.id,
      name: jobPositions.name,
      teamId: jobPositions.teamId,
      teamName: jobPositions.teamName,
      jobDescriptionTemplateId: jobPositions.jobDescriptionTemplateId,
      jobDescriptionTemplateName: jobPositions.jobDescriptionTemplateName,
      jobTitle: jobPositions.jobTitle,
      lineOfWork: jobPositions.lineOfWork,
      department: jobPositions.department,
      educationBackground: jobPositions.educationBackground,
      salary: jobPositions.salary,
      location: jobPositions.location,
      summary: jobPositions.summary,
      mainReponsibilities: jobPositions.mainReponsibilities,
      jobDescription: jobPositions.jobDescription,
      jobQualifications: jobPositions.jobQualifications,
      profilePhoto: jobPositions.profilePhoto,
      militaryPass: jobPositions.militaryPass,
      enLanguageScore: jobPositions.enLanguageScore,
      educationCertificate: jobPositions.educationCertificate,
      driverLicense: jobPositions.driverLicense,
      chLanguageScore: jobPositions.chLanguageScore,
      educationQualification: jobPositions.educationQualification,
      citizenID: jobPositions.citizenID,
      jpLanguageScore: jobPositions.jpLanguageScore,
      workPermit: jobPositions.workPermit,
      marriageCertificate: jobPositions.marriageCertificate,
      houseRegistry: jobPositions.houseRegistry,
      birthCertificate: jobPositions.birthCertificate,
      from: jobPositions.from ? jobPositions.from.format(DATE_TIME_FORMAT) : null,
      to: jobPositions.to ? jobPositions.to.format(DATE_TIME_FORMAT) : null,
      status: jobPositions.status,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jobPositions = this.createFromForm();
    if (jobPositions.id !== undefined) {
      this.subscribeToSaveResponse(this.jobPositionsService.update(jobPositions));
    } else {
      this.subscribeToSaveResponse(this.jobPositionsService.create(jobPositions));
    }
  }

  private createFromForm(): IJobPositions {
    return {
      ...new JobPositions(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      teamId: this.editForm.get(['teamId'])!.value,
      teamName: this.editForm.get(['teamName'])!.value,
      jobDescriptionTemplateId: this.editForm.get(['jobDescriptionTemplateId'])!.value,
      jobDescriptionTemplateName: this.editForm.get(['jobDescriptionTemplateName'])!.value,
      jobTitle: this.editForm.get(['jobTitle'])!.value,
      lineOfWork: this.editForm.get(['lineOfWork'])!.value,
      department: this.editForm.get(['department'])!.value,
      educationBackground: this.editForm.get(['educationBackground'])!.value,
      salary: this.editForm.get(['salary'])!.value,
      location: this.editForm.get(['location'])!.value,
      summary: this.editForm.get(['summary'])!.value,
      mainReponsibilities: this.editForm.get(['mainReponsibilities'])!.value,
      jobDescription: this.editForm.get(['jobDescription'])!.value,
      jobQualifications: this.editForm.get(['jobQualifications'])!.value,
      profilePhoto: this.editForm.get(['profilePhoto'])!.value,
      militaryPass: this.editForm.get(['militaryPass'])!.value,
      enLanguageScore: this.editForm.get(['enLanguageScore'])!.value,
      educationCertificate: this.editForm.get(['educationCertificate'])!.value,
      driverLicense: this.editForm.get(['driverLicense'])!.value,
      chLanguageScore: this.editForm.get(['chLanguageScore'])!.value,
      educationQualification: this.editForm.get(['educationQualification'])!.value,
      citizenID: this.editForm.get(['citizenID'])!.value,
      jpLanguageScore: this.editForm.get(['jpLanguageScore'])!.value,
      workPermit: this.editForm.get(['workPermit'])!.value,
      marriageCertificate: this.editForm.get(['marriageCertificate'])!.value,
      houseRegistry: this.editForm.get(['houseRegistry'])!.value,
      birthCertificate: this.editForm.get(['birthCertificate'])!.value,
      from: this.editForm.get(['from'])!.value ? moment(this.editForm.get(['from'])!.value, DATE_TIME_FORMAT) : undefined,
      to: this.editForm.get(['to'])!.value ? moment(this.editForm.get(['to'])!.value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJobPositions>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
