import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJobPositions } from 'app/shared/model/job-positions.model';
import { JobPositionsService } from './job-positions.service';

@Component({
  templateUrl: './job-positions-delete-dialog.component.html',
})
export class JobPositionsDeleteDialogComponent {
  jobPositions?: IJobPositions;

  constructor(
    protected jobPositionsService: JobPositionsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.jobPositionsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jobPositionsListModification');
      this.activeModal.close();
    });
  }
}
