import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJobPositions } from 'app/shared/model/job-positions.model';
import { JobPositionsService } from './job-positions.service';
import { JobPositionsDeleteDialogComponent } from './job-positions-delete-dialog.component';

@Component({
  selector: 'jhi-job-positions',
  templateUrl: './job-positions.component.html',
})
export class JobPositionsComponent implements OnInit, OnDestroy {
  jobPositions?: IJobPositions[];
  eventSubscriber?: Subscription;

  constructor(
    protected jobPositionsService: JobPositionsService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.jobPositionsService.query().subscribe((res: HttpResponse<IJobPositions[]>) => (this.jobPositions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJobPositions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJobPositions): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJobPositions(): void {
    this.eventSubscriber = this.eventManager.subscribe('jobPositionsListModification', () => this.loadAll());
  }

  delete(jobPositions: IJobPositions): void {
    const modalRef = this.modalService.open(JobPositionsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jobPositions = jobPositions;
  }
}
