import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { JobPositionsComponent } from './job-positions.component';
import { JobPositionsDetailComponent } from './job-positions-detail.component';
import { JobPositionsUpdateComponent } from './job-positions-update.component';
import { JobPositionsDeleteDialogComponent } from './job-positions-delete-dialog.component';
import { jobPositionsRoute } from './job-positions.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(jobPositionsRoute)],
  declarations: [JobPositionsComponent, JobPositionsDetailComponent, JobPositionsUpdateComponent, JobPositionsDeleteDialogComponent],
  entryComponents: [JobPositionsDeleteDialogComponent],
})
export class MisbeJobPositionsModule {}
