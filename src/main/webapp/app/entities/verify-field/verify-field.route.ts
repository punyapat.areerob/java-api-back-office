import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVerifyField, VerifyField } from 'app/shared/model/verify-field.model';
import { VerifyFieldService } from './verify-field.service';
import { VerifyFieldComponent } from './verify-field.component';
import { VerifyFieldDetailComponent } from './verify-field-detail.component';
import { VerifyFieldUpdateComponent } from './verify-field-update.component';

@Injectable({ providedIn: 'root' })
export class VerifyFieldResolve implements Resolve<IVerifyField> {
  constructor(private service: VerifyFieldService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVerifyField> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((verifyField: HttpResponse<VerifyField>) => {
          if (verifyField.body) {
            return of(verifyField.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VerifyField());
  }
}

export const verifyFieldRoute: Routes = [
  {
    path: '',
    component: VerifyFieldComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VerifyFields',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VerifyFieldDetailComponent,
    resolve: {
      verifyField: VerifyFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VerifyFields',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VerifyFieldUpdateComponent,
    resolve: {
      verifyField: VerifyFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VerifyFields',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VerifyFieldUpdateComponent,
    resolve: {
      verifyField: VerifyFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VerifyFields',
    },
    canActivate: [UserRouteAccessService],
  },
];
