import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVerifyField } from 'app/shared/model/verify-field.model';
import { VerifyFieldService } from './verify-field.service';

@Component({
  templateUrl: './verify-field-delete-dialog.component.html',
})
export class VerifyFieldDeleteDialogComponent {
  verifyField?: IVerifyField;

  constructor(
    protected verifyFieldService: VerifyFieldService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.verifyFieldService.delete(id).subscribe(() => {
      this.eventManager.broadcast('verifyFieldListModification');
      this.activeModal.close();
    });
  }
}
