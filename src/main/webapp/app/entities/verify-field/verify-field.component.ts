import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVerifyField } from 'app/shared/model/verify-field.model';
import { VerifyFieldService } from './verify-field.service';
import { VerifyFieldDeleteDialogComponent } from './verify-field-delete-dialog.component';

@Component({
  selector: 'jhi-verify-field',
  templateUrl: './verify-field.component.html',
})
export class VerifyFieldComponent implements OnInit, OnDestroy {
  verifyFields?: IVerifyField[];
  eventSubscriber?: Subscription;

  constructor(
    protected verifyFieldService: VerifyFieldService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.verifyFieldService.query().subscribe((res: HttpResponse<IVerifyField[]>) => (this.verifyFields = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInVerifyFields();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IVerifyField): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInVerifyFields(): void {
    this.eventSubscriber = this.eventManager.subscribe('verifyFieldListModification', () => this.loadAll());
  }

  delete(verifyField: IVerifyField): void {
    const modalRef = this.modalService.open(VerifyFieldDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.verifyField = verifyField;
  }
}
