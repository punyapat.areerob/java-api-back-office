import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVerifyField, VerifyField } from 'app/shared/model/verify-field.model';
import { VerifyFieldService } from './verify-field.service';
import { ICaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from 'app/entities/case-step/case-step.service';
import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from 'app/entities/case-step-instance/case-step-instance.service';

type SelectableEntity = ICaseStep | ICaseStepInstance;

@Component({
  selector: 'jhi-verify-field-update',
  templateUrl: './verify-field-update.component.html',
})
export class VerifyFieldUpdateComponent implements OnInit {
  isSaving = false;
  casesteps: ICaseStep[] = [];
  casestepinstances: ICaseStepInstance[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    label: [],
    link: [],
    verify: [],
    caseStep: [],
    caseStepInstance: [],
  });

  constructor(
    protected verifyFieldService: VerifyFieldService,
    protected caseStepService: CaseStepService,
    protected caseStepInstanceService: CaseStepInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ verifyField }) => {
      this.updateForm(verifyField);

      this.caseStepService.query().subscribe((res: HttpResponse<ICaseStep[]>) => (this.casesteps = res.body || []));

      this.caseStepInstanceService.query().subscribe((res: HttpResponse<ICaseStepInstance[]>) => (this.casestepinstances = res.body || []));
    });
  }

  updateForm(verifyField: IVerifyField): void {
    this.editForm.patchValue({
      id: verifyField.id,
      name: verifyField.name,
      label: verifyField.label,
      link: verifyField.link,
      verify: verifyField.verify,
      caseStep: verifyField.caseStep,
      caseStepInstance: verifyField.caseStepInstance,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const verifyField = this.createFromForm();
    if (verifyField.id !== undefined) {
      this.subscribeToSaveResponse(this.verifyFieldService.update(verifyField));
    } else {
      this.subscribeToSaveResponse(this.verifyFieldService.create(verifyField));
    }
  }

  private createFromForm(): IVerifyField {
    return {
      ...new VerifyField(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      label: this.editForm.get(['label'])!.value,
      link: this.editForm.get(['link'])!.value,
      verify: this.editForm.get(['verify'])!.value,
      caseStep: this.editForm.get(['caseStep'])!.value,
      caseStepInstance: this.editForm.get(['caseStepInstance'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVerifyField>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
