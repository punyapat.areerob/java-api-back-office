import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVerifyField } from 'app/shared/model/verify-field.model';

@Component({
  selector: 'jhi-verify-field-detail',
  templateUrl: './verify-field-detail.component.html',
})
export class VerifyFieldDetailComponent implements OnInit {
  verifyField: IVerifyField | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ verifyField }) => (this.verifyField = verifyField));
  }

  previousState(): void {
    window.history.back();
  }
}
