import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { VerifyFieldComponent } from './verify-field.component';
import { VerifyFieldDetailComponent } from './verify-field-detail.component';
import { VerifyFieldUpdateComponent } from './verify-field-update.component';
import { VerifyFieldDeleteDialogComponent } from './verify-field-delete-dialog.component';
import { verifyFieldRoute } from './verify-field.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(verifyFieldRoute)],
  declarations: [VerifyFieldComponent, VerifyFieldDetailComponent, VerifyFieldUpdateComponent, VerifyFieldDeleteDialogComponent],
  entryComponents: [VerifyFieldDeleteDialogComponent],
})
export class MisbeVerifyFieldModule {}
