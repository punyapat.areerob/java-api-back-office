import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVerifyField } from 'app/shared/model/verify-field.model';

type EntityResponseType = HttpResponse<IVerifyField>;
type EntityArrayResponseType = HttpResponse<IVerifyField[]>;

@Injectable({ providedIn: 'root' })
export class VerifyFieldService {
  public resourceUrl = SERVER_API_URL + 'api/verify-fields';

  constructor(protected http: HttpClient) {}

  create(verifyField: IVerifyField): Observable<EntityResponseType> {
    return this.http.post<IVerifyField>(this.resourceUrl, verifyField, { observe: 'response' });
  }

  update(verifyField: IVerifyField): Observable<EntityResponseType> {
    return this.http.put<IVerifyField>(this.resourceUrl, verifyField, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IVerifyField>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVerifyField[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
