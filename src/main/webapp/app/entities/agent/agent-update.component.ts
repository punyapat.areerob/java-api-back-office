import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAgent, Agent } from 'app/shared/model/agent.model';
import { AgentService } from './agent.service';

@Component({
  selector: 'jhi-agent-update',
  templateUrl: './agent-update.component.html',
})
export class AgentUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentId: [],
    type: [],
    name: [],
    address: [],
    fax: [],
    phone: [],
    website: [],
    email: [],
    status: [],
  });

  constructor(protected agentService: AgentService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agent }) => {
      this.updateForm(agent);
    });
  }

  updateForm(agent: IAgent): void {
    this.editForm.patchValue({
      id: agent.id,
      agentId: agent.agentId,
      type: agent.type,
      name: agent.name,
      address: agent.address,
      fax: agent.fax,
      phone: agent.phone,
      website: agent.website,
      email: agent.email,
      status: agent.status,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agent = this.createFromForm();
    if (agent.id !== undefined) {
      this.subscribeToSaveResponse(this.agentService.update(agent));
    } else {
      this.subscribeToSaveResponse(this.agentService.create(agent));
    }
  }

  private createFromForm(): IAgent {
    return {
      ...new Agent(),
      id: this.editForm.get(['id'])!.value,
      agentId: this.editForm.get(['agentId'])!.value,
      type: this.editForm.get(['type'])!.value,
      name: this.editForm.get(['name'])!.value,
      address: this.editForm.get(['address'])!.value,
      fax: this.editForm.get(['fax'])!.value,
      phone: this.editForm.get(['phone'])!.value,
      website: this.editForm.get(['website'])!.value,
      email: this.editForm.get(['email'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgent>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
