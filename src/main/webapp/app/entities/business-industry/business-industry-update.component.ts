import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBusinessIndustry, BusinessIndustry } from 'app/shared/model/business-industry.model';
import { BusinessIndustryService } from './business-industry.service';

@Component({
  selector: 'jhi-business-industry-update',
  templateUrl: './business-industry-update.component.html',
})
export class BusinessIndustryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(
    protected businessIndustryService: BusinessIndustryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ businessIndustry }) => {
      this.updateForm(businessIndustry);
    });
  }

  updateForm(businessIndustry: IBusinessIndustry): void {
    this.editForm.patchValue({
      id: businessIndustry.id,
      name: businessIndustry.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const businessIndustry = this.createFromForm();
    if (businessIndustry.id !== undefined) {
      this.subscribeToSaveResponse(this.businessIndustryService.update(businessIndustry));
    } else {
      this.subscribeToSaveResponse(this.businessIndustryService.create(businessIndustry));
    }
  }

  private createFromForm(): IBusinessIndustry {
    return {
      ...new BusinessIndustry(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBusinessIndustry>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
