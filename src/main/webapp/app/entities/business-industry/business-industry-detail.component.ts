import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBusinessIndustry } from 'app/shared/model/business-industry.model';

@Component({
  selector: 'jhi-business-industry-detail',
  templateUrl: './business-industry-detail.component.html',
})
export class BusinessIndustryDetailComponent implements OnInit {
  businessIndustry: IBusinessIndustry | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ businessIndustry }) => (this.businessIndustry = businessIndustry));
  }

  previousState(): void {
    window.history.back();
  }
}
