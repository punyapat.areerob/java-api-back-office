import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBusinessIndustry } from 'app/shared/model/business-industry.model';

type EntityResponseType = HttpResponse<IBusinessIndustry>;
type EntityArrayResponseType = HttpResponse<IBusinessIndustry[]>;

@Injectable({ providedIn: 'root' })
export class BusinessIndustryService {
  public resourceUrl = SERVER_API_URL + 'api/business-industries';

  constructor(protected http: HttpClient) {}

  create(businessIndustry: IBusinessIndustry): Observable<EntityResponseType> {
    return this.http.post<IBusinessIndustry>(this.resourceUrl, businessIndustry, { observe: 'response' });
  }

  update(businessIndustry: IBusinessIndustry): Observable<EntityResponseType> {
    return this.http.put<IBusinessIndustry>(this.resourceUrl, businessIndustry, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IBusinessIndustry>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBusinessIndustry[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
