import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBusinessIndustry, BusinessIndustry } from 'app/shared/model/business-industry.model';
import { BusinessIndustryService } from './business-industry.service';
import { BusinessIndustryComponent } from './business-industry.component';
import { BusinessIndustryDetailComponent } from './business-industry-detail.component';
import { BusinessIndustryUpdateComponent } from './business-industry-update.component';

@Injectable({ providedIn: 'root' })
export class BusinessIndustryResolve implements Resolve<IBusinessIndustry> {
  constructor(private service: BusinessIndustryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBusinessIndustry> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((businessIndustry: HttpResponse<BusinessIndustry>) => {
          if (businessIndustry.body) {
            return of(businessIndustry.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BusinessIndustry());
  }
}

export const businessIndustryRoute: Routes = [
  {
    path: '',
    component: BusinessIndustryComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BusinessIndustries',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BusinessIndustryDetailComponent,
    resolve: {
      businessIndustry: BusinessIndustryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BusinessIndustries',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BusinessIndustryUpdateComponent,
    resolve: {
      businessIndustry: BusinessIndustryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BusinessIndustries',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BusinessIndustryUpdateComponent,
    resolve: {
      businessIndustry: BusinessIndustryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BusinessIndustries',
    },
    canActivate: [UserRouteAccessService],
  },
];
