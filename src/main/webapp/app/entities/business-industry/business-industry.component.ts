import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBusinessIndustry } from 'app/shared/model/business-industry.model';
import { BusinessIndustryService } from './business-industry.service';
import { BusinessIndustryDeleteDialogComponent } from './business-industry-delete-dialog.component';

@Component({
  selector: 'jhi-business-industry',
  templateUrl: './business-industry.component.html',
})
export class BusinessIndustryComponent implements OnInit, OnDestroy {
  businessIndustries?: IBusinessIndustry[];
  eventSubscriber?: Subscription;

  constructor(
    protected businessIndustryService: BusinessIndustryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.businessIndustryService.query().subscribe((res: HttpResponse<IBusinessIndustry[]>) => (this.businessIndustries = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBusinessIndustries();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBusinessIndustry): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBusinessIndustries(): void {
    this.eventSubscriber = this.eventManager.subscribe('businessIndustryListModification', () => this.loadAll());
  }

  delete(businessIndustry: IBusinessIndustry): void {
    const modalRef = this.modalService.open(BusinessIndustryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.businessIndustry = businessIndustry;
  }
}
