import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BusinessIndustryComponent } from './business-industry.component';
import { BusinessIndustryDetailComponent } from './business-industry-detail.component';
import { BusinessIndustryUpdateComponent } from './business-industry-update.component';
import { BusinessIndustryDeleteDialogComponent } from './business-industry-delete-dialog.component';
import { businessIndustryRoute } from './business-industry.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(businessIndustryRoute)],
  declarations: [
    BusinessIndustryComponent,
    BusinessIndustryDetailComponent,
    BusinessIndustryUpdateComponent,
    BusinessIndustryDeleteDialogComponent,
  ],
  entryComponents: [BusinessIndustryDeleteDialogComponent],
})
export class MisbeBusinessIndustryModule {}
