import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBusinessIndustry } from 'app/shared/model/business-industry.model';
import { BusinessIndustryService } from './business-industry.service';

@Component({
  templateUrl: './business-industry-delete-dialog.component.html',
})
export class BusinessIndustryDeleteDialogComponent {
  businessIndustry?: IBusinessIndustry;

  constructor(
    protected businessIndustryService: BusinessIndustryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.businessIndustryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('businessIndustryListModification');
      this.activeModal.close();
    });
  }
}
