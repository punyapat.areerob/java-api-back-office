import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVisaType } from 'app/shared/model/visa-type.model';
import { VisaTypeService } from './visa-type.service';

@Component({
  templateUrl: './visa-type-delete-dialog.component.html',
})
export class VisaTypeDeleteDialogComponent {
  visaType?: IVisaType;

  constructor(protected visaTypeService: VisaTypeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.visaTypeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('visaTypeListModification');
      this.activeModal.close();
    });
  }
}
