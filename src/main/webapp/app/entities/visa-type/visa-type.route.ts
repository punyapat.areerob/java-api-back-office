import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IVisaType, VisaType } from 'app/shared/model/visa-type.model';
import { VisaTypeService } from './visa-type.service';
import { VisaTypeComponent } from './visa-type.component';
import { VisaTypeDetailComponent } from './visa-type-detail.component';
import { VisaTypeUpdateComponent } from './visa-type-update.component';

@Injectable({ providedIn: 'root' })
export class VisaTypeResolve implements Resolve<IVisaType> {
  constructor(private service: VisaTypeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVisaType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((visaType: HttpResponse<VisaType>) => {
          if (visaType.body) {
            return of(visaType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new VisaType());
  }
}

export const visaTypeRoute: Routes = [
  {
    path: '',
    component: VisaTypeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VisaTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VisaTypeDetailComponent,
    resolve: {
      visaType: VisaTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VisaTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VisaTypeUpdateComponent,
    resolve: {
      visaType: VisaTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VisaTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VisaTypeUpdateComponent,
    resolve: {
      visaType: VisaTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'VisaTypes',
    },
    canActivate: [UserRouteAccessService],
  },
];
