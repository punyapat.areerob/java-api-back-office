import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IVisaType, VisaType } from 'app/shared/model/visa-type.model';
import { VisaTypeService } from './visa-type.service';

@Component({
  selector: 'jhi-visa-type-update',
  templateUrl: './visa-type-update.component.html',
})
export class VisaTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    active: [],
  });

  constructor(protected visaTypeService: VisaTypeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ visaType }) => {
      this.updateForm(visaType);
    });
  }

  updateForm(visaType: IVisaType): void {
    this.editForm.patchValue({
      id: visaType.id,
      name: visaType.name,
      active: visaType.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const visaType = this.createFromForm();
    if (visaType.id !== undefined) {
      this.subscribeToSaveResponse(this.visaTypeService.update(visaType));
    } else {
      this.subscribeToSaveResponse(this.visaTypeService.create(visaType));
    }
  }

  private createFromForm(): IVisaType {
    return {
      ...new VisaType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVisaType>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
