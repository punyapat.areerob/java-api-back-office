import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IVisaType } from 'app/shared/model/visa-type.model';
import { VisaTypeService } from './visa-type.service';
import { VisaTypeDeleteDialogComponent } from './visa-type-delete-dialog.component';

@Component({
  selector: 'jhi-visa-type',
  templateUrl: './visa-type.component.html',
})
export class VisaTypeComponent implements OnInit, OnDestroy {
  visaTypes?: IVisaType[];
  eventSubscriber?: Subscription;

  constructor(protected visaTypeService: VisaTypeService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.visaTypeService.query().subscribe((res: HttpResponse<IVisaType[]>) => (this.visaTypes = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInVisaTypes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IVisaType): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInVisaTypes(): void {
    this.eventSubscriber = this.eventManager.subscribe('visaTypeListModification', () => this.loadAll());
  }

  delete(visaType: IVisaType): void {
    const modalRef = this.modalService.open(VisaTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.visaType = visaType;
  }
}
