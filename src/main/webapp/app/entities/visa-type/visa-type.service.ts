import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVisaType } from 'app/shared/model/visa-type.model';

type EntityResponseType = HttpResponse<IVisaType>;
type EntityArrayResponseType = HttpResponse<IVisaType[]>;

@Injectable({ providedIn: 'root' })
export class VisaTypeService {
  public resourceUrl = SERVER_API_URL + 'api/visa-types';

  constructor(protected http: HttpClient) {}

  create(visaType: IVisaType): Observable<EntityResponseType> {
    return this.http.post<IVisaType>(this.resourceUrl, visaType, { observe: 'response' });
  }

  update(visaType: IVisaType): Observable<EntityResponseType> {
    return this.http.put<IVisaType>(this.resourceUrl, visaType, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IVisaType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IVisaType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
