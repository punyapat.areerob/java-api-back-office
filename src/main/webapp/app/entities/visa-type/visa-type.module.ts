import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { VisaTypeComponent } from './visa-type.component';
import { VisaTypeDetailComponent } from './visa-type-detail.component';
import { VisaTypeUpdateComponent } from './visa-type-update.component';
import { VisaTypeDeleteDialogComponent } from './visa-type-delete-dialog.component';
import { visaTypeRoute } from './visa-type.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(visaTypeRoute)],
  declarations: [VisaTypeComponent, VisaTypeDetailComponent, VisaTypeUpdateComponent, VisaTypeDeleteDialogComponent],
  entryComponents: [VisaTypeDeleteDialogComponent],
})
export class MisbeVisaTypeModule {}
