import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVisaType } from 'app/shared/model/visa-type.model';

@Component({
  selector: 'jhi-visa-type-detail',
  templateUrl: './visa-type-detail.component.html',
})
export class VisaTypeDetailComponent implements OnInit {
  visaType: IVisaType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ visaType }) => (this.visaType = visaType));
  }

  previousState(): void {
    window.history.back();
  }
}
