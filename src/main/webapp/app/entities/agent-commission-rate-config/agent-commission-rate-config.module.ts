import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { AgentCommissionRateConfigComponent } from './agent-commission-rate-config.component';
import { AgentCommissionRateConfigDetailComponent } from './agent-commission-rate-config-detail.component';
import { AgentCommissionRateConfigUpdateComponent } from './agent-commission-rate-config-update.component';
import { AgentCommissionRateConfigDeleteDialogComponent } from './agent-commission-rate-config-delete-dialog.component';
import { agentCommissionRateConfigRoute } from './agent-commission-rate-config.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(agentCommissionRateConfigRoute)],
  declarations: [
    AgentCommissionRateConfigComponent,
    AgentCommissionRateConfigDetailComponent,
    AgentCommissionRateConfigUpdateComponent,
    AgentCommissionRateConfigDeleteDialogComponent,
  ],
  entryComponents: [AgentCommissionRateConfigDeleteDialogComponent],
})
export class MisbeAgentCommissionRateConfigModule {}
