import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';
import { AgentCommissionRateConfigService } from './agent-commission-rate-config.service';
import { AgentCommissionRateConfigDeleteDialogComponent } from './agent-commission-rate-config-delete-dialog.component';

@Component({
  selector: 'jhi-agent-commission-rate-config',
  templateUrl: './agent-commission-rate-config.component.html',
})
export class AgentCommissionRateConfigComponent implements OnInit, OnDestroy {
  agentCommissionRateConfigs?: IAgentCommissionRateConfig[];
  eventSubscriber?: Subscription;

  constructor(
    protected agentCommissionRateConfigService: AgentCommissionRateConfigService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.agentCommissionRateConfigService
      .query()
      .subscribe((res: HttpResponse<IAgentCommissionRateConfig[]>) => (this.agentCommissionRateConfigs = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAgentCommissionRateConfigs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAgentCommissionRateConfig): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAgentCommissionRateConfigs(): void {
    this.eventSubscriber = this.eventManager.subscribe('agentCommissionRateConfigListModification', () => this.loadAll());
  }

  delete(agentCommissionRateConfig: IAgentCommissionRateConfig): void {
    const modalRef = this.modalService.open(AgentCommissionRateConfigDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.agentCommissionRateConfig = agentCommissionRateConfig;
  }
}
