import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAgentCommissionRateConfig, AgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';
import { AgentCommissionRateConfigService } from './agent-commission-rate-config.service';

@Component({
  selector: 'jhi-agent-commission-rate-config-update',
  templateUrl: './agent-commission-rate-config-update.component.html',
})
export class AgentCommissionRateConfigUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    agentId: [],
    commissionId: [],
    start: [],
    end: [],
  });

  constructor(
    protected agentCommissionRateConfigService: AgentCommissionRateConfigService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentCommissionRateConfig }) => {
      if (!agentCommissionRateConfig.id) {
        const today = moment().startOf('day');
        agentCommissionRateConfig.start = today;
        agentCommissionRateConfig.end = today;
      }

      this.updateForm(agentCommissionRateConfig);
    });
  }

  updateForm(agentCommissionRateConfig: IAgentCommissionRateConfig): void {
    this.editForm.patchValue({
      id: agentCommissionRateConfig.id,
      agentId: agentCommissionRateConfig.agentId,
      commissionId: agentCommissionRateConfig.commissionId,
      start: agentCommissionRateConfig.start ? agentCommissionRateConfig.start.format(DATE_TIME_FORMAT) : null,
      end: agentCommissionRateConfig.end ? agentCommissionRateConfig.end.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agentCommissionRateConfig = this.createFromForm();
    if (agentCommissionRateConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.agentCommissionRateConfigService.update(agentCommissionRateConfig));
    } else {
      this.subscribeToSaveResponse(this.agentCommissionRateConfigService.create(agentCommissionRateConfig));
    }
  }

  private createFromForm(): IAgentCommissionRateConfig {
    return {
      ...new AgentCommissionRateConfig(),
      id: this.editForm.get(['id'])!.value,
      agentId: this.editForm.get(['agentId'])!.value,
      commissionId: this.editForm.get(['commissionId'])!.value,
      start: this.editForm.get(['start'])!.value ? moment(this.editForm.get(['start'])!.value, DATE_TIME_FORMAT) : undefined,
      end: this.editForm.get(['end'])!.value ? moment(this.editForm.get(['end'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentCommissionRateConfig>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
