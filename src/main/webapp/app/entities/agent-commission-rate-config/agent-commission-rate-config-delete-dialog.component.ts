import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';
import { AgentCommissionRateConfigService } from './agent-commission-rate-config.service';

@Component({
  templateUrl: './agent-commission-rate-config-delete-dialog.component.html',
})
export class AgentCommissionRateConfigDeleteDialogComponent {
  agentCommissionRateConfig?: IAgentCommissionRateConfig;

  constructor(
    protected agentCommissionRateConfigService: AgentCommissionRateConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.agentCommissionRateConfigService.delete(id).subscribe(() => {
      this.eventManager.broadcast('agentCommissionRateConfigListModification');
      this.activeModal.close();
    });
  }
}
