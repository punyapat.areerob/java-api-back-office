import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';

type EntityResponseType = HttpResponse<IAgentCommissionRateConfig>;
type EntityArrayResponseType = HttpResponse<IAgentCommissionRateConfig[]>;

@Injectable({ providedIn: 'root' })
export class AgentCommissionRateConfigService {
  public resourceUrl = SERVER_API_URL + 'api/agent-commission-rate-configs';

  constructor(protected http: HttpClient) {}

  create(agentCommissionRateConfig: IAgentCommissionRateConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agentCommissionRateConfig);
    return this.http
      .post<IAgentCommissionRateConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(agentCommissionRateConfig: IAgentCommissionRateConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(agentCommissionRateConfig);
    return this.http
      .put<IAgentCommissionRateConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IAgentCommissionRateConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAgentCommissionRateConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(agentCommissionRateConfig: IAgentCommissionRateConfig): IAgentCommissionRateConfig {
    const copy: IAgentCommissionRateConfig = Object.assign({}, agentCommissionRateConfig, {
      start:
        agentCommissionRateConfig.start && agentCommissionRateConfig.start.isValid() ? agentCommissionRateConfig.start.toJSON() : undefined,
      end: agentCommissionRateConfig.end && agentCommissionRateConfig.end.isValid() ? agentCommissionRateConfig.end.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.start = res.body.start ? moment(res.body.start) : undefined;
      res.body.end = res.body.end ? moment(res.body.end) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((agentCommissionRateConfig: IAgentCommissionRateConfig) => {
        agentCommissionRateConfig.start = agentCommissionRateConfig.start ? moment(agentCommissionRateConfig.start) : undefined;
        agentCommissionRateConfig.end = agentCommissionRateConfig.end ? moment(agentCommissionRateConfig.end) : undefined;
      });
    }
    return res;
  }
}
