import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAgentCommissionRateConfig, AgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';
import { AgentCommissionRateConfigService } from './agent-commission-rate-config.service';
import { AgentCommissionRateConfigComponent } from './agent-commission-rate-config.component';
import { AgentCommissionRateConfigDetailComponent } from './agent-commission-rate-config-detail.component';
import { AgentCommissionRateConfigUpdateComponent } from './agent-commission-rate-config-update.component';

@Injectable({ providedIn: 'root' })
export class AgentCommissionRateConfigResolve implements Resolve<IAgentCommissionRateConfig> {
  constructor(private service: AgentCommissionRateConfigService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAgentCommissionRateConfig> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((agentCommissionRateConfig: HttpResponse<AgentCommissionRateConfig>) => {
          if (agentCommissionRateConfig.body) {
            return of(agentCommissionRateConfig.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AgentCommissionRateConfig());
  }
}

export const agentCommissionRateConfigRoute: Routes = [
  {
    path: '',
    component: AgentCommissionRateConfigComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRateConfigs',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AgentCommissionRateConfigDetailComponent,
    resolve: {
      agentCommissionRateConfig: AgentCommissionRateConfigResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRateConfigs',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AgentCommissionRateConfigUpdateComponent,
    resolve: {
      agentCommissionRateConfig: AgentCommissionRateConfigResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRateConfigs',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AgentCommissionRateConfigUpdateComponent,
    resolve: {
      agentCommissionRateConfig: AgentCommissionRateConfigResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AgentCommissionRateConfigs',
    },
    canActivate: [UserRouteAccessService],
  },
];
