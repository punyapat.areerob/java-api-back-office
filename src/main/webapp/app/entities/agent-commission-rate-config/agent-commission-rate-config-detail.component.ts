import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';

@Component({
  selector: 'jhi-agent-commission-rate-config-detail',
  templateUrl: './agent-commission-rate-config-detail.component.html',
})
export class AgentCommissionRateConfigDetailComponent implements OnInit {
  agentCommissionRateConfig: IAgentCommissionRateConfig | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentCommissionRateConfig }) => (this.agentCommissionRateConfig = agentCommissionRateConfig));
  }

  previousState(): void {
    window.history.back();
  }
}
