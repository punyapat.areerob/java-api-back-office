import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAccommodationType } from 'app/shared/model/accommodation-type.model';
import { AccommodationTypeService } from './accommodation-type.service';

@Component({
  templateUrl: './accommodation-type-delete-dialog.component.html',
})
export class AccommodationTypeDeleteDialogComponent {
  accommodationType?: IAccommodationType;

  constructor(
    protected accommodationTypeService: AccommodationTypeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.accommodationTypeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('accommodationTypeListModification');
      this.activeModal.close();
    });
  }
}
