import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAccommodationType, AccommodationType } from 'app/shared/model/accommodation-type.model';
import { AccommodationTypeService } from './accommodation-type.service';
import { AccommodationTypeComponent } from './accommodation-type.component';
import { AccommodationTypeDetailComponent } from './accommodation-type-detail.component';
import { AccommodationTypeUpdateComponent } from './accommodation-type-update.component';

@Injectable({ providedIn: 'root' })
export class AccommodationTypeResolve implements Resolve<IAccommodationType> {
  constructor(private service: AccommodationTypeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAccommodationType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((accommodationType: HttpResponse<AccommodationType>) => {
          if (accommodationType.body) {
            return of(accommodationType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AccommodationType());
  }
}

export const accommodationTypeRoute: Routes = [
  {
    path: '',
    component: AccommodationTypeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AccommodationTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AccommodationTypeDetailComponent,
    resolve: {
      accommodationType: AccommodationTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AccommodationTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AccommodationTypeUpdateComponent,
    resolve: {
      accommodationType: AccommodationTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AccommodationTypes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AccommodationTypeUpdateComponent,
    resolve: {
      accommodationType: AccommodationTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'AccommodationTypes',
    },
    canActivate: [UserRouteAccessService],
  },
];
