import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAccommodationType } from 'app/shared/model/accommodation-type.model';

type EntityResponseType = HttpResponse<IAccommodationType>;
type EntityArrayResponseType = HttpResponse<IAccommodationType[]>;

@Injectable({ providedIn: 'root' })
export class AccommodationTypeService {
  public resourceUrl = SERVER_API_URL + 'api/accommodation-types';

  constructor(protected http: HttpClient) {}

  create(accommodationType: IAccommodationType): Observable<EntityResponseType> {
    return this.http.post<IAccommodationType>(this.resourceUrl, accommodationType, { observe: 'response' });
  }

  update(accommodationType: IAccommodationType): Observable<EntityResponseType> {
    return this.http.put<IAccommodationType>(this.resourceUrl, accommodationType, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IAccommodationType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAccommodationType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
