import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { AccommodationTypeComponent } from './accommodation-type.component';
import { AccommodationTypeDetailComponent } from './accommodation-type-detail.component';
import { AccommodationTypeUpdateComponent } from './accommodation-type-update.component';
import { AccommodationTypeDeleteDialogComponent } from './accommodation-type-delete-dialog.component';
import { accommodationTypeRoute } from './accommodation-type.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(accommodationTypeRoute)],
  declarations: [
    AccommodationTypeComponent,
    AccommodationTypeDetailComponent,
    AccommodationTypeUpdateComponent,
    AccommodationTypeDeleteDialogComponent,
  ],
  entryComponents: [AccommodationTypeDeleteDialogComponent],
})
export class MisbeAccommodationTypeModule {}
