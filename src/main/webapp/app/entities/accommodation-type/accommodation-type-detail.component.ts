import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAccommodationType } from 'app/shared/model/accommodation-type.model';

@Component({
  selector: 'jhi-accommodation-type-detail',
  templateUrl: './accommodation-type-detail.component.html',
})
export class AccommodationTypeDetailComponent implements OnInit {
  accommodationType: IAccommodationType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accommodationType }) => (this.accommodationType = accommodationType));
  }

  previousState(): void {
    window.history.back();
  }
}
