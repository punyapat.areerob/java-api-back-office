import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAccommodationType, AccommodationType } from 'app/shared/model/accommodation-type.model';
import { AccommodationTypeService } from './accommodation-type.service';

@Component({
  selector: 'jhi-accommodation-type-update',
  templateUrl: './accommodation-type-update.component.html',
})
export class AccommodationTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    active: [],
  });

  constructor(
    protected accommodationTypeService: AccommodationTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accommodationType }) => {
      this.updateForm(accommodationType);
    });
  }

  updateForm(accommodationType: IAccommodationType): void {
    this.editForm.patchValue({
      id: accommodationType.id,
      name: accommodationType.name,
      active: accommodationType.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accommodationType = this.createFromForm();
    if (accommodationType.id !== undefined) {
      this.subscribeToSaveResponse(this.accommodationTypeService.update(accommodationType));
    } else {
      this.subscribeToSaveResponse(this.accommodationTypeService.create(accommodationType));
    }
  }

  private createFromForm(): IAccommodationType {
    return {
      ...new AccommodationType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccommodationType>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
