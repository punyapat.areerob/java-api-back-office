import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccommodationType } from 'app/shared/model/accommodation-type.model';
import { AccommodationTypeService } from './accommodation-type.service';
import { AccommodationTypeDeleteDialogComponent } from './accommodation-type-delete-dialog.component';

@Component({
  selector: 'jhi-accommodation-type',
  templateUrl: './accommodation-type.component.html',
})
export class AccommodationTypeComponent implements OnInit, OnDestroy {
  accommodationTypes?: IAccommodationType[];
  eventSubscriber?: Subscription;

  constructor(
    protected accommodationTypeService: AccommodationTypeService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.accommodationTypeService
      .query()
      .subscribe((res: HttpResponse<IAccommodationType[]>) => (this.accommodationTypes = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAccommodationTypes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAccommodationType): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAccommodationTypes(): void {
    this.eventSubscriber = this.eventManager.subscribe('accommodationTypeListModification', () => this.loadAll());
  }

  delete(accommodationType: IAccommodationType): void {
    const modalRef = this.modalService.open(AccommodationTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.accommodationType = accommodationType;
  }
}
