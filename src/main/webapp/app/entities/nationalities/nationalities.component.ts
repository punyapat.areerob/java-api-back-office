import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { INationalities } from 'app/shared/model/nationalities.model';
import { NationalitiesService } from './nationalities.service';
import { NationalitiesDeleteDialogComponent } from './nationalities-delete-dialog.component';

@Component({
  selector: 'jhi-nationalities',
  templateUrl: './nationalities.component.html',
})
export class NationalitiesComponent implements OnInit, OnDestroy {
  nationalities?: INationalities[];
  eventSubscriber?: Subscription;

  constructor(
    protected nationalitiesService: NationalitiesService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.nationalitiesService.query().subscribe((res: HttpResponse<INationalities[]>) => (this.nationalities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInNationalities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: INationalities): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInNationalities(): void {
    this.eventSubscriber = this.eventManager.subscribe('nationalitiesListModification', () => this.loadAll());
  }

  delete(nationalities: INationalities): void {
    const modalRef = this.modalService.open(NationalitiesDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.nationalities = nationalities;
  }
}
