import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INationalities } from 'app/shared/model/nationalities.model';

@Component({
  selector: 'jhi-nationalities-detail',
  templateUrl: './nationalities-detail.component.html',
})
export class NationalitiesDetailComponent implements OnInit {
  nationalities: INationalities | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nationalities }) => (this.nationalities = nationalities));
  }

  previousState(): void {
    window.history.back();
  }
}
