import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { NationalitiesComponent } from './nationalities.component';
import { NationalitiesDetailComponent } from './nationalities-detail.component';
import { NationalitiesUpdateComponent } from './nationalities-update.component';
import { NationalitiesDeleteDialogComponent } from './nationalities-delete-dialog.component';
import { nationalitiesRoute } from './nationalities.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(nationalitiesRoute)],
  declarations: [NationalitiesComponent, NationalitiesDetailComponent, NationalitiesUpdateComponent, NationalitiesDeleteDialogComponent],
  entryComponents: [NationalitiesDeleteDialogComponent],
})
export class MisbeNationalitiesModule {}
