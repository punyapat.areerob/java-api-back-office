import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INationalities } from 'app/shared/model/nationalities.model';
import { NationalitiesService } from './nationalities.service';

@Component({
  templateUrl: './nationalities-delete-dialog.component.html',
})
export class NationalitiesDeleteDialogComponent {
  nationalities?: INationalities;

  constructor(
    protected nationalitiesService: NationalitiesService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.nationalitiesService.delete(id).subscribe(() => {
      this.eventManager.broadcast('nationalitiesListModification');
      this.activeModal.close();
    });
  }
}
