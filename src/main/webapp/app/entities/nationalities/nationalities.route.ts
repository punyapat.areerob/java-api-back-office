import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { INationalities, Nationalities } from 'app/shared/model/nationalities.model';
import { NationalitiesService } from './nationalities.service';
import { NationalitiesComponent } from './nationalities.component';
import { NationalitiesDetailComponent } from './nationalities-detail.component';
import { NationalitiesUpdateComponent } from './nationalities-update.component';

@Injectable({ providedIn: 'root' })
export class NationalitiesResolve implements Resolve<INationalities> {
  constructor(private service: NationalitiesService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INationalities> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((nationalities: HttpResponse<Nationalities>) => {
          if (nationalities.body) {
            return of(nationalities.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Nationalities());
  }
}

export const nationalitiesRoute: Routes = [
  {
    path: '',
    component: NationalitiesComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Nationalities',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NationalitiesDetailComponent,
    resolve: {
      nationalities: NationalitiesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Nationalities',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NationalitiesUpdateComponent,
    resolve: {
      nationalities: NationalitiesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Nationalities',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NationalitiesUpdateComponent,
    resolve: {
      nationalities: NationalitiesResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Nationalities',
    },
    canActivate: [UserRouteAccessService],
  },
];
