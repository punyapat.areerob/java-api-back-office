import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { INationalities, Nationalities } from 'app/shared/model/nationalities.model';
import { NationalitiesService } from './nationalities.service';

@Component({
  selector: 'jhi-nationalities-update',
  templateUrl: './nationalities-update.component.html',
})
export class NationalitiesUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(protected nationalitiesService: NationalitiesService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ nationalities }) => {
      this.updateForm(nationalities);
    });
  }

  updateForm(nationalities: INationalities): void {
    this.editForm.patchValue({
      id: nationalities.id,
      name: nationalities.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const nationalities = this.createFromForm();
    if (nationalities.id !== undefined) {
      this.subscribeToSaveResponse(this.nationalitiesService.update(nationalities));
    } else {
      this.subscribeToSaveResponse(this.nationalitiesService.create(nationalities));
    }
  }

  private createFromForm(): INationalities {
    return {
      ...new Nationalities(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INationalities>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
