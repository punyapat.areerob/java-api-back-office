import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBroadcastMessage } from 'app/shared/model/broadcast-message.model';

type EntityResponseType = HttpResponse<IBroadcastMessage>;
type EntityArrayResponseType = HttpResponse<IBroadcastMessage[]>;

@Injectable({ providedIn: 'root' })
export class BroadcastMessageService {
  public resourceUrl = SERVER_API_URL + 'api/broadcast-messages';

  constructor(protected http: HttpClient) {}

  create(broadcastMessage: IBroadcastMessage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(broadcastMessage);
    return this.http
      .post<IBroadcastMessage>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(broadcastMessage: IBroadcastMessage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(broadcastMessage);
    return this.http
      .put<IBroadcastMessage>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IBroadcastMessage>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBroadcastMessage[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(broadcastMessage: IBroadcastMessage): IBroadcastMessage {
    const copy: IBroadcastMessage = Object.assign({}, broadcastMessage, {
      from: broadcastMessage.from && broadcastMessage.from.isValid() ? broadcastMessage.from.toJSON() : undefined,
      to: broadcastMessage.to && broadcastMessage.to.isValid() ? broadcastMessage.to.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.from = res.body.from ? moment(res.body.from) : undefined;
      res.body.to = res.body.to ? moment(res.body.to) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((broadcastMessage: IBroadcastMessage) => {
        broadcastMessage.from = broadcastMessage.from ? moment(broadcastMessage.from) : undefined;
        broadcastMessage.to = broadcastMessage.to ? moment(broadcastMessage.to) : undefined;
      });
    }
    return res;
  }
}
