import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBroadcastMessage, BroadcastMessage } from 'app/shared/model/broadcast-message.model';
import { BroadcastMessageService } from './broadcast-message.service';
import { BroadcastMessageComponent } from './broadcast-message.component';
import { BroadcastMessageDetailComponent } from './broadcast-message-detail.component';
import { BroadcastMessageUpdateComponent } from './broadcast-message-update.component';

@Injectable({ providedIn: 'root' })
export class BroadcastMessageResolve implements Resolve<IBroadcastMessage> {
  constructor(private service: BroadcastMessageService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBroadcastMessage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((broadcastMessage: HttpResponse<BroadcastMessage>) => {
          if (broadcastMessage.body) {
            return of(broadcastMessage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BroadcastMessage());
  }
}

export const broadcastMessageRoute: Routes = [
  {
    path: '',
    component: BroadcastMessageComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BroadcastMessages',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BroadcastMessageDetailComponent,
    resolve: {
      broadcastMessage: BroadcastMessageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BroadcastMessages',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BroadcastMessageUpdateComponent,
    resolve: {
      broadcastMessage: BroadcastMessageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BroadcastMessages',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BroadcastMessageUpdateComponent,
    resolve: {
      broadcastMessage: BroadcastMessageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BroadcastMessages',
    },
    canActivate: [UserRouteAccessService],
  },
];
