import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { BroadcastMessageComponent } from './broadcast-message.component';
import { BroadcastMessageDetailComponent } from './broadcast-message-detail.component';
import { BroadcastMessageUpdateComponent } from './broadcast-message-update.component';
import { BroadcastMessageDeleteDialogComponent } from './broadcast-message-delete-dialog.component';
import { broadcastMessageRoute } from './broadcast-message.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(broadcastMessageRoute)],
  declarations: [
    BroadcastMessageComponent,
    BroadcastMessageDetailComponent,
    BroadcastMessageUpdateComponent,
    BroadcastMessageDeleteDialogComponent,
  ],
  entryComponents: [BroadcastMessageDeleteDialogComponent],
})
export class MisbeBroadcastMessageModule {}
