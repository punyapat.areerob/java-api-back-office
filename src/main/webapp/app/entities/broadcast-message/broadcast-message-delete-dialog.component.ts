import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBroadcastMessage } from 'app/shared/model/broadcast-message.model';
import { BroadcastMessageService } from './broadcast-message.service';

@Component({
  templateUrl: './broadcast-message-delete-dialog.component.html',
})
export class BroadcastMessageDeleteDialogComponent {
  broadcastMessage?: IBroadcastMessage;

  constructor(
    protected broadcastMessageService: BroadcastMessageService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.broadcastMessageService.delete(id).subscribe(() => {
      this.eventManager.broadcast('broadcastMessageListModification');
      this.activeModal.close();
    });
  }
}
