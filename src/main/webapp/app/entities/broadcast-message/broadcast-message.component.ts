import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBroadcastMessage } from 'app/shared/model/broadcast-message.model';
import { BroadcastMessageService } from './broadcast-message.service';
import { BroadcastMessageDeleteDialogComponent } from './broadcast-message-delete-dialog.component';

@Component({
  selector: 'jhi-broadcast-message',
  templateUrl: './broadcast-message.component.html',
})
export class BroadcastMessageComponent implements OnInit, OnDestroy {
  broadcastMessages?: IBroadcastMessage[];
  eventSubscriber?: Subscription;

  constructor(
    protected broadcastMessageService: BroadcastMessageService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.broadcastMessageService.query().subscribe((res: HttpResponse<IBroadcastMessage[]>) => (this.broadcastMessages = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBroadcastMessages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBroadcastMessage): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBroadcastMessages(): void {
    this.eventSubscriber = this.eventManager.subscribe('broadcastMessageListModification', () => this.loadAll());
  }

  delete(broadcastMessage: IBroadcastMessage): void {
    const modalRef = this.modalService.open(BroadcastMessageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.broadcastMessage = broadcastMessage;
  }
}
