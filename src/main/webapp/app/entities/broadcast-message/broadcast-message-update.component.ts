import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBroadcastMessage, BroadcastMessage } from 'app/shared/model/broadcast-message.model';
import { BroadcastMessageService } from './broadcast-message.service';

@Component({
  selector: 'jhi-broadcast-message-update',
  templateUrl: './broadcast-message-update.component.html',
})
export class BroadcastMessageUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    subject: [],
    message: [],
    from: [],
    to: [],
    status: [],
    visible: [],
    creatorTeamId: [],
  });

  constructor(
    protected broadcastMessageService: BroadcastMessageService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ broadcastMessage }) => {
      if (!broadcastMessage.id) {
        const today = moment().startOf('day');
        broadcastMessage.from = today;
        broadcastMessage.to = today;
      }

      this.updateForm(broadcastMessage);
    });
  }

  updateForm(broadcastMessage: IBroadcastMessage): void {
    this.editForm.patchValue({
      id: broadcastMessage.id,
      subject: broadcastMessage.subject,
      message: broadcastMessage.message,
      from: broadcastMessage.from ? broadcastMessage.from.format(DATE_TIME_FORMAT) : null,
      to: broadcastMessage.to ? broadcastMessage.to.format(DATE_TIME_FORMAT) : null,
      status: broadcastMessage.status,
      visible: broadcastMessage.visible,
      creatorTeamId: broadcastMessage.creatorTeamId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const broadcastMessage = this.createFromForm();
    if (broadcastMessage.id !== undefined) {
      this.subscribeToSaveResponse(this.broadcastMessageService.update(broadcastMessage));
    } else {
      this.subscribeToSaveResponse(this.broadcastMessageService.create(broadcastMessage));
    }
  }

  private createFromForm(): IBroadcastMessage {
    return {
      ...new BroadcastMessage(),
      id: this.editForm.get(['id'])!.value,
      subject: this.editForm.get(['subject'])!.value,
      message: this.editForm.get(['message'])!.value,
      from: this.editForm.get(['from'])!.value ? moment(this.editForm.get(['from'])!.value, DATE_TIME_FORMAT) : undefined,
      to: this.editForm.get(['to'])!.value ? moment(this.editForm.get(['to'])!.value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status'])!.value,
      visible: this.editForm.get(['visible'])!.value,
      creatorTeamId: this.editForm.get(['creatorTeamId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBroadcastMessage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
