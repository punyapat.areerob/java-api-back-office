import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseStepState } from 'app/shared/model/case-step-state.model';

@Component({
  selector: 'jhi-case-step-state-detail',
  templateUrl: './case-step-state-detail.component.html',
})
export class CaseStepStateDetailComponent implements OnInit {
  caseStepState: ICaseStepState | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseStepState }) => (this.caseStepState = caseStepState));
  }

  previousState(): void {
    window.history.back();
  }
}
