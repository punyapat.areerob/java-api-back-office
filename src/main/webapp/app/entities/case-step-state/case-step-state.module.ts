import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseStepStateComponent } from './case-step-state.component';
import { CaseStepStateDetailComponent } from './case-step-state-detail.component';
import { CaseStepStateUpdateComponent } from './case-step-state-update.component';
import { CaseStepStateDeleteDialogComponent } from './case-step-state-delete-dialog.component';
import { caseStepStateRoute } from './case-step-state.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseStepStateRoute)],
  declarations: [CaseStepStateComponent, CaseStepStateDetailComponent, CaseStepStateUpdateComponent, CaseStepStateDeleteDialogComponent],
  entryComponents: [CaseStepStateDeleteDialogComponent],
})
export class MisbeCaseStepStateModule {}
