import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseStepState } from 'app/shared/model/case-step-state.model';
import { CaseStepStateService } from './case-step-state.service';
import { CaseStepStateDeleteDialogComponent } from './case-step-state-delete-dialog.component';

@Component({
  selector: 'jhi-case-step-state',
  templateUrl: './case-step-state.component.html',
})
export class CaseStepStateComponent implements OnInit, OnDestroy {
  caseStepStates?: ICaseStepState[];
  eventSubscriber?: Subscription;

  constructor(
    protected caseStepStateService: CaseStepStateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.caseStepStateService.query().subscribe((res: HttpResponse<ICaseStepState[]>) => (this.caseStepStates = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseStepStates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseStepState): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseStepStates(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseStepStateListModification', () => this.loadAll());
  }

  delete(caseStepState: ICaseStepState): void {
    const modalRef = this.modalService.open(CaseStepStateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseStepState = caseStepState;
  }
}
