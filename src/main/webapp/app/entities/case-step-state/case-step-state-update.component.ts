import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICaseStepState, CaseStepState } from 'app/shared/model/case-step-state.model';
import { CaseStepStateService } from './case-step-state.service';
import { ICaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from 'app/entities/case-step/case-step.service';

@Component({
  selector: 'jhi-case-step-state-update',
  templateUrl: './case-step-state-update.component.html',
})
export class CaseStepStateUpdateComponent implements OnInit {
  isSaving = false;
  casesteps: ICaseStep[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    state: [],
    step: [],
  });

  constructor(
    protected caseStepStateService: CaseStepStateService,
    protected caseStepService: CaseStepService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseStepState }) => {
      this.updateForm(caseStepState);

      this.caseStepService.query().subscribe((res: HttpResponse<ICaseStep[]>) => (this.casesteps = res.body || []));
    });
  }

  updateForm(caseStepState: ICaseStepState): void {
    this.editForm.patchValue({
      id: caseStepState.id,
      name: caseStepState.name,
      state: caseStepState.state,
      step: caseStepState.step,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseStepState = this.createFromForm();
    if (caseStepState.id !== undefined) {
      this.subscribeToSaveResponse(this.caseStepStateService.update(caseStepState));
    } else {
      this.subscribeToSaveResponse(this.caseStepStateService.create(caseStepState));
    }
  }

  private createFromForm(): ICaseStepState {
    return {
      ...new CaseStepState(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      state: this.editForm.get(['state'])!.value,
      step: this.editForm.get(['step'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseStepState>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICaseStep): any {
    return item.id;
  }
}
