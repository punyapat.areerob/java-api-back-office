import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaseStepState, CaseStepState } from 'app/shared/model/case-step-state.model';
import { CaseStepStateService } from './case-step-state.service';
import { CaseStepStateComponent } from './case-step-state.component';
import { CaseStepStateDetailComponent } from './case-step-state-detail.component';
import { CaseStepStateUpdateComponent } from './case-step-state-update.component';

@Injectable({ providedIn: 'root' })
export class CaseStepStateResolve implements Resolve<ICaseStepState> {
  constructor(private service: CaseStepStateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaseStepState> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caseStepState: HttpResponse<CaseStepState>) => {
          if (caseStepState.body) {
            return of(caseStepState.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaseStepState());
  }
}

export const caseStepStateRoute: Routes = [
  {
    path: '',
    component: CaseStepStateComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepStates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaseStepStateDetailComponent,
    resolve: {
      caseStepState: CaseStepStateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepStates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaseStepStateUpdateComponent,
    resolve: {
      caseStepState: CaseStepStateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepStates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaseStepStateUpdateComponent,
    resolve: {
      caseStepState: CaseStepStateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseStepStates',
    },
    canActivate: [UserRouteAccessService],
  },
];
