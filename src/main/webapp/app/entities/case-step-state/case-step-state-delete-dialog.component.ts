import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseStepState } from 'app/shared/model/case-step-state.model';
import { CaseStepStateService } from './case-step-state.service';

@Component({
  templateUrl: './case-step-state-delete-dialog.component.html',
})
export class CaseStepStateDeleteDialogComponent {
  caseStepState?: ICaseStepState;

  constructor(
    protected caseStepStateService: CaseStepStateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseStepStateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseStepStateListModification');
      this.activeModal.close();
    });
  }
}
