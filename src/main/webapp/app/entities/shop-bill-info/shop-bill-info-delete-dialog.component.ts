import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShopBillInfo } from 'app/shared/model/shop-bill-info.model';
import { ShopBillInfoService } from './shop-bill-info.service';

@Component({
  templateUrl: './shop-bill-info-delete-dialog.component.html',
})
export class ShopBillInfoDeleteDialogComponent {
  shopBillInfo?: IShopBillInfo;

  constructor(
    protected shopBillInfoService: ShopBillInfoService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.shopBillInfoService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shopBillInfoListModification');
      this.activeModal.close();
    });
  }
}
