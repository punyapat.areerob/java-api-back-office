import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IShopBillInfo, ShopBillInfo } from 'app/shared/model/shop-bill-info.model';
import { ShopBillInfoService } from './shop-bill-info.service';

@Component({
  selector: 'jhi-shop-bill-info-update',
  templateUrl: './shop-bill-info-update.component.html',
})
export class ShopBillInfoUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    vendorId: [],
    companyName: [],
    taxId: [],
    address: [],
    status: [],
    active: [],
  });

  constructor(protected shopBillInfoService: ShopBillInfoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shopBillInfo }) => {
      this.updateForm(shopBillInfo);
    });
  }

  updateForm(shopBillInfo: IShopBillInfo): void {
    this.editForm.patchValue({
      id: shopBillInfo.id,
      vendorId: shopBillInfo.vendorId,
      companyName: shopBillInfo.companyName,
      taxId: shopBillInfo.taxId,
      address: shopBillInfo.address,
      status: shopBillInfo.status,
      active: shopBillInfo.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shopBillInfo = this.createFromForm();
    if (shopBillInfo.id !== undefined) {
      this.subscribeToSaveResponse(this.shopBillInfoService.update(shopBillInfo));
    } else {
      this.subscribeToSaveResponse(this.shopBillInfoService.create(shopBillInfo));
    }
  }

  private createFromForm(): IShopBillInfo {
    return {
      ...new ShopBillInfo(),
      id: this.editForm.get(['id'])!.value,
      vendorId: this.editForm.get(['vendorId'])!.value,
      companyName: this.editForm.get(['companyName'])!.value,
      taxId: this.editForm.get(['taxId'])!.value,
      address: this.editForm.get(['address'])!.value,
      status: this.editForm.get(['status'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShopBillInfo>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
