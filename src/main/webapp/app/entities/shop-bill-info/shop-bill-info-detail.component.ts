import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShopBillInfo } from 'app/shared/model/shop-bill-info.model';

@Component({
  selector: 'jhi-shop-bill-info-detail',
  templateUrl: './shop-bill-info-detail.component.html',
})
export class ShopBillInfoDetailComponent implements OnInit {
  shopBillInfo: IShopBillInfo | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shopBillInfo }) => (this.shopBillInfo = shopBillInfo));
  }

  previousState(): void {
    window.history.back();
  }
}
