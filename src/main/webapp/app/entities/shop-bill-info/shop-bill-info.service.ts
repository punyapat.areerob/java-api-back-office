import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShopBillInfo } from 'app/shared/model/shop-bill-info.model';

type EntityResponseType = HttpResponse<IShopBillInfo>;
type EntityArrayResponseType = HttpResponse<IShopBillInfo[]>;

@Injectable({ providedIn: 'root' })
export class ShopBillInfoService {
  public resourceUrl = SERVER_API_URL + 'api/shop-bill-infos';

  constructor(protected http: HttpClient) {}

  create(shopBillInfo: IShopBillInfo): Observable<EntityResponseType> {
    return this.http.post<IShopBillInfo>(this.resourceUrl, shopBillInfo, { observe: 'response' });
  }

  update(shopBillInfo: IShopBillInfo): Observable<EntityResponseType> {
    return this.http.put<IShopBillInfo>(this.resourceUrl, shopBillInfo, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IShopBillInfo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShopBillInfo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
