import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShopBillInfo } from 'app/shared/model/shop-bill-info.model';
import { ShopBillInfoService } from './shop-bill-info.service';
import { ShopBillInfoDeleteDialogComponent } from './shop-bill-info-delete-dialog.component';

@Component({
  selector: 'jhi-shop-bill-info',
  templateUrl: './shop-bill-info.component.html',
})
export class ShopBillInfoComponent implements OnInit, OnDestroy {
  shopBillInfos?: IShopBillInfo[];
  eventSubscriber?: Subscription;

  constructor(
    protected shopBillInfoService: ShopBillInfoService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.shopBillInfoService.query().subscribe((res: HttpResponse<IShopBillInfo[]>) => (this.shopBillInfos = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShopBillInfos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShopBillInfo): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShopBillInfos(): void {
    this.eventSubscriber = this.eventManager.subscribe('shopBillInfoListModification', () => this.loadAll());
  }

  delete(shopBillInfo: IShopBillInfo): void {
    const modalRef = this.modalService.open(ShopBillInfoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shopBillInfo = shopBillInfo;
  }
}
