import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ShopBillInfoComponent } from './shop-bill-info.component';
import { ShopBillInfoDetailComponent } from './shop-bill-info-detail.component';
import { ShopBillInfoUpdateComponent } from './shop-bill-info-update.component';
import { ShopBillInfoDeleteDialogComponent } from './shop-bill-info-delete-dialog.component';
import { shopBillInfoRoute } from './shop-bill-info.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(shopBillInfoRoute)],
  declarations: [ShopBillInfoComponent, ShopBillInfoDetailComponent, ShopBillInfoUpdateComponent, ShopBillInfoDeleteDialogComponent],
  entryComponents: [ShopBillInfoDeleteDialogComponent],
})
export class MisbeShopBillInfoModule {}
