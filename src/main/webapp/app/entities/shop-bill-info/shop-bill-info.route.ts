import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IShopBillInfo, ShopBillInfo } from 'app/shared/model/shop-bill-info.model';
import { ShopBillInfoService } from './shop-bill-info.service';
import { ShopBillInfoComponent } from './shop-bill-info.component';
import { ShopBillInfoDetailComponent } from './shop-bill-info-detail.component';
import { ShopBillInfoUpdateComponent } from './shop-bill-info-update.component';

@Injectable({ providedIn: 'root' })
export class ShopBillInfoResolve implements Resolve<IShopBillInfo> {
  constructor(private service: ShopBillInfoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IShopBillInfo> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((shopBillInfo: HttpResponse<ShopBillInfo>) => {
          if (shopBillInfo.body) {
            return of(shopBillInfo.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ShopBillInfo());
  }
}

export const shopBillInfoRoute: Routes = [
  {
    path: '',
    component: ShopBillInfoComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShopBillInfos',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ShopBillInfoDetailComponent,
    resolve: {
      shopBillInfo: ShopBillInfoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShopBillInfos',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ShopBillInfoUpdateComponent,
    resolve: {
      shopBillInfo: ShopBillInfoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShopBillInfos',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ShopBillInfoUpdateComponent,
    resolve: {
      shopBillInfo: ShopBillInfoResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShopBillInfos',
    },
    canActivate: [UserRouteAccessService],
  },
];
