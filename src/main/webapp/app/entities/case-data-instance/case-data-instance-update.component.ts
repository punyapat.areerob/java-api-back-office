import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICaseDataInstance, CaseDataInstance } from 'app/shared/model/case-data-instance.model';
import { CaseDataInstanceService } from './case-data-instance.service';

@Component({
  selector: 'jhi-case-data-instance-update',
  templateUrl: './case-data-instance-update.component.html',
})
export class CaseDataInstanceUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(
    protected caseDataInstanceService: CaseDataInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseDataInstance }) => {
      this.updateForm(caseDataInstance);
    });
  }

  updateForm(caseDataInstance: ICaseDataInstance): void {
    this.editForm.patchValue({
      id: caseDataInstance.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseDataInstance = this.createFromForm();
    if (caseDataInstance.id !== undefined) {
      this.subscribeToSaveResponse(this.caseDataInstanceService.update(caseDataInstance));
    } else {
      this.subscribeToSaveResponse(this.caseDataInstanceService.create(caseDataInstance));
    }
  }

  private createFromForm(): ICaseDataInstance {
    return {
      ...new CaseDataInstance(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseDataInstance>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
