import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseDataInstanceComponent } from './case-data-instance.component';
import { CaseDataInstanceDetailComponent } from './case-data-instance-detail.component';
import { CaseDataInstanceUpdateComponent } from './case-data-instance-update.component';
import { CaseDataInstanceDeleteDialogComponent } from './case-data-instance-delete-dialog.component';
import { caseDataInstanceRoute } from './case-data-instance.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseDataInstanceRoute)],
  declarations: [
    CaseDataInstanceComponent,
    CaseDataInstanceDetailComponent,
    CaseDataInstanceUpdateComponent,
    CaseDataInstanceDeleteDialogComponent,
  ],
  entryComponents: [CaseDataInstanceDeleteDialogComponent],
})
export class MisbeCaseDataInstanceModule {}
