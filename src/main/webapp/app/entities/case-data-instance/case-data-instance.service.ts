import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICaseDataInstance } from 'app/shared/model/case-data-instance.model';

type EntityResponseType = HttpResponse<ICaseDataInstance>;
type EntityArrayResponseType = HttpResponse<ICaseDataInstance[]>;

@Injectable({ providedIn: 'root' })
export class CaseDataInstanceService {
  public resourceUrl = SERVER_API_URL + 'api/case-data-instances';

  constructor(protected http: HttpClient) {}

  create(caseDataInstance: ICaseDataInstance): Observable<EntityResponseType> {
    return this.http.post<ICaseDataInstance>(this.resourceUrl, caseDataInstance, { observe: 'response' });
  }

  update(caseDataInstance: ICaseDataInstance): Observable<EntityResponseType> {
    return this.http.put<ICaseDataInstance>(this.resourceUrl, caseDataInstance, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ICaseDataInstance>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICaseDataInstance[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
