import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseDataInstance } from 'app/shared/model/case-data-instance.model';
import { CaseDataInstanceService } from './case-data-instance.service';
import { CaseDataInstanceDeleteDialogComponent } from './case-data-instance-delete-dialog.component';

@Component({
  selector: 'jhi-case-data-instance',
  templateUrl: './case-data-instance.component.html',
})
export class CaseDataInstanceComponent implements OnInit, OnDestroy {
  caseDataInstances?: ICaseDataInstance[];
  eventSubscriber?: Subscription;

  constructor(
    protected caseDataInstanceService: CaseDataInstanceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.caseDataInstanceService.query().subscribe((res: HttpResponse<ICaseDataInstance[]>) => (this.caseDataInstances = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseDataInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseDataInstance): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseDataInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseDataInstanceListModification', () => this.loadAll());
  }

  delete(caseDataInstance: ICaseDataInstance): void {
    const modalRef = this.modalService.open(CaseDataInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseDataInstance = caseDataInstance;
  }
}
