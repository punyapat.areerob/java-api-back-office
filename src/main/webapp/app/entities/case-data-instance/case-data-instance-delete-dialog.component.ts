import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseDataInstance } from 'app/shared/model/case-data-instance.model';
import { CaseDataInstanceService } from './case-data-instance.service';

@Component({
  templateUrl: './case-data-instance-delete-dialog.component.html',
})
export class CaseDataInstanceDeleteDialogComponent {
  caseDataInstance?: ICaseDataInstance;

  constructor(
    protected caseDataInstanceService: CaseDataInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseDataInstanceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseDataInstanceListModification');
      this.activeModal.close();
    });
  }
}
