import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseDataInstance } from 'app/shared/model/case-data-instance.model';

@Component({
  selector: 'jhi-case-data-instance-detail',
  templateUrl: './case-data-instance-detail.component.html',
})
export class CaseDataInstanceDetailComponent implements OnInit {
  caseDataInstance: ICaseDataInstance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseDataInstance }) => (this.caseDataInstance = caseDataInstance));
  }

  previousState(): void {
    window.history.back();
  }
}
