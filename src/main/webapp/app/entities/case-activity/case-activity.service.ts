import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICaseActivity } from 'app/shared/model/case-activity.model';

type EntityResponseType = HttpResponse<ICaseActivity>;
type EntityArrayResponseType = HttpResponse<ICaseActivity[]>;

@Injectable({ providedIn: 'root' })
export class CaseActivityService {
  public resourceUrl = SERVER_API_URL + 'api/case-activities';

  constructor(protected http: HttpClient) {}

  create(caseActivity: ICaseActivity): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caseActivity);
    return this.http
      .post<ICaseActivity>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(caseActivity: ICaseActivity): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caseActivity);
    return this.http
      .put<ICaseActivity>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<ICaseActivity>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICaseActivity[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(caseActivity: ICaseActivity): ICaseActivity {
    const copy: ICaseActivity = Object.assign({}, caseActivity, {
      createdAt: caseActivity.createdAt && caseActivity.createdAt.isValid() ? caseActivity.createdAt.toJSON() : undefined,
      updatedAt: caseActivity.updatedAt && caseActivity.updatedAt.isValid() ? caseActivity.updatedAt.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((caseActivity: ICaseActivity) => {
        caseActivity.createdAt = caseActivity.createdAt ? moment(caseActivity.createdAt) : undefined;
        caseActivity.updatedAt = caseActivity.updatedAt ? moment(caseActivity.updatedAt) : undefined;
      });
    }
    return res;
  }
}
