import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaseActivity, CaseActivity } from 'app/shared/model/case-activity.model';
import { CaseActivityService } from './case-activity.service';
import { CaseActivityComponent } from './case-activity.component';
import { CaseActivityDetailComponent } from './case-activity-detail.component';
import { CaseActivityUpdateComponent } from './case-activity-update.component';

@Injectable({ providedIn: 'root' })
export class CaseActivityResolve implements Resolve<ICaseActivity> {
  constructor(private service: CaseActivityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaseActivity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caseActivity: HttpResponse<CaseActivity>) => {
          if (caseActivity.body) {
            return of(caseActivity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaseActivity());
  }
}

export const caseActivityRoute: Routes = [
  {
    path: '',
    component: CaseActivityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseActivities',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaseActivityDetailComponent,
    resolve: {
      caseActivity: CaseActivityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseActivities',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaseActivityUpdateComponent,
    resolve: {
      caseActivity: CaseActivityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseActivities',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaseActivityUpdateComponent,
    resolve: {
      caseActivity: CaseActivityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseActivities',
    },
    canActivate: [UserRouteAccessService],
  },
];
