import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseActivityComponent } from './case-activity.component';
import { CaseActivityDetailComponent } from './case-activity-detail.component';
import { CaseActivityUpdateComponent } from './case-activity-update.component';
import { CaseActivityDeleteDialogComponent } from './case-activity-delete-dialog.component';
import { caseActivityRoute } from './case-activity.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseActivityRoute)],
  declarations: [CaseActivityComponent, CaseActivityDetailComponent, CaseActivityUpdateComponent, CaseActivityDeleteDialogComponent],
  entryComponents: [CaseActivityDeleteDialogComponent],
})
export class MisbeCaseActivityModule {}
