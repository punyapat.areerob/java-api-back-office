import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseActivity } from 'app/shared/model/case-activity.model';
import { CaseActivityService } from './case-activity.service';
import { CaseActivityDeleteDialogComponent } from './case-activity-delete-dialog.component';

@Component({
  selector: 'jhi-case-activity',
  templateUrl: './case-activity.component.html',
})
export class CaseActivityComponent implements OnInit, OnDestroy {
  caseActivities?: ICaseActivity[];
  eventSubscriber?: Subscription;

  constructor(
    protected caseActivityService: CaseActivityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.caseActivityService.query().subscribe((res: HttpResponse<ICaseActivity[]>) => (this.caseActivities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseActivities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseActivity): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseActivities(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseActivityListModification', () => this.loadAll());
  }

  delete(caseActivity: ICaseActivity): void {
    const modalRef = this.modalService.open(CaseActivityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseActivity = caseActivity;
  }
}
