import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseActivity } from 'app/shared/model/case-activity.model';
import { CaseActivityService } from './case-activity.service';

@Component({
  templateUrl: './case-activity-delete-dialog.component.html',
})
export class CaseActivityDeleteDialogComponent {
  caseActivity?: ICaseActivity;

  constructor(
    protected caseActivityService: CaseActivityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseActivityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseActivityListModification');
      this.activeModal.close();
    });
  }
}
