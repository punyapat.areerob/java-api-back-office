import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICaseActivity, CaseActivity } from 'app/shared/model/case-activity.model';
import { CaseActivityService } from './case-activity.service';

@Component({
  selector: 'jhi-case-activity-update',
  templateUrl: './case-activity-update.component.html',
})
export class CaseActivityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    caseInstanceId: [],
    event: [],
    remark: [],
    createdBy: [],
    updatedBy: [],
    createdAt: [],
    updatedAt: [],
  });

  constructor(protected caseActivityService: CaseActivityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseActivity }) => {
      if (!caseActivity.id) {
        const today = moment().startOf('day');
        caseActivity.createdAt = today;
        caseActivity.updatedAt = today;
      }

      this.updateForm(caseActivity);
    });
  }

  updateForm(caseActivity: ICaseActivity): void {
    this.editForm.patchValue({
      id: caseActivity.id,
      caseInstanceId: caseActivity.caseInstanceId,
      event: caseActivity.event,
      remark: caseActivity.remark,
      createdBy: caseActivity.createdBy,
      updatedBy: caseActivity.updatedBy,
      createdAt: caseActivity.createdAt ? caseActivity.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: caseActivity.updatedAt ? caseActivity.updatedAt.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseActivity = this.createFromForm();
    if (caseActivity.id !== undefined) {
      this.subscribeToSaveResponse(this.caseActivityService.update(caseActivity));
    } else {
      this.subscribeToSaveResponse(this.caseActivityService.create(caseActivity));
    }
  }

  private createFromForm(): ICaseActivity {
    return {
      ...new CaseActivity(),
      id: this.editForm.get(['id'])!.value,
      caseInstanceId: this.editForm.get(['caseInstanceId'])!.value,
      event: this.editForm.get(['event'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseActivity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
