import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseActivity } from 'app/shared/model/case-activity.model';

@Component({
  selector: 'jhi-case-activity-detail',
  templateUrl: './case-activity-detail.component.html',
})
export class CaseActivityDetailComponent implements OnInit {
  caseActivity: ICaseActivity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseActivity }) => (this.caseActivity = caseActivity));
  }

  previousState(): void {
    window.history.back();
  }
}
