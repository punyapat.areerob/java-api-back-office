import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'title',
        loadChildren: () => import('./title/title.module').then(m => m.MisbeTitleModule),
      },
      {
        path: 'team',
        loadChildren: () => import('./team/team.module').then(m => m.MisbeTeamModule),
      },
      {
        path: 'staff',
        loadChildren: () => import('./staff/staff.module').then(m => m.MisbeStaffModule),
      },
      {
        path: 'broadcast-message',
        loadChildren: () => import('./broadcast-message/broadcast-message.module').then(m => m.MisbeBroadcastMessageModule),
      },
      {
        path: 'email-template-meta',
        loadChildren: () => import('./email-template-meta/email-template-meta.module').then(m => m.MisbeEmailTemplateMetaModule),
      },
      {
        path: 'email-template',
        loadChildren: () => import('./email-template/email-template.module').then(m => m.MisbeEmailTemplateModule),
      },
      {
        path: 'phone-prefix',
        loadChildren: () => import('./phone-prefix/phone-prefix.module').then(m => m.MisbePhonePrefixModule),
      },
      {
        path: 'nationalities',
        loadChildren: () => import('./nationalities/nationalities.module').then(m => m.MisbeNationalitiesModule),
      },
      {
        path: 'gender',
        loadChildren: () => import('./gender/gender.module').then(m => m.MisbeGenderModule),
      },
      {
        path: 'blood-type',
        loadChildren: () => import('./blood-type/blood-type.module').then(m => m.MisbeBloodTypeModule),
      },
      {
        path: 'religion',
        loadChildren: () => import('./religion/religion.module').then(m => m.MisbeReligionModule),
      },
      {
        path: 'occupation',
        loadChildren: () => import('./occupation/occupation.module').then(m => m.MisbeOccupationModule),
      },
      {
        path: 'business-industry',
        loadChildren: () => import('./business-industry/business-industry.module').then(m => m.MisbeBusinessIndustryModule),
      },
      {
        path: 'country',
        loadChildren: () => import('./country/country.module').then(m => m.MisbeCountryModule),
      },
      {
        path: 'case-template',
        loadChildren: () => import('./case-template/case-template.module').then(m => m.MisbeCaseTemplateModule),
      },
      {
        path: 'case-instance',
        loadChildren: () => import('./case-instance/case-instance.module').then(m => m.MisbeCaseInstanceModule),
      },
      {
        path: 'case-step',
        loadChildren: () => import('./case-step/case-step.module').then(m => m.MisbeCaseStepModule),
      },
      {
        path: 'case-step-instance',
        loadChildren: () => import('./case-step-instance/case-step-instance.module').then(m => m.MisbeCaseStepInstanceModule),
      },
      {
        path: 'case-step-state',
        loadChildren: () => import('./case-step-state/case-step-state.module').then(m => m.MisbeCaseStepStateModule),
      },
      {
        path: 'action',
        loadChildren: () => import('./action/action.module').then(m => m.MisbeActionModule),
      },
      {
        path: 'action-instance',
        loadChildren: () => import('./action-instance/action-instance.module').then(m => m.MisbeActionInstanceModule),
      },
      {
        path: 'case-data-instance',
        loadChildren: () => import('./case-data-instance/case-data-instance.module').then(m => m.MisbeCaseDataInstanceModule),
      },
      {
        path: 'case-activity',
        loadChildren: () => import('./case-activity/case-activity.module').then(m => m.MisbeCaseActivityModule),
      },
      {
        path: 'input-field',
        loadChildren: () => import('./input-field/input-field.module').then(m => m.MisbeInputFieldModule),
      },
      {
        path: 'verify-field',
        loadChildren: () => import('./verify-field/verify-field.module').then(m => m.MisbeVerifyFieldModule),
      },
      {
        path: 'prospect-data',
        loadChildren: () => import('./prospect-data/prospect-data.module').then(m => m.MisbeProspectDataModule),
      },
      {
        path: 'email-transaction',
        loadChildren: () => import('./email-transaction/email-transaction.module').then(m => m.MisbeEmailTransactionModule),
      },
      {
        path: 'accommodation-type',
        loadChildren: () => import('./accommodation-type/accommodation-type.module').then(m => m.MisbeAccommodationTypeModule),
      },
      {
        path: 'channel-know',
        loadChildren: () => import('./channel-know/channel-know.module').then(m => m.MisbeChannelKnowModule),
      },
      {
        path: 'visa-type',
        loadChildren: () => import('./visa-type/visa-type.module').then(m => m.MisbeVisaTypeModule),
      },
      {
        path: 'member-remark-instance',
        loadChildren: () => import('./member-remark-instance/member-remark-instance.module').then(m => m.MisbeMemberRemarkInstanceModule),
      },
      {
        path: 'member-remark-tag',
        loadChildren: () => import('./member-remark-tag/member-remark-tag.module').then(m => m.MisbeMemberRemarkTagModule),
      },
      {
        path: 'step-owner',
        loadChildren: () => import('./step-owner/step-owner.module').then(m => m.MisbeStepOwnerModule),
      },
      {
        path: 'vendor',
        loadChildren: () => import('./vendor/vendor.module').then(m => m.MisbeVendorModule),
      },
      {
        path: 'shop',
        loadChildren: () => import('./shop/shop.module').then(m => m.MisbeShopModule),
      },
      {
        path: 'shop-bill-info',
        loadChildren: () => import('./shop-bill-info/shop-bill-info.module').then(m => m.MisbeShopBillInfoModule),
      },
      {
        path: 'vendors-contact',
        loadChildren: () => import('./vendors-contact/vendors-contact.module').then(m => m.MisbeVendorsContactModule),
      },
      {
        path: 'contact-tag-master',
        loadChildren: () => import('./contact-tag-master/contact-tag-master.module').then(m => m.MisbeContactTagMasterModule),
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.MisbeProductModule),
      },
      {
        path: 'product-variant-group',
        loadChildren: () => import('./product-variant-group/product-variant-group.module').then(m => m.MisbeProductVariantGroupModule),
      },
      {
        path: 'product-variant-value',
        loadChildren: () => import('./product-variant-value/product-variant-value.module').then(m => m.MisbeProductVariantValueModule),
      },
      {
        path: 'service',
        loadChildren: () => import('./service/service.module').then(m => m.MisbeServiceModule),
      },
      {
        path: 'product-price-rule',
        loadChildren: () => import('./product-price-rule/product-price-rule.module').then(m => m.MisbeProductPriceRuleModule),
      },
      {
        path: 'product-category',
        loadChildren: () => import('./product-category/product-category.module').then(m => m.MisbeProductCategoryModule),
      },
      {
        path: 'sub-product-category',
        loadChildren: () => import('./sub-product-category/sub-product-category.module').then(m => m.MisbeSubProductCategoryModule),
      },
      {
        path: 'job-description',
        loadChildren: () => import('./job-description/job-description.module').then(m => m.MisbeJobDescriptionModule),
      },
      {
        path: 'job-positions',
        loadChildren: () => import('./job-positions/job-positions.module').then(m => m.MisbeJobPositionsModule),
      },
      {
        path: 'job-application',
        loadChildren: () => import('./job-application/job-application.module').then(m => m.MisbeJobApplicationModule),
      },
      {
        path: 'card',
        loadChildren: () => import('./card/card.module').then(m => m.MisbeCardModule),
      },
      {
        path: 'card-privilege',
        loadChildren: () => import('./card-privilege/card-privilege.module').then(m => m.MisbeCardPrivilegeModule),
      },
      {
        path: 'privilege',
        loadChildren: () => import('./privilege/privilege.module').then(m => m.MisbePrivilegeModule),
      },
      {
        path: 'department',
        loadChildren: () => import('./department/department.module').then(m => m.MisbeDepartmentModule),
      },
      {
        path: 'booking-status',
        loadChildren: () => import('./booking-status/booking-status.module').then(m => m.MisbeBookingStatusModule),
      },
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then(m => m.MisbeBookingModule),
      },
      {
        path: 'booking-services',
        loadChildren: () => import('./booking-services/booking-services.module').then(m => m.MisbeBookingServicesModule),
      },
      {
        path: 'booking-service-member',
        loadChildren: () => import('./booking-service-member/booking-service-member.module').then(m => m.MisbeBookingServiceMemberModule),
      },
      {
        path: 'booking-guest',
        loadChildren: () => import('./booking-guest/booking-guest.module').then(m => m.MisbeBookingGuestModule),
      },
      {
        path: 'job-assignment',
        loadChildren: () => import('./job-assignment/job-assignment.module').then(m => m.MisbeJobAssignmentModule),
      },
      {
        path: 'agent',
        loadChildren: () => import('./agent/agent.module').then(m => m.MisbeAgentModule),
      },
      {
        path: 'agent-commission-rate',
        loadChildren: () => import('./agent-commission-rate/agent-commission-rate.module').then(m => m.MisbeAgentCommissionRateModule),
      },
      {
        path: 'agent-commission-rate-config',
        loadChildren: () =>
          import('./agent-commission-rate-config/agent-commission-rate-config.module').then(m => m.MisbeAgentCommissionRateConfigModule),
      },
      {
        path: 'agent-member-activation',
        loadChildren: () =>
          import('./agent-member-activation/agent-member-activation.module').then(m => m.MisbeAgentMemberActivationModule),
      },
      {
        path: 'json-string',
        loadChildren: () => import('./json-string/json-string.module').then(m => m.MisbeJsonStringModule),
      },
      {
        path: 'json-integer',
        loadChildren: () => import('./json-integer/json-integer.module').then(m => m.MisbeJsonIntegerModule),
      },
      {
        path: 'agent-contract',
        loadChildren: () => import('./agent-contract/agent-contract.module').then(m => m.MisbeAgentContractModule),
      },
      {
        path: 'vendor-contract',
        loadChildren: () => import('./vendor-contract/vendor-contract.module').then(m => m.MisbeVendorContractModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MisbeEntityModule {}
