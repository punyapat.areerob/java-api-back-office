import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaseTemplate } from 'app/shared/model/case-template.model';

@Component({
  selector: 'jhi-case-template-detail',
  templateUrl: './case-template-detail.component.html',
})
export class CaseTemplateDetailComponent implements OnInit {
  caseTemplate: ICaseTemplate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseTemplate }) => (this.caseTemplate = caseTemplate));
  }

  previousState(): void {
    window.history.back();
  }
}
