import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICaseTemplate, CaseTemplate } from 'app/shared/model/case-template.model';
import { CaseTemplateService } from './case-template.service';

@Component({
  selector: 'jhi-case-template-update',
  templateUrl: './case-template-update.component.html',
})
export class CaseTemplateUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    status: [],
  });

  constructor(protected caseTemplateService: CaseTemplateService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caseTemplate }) => {
      this.updateForm(caseTemplate);
    });
  }

  updateForm(caseTemplate: ICaseTemplate): void {
    this.editForm.patchValue({
      id: caseTemplate.id,
      name: caseTemplate.name,
      status: caseTemplate.status,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caseTemplate = this.createFromForm();
    if (caseTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.caseTemplateService.update(caseTemplate));
    } else {
      this.subscribeToSaveResponse(this.caseTemplateService.create(caseTemplate));
    }
  }

  private createFromForm(): ICaseTemplate {
    return {
      ...new CaseTemplate(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaseTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
