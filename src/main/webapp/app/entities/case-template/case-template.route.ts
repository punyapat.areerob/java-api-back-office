import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaseTemplate, CaseTemplate } from 'app/shared/model/case-template.model';
import { CaseTemplateService } from './case-template.service';
import { CaseTemplateComponent } from './case-template.component';
import { CaseTemplateDetailComponent } from './case-template-detail.component';
import { CaseTemplateUpdateComponent } from './case-template-update.component';

@Injectable({ providedIn: 'root' })
export class CaseTemplateResolve implements Resolve<ICaseTemplate> {
  constructor(private service: CaseTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaseTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caseTemplate: HttpResponse<CaseTemplate>) => {
          if (caseTemplate.body) {
            return of(caseTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaseTemplate());
  }
}

export const caseTemplateRoute: Routes = [
  {
    path: '',
    component: CaseTemplateComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaseTemplateDetailComponent,
    resolve: {
      caseTemplate: CaseTemplateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaseTemplateUpdateComponent,
    resolve: {
      caseTemplate: CaseTemplateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaseTemplateUpdateComponent,
    resolve: {
      caseTemplate: CaseTemplateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CaseTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
];
