import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { CaseTemplateComponent } from './case-template.component';
import { CaseTemplateDetailComponent } from './case-template-detail.component';
import { CaseTemplateUpdateComponent } from './case-template-update.component';
import { CaseTemplateDeleteDialogComponent } from './case-template-delete-dialog.component';
import { caseTemplateRoute } from './case-template.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(caseTemplateRoute)],
  declarations: [CaseTemplateComponent, CaseTemplateDetailComponent, CaseTemplateUpdateComponent, CaseTemplateDeleteDialogComponent],
  entryComponents: [CaseTemplateDeleteDialogComponent],
})
export class MisbeCaseTemplateModule {}
