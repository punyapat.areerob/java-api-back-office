import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaseTemplate } from 'app/shared/model/case-template.model';
import { CaseTemplateService } from './case-template.service';
import { CaseTemplateDeleteDialogComponent } from './case-template-delete-dialog.component';

@Component({
  selector: 'jhi-case-template',
  templateUrl: './case-template.component.html',
})
export class CaseTemplateComponent implements OnInit, OnDestroy {
  caseTemplates?: ICaseTemplate[];
  eventSubscriber?: Subscription;

  constructor(
    protected caseTemplateService: CaseTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.caseTemplateService.query().subscribe((res: HttpResponse<ICaseTemplate[]>) => (this.caseTemplates = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaseTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaseTemplate): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaseTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('caseTemplateListModification', () => this.loadAll());
  }

  delete(caseTemplate: ICaseTemplate): void {
    const modalRef = this.modalService.open(CaseTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caseTemplate = caseTemplate;
  }
}
