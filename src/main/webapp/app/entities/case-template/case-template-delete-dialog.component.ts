import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaseTemplate } from 'app/shared/model/case-template.model';
import { CaseTemplateService } from './case-template.service';

@Component({
  templateUrl: './case-template-delete-dialog.component.html',
})
export class CaseTemplateDeleteDialogComponent {
  caseTemplate?: ICaseTemplate;

  constructor(
    protected caseTemplateService: CaseTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.caseTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caseTemplateListModification');
      this.activeModal.close();
    });
  }
}
