import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEmailTemplate, EmailTemplate } from 'app/shared/model/email-template.model';
import { EmailTemplateService } from './email-template.service';

@Component({
  selector: 'jhi-email-template-update',
  templateUrl: './email-template-update.component.html',
})
export class EmailTemplateUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    description: [],
    content: [],
  });

  constructor(protected emailTemplateService: EmailTemplateService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emailTemplate }) => {
      this.updateForm(emailTemplate);
    });
  }

  updateForm(emailTemplate: IEmailTemplate): void {
    this.editForm.patchValue({
      id: emailTemplate.id,
      name: emailTemplate.name,
      description: emailTemplate.description,
      content: emailTemplate.content,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const emailTemplate = this.createFromForm();
    if (emailTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.emailTemplateService.update(emailTemplate));
    } else {
      this.subscribeToSaveResponse(this.emailTemplateService.create(emailTemplate));
    }
  }

  private createFromForm(): IEmailTemplate {
    return {
      ...new EmailTemplate(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      content: this.editForm.get(['content'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmailTemplate>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
