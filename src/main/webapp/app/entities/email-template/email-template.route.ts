import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEmailTemplate, EmailTemplate } from 'app/shared/model/email-template.model';
import { EmailTemplateService } from './email-template.service';
import { EmailTemplateComponent } from './email-template.component';
import { EmailTemplateDetailComponent } from './email-template-detail.component';
import { EmailTemplateUpdateComponent } from './email-template-update.component';

@Injectable({ providedIn: 'root' })
export class EmailTemplateResolve implements Resolve<IEmailTemplate> {
  constructor(private service: EmailTemplateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEmailTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((emailTemplate: HttpResponse<EmailTemplate>) => {
          if (emailTemplate.body) {
            return of(emailTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EmailTemplate());
  }
}

export const emailTemplateRoute: Routes = [
  {
    path: '',
    component: EmailTemplateComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmailTemplateDetailComponent,
    resolve: {
      emailTemplate: EmailTemplateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmailTemplateUpdateComponent,
    resolve: {
      emailTemplate: EmailTemplateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmailTemplateUpdateComponent,
    resolve: {
      emailTemplate: EmailTemplateResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'EmailTemplates',
    },
    canActivate: [UserRouteAccessService],
  },
];
