import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { EmailTemplateComponent } from './email-template.component';
import { EmailTemplateDetailComponent } from './email-template-detail.component';
import { EmailTemplateUpdateComponent } from './email-template-update.component';
import { EmailTemplateDeleteDialogComponent } from './email-template-delete-dialog.component';
import { emailTemplateRoute } from './email-template.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(emailTemplateRoute)],
  declarations: [EmailTemplateComponent, EmailTemplateDetailComponent, EmailTemplateUpdateComponent, EmailTemplateDeleteDialogComponent],
  entryComponents: [EmailTemplateDeleteDialogComponent],
})
export class MisbeEmailTemplateModule {}
