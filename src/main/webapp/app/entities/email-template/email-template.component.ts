import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmailTemplate } from 'app/shared/model/email-template.model';
import { EmailTemplateService } from './email-template.service';
import { EmailTemplateDeleteDialogComponent } from './email-template-delete-dialog.component';

@Component({
  selector: 'jhi-email-template',
  templateUrl: './email-template.component.html',
})
export class EmailTemplateComponent implements OnInit, OnDestroy {
  emailTemplates?: IEmailTemplate[];
  eventSubscriber?: Subscription;

  constructor(
    protected emailTemplateService: EmailTemplateService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.emailTemplateService.query().subscribe((res: HttpResponse<IEmailTemplate[]>) => (this.emailTemplates = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEmailTemplates();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IEmailTemplate): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEmailTemplates(): void {
    this.eventSubscriber = this.eventManager.subscribe('emailTemplateListModification', () => this.loadAll());
  }

  delete(emailTemplate: IEmailTemplate): void {
    const modalRef = this.modalService.open(EmailTemplateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.emailTemplate = emailTemplate;
  }
}
