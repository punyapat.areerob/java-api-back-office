import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEmailTemplate } from 'app/shared/model/email-template.model';
import { EmailTemplateService } from './email-template.service';

@Component({
  templateUrl: './email-template-delete-dialog.component.html',
})
export class EmailTemplateDeleteDialogComponent {
  emailTemplate?: IEmailTemplate;

  constructor(
    protected emailTemplateService: EmailTemplateService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.emailTemplateService.delete(id).subscribe(() => {
      this.eventManager.broadcast('emailTemplateListModification');
      this.activeModal.close();
    });
  }
}
