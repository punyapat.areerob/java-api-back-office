import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ProductPriceRuleComponent } from './product-price-rule.component';
import { ProductPriceRuleDetailComponent } from './product-price-rule-detail.component';
import { ProductPriceRuleUpdateComponent } from './product-price-rule-update.component';
import { ProductPriceRuleDeleteDialogComponent } from './product-price-rule-delete-dialog.component';
import { productPriceRuleRoute } from './product-price-rule.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(productPriceRuleRoute)],
  declarations: [
    ProductPriceRuleComponent,
    ProductPriceRuleDetailComponent,
    ProductPriceRuleUpdateComponent,
    ProductPriceRuleDeleteDialogComponent,
  ],
  entryComponents: [ProductPriceRuleDeleteDialogComponent],
})
export class MisbeProductPriceRuleModule {}
