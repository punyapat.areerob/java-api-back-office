import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductPriceRule } from 'app/shared/model/product-price-rule.model';

@Component({
  selector: 'jhi-product-price-rule-detail',
  templateUrl: './product-price-rule-detail.component.html',
})
export class ProductPriceRuleDetailComponent implements OnInit {
  productPriceRule: IProductPriceRule | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productPriceRule }) => (this.productPriceRule = productPriceRule));
  }

  previousState(): void {
    window.history.back();
  }
}
