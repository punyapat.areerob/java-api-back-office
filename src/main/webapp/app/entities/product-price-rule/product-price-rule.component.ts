import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductPriceRule } from 'app/shared/model/product-price-rule.model';
import { ProductPriceRuleService } from './product-price-rule.service';
import { ProductPriceRuleDeleteDialogComponent } from './product-price-rule-delete-dialog.component';

@Component({
  selector: 'jhi-product-price-rule',
  templateUrl: './product-price-rule.component.html',
})
export class ProductPriceRuleComponent implements OnInit, OnDestroy {
  productPriceRules?: IProductPriceRule[];
  eventSubscriber?: Subscription;

  constructor(
    protected productPriceRuleService: ProductPriceRuleService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.productPriceRuleService.query().subscribe((res: HttpResponse<IProductPriceRule[]>) => (this.productPriceRules = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductPriceRules();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductPriceRule): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductPriceRules(): void {
    this.eventSubscriber = this.eventManager.subscribe('productPriceRuleListModification', () => this.loadAll());
  }

  delete(productPriceRule: IProductPriceRule): void {
    const modalRef = this.modalService.open(ProductPriceRuleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productPriceRule = productPriceRule;
  }
}
