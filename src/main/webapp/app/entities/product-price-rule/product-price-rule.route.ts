import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductPriceRule, ProductPriceRule } from 'app/shared/model/product-price-rule.model';
import { ProductPriceRuleService } from './product-price-rule.service';
import { ProductPriceRuleComponent } from './product-price-rule.component';
import { ProductPriceRuleDetailComponent } from './product-price-rule-detail.component';
import { ProductPriceRuleUpdateComponent } from './product-price-rule-update.component';

@Injectable({ providedIn: 'root' })
export class ProductPriceRuleResolve implements Resolve<IProductPriceRule> {
  constructor(private service: ProductPriceRuleService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductPriceRule> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productPriceRule: HttpResponse<ProductPriceRule>) => {
          if (productPriceRule.body) {
            return of(productPriceRule.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductPriceRule());
  }
}

export const productPriceRuleRoute: Routes = [
  {
    path: '',
    component: ProductPriceRuleComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductPriceRules',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductPriceRuleDetailComponent,
    resolve: {
      productPriceRule: ProductPriceRuleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductPriceRules',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductPriceRuleUpdateComponent,
    resolve: {
      productPriceRule: ProductPriceRuleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductPriceRules',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductPriceRuleUpdateComponent,
    resolve: {
      productPriceRule: ProductPriceRuleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductPriceRules',
    },
    canActivate: [UserRouteAccessService],
  },
];
