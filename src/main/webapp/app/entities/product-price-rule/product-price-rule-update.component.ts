import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductPriceRule, ProductPriceRule } from 'app/shared/model/product-price-rule.model';
import { ProductPriceRuleService } from './product-price-rule.service';

@Component({
  selector: 'jhi-product-price-rule-update',
  templateUrl: './product-price-rule-update.component.html',
})
export class ProductPriceRuleUpdateComponent implements OnInit {
  isSaving = false;
  activeStartDateDp: any;
  activeEndDateDp: any;

  editForm = this.fb.group({
    id: [],
    productId: [],
    usedQuota: [],
    priceSale: [],
    priceBase: [],
    status: [],
    activeStartDate: [],
    activeEndDate: [],
    active: [],
  });

  constructor(
    protected productPriceRuleService: ProductPriceRuleService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productPriceRule }) => {
      this.updateForm(productPriceRule);
    });
  }

  updateForm(productPriceRule: IProductPriceRule): void {
    this.editForm.patchValue({
      id: productPriceRule.id,
      productId: productPriceRule.productId,
      usedQuota: productPriceRule.usedQuota,
      priceSale: productPriceRule.priceSale,
      priceBase: productPriceRule.priceBase,
      status: productPriceRule.status,
      activeStartDate: productPriceRule.activeStartDate,
      activeEndDate: productPriceRule.activeEndDate,
      active: productPriceRule.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productPriceRule = this.createFromForm();
    if (productPriceRule.id !== undefined) {
      this.subscribeToSaveResponse(this.productPriceRuleService.update(productPriceRule));
    } else {
      this.subscribeToSaveResponse(this.productPriceRuleService.create(productPriceRule));
    }
  }

  private createFromForm(): IProductPriceRule {
    return {
      ...new ProductPriceRule(),
      id: this.editForm.get(['id'])!.value,
      productId: this.editForm.get(['productId'])!.value,
      usedQuota: this.editForm.get(['usedQuota'])!.value,
      priceSale: this.editForm.get(['priceSale'])!.value,
      priceBase: this.editForm.get(['priceBase'])!.value,
      status: this.editForm.get(['status'])!.value,
      activeStartDate: this.editForm.get(['activeStartDate'])!.value,
      activeEndDate: this.editForm.get(['activeEndDate'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductPriceRule>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
