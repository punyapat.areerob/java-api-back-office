import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductPriceRule } from 'app/shared/model/product-price-rule.model';

type EntityResponseType = HttpResponse<IProductPriceRule>;
type EntityArrayResponseType = HttpResponse<IProductPriceRule[]>;

@Injectable({ providedIn: 'root' })
export class ProductPriceRuleService {
  public resourceUrl = SERVER_API_URL + 'api/product-price-rules';

  constructor(protected http: HttpClient) {}

  create(productPriceRule: IProductPriceRule): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(productPriceRule);
    return this.http
      .post<IProductPriceRule>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(productPriceRule: IProductPriceRule): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(productPriceRule);
    return this.http
      .put<IProductPriceRule>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IProductPriceRule>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IProductPriceRule[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(productPriceRule: IProductPriceRule): IProductPriceRule {
    const copy: IProductPriceRule = Object.assign({}, productPriceRule, {
      activeStartDate:
        productPriceRule.activeStartDate && productPriceRule.activeStartDate.isValid()
          ? productPriceRule.activeStartDate.format(DATE_FORMAT)
          : undefined,
      activeEndDate:
        productPriceRule.activeEndDate && productPriceRule.activeEndDate.isValid()
          ? productPriceRule.activeEndDate.format(DATE_FORMAT)
          : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.activeStartDate = res.body.activeStartDate ? moment(res.body.activeStartDate) : undefined;
      res.body.activeEndDate = res.body.activeEndDate ? moment(res.body.activeEndDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((productPriceRule: IProductPriceRule) => {
        productPriceRule.activeStartDate = productPriceRule.activeStartDate ? moment(productPriceRule.activeStartDate) : undefined;
        productPriceRule.activeEndDate = productPriceRule.activeEndDate ? moment(productPriceRule.activeEndDate) : undefined;
      });
    }
    return res;
  }
}
