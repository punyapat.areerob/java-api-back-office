import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductPriceRule } from 'app/shared/model/product-price-rule.model';
import { ProductPriceRuleService } from './product-price-rule.service';

@Component({
  templateUrl: './product-price-rule-delete-dialog.component.html',
})
export class ProductPriceRuleDeleteDialogComponent {
  productPriceRule?: IProductPriceRule;

  constructor(
    protected productPriceRuleService: ProductPriceRuleService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.productPriceRuleService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productPriceRuleListModification');
      this.activeModal.close();
    });
  }
}
