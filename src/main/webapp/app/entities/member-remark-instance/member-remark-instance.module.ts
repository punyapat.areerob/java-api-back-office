import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { MemberRemarkInstanceComponent } from './member-remark-instance.component';
import { MemberRemarkInstanceDetailComponent } from './member-remark-instance-detail.component';
import { MemberRemarkInstanceUpdateComponent } from './member-remark-instance-update.component';
import { MemberRemarkInstanceDeleteDialogComponent } from './member-remark-instance-delete-dialog.component';
import { memberRemarkInstanceRoute } from './member-remark-instance.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(memberRemarkInstanceRoute)],
  declarations: [
    MemberRemarkInstanceComponent,
    MemberRemarkInstanceDetailComponent,
    MemberRemarkInstanceUpdateComponent,
    MemberRemarkInstanceDeleteDialogComponent,
  ],
  entryComponents: [MemberRemarkInstanceDeleteDialogComponent],
})
export class MisbeMemberRemarkInstanceModule {}
