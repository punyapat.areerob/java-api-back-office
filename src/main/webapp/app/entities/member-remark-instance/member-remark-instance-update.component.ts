import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMemberRemarkInstance, MemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';
import { MemberRemarkInstanceService } from './member-remark-instance.service';

@Component({
  selector: 'jhi-member-remark-instance-update',
  templateUrl: './member-remark-instance-update.component.html',
})
export class MemberRemarkInstanceUpdateComponent implements OnInit {
  isSaving = false;
  createDateDp: any;
  updateDateDp: any;

  editForm = this.fb.group({
    id: [],
    memberId: [],
    text: [],
    tagId: [],
    tagName: [],
    accessTeams: [],
    createDate: [],
    createBy: [],
    updateDate: [],
    updateBy: [],
    active: [],
  });

  constructor(
    protected memberRemarkInstanceService: MemberRemarkInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ memberRemarkInstance }) => {
      this.updateForm(memberRemarkInstance);
    });
  }

  updateForm(memberRemarkInstance: IMemberRemarkInstance): void {
    this.editForm.patchValue({
      id: memberRemarkInstance.id,
      memberId: memberRemarkInstance.memberId,
      text: memberRemarkInstance.text,
      tagId: memberRemarkInstance.tagId,
      tagName: memberRemarkInstance.tagName,
      accessTeams: memberRemarkInstance.accessTeams,
      createDate: memberRemarkInstance.createDate,
      createBy: memberRemarkInstance.createBy,
      updateDate: memberRemarkInstance.updateDate,
      updateBy: memberRemarkInstance.updateBy,
      active: memberRemarkInstance.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const memberRemarkInstance = this.createFromForm();
    if (memberRemarkInstance.id !== undefined) {
      this.subscribeToSaveResponse(this.memberRemarkInstanceService.update(memberRemarkInstance));
    } else {
      this.subscribeToSaveResponse(this.memberRemarkInstanceService.create(memberRemarkInstance));
    }
  }

  private createFromForm(): IMemberRemarkInstance {
    return {
      ...new MemberRemarkInstance(),
      id: this.editForm.get(['id'])!.value,
      memberId: this.editForm.get(['memberId'])!.value,
      text: this.editForm.get(['text'])!.value,
      tagId: this.editForm.get(['tagId'])!.value,
      tagName: this.editForm.get(['tagName'])!.value,
      accessTeams: this.editForm.get(['accessTeams'])!.value,
      createDate: this.editForm.get(['createDate'])!.value,
      createBy: this.editForm.get(['createBy'])!.value,
      updateDate: this.editForm.get(['updateDate'])!.value,
      updateBy: this.editForm.get(['updateBy'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMemberRemarkInstance>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
