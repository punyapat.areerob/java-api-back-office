import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';

type EntityResponseType = HttpResponse<IMemberRemarkInstance>;
type EntityArrayResponseType = HttpResponse<IMemberRemarkInstance[]>;

@Injectable({ providedIn: 'root' })
export class MemberRemarkInstanceService {
  public resourceUrl = SERVER_API_URL + 'api/member-remark-instances';

  constructor(protected http: HttpClient) {}

  create(memberRemarkInstance: IMemberRemarkInstance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(memberRemarkInstance);
    return this.http
      .post<IMemberRemarkInstance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(memberRemarkInstance: IMemberRemarkInstance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(memberRemarkInstance);
    return this.http
      .put<IMemberRemarkInstance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IMemberRemarkInstance>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMemberRemarkInstance[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(memberRemarkInstance: IMemberRemarkInstance): IMemberRemarkInstance {
    const copy: IMemberRemarkInstance = Object.assign({}, memberRemarkInstance, {
      createDate:
        memberRemarkInstance.createDate && memberRemarkInstance.createDate.isValid()
          ? memberRemarkInstance.createDate.format(DATE_FORMAT)
          : undefined,
      updateDate:
        memberRemarkInstance.updateDate && memberRemarkInstance.updateDate.isValid()
          ? memberRemarkInstance.updateDate.format(DATE_FORMAT)
          : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createDate = res.body.createDate ? moment(res.body.createDate) : undefined;
      res.body.updateDate = res.body.updateDate ? moment(res.body.updateDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((memberRemarkInstance: IMemberRemarkInstance) => {
        memberRemarkInstance.createDate = memberRemarkInstance.createDate ? moment(memberRemarkInstance.createDate) : undefined;
        memberRemarkInstance.updateDate = memberRemarkInstance.updateDate ? moment(memberRemarkInstance.updateDate) : undefined;
      });
    }
    return res;
  }
}
