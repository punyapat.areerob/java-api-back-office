import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';
import { MemberRemarkInstanceService } from './member-remark-instance.service';
import { MemberRemarkInstanceDeleteDialogComponent } from './member-remark-instance-delete-dialog.component';

@Component({
  selector: 'jhi-member-remark-instance',
  templateUrl: './member-remark-instance.component.html',
})
export class MemberRemarkInstanceComponent implements OnInit, OnDestroy {
  memberRemarkInstances?: IMemberRemarkInstance[];
  eventSubscriber?: Subscription;

  constructor(
    protected memberRemarkInstanceService: MemberRemarkInstanceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.memberRemarkInstanceService
      .query()
      .subscribe((res: HttpResponse<IMemberRemarkInstance[]>) => (this.memberRemarkInstances = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMemberRemarkInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMemberRemarkInstance): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMemberRemarkInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('memberRemarkInstanceListModification', () => this.loadAll());
  }

  delete(memberRemarkInstance: IMemberRemarkInstance): void {
    const modalRef = this.modalService.open(MemberRemarkInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.memberRemarkInstance = memberRemarkInstance;
  }
}
