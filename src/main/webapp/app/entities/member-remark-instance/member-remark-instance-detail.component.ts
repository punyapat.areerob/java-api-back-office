import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';

@Component({
  selector: 'jhi-member-remark-instance-detail',
  templateUrl: './member-remark-instance-detail.component.html',
})
export class MemberRemarkInstanceDetailComponent implements OnInit {
  memberRemarkInstance: IMemberRemarkInstance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ memberRemarkInstance }) => (this.memberRemarkInstance = memberRemarkInstance));
  }

  previousState(): void {
    window.history.back();
  }
}
