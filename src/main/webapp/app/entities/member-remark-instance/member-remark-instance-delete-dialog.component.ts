import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';
import { MemberRemarkInstanceService } from './member-remark-instance.service';

@Component({
  templateUrl: './member-remark-instance-delete-dialog.component.html',
})
export class MemberRemarkInstanceDeleteDialogComponent {
  memberRemarkInstance?: IMemberRemarkInstance;

  constructor(
    protected memberRemarkInstanceService: MemberRemarkInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.memberRemarkInstanceService.delete(id).subscribe(() => {
      this.eventManager.broadcast('memberRemarkInstanceListModification');
      this.activeModal.close();
    });
  }
}
