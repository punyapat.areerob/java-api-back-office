import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMemberRemarkInstance, MemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';
import { MemberRemarkInstanceService } from './member-remark-instance.service';
import { MemberRemarkInstanceComponent } from './member-remark-instance.component';
import { MemberRemarkInstanceDetailComponent } from './member-remark-instance-detail.component';
import { MemberRemarkInstanceUpdateComponent } from './member-remark-instance-update.component';

@Injectable({ providedIn: 'root' })
export class MemberRemarkInstanceResolve implements Resolve<IMemberRemarkInstance> {
  constructor(private service: MemberRemarkInstanceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMemberRemarkInstance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((memberRemarkInstance: HttpResponse<MemberRemarkInstance>) => {
          if (memberRemarkInstance.body) {
            return of(memberRemarkInstance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MemberRemarkInstance());
  }
}

export const memberRemarkInstanceRoute: Routes = [
  {
    path: '',
    component: MemberRemarkInstanceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MemberRemarkInstanceDetailComponent,
    resolve: {
      memberRemarkInstance: MemberRemarkInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MemberRemarkInstanceUpdateComponent,
    resolve: {
      memberRemarkInstance: MemberRemarkInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkInstances',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MemberRemarkInstanceUpdateComponent,
    resolve: {
      memberRemarkInstance: MemberRemarkInstanceResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'MemberRemarkInstances',
    },
    canActivate: [UserRouteAccessService],
  },
];
