import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJobApplication } from 'app/shared/model/job-application.model';
import { JobApplicationService } from './job-application.service';
import { JobApplicationDeleteDialogComponent } from './job-application-delete-dialog.component';

@Component({
  selector: 'jhi-job-application',
  templateUrl: './job-application.component.html',
})
export class JobApplicationComponent implements OnInit, OnDestroy {
  jobApplications?: IJobApplication[];
  eventSubscriber?: Subscription;

  constructor(
    protected jobApplicationService: JobApplicationService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.jobApplicationService.query().subscribe((res: HttpResponse<IJobApplication[]>) => (this.jobApplications = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJobApplications();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJobApplication): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJobApplications(): void {
    this.eventSubscriber = this.eventManager.subscribe('jobApplicationListModification', () => this.loadAll());
  }

  delete(jobApplication: IJobApplication): void {
    const modalRef = this.modalService.open(JobApplicationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jobApplication = jobApplication;
  }
}
