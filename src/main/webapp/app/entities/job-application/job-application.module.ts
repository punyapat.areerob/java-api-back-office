import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { JobApplicationComponent } from './job-application.component';
import { JobApplicationDetailComponent } from './job-application-detail.component';
import { JobApplicationUpdateComponent } from './job-application-update.component';
import { JobApplicationDeleteDialogComponent } from './job-application-delete-dialog.component';
import { jobApplicationRoute } from './job-application.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(jobApplicationRoute)],
  declarations: [
    JobApplicationComponent,
    JobApplicationDetailComponent,
    JobApplicationUpdateComponent,
    JobApplicationDeleteDialogComponent,
  ],
  entryComponents: [JobApplicationDeleteDialogComponent],
})
export class MisbeJobApplicationModule {}
