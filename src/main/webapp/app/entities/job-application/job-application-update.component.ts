import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IJobApplication, JobApplication } from 'app/shared/model/job-application.model';
import { JobApplicationService } from './job-application.service';

@Component({
  selector: 'jhi-job-application-update',
  templateUrl: './job-application-update.component.html',
})
export class JobApplicationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    status: [],
  });

  constructor(protected jobApplicationService: JobApplicationService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobApplication }) => {
      this.updateForm(jobApplication);
    });
  }

  updateForm(jobApplication: IJobApplication): void {
    this.editForm.patchValue({
      id: jobApplication.id,
      status: jobApplication.status,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jobApplication = this.createFromForm();
    if (jobApplication.id !== undefined) {
      this.subscribeToSaveResponse(this.jobApplicationService.update(jobApplication));
    } else {
      this.subscribeToSaveResponse(this.jobApplicationService.create(jobApplication));
    }
  }

  private createFromForm(): IJobApplication {
    return {
      ...new JobApplication(),
      id: this.editForm.get(['id'])!.value,
      status: this.editForm.get(['status'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJobApplication>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
