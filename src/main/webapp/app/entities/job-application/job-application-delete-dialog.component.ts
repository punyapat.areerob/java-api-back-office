import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IJobApplication } from 'app/shared/model/job-application.model';
import { JobApplicationService } from './job-application.service';

@Component({
  templateUrl: './job-application-delete-dialog.component.html',
})
export class JobApplicationDeleteDialogComponent {
  jobApplication?: IJobApplication;

  constructor(
    protected jobApplicationService: JobApplicationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.jobApplicationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('jobApplicationListModification');
      this.activeModal.close();
    });
  }
}
