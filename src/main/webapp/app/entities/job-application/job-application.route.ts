import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IJobApplication, JobApplication } from 'app/shared/model/job-application.model';
import { JobApplicationService } from './job-application.service';
import { JobApplicationComponent } from './job-application.component';
import { JobApplicationDetailComponent } from './job-application-detail.component';
import { JobApplicationUpdateComponent } from './job-application-update.component';

@Injectable({ providedIn: 'root' })
export class JobApplicationResolve implements Resolve<IJobApplication> {
  constructor(private service: JobApplicationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJobApplication> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((jobApplication: HttpResponse<JobApplication>) => {
          if (jobApplication.body) {
            return of(jobApplication.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new JobApplication());
  }
}

export const jobApplicationRoute: Routes = [
  {
    path: '',
    component: JobApplicationComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobApplications',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JobApplicationDetailComponent,
    resolve: {
      jobApplication: JobApplicationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobApplications',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JobApplicationUpdateComponent,
    resolve: {
      jobApplication: JobApplicationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobApplications',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JobApplicationUpdateComponent,
    resolve: {
      jobApplication: JobApplicationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'JobApplications',
    },
    canActivate: [UserRouteAccessService],
  },
];
