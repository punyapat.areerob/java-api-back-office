import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPhonePrefix, PhonePrefix } from 'app/shared/model/phone-prefix.model';
import { PhonePrefixService } from './phone-prefix.service';

@Component({
  selector: 'jhi-phone-prefix-update',
  templateUrl: './phone-prefix-update.component.html',
})
export class PhonePrefixUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    dialCode: [],
  });

  constructor(protected phonePrefixService: PhonePrefixService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phonePrefix }) => {
      this.updateForm(phonePrefix);
    });
  }

  updateForm(phonePrefix: IPhonePrefix): void {
    this.editForm.patchValue({
      id: phonePrefix.id,
      name: phonePrefix.name,
      dialCode: phonePrefix.dialCode,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phonePrefix = this.createFromForm();
    if (phonePrefix.id !== undefined) {
      this.subscribeToSaveResponse(this.phonePrefixService.update(phonePrefix));
    } else {
      this.subscribeToSaveResponse(this.phonePrefixService.create(phonePrefix));
    }
  }

  private createFromForm(): IPhonePrefix {
    return {
      ...new PhonePrefix(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      dialCode: this.editForm.get(['dialCode'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhonePrefix>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
