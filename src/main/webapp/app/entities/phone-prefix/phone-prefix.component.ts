import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPhonePrefix } from 'app/shared/model/phone-prefix.model';
import { PhonePrefixService } from './phone-prefix.service';
import { PhonePrefixDeleteDialogComponent } from './phone-prefix-delete-dialog.component';

@Component({
  selector: 'jhi-phone-prefix',
  templateUrl: './phone-prefix.component.html',
})
export class PhonePrefixComponent implements OnInit, OnDestroy {
  phonePrefixes?: IPhonePrefix[];
  eventSubscriber?: Subscription;

  constructor(
    protected phonePrefixService: PhonePrefixService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.phonePrefixService.query().subscribe((res: HttpResponse<IPhonePrefix[]>) => (this.phonePrefixes = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPhonePrefixes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPhonePrefix): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPhonePrefixes(): void {
    this.eventSubscriber = this.eventManager.subscribe('phonePrefixListModification', () => this.loadAll());
  }

  delete(phonePrefix: IPhonePrefix): void {
    const modalRef = this.modalService.open(PhonePrefixDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.phonePrefix = phonePrefix;
  }
}
