import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPhonePrefix, PhonePrefix } from 'app/shared/model/phone-prefix.model';
import { PhonePrefixService } from './phone-prefix.service';
import { PhonePrefixComponent } from './phone-prefix.component';
import { PhonePrefixDetailComponent } from './phone-prefix-detail.component';
import { PhonePrefixUpdateComponent } from './phone-prefix-update.component';

@Injectable({ providedIn: 'root' })
export class PhonePrefixResolve implements Resolve<IPhonePrefix> {
  constructor(private service: PhonePrefixService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhonePrefix> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((phonePrefix: HttpResponse<PhonePrefix>) => {
          if (phonePrefix.body) {
            return of(phonePrefix.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PhonePrefix());
  }
}

export const phonePrefixRoute: Routes = [
  {
    path: '',
    component: PhonePrefixComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'PhonePrefixes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PhonePrefixDetailComponent,
    resolve: {
      phonePrefix: PhonePrefixResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'PhonePrefixes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PhonePrefixUpdateComponent,
    resolve: {
      phonePrefix: PhonePrefixResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'PhonePrefixes',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PhonePrefixUpdateComponent,
    resolve: {
      phonePrefix: PhonePrefixResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'PhonePrefixes',
    },
    canActivate: [UserRouteAccessService],
  },
];
