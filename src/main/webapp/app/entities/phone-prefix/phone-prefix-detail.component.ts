import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhonePrefix } from 'app/shared/model/phone-prefix.model';

@Component({
  selector: 'jhi-phone-prefix-detail',
  templateUrl: './phone-prefix-detail.component.html',
})
export class PhonePrefixDetailComponent implements OnInit {
  phonePrefix: IPhonePrefix | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ phonePrefix }) => (this.phonePrefix = phonePrefix));
  }

  previousState(): void {
    window.history.back();
  }
}
