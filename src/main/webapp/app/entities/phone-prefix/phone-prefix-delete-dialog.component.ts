import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPhonePrefix } from 'app/shared/model/phone-prefix.model';
import { PhonePrefixService } from './phone-prefix.service';

@Component({
  templateUrl: './phone-prefix-delete-dialog.component.html',
})
export class PhonePrefixDeleteDialogComponent {
  phonePrefix?: IPhonePrefix;

  constructor(
    protected phonePrefixService: PhonePrefixService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.phonePrefixService.delete(id).subscribe(() => {
      this.eventManager.broadcast('phonePrefixListModification');
      this.activeModal.close();
    });
  }
}
