import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPhonePrefix } from 'app/shared/model/phone-prefix.model';

type EntityResponseType = HttpResponse<IPhonePrefix>;
type EntityArrayResponseType = HttpResponse<IPhonePrefix[]>;

@Injectable({ providedIn: 'root' })
export class PhonePrefixService {
  public resourceUrl = SERVER_API_URL + 'api/phone-prefixes';

  constructor(protected http: HttpClient) {}

  create(phonePrefix: IPhonePrefix): Observable<EntityResponseType> {
    return this.http.post<IPhonePrefix>(this.resourceUrl, phonePrefix, { observe: 'response' });
  }

  update(phonePrefix: IPhonePrefix): Observable<EntityResponseType> {
    return this.http.put<IPhonePrefix>(this.resourceUrl, phonePrefix, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IPhonePrefix>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPhonePrefix[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
