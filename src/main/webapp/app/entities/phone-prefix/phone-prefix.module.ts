import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { PhonePrefixComponent } from './phone-prefix.component';
import { PhonePrefixDetailComponent } from './phone-prefix-detail.component';
import { PhonePrefixUpdateComponent } from './phone-prefix-update.component';
import { PhonePrefixDeleteDialogComponent } from './phone-prefix-delete-dialog.component';
import { phonePrefixRoute } from './phone-prefix.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(phonePrefixRoute)],
  declarations: [PhonePrefixComponent, PhonePrefixDetailComponent, PhonePrefixUpdateComponent, PhonePrefixDeleteDialogComponent],
  entryComponents: [PhonePrefixDeleteDialogComponent],
})
export class MisbePhonePrefixModule {}
