import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IInputField } from 'app/shared/model/input-field.model';
import { InputFieldService } from './input-field.service';
import { InputFieldDeleteDialogComponent } from './input-field-delete-dialog.component';

@Component({
  selector: 'jhi-input-field',
  templateUrl: './input-field.component.html',
})
export class InputFieldComponent implements OnInit, OnDestroy {
  inputFields?: IInputField[];
  eventSubscriber?: Subscription;

  constructor(protected inputFieldService: InputFieldService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.inputFieldService.query().subscribe((res: HttpResponse<IInputField[]>) => (this.inputFields = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInInputFields();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IInputField): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInInputFields(): void {
    this.eventSubscriber = this.eventManager.subscribe('inputFieldListModification', () => this.loadAll());
  }

  delete(inputField: IInputField): void {
    const modalRef = this.modalService.open(InputFieldDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.inputField = inputField;
  }
}
