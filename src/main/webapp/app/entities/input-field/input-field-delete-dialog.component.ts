import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInputField } from 'app/shared/model/input-field.model';
import { InputFieldService } from './input-field.service';

@Component({
  templateUrl: './input-field-delete-dialog.component.html',
})
export class InputFieldDeleteDialogComponent {
  inputField?: IInputField;

  constructor(
    protected inputFieldService: InputFieldService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.inputFieldService.delete(id).subscribe(() => {
      this.eventManager.broadcast('inputFieldListModification');
      this.activeModal.close();
    });
  }
}
