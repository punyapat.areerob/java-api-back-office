import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IInputField, InputField } from 'app/shared/model/input-field.model';
import { InputFieldService } from './input-field.service';
import { ICaseStep } from 'app/shared/model/case-step.model';
import { CaseStepService } from 'app/entities/case-step/case-step.service';
import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { CaseStepInstanceService } from 'app/entities/case-step-instance/case-step-instance.service';

type SelectableEntity = ICaseStep | ICaseStepInstance;

@Component({
  selector: 'jhi-input-field-update',
  templateUrl: './input-field-update.component.html',
})
export class InputFieldUpdateComponent implements OnInit {
  isSaving = false;
  casesteps: ICaseStep[] = [];
  casestepinstances: ICaseStepInstance[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    label: [],
    link: [],
    caseStep: [],
    caseStepInstance: [],
  });

  constructor(
    protected inputFieldService: InputFieldService,
    protected caseStepService: CaseStepService,
    protected caseStepInstanceService: CaseStepInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ inputField }) => {
      this.updateForm(inputField);

      this.caseStepService.query().subscribe((res: HttpResponse<ICaseStep[]>) => (this.casesteps = res.body || []));

      this.caseStepInstanceService.query().subscribe((res: HttpResponse<ICaseStepInstance[]>) => (this.casestepinstances = res.body || []));
    });
  }

  updateForm(inputField: IInputField): void {
    this.editForm.patchValue({
      id: inputField.id,
      name: inputField.name,
      label: inputField.label,
      link: inputField.link,
      caseStep: inputField.caseStep,
      caseStepInstance: inputField.caseStepInstance,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const inputField = this.createFromForm();
    if (inputField.id !== undefined) {
      this.subscribeToSaveResponse(this.inputFieldService.update(inputField));
    } else {
      this.subscribeToSaveResponse(this.inputFieldService.create(inputField));
    }
  }

  private createFromForm(): IInputField {
    return {
      ...new InputField(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      label: this.editForm.get(['label'])!.value,
      link: this.editForm.get(['link'])!.value,
      caseStep: this.editForm.get(['caseStep'])!.value,
      caseStepInstance: this.editForm.get(['caseStepInstance'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInputField>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
