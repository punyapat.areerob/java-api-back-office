import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInputField } from 'app/shared/model/input-field.model';

@Component({
  selector: 'jhi-input-field-detail',
  templateUrl: './input-field-detail.component.html',
})
export class InputFieldDetailComponent implements OnInit {
  inputField: IInputField | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ inputField }) => (this.inputField = inputField));
  }

  previousState(): void {
    window.history.back();
  }
}
