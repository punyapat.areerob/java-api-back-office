import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IInputField, InputField } from 'app/shared/model/input-field.model';
import { InputFieldService } from './input-field.service';
import { InputFieldComponent } from './input-field.component';
import { InputFieldDetailComponent } from './input-field-detail.component';
import { InputFieldUpdateComponent } from './input-field-update.component';

@Injectable({ providedIn: 'root' })
export class InputFieldResolve implements Resolve<IInputField> {
  constructor(private service: InputFieldService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IInputField> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((inputField: HttpResponse<InputField>) => {
          if (inputField.body) {
            return of(inputField.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new InputField());
  }
}

export const inputFieldRoute: Routes = [
  {
    path: '',
    component: InputFieldComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'InputFields',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: InputFieldDetailComponent,
    resolve: {
      inputField: InputFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'InputFields',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: InputFieldUpdateComponent,
    resolve: {
      inputField: InputFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'InputFields',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: InputFieldUpdateComponent,
    resolve: {
      inputField: InputFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'InputFields',
    },
    canActivate: [UserRouteAccessService],
  },
];
