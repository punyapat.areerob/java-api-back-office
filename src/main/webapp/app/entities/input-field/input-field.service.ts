import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IInputField } from 'app/shared/model/input-field.model';

type EntityResponseType = HttpResponse<IInputField>;
type EntityArrayResponseType = HttpResponse<IInputField[]>;

@Injectable({ providedIn: 'root' })
export class InputFieldService {
  public resourceUrl = SERVER_API_URL + 'api/input-fields';

  constructor(protected http: HttpClient) {}

  create(inputField: IInputField): Observable<EntityResponseType> {
    return this.http.post<IInputField>(this.resourceUrl, inputField, { observe: 'response' });
  }

  update(inputField: IInputField): Observable<EntityResponseType> {
    return this.http.put<IInputField>(this.resourceUrl, inputField, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IInputField>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInputField[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
