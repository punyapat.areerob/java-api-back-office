import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { InputFieldComponent } from './input-field.component';
import { InputFieldDetailComponent } from './input-field-detail.component';
import { InputFieldUpdateComponent } from './input-field-update.component';
import { InputFieldDeleteDialogComponent } from './input-field-delete-dialog.component';
import { inputFieldRoute } from './input-field.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(inputFieldRoute)],
  declarations: [InputFieldComponent, InputFieldDetailComponent, InputFieldUpdateComponent, InputFieldDeleteDialogComponent],
  entryComponents: [InputFieldDeleteDialogComponent],
})
export class MisbeInputFieldModule {}
