import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICard, Card } from 'app/shared/model/card.model';
import { CardService } from './card.service';

@Component({
  selector: 'jhi-card-update',
  templateUrl: './card-update.component.html',
})
export class CardUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    abbreviation: [],
    description: [],
    status: [],
    type: [],
    photo: [],
    requireApplicant: [],
    memberValidity: [],
    memberFee: [],
    memberFeeVat: [],
    additionalMember: [],
    additionalMemberFee: [],
    numberOfTransfer: [],
    transferFee: [],
    annualFee: [],
    annualFeeVat: [],
    ageMin: [],
    ageMax: [],
    visaPeriod: [],
    activeStartDate: [],
    activeEndDate: [],
    memberInformationAgreement: [],
    memberPackageAgreement: [],
    remark: [],
    active: [],
  });

  constructor(protected cardService: CardService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ card }) => {
      if (!card.id) {
        const today = moment().startOf('day');
        card.activeStartDate = today;
        card.activeEndDate = today;
      }

      this.updateForm(card);
    });
  }

  updateForm(card: ICard): void {
    this.editForm.patchValue({
      id: card.id,
      name: card.name,
      abbreviation: card.abbreviation,
      description: card.description,
      status: card.status,
      type: card.type,
      photo: card.photo,
      requireApplicant: card.requireApplicant,
      memberValidity: card.memberValidity,
      memberFee: card.memberFee,
      memberFeeVat: card.memberFeeVat,
      additionalMember: card.additionalMember,
      additionalMemberFee: card.additionalMemberFee,
      numberOfTransfer: card.numberOfTransfer,
      transferFee: card.transferFee,
      annualFee: card.annualFee,
      annualFeeVat: card.annualFeeVat,
      ageMin: card.ageMin,
      ageMax: card.ageMax,
      visaPeriod: card.visaPeriod,
      activeStartDate: card.activeStartDate ? card.activeStartDate.format(DATE_TIME_FORMAT) : null,
      activeEndDate: card.activeEndDate ? card.activeEndDate.format(DATE_TIME_FORMAT) : null,
      memberInformationAgreement: card.memberInformationAgreement,
      memberPackageAgreement: card.memberPackageAgreement,
      remark: card.remark,
      active: card.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const card = this.createFromForm();
    if (card.id !== undefined) {
      this.subscribeToSaveResponse(this.cardService.update(card));
    } else {
      this.subscribeToSaveResponse(this.cardService.create(card));
    }
  }

  private createFromForm(): ICard {
    return {
      ...new Card(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      abbreviation: this.editForm.get(['abbreviation'])!.value,
      description: this.editForm.get(['description'])!.value,
      status: this.editForm.get(['status'])!.value,
      type: this.editForm.get(['type'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      requireApplicant: this.editForm.get(['requireApplicant'])!.value,
      memberValidity: this.editForm.get(['memberValidity'])!.value,
      memberFee: this.editForm.get(['memberFee'])!.value,
      memberFeeVat: this.editForm.get(['memberFeeVat'])!.value,
      additionalMember: this.editForm.get(['additionalMember'])!.value,
      additionalMemberFee: this.editForm.get(['additionalMemberFee'])!.value,
      numberOfTransfer: this.editForm.get(['numberOfTransfer'])!.value,
      transferFee: this.editForm.get(['transferFee'])!.value,
      annualFee: this.editForm.get(['annualFee'])!.value,
      annualFeeVat: this.editForm.get(['annualFeeVat'])!.value,
      ageMin: this.editForm.get(['ageMin'])!.value,
      ageMax: this.editForm.get(['ageMax'])!.value,
      visaPeriod: this.editForm.get(['visaPeriod'])!.value,
      activeStartDate: this.editForm.get(['activeStartDate'])!.value
        ? moment(this.editForm.get(['activeStartDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      activeEndDate: this.editForm.get(['activeEndDate'])!.value
        ? moment(this.editForm.get(['activeEndDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      memberInformationAgreement: this.editForm.get(['memberInformationAgreement'])!.value,
      memberPackageAgreement: this.editForm.get(['memberPackageAgreement'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICard>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
