import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICard } from 'app/shared/model/card.model';
import { CardService } from './card.service';
import { CardDeleteDialogComponent } from './card-delete-dialog.component';

@Component({
  selector: 'jhi-card',
  templateUrl: './card.component.html',
})
export class CardComponent implements OnInit, OnDestroy {
  cards?: ICard[];
  eventSubscriber?: Subscription;

  constructor(protected cardService: CardService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.cardService.query().subscribe((res: HttpResponse<ICard[]>) => (this.cards = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCards();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICard): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCards(): void {
    this.eventSubscriber = this.eventManager.subscribe('cardListModification', () => this.loadAll());
  }

  delete(card: ICard): void {
    const modalRef = this.modalService.open(CardDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.card = card;
  }
}
