import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ISubProductCategory, SubProductCategory } from 'app/shared/model/sub-product-category.model';
import { SubProductCategoryService } from './sub-product-category.service';

@Component({
  selector: 'jhi-sub-product-category-update',
  templateUrl: './sub-product-category-update.component.html',
})
export class SubProductCategoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    productCategoryId: [],
    name: [],
    icon: [],
    description: [],
    active: [],
  });

  constructor(
    protected subProductCategoryService: SubProductCategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subProductCategory }) => {
      this.updateForm(subProductCategory);
    });
  }

  updateForm(subProductCategory: ISubProductCategory): void {
    this.editForm.patchValue({
      id: subProductCategory.id,
      productCategoryId: subProductCategory.productCategoryId,
      name: subProductCategory.name,
      icon: subProductCategory.icon,
      description: subProductCategory.description,
      active: subProductCategory.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const subProductCategory = this.createFromForm();
    if (subProductCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.subProductCategoryService.update(subProductCategory));
    } else {
      this.subscribeToSaveResponse(this.subProductCategoryService.create(subProductCategory));
    }
  }

  private createFromForm(): ISubProductCategory {
    return {
      ...new SubProductCategory(),
      id: this.editForm.get(['id'])!.value,
      productCategoryId: this.editForm.get(['productCategoryId'])!.value,
      name: this.editForm.get(['name'])!.value,
      icon: this.editForm.get(['icon'])!.value,
      description: this.editForm.get(['description'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubProductCategory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
