import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { SubProductCategoryComponent } from './sub-product-category.component';
import { SubProductCategoryDetailComponent } from './sub-product-category-detail.component';
import { SubProductCategoryUpdateComponent } from './sub-product-category-update.component';
import { SubProductCategoryDeleteDialogComponent } from './sub-product-category-delete-dialog.component';
import { subProductCategoryRoute } from './sub-product-category.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(subProductCategoryRoute)],
  declarations: [
    SubProductCategoryComponent,
    SubProductCategoryDetailComponent,
    SubProductCategoryUpdateComponent,
    SubProductCategoryDeleteDialogComponent,
  ],
  entryComponents: [SubProductCategoryDeleteDialogComponent],
})
export class MisbeSubProductCategoryModule {}
