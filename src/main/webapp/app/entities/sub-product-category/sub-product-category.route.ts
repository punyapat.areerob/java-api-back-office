import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISubProductCategory, SubProductCategory } from 'app/shared/model/sub-product-category.model';
import { SubProductCategoryService } from './sub-product-category.service';
import { SubProductCategoryComponent } from './sub-product-category.component';
import { SubProductCategoryDetailComponent } from './sub-product-category-detail.component';
import { SubProductCategoryUpdateComponent } from './sub-product-category-update.component';

@Injectable({ providedIn: 'root' })
export class SubProductCategoryResolve implements Resolve<ISubProductCategory> {
  constructor(private service: SubProductCategoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISubProductCategory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((subProductCategory: HttpResponse<SubProductCategory>) => {
          if (subProductCategory.body) {
            return of(subProductCategory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new SubProductCategory());
  }
}

export const subProductCategoryRoute: Routes = [
  {
    path: '',
    component: SubProductCategoryComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'SubProductCategories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: SubProductCategoryDetailComponent,
    resolve: {
      subProductCategory: SubProductCategoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'SubProductCategories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: SubProductCategoryUpdateComponent,
    resolve: {
      subProductCategory: SubProductCategoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'SubProductCategories',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: SubProductCategoryUpdateComponent,
    resolve: {
      subProductCategory: SubProductCategoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'SubProductCategories',
    },
    canActivate: [UserRouteAccessService],
  },
];
