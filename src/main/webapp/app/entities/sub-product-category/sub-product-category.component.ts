import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISubProductCategory } from 'app/shared/model/sub-product-category.model';
import { SubProductCategoryService } from './sub-product-category.service';
import { SubProductCategoryDeleteDialogComponent } from './sub-product-category-delete-dialog.component';

@Component({
  selector: 'jhi-sub-product-category',
  templateUrl: './sub-product-category.component.html',
})
export class SubProductCategoryComponent implements OnInit, OnDestroy {
  subProductCategories?: ISubProductCategory[];
  eventSubscriber?: Subscription;

  constructor(
    protected subProductCategoryService: SubProductCategoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.subProductCategoryService
      .query()
      .subscribe((res: HttpResponse<ISubProductCategory[]>) => (this.subProductCategories = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInSubProductCategories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISubProductCategory): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSubProductCategories(): void {
    this.eventSubscriber = this.eventManager.subscribe('subProductCategoryListModification', () => this.loadAll());
  }

  delete(subProductCategory: ISubProductCategory): void {
    const modalRef = this.modalService.open(SubProductCategoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.subProductCategory = subProductCategory;
  }
}
