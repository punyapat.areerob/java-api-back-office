import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubProductCategory } from 'app/shared/model/sub-product-category.model';

@Component({
  selector: 'jhi-sub-product-category-detail',
  templateUrl: './sub-product-category-detail.component.html',
})
export class SubProductCategoryDetailComponent implements OnInit {
  subProductCategory: ISubProductCategory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ subProductCategory }) => (this.subProductCategory = subProductCategory));
  }

  previousState(): void {
    window.history.back();
  }
}
