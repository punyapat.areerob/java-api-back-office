import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISubProductCategory } from 'app/shared/model/sub-product-category.model';
import { SubProductCategoryService } from './sub-product-category.service';

@Component({
  templateUrl: './sub-product-category-delete-dialog.component.html',
})
export class SubProductCategoryDeleteDialogComponent {
  subProductCategory?: ISubProductCategory;

  constructor(
    protected subProductCategoryService: SubProductCategoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.subProductCategoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('subProductCategoryListModification');
      this.activeModal.close();
    });
  }
}
