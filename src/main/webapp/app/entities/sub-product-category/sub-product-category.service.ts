import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISubProductCategory } from 'app/shared/model/sub-product-category.model';

type EntityResponseType = HttpResponse<ISubProductCategory>;
type EntityArrayResponseType = HttpResponse<ISubProductCategory[]>;

@Injectable({ providedIn: 'root' })
export class SubProductCategoryService {
  public resourceUrl = SERVER_API_URL + 'api/sub-product-categories';

  constructor(protected http: HttpClient) {}

  create(subProductCategory: ISubProductCategory): Observable<EntityResponseType> {
    return this.http.post<ISubProductCategory>(this.resourceUrl, subProductCategory, { observe: 'response' });
  }

  update(subProductCategory: ISubProductCategory): Observable<EntityResponseType> {
    return this.http.put<ISubProductCategory>(this.resourceUrl, subProductCategory, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<ISubProductCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISubProductCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
