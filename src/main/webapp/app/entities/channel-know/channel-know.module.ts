import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MisbeSharedModule } from 'app/shared/shared.module';
import { ChannelKnowComponent } from './channel-know.component';
import { ChannelKnowDetailComponent } from './channel-know-detail.component';
import { ChannelKnowUpdateComponent } from './channel-know-update.component';
import { ChannelKnowDeleteDialogComponent } from './channel-know-delete-dialog.component';
import { channelKnowRoute } from './channel-know.route';

@NgModule({
  imports: [MisbeSharedModule, RouterModule.forChild(channelKnowRoute)],
  declarations: [ChannelKnowComponent, ChannelKnowDetailComponent, ChannelKnowUpdateComponent, ChannelKnowDeleteDialogComponent],
  entryComponents: [ChannelKnowDeleteDialogComponent],
})
export class MisbeChannelKnowModule {}
