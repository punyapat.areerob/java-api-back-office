import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IChannelKnow, ChannelKnow } from 'app/shared/model/channel-know.model';
import { ChannelKnowService } from './channel-know.service';
import { ChannelKnowComponent } from './channel-know.component';
import { ChannelKnowDetailComponent } from './channel-know-detail.component';
import { ChannelKnowUpdateComponent } from './channel-know-update.component';

@Injectable({ providedIn: 'root' })
export class ChannelKnowResolve implements Resolve<IChannelKnow> {
  constructor(private service: ChannelKnowService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChannelKnow> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((channelKnow: HttpResponse<ChannelKnow>) => {
          if (channelKnow.body) {
            return of(channelKnow.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChannelKnow());
  }
}

export const channelKnowRoute: Routes = [
  {
    path: '',
    component: ChannelKnowComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ChannelKnows',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChannelKnowDetailComponent,
    resolve: {
      channelKnow: ChannelKnowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ChannelKnows',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChannelKnowUpdateComponent,
    resolve: {
      channelKnow: ChannelKnowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ChannelKnows',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChannelKnowUpdateComponent,
    resolve: {
      channelKnow: ChannelKnowResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ChannelKnows',
    },
    canActivate: [UserRouteAccessService],
  },
];
