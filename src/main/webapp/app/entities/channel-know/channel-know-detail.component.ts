import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChannelKnow } from 'app/shared/model/channel-know.model';

@Component({
  selector: 'jhi-channel-know-detail',
  templateUrl: './channel-know-detail.component.html',
})
export class ChannelKnowDetailComponent implements OnInit {
  channelKnow: IChannelKnow | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ channelKnow }) => (this.channelKnow = channelKnow));
  }

  previousState(): void {
    window.history.back();
  }
}
