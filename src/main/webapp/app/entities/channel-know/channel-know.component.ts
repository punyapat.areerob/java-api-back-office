import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IChannelKnow } from 'app/shared/model/channel-know.model';
import { ChannelKnowService } from './channel-know.service';
import { ChannelKnowDeleteDialogComponent } from './channel-know-delete-dialog.component';

@Component({
  selector: 'jhi-channel-know',
  templateUrl: './channel-know.component.html',
})
export class ChannelKnowComponent implements OnInit, OnDestroy {
  channelKnows?: IChannelKnow[];
  eventSubscriber?: Subscription;

  constructor(
    protected channelKnowService: ChannelKnowService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.channelKnowService.query().subscribe((res: HttpResponse<IChannelKnow[]>) => (this.channelKnows = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInChannelKnows();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IChannelKnow): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInChannelKnows(): void {
    this.eventSubscriber = this.eventManager.subscribe('channelKnowListModification', () => this.loadAll());
  }

  delete(channelKnow: IChannelKnow): void {
    const modalRef = this.modalService.open(ChannelKnowDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.channelKnow = channelKnow;
  }
}
