import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IChannelKnow } from 'app/shared/model/channel-know.model';
import { ChannelKnowService } from './channel-know.service';

@Component({
  templateUrl: './channel-know-delete-dialog.component.html',
})
export class ChannelKnowDeleteDialogComponent {
  channelKnow?: IChannelKnow;

  constructor(
    protected channelKnowService: ChannelKnowService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.channelKnowService.delete(id).subscribe(() => {
      this.eventManager.broadcast('channelKnowListModification');
      this.activeModal.close();
    });
  }
}
