import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IChannelKnow } from 'app/shared/model/channel-know.model';

type EntityResponseType = HttpResponse<IChannelKnow>;
type EntityArrayResponseType = HttpResponse<IChannelKnow[]>;

@Injectable({ providedIn: 'root' })
export class ChannelKnowService {
  public resourceUrl = SERVER_API_URL + 'api/channel-knows';

  constructor(protected http: HttpClient) {}

  create(channelKnow: IChannelKnow): Observable<EntityResponseType> {
    return this.http.post<IChannelKnow>(this.resourceUrl, channelKnow, { observe: 'response' });
  }

  update(channelKnow: IChannelKnow): Observable<EntityResponseType> {
    return this.http.put<IChannelKnow>(this.resourceUrl, channelKnow, { observe: 'response' });
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IChannelKnow>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IChannelKnow[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
