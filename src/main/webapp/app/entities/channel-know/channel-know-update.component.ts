import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IChannelKnow, ChannelKnow } from 'app/shared/model/channel-know.model';
import { ChannelKnowService } from './channel-know.service';

@Component({
  selector: 'jhi-channel-know-update',
  templateUrl: './channel-know-update.component.html',
})
export class ChannelKnowUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    active: [],
  });

  constructor(protected channelKnowService: ChannelKnowService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ channelKnow }) => {
      this.updateForm(channelKnow);
    });
  }

  updateForm(channelKnow: IChannelKnow): void {
    this.editForm.patchValue({
      id: channelKnow.id,
      name: channelKnow.name,
      active: channelKnow.active,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const channelKnow = this.createFromForm();
    if (channelKnow.id !== undefined) {
      this.subscribeToSaveResponse(this.channelKnowService.update(channelKnow));
    } else {
      this.subscribeToSaveResponse(this.channelKnowService.create(channelKnow));
    }
  }

  private createFromForm(): IChannelKnow {
    return {
      ...new ChannelKnow(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      active: this.editForm.get(['active'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChannelKnow>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
