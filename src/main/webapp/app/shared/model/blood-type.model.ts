export interface IBloodType {
  id?: string;
  name?: string;
}

export class BloodType implements IBloodType {
  constructor(public id?: string, public name?: string) {}
}
