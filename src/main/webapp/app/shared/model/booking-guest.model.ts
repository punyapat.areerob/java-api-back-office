import { Moment } from 'moment';
import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';

export interface IBookingGuest {
  id?: string;
  name?: string;
  amount?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
  bookingServiceMember?: IBookingServiceMember;
}

export class BookingGuest implements IBookingGuest {
  constructor(
    public id?: string,
    public name?: string,
    public amount?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment,
    public bookingServiceMember?: IBookingServiceMember
  ) {}
}
