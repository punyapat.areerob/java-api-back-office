export interface IChannelKnow {
  id?: string;
  name?: string;
  active?: boolean;
}

export class ChannelKnow implements IChannelKnow {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
