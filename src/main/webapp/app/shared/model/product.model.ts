import { IProductVariantGroup } from 'app/shared/model/product-variant-group.model';
import { IService } from 'app/shared/model/service.model';
import { ISubProductCategory } from 'app/shared/model/sub-product-category.model';

export interface IProduct {
  id?: string;
  shopId?: string;
  name?: string;
  description?: string;
  policy?: string;
  images?: string;
  active?: boolean;
  variantGroups?: IProductVariantGroup[];
  service?: IService;
  subProductCategory?: ISubProductCategory;
}

export class Product implements IProduct {
  constructor(
    public id?: string,
    public shopId?: string,
    public name?: string,
    public description?: string,
    public policy?: string,
    public images?: string,
    public active?: boolean,
    public variantGroups?: IProductVariantGroup[],
    public service?: IService,
    public subProductCategory?: ISubProductCategory
  ) {
    this.active = this.active || false;
  }
}
