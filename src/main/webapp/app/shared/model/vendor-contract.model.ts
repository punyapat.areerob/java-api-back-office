export interface IVendorContract {
  id?: string;
  name?: string;
  vendorId?: string;
  vendorContractStartDate?: string;
  vendorContractEndDate?: string;
  vendorContractFile?: string;
  remark?: string;
  status?: string;
}

export class VendorContract implements IVendorContract {
  constructor(
    public id?: string,
    public name?: string,
    public vendorId?: string,
    public vendorContractStartDate?: string,
    public vendorContractEndDate?: string,
    public vendorContractFile?: string,
    public remark?: string,
    public status?: string
  ) {}
}
