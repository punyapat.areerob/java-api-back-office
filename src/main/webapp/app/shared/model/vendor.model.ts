import { IVendorsContact } from 'app/shared/model/vendors-contact.model';

export interface IVendor {
  id?: string;
  name?: string;
  description?: string;
  mobile?: string;
  email?: string;
  photos?: string;
  rating?: string;
  webSite?: string;
  status?: string;
  active?: boolean;
  contacts?: IVendorsContact[];
}

export class Vendor implements IVendor {
  constructor(
    public id?: string,
    public name?: string,
    public description?: string,
    public mobile?: string,
    public email?: string,
    public photos?: string,
    public rating?: string,
    public webSite?: string,
    public status?: string,
    public active?: boolean,
    public contacts?: IVendorsContact[]
  ) {
    this.active = this.active || false;
  }
}
