import { ITeam } from 'app/shared/model/team.model';
import { IStepOwner } from 'app/shared/model/step-owner.model';
import { IActionInstance } from 'app/shared/model/action-instance.model';
import { IVerifyField } from 'app/shared/model/verify-field.model';
import { IInputField } from 'app/shared/model/input-field.model';
import { ICaseStepState } from 'app/shared/model/case-step-state.model';

export interface ICaseStepInstance {
  id?: string;
  name?: string;
  stepId?: string;
  caseInstanceId?: string;
  begin?: boolean;
  team?: ITeam;
  stepOwner?: IStepOwner;
  actionInstances?: IActionInstance[];
  verifyFields?: IVerifyField[];
  inputFields?: IInputField[];
  state?: ICaseStepState;
}

export class CaseStepInstance implements ICaseStepInstance {
  constructor(
    public id?: string,
    public name?: string,
    public stepId?: string,
    public caseInstanceId?: string,
    public begin?: boolean,
    public team?: ITeam,
    public stepOwner?: IStepOwner,
    public actionInstances?: IActionInstance[],
    public verifyFields?: IVerifyField[],
    public inputFields?: IInputField[],
    public state?: ICaseStepState
  ) {
    this.begin = this.begin || false;
  }
}
