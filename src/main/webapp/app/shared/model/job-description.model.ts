export interface IJobDescription {
  id?: string;
  name?: string;
  teamId?: string;
  teamName?: string;
  jobTitle?: string;
  lineOfWork?: string;
  department?: string;
  educationBackground?: string;
  salary?: number;
  location?: string;
  summary?: string;
  mainResponsibilities?: string;
  jobDescription?: string;
  jobQualifications?: string;
  profilePhoto?: boolean;
  militaryPass?: boolean;
  enLanguageScore?: boolean;
  educationCertificate?: boolean;
  driverLicense?: boolean;
  chLanguageScore?: boolean;
  educationQualification?: boolean;
  citizenID?: boolean;
  jpLanguageScore?: boolean;
  workPermit?: boolean;
  marriageCertificate?: boolean;
  houseRegistry?: boolean;
  birthCertificate?: boolean;
  remark?: string;
}

export class JobDescription implements IJobDescription {
  constructor(
    public id?: string,
    public name?: string,
    public teamId?: string,
    public teamName?: string,
    public jobTitle?: string,
    public lineOfWork?: string,
    public department?: string,
    public educationBackground?: string,
    public salary?: number,
    public location?: string,
    public summary?: string,
    public mainResponsibilities?: string,
    public jobDescription?: string,
    public jobQualifications?: string,
    public profilePhoto?: boolean,
    public militaryPass?: boolean,
    public enLanguageScore?: boolean,
    public educationCertificate?: boolean,
    public driverLicense?: boolean,
    public chLanguageScore?: boolean,
    public educationQualification?: boolean,
    public citizenID?: boolean,
    public jpLanguageScore?: boolean,
    public workPermit?: boolean,
    public marriageCertificate?: boolean,
    public houseRegistry?: boolean,
    public birthCertificate?: boolean,
    public remark?: string
  ) {
    this.profilePhoto = this.profilePhoto || false;
    this.militaryPass = this.militaryPass || false;
    this.enLanguageScore = this.enLanguageScore || false;
    this.educationCertificate = this.educationCertificate || false;
    this.driverLicense = this.driverLicense || false;
    this.chLanguageScore = this.chLanguageScore || false;
    this.educationQualification = this.educationQualification || false;
    this.citizenID = this.citizenID || false;
    this.jpLanguageScore = this.jpLanguageScore || false;
    this.workPermit = this.workPermit || false;
    this.marriageCertificate = this.marriageCertificate || false;
    this.houseRegistry = this.houseRegistry || false;
    this.birthCertificate = this.birthCertificate || false;
  }
}
