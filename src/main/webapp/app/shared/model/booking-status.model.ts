import { Moment } from 'moment';

export interface IBookingStatus {
  id?: string;
  name?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class BookingStatus implements IBookingStatus {
  constructor(public id?: string, public name?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
