import { Moment } from 'moment';

export interface IAgentContract {
  id?: string;
  agentId?: string;
  commissionId?: string;
  start?: Moment;
  end?: Moment;
}

export class AgentContract implements IAgentContract {
  constructor(public id?: string, public agentId?: string, public commissionId?: string, public start?: Moment, public end?: Moment) {}
}
