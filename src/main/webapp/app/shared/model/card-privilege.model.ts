import { CardPrivilegeValidity } from 'app/shared/model/enumerations/card-privilege-validity.model';

export interface ICardPrivilege {
  id?: string;
  cardId?: string;
  cardName?: string;
  privilegeId?: string;
  privilegeName?: string;
  quota?: number;
  validityPeriod?: CardPrivilegeValidity;
  active?: boolean;
}

export class CardPrivilege implements ICardPrivilege {
  constructor(
    public id?: string,
    public cardId?: string,
    public cardName?: string,
    public privilegeId?: string,
    public privilegeName?: string,
    public quota?: number,
    public validityPeriod?: CardPrivilegeValidity,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
