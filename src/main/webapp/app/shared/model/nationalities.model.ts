export interface INationalities {
  id?: string;
  name?: string;
}

export class Nationalities implements INationalities {
  constructor(public id?: string, public name?: string) {}
}
