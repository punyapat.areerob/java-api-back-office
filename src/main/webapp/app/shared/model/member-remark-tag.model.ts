export interface IMemberRemarkTag {
  id?: string;
  name?: string;
  active?: boolean;
}

export class MemberRemarkTag implements IMemberRemarkTag {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
