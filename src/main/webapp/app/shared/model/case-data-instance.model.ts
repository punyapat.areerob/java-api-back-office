export interface ICaseDataInstance {
  id?: string;
}

export class CaseDataInstance implements ICaseDataInstance {
  constructor(public id?: string) {}
}
