export interface IProductCategory {
  id?: string;
  name?: string;
  icon?: string;
  description?: string;
  active?: boolean;
}

export class ProductCategory implements IProductCategory {
  constructor(public id?: string, public name?: string, public icon?: string, public description?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
