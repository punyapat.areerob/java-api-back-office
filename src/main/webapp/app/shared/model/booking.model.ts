import { Moment } from 'moment';
import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { IBookingServices } from 'app/shared/model/booking-services.model';

export interface IBooking {
  id?: string;
  remark?: string;
  createdBy?: Moment;
  updatedBy?: Moment;
  createdAt?: Moment;
  updatedAt?: Moment;
  bookingStatus?: IBookingStatus;
  bookingServiceIds?: IBookingServices[];
}

export class Booking implements IBooking {
  constructor(
    public id?: string,
    public remark?: string,
    public createdBy?: Moment,
    public updatedBy?: Moment,
    public createdAt?: Moment,
    public updatedAt?: Moment,
    public bookingStatus?: IBookingStatus,
    public bookingServiceIds?: IBookingServices[]
  ) {}
}
