import { ITeam } from 'app/shared/model/team.model';
import { IAction } from 'app/shared/model/action.model';
import { ICaseStepState } from 'app/shared/model/case-step-state.model';
import { IVerifyField } from 'app/shared/model/verify-field.model';
import { IInputField } from 'app/shared/model/input-field.model';

export interface ICaseStep {
  id?: string;
  name?: string;
  caseId?: string;
  begin?: boolean;
  team?: ITeam;
  actions?: IAction[];
  caseStepStates?: ICaseStepState[];
  verifyFields?: IVerifyField[];
  inputFields?: IInputField[];
}

export class CaseStep implements ICaseStep {
  constructor(
    public id?: string,
    public name?: string,
    public caseId?: string,
    public begin?: boolean,
    public team?: ITeam,
    public actions?: IAction[],
    public caseStepStates?: ICaseStepState[],
    public verifyFields?: IVerifyField[],
    public inputFields?: IInputField[]
  ) {
    this.begin = this.begin || false;
  }
}
