export interface IJsonInteger {
  id?: string;
  key?: string;
  value?: number;
}

export class JsonInteger implements IJsonInteger {
  constructor(public id?: string, public key?: string, public value?: number) {}
}
