import { Moment } from 'moment';
import { IAgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';
import { IAgent } from 'app/shared/model/agent.model';

export interface IAgentCommissionRateConfig {
  id?: string;
  agentId?: string;
  commissionId?: string;
  start?: Moment;
  end?: Moment;
  commIds?: IAgentCommissionRate[];
  agentIds?: IAgent[];
}

export class AgentCommissionRateConfig implements IAgentCommissionRateConfig {
  constructor(
    public id?: string,
    public agentId?: string,
    public commissionId?: string,
    public start?: Moment,
    public end?: Moment,
    public commIds?: IAgentCommissionRate[],
    public agentIds?: IAgent[]
  ) {}
}
