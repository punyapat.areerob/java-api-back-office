import { Moment } from 'moment';
import { EmailTransactionStatus } from 'app/shared/model/enumerations/email-transaction-status.model';

export interface IEmailTransaction {
  id?: string;
  templateName?: string;
  to?: string;
  subject?: string;
  body?: string;
  sendDate?: Moment;
  status?: EmailTransactionStatus;
  error?: string;
}

export class EmailTransaction implements IEmailTransaction {
  constructor(
    public id?: string,
    public templateName?: string,
    public to?: string,
    public subject?: string,
    public body?: string,
    public sendDate?: Moment,
    public status?: EmailTransactionStatus,
    public error?: string
  ) {}
}
