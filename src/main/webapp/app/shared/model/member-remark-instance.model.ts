import { Moment } from 'moment';

export interface IMemberRemarkInstance {
  id?: string;
  memberId?: string;
  text?: string;
  tagId?: string;
  tagName?: string;
  accessTeams?: string;
  createDate?: Moment;
  createBy?: string;
  updateDate?: Moment;
  updateBy?: string;
  active?: boolean;
}

export class MemberRemarkInstance implements IMemberRemarkInstance {
  constructor(
    public id?: string,
    public memberId?: string,
    public text?: string,
    public tagId?: string,
    public tagName?: string,
    public accessTeams?: string,
    public createDate?: Moment,
    public createBy?: string,
    public updateDate?: Moment,
    public updateBy?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
