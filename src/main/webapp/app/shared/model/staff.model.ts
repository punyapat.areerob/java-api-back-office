import { ITeam } from 'app/shared/model/team.model';
import { StaffStatus } from 'app/shared/model/enumerations/staff-status.model';
import { StaffLoginType } from 'app/shared/model/enumerations/staff-login-type.model';

export interface IStaff {
  id?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  status?: StaffStatus;
  loginType?: StaffLoginType;
  team?: ITeam;
}

export class Staff implements IStaff {
  constructor(
    public id?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public password?: string,
    public status?: StaffStatus,
    public loginType?: StaffLoginType,
    public team?: ITeam
  ) {}
}
