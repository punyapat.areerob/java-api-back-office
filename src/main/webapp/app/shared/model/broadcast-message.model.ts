import { Moment } from 'moment';
import { BroadcastMessageStatus } from 'app/shared/model/enumerations/broadcast-message-status.model';
import { BroadcastMessageVisible } from 'app/shared/model/enumerations/broadcast-message-visible.model';

export interface IBroadcastMessage {
  id?: string;
  subject?: string;
  message?: string;
  from?: Moment;
  to?: Moment;
  status?: BroadcastMessageStatus;
  visible?: BroadcastMessageVisible;
  creatorTeamId?: string;
}

export class BroadcastMessage implements IBroadcastMessage {
  constructor(
    public id?: string,
    public subject?: string,
    public message?: string,
    public from?: Moment,
    public to?: Moment,
    public status?: BroadcastMessageStatus,
    public visible?: BroadcastMessageVisible,
    public creatorTeamId?: string
  ) {}
}
