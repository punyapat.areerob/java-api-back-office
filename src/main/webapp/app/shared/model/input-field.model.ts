import { ICaseStep } from 'app/shared/model/case-step.model';
import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';

export interface IInputField {
  id?: string;
  name?: string;
  label?: string;
  link?: string;
  caseStep?: ICaseStep;
  caseStepInstance?: ICaseStepInstance;
}

export class InputField implements IInputField {
  constructor(
    public id?: string,
    public name?: string,
    public label?: string,
    public link?: string,
    public caseStep?: ICaseStep,
    public caseStepInstance?: ICaseStepInstance
  ) {}
}
