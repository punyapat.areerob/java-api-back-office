import { Moment } from 'moment';
import { CaseActivityEvent } from 'app/shared/model/enumerations/case-activity-event.model';

export interface ICaseActivity {
  id?: string;
  caseInstanceId?: string;
  event?: CaseActivityEvent;
  remark?: string;
  createdBy?: string;
  updatedBy?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class CaseActivity implements ICaseActivity {
  constructor(
    public id?: string,
    public caseInstanceId?: string,
    public event?: CaseActivityEvent,
    public remark?: string,
    public createdBy?: string,
    public updatedBy?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
