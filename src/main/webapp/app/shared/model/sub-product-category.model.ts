import { IProduct } from 'app/shared/model/product.model';

export interface ISubProductCategory {
  id?: string;
  productCategoryId?: string;
  name?: string;
  icon?: string;
  description?: string;
  active?: boolean;
  productLists?: IProduct[];
}

export class SubProductCategory implements ISubProductCategory {
  constructor(
    public id?: string,
    public productCategoryId?: string,
    public name?: string,
    public icon?: string,
    public description?: string,
    public active?: boolean,
    public productLists?: IProduct[]
  ) {
    this.active = this.active || false;
  }
}
