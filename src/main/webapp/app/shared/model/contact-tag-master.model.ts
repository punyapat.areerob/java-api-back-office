export interface IContactTagMaster {
  id?: string;
  name?: string;
  color?: string;
  createBy?: string;
  active?: boolean;
}

export class ContactTagMaster implements IContactTagMaster {
  constructor(public id?: string, public name?: string, public color?: string, public createBy?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
