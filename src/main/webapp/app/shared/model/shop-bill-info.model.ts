export interface IShopBillInfo {
  id?: string;
  vendorId?: string;
  companyName?: string;
  taxId?: string;
  address?: string;
  status?: string;
  active?: boolean;
}

export class ShopBillInfo implements IShopBillInfo {
  constructor(
    public id?: string,
    public vendorId?: string,
    public companyName?: string,
    public taxId?: string,
    public address?: string,
    public status?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
