export interface IStepOwner {
  id?: string;
  staffId?: string;
  givenName?: string;
  middleName?: string;
  surName?: string;
  email?: string;
}

export class StepOwner implements IStepOwner {
  constructor(
    public id?: string,
    public staffId?: string,
    public givenName?: string,
    public middleName?: string,
    public surName?: string,
    public email?: string
  ) {}
}
