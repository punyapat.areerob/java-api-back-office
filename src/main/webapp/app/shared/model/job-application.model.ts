import { JobApplicationStatus } from 'app/shared/model/enumerations/job-application-status.model';

export interface IJobApplication {
  id?: string;
  status?: JobApplicationStatus;
}

export class JobApplication implements IJobApplication {
  constructor(public id?: string, public status?: JobApplicationStatus) {}
}
