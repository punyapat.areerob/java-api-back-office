import { AgentRateType } from 'app/shared/model/enumerations/agent-rate-type.model';

export interface IAgentCommissionRate {
  id?: string;
  constraint?: string;
  commissionRate?: number;
  type?: AgentRateType;
  comment?: string;
}

export class AgentCommissionRate implements IAgentCommissionRate {
  constructor(
    public id?: string,
    public constraint?: string,
    public commissionRate?: number,
    public type?: AgentRateType,
    public comment?: string
  ) {}
}
