export interface IEmailTemplateMeta {
  id?: string;
}

export class EmailTemplateMeta implements IEmailTemplateMeta {
  constructor(public id?: string) {}
}
