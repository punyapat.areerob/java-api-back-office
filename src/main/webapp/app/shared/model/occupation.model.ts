export interface IOccupation {
  id?: string;
  name?: string;
}

export class Occupation implements IOccupation {
  constructor(public id?: string, public name?: string) {}
}
