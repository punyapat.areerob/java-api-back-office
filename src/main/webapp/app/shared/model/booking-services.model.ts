import { Moment } from 'moment';
import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { IBookingServiceMember } from 'app/shared/model/booking-service-member.model';
import { IBooking } from 'app/shared/model/booking.model';

export interface IBookingServices {
  id?: string;
  shopCategoryId?: string;
  shopId?: string;
  remark?: string;
  isActive?: boolean;
  serviceName?: string;
  serviceId?: string;
  fromLatitude?: string;
  fromLongitude?: string;
  toLatitude?: string;
  toLongitude?: string;
  createdBy?: Moment;
  updatedBy?: Moment;
  createdAt?: Moment;
  updatedAt?: Moment;
  bookingStatusId?: IBookingStatus;
  bookingServiceMemberIds?: IBookingServiceMember[];
  booking?: IBooking;
}

export class BookingServices implements IBookingServices {
  constructor(
    public id?: string,
    public shopCategoryId?: string,
    public shopId?: string,
    public remark?: string,
    public isActive?: boolean,
    public serviceName?: string,
    public serviceId?: string,
    public fromLatitude?: string,
    public fromLongitude?: string,
    public toLatitude?: string,
    public toLongitude?: string,
    public createdBy?: Moment,
    public updatedBy?: Moment,
    public createdAt?: Moment,
    public updatedAt?: Moment,
    public bookingStatusId?: IBookingStatus,
    public bookingServiceMemberIds?: IBookingServiceMember[],
    public booking?: IBooking
  ) {
    this.isActive = this.isActive || false;
  }
}
