import { Moment } from 'moment';

export interface IAgentMemberActivation {
  id?: string;
  agentId?: string;
  invoice?: string;
  date?: Moment;
  activate?: Moment;
  remark?: string;
  currency?: string;
  money?: number;
}

export class AgentMemberActivation implements IAgentMemberActivation {
  constructor(
    public id?: string,
    public agentId?: string,
    public invoice?: string,
    public date?: Moment,
    public activate?: Moment,
    public remark?: string,
    public currency?: string,
    public money?: number
  ) {}
}
