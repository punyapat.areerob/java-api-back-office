export interface IGender {
  id?: string;
  name?: string;
}

export class Gender implements IGender {
  constructor(public id?: string, public name?: string) {}
}
