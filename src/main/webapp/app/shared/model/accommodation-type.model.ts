export interface IAccommodationType {
  id?: string;
  name?: string;
  active?: boolean;
}

export class AccommodationType implements IAccommodationType {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
