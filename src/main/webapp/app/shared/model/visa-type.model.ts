export interface IVisaType {
  id?: string;
  name?: string;
  active?: boolean;
}

export class VisaType implements IVisaType {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
