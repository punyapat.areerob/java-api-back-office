import { IVendor } from 'app/shared/model/vendor.model';
import { IShop } from 'app/shared/model/shop.model';

export interface IVendorsContact {
  id?: string;
  title?: string;
  firstName?: string;
  lastName?: string;
  mobile?: string;
  email?: string;
  businessTitle?: string;
  contactTagId?: string;
  contactTagName?: string;
  active?: boolean;
  vendor?: IVendor;
  shop?: IShop;
}

export class VendorsContact implements IVendorsContact {
  constructor(
    public id?: string,
    public title?: string,
    public firstName?: string,
    public lastName?: string,
    public mobile?: string,
    public email?: string,
    public businessTitle?: string,
    public contactTagId?: string,
    public contactTagName?: string,
    public active?: boolean,
    public vendor?: IVendor,
    public shop?: IShop
  ) {
    this.active = this.active || false;
  }
}
