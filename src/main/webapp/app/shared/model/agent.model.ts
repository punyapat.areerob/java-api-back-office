import { AgentType } from 'app/shared/model/enumerations/agent-type.model';
import { AgentStatus } from 'app/shared/model/enumerations/agent-status.model';

export interface IAgent {
  id?: string;
  agentId?: string;
  type?: AgentType;
  name?: string;
  address?: string;
  fax?: string;
  phone?: string;
  website?: string;
  email?: string;
  status?: AgentStatus;
}

export class Agent implements IAgent {
  constructor(
    public id?: string,
    public agentId?: string,
    public type?: AgentType,
    public name?: string,
    public address?: string,
    public fax?: string,
    public phone?: string,
    public website?: string,
    public email?: string,
    public status?: AgentStatus
  ) {}
}
