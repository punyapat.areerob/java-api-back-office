import { IProduct } from 'app/shared/model/product.model';

export interface IProductVariantGroup {
  id?: string;
  shopId?: string;
  name?: string;
  type?: string;
  images?: string;
  active?: boolean;
  product?: IProduct;
}

export class ProductVariantGroup implements IProductVariantGroup {
  constructor(
    public id?: string,
    public shopId?: string,
    public name?: string,
    public type?: string,
    public images?: string,
    public active?: boolean,
    public product?: IProduct
  ) {
    this.active = this.active || false;
  }
}
