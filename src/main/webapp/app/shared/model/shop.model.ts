import { Moment } from 'moment';
import { IShopBillInfo } from 'app/shared/model/shop-bill-info.model';
import { IVendorsContact } from 'app/shared/model/vendors-contact.model';

export interface IShop {
  id?: string;
  name?: string;
  vendorId?: string;
  email?: string;
  openTime?: Moment;
  closeTime?: Moment;
  mobile?: string;
  address?: string;
  subDistrict?: string;
  district?: string;
  province?: string;
  country?: string;
  postCode?: string;
  latitude?: number;
  longitude?: number;
  active?: boolean;
  billInfo?: IShopBillInfo;
  contacts?: IVendorsContact[];
}

export class Shop implements IShop {
  constructor(
    public id?: string,
    public name?: string,
    public vendorId?: string,
    public email?: string,
    public openTime?: Moment,
    public closeTime?: Moment,
    public mobile?: string,
    public address?: string,
    public subDistrict?: string,
    public district?: string,
    public province?: string,
    public country?: string,
    public postCode?: string,
    public latitude?: number,
    public longitude?: number,
    public active?: boolean,
    public billInfo?: IShopBillInfo,
    public contacts?: IVendorsContact[]
  ) {
    this.active = this.active || false;
  }
}
