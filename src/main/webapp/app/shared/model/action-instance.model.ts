import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { ActionType } from 'app/shared/model/enumerations/action-type.model';

export interface IActionInstance {
  id?: string;
  name?: string;
  next?: string;
  type?: ActionType;
  caseStepInstance?: ICaseStepInstance;
}

export class ActionInstance implements IActionInstance {
  constructor(
    public id?: string,
    public name?: string,
    public next?: string,
    public type?: ActionType,
    public caseStepInstance?: ICaseStepInstance
  ) {}
}
