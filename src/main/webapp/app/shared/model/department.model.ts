export interface IDepartment {
  id?: string;
  name?: string;
  active?: boolean;
}

export class Department implements IDepartment {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
