import { Moment } from 'moment';
import { StepState } from 'app/shared/model/enumerations/step-state.model';

export interface ICaseInstance {
  id?: string;
  caseNo?: string;
  caseId?: string;
  currentStepId?: string;
  remark?: string;
  memberId?: string;
  caseDataId?: string;
  stepStatus?: StepState;
  createdBy?: string;
  updatedBy?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class CaseInstance implements ICaseInstance {
  constructor(
    public id?: string,
    public caseNo?: string,
    public caseId?: string,
    public currentStepId?: string,
    public remark?: string,
    public memberId?: string,
    public caseDataId?: string,
    public stepStatus?: StepState,
    public createdBy?: string,
    public updatedBy?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
