import { ICaseStep } from 'app/shared/model/case-step.model';
import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';

export interface IVerifyField {
  id?: string;
  name?: string;
  label?: string;
  link?: string;
  verify?: boolean;
  caseStep?: ICaseStep;
  caseStepInstance?: ICaseStepInstance;
}

export class VerifyField implements IVerifyField {
  constructor(
    public id?: string,
    public name?: string,
    public label?: string,
    public link?: string,
    public verify?: boolean,
    public caseStep?: ICaseStep,
    public caseStepInstance?: ICaseStepInstance
  ) {
    this.verify = this.verify || false;
  }
}
