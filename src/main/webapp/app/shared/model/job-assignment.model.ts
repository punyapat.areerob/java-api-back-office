import { Moment } from 'moment';

export interface IJobAssignment {
  id?: string;
  imageUrl?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class JobAssignment implements IJobAssignment {
  constructor(public id?: string, public imageUrl?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
