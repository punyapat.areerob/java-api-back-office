export interface IPrivilege {
  id?: string;
  name?: string;
  active?: boolean;
}

export class Privilege implements IPrivilege {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
