import { CaseStatus } from 'app/shared/model/enumerations/case-status.model';

export interface ICaseTemplate {
  id?: string;
  name?: string;
  status?: CaseStatus;
}

export class CaseTemplate implements ICaseTemplate {
  constructor(public id?: string, public name?: string, public status?: CaseStatus) {}
}
