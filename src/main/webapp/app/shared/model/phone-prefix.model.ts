export interface IPhonePrefix {
  id?: string;
  name?: string;
  dialCode?: string;
}

export class PhonePrefix implements IPhonePrefix {
  constructor(public id?: string, public name?: string, public dialCode?: string) {}
}
