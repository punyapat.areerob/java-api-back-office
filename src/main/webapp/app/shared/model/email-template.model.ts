export interface IEmailTemplate {
  id?: string;
  name?: string;
  description?: string;
  content?: string;
}

export class EmailTemplate implements IEmailTemplate {
  constructor(public id?: string, public name?: string, public description?: string, public content?: string) {}
}
