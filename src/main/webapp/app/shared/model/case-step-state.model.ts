import { ICaseStepInstance } from 'app/shared/model/case-step-instance.model';
import { ICaseStep } from 'app/shared/model/case-step.model';
import { StepState } from 'app/shared/model/enumerations/step-state.model';

export interface ICaseStepState {
  id?: string;
  name?: string;
  state?: StepState;
  caseStepInstances?: ICaseStepInstance[];
  step?: ICaseStep;
}

export class CaseStepState implements ICaseStepState {
  constructor(
    public id?: string,
    public name?: string,
    public state?: StepState,
    public caseStepInstances?: ICaseStepInstance[],
    public step?: ICaseStep
  ) {}
}
