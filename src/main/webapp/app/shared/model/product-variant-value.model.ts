export interface IProductVariantValue {
  id?: string;
  productVariantGroupId?: string;
  name?: string;
  size?: string;
  status?: string;
  active?: boolean;
}

export class ProductVariantValue implements IProductVariantValue {
  constructor(
    public id?: string,
    public productVariantGroupId?: string,
    public name?: string,
    public size?: string,
    public status?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
