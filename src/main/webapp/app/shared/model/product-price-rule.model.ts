import { Moment } from 'moment';

export interface IProductPriceRule {
  id?: string;
  productId?: string;
  usedQuota?: string;
  priceSale?: string;
  priceBase?: string;
  status?: string;
  activeStartDate?: Moment;
  activeEndDate?: Moment;
  active?: boolean;
}

export class ProductPriceRule implements IProductPriceRule {
  constructor(
    public id?: string,
    public productId?: string,
    public usedQuota?: string,
    public priceSale?: string,
    public priceBase?: string,
    public status?: string,
    public activeStartDate?: Moment,
    public activeEndDate?: Moment,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
