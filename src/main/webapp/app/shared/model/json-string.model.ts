export interface IJsonString {
  id?: string;
  key?: string;
  value?: string;
}

export class JsonString implements IJsonString {
  constructor(public id?: string, public key?: string, public value?: string) {}
}
