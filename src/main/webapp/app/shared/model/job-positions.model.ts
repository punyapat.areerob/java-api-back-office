import { Moment } from 'moment';
import { JobPositionsStatus } from 'app/shared/model/enumerations/job-positions-status.model';

export interface IJobPositions {
  id?: string;
  name?: string;
  teamId?: string;
  teamName?: string;
  jobDescriptionTemplateId?: string;
  jobDescriptionTemplateName?: string;
  jobTitle?: string;
  lineOfWork?: string;
  department?: string;
  educationBackground?: string;
  salary?: number;
  location?: string;
  summary?: string;
  mainReponsibilities?: string;
  jobDescription?: string;
  jobQualifications?: string;
  profilePhoto?: boolean;
  militaryPass?: boolean;
  enLanguageScore?: boolean;
  educationCertificate?: boolean;
  driverLicense?: boolean;
  chLanguageScore?: boolean;
  educationQualification?: boolean;
  citizenID?: boolean;
  jpLanguageScore?: boolean;
  workPermit?: boolean;
  marriageCertificate?: boolean;
  houseRegistry?: boolean;
  birthCertificate?: boolean;
  from?: Moment;
  to?: Moment;
  status?: JobPositionsStatus;
}

export class JobPositions implements IJobPositions {
  constructor(
    public id?: string,
    public name?: string,
    public teamId?: string,
    public teamName?: string,
    public jobDescriptionTemplateId?: string,
    public jobDescriptionTemplateName?: string,
    public jobTitle?: string,
    public lineOfWork?: string,
    public department?: string,
    public educationBackground?: string,
    public salary?: number,
    public location?: string,
    public summary?: string,
    public mainReponsibilities?: string,
    public jobDescription?: string,
    public jobQualifications?: string,
    public profilePhoto?: boolean,
    public militaryPass?: boolean,
    public enLanguageScore?: boolean,
    public educationCertificate?: boolean,
    public driverLicense?: boolean,
    public chLanguageScore?: boolean,
    public educationQualification?: boolean,
    public citizenID?: boolean,
    public jpLanguageScore?: boolean,
    public workPermit?: boolean,
    public marriageCertificate?: boolean,
    public houseRegistry?: boolean,
    public birthCertificate?: boolean,
    public from?: Moment,
    public to?: Moment,
    public status?: JobPositionsStatus
  ) {
    this.profilePhoto = this.profilePhoto || false;
    this.militaryPass = this.militaryPass || false;
    this.enLanguageScore = this.enLanguageScore || false;
    this.educationCertificate = this.educationCertificate || false;
    this.driverLicense = this.driverLicense || false;
    this.chLanguageScore = this.chLanguageScore || false;
    this.educationQualification = this.educationQualification || false;
    this.citizenID = this.citizenID || false;
    this.jpLanguageScore = this.jpLanguageScore || false;
    this.workPermit = this.workPermit || false;
    this.marriageCertificate = this.marriageCertificate || false;
    this.houseRegistry = this.houseRegistry || false;
    this.birthCertificate = this.birthCertificate || false;
  }
}
