export interface ITitle {
  id?: string;
  name?: string;
  active?: boolean;
}

export class Title implements ITitle {
  constructor(public id?: string, public name?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
