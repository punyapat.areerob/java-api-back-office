import { Moment } from 'moment';
import { CardStatus } from 'app/shared/model/enumerations/card-status.model';
import { CardType } from 'app/shared/model/enumerations/card-type.model';

export interface ICard {
  id?: string;
  name?: string;
  abbreviation?: string;
  description?: string;
  status?: CardStatus;
  type?: CardType;
  photo?: string;
  requireApplicant?: number;
  memberValidity?: number;
  memberFee?: number;
  memberFeeVat?: number;
  additionalMember?: number;
  additionalMemberFee?: number;
  numberOfTransfer?: number;
  transferFee?: number;
  annualFee?: number;
  annualFeeVat?: number;
  ageMin?: number;
  ageMax?: number;
  visaPeriod?: number;
  activeStartDate?: Moment;
  activeEndDate?: Moment;
  memberInformationAgreement?: string;
  memberPackageAgreement?: string;
  remark?: string;
  active?: boolean;
}

export class Card implements ICard {
  constructor(
    public id?: string,
    public name?: string,
    public abbreviation?: string,
    public description?: string,
    public status?: CardStatus,
    public type?: CardType,
    public photo?: string,
    public requireApplicant?: number,
    public memberValidity?: number,
    public memberFee?: number,
    public memberFeeVat?: number,
    public additionalMember?: number,
    public additionalMemberFee?: number,
    public numberOfTransfer?: number,
    public transferFee?: number,
    public annualFee?: number,
    public annualFeeVat?: number,
    public ageMin?: number,
    public ageMax?: number,
    public visaPeriod?: number,
    public activeStartDate?: Moment,
    public activeEndDate?: Moment,
    public memberInformationAgreement?: string,
    public memberPackageAgreement?: string,
    public remark?: string,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
