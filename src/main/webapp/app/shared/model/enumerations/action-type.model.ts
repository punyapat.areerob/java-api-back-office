export const enum ActionType {
  START = 'START',

  DONE = 'DONE',

  REJECT = 'REJECT',

  MOVE_TO = 'MOVE_TO',

  SKIP = 'SKIP',
}
