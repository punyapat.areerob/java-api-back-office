export const enum CaseActivityEvent {
  CREATED = 'CREATED',

  COMMENT = 'COMMENT',

  ACTION = 'ACTION',

  UPDATED = 'UPDATED',
}
