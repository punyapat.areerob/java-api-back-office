export const enum AgentType {
  GSSA = 'GSSA',

  Individual = 'Individual',

  HenleyAndPartner = 'HenleyAndPartner',
}
