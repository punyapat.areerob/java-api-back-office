export const enum StaffStatus {
  ACTIVE = 'ACTIVE',

  PENDING = 'PENDING',

  INACTIVE = 'INACTIVE',
}
