export const enum JobPositionsStatus {
  ACTIVE = 'ACTIVE',

  DRAFT = 'DRAFT',

  EXPIRED = 'EXPIRED',
}
