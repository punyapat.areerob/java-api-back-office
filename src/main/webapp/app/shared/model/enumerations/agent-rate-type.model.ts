export const enum AgentRateType {
  STEP = 'STEP',

  FIXED = 'FIXED',

  HENLEY = 'HENLEY',
}
