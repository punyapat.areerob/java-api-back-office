export const enum JobApplicationStatus {
  CREATED = 'CREATED',

  REVIEW = 'REVIEW',

  INTERVIEW = 'INTERVIEW',

  REJECTED = 'REJECTED',

  HIRED = 'HIRED',
}
