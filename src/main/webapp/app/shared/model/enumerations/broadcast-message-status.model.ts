export const enum BroadcastMessageStatus {
  DRAFT = 'DRAFT',

  ACTIVE = 'ACTIVE',

  EXPIRED = 'EXPIRED',
}
