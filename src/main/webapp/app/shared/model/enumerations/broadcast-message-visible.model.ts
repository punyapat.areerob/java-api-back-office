export const enum BroadcastMessageVisible {
  TEAM = 'TEAM',

  ALL = 'ALL',

  CUSTOM = 'CUSTOM',
}
