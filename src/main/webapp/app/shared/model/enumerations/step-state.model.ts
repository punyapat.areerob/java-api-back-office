export const enum StepState {
  NEW = 'NEW',

  IN_PROGRESS = 'IN_PROGRESS',

  DONE = 'DONE',

  BLOCK = 'BLOCK',
}
