export const enum CardPrivilegeValidity {
  YEARLY = 'YEARLY',

  LIFE_TIME = 'LIFE_TIME',
}
