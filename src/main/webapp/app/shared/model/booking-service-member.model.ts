import { Moment } from 'moment';
import { IBookingStatus } from 'app/shared/model/booking-status.model';
import { IJobAssignment } from 'app/shared/model/job-assignment.model';
import { IBookingGuest } from 'app/shared/model/booking-guest.model';
import { IBookingServices } from 'app/shared/model/booking-services.model';

export interface IBookingServiceMember {
  id?: string;
  userId?: string;
  productPriceRule?: string;
  skuId?: string;
  price?: number;
  remark?: string;
  startTime?: Moment;
  endTime?: Moment;
  imageUpload?: string;
  bookingDetail?: string;
  createdBy?: Moment;
  updatedBy?: Moment;
  createdAt?: Moment;
  updatedAt?: Moment;
  bookingStatusId?: IBookingStatus;
  jobAssignmentId?: IJobAssignment;
  bookingGuestIds?: IBookingGuest[];
  bookingServices?: IBookingServices;
}

export class BookingServiceMember implements IBookingServiceMember {
  constructor(
    public id?: string,
    public userId?: string,
    public productPriceRule?: string,
    public skuId?: string,
    public price?: number,
    public remark?: string,
    public startTime?: Moment,
    public endTime?: Moment,
    public imageUpload?: string,
    public bookingDetail?: string,
    public createdBy?: Moment,
    public updatedBy?: Moment,
    public createdAt?: Moment,
    public updatedAt?: Moment,
    public bookingStatusId?: IBookingStatus,
    public jobAssignmentId?: IJobAssignment,
    public bookingGuestIds?: IBookingGuest[],
    public bookingServices?: IBookingServices
  ) {}
}
