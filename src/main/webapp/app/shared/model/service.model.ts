import { IProduct } from 'app/shared/model/product.model';

export interface IService {
  id?: string;
  name?: string;
  description?: string;
  active?: boolean;
  products?: IProduct[];
}

export class Service implements IService {
  constructor(
    public id?: string,
    public name?: string,
    public description?: string,
    public active?: boolean,
    public products?: IProduct[]
  ) {
    this.active = this.active || false;
  }
}
