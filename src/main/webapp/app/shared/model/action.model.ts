import { ICaseStep } from 'app/shared/model/case-step.model';
import { ActionType } from 'app/shared/model/enumerations/action-type.model';

export interface IAction {
  id?: string;
  name?: string;
  next?: string;
  type?: ActionType;
  caseStep?: ICaseStep;
}

export class Action implements IAction {
  constructor(public id?: string, public name?: string, public next?: string, public type?: ActionType, public caseStep?: ICaseStep) {}
}
