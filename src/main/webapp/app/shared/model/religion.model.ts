export interface IReligion {
  id?: string;
  name?: string;
}

export class Religion implements IReligion {
  constructor(public id?: string, public name?: string) {}
}
