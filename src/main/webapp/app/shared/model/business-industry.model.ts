export interface IBusinessIndustry {
  id?: string;
  name?: string;
}

export class BusinessIndustry implements IBusinessIndustry {
  constructor(public id?: string, public name?: string) {}
}
