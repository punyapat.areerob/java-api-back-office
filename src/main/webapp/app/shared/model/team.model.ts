export interface ITeam {
  id?: string;
  name?: string;
  misRole?: string;
  active?: boolean;
}

export class Team implements ITeam {
  constructor(public id?: string, public name?: string, public misRole?: string, public active?: boolean) {
    this.active = this.active || false;
  }
}
