export interface IProspectData {
  id?: string;
  title?: string;
  givenName?: string;
  middleName?: string;
  surName?: string;
  areaCode?: string;
  contactNo?: string;
  passportNo?: string;
  passportExpireDate?: string;
  passportDateOfBirth?: string;
  agreePda?: boolean;
  allowContactMe?: boolean;
}

export class ProspectData implements IProspectData {
  constructor(
    public id?: string,
    public title?: string,
    public givenName?: string,
    public middleName?: string,
    public surName?: string,
    public areaCode?: string,
    public contactNo?: string,
    public passportNo?: string,
    public passportExpireDate?: string,
    public passportDateOfBirth?: string,
    public agreePda?: boolean,
    public allowContactMe?: boolean
  ) {
    this.agreePda = this.agreePda || false;
    this.allowContactMe = this.allowContactMe || false;
  }
}
