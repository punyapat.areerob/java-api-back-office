package com.thailandelite.mis.migrate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TAX_ID" ,
    "VENDER_ADDR_TAX" ,
    "SUB_DISTRICT_TAX_ID" ,
    "DISTRICT_TAX_ID" ,
    "PROVINCE_TAX_ID" ,
    "VENDER_TAX_ZIPCODE" ,
    "VENDER_TAX_PHONE" ,
    "VENDER_TAX_FAX" ,
    "VENDER_ID"
})
@Data
public class VendorBillInfoDB {

    @JsonProperty("TAX_ID")
    public Integer taxId;
    @JsonProperty("VENDER_ID")
    public Integer venderId;
    @JsonProperty("VENDER_ADDR_TAX")
    public String venderAddrTax;
    @JsonProperty("SUB_DISTRICT_TAX_ID")
    public Integer subDistrictTaxId;
    @JsonProperty("DISTRICT_TAX_ID")
    public Integer districtTaxId;
    @JsonProperty("PROVINCE_TAX_ID")
    public Integer provinceTaxId;
    @JsonProperty("VENDER_TAX_ZIPCODE")
    public String venderTaxZipcode;
    @JsonProperty("VENDER_TAX_PHONE")
    public String venderTaxPhone;
    @JsonProperty("VENDER_TAX_FAX")
    public String venderTaxFax;
    @JsonProperty("RECORD_STATUS")
    public String RecordStatus;
    @JsonProperty("CREATE_DATE")
    public String CreateDate;
    @JsonProperty("CREATE_USER")
    public String CreateUser;
    @JsonProperty("CREATE_USER_TYPE")
    public String CreateUserType;
    @JsonProperty("LAST_DATE")
    public String LastDate;
    @JsonProperty("LAST_USER")
    public String LastUser;
    @JsonProperty("LAST_USER_TYPE")
    public String LastUserType;

}
