package com.thailandelite.mis.migrate;

public class MASVendor extends MASDB{
    private float VENDER_ID;
    private String VENDER_PICTURE;
    private String VENDER_HEADER = null;
    private String VENDER_PATH;
    private float PRIVILEGE_GROUP_ID;
    private String VENDER_CODE;
    private String VENDER_NAME_TH;
    private String VENDER_NAME_EN;
    private String DESCRIPTION;
    private String VENDER_ADDR;
    private String SUB_DISTRICT_ID = null;
    private String DISTRICT_ID = null;
    private float PROVINCE_ID;
    private String VENDER_ZIPCODE;
    private String VENDER_PHONE;
    private String VENDER_FAX;
    private String VENDER_WEBSITE;
    private String VENDER_MAP = null;
    private float VENDER_SERVICE_POINT;
    private float VENDER_SERVICE_TIMES;
    private String EMAIL;
    private String CONTACT_NAME = null;
    private String VENDER_RESERVATION_POLICY_DAY = null;
    private float VENDER_RESERVATION_POLICY_HOUR;
    private String VENDER_CANCELLATION_POLICY_DAY = null;
    private float VENDER_CANCELLATION_POLICY_HOUR;
    private String VENDER_CONTACT_RESERVATION_NAME = null;
    private String VENDER_CONTACT_RESERVATION_PHONE = null;
    private String VENDER_CONTACT_RESERVATION_FAX = null;
    private String VENDER_CONTACT_RESERVATION_MOBILE = null;
    private String VENDER_CONTACT_RESERVATION_EMAIL = null;
    private String VENDER_CONTACT_SETTLEMENT_NAME = null;
    private String VENDER_CONTACT_SETTLEMENT_PHONE = null;
    private String VENDER_CONTACT_SETTLEMENT_FAX = null;
    private String VENDER_CONTACT_SETTLEMENT_MOBILE = null;
    private String VENDER_CONTACT_SETTLEMENT_EMAIL = null;
    private String VENDER_CONTRACT_START_DATE = null;
    private String VENDER_CONTRACT_END_DATE = null;
    private String VENDER_PRICE = null;
    private float PAYMENT_ID;
    private String VENDER_STATUS;
    private float COMPANY_ID;
    private String POLICY;
    private String REMARK = null;
    private String CONTACT_BY;
    private String TAX_ID = null;
    private String VENDER_ADDR_TAX = null;
    private String SUB_DISTRICT_TAX_ID = null;
    private String DISTRICT_TAX_ID = null;
    private String PROVINCE_TAX_ID = null;
    private String VENDER_TAX_ZIPCODE = null;
    private String VENDER_TAX_PHONE = null;
    private String VENDER_TAX_FAX = null;
    private String ATTACH_FILE1 = null;
    private String ATTACH_FILE2 = null;
    private String ATTACH_FILE3 = null;
    private String VENDOR_STATUS;
    private String SERVICE_TYPE_CODE_old = null;
    private String VENDOR_ID_old = null;
    private String VENDER_ACTIVE_STATUS = null;
    private String PROVINCE_CODE = null;
    private String REMARK_VENDER_EXPIRE = null;
    private String ACTIVE_NEW = null;
    private String PROVINCE_ID_new = null;
    private String VENDER_PRICE_DETAIL = null;
    private String REF_ID = null;
    private String EXPIRY_DATE_BACK = null;
    private String PROVINCE_ID_new1 = null;
    private String VENDER_BENEFIT = null;
    private String VENDER_OTHER_FILE = null;
    private String VENDER_COPY_ID = null;


    // Getter Methods

    public float getVENDER_ID() {
        return VENDER_ID;
    }

    public String getVENDER_PICTURE() {
        return VENDER_PICTURE;
    }

    public String getVENDER_HEADER() {
        return VENDER_HEADER;
    }

    public String getVENDER_PATH() {
        return VENDER_PATH;
    }

    public float getPRIVILEGE_GROUP_ID() {
        return PRIVILEGE_GROUP_ID;
    }

    public String getVENDER_CODE() {
        return VENDER_CODE;
    }

    public String getVENDER_NAME_TH() {
        return VENDER_NAME_TH;
    }

    public String getVENDER_NAME_EN() {
        return VENDER_NAME_EN;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public String getVENDER_ADDR() {
        return VENDER_ADDR;
    }

    public String getSUB_DISTRICT_ID() {
        return SUB_DISTRICT_ID;
    }

    public String getDISTRICT_ID() {
        return DISTRICT_ID;
    }

    public float getPROVINCE_ID() {
        return PROVINCE_ID;
    }

    public String getVENDER_ZIPCODE() {
        return VENDER_ZIPCODE;
    }

    public String getVENDER_PHONE() {
        return VENDER_PHONE;
    }

    public String getVENDER_FAX() {
        return VENDER_FAX;
    }

    public String getVENDER_WEBSITE() {
        return VENDER_WEBSITE;
    }

    public String getVENDER_MAP() {
        return VENDER_MAP;
    }

    public float getVENDER_SERVICE_POINT() {
        return VENDER_SERVICE_POINT;
    }

    public float getVENDER_SERVICE_TIMES() {
        return VENDER_SERVICE_TIMES;
    }

    public String getRECORD_STATUS() {
        return RECORD_STATUS;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public String getCREATE_USER_TYPE() {
        return CREATE_USER_TYPE;
    }

    public String getLAST_DATE() {
        return LAST_DATE;
    }

    public String getLAST_USER() {
        return LAST_USER;
    }

    public String getLAST_USER_TYPE() {
        return LAST_USER_TYPE;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public String getCONTACT_NAME() {
        return CONTACT_NAME;
    }

    public String getVENDER_RESERVATION_POLICY_DAY() {
        return VENDER_RESERVATION_POLICY_DAY;
    }

    public float getVENDER_RESERVATION_POLICY_HOUR() {
        return VENDER_RESERVATION_POLICY_HOUR;
    }

    public String getVENDER_CANCELLATION_POLICY_DAY() {
        return VENDER_CANCELLATION_POLICY_DAY;
    }

    public float getVENDER_CANCELLATION_POLICY_HOUR() {
        return VENDER_CANCELLATION_POLICY_HOUR;
    }

    public String getVENDER_CONTACT_RESERVATION_NAME() {
        return VENDER_CONTACT_RESERVATION_NAME;
    }

    public String getVENDER_CONTACT_RESERVATION_PHONE() {
        return VENDER_CONTACT_RESERVATION_PHONE;
    }

    public String getVENDER_CONTACT_RESERVATION_FAX() {
        return VENDER_CONTACT_RESERVATION_FAX;
    }

    public String getVENDER_CONTACT_RESERVATION_MOBILE() {
        return VENDER_CONTACT_RESERVATION_MOBILE;
    }

    public String getVENDER_CONTACT_RESERVATION_EMAIL() {
        return VENDER_CONTACT_RESERVATION_EMAIL;
    }

    public String getVENDER_CONTACT_SETTLEMENT_NAME() {
        return VENDER_CONTACT_SETTLEMENT_NAME;
    }

    public String getVENDER_CONTACT_SETTLEMENT_PHONE() {
        return VENDER_CONTACT_SETTLEMENT_PHONE;
    }

    public String getVENDER_CONTACT_SETTLEMENT_FAX() {
        return VENDER_CONTACT_SETTLEMENT_FAX;
    }

    public String getVENDER_CONTACT_SETTLEMENT_MOBILE() {
        return VENDER_CONTACT_SETTLEMENT_MOBILE;
    }

    public String getVENDER_CONTACT_SETTLEMENT_EMAIL() {
        return VENDER_CONTACT_SETTLEMENT_EMAIL;
    }

    public String getVENDER_CONTRACT_START_DATE() {
        return VENDER_CONTRACT_START_DATE;
    }

    public String getVENDER_CONTRACT_END_DATE() {
        return VENDER_CONTRACT_END_DATE;
    }

    public String getVENDER_PRICE() {
        return VENDER_PRICE;
    }

    public float getPAYMENT_ID() {
        return PAYMENT_ID;
    }

    public String getVENDER_STATUS() {
        return VENDER_STATUS;
    }

    public float getCOMPANY_ID() {
        return COMPANY_ID;
    }

    public String getPOLICY() {
        return POLICY;
    }

    public String getREMARK() {
        return REMARK;
    }

    public String getCONTACT_BY() {
        return CONTACT_BY;
    }

    public String getTAX_ID() {
        return TAX_ID;
    }

    public String getVENDER_ADDR_TAX() {
        return VENDER_ADDR_TAX;
    }

    public String getSUB_DISTRICT_TAX_ID() {
        return SUB_DISTRICT_TAX_ID;
    }

    public String getDISTRICT_TAX_ID() {
        return DISTRICT_TAX_ID;
    }

    public String getPROVINCE_TAX_ID() {
        return PROVINCE_TAX_ID;
    }

    public String getVENDER_TAX_ZIPCODE() {
        return VENDER_TAX_ZIPCODE;
    }

    public String getVENDER_TAX_PHONE() {
        return VENDER_TAX_PHONE;
    }

    public String getVENDER_TAX_FAX() {
        return VENDER_TAX_FAX;
    }

    public String getATTACH_FILE1() {
        return ATTACH_FILE1;
    }

    public String getATTACH_FILE2() {
        return ATTACH_FILE2;
    }

    public String getATTACH_FILE3() {
        return ATTACH_FILE3;
    }

    public String getVENDOR_STATUS() {
        return VENDOR_STATUS;
    }

    public String getSERVICE_TYPE_CODE_old() {
        return SERVICE_TYPE_CODE_old;
    }

    public String getVENDOR_ID_old() {
        return VENDOR_ID_old;
    }

    public String getVENDER_ACTIVE_STATUS() {
        return VENDER_ACTIVE_STATUS;
    }

    public String getPROVINCE_CODE() {
        return PROVINCE_CODE;
    }

    public String getREMARK_VENDER_EXPIRE() {
        return REMARK_VENDER_EXPIRE;
    }

    public String getACTIVE_NEW() {
        return ACTIVE_NEW;
    }

    public String getPROVINCE_ID_new() {
        return PROVINCE_ID_new;
    }

    public String getVENDER_PRICE_DETAIL() {
        return VENDER_PRICE_DETAIL;
    }

    public String getREF_ID() {
        return REF_ID;
    }

    public String getEXPIRY_DATE_BACK() {
        return EXPIRY_DATE_BACK;
    }

    public String getPROVINCE_ID_new1() {
        return PROVINCE_ID_new1;
    }

    public String getVENDER_BENEFIT() {
        return VENDER_BENEFIT;
    }

    public String getVENDER_OTHER_FILE() {
        return VENDER_OTHER_FILE;
    }

    public String getVENDER_COPY_ID() {
        return VENDER_COPY_ID;
    }

    // Setter Methods

    public void setVENDER_ID(float VENDER_ID) {
        this.VENDER_ID = VENDER_ID;
    }

    public void setVENDER_PICTURE(String VENDER_PICTURE) {
        this.VENDER_PICTURE = VENDER_PICTURE;
    }

    public void setVENDER_HEADER(String VENDER_HEADER) {
        this.VENDER_HEADER = VENDER_HEADER;
    }

    public void setVENDER_PATH(String VENDER_PATH) {
        this.VENDER_PATH = VENDER_PATH;
    }

    public void setPRIVILEGE_GROUP_ID(float PRIVILEGE_GROUP_ID) {
        this.PRIVILEGE_GROUP_ID = PRIVILEGE_GROUP_ID;
    }

    public void setVENDER_CODE(String VENDER_CODE) {
        this.VENDER_CODE = VENDER_CODE;
    }

    public void setVENDER_NAME_TH(String VENDER_NAME_TH) {
        this.VENDER_NAME_TH = VENDER_NAME_TH;
    }

    public void setVENDER_NAME_EN(String VENDER_NAME_EN) {
        this.VENDER_NAME_EN = VENDER_NAME_EN;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public void setVENDER_ADDR(String VENDER_ADDR) {
        this.VENDER_ADDR = VENDER_ADDR;
    }

    public void setSUB_DISTRICT_ID(String SUB_DISTRICT_ID) {
        this.SUB_DISTRICT_ID = SUB_DISTRICT_ID;
    }

    public void setDISTRICT_ID(String DISTRICT_ID) {
        this.DISTRICT_ID = DISTRICT_ID;
    }

    public void setPROVINCE_ID(float PROVINCE_ID) {
        this.PROVINCE_ID = PROVINCE_ID;
    }

    public void setVENDER_ZIPCODE(String VENDER_ZIPCODE) {
        this.VENDER_ZIPCODE = VENDER_ZIPCODE;
    }

    public void setVENDER_PHONE(String VENDER_PHONE) {
        this.VENDER_PHONE = VENDER_PHONE;
    }

    public void setVENDER_FAX(String VENDER_FAX) {
        this.VENDER_FAX = VENDER_FAX;
    }

    public void setVENDER_WEBSITE(String VENDER_WEBSITE) {
        this.VENDER_WEBSITE = VENDER_WEBSITE;
    }

    public void setVENDER_MAP(String VENDER_MAP) {
        this.VENDER_MAP = VENDER_MAP;
    }

    public void setVENDER_SERVICE_POINT(float VENDER_SERVICE_POINT) {
        this.VENDER_SERVICE_POINT = VENDER_SERVICE_POINT;
    }

    public void setVENDER_SERVICE_TIMES(float VENDER_SERVICE_TIMES) {
        this.VENDER_SERVICE_TIMES = VENDER_SERVICE_TIMES;
    }

    public void setRECORD_STATUS(String RECORD_STATUS) {
        this.RECORD_STATUS = RECORD_STATUS;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public void setCREATE_USER_TYPE(String CREATE_USER_TYPE) {
        this.CREATE_USER_TYPE = CREATE_USER_TYPE;
    }

    public void setLAST_DATE(String LAST_DATE) {
        this.LAST_DATE = LAST_DATE;
    }

    public void setLAST_USER(String LAST_USER) {
        this.LAST_USER = LAST_USER;
    }

    public void setLAST_USER_TYPE(String LAST_USER_TYPE) {
        this.LAST_USER_TYPE = LAST_USER_TYPE;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public void setCONTACT_NAME(String CONTACT_NAME) {
        this.CONTACT_NAME = CONTACT_NAME;
    }

    public void setVENDER_RESERVATION_POLICY_DAY(String VENDER_RESERVATION_POLICY_DAY) {
        this.VENDER_RESERVATION_POLICY_DAY = VENDER_RESERVATION_POLICY_DAY;
    }

    public void setVENDER_RESERVATION_POLICY_HOUR(float VENDER_RESERVATION_POLICY_HOUR) {
        this.VENDER_RESERVATION_POLICY_HOUR = VENDER_RESERVATION_POLICY_HOUR;
    }

    public void setVENDER_CANCELLATION_POLICY_DAY(String VENDER_CANCELLATION_POLICY_DAY) {
        this.VENDER_CANCELLATION_POLICY_DAY = VENDER_CANCELLATION_POLICY_DAY;
    }

    public void setVENDER_CANCELLATION_POLICY_HOUR(float VENDER_CANCELLATION_POLICY_HOUR) {
        this.VENDER_CANCELLATION_POLICY_HOUR = VENDER_CANCELLATION_POLICY_HOUR;
    }

    public void setVENDER_CONTACT_RESERVATION_NAME(String VENDER_CONTACT_RESERVATION_NAME) {
        this.VENDER_CONTACT_RESERVATION_NAME = VENDER_CONTACT_RESERVATION_NAME;
    }

    public void setVENDER_CONTACT_RESERVATION_PHONE(String VENDER_CONTACT_RESERVATION_PHONE) {
        this.VENDER_CONTACT_RESERVATION_PHONE = VENDER_CONTACT_RESERVATION_PHONE;
    }

    public void setVENDER_CONTACT_RESERVATION_FAX(String VENDER_CONTACT_RESERVATION_FAX) {
        this.VENDER_CONTACT_RESERVATION_FAX = VENDER_CONTACT_RESERVATION_FAX;
    }

    public void setVENDER_CONTACT_RESERVATION_MOBILE(String VENDER_CONTACT_RESERVATION_MOBILE) {
        this.VENDER_CONTACT_RESERVATION_MOBILE = VENDER_CONTACT_RESERVATION_MOBILE;
    }

    public void setVENDER_CONTACT_RESERVATION_EMAIL(String VENDER_CONTACT_RESERVATION_EMAIL) {
        this.VENDER_CONTACT_RESERVATION_EMAIL = VENDER_CONTACT_RESERVATION_EMAIL;
    }

    public void setVENDER_CONTACT_SETTLEMENT_NAME(String VENDER_CONTACT_SETTLEMENT_NAME) {
        this.VENDER_CONTACT_SETTLEMENT_NAME = VENDER_CONTACT_SETTLEMENT_NAME;
    }

    public void setVENDER_CONTACT_SETTLEMENT_PHONE(String VENDER_CONTACT_SETTLEMENT_PHONE) {
        this.VENDER_CONTACT_SETTLEMENT_PHONE = VENDER_CONTACT_SETTLEMENT_PHONE;
    }

    public void setVENDER_CONTACT_SETTLEMENT_FAX(String VENDER_CONTACT_SETTLEMENT_FAX) {
        this.VENDER_CONTACT_SETTLEMENT_FAX = VENDER_CONTACT_SETTLEMENT_FAX;
    }

    public void setVENDER_CONTACT_SETTLEMENT_MOBILE(String VENDER_CONTACT_SETTLEMENT_MOBILE) {
        this.VENDER_CONTACT_SETTLEMENT_MOBILE = VENDER_CONTACT_SETTLEMENT_MOBILE;
    }

    public void setVENDER_CONTACT_SETTLEMENT_EMAIL(String VENDER_CONTACT_SETTLEMENT_EMAIL) {
        this.VENDER_CONTACT_SETTLEMENT_EMAIL = VENDER_CONTACT_SETTLEMENT_EMAIL;
    }

    public void setVENDER_CONTRACT_START_DATE(String VENDER_CONTRACT_START_DATE) {
        this.VENDER_CONTRACT_START_DATE = VENDER_CONTRACT_START_DATE;
    }

    public void setVENDER_CONTRACT_END_DATE(String VENDER_CONTRACT_END_DATE) {
        this.VENDER_CONTRACT_END_DATE = VENDER_CONTRACT_END_DATE;
    }

    public void setVENDER_PRICE(String VENDER_PRICE) {
        this.VENDER_PRICE = VENDER_PRICE;
    }

    public void setPAYMENT_ID(float PAYMENT_ID) {
        this.PAYMENT_ID = PAYMENT_ID;
    }

    public void setVENDER_STATUS(String VENDER_STATUS) {
        this.VENDER_STATUS = VENDER_STATUS;
    }

    public void setCOMPANY_ID(float COMPANY_ID) {
        this.COMPANY_ID = COMPANY_ID;
    }

    public void setPOLICY(String POLICY) {
        this.POLICY = POLICY;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public void setCONTACT_BY(String CONTACT_BY) {
        this.CONTACT_BY = CONTACT_BY;
    }

    public void setTAX_ID(String TAX_ID) {
        this.TAX_ID = TAX_ID;
    }

    public void setVENDER_ADDR_TAX(String VENDER_ADDR_TAX) {
        this.VENDER_ADDR_TAX = VENDER_ADDR_TAX;
    }

    public void setSUB_DISTRICT_TAX_ID(String SUB_DISTRICT_TAX_ID) {
        this.SUB_DISTRICT_TAX_ID = SUB_DISTRICT_TAX_ID;
    }

    public void setDISTRICT_TAX_ID(String DISTRICT_TAX_ID) {
        this.DISTRICT_TAX_ID = DISTRICT_TAX_ID;
    }

    public void setPROVINCE_TAX_ID(String PROVINCE_TAX_ID) {
        this.PROVINCE_TAX_ID = PROVINCE_TAX_ID;
    }

    public void setVENDER_TAX_ZIPCODE(String VENDER_TAX_ZIPCODE) {
        this.VENDER_TAX_ZIPCODE = VENDER_TAX_ZIPCODE;
    }

    public void setVENDER_TAX_PHONE(String VENDER_TAX_PHONE) {
        this.VENDER_TAX_PHONE = VENDER_TAX_PHONE;
    }

    public void setVENDER_TAX_FAX(String VENDER_TAX_FAX) {
        this.VENDER_TAX_FAX = VENDER_TAX_FAX;
    }

    public void setATTACH_FILE1(String ATTACH_FILE1) {
        this.ATTACH_FILE1 = ATTACH_FILE1;
    }

    public void setATTACH_FILE2(String ATTACH_FILE2) {
        this.ATTACH_FILE2 = ATTACH_FILE2;
    }

    public void setATTACH_FILE3(String ATTACH_FILE3) {
        this.ATTACH_FILE3 = ATTACH_FILE3;
    }

    public void setVENDOR_STATUS(String VENDOR_STATUS) {
        this.VENDOR_STATUS = VENDOR_STATUS;
    }

    public void setSERVICE_TYPE_CODE_old(String SERVICE_TYPE_CODE_old) {
        this.SERVICE_TYPE_CODE_old = SERVICE_TYPE_CODE_old;
    }

    public void setVENDOR_ID_old(String VENDOR_ID_old) {
        this.VENDOR_ID_old = VENDOR_ID_old;
    }

    public void setVENDER_ACTIVE_STATUS(String VENDER_ACTIVE_STATUS) {
        this.VENDER_ACTIVE_STATUS = VENDER_ACTIVE_STATUS;
    }

    public void setPROVINCE_CODE(String PROVINCE_CODE) {
        this.PROVINCE_CODE = PROVINCE_CODE;
    }

    public void setREMARK_VENDER_EXPIRE(String REMARK_VENDER_EXPIRE) {
        this.REMARK_VENDER_EXPIRE = REMARK_VENDER_EXPIRE;
    }

    public void setACTIVE_NEW(String ACTIVE_NEW) {
        this.ACTIVE_NEW = ACTIVE_NEW;
    }

    public void setPROVINCE_ID_new(String PROVINCE_ID_new) {
        this.PROVINCE_ID_new = PROVINCE_ID_new;
    }

    public void setVENDER_PRICE_DETAIL(String VENDER_PRICE_DETAIL) {
        this.VENDER_PRICE_DETAIL = VENDER_PRICE_DETAIL;
    }

    public void setREF_ID(String REF_ID) {
        this.REF_ID = REF_ID;
    }

    public void setEXPIRY_DATE_BACK(String EXPIRY_DATE_BACK) {
        this.EXPIRY_DATE_BACK = EXPIRY_DATE_BACK;
    }

    public void setPROVINCE_ID_new1(String PROVINCE_ID_new1) {
        this.PROVINCE_ID_new1 = PROVINCE_ID_new1;
    }

    public void setVENDER_BENEFIT(String VENDER_BENEFIT) {
        this.VENDER_BENEFIT = VENDER_BENEFIT;
    }

    public void setVENDER_OTHER_FILE(String VENDER_OTHER_FILE) {
        this.VENDER_OTHER_FILE = VENDER_OTHER_FILE;
    }

    public void setVENDER_COPY_ID(String VENDER_COPY_ID) {
        this.VENDER_COPY_ID = VENDER_COPY_ID;
    }
}
