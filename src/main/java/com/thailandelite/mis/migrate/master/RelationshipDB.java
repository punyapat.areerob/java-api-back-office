package com.thailandelite.mis.migrate.master;

import lombok.Data;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RELATIONSHIP_ID",
    "RELATIONSHIP_CODE",
    "RELATIONSHIP_NAME_TH",
    "RELATIONSHIP_NAME_EN",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
@Generated("jsonschema2pojo")
public class RelationshipDB {

    @JsonProperty("RELATIONSHIP_ID")
    private Integer relationshipId;
    @JsonProperty("RELATIONSHIP_CODE")
    private String relationshipCode;
    @JsonProperty("RELATIONSHIP_NAME_TH")
    private String relationshipNameTh;
    @JsonProperty("RELATIONSHIP_NAME_EN")
    private String relationshipNameEn;
    @JsonProperty("REMARK")
    private String remark;
    @JsonProperty("RECORD_STATUS")
    private String recordStatus;
    @JsonProperty("CREATE_DATE")
    private String createDate;
    @JsonProperty("CREATE_USER")
    private String createUser;
    @JsonProperty("CREATE_USER_TYPE")
    private String createUserType;
    @JsonProperty("LAST_DATE")
    private String lastDate;
    @JsonProperty("LAST_USER")
    private String lastUser;
    @JsonProperty("LAST_USER_TYPE")
    private String lastUserType;

    @JsonProperty("RELATIONSHIP_ID")
    public Integer getRelationshipId() {
        return relationshipId;
    }

    @JsonProperty("RELATIONSHIP_ID")
    public void setRelationshipId(Integer relationshipId) {
        this.relationshipId = relationshipId;
    }

    @JsonProperty("RELATIONSHIP_CODE")
    public String getRelationshipCode() {
        return relationshipCode;
    }

    @JsonProperty("RELATIONSHIP_CODE")
    public void setRelationshipCode(String relationshipCode) {
        this.relationshipCode = relationshipCode;
    }

    @JsonProperty("RELATIONSHIP_NAME_TH")
    public String getRelationshipNameTh() {
        return relationshipNameTh;
    }

    @JsonProperty("RELATIONSHIP_NAME_TH")
    public void setRelationshipNameTh(String relationshipNameTh) {
        this.relationshipNameTh = relationshipNameTh;
    }

    @JsonProperty("RELATIONSHIP_NAME_EN")
    public String getRelationshipNameEn() {
        return relationshipNameEn;
    }

    @JsonProperty("RELATIONSHIP_NAME_EN")
    public void setRelationshipNameEn(String relationshipNameEn) {
        this.relationshipNameEn = relationshipNameEn;
    }

    @JsonProperty("REMARK")
    public String getRemark() {
        return remark;
    }

    @JsonProperty("REMARK")
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @JsonProperty("RECORD_STATUS")
    public String getRecordStatus() {
        return recordStatus;
    }

    @JsonProperty("RECORD_STATUS")
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    @JsonProperty("CREATE_DATE")
    public String getCreateDate() {
        return createDate;
    }

    @JsonProperty("CREATE_DATE")
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @JsonProperty("CREATE_USER")
    public String getCreateUser() {
        return createUser;
    }

    @JsonProperty("CREATE_USER")
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @JsonProperty("CREATE_USER_TYPE")
    public String getCreateUserType() {
        return createUserType;
    }

    @JsonProperty("CREATE_USER_TYPE")
    public void setCreateUserType(String createUserType) {
        this.createUserType = createUserType;
    }

    @JsonProperty("LAST_DATE")
    public String getLastDate() {
        return lastDate;
    }

    @JsonProperty("LAST_DATE")
    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    @JsonProperty("LAST_USER")
    public String getLastUser() {
        return lastUser;
    }

    @JsonProperty("LAST_USER")
    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }

    @JsonProperty("LAST_USER_TYPE")
    public String getLastUserType() {
        return lastUserType;
    }

    @JsonProperty("LAST_USER_TYPE")
    public void setLastUserType(String lastUserType) {
        this.lastUserType = lastUserType;
    }

}
