package com.thailandelite.mis.migrate.master;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RELIGION_ID",
    "RELIGION_CODE",
    "RELIGION_NAME_TH",
    "RELIGION_NAME_EN",
    "PUBLICE_STATUS",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
@Data
public class ReligionDB {

    @JsonProperty("RELIGION_ID")
    public Integer rELIGIONID;
    @JsonProperty("RELIGION_CODE")
    public String rELIGIONCODE;
    @JsonProperty("RELIGION_NAME_TH")
    public Object rELIGIONNAMETH;
    @JsonProperty("RELIGION_NAME_EN")
    public String rELIGIONNAMEEN;
    @JsonProperty("PUBLICE_STATUS")
    public String pUBLICESTATUS;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public Object lASTDATE;
    @JsonProperty("LAST_USER")
    public Object lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public Object lASTUSERTYPE;

}
