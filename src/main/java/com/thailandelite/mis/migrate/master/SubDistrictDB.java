package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SUB_DISTRICT_ID",
    "SUB_DISTRICT_NAME_TH",
    "SUB_DISTRICT_NAME_EN",
    "DISTRICT_ID",
    "PROVINCE_ID",
    "FULL_ID"
})
public class SubDistrictDB {

    @JsonProperty("SUB_DISTRICT_ID")
    public String sUBDISTRICTID;
    @JsonProperty("SUB_DISTRICT_NAME_TH")
    public String sUBDISTRICTNAMETH;
    @JsonProperty("SUB_DISTRICT_NAME_EN")
    public String sUBDISTRICTNAMEEN;
    @JsonProperty("DISTRICT_ID")
    public String dISTRICTID;
    @JsonProperty("PROVINCE_ID")
    public String pROVINCEID;
    @JsonProperty("FULL_ID")
    public String fULLID;

}
