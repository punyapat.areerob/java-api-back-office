package com.thailandelite.mis.migrate.master;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "STATE_ID",
    "COUNTRY_ID",
    "STATE_CODE",
    "STATE_CODE_old",
    "STATE_NAME_TH",
    "STATE_NAME_EN",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE",
    "PROVINCE_ID",
    "COUNTRY_CODE",
    "STATE_ID_old"
})
@Generated("jsonschema2pojo")
public class StateDB {

    @JsonProperty("STATE_ID")
    private Integer stateId;
    @JsonProperty("COUNTRY_ID")
    private Integer countryId;
    @JsonProperty("STATE_CODE")
    private String stateCode;
    @JsonProperty("STATE_CODE_old")
    private String sTATECODEOld;
    @JsonProperty("STATE_NAME_TH")
    private String stateNameTh;
    @JsonProperty("STATE_NAME_EN")
    private String stateNameEn;
    @JsonProperty("REMARK")
    private Object remark;
    @JsonProperty("RECORD_STATUS")
    private String recordStatus;
    @JsonProperty("CREATE_DATE")
    private Object createDate;
    @JsonProperty("CREATE_USER")
    private Object createUser;
    @JsonProperty("CREATE_USER_TYPE")
    private Object createUserType;
    @JsonProperty("LAST_DATE")
    private Object lastDate;
    @JsonProperty("LAST_USER")
    private Object lastUser;
    @JsonProperty("LAST_USER_TYPE")
    private Object lastUserType;
    @JsonProperty("PROVINCE_ID")
    private Object provinceId;
    @JsonProperty("COUNTRY_CODE")
    private String countryCode;
    @JsonProperty("STATE_ID_old")
    private Object sTATEIDOld;

    @JsonProperty("STATE_ID")
    public Integer getStateId() {
        return stateId;
    }

    @JsonProperty("STATE_ID")
    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    @JsonProperty("COUNTRY_ID")
    public Integer getCountryId() {
        return countryId;
    }

    @JsonProperty("COUNTRY_ID")
    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    @JsonProperty("STATE_CODE")
    public String getStateCode() {
        return stateCode;
    }

    @JsonProperty("STATE_CODE")
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @JsonProperty("STATE_CODE_old")
    public String getSTATECODEOld() {
        return sTATECODEOld;
    }

    @JsonProperty("STATE_CODE_old")
    public void setSTATECODEOld(String sTATECODEOld) {
        this.sTATECODEOld = sTATECODEOld;
    }

    @JsonProperty("STATE_NAME_TH")
    public String getStateNameTh() {
        return stateNameTh;
    }

    @JsonProperty("STATE_NAME_TH")
    public void setStateNameTh(String stateNameTh) {
        this.stateNameTh = stateNameTh;
    }

    @JsonProperty("STATE_NAME_EN")
    public String getStateNameEn() {
        return stateNameEn;
    }

    @JsonProperty("STATE_NAME_EN")
    public void setStateNameEn(String stateNameEn) {
        this.stateNameEn = stateNameEn;
    }

    @JsonProperty("REMARK")
    public Object getRemark() {
        return remark;
    }

    @JsonProperty("REMARK")
    public void setRemark(Object remark) {
        this.remark = remark;
    }

    @JsonProperty("RECORD_STATUS")
    public String getRecordStatus() {
        return recordStatus;
    }

    @JsonProperty("RECORD_STATUS")
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    @JsonProperty("CREATE_DATE")
    public Object getCreateDate() {
        return createDate;
    }

    @JsonProperty("CREATE_DATE")
    public void setCreateDate(Object createDate) {
        this.createDate = createDate;
    }

    @JsonProperty("CREATE_USER")
    public Object getCreateUser() {
        return createUser;
    }

    @JsonProperty("CREATE_USER")
    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    @JsonProperty("CREATE_USER_TYPE")
    public Object getCreateUserType() {
        return createUserType;
    }

    @JsonProperty("CREATE_USER_TYPE")
    public void setCreateUserType(Object createUserType) {
        this.createUserType = createUserType;
    }

    @JsonProperty("LAST_DATE")
    public Object getLastDate() {
        return lastDate;
    }

    @JsonProperty("LAST_DATE")
    public void setLastDate(Object lastDate) {
        this.lastDate = lastDate;
    }

    @JsonProperty("LAST_USER")
    public Object getLastUser() {
        return lastUser;
    }

    @JsonProperty("LAST_USER")
    public void setLastUser(Object lastUser) {
        this.lastUser = lastUser;
    }

    @JsonProperty("LAST_USER_TYPE")
    public Object getLastUserType() {
        return lastUserType;
    }

    @JsonProperty("LAST_USER_TYPE")
    public void setLastUserType(Object lastUserType) {
        this.lastUserType = lastUserType;
    }

    @JsonProperty("PROVINCE_ID")
    public Object getProvinceId() {
        return provinceId;
    }

    @JsonProperty("PROVINCE_ID")
    public void setProvinceId(Object provinceId) {
        this.provinceId = provinceId;
    }

    @JsonProperty("COUNTRY_CODE")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("COUNTRY_CODE")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("STATE_ID_old")
    public Object getSTATEIDOld() {
        return sTATEIDOld;
    }

    @JsonProperty("STATE_ID_old")
    public void setSTATEIDOld(Object sTATEIDOld) {
        this.sTATEIDOld = sTATEIDOld;
    }

}
