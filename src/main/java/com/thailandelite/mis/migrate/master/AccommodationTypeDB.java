package com.thailandelite.mis.migrate.master;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ADDRESS_TYPE_ID",
    "ADDRESS_TYPE_CODE",
    "ADDRESS_TYPE_NAME_TH",
    "ADDRESS_TYPE_NAME_EN",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
public class AccommodationTypeDB {

    @JsonProperty("ADDRESS_TYPE_ID")
    public Integer aDDRESSTYPEID;
    @JsonProperty("ADDRESS_TYPE_CODE")
    public String aDDRESSTYPECODE;
    @JsonProperty("ADDRESS_TYPE_NAME_TH")
    public String aDDRESSTYPENAMETH;
    @JsonProperty("ADDRESS_TYPE_NAME_EN")
    public String aDDRESSTYPENAMEEN;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public String cREATEDATE;
    @JsonProperty("CREATE_USER")
    public String cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public String cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public String lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;

}
