package com.thailandelite.mis.migrate.master;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "DISTRICT_ID",
    "DISTRICT_NAME_TH",
    "DISTRICT_NAME_EN",
    "PROVINCE_ID",
    "FULL_ID"
})
public class DistrictDB {

    @JsonProperty("DISTRICT_ID")
    public String dISTRICTID;
    @JsonProperty("DISTRICT_NAME_TH")
    public String dISTRICTNAMETH;
    @JsonProperty("DISTRICT_NAME_EN")
    public String dISTRICTNAMEEN;
    @JsonProperty("PROVINCE_ID")
    public String pROVINCEID;
    @JsonProperty("FULL_ID")
    public String fULLID;

}
