package com.thailandelite.mis.migrate.master;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "BLOOD_TYPE_ID",
    "BLOOD_TYPE_CODE",
    "BLOOD_TYPE_NAME",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
@Data
public class BloodTypeDB {

    @JsonProperty("BLOOD_TYPE_ID")
    public Integer bLOODTYPEID;
    @JsonProperty("BLOOD_TYPE_CODE")
    public String bLOODTYPECODE;
    @JsonProperty("BLOOD_TYPE_NAME")
    public String bLOODTYPENAME;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public Object lASTDATE;
    @JsonProperty("LAST_USER")
    public Object lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public Object lASTUSERTYPE;

}
