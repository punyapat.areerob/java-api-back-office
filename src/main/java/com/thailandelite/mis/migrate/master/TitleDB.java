package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TITLE_ID",
    "TITLE_CODE",
    "TITLE_NAME_TH",
    "TITLE_NAME_EN",
    "PUBLICE_STATUS",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
public class TitleDB {
    @JsonProperty("TITLE_ID")
    public Integer tITLEID;
    @JsonProperty("TITLE_CODE")
    public String tITLECODE;
    @JsonProperty("TITLE_NAME_TH")
    public String tITLENAMETH;
    @JsonProperty("TITLE_NAME_EN")
    public String tITLENAMEEN;
    @JsonProperty("PUBLICE_STATUS")
    public String pUBLICESTATUS;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public String lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;

}
