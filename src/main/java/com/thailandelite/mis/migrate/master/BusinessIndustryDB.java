package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NATURE_OF_BUSINESS_ID",
    "NATURE_OF_BUSINESS_CODE",
    "NATURE_OF_BUSINESS_NAME_TH",
    "NATURE_OF_BUSINESS_NAME_EN",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
public class BusinessIndustryDB {

    @JsonProperty("NATURE_OF_BUSINESS_ID")
    public Integer nATUREOFBUSINESSID;
    @JsonProperty("NATURE_OF_BUSINESS_CODE")
    public String nATUREOFBUSINESSCODE;
    @JsonProperty("NATURE_OF_BUSINESS_NAME_TH")
    public String nATUREOFBUSINESSNAMETH;
    @JsonProperty("NATURE_OF_BUSINESS_NAME_EN")
    public String nATUREOFBUSINESSNAMEEN;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public String cREATEDATE;
    @JsonProperty("CREATE_USER")
    public String cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public String cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public String lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;

}
