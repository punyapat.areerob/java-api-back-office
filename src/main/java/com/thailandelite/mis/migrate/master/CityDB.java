package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CITY_ID",
    "STATE_ID",
    "COUNTRY_ID",
    "CITY_CODE",
    "CITY_NAME_TH",
    "CITY_NAME_EN",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE",
    "PROVINCE_ID",
    "DISTRICT_ID",
    "STATE_CODE_old",
    "COUNTRY_CODE",
    "CITY_ID_old"
})
public class CityDB {

    @JsonProperty("CITY_ID")
    public Integer cITYID;
    @JsonProperty("STATE_ID")
    public Integer sTATEID;
    @JsonProperty("COUNTRY_ID")
    public Integer cOUNTRYID;
    @JsonProperty("CITY_CODE")
    public String cITYCODE;
    @JsonProperty("CITY_NAME_TH")
    public String cITYNAMETH;
    @JsonProperty("CITY_NAME_EN")
    public String cITYNAMEEN;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public Object lASTDATE;
    @JsonProperty("LAST_USER")
    public Object lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public Object lASTUSERTYPE;
    @JsonProperty("PROVINCE_ID")
    public Object pROVINCEID;
    @JsonProperty("DISTRICT_ID")
    public Object dISTRICTID;
    @JsonProperty("STATE_CODE_old")
    public String sTATECODEOld;
    @JsonProperty("COUNTRY_CODE")
    public String cOUNTRYCODE;
    @JsonProperty("CITY_ID_old")
    public Object cITYIDOld;

}
