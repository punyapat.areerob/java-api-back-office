package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "COUNTRY_ID",
    "COUNTRY_CODE",
    "COUNTRY_NAME_TH",
    "COUNTRY_NAME_EN",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE",
    "COUNTRY_CODE_old"
})
public class CountryDB {

    @JsonProperty("COUNTRY_ID")
    public Integer cOUNTRYID;
    @JsonProperty("COUNTRY_CODE")
    public String cOUNTRYCODE;
    @JsonProperty("COUNTRY_NAME_TH")
    public String cOUNTRYNAMETH;
    @JsonProperty("COUNTRY_NAME_EN")
    public String cOUNTRYNAMEEN;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public String lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;
    @JsonProperty("COUNTRY_CODE_old")
    public Object cOUNTRYCODEOld;

}
