package com.thailandelite.mis.migrate.master;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NATIONALITY_ID",
    "NATIONALITY_CODE",
    "NATIONALITY_NAME_TH",
    "NATIONALITY_NAME_EN",
    "COUNTRY_CODE",
    "COUNTRY_NAME",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE",
    "NATIONALITY_PASSPORT_NAME_TH",
    "NATIONALITY_PASSPORT_NAME_EN"
})
public class NationalitiesDB {

    @JsonProperty("NATIONALITY_ID")
    public Integer nATIONALITYID;
    @JsonProperty("NATIONALITY_CODE")
    public String nATIONALITYCODE;
    @JsonProperty("NATIONALITY_NAME_TH")
    public String nATIONALITYNAMETH;
    @JsonProperty("NATIONALITY_NAME_EN")
    public String nATIONALITYNAMEEN;
    @JsonProperty("COUNTRY_CODE")
    public String cOUNTRYCODE;
    @JsonProperty("COUNTRY_NAME")
    public String cOUNTRYNAME;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public String lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;
    @JsonProperty("NATIONALITY_PASSPORT_NAME_TH")
    public String nATIONALITYPASSPORTNAMETH;
    @JsonProperty("NATIONALITY_PASSPORT_NAME_EN")
    public String nATIONALITYPASSPORTNAMEEN;

}
