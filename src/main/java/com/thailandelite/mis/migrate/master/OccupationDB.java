package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "OCCUPATION_ID",
    "OCCUPATION_CODE",
    "OCCUPATION_NAME_TH",
    "OCCUPATION_NAME_EN",
    "PUBLICE_STATUS",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE"
})
public class OccupationDB {

    @JsonProperty("OCCUPATION_ID")
    public Integer oCCUPATIONID;
    @JsonProperty("OCCUPATION_CODE")
    public String oCCUPATIONCODE;
    @JsonProperty("OCCUPATION_NAME_TH")
    public String oCCUPATIONNAMETH;
    @JsonProperty("OCCUPATION_NAME_EN")
    public String oCCUPATIONNAMEEN;
    @JsonProperty("PUBLICE_STATUS")
    public String pUBLICESTATUS;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public String lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;

}
