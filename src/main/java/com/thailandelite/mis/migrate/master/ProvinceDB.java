package com.thailandelite.mis.migrate.master;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PROVINCE_ID",
    "COUNTRY_ID",
    "PROVINCE_CODE",
    "PROVINCE_NAME_TH",
    "PROVINCE_NAME_EN_bk",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE",
    "COUNTRY_CODE_old",
    "PROVINCE_NAME_EN"
})
public class ProvinceDB {

    @JsonProperty("PROVINCE_ID")
    public Integer pROVINCEID;
    @JsonProperty("COUNTRY_ID")
    public Integer cOUNTRYID;
    @JsonProperty("PROVINCE_CODE")
    public String pROVINCECODE;
    @JsonProperty("PROVINCE_NAME_TH")
    public String pROVINCENAMETH;
    @JsonProperty("PROVINCE_NAME_EN_bk")
    public String pROVINCENAMEENBk;
    @JsonProperty("REMARK")
    public Object rEMARK;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Object cREATEDATE;
    @JsonProperty("CREATE_USER")
    public Object cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public Object cREATEUSERTYPE;
    @JsonProperty("LAST_DATE")
    public Object lASTDATE;
    @JsonProperty("LAST_USER")
    public Object lASTUSER;
    @JsonProperty("LAST_USER_TYPE")
    public Object lASTUSERTYPE;
    @JsonProperty("COUNTRY_CODE_old")
    public String cOUNTRYCODEOld;
    @JsonProperty("PROVINCE_NAME_EN")
    public String pROVINCENAMEEN;

}
