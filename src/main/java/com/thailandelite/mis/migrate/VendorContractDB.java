package com.thailandelite.mis.migrate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "VENDER_CONTRACT_ID" ,
    "VENDER_ID" ,
    "VENDER_CONTRACT_START_DATE" ,
    "VENDER_CONTRACT_END_DATE" ,
    "VENDER_CONTRACT_FILE" ,
    "VENDER_CONTRACT_PATH" ,
    "VENDER_CONTRACT_STATUS" ,
    "RECORD_STATUS" ,
    "CREATE_DATE" ,
    "CREATE_USER" ,
    "CREATE_USER_TYPE" ,
    "LAST_DATE" ,
    "LAST_USER" ,
    "LAST_USER_TYPE" ,
})
@Data
public class VendorContractDB {

    @JsonProperty("VENDER_CONTRACT_ID")
    public Integer VenderContractId;
    @JsonProperty("VENDER_ID")
    public Integer VenderId;
    @JsonProperty("VENDER_CONTRACT_START_DATE")
    public String VenderStartDate;
    @JsonProperty("VENDER_CONTRACT_END_DATE")
    public String VenderEndDate;
    @JsonProperty("VENDER_CONTRACT_FILE")
    public String VenderFile;
    @JsonProperty("VENDER_CONTRACT_PATH")
    public String VenderPath;
    @JsonProperty("VENDER_CONTRACT_STATUS")
    public String VenderStatus;
    @JsonProperty("REMARK")
    public String Remark;
    @JsonProperty("RECORD_STATUS")
    public String RecordStatus;
    @JsonProperty("CREATE_DATE")
    public String CreateDate;
    @JsonProperty("CREATE_USER")
    public String CreateUser;
    @JsonProperty("CREATE_USER_TYPE")
    public String CreateUserType;
    @JsonProperty("LAST_DATE")
    public String LastDate;
    @JsonProperty("LAST_USER")
    public String LastUser;
    @JsonProperty("LAST_USER_TYPE")
    public String LastUserType;
}
