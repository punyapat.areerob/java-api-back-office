package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_CALLCENTER_LOG")
public class CcsCallcenterLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "CALLCENTER_LOG_ID", nullable = false)
    private Integer callcenterLogId;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "EXTENTION")
    private String EXTENTION;

    @Column(name = "BREAK_ID")
    private Integer breakId;

    @Column(name = "LOG_DATE")
    private Date logDate;

    @Column(name = "LOG_TYPE")
    private String logType;

    @Column(name = "SKILL")
    private String SKILL;

    @Column(name = "REMARK")
    private String REMARK;

}
