package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_MEMBER_PHONE")
public class SmmMemberPhone implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PHONE_ID", nullable = false)
    private Integer phoneId;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "PHONE_NO")
    private String phoneNo;

    @Column(name = "PHONE_PRIMARY")
    private String phonePrimary;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PHONE_TYPE_ID")
    private Integer phoneTypeId;

    @Column(name = "PHONE_CONTACT")
    private String phoneContact;

    @Column(name = "REMARK")
    private String REMARK;

}
