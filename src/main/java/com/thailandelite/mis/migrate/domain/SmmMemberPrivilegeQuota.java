package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_MEMBER_PRIVILEGE_QUOTA")
public class SmmMemberPrivilegeQuota implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MEMBER_PRIVILEGE_QUOTA_ID", nullable = false)
    private Integer memberPrivilegeQuotaId;

    @Column(name = "MEMBER_GROUP_ID")
    private Integer memberGroupId;

    @Column(name = "PRIVILEGE_ID")
    private Integer privilegeId;

    @Column(name = "YEAR_QUOTA")
    private String yearQuota;

    @Column(name = "QUOTA_QUANTITY")
    private Integer quotaQuantity;

    @Column(name = "QUOTA_ADJUST")
    private Integer quotaAdjust;

    @Column(name = "QUOTA_USED")
    private Integer quotaUsed;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private ZonedDateTime createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private ZonedDateTime lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "QUOTA_USED_bk")
    private Integer quotaUsedBk;

}
