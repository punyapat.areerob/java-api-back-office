package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_MAP_PROVINCE_EPA_VENDOR")
public class CcsMapProvinceEpaVendor implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MAP_PROVINCE_EPA_VENDOR_ID", nullable = false)
    private Integer mapProvinceEpaVendorId;

    @Column(name = "EPA_PROVINCE_ID")
    private Integer epaProvinceId;

    @Column(name = "VENDOR_PROVINCE_ID")
    private Integer vendorProvinceId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "EPA_PROVINCE_ID_new")
    private Integer epaProvinceIdNew;

    @Column(name = "VENDOR_PROVINCE_ID_new")
    private Integer vendorProvinceIdNew;

    @Column(name = "REMARK")
    private String REMARK;

}
