package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_MEMBERSHIP_TRANSFER")
public class MasMembershipTransfer implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MEMBERSHIP_TRANSFER_ID", nullable = false)
    private Integer membershipTransferId;

    @Column(name = "MEMBERSHIP_TRANSFER_CODE")
    private String membershipTransferCode;

    @Column(name = "MEMBERSHIP_TRANSFER_LABEL")
    private String membershipTransferLabel;

    @Column(name = "MEMBERSHIP_TRANSFER_TIME")
    private Integer membershipTransferTime;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
