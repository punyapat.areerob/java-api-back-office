package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_JOB_ASSIGNMENT_EMAIL")
public class CcsJobAssignmentEmail implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "JOB_ASSIGNMENT_EMAIL_ID", nullable = false)
    private Integer jobAssignmentEmailId;

    @Column(name = "JOB_ASSIGNMENT_ID")
    private Integer jobAssignmentId;

    @Column(name = "EMAIL_SENDER")
    private String emailSender;

    @Column(name = "EMAIL_RECEIVER")
    private String emailReceiver;

    @Column(name = "DATETIME_SEND")
    private Date datetimeSend;

    @Column(name = "SEND_STATUS")
    private String sendStatus;

    @Column(name = "EMAIL_SUBJECT")
    private String emailSubject;

    @Column(name = "EMAIL_STATUS")
    private String emailStatus;

    @Column(name = "EMAIL_CC")
    private String emailCc;

    @Column(name = "EMAIL_TYPE")
    private String emailType;

    @Column(name = "SELECT_ID")
    private String selectId;

    @Column(name = "PARENT_ID")
    private String parentId;

    @Column(name = "JOB_ASSIGNMENT_CODE")
    private String jobAssignmentCode;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "FILE_CONTENT_TYPE")
    private String fileContentType;

    @Column(name = "FILE_PATH_SUB")
    private String filePathSub;

    @Column(name = "FILE_OTHER_CONTENT_TYPE")
    private String fileOtherContentType;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "EMAIL_BODY")
    private String emailBody;

    @Column(name = "EMAIL_STATUS_DETAIL")
    private String emailStatusDetail;

    @Column(name = "FILE_BASE64")
    private String fileBase64;

    @Column(name = "FILE_OTHER_BASE64")
    private String fileOtherBase64;

    @Column(name = "REMARK")
    private String REMARK;

}
