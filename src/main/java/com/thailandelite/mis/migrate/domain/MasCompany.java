package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_COMPANY")
public class MasCompany implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "COMPANY_ID", nullable = false)
    private Integer companyId;

    @Column(name = "COMPANY_NAME_TH")
    private String companyNameTh;

    @Column(name = "COMPANY_NAME_EN")
    private String companyNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "COMPANY_ADDR")
    private String companyAddr;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "PROVINCE_COMPANY_ID")
    private Integer provinceCompanyId;

    @Column(name = "PROVINCE_ID")
    private Integer provinceId;

    @Column(name = "COMPANY_ZIPCODE")
    private String companyZipcode;

    @Column(name = "REGISTRATION_NO")
    private String registrationNo;

    @Column(name = "COMPANY_PHONE")
    private String companyPhone;

    @Column(name = "COMPANY_FAX")
    private String companyFax;

    @Column(name = "TAX_ID")
    private String taxId;

    @Column(name = "COMPANY_TAX_SUB_DISTRICT_ID")
    private Integer companyTaxSubDistrictId;

    @Column(name = "COMPANY_TAX_DISTRICT_ID")
    private Integer companyTaxDistrictId;

    @Column(name = "COMPANY_TAX_PROVINCE_ID")
    private Integer companyTaxProvinceId;

    @Column(name = "COMPANY_TAX_ZIPCODE")
    private String companyTaxZipcode;

    @Column(name = "COMPANY_TAX_PHONE")
    private String companyTaxPhone;

    @Column(name = "COMPANY_TAX_FAX")
    private String companyTaxFax;

    @Column(name = "COMPANY_TAX_COUNTRY_ID")
    private Integer companyTaxCountryId;

    @Column(name = "COMPANY_TAX_STATE_ID")
    private Integer companyTaxStateId;

    @Column(name = "COMPANY_TAX_CITY_ID")
    private Integer companyTaxCityId;

    @Column(name = "AGENT_ID")
    private Integer agentId;

    @Column(name = "BRANCH_NO")
    private String branchNo;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "COMPANY_ADDR_TAX")
    private String companyAddrTax;

}
