package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SLM_AGENT_STATUS")
public class SlmAgentStatus implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "AGENT_STATUS_ID", nullable = false)
    private Integer agentStatusId;

    @Column(name = "AGENT_STATUS_CODE", nullable = false)
    private String agentStatusCode;

    @Column(name = "AGENT_STATUS_NAME_TH")
    private String agentStatusNameTh;

    @Column(name = "AGENT_STATUS_NAME_EN")
    private String agentStatusNameEn;

    @Column(name = "AGENT_STATUS_ORDER", nullable = false)
    private Integer agentStatusOrder;

    @Column(name = "CANCEL_AGENT_STATUS_ID")
    private Integer cancelAgentStatusId;

    @Column(name = "NEXT_AGENT_STATUS_ID")
    private Integer nextAgentStatusId;

    @Column(name = "AGENT_BUTTON_NEXT_STATUS_NAME_TH")
    private String agentButtonNextStatusNameTh;

    @Column(name = "AGENT_BUTTON_NEXT_STATUS_NAME_EN")
    private String agentButtonNextStatusNameEn;

    @Column(name = "AGENT_BUTTON_CANCEL_STATUS_NAME_TH")
    private String agentButtonCancelStatusNameTh;

    @Column(name = "AGENT_BUTTON_CANCEL_STATUS_NAME_EN")
    private String agentButtonCancelStatusNameEn;

    @Column(name = "EDITABLE")
    private String EDITABLE;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

}
