package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@Table(name = "CMM_COMMISSION_ISSUE")
public class CmmCommissionIssue implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "COMMISSION_ISSUE_ID", nullable = false)
    private Integer commissionIssueId;

    @Column(name = "DOC_NO")
    private String docNo;

    @Column(name = "DOC_DATE")
    private Date docDate;

    @Column(name = "AGENT_ID")
    private Integer agentId;

    @Column(name = "ISSUE_FILE")
    private String issueFile;

    @Column(name = "ISSUE_STATUS_CODE")
    private String issueStatusCode;

    @Column(name = "ISSUE_PATH")
    private String issuePath;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "ISSUE_FILE2")
    private String issueFile2;

    @Column(name = "ISSUE_FILE3")
    private String issueFile3;

    @Column(name = "ISSUE_FILE4")
    private String issueFile4;

    @Column(name = "INVOICE_NO")
    private String invoiceNo;

    @Column(name = "INVOICE_DATE")
    private LocalDate invoiceDate;

    @Column(name = "INVOICE_AMOUNT")
    private BigDecimal invoiceAmount;

    @Column(name = "COMMISSION_GROUP_ISSUE_ID")
    private Integer commissionGroupIssueId;

    @Column(name = "ISSUE_STATUS_GROUP_PAYMENT")
    private String issueStatusGroupPayment;

    @Column(name = "OPTION_VAT")
    private String optionVat;

    @Column(name = "REMARK")
    private String REMARK;

}
