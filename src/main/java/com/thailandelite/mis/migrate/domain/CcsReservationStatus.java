package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_RESERVATION_STATUS")
public class CcsReservationStatus implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "RESERVATION_STATUS_ID", nullable = false)
    private Integer reservationStatusId;

    @Column(name = "RESERVATION_STATUS_CODE", nullable = false)
    private String reservationStatusCode;

    @Column(name = "RESERVATION_STATUS_NAME_TH")
    private String reservationStatusNameTh;

    @Column(name = "RESERVATION_STATUS_NAME_EN")
    private String reservationStatusNameEn;

    @Column(name = "RESERVATION_STATUS_ORDER", nullable = false)
    private Integer reservationStatusOrder;

    @Column(name = "CANCEL_RESERVATION_STATUS_ID")
    private Integer cancelReservationStatusId;

    @Column(name = "NEXT_RESERVATION_STATUS_ID")
    private Integer nextReservationStatusId;

    @Column(name = "RESERVATION_BUTTON_NEXT_STATUS_NAME_TH")
    private String reservationButtonNextStatusNameTh;

    @Column(name = "RESERVATION_BUTTON_NEXT_STATUS_NAME_EN")
    private String reservationButtonNextStatusNameEn;

    @Column(name = "RESERVATION_BUTTON_CANCEL_STATUS_NAME_TH")
    private String reservationButtonCancelStatusNameTh;

    @Column(name = "RESERVATION_BUTTON_CANCEL_STATUS_NAME_EN")
    private String reservationButtonCancelStatusNameEn;

    @Column(name = "EDITABLE")
    private String EDITABLE;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

}
