package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_APPLICATION_WELCOME_PACK")
public class SmmApplicationWelcomePack implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "APPLICATION_WELCOME_PACK_ID", nullable = false)
    private Integer applicationWelcomePackId;

    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "WELCOME_PACK_DATE")
    private Date welcomePackDate;

    @Column(name = "WELCOME_PACK_STATUS")
    private String welcomePackStatus;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PLACE")
    private String PLACE;

    @Column(name = "REMARK")
    private String REMARK;

}
