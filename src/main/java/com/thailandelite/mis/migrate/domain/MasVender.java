package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_VENDER")
public class MasVender implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "VENDER_ID", nullable = false)
    private Integer venderId;

    @Column(name = "VENDER_PICTURE")
    private String venderPicture;

    @Column(name = "VENDER_HEADER")
    private String venderHeader;

    @Column(name = "VENDER_PATH")
    private String venderPath;

    @Column(name = "PRIVILEGE_GROUP_ID")
    private Integer privilegeGroupId;

    @Column(name = "VENDER_CODE")
    private String venderCode;

    @Column(name = "VENDER_NAME_TH")
    private String venderNameTh;

    @Column(name = "VENDER_NAME_EN")
    private String venderNameEn;

    @Column(name = "SUB_DISTRICT_ID")
    private Integer subDistrictId;

    @Column(name = "DISTRICT_ID")
    private Integer districtId;

    @Column(name = "PROVINCE_ID")
    private Integer provinceId;

    @Column(name = "VENDER_ZIPCODE")
    private String venderZipcode;

    @Column(name = "VENDER_PHONE")
    private String venderPhone;

    @Column(name = "VENDER_FAX")
    private String venderFax;

    @Column(name = "VENDER_WEBSITE")
    private String venderWebsite;

    @Column(name = "VENDER_MAP")
    private String venderMap;

    @Column(name = "VENDER_SERVICE_POINT")
    private Integer venderServicePoint;

    @Column(name = "VENDER_SERVICE_TIMES")
    private Integer venderServiceTimes;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "EMAIL")
    private String EMAIL;

    @Column(name = "CONTACT_NAME")
    private String contactName;

    @Column(name = "VENDER_RESERVATION_POLICY_DAY")
    private Integer venderReservationPolicyDay;

    @Column(name = "VENDER_RESERVATION_POLICY_HOUR")
    private Integer venderReservationPolicyHour;

    @Column(name = "VENDER_CANCELLATION_POLICY_DAY")
    private Integer venderCancellationPolicyDay;

    @Column(name = "VENDER_CANCELLATION_POLICY_HOUR")
    private Integer venderCancellationPolicyHour;

    @Column(name = "VENDER_CONTACT_RESERVATION_NAME")
    private String venderContactReservationName;

    @Column(name = "VENDER_CONTACT_RESERVATION_PHONE")
    private String venderContactReservationPhone;

    @Column(name = "VENDER_CONTACT_RESERVATION_FAX")
    private String venderContactReservationFax;

    @Column(name = "VENDER_CONTACT_RESERVATION_MOBILE")
    private String venderContactReservationMobile;

    @Column(name = "VENDER_CONTACT_RESERVATION_EMAIL")
    private String venderContactReservationEmail;

    @Column(name = "VENDER_CONTACT_SETTLEMENT_NAME")
    private String venderContactSettlementName;

    @Column(name = "VENDER_CONTACT_SETTLEMENT_PHONE")
    private String venderContactSettlementPhone;

    @Column(name = "VENDER_CONTACT_SETTLEMENT_FAX")
    private String venderContactSettlementFax;

    @Column(name = "VENDER_CONTACT_SETTLEMENT_MOBILE")
    private String venderContactSettlementMobile;

    @Column(name = "VENDER_CONTACT_SETTLEMENT_EMAIL")
    private String venderContactSettlementEmail;

    @Column(name = "VENDER_CONTRACT_START_DATE")
    private Date venderContractStartDate;

    @Column(name = "VENDER_CONTRACT_END_DATE")
    private Date venderContractEndDate;

    @Column(name = "VENDER_PRICE")
    private BigDecimal venderPrice;

    @Column(name = "PAYMENT_ID")
    private Integer paymentId;

    @Column(name = "VENDER_STATUS")
    private String venderStatus;

    @Column(name = "COMPANY_ID")
    private Integer companyId;

    @Column(name = "CONTACT_BY")
    private String contactBy;

    @Column(name = "TAX_ID")
    private String taxId;

    @Column(name = "SUB_DISTRICT_TAX_ID")
    private Integer subDistrictTaxId;

    @Column(name = "DISTRICT_TAX_ID")
    private Integer districtTaxId;

    @Column(name = "PROVINCE_TAX_ID")
    private Integer provinceTaxId;

    @Column(name = "VENDER_TAX_ZIPCODE")
    private String venderTaxZipcode;

    @Column(name = "VENDER_TAX_PHONE")
    private String venderTaxPhone;

    @Column(name = "VENDER_TAX_FAX")
    private String venderTaxFax;

    @Column(name = "ATTACH_FILE1")
    private String attachFile1;

    @Column(name = "ATTACH_FILE2")
    private String attachFile2;

    @Column(name = "ATTACH_FILE3")
    private String attachFile3;

    @Column(name = "VENDOR_STATUS")
    private String vendorStatus;

    @Column(name = "SERVICE_TYPE_CODE_old")
    private String serviceTypeCodeOld;

    @Column(name = "VENDOR_ID_old")
    private String vendorIdOld;

    @Column(name = "VENDER_ACTIVE_STATUS")
    private String venderActiveStatus;

    @Column(name = "PROVINCE_CODE")
    private String provinceCode;

    @Column(name = "ACTIVE_NEW")
    private String activeNew;

    @Column(name = "PROVINCE_ID_new")
    private Integer provinceIdNew;

    @Column(name = "VENDER_PRICE_DETAIL")
    private String venderPriceDetail;

    @Column(name = "REF_ID")
    private Integer refId;

    @Column(name = "EXPIRY_DATE_BACK")
    private Date expiryDateBack;

    @Column(name = "PROVINCE_ID_new1")
    private Integer provinceIdNew1;

    @Column(name = "VENDER_OTHER_FILE")
    private String venderOtherFile;

    @Column(name = "VENDER_COPY_ID")
    private Integer venderCopyId;

    @Column(name = "DESCRIPTION")
    private String DESCRIPTION;

    @Column(name = "VENDER_ADDR")
    private String venderAddr;

    @Column(name = "POLICY")
    private String POLICY;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "VENDER_ADDR_TAX")
    private String venderAddrTax;

    @Column(name = "REMARK_VENDER_EXPIRE")
    private String remarkVenderExpire;

    @Column(name = "VENDER_BENEFIT")
    private String venderBenefit;

}
