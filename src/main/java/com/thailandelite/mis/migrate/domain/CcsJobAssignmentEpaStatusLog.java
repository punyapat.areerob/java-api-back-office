package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_JOB_ASSIGNMENT_EPA_STATUS_LOG")
public class CcsJobAssignmentEpaStatusLog implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "CCS_JOB_ASSIGNMENT_EPA_STATUS_LOG_ID", nullable = false)
    private Integer ccsJobAssignmentEpaStatusLogId;

    @Column(name = "JOB_ASSIGNMENT_ID", nullable = false)
    private Integer jobAssignmentId;

    @Column(name = "EPA_STATUS")
    private String epaStatus;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
