package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_MEMBER_CARD")
public class SmmMemberCard implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MEMBER_CARD_ID", nullable = false)
    private Integer memberCardId;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "CAUSE_DATE")
    private Date causeDate;

    @Column(name = "MEMBER_CARD_DETAIL")
    private String memberCardDetail;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "MEMBER_CARD_PATH")
    private String memberCardPath;

    @Column(name = "MEMBER_CARD_PICTURE")
    private String memberCardPicture;

    @Column(name = "REMARK")
    private String REMARK;

}
