package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_RELATIONSHIP")
public class MasRelationship implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "RELATIONSHIP_ID", nullable = false)
    private Integer relationshipId;

    @Column(name = "RELATIONSHIP_CODE")
    private String relationshipCode;

    @Column(name = "RELATIONSHIP_NAME_TH")
    private String relationshipNameTh;

    @Column(name = "RELATIONSHIP_NAME_EN")
    private String relationshipNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
