package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_VENDER_CONTACT")
public class MasVenderContact implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "VENDER_CONTACT_ID", nullable = false)
    private Integer venderContactId;

    @Column(name = "VENDER_ID")
    private Integer venderId;

    @Column(name = "CONTACT_TYPE_ID")
    private Integer contactTypeId;

    @Column(name = "VENDER_CONTACT_NAME")
    private String venderContactName;

    @Column(name = "VENDER_CONTACT_PHONE")
    private String venderContactPhone;

    @Column(name = "VENDER_CONTACT_FAX")
    private String venderContactFax;

    @Column(name = "VENDER_CONTACT_MOBILE")
    private String venderContactMobile;

    @Column(name = "VENDER_CONTACT_EMAIL")
    private String venderContactEmail;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "VENDER_CONTACT_PRIMARY")
    private String venderContactPrimary;

    @Column(name = "VENDOR_ID_old")
    private String vendorIdOld;

    @Column(name = "REMARK")
    private String REMARK;

}
