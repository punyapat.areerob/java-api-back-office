package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_SALES_SUPPORT")
public class SmmSalesSupport implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "SALES_SUPPORT_ID", nullable = false)
    private Integer salesSupportId;

    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "PACKAGE_ID")
    private Integer packageId;

    @Column(name = "APPLICATION_PICTURE")
    private String applicationPicture;

    @Column(name = "APPLICATION_PATH")
    private String applicationPath;

    @Column(name = "APPLICATION_NO")
    private String applicationNo;

    @Column(name = "MEMBERSHIP_NO")
    private String membershipNo;

    @Column(name = "AGENT_ID")
    private Integer agentId;

    @Column(name = "MEMBERSHIP_ID_NO")
    private String membershipIdNo;

    @Column(name = "APPROVAL_NO")
    private String approvalNo;

    @Column(name = "GENDER_ID")
    private String genderId;

    @Column(name = "TITLE_ID")
    private Integer titleId;

    @Column(name = "FNAME_TH")
    private String fnameTh;

    @Column(name = "FNAME_EN")
    private String fnameEn;

    @Column(name = "MNAME_TH")
    private String mnameTh;

    @Column(name = "MNAME_EN")
    private String mnameEn;

    @Column(name = "LNAME_TH")
    private String lnameTh;

    @Column(name = "LNAME_EN")
    private String lnameEn;

    @Column(name = "NAME_ON_CARD")
    private String nameOnCard;

    @Column(name = "BIRTHDATE")
    private Date BIRTHDATE;

    @Column(name = "BLOOD_TYPE")
    private String bloodType;

    @Column(name = "RELIGION_ID")
    private Integer religionId;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "PASSPORT_ISSUE_BY")
    private String passportIssueBy;

    @Column(name = "PASSPORT_ISSUE_DATE")
    private Date passportIssueDate;

    @Column(name = "PASSPORT_EXPIRY_DATE")
    private Date passportExpiryDate;

    @Column(name = "OCCUPATION_ID")
    private Integer occupationId;

    @Column(name = "BUSINESS_TITLE")
    private String businessTitle;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "NATURE_OF_BUSINESS_ID")
    private Integer natureOfBusinessId;

    @Column(name = "PAYMENT_STATUS")
    private String paymentStatus;

    @Column(name = "APPLICATION_STATUS_CODE", nullable = false)
    private String applicationStatusCode;

    @Column(name = "PAYMENT_DATE")
    private Date paymentDate;

    @Column(name = "CARD_ISSUE_DATE")
    private Date cardIssueDate;

    @Column(name = "CARD_EXPIRY_DATE")
    private Date cardExpiryDate;

    @Column(name = "APPLICATION_FILE")
    private String applicationFile;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "ALLERGIC")
    private String ALLERGIC;

    @Column(name = "REMARK")
    private String REMARK;

}
