package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_UPGRADE")
public class MasUpgrade implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "UPGRADE_ID", nullable = false)
    private Integer upgradeId;

    @Column(name = "UPGRADE_CODE")
    private String upgradeCode;

    @Column(name = "UPGRADE_AMOUNT")
    private BigDecimal upgradeAmount;

    @Column(name = "VAT_TYPE")
    private String vatType;

    @Column(name = "UPGARDE_STATUS")
    private String upgardeStatus;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
