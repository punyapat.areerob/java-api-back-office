package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_GR_PROCESS")
public class SmmGrProcess implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "GR_PROCESS_ID", nullable = false)
    private Integer grProcessId;

    @Column(name = "DOC_NO")
    private String docNo;

    @Column(name = "DOC_DATE")
    private Date docDate;

    @Column(name = "IMMIGRATION_OFFER_STATUS")
    private String immigrationOfferStatus;

    @Column(name = "GR_RECEIVE_FILE_STATUS")
    private String grReceiveFileStatus;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "IMMIGRATION_CONFIRM_STATUS")
    private String immigrationConfirmStatus;

    @Column(name = "IMMIGRATION_PATH")
    private String immigrationPath;

    @Column(name = "IMMIGRATION_ATTACH_FILE")
    private String immigrationAttachFile;

    @Column(name = "IMMIGRATION_APPROVE_DATE")
    private Date immigrationApproveDate;

    @Column(name = "IMMIGRATION_DOC_NO")
    private String immigrationDocNo;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "IMMIGRATION_FILE_REMARK")
    private String immigrationFileRemark;

}
