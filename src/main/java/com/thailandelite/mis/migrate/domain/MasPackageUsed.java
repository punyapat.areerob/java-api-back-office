package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_PACKAGE_USED")
public class MasPackageUsed implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PACKAGE_USED_ID", nullable = false)
    private Integer packageUsedId;

    @Column(name = "PACKAGE_GROUP_ID")
    private Integer packageGroupId;

    @Column(name = "PACKAGE_USED_YEAR")
    private String packageUsedYear;

    @Column(name = "PACKAGE_USED_TIME")
    private Integer packageUsedTime;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PRIVILEGE_CODE")
    private String privilegeCode;

    @Column(name = "REMARK")
    private String REMARK;

}
