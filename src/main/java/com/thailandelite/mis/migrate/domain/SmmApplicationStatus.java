package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_APPLICATION_STATUS")
public class SmmApplicationStatus implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "APPLICATION_STATUS_ID", nullable = false)
    private Integer applicationStatusId;

    @Column(name = "APPLICATION_STATUS_CODE", nullable = false)
    private String applicationStatusCode;

    @Column(name = "APPLICATION_STATUS_NAME_TH")
    private String applicationStatusNameTh;

    @Column(name = "APPLICATION_STATUS_NAME_EN")
    private String applicationStatusNameEn;

    @Column(name = "APPLICATION_STATUS_ORDER", nullable = false)
    private Integer applicationStatusOrder;

    @Column(name = "CANCEL_APPLICATION_STATUS_ID")
    private Integer cancelApplicationStatusId;

    @Column(name = "NEXT_APPLICATION_STATUS_ID")
    private Integer nextApplicationStatusId;

    @Column(name = "APPLICATION_BUTTON_NEXT_STATUS_NAME_TH")
    private String applicationButtonNextStatusNameTh;

    @Column(name = "APPLICATION_BUTTON_NEXT_STATUS_NAME_EN")
    private String applicationButtonNextStatusNameEn;

    @Column(name = "APPLICATION_BUTTON_CANCEL_STATUS_NAME_TH")
    private String applicationButtonCancelStatusNameTh;

    @Column(name = "APPLICATION_BUTTON_CANCEL_STATUS_NAME_EN")
    private String applicationButtonCancelStatusNameEn;

    @Column(name = "EDITABLE")
    private String EDITABLE;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

}
