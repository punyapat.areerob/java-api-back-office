package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_CITY")
public class MasCity implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "CITY_ID", nullable = false)
    private Integer cityId;

    @Column(name = "STATE_ID")
    private Integer stateId;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "CITY_CODE")
    private String cityCode;

    @Column(name = "CITY_NAME_TH")
    private String cityNameTh;

    @Column(name = "CITY_NAME_EN")
    private String cityNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PROVINCE_ID")
    private String provinceId;

    @Column(name = "DISTRICT_ID")
    private String districtId;

    @Column(name = "STATE_CODE_old")
    private String stateCodeOld;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "CITY_ID_old")
    private Integer cityIdOld;

    @Column(name = "REMARK")
    private String REMARK;

}
