package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_RESTAURANT")
public class MasRestaurant implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "RESTAURANT_ID", nullable = false)
    private Integer restaurantId;

    @Column(name = "RESTAURANT_CODE")
    private String restaurantCode;

    @Column(name = "RESTAURANT_NAME_TH")
    private String restaurantNameTh;

    @Column(name = "RESTAURANT_NAME_EN")
    private String restaurantNameEn;

    @Column(name = "COMPANY_ID")
    private Integer companyId;

    @Column(name = "RESTAURANT_START")
    private Date restaurantStart;

    @Column(name = "RESTAURANT_END")
    private Date restaurantEnd;

    @Column(name = "RESTAURANT_STATUS")
    private String restaurantStatus;

    @Column(name = "PAYMENT_STATUS")
    private String paymentStatus;

    @Column(name = "RESTAURANT_CUISINE")
    private String restaurantCuisine;

    @Column(name = "RESTAURANT_OPERATION_HOME")
    private String restaurantOperationHome;

    @Column(name = "RESTAURANT_ADDRESS")
    private String restaurantAddress;

    @Column(name = "SUB_DISTRICT_ID")
    private Integer subDistrictId;

    @Column(name = "DISTRICT_ID")
    private Integer districtId;

    @Column(name = "PROVINCE_ID")
    private Integer provinceId;

    @Column(name = "RESTAURANT_POSTCODE")
    private String restaurantPostcode;

    @Column(name = "RESTAURANT_PHONE")
    private String restaurantPhone;

    @Column(name = "RESTAURANT_FAX")
    private String restaurantFax;

    @Column(name = "RESTAURANT_WEBSITE")
    private String restaurantWebsite;

    @Column(name = "RESTAURANT_EMAIL")
    private String restaurantEmail;

    @Column(name = "PRIVILEGE_ID")
    private Integer privilegeId;

    @Column(name = "RESTAURANT_RESERVATION_POLICY_HOUR")
    private Integer restaurantReservationPolicyHour;

    @Column(name = "RESTAURANT_CANCELLATION_POLICY_HOUR")
    private Integer restaurantCancellationPolicyHour;

    @Column(name = "RESTAURANT_DESCRIPTION")
    private String restaurantDescription;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
