package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@Table(name = "CMM_COMMISSION_ISSUE_ITEM")
public class CmmCommissionIssueItem implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "COMMISSION_ISSUE_ITEM_ID", nullable = false)
    private Integer commissionIssueItemId;

    @Column(name = "COMMISSION_ISSUE_ID")
    private Integer commissionIssueId;

    @Column(name = "COMMISSION_RATE_PERCENT")
    private Float commissionRatePercent;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "COMMISSION_AMOUNT")
    private BigDecimal commissionAmount;

    @Column(name = "INVOICE_NO")
    private String invoiceNo;

    @Column(name = "INVOICE_DATE")
    private LocalDate invoiceDate;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "COMMISSION_ORDER")
    private Integer commissionOrder;

    @Column(name = "VAT_TYPE")
    private String vatType;

    @Column(name = "CURRENCY")
    private String CURRENCY;

    @Column(name = "RATE")
    private BigDecimal RATE;

    @Column(name = "MEMBERSHIP_BEFORE_VAT_AMOUNT")
    private BigDecimal membershipBeforeVatAmount;

    @Column(name = "MEMBERSHIP_VAT_AMOUNT")
    private BigDecimal membershipVatAmount;

    @Column(name = "MEMBERSHIP_TOTAL_AMOUNT")
    private BigDecimal membershipTotalAmount;

    @Column(name = "COMMISSION_BEFORE_VAT_AMOUNT")
    private BigDecimal commissionBeforeVatAmount;

    @Column(name = "COMMISSION_VAT_AMOUNT")
    private BigDecimal commissionVatAmount;

    @Column(name = "COMMISSION_TOTAL_AMOUNT")
    private BigDecimal commissionTotalAmount;

}
