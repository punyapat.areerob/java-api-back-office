package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_JOB_ASSIGNMENT_AMEND")
public class CcsJobAssignmentAmend implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "JOB_ASSIGNMENT_AMEND_ID", nullable = false)
    private Integer jobAssignmentAmendId;

    @Column(name = "JOB_ASSIGNMENT_ID")
    private Integer jobAssignmentId;

    @Column(name = "RUNNING")
    private Integer RUNNING;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "RESERVATION_ID")
    private Integer reservationId;

    @Column(name = "import_name")
    private String importName;

}
