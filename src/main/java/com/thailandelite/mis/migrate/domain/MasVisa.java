package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_VISA")
public class MasVisa implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "VISA_ID", nullable = false)
    private Integer visaId;

    @Column(name = "VISA_CODE")
    private String visaCode;

    @Column(name = "VISA_YEAR_QUANTITY")
    private Integer visaYearQuantity;

    @Column(name = "VISA_RENEWABLE")
    private String visaRenewable;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
