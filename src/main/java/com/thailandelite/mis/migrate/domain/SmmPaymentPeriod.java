package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_PAYMENT_PERIOD")
public class SmmPaymentPeriod implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PAYMENT_PERIOD_ID", nullable = false)
    private Integer paymentPeriodId;

    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "AMOUNT")
    private BigDecimal AMOUNT;

    @Column(name = "PAY_DATE")
    private LocalDate payDate;

    @Column(name = "PAYMENT_PATH")
    private String paymentPath;

    @Column(name = "PAYMENT_FILE")
    private String paymentFile;

    @Column(name = "FINANCE_FILE")
    private String financeFile;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PAY_TIME")
    private LocalTime payTime;

    @Column(name = "PAY_STATUS")
    private String payStatus;

    @Column(name = "FINANCE_CONFIRM")
    private String financeConfirm;

    @Column(name = "RECEIVED_AMOUNT")
    private BigDecimal receivedAmount;

    @Column(name = "RECEIVE_DATE")
    private Date receiveDate;

    @Column(name = "RECEIVE_TIME")
    private LocalTime receiveTime;

    @Column(name = "PAYMENT_PERIOD_CURRENCY")
    private String paymentPeriodCurrency;

    @Column(name = "PAYMENT_RATE")
    private BigDecimal paymentRate;

    @Column(name = "CHANNEL")
    private String CHANNEL;

    @Column(name = "REMARK")
    private String REMARK;

}
