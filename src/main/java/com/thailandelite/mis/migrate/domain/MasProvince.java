package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_PROVINCE")
public class MasProvince implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PROVINCE_ID", nullable = false)
    private Integer provinceId;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "PROVINCE_CODE")
    private String provinceCode;

    @Column(name = "PROVINCE_NAME_TH")
    private String provinceNameTh;

    @Column(name = "PROVINCE_NAME_EN_bk")
    private String provinceNameEnBk;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "COUNTRY_CODE_old")
    private String countryCodeOld;

    @Column(name = "PROVINCE_NAME_EN")
    private String provinceNameEn;

    @Column(name = "REMARK")
    private String REMARK;

}
