package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_MEMBER_PASSPORT")
public class SmmMemberPassport implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PASSPORT_ID", nullable = false)
    private Integer passportId;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "PASSPORT_ISSUE_BY")
    private String passportIssueBy;

    @Column(name = "PASSPORT_ISSUE_DATE")
    private ZonedDateTime passportIssueDate;

    @Column(name = "PASSPORT_EXPIRY_DATE")
    private ZonedDateTime passportExpiryDate;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "PASSPORT_PATH")
    private String passportPath;

    @Column(name = "FILE_PASSPORT")
    private String filePassport;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private ZonedDateTime createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private ZonedDateTime lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "NATIONALITY_PASSPORT_ID")
    private Integer nationalityPassportId;

    @Column(name = "REMARK")
    private String REMARK;

}
