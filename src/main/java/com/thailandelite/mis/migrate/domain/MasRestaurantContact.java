package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_RESTAURANT_CONTACT")
public class MasRestaurantContact implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "RESTAURANT_CONTACT_ID", nullable = false)
    private Integer restaurantContactId;

    @Column(name = "RESTAURANT_ID")
    private Integer restaurantId;

    @Column(name = "RESTAURANT_TYPE_ID")
    private Integer restaurantTypeId;

    @Column(name = "RESTAURANT_CONTACT_NAME")
    private String restaurantContactName;

    @Column(name = "RESTAURANT_CONTACT_PHONE")
    private String restaurantContactPhone;

    @Column(name = "RESTAURANT_CONTACT_FAX")
    private String restaurantContactFax;

    @Column(name = "RESTAURANT_VENDER_CONTACT_MOBILE")
    private String restaurantVenderContactMobile;

    @Column(name = "RESTAURANT_CONTACT_EMAIL")
    private String restaurantContactEmail;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
