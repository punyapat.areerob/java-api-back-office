package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_MEMBER_VISA")
public class SmmMemberVisa implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "VISA_ID", nullable = false)
    private Integer visaId;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "VISA_NO")
    private String visaNo;

    @Column(name = "VISA_AT_ISSUE")
    private String visaAtIssue;

    @Column(name = "VISA_ISSUE_DATE")
    private Date visaIssueDate;

    @Column(name = "VISA_EXPIRY_DATE")
    private Date visaExpiryDate;

    @Column(name = "VISA_PATH")
    private String visaPath;

    @Column(name = "FILE_VISA")
    private String fileVisa;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "PASSPORT_ID")
    private String passportId;

    @Column(name = "IMPORT_BY")
    private String importBy;

    @Column(name = "REMARK")
    private String REMARK;

}
