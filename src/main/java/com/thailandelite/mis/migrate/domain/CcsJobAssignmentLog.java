package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_JOB_ASSIGNMENT_LOG")
public class CcsJobAssignmentLog implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "JOB_ASSIGNMENT_LOG_ID", nullable = false)
    private Integer jobAssignmentLogId;

    @Column(name = "JOB_ASSIGNMENT_ID")
    private Integer jobAssignmentId;

    @Column(name = "CALLING_ID")
    private Integer callingId;

    @Column(name = "VENDER_ID")
    private Integer venderId;

    @Column(name = "PRIVILEGE_SERVICES_ID")
    private Integer privilegeServicesId;

    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Column(name = "IMMIGRATION_OFFICE_ID")
    private Integer immigrationOfficeId;

    @Column(name = "JOB_ASSIGNMENT_CODE")
    private String jobAssignmentCode;

    @Column(name = "PICKUP_FROM_IN")
    private Integer pickupFromIn;

    @Column(name = "TRANSFER_TO_IN")
    private Integer transferToIn;

    @Column(name = "FLIGHT_DATE")
    private Date flightDate;

    @Column(name = "FLIGHT_NO")
    private String flightNo;

    @Column(name = "FLIGHT_FROM")
    private Integer flightFrom;

    @Column(name = "FLIGHT_TO")
    private Integer flightTo;

    @Column(name = "FLIGHT_CLASS_ID")
    private Integer flightClassId;

    @Column(name = "ETD")
    private LocalTime ETD;

    @Column(name = "ETA")
    private LocalTime ETA;

    @Column(name = "FLIGHT_DATE2")
    private Date flightDate2;

    @Column(name = "FLIGHT_NO2")
    private String flightNo2;

    @Column(name = "FLIGHT_FROM2")
    private Integer flightFrom2;

    @Column(name = "FLIGHT_TO2")
    private Integer flightTo2;

    @Column(name = "FLIGHT_CLASS_ID2")
    private Integer flightClassId2;

    @Column(name = "ETD2")
    private LocalTime ETD2;

    @Column(name = "ETA2")
    private LocalTime ETA2;

    @Column(name = "REQUIRE_DATE")
    private Date requireDate;

    @Column(name = "REQUIRE_TIME")
    private LocalTime requireTime;

    @Column(name = "VIP_TYPE_ID")
    private Integer vipTypeId;

    @Column(name = "VIP_DETAIL")
    private String vipDetail;

    @Column(name = "PICKUP_FROM_OUT")
    private Integer pickupFromOut;

    @Column(name = "TRANSFER_TO_OUT")
    private Integer transferToOut;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "JA_STATUS_CODE")
    private String jaStatusCode;

    @Column(name = "EPA_APPROVE")
    private String epaApprove;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "PRIVILEGE_ID")
    private Integer privilegeId;

    @Column(name = "JOB_ASSIGNMENT_PARENT_ID")
    private Integer jobAssignmentParentId;

    @Column(name = "AMEND_NO")
    private Integer amendNo;

    @Column(name = "MEMBER_PAID_STATUS")
    private String memberPaidStatus;

    @Column(name = "EPA_STATUS")
    private String epaStatus;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "JA_STATUS")
    private String jaStatus;

    @Column(name = "SATISFACTION")
    private Integer SATISFACTION;

    @Column(name = "ATTACH_FILE1")
    private String attachFile1;

    @Column(name = "ATTACH_FILE2")
    private String attachFile2;

    @Column(name = "ATTACH_FILE3")
    private String attachFile3;

    @Column(name = "CONTACT_NAME_STAFF")
    private String contactNameStaff;

    @Column(name = "SERVICES_ID")
    private Integer servicesId;

    @Column(name = "PRIVILEGE_GROUP_ID")
    private Integer privilegeGroupId;

    @Column(name = "RESERVATION_ID")
    private Integer reservationId;

    @Column(name = "CUSTOMER_ID")
    private String customerId;

    @Column(name = "SERVICE_TYPE_CODE")
    private Integer serviceTypeCode;

    @Column(name = "RESERVATION_STATUS_CODE")
    private Integer reservationStatusCode;

    @Column(name = "VENDOR_ID_old")
    private String vendorIdOld;

    @Column(name = "PRODUCT_ID_old")
    private String productIdOld;

    @Column(name = "TICKET_ID_old")
    private String ticketIdOld;

    @Column(name = "import_name")
    private String importName;

    @Column(name = "GUEST")
    private Integer GUEST;

    @Column(name = "RESERVATION_FILE")
    private String reservationFile;

    @Column(name = "RESERVATION_PATH")
    private String reservationPath;

    @Column(name = "JA_QUOTA_STATUS")
    private String jaQuotaStatus;

    @Column(name = "JA_CLOSE_DATE")
    private Date jaCloseDate;

    @Column(name = "JA_CLOSE_TIME")
    private LocalTime jaCloseTime;

    @Column(name = "EPL_STATUS")
    private String eplStatus;

    @Column(name = "GR_USER_ID")
    private Integer grUserId;

    @Column(name = "GR_STATUS")
    private String grStatus;

    @Column(name = "id_old")
    private Integer idOld;

    @Column(name = "MEMBER_NAME")
    private String memberName;

    @Column(name = "MEMBERSHIP_ID_NO")
    private String membershipIdNo;

    @Column(name = "MEMBER_ACTIVE_DATE")
    private Date memberActiveDate;

    @Column(name = "MEMBER_EXPIRE_DATE")
    private Date memberExpireDate;

    @Column(name = "MEMBER_NATIONALITY_NAME")
    private String memberNationalityName;

    @Column(name = "MEMBER_PASSPORT_NO")
    private String memberPassportNo;

    @Column(name = "MEMBER_TYPE_NAME")
    private String memberTypeName;

    @Column(name = "MEMBER_EMAIL")
    private String memberEmail;

    @Column(name = "MEMBER_BIRTHDATE")
    private Date memberBirthdate;

    @Column(name = "PACKAGE_NAME")
    private String packageName;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "VENDOR_NAME")
    private String vendorName;

    @Column(name = "VENDOR_EMAIL")
    private String vendorEmail;

    @Column(name = "VENDOR_PHONE")
    private String vendorPhone;

    @Column(name = "VENDOR_FAX")
    private String vendorFax;

    @Column(name = "PRIVILEGE_GROUP_NAME")
    private String privilegeGroupName;

    @Column(name = "PRIVILEGE_NAME")
    private String privilegeName;

    @Column(name = "SERVICES_NAME")
    private String servicesName;

    @Column(name = "JOB_ASSIGNMENT_TOGETHER_ID")
    private Integer jobAssignmentTogetherId;

    @Column(name = "AMEND_DATE")
    private Date amendDate;

    @Column(name = "AMEND_USER")
    private String amendUser;

    @Column(name = "AMEND_USER_TYPE")
    private String amendUserType;

    @Column(name = "MEETING_POINT")
    private String meetingPoint;

    @Column(name = "EPA_REMARK")
    private String epaRemark;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "SATISFACTION_DETAIL")
    private String satisfactionDetail;

    @Column(name = "REMARK_CANCEL_JA")
    private String remarkCancelJa;

    @Column(name = "SERVICES_CONFIRM_LETTER")
    private String servicesConfirmLetter;

    @Column(name = "EPL_REMARK")
    private String eplRemark;

}
