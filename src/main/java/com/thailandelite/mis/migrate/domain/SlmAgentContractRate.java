package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SLM_AGENT_CONTRACT_RATE")
public class SlmAgentContractRate implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "AGENT_CONTRACT_ITEM_ID", nullable = false)
    private Integer agentContractItemId;

    @Column(name = "AGENT_CONTRACT_ID")
    private Integer agentContractId;

    @Column(name = "COMMISSION_RATE_ID")
    private Integer commissionRateId;

    @Column(name = "CONTRACT_DATE_START")
    private Date contractDateStart;

    @Column(name = "CONTRACT_DATE_END")
    private Date contractDateEnd;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
