package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_PACKAGE")
public class MasPackage implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PACKAGE_ID", nullable = false)
    private Integer packageId;

    @Column(name = "PACKAGE_SMALL_PICTURE")
    private String packageSmallPicture;

    @Column(name = "PACKAGE_LARGE_PICTURE")
    private String packageLargePicture;

    @Column(name = "PACKAGE_PATH")
    private String packagePath;

    @Column(name = "CARD_PICTURE")
    private String cardPicture;

    @Column(name = "PACKAGE_CODE")
    private String packageCode;

    @Column(name = "PACKAGE_NAME_EN")
    private String packageNameEn;

    @Column(name = "PACKAGE_NAME_TH")
    private String packageNameTh;

    @Column(name = "CARD_ID")
    private Integer cardId;

    @Column(name = "MEMBERSHIP_ID")
    private Integer membershipId;

    @Column(name = "MEMBERSHIP_VABILITY_ID")
    private Integer membershipVabilityId;

    @Column(name = "MEMBERSHIP_FEE_ID")
    private Integer membershipFeeId;

    @Column(name = "ADDITIONNAL_FAMILY_MEMBER_ID")
    private Integer additionnalFamilyMemberId;

    @Column(name = "MEMBERSHIP_TRANSFER_ID")
    private Integer membershipTransferId;

    @Column(name = "ADDITIONAL_FAMILY_MEMBER_ID")
    private Integer additionalFamilyMemberId;

    @Column(name = "TRANSFER_FEE_ID")
    private Integer transferFeeId;

    @Column(name = "ANNUAL_FEE_ID")
    private Integer annualFeeId;

    @Column(name = "AGE_ID")
    private Integer ageId;

    @Column(name = "VISA_ID")
    private Integer visaId;

    @Column(name = "AIRPORT_SERVICE")
    private String airportService;

    @Column(name = "DEPARTURE_ARRIVAL_ID")
    private Integer departureArrivalId;

    @Column(name = "SH_TRANSFER_SERVICE_ID")
    private Integer shTransferServiceId;

    @Column(name = "LH_TRANSFER_SERVICE_ID")
    private Integer lhTransferServiceId;

    @Column(name = "TRANSFER_SERVICE_TIME_ID")
    private Integer transferServiceTimeId;

    @Column(name = "DISCOUNT")
    private String DISCOUNT;

    @Column(name = "MEMBER_CONTACT_CENTER")
    private String memberContactCenter;

    @Column(name = "GOVERNMENT_CONCIERGES")
    private String governmentConcierges;

    @Column(name = "NINETY_DAY_REPORT")
    private String ninetyDayReport;

    @Column(name = "UPGRADE_ID")
    private Integer upgradeId;

    @Column(name = "ID_PACKAGE")
    private String idPackage;

    @Column(name = "UPGRADE_TO_PACKAGE_ID")
    private Integer upgradeToPackageId;

    @Column(name = "ID")
    private Integer ID;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "TERMINATE_STATUS")
    private String terminateStatus;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PROJECT_ID")
    private Integer projectId;

    @Column(name = "PACKAGE_ID_OLD")
    private Integer packageIdOld;

    @Column(name = "PACKAGE_OLD")
    private String packageOld;

    @Column(name = "MEMBER_TYPE_OLD")
    private String memberTypeOld;

    @Column(name = "SHOW_CORE_OTHER_STATUS")
    private String showCoreOtherStatus;

    @Column(name = "SHOW_AMOUNT")
    private String showAmount;

    @Column(name = "ACC_CHART_CODE")
    private String accChartCode;

    @Column(name = "ACC_CHART_CODE_OPPOSITE")
    private String accChartCodeOpposite;

    @Column(name = "LAST_ACCOUNT_DATE")
    private Date lastAccountDate;

    @Column(name = "LAST_ACCOUNT_USER")
    private String lastAccountUser;

    @Column(name = "ACC_CHART_CODE_ACCUMULATE")
    private String accChartCodeAccumulate;

    @Column(name = "NEW_PACKAGE_STATUS")
    private String newPackageStatus;

    @Column(name = "PUBLICE_STATUS")
    private String publiceStatus;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "APPROVAL_LETTER_DETAIL")
    private String approvalLetterDetail;

    @Column(name = "WELCOME_LETTER_DETAIL")
    private String welcomeLetterDetail;

}
