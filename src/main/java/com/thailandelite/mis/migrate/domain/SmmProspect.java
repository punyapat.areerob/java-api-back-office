package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_PROSPECT")
public class SmmProspect implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PROSPECT_ID", nullable = false)
    private Integer prospectId;

    @Column(name = "GENDER_ID")
    private String genderId;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "AGE")
    private Integer AGE;

    @Column(name = "EMAIL")
    private String EMAIL;

    @Column(name = "MOBILE")
    private String MOBILE;

    @Column(name = "TELEPHONE")
    private String TELEPHONE;

    @Column(name = "COMPANY")
    private String COMPANY;

    @Column(name = "STATUS")
    private String STATUS;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "PROJECT_ID")
    private Integer projectId;

    @Column(name = "CHANNEL_ID")
    private String channelId;

    @Column(name = "ADDRESS")
    private String ADDRESS;

    @Column(name = "REMARK")
    private String REMARK;

}
