package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_DOCUMENT_CODE")
public class MasDocumentCode implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MAS_DOCUMENT_CODE_ID", nullable = false)
    private Integer masDocumentCodeId;

    @Column(name = "DOCUMENT_CODE_TYPE")
    private String documentCodeType;

    @Column(name = "DOCUMENT_CODE_SYSTEM")
    private String documentCodeSystem;

    @Column(name = "DOCUMENT_CODE_FORMAT")
    private String documentCodeFormat;

    @Column(name = "DOCUMENT_CODE_NAME_TH")
    private String documentCodeNameTh;

    @Column(name = "DOCUMENT_CODE_NAME_EN")
    private String documentCodeNameEn;

    @Column(name = "DOCUMENT_CODE_LAST")
    private String documentCodeLast;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "DOCUMENT_CODE_TYPE_DESC")
    private String documentCodeTypeDesc;

}
