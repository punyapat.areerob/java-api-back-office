package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_CALLING")
public class CcsCalling implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "CALLING_ID", nullable = false)
    private Integer callingId;

    @Column(name = "TICKET_ID")
    private String ticketId;

    @Column(name = "PHONE_NO")
    private String phoneNo;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "CALLING_TYPE")
    private String callingType;

    @Column(name = "RESERVATION_STATUS")
    private String reservationStatus;

    @Column(name = "PHONE_ID")
    private Integer phoneId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "MORE_MEMBER_ID")
    private String moreMemberId;

    @Column(name = "CHANEL")
    private String CHANEL;

    @Column(name = "FIRST_REQUIRE_DATE")
    private LocalDate firstRequireDate;

    @Column(name = "CALL_CENTER_ASSIGN_USER_ID")
    private Integer callCenterAssignUserId;

    @Column(name = "CUSTOMER_ID")
    private String customerId;

    @Column(name = "SEVERITY_LEVEL")
    private Integer severityLevel;

    @Column(name = "PRIORITY_LEVEL")
    private Integer priorityLevel;

    @Column(name = "import_name")
    private String importName;

    @Column(name = "URGENT_STATUS")
    private String urgentStatus;

    @Column(name = "CALLING_ID_old")
    private Integer callingIdOld;

    @Column(name = "CALLING_DESCRIPTION")
    private String callingDescription;

    @Column(name = "CALLING_SOLUTIONS")
    private String callingSolutions;

    @Column(name = "REMARK")
    private String REMARK;

}
