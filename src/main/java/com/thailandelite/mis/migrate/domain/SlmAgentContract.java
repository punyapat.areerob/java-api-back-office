package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SLM_AGENT_CONTRACT")
public class SlmAgentContract implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "AGENT_CONTRACT_ID", nullable = false)
    private Integer agentContractId;

    @Column(name = "AGENT_ID")
    private Integer agentId;

    @Column(name = "CONTRACT_DATE_START")
    private Date contractDateStart;

    @Column(name = "CONTRACT_DATE_END")
    private Date contractDateEnd;

    @Column(name = "TARGET")
    private Integer TARGET;

    @Column(name = "TARGET_FIRST_YEAR")
    private Integer targetFirstYear;

    @Column(name = "TARGET_SECOND_YEAR")
    private Integer targetSecondYear;

    @Column(name = "AGENT_CONTRACT_PATH")
    private String agentContractPath;

    @Column(name = "FILE_PROPERTY_CONTRACT")
    private String filePropertyContract;

    @Column(name = "FILE_CONTRACT_TH")
    private String fileContractTh;

    @Column(name = "FILE_CONTRACT_EN")
    private String fileContractEn;

    @Column(name = "FILE_COMPANY_CERTIFICATE")
    private String fileCompanyCertificate;

    @Column(name = "FILE_BOOK_BANK")
    private String fileBookBank;

    @Column(name = "FILE_ID_CARD_COPY")
    private String fileIdCardCopy;

    @Column(name = "FILE_ADDRESS_COPY")
    private String fileAddressCopy;

    @Column(name = "FILE_PASSPORT")
    private String filePassport;

    @Column(name = "FILE_POWER_OF_ATTORNEY")
    private String filePowerOfAttorney;

    @Column(name = "AGENT_CONTRACT_STATUS")
    private String agentContractStatus;

    @Column(name = "COMMISSION_RATE_ID")
    private Integer commissionRateId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "CONTRACT_DATE_END_bk")
    private Date contractDateEndBk;

    @Column(name = "REMARK")
    private String REMARK;

}
