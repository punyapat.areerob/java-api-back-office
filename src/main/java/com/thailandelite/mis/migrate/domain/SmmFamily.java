package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_FAMILY")
public class SmmFamily implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "FAMILY_ID", nullable = false)
    private Integer familyId;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "RELATIONSHIP_ID")
    private Integer relationshipId;

    @Column(name = "FAMILY_PICTURE")
    private String familyPicture;

    @Column(name = "FAMILY_PICTURE_PATH")
    private String familyPicturePath;

    @Column(name = "FNAME_TH")
    private String fnameTh;

    @Column(name = "FNAME_EN")
    private String fnameEn;

    @Column(name = "LNAME_TH")
    private String lnameTh;

    @Column(name = "LNAME_EN")
    private String lnameEn;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "RACE_ID")
    private Integer raceId;

    @Column(name = "RELIGION_ID")
    private Integer religionId;

    @Column(name = "GENDER_ID")
    private String genderId;

    @Column(name = "BIRTHDATE")
    private Date BIRTHDATE;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "PASSPORT_EXPIRY_DATE")
    private Date passportExpiryDate;

    @Column(name = "VISA_NO")
    private String visaNo;

    @Column(name = "VISA_EXPIRY_DATE")
    private Date visaExpiryDate;

    @Column(name = "EMAIL")
    private String EMAIL;

    @Column(name = "MOBILE_IN_HOME_COUNTRY")
    private String mobileInHomeCountry;

    @Column(name = "MOBILE_IN_THAI")
    private String mobileInThai;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "TITLE_ID")
    private Integer titleId;

    @Column(name = "REMARK")
    private String REMARK;

}
