package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_PRIVILEGE_GROUP")
public class MasPrivilegeGroup implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PRIVILEGE_GROUP_ID", nullable = false)
    private Integer privilegeGroupId;

    @Column(name = "PRIVILEGE_GROUP_NAME_TH")
    private String privilegeGroupNameTh;

    @Column(name = "PRIVILEGE_GROUP_NAME_EN")
    private String privilegeGroupNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PRIVILEGE_GROUP_CODE")
    private String privilegeGroupCode;

    @Column(name = "AOT_REPORT_STATUS")
    private String aotReportStatus;

    @Column(name = "AOT_REPORT_ORDER")
    private Integer aotReportOrder;

    @Column(name = "ACC_CHART_CODE")
    private String accChartCode;

    @Column(name = "LAST_ACCOUNT_DATE")
    private Date lastAccountDate;

    @Column(name = "LAST_ACCOUNT_USER")
    private String lastAccountUser;

    @Column(name = "REMARK")
    private String REMARK;

}
