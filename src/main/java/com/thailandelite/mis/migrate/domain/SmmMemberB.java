package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_MEMBER_B")
public class SmmMemberB implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MEMBER_ID", nullable = false)
    private Integer memberId;

    @Column(name = "MEMBER_PICTURE")
    private String memberPicture;

    @Column(name = "MEMBER_PATH")
    private String memberPath;

    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "PACKAGE_ID")
    private Integer packageId;

    @Column(name = "APPLICATION_NO")
    private String applicationNo;

    @Column(name = "MEMBERSHIP_NO")
    private String membershipNo;

    @Column(name = "AGENT_ID")
    private Integer agentId;

    @Column(name = "MEMBERSHIP_ID_NO")
    private String membershipIdNo;

    @Column(name = "APPROVAL_NO")
    private String approvalNo;

    @Column(name = "GENDER_ID")
    private String genderId;

    @Column(name = "TITLE_ID")
    private Integer titleId;

    @Column(name = "FNAME")
    private String FNAME;

    @Column(name = "MNAME")
    private String MNAME;

    @Column(name = "LNAME")
    private String LNAME;

    @Column(name = "NICK_NAME")
    private String nickName;

    @Column(name = "NAME_ON_CARD")
    private String nameOnCard;

    @Column(name = "BIRTHDATE")
    private Date BIRTHDATE;

    @Column(name = "BLOOD_TYPE")
    private String bloodType;

    @Column(name = "RELIGION_ID")
    private Integer religionId;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "PASSPORT_ISSUE_BY")
    private String passportIssueBy;

    @Column(name = "PASSPORT_ISSUE_DATE")
    private Date passportIssueDate;

    @Column(name = "PASSPORT_EXPIRY_DATE")
    private Date passportExpiryDate;

    @Column(name = "OCCUPATION_ID")
    private Integer occupationId;

    @Column(name = "BUSINESS_TITLE")
    private String businessTitle;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "NATURE_OF_BUSINESS_ID")
    private Integer natureOfBusinessId;

    @Column(name = "NATURE_OF_BUSINESS_OTHER")
    private String natureOfBusinessOther;

    @Column(name = "OCCUPATION_OTHER")
    private String occupationOther;

    @Column(name = "MEMBER_SINCE")
    private Date memberSince;

    @Column(name = "RACE_ID")
    private Integer raceId;

    @Column(name = "VISA_NO")
    private String visaNo;

    @Column(name = "VISA_EXPIRY_DATE")
    private Date visaExpiryDate;

    @Column(name = "EMAIL")
    private String EMAIL;

    @Column(name = "MOBILE_IN_HOME_COUNTRY")
    private String mobileInHomeCountry;

    @Column(name = "MOBILE_IN_THAI")
    private String mobileInThai;

    @Column(name = "DEGREE_ID")
    private Integer degreeId;

    @Column(name = "REVENUE")
    private BigDecimal REVENUE;

    @Column(name = "BIRTHPLACE")
    private String BIRTHPLACE;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "MEMBER_STATUS")
    private String memberStatus;

    @Column(name = "CARD_ISSUE_DATE")
    private Date cardIssueDate;

    @Column(name = "CARD_EXPIRY_DATE")
    private Date cardExpiryDate;

    @Column(name = "MEMBER_TYPE")
    private String memberType;

    @Column(name = "MEMBER_CORE_ID")
    private Integer memberCoreId;

    @Column(name = "MEMBER_FILE_APPLICATION")
    private String memberFileApplication;

    @Column(name = "MEMBER_FILE_OTHER")
    private String memberFileOther;

    @Column(name = "MEMBER_FILE_PASSPORT")
    private String memberFilePassport;

    @Column(name = "MEMBER_GROUP_ID")
    private Integer memberGroupId;

    @Column(name = "MEMBER_NAME_FAMILY")
    private String memberNameFamily;

    @Column(name = "MARKETING_ID")
    private Integer marketingId;

    @Column(name = "ACTIVE_DATE")
    private Date activeDate;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "CORE_MEMBER_ID")
    private Integer coreMemberId;

    @Column(name = "MEMBER_TRANSFER_ID")
    private Integer memberTransferId;

    @Column(name = "VISA_AT_ISSUE")
    private String visaAtIssue;

    @Column(name = "VISA_ISSUE_DATE")
    private Date visaIssueDate;

    @Column(name = "MEMBER_BLOCK_SERVICES_STATUS")
    private String memberBlockServicesStatus;

    @Column(name = "ATTACH_FILE1")
    private String attachFile1;

    @Column(name = "ATTACH_FILE2")
    private String attachFile2;

    @Column(name = "ATTACH_FILE3")
    private String attachFile3;

    @Column(name = "HOBBY_ID")
    private Integer hobbyId;

    @Column(name = "INCOME_ID")
    private Integer incomeId;

    @Column(name = "ALLERGIC")
    private String ALLERGIC;

    @Column(name = "HOBBY")
    private String HOBBY;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "EPA_MEMBER_REMARK")
    private String epaMemberRemark;

    @Column(name = "CALLCENTER_MEMBER_REMARK")
    private String callcenterMemberRemark;

    @Column(name = "MEMBER_BLOCK_SERVICES_REMARK")
    private String memberBlockServicesRemark;

}
