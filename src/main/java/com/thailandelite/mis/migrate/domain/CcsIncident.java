package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_INCIDENT")
public class CcsIncident implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "INCIDENT_ID", nullable = false)
    private Integer incidentId;

    @Column(name = "AGENT_TYPE_ID")
    private Integer agentTypeId;

    @Column(name = "REGISTER_TYPE_ID")
    private Integer registerTypeId;

    @Column(name = "JOB_ASSIGNMENT_ID")
    private Integer jobAssignmentId;

    @Column(name = "AGENT_ON_INCIDENT_ID")
    private Integer agentOnIncidentId;

    @Column(name = "CAUSE_AGENT_ID")
    private Integer causeAgentId;

    @Column(name = "CAUSE_AGENT_HOW_TO1")
    private String causeAgentHowTo1;

    @Column(name = "CAUSE_AGENT_HOW_TO2")
    private String causeAgentHowTo2;

    @Column(name = "CAUSE_AGENT_HOW_TO3")
    private String causeAgentHowTo3;

    @Column(name = "DEPARTMENT_ID")
    private Integer departmentId;

    @Column(name = "SUPERVIOR_ID")
    private Integer superviorId;

    @Column(name = "MS_ID")
    private Integer msId;

    @Column(name = "CONCLUSION")
    private String CONCLUSION;

    @Column(name = "NAME_INVOLVED")
    private String nameInvolved;

    @Column(name = "CRITERIA_OF_EVALUATION")
    private String criteriaOfEvaluation;

    @Column(name = "REASON")
    private String REASON;

    @Column(name = "SCORE")
    private String SCORE;

    @Column(name = "SCORE_RANKING")
    private String scoreRanking;

    @Column(name = "MANAGER_ID")
    private Integer managerId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "TIME1")
    private LocalTime TIME1;

    @Column(name = "TIME2")
    private LocalTime TIME2;

    @Column(name = "TIME3")
    private LocalTime TIME3;

    @Column(name = "SEVERITY_STATUS")
    private String severityStatus;

    @Column(name = "PRIORITY_STATUS")
    private String priorityStatus;

    @Column(name = "INCIDENT_CODE")
    private String incidentCode;

    @Column(name = "SUP_CREATE_DATE")
    private Date supCreateDate;

    @Column(name = "CAUSE_AGENT_DATE")
    private Date causeAgentDate;

    @Column(name = "CAUSE_AGENT_DEPARTMENT_ID")
    private Integer causeAgentDepartmentId;

    @Column(name = "SUP_DEPARTMENT_ID")
    private Integer supDepartmentId;

    @Column(name = "INCIDENT_FILE_1")
    private String incidentFile1;

    @Column(name = "INCIDENT_PATH")
    private String incidentPath;

    @Column(name = "INCIDENT_FILE_2")
    private String incidentFile2;

    @Column(name = "INCIDENT_FILE_3")
    private String incidentFile3;

    @Column(name = "INCIDENT_FILE_4")
    private String incidentFile4;

    @Column(name = "INCIDENT_FILE_5")
    private String incidentFile5;

    @Column(name = "JOB_ASSIGNMENT_ID_OTHER")
    private String jobAssignmentIdOther;

    @Column(name = "INCIDENT_STATUS")
    private String incidentStatus;

    @Column(name = "CONCLUSION_STATUS")
    private String conclusionStatus;

    @Column(name = "MEMBER_ID")
    private Integer memberId;

    @Column(name = "DETAIL1")
    private String DETAIL1;

    @Column(name = "DETAIL2")
    private String DETAIL2;

    @Column(name = "DETAIL3")
    private String DETAIL3;

    @Column(name = "CAUSE_AGENT_DETAIL1")
    private String causeAgentDetail1;

    @Column(name = "CAUSE_AGENT_DETAIL2")
    private String causeAgentDetail2;

    @Column(name = "CAUSE_AGENT_DETAIL3")
    private String causeAgentDetail3;

    @Column(name = "SUPERVIOR_COMMENT")
    private String superviorComment;

    @Column(name = "MS_DETAIL")
    private String msDetail;

    @Column(name = "MANAGER_DETAIL")
    private String managerDetail;

    @Column(name = "ACKNOWLEDGEMENT_DETAIL")
    private String acknowledgementDetail;

    @Column(name = "REMARK")
    private String REMARK;

}
