package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_APPLICATION_PROMOTION")
public class SmmApplicationPromotion implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "APPLICATION_PROMOTION_ID", nullable = false)
    private Integer applicationPromotionId;

    @Column(name = "APPLICATION_ID", nullable = false)
    private Integer applicationId;

    @Column(name = "PROMOTION_ID", nullable = false)
    private Integer promotionId;

    @Column(name = "PROMOTION_CODE")
    private String promotionCode;

    @Column(name = "PROMOTION_NAME")
    private String promotionName;

    @Column(name = "DISCOUNT_PERCENT")
    private Integer discountPercent;

    @Column(name = "DISCOUNT_MAX")
    private BigDecimal discountMax;

    @Column(name = "DISCOUNT_AMOUNT")
    private BigDecimal discountAmount;

    @Column(name = "PROMOTION_TYPE")
    private String promotionType;

    @Column(name = "PROMOTION_DATE_FROM")
    private Date promotionDateFrom;

    @Column(name = "PROMOTION_DATE_TO")
    private Date promotionDateTo;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PROMOTION_DETAIL")
    private String promotionDetail;

    @Column(name = "REMARK")
    private String REMARK;

}
