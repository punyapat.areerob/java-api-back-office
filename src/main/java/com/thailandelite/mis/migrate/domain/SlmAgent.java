package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SLM_AGENT")
public class SlmAgent implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "AGENT_ID", nullable = false)
    private Integer agentId;

    @Column(name = "AGENT_CODE")
    private String agentCode;

    @Column(name = "SALES_AGENT_TH")
    private String salesAgentTh;

    @Column(name = "SALES_AGENT_EN")
    private String salesAgentEn;

    @Column(name = "SALES_CONTACT_TH")
    private String salesContactTh;

    @Column(name = "SALES_CONTACT_EN")
    private String salesContactEn;

    @Column(name = "AGENT_TYPE_ID")
    private Integer agentTypeId;

    @Column(name = "TERRITORY")
    private String TERRITORY;

    @Column(name = "CONTRACT_NO")
    private String contractNo;

    @Column(name = "CONTRACT_DATE_START")
    private Date contractDateStart;

    @Column(name = "CONTRACT_DATE_END")
    private Date contractDateEnd;

    @Column(name = "NO_STREET")
    private String noStreet;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "STATE_ID_bk")
    private Integer stateIdBk;

    @Column(name = "STATE_ID")
    private Integer stateId;

    @Column(name = "CITY_ID")
    private Integer cityId;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "AGENT_PHONE")
    private String agentPhone;

    @Column(name = "AGENT_FAX")
    private String agentFax;

    @Column(name = "AGENT_WEBSITE")
    private String agentWebsite;

    @Column(name = "AGENT_EMAIL")
    private String agentEmail;

    @Column(name = "AGENT_STATUS")
    private String agentStatus;

    @Column(name = "COMMISSION_RATE_ID")
    private Integer commissionRateId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "AGENT_STATUS_REGISTER")
    private String agentStatusRegister;

    @Column(name = "AGENT_FILE_BOOK_BANK")
    private String agentFileBookBank;

    @Column(name = "AGENT_FILE_PASSPORT")
    private String agentFilePassport;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "COMPANY_PROFILE")
    private String companyProfile;

    @Column(name = "WEB_AGENT_ID")
    private Integer webAgentId;

    @Column(name = "REMARK_AGENT_CODE")
    private String remarkAgentCode;

    @Column(name = "VAT_TYPE")
    private String vatType;

    @Column(name = "COMPANY_REGIS_IN_THAI")
    private String companyRegisInThai;

    @Column(name = "TAX_ID")
    private String taxId;

    @Column(name = "REMARK")
    private String REMARK;

}
