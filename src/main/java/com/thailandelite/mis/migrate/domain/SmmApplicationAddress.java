package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_APPLICATION_ADDRESS")
public class SmmApplicationAddress implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "ADDRESS_ID", nullable = false)
    private Integer addressId;

    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "ADDR_NO")
    private String addrNo;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "STATE_ID_bk")
    private Integer stateIdBk;

    @Column(name = "STATE_ID")
    private Integer stateId;

    @Column(name = "CITY_ID_bk")
    private Integer cityIdBk;

    @Column(name = "CITY_ID")
    private Integer cityId;

    @Column(name = "ADDR_POSTAL_CODE")
    private String addrPostalCode;

    @Column(name = "ADDR_PHONE_NO")
    private String addrPhoneNo;

    @Column(name = "ADDR_FAX_NO")
    private String addrFaxNo;

    @Column(name = "ADDR_EMAIL")
    private String addrEmail;

    @Column(name = "ADDRESS_TYPE_ID")
    private Integer addressTypeId;

    @Column(name = "OPTION_ACTIVE")
    private String optionActive;

    @Column(name = "MAILING_ADDRESS")
    private String mailingAddress;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "id_old")
    private Integer idOld;

    @Column(name = "import_name")
    private String importName;

    @Column(name = "ADDRESS_FILE")
    private String addressFile;

    @Column(name = "ADDRESS_PATH")
    private String addressPath;

    @Column(name = "REMARK")
    private String REMARK;

}
