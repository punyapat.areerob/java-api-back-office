package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_SERVICES")
public class MasServices implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "SERVICES_ID", nullable = false)
    private Integer servicesId;

    @Column(name = "SERVICES_CODE")
    private String servicesCode;

    @Column(name = "SERVICES_TYPE")
    private String servicesType;

    @Column(name = "SERVICES_NAME")
    private String servicesName;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "SERVICES_CODE_OLD")
    private Integer servicesCodeOld;

    @Column(name = "EPA_SERVICE_CODE")
    private String epaServiceCode;

    @Column(name = "EPA_SERVICE_NAME")
    private String epaServiceName;

    @Column(name = "AOT_REPORT_ORDER")
    private Integer aotReportOrder;

    @Column(name = "AOT_REPORT_STATUS")
    private String aotReportStatus;

    @Column(name = "GR_PROCESS_STATUS")
    private String grProcessStatus;

    @Column(name = "SHOW_MEMBER_EXPIRE")
    private String showMemberExpire;

    @Column(name = "PUBLICE_STATUS")
    private String publiceStatus;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "SERVICES_CONFIRM_LETTER")
    private String servicesConfirmLetter;

}
