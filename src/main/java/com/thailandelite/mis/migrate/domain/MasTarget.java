package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_TARGET")
public class MasTarget implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "TARGET_ID", nullable = false)
    private Integer targetId;

    @Column(name = "BUD_YEAR")
    private Integer budYear;

    @Column(name = "MONTH")
    private String MONTH;

    @Column(name = "เป้ายอดขายTPC")
    private BigDecimal เป้ายอดขายTPC;

    @Column(name = "เป้ายอดคนTPC")
    private Integer เป้ายอดคนTPC;

    @Column(name = "เป้ายอดขายHP")
    private BigDecimal เป้ายอดขายHP;

    @Column(name = "เป้ายอดคนHP")
    private Integer เป้ายอดคนHP;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

}
