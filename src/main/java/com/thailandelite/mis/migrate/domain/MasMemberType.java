package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_MEMBER_TYPE")
public class MasMemberType implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "MEMBER_TYPE_ID", nullable = false)
    private Integer memberTypeId;

    @Column(name = "MEMBER_TYPE_CODE")
    private String memberTypeCode;

    @Column(name = "MEMBER_TYPE_NAME")
    private String memberTypeName;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

}
