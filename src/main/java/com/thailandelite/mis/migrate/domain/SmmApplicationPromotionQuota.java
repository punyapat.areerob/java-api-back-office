package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_APPLICATION_PROMOTION_QUOTA")
public class SmmApplicationPromotionQuota implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "APPLICATION_PROMOTION_QUOTA_ID", nullable = false)
    private Integer applicationPromotionQuotaId;

    @Column(name = "APPLICATION_PROMOTION_ID", nullable = false)
    private Integer applicationPromotionId;

    @Column(name = "APPLICATION_ID", nullable = false)
    private Integer applicationId;

    @Column(name = "PROMOTION_QUOTA_ID", nullable = false)
    private Integer promotionQuotaId;

    @Column(name = "PROMOTION_ID", nullable = false)
    private Integer promotionId;

    @Column(name = "PRIVILEGE_ID", nullable = false)
    private Integer privilegeId;

    @Column(name = "QUOTA_QUANTITY")
    private Integer quotaQuantity;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "REMARK")
    private String REMARK;

}
