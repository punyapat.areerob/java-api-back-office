package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_STATE")
public class MasState implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "STATE_ID", nullable = false)
    private Integer stateId;

    @Column(name = "COUNTRY_ID")
    private Integer countryId;

    @Column(name = "STATE_CODE")
    private String stateCode;

    @Column(name = "STATE_CODE_old")
    private String stateCodeOld;

    @Column(name = "STATE_NAME_TH")
    private String stateNameTh;

    @Column(name = "STATE_NAME_EN")
    private String stateNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PROVINCE_ID")
    private String provinceId;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "STATE_ID_old")
    private Integer stateIdOld;

    @Column(name = "REMARK")
    private String REMARK;

}
