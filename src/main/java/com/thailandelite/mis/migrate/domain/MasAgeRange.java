package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
@Table(name = "MAS_AGE_RANGE")
public class MasAgeRange implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "AGE_RANGE_CODE")
    private Integer ageRangeCode;

    @Column(name = "AGE_RANGE_MIN")
    private Integer ageRangeMin;

    @Column(name = "AGE_RANGE_MAX")
    private Integer ageRangeMax;

}
