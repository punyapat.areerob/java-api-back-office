package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_PRODUCT_VENDER")
public class MasProductVender implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PRODUCT_VENDER_ID", nullable = false)
    private Integer productVenderId;

    @Column(name = "VENDER_ID")
    private Integer venderId;

    @Column(name = "PRODUCT_CODE")
    private String productCode;

    @Column(name = "PRODUCT_NAME_TH")
    private String productNameTh;

    @Column(name = "PRODUCT_NAME_EN")
    private String productNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRICE")
    private BigDecimal PRICE;

    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Column(name = "VENDOR_ID_old")
    private String vendorIdOld;

    @Column(name = "PRODUCT_CODE_bk")
    private String productCodeBk;

    @Column(name = "SERVICE_TYPE_CODE_old")
    private String serviceTypeCodeOld;

    @Column(name = "NORMAL_PRICE")
    private BigDecimal normalPrice;

    @Column(name = "GUEST_PRICE")
    private BigDecimal guestPrice;

    @Column(name = "ACTIVE_STATUS")
    private String activeStatus;

    @Column(name = "PACKAGE_USED")
    private String packageUsed;

    @Column(name = "PACKAGE_USED_BACK")
    private String packageUsedBack;

    @Column(name = "PRIVILEGE_POINT")
    private Integer privilegePoint;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "DESCRIPTION")
    private String DESCRIPTION;

}
