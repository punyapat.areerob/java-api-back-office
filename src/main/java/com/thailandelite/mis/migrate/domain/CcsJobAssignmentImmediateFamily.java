package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_JOB_ASSIGNMENT_IMMEDIATE_FAMILY")
public class CcsJobAssignmentImmediateFamily implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "JOB_ASSIGNMENT_IMMEDIATE_FAMILY_ID", nullable = false)
    private Integer jobAssignmentImmediateFamilyId;

    @Column(name = "JOB_ASSIGNMENT_ID")
    private Integer jobAssignmentId;

    @Column(name = "RELATIONSHIP_ID")
    private Integer relationshipId;

    @Column(name = "IMMEDIATE_TYPE")
    private String immediateType;

    @Column(name = "TITLE_ID")
    private Integer titleId;

    @Column(name = "FNAME")
    private String FNAME;

    @Column(name = "MNAME")
    private String MNAME;

    @Column(name = "LNAME")
    private String LNAME;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "JOB_ID")
    private Integer jobId;

    @Column(name = "RELATIONSHIP_NAME_EN")
    private String relationshipNameEn;

    @Column(name = "TITLE_NAME")
    private String titleName;

    @Column(name = "PASSPORT_EXPIRED")
    private Date passportExpired;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "import_name")
    private String importName;

    @Column(name = "REMARK")
    private String REMARK;

}
