package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CCS_JOB_ASSIGNMENT_ITEM")
public class CcsJobAssignmentItem implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "JOB_ASSIGNMENT_ITEM_ID", nullable = false)
    private Integer jobAssignmentItemId;

    @Column(name = "JOB_ASSIGNMENT_ID")
    private Integer jobAssignmentId;

    @Column(name = "FOM_CONFIG_SERVICES_ID")
    private Integer fomConfigServicesId;

    @Column(name = "FORM_ID")
    private Integer formId;

    @Column(name = "SERVICES_ID")
    private Integer servicesId;

    @Column(name = "FORM_TYPE")
    private String formType;

    @Column(name = "FORM_LABEL")
    private String formLabel;

    @Column(name = "FORM_ORDER")
    private Integer formOrder;

    @Column(name = "FORM_COLSPAN")
    private String formColspan;

    @Column(name = "ITEM_VALUE")
    private String itemValue;

    @Column(name = "ITEM_DATA")
    private String itemData;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "JOB_ASSIGNMENT_CODE")
    private String jobAssignmentCode;

    @Column(name = "TICKET_ID_old")
    private String ticketIdOld;

    @Column(name = "RESERVATION_ID")
    private Integer reservationId;

    @Column(name = "CODE_old")
    private String codeOld;

    @Column(name = "NAME_old")
    private String nameOld;

    @Column(name = "SERVICE_TYPE_CODE_old")
    private String serviceTypeCodeOld;

    @Column(name = "check_dup")
    private String checkDup;

    @Column(name = "temp_id")
    private Integer tempId;

    @Column(name = "import_name")
    private String importName;

    @Column(name = "JOB_ASSIGNMENT_ID_old")
    private Integer jobAssignmentIdOld;

    @Column(name = "JOB_ASSIGNMENT_ITEM_ID_old")
    private Integer jobAssignmentItemIdOld;

    @Column(name = "REMARK")
    private String REMARK;

}
