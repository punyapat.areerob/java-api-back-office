package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_NATIONALITY")
public class MasNationality implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "NATIONALITY_ID", nullable = false)
    private Integer nationalityId;

    @Column(name = "NATIONALITY_CODE")
    private String nationalityCode;

    @Column(name = "NATIONALITY_NAME_TH")
    private String nationalityNameTh;

    @Column(name = "NATIONALITY_NAME_EN")
    private String nationalityNameEn;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "COUNTRY_NAME")
    private String countryName;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "NATIONALITY_PASSPORT_NAME_TH")
    private String nationalityPassportNameTh;

    @Column(name = "NATIONALITY_PASSPORT_NAME_EN")
    private String nationalityPassportNameEn;

    @Column(name = "REMARK")
    private String REMARK;

}
