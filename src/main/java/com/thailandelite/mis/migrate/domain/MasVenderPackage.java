package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_VENDER_PACKAGE")
public class MasVenderPackage implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "VENDER_PACKAGE_ID", nullable = false)
    private Integer venderPackageId;

    @Column(name = "VENDER_ID")
    private Integer venderId;

    @Column(name = "PACKAGE_ID")
    private Integer packageId;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PRODUCT_VENDER_ID")
    private Integer productVenderId;

    @Column(name = "VENDER_PACKAGE_ID_import")
    private Integer venderPackageIdImport;

    @Column(name = "REMARK")
    private String REMARK;

}
