package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MAS_PRIVILEGE")
public class MasPrivilege implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "PRIVILEGE_ID", nullable = false)
    private Integer privilegeId;

    @Column(name = "PRIVILEGE_GROUP_ID")
    private Integer privilegeGroupId;

    @Column(name = "PRIVILEGE_CODE")
    private String privilegeCode;

    @Column(name = "PRIVILEGE_NAME_TH")
    private String privilegeNameTh;

    @Column(name = "PRIVILEGE_NAME_EN")
    private String privilegeNameEn;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "SHOW_IN_ADD_JA")
    private String showInAddJa;

    @Column(name = "REMARK")
    private String REMARK;

}
