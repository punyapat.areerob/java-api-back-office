package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "SMM_APPLICATION")
public class SmmApplication implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "APPLICATION_ID", nullable = false)
    private Integer applicationId;

    @Column(name = "APPLICATION_PICTURE")
    private String applicationPicture;

    @Column(name = "APPLICATION_PATH")
    private String applicationPath;

    @Column(name = "PACKAGE_ID")
    private Integer packageId;

    @Column(name = "APPLICATION_NO")
    private String applicationNo;

    @Column(name = "MEMBERSHIP_NO")
    private String membershipNo;

    @Column(name = "AGENT_ID")
    private Integer agentId;

    @Column(name = "MEMBERSHIP_ID_NO")
    private String membershipIdNo;

    @Column(name = "APPROVAL_NO")
    private String approvalNo;

    @Column(name = "GENDER_ID")
    private String genderId;

    @Column(name = "TITLE_ID")
    private Integer titleId;

    @Column(name = "FNAME")
    private String FNAME;

    @Column(name = "MNAME")
    private String MNAME;

    @Column(name = "LNAME")
    private String LNAME;

    @Column(name = "NICK_NAME")
    private String nickName;

    @Column(name = "NAME_ON_CARD")
    private String nameOnCard;

    @Column(name = "BIRTHDATE")
    private Date BIRTHDATE;

    @Column(name = "BLOOD_TYPE")
    private String bloodType;

    @Column(name = "RELIGION_ID")
    private Integer religionId;

    @Column(name = "NATIONALITY_ID")
    private Integer nationalityId;

    @Column(name = "PASSPORT_NO")
    private String passportNo;

    @Column(name = "PASSPORT_ISSUE_BY")
    private String passportIssueBy;

    @Column(name = "PASSPORT_ISSUE_DATE")
    private Date passportIssueDate;

    @Column(name = "PASSPORT_EXPIRY_DATE")
    private Date passportExpiryDate;

    @Column(name = "OCCUPATION_ID")
    private Integer occupationId;

    @Column(name = "BUSINESS_TITLE")
    private String businessTitle;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "NATURE_OF_BUSINESS_ID")
    private Integer natureOfBusinessId;

    @Column(name = "NATURE_OF_BUSINESS_OTHER")
    private String natureOfBusinessOther;

    @Column(name = "PAYMENT_DATE")
    private Date paymentDate;

    @Column(name = "CARD_ISSUE_DATE")
    private Date cardIssueDate;

    @Column(name = "CARD_EXPIRY_DATE")
    private Date cardExpiryDate;

    @Column(name = "APPLICATION_FILE_PASSPORT")
    private String applicationFilePassport;

    @Column(name = "OCCUPATION_OTHER")
    private String occupationOther;

    @Column(name = "MEMBER_RECEIVE_CARD_STATUS")
    private String memberReceiveCardStatus;

    @Column(name = "WELCOME_PACK_STATUS")
    private String welcomePackStatus;

    @Column(name = "PAYMENT_STATUS")
    private String paymentStatus;

    @Column(name = "APPLICATION_STATUS_CODE")
    private String applicationStatusCode;

    @Column(name = "APPLICATION_FILE_APPLICATION")
    private String applicationFileApplication;

    @Column(name = "APPLICATION_FILE_OTHER")
    private String applicationFileOther;

    @Column(name = "IMMIGRATION_ATTACH_FILE")
    private String immigrationAttachFile;

    @Column(name = "APPLICATION_TYPE")
    private String applicationType;

    @Column(name = "MEMBER_TRANSFER_ID")
    private Integer memberTransferId;

    @Column(name = "MEMBER_TYPE")
    private String memberType;

    @Column(name = "MEMBER_NAME_FAMILY")
    private String memberNameFamily;

    @Column(name = "MARKETING_ID")
    private Integer marketingId;

    @Column(name = "GR_PROCESS_ITEM_ID")
    private Integer grProcessItemId;

    @Column(name = "MEMBER_UPGRAD_ID")
    private Integer memberUpgradId;

    @Column(name = "PAYMENT_ATTACH_FILE")
    private String paymentAttachFile;

    @Column(name = "CORE_MEMBER_ID")
    private Integer coreMemberId;

    @Column(name = "ACTIVE_DATE")
    private Date activeDate;

    @Column(name = "APPROVE_DATE")
    private Date approveDate;

    @Column(name = "PACKAGE_ID_OLD")
    private Integer packageIdOld;

    @Column(name = "PACKAGE_NAME_OLD")
    private String packageNameOld;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

    @Column(name = "PROJECT_ID")
    private Integer projectId;

    @Column(name = "TRANSFER_FILE_1")
    private String transferFile1;

    @Column(name = "TRANSFER_FILE_2")
    private String transferFile2;

    @Column(name = "TRANSFER_FILE_3")
    private String transferFile3;

    @Column(name = "Q_PASSPORT_EXPIRY_DATE")
    private Date qPassportExpiryDate;

    @Column(name = "URGENT_STATUS")
    private String urgentStatus;

    @Column(name = "APPLICATION_BIRTH_CERTIFICATE")
    private String applicationBirthCertificate;

    @Column(name = "APPLICATION_IDENTITY_DOCUMENT")
    private String applicationIdentityDocument;

    @Column(name = "APPLICATION_MARRIAGE_CERTIFICATE")
    private String applicationMarriageCertificate;

    @Column(name = "APPLICATION_PROOF_PAYMENT")
    private String applicationProofPayment;

    @Column(name = "EMAIL")
    private String EMAIL;

    @Column(name = "PACKAGE_TYPE_CODE_old")
    private String packageTypeCodeOld;

    @Column(name = "id_old")
    private Integer idOld;

    @Column(name = "CORE_MEMBER_REMARK")
    private String coreMemberRemark;

    @Column(name = "FINANCE_STATUS")
    private String financeStatus;

    @Column(name = "IMMIGRATION_APPROVE_DATE")
    private Date immigrationApproveDate;

    @Column(name = "GR_DOC_NO")
    private String grDocNo;

    @Column(name = "DOC_NO_TM")
    private String docNoTm;

    @Column(name = "GR_DOC_DATE")
    private Date grDocDate;

    @Column(name = "import_name")
    private String importName;

    @Column(name = "NATIONALITY_PASSPORT_ID")
    private Integer nationalityPassportId;

    @Column(name = "LEGAL_RELATIONSHIP_TYPE")
    private String legalRelationshipType;

    @Column(name = "PAYMENT_RATE")
    private BigDecimal paymentRate;

    @Column(name = "CHANNEL")
    private String CHANNEL;

    @Column(name = "MEMBER_RENEW_ID")
    private Integer memberRenewId;

    @Column(name = "EARLY_BIRD")
    private String earlyBird;

    @Column(name = "ACTIVATE_DATE")
    private Date activateDate;

    @Column(name = "COUNTRY_OF_BIRTH")
    private String countryOfBirth;

    @Column(name = "Q_STAY_IN_THAI_PAST_3_YEAR")
    private String qStayInThaiPast3Year;

    @Column(name = "Q_STAY_IN_THAI_TIMES")
    private String qStayInThaiTimes;

    @Column(name = "ACCOMMODATION_IN_THAI_TYPE")
    private String accommodationInThaiType;

    @Column(name = "ACCOMMODATION_IN_THAI_OTHER")
    private String accommodationInThaiOther;

    @Column(name = "ALLERGIC")
    private String ALLERGIC;

    @Column(name = "IMMIGRATION_ATTACH_FILE_REMARK")
    private String immigrationAttachFileRemark;

    @Column(name = "PAYMENT_ATTACH_FILE_REMARK")
    private String paymentAttachFileRemark;

    @Column(name = "REMARK")
    private String REMARK;

    @Column(name = "Q_VISA_CURRENT")
    private String qVisaCurrent;

    @Column(name = "Q_PASSPORT_LEAST3_EMPTY_PAGE")
    private String qPassportLeast3EmptyPage;

    @Column(name = "Q_MEMBERSHIP_FEE_PAID_FROM")
    private String qMembershipFeePaidFrom;

    @Column(name = "Q_AFFIX_ELITE_VISA")
    private String qAffixEliteVisa;

    @Column(name = "Q_YOUR_LATEST")
    private String qYourLatest;

    @Column(name = "Q_KNOW_US")
    private String qKnowUs;

    @Column(name = "REMARK_EDIT")
    private String remarkEdit;

}
