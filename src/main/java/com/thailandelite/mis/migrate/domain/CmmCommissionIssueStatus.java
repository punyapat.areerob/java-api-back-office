package com.thailandelite.mis.migrate.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CMM_COMMISSION_ISSUE_STATUS")
public class CmmCommissionIssueStatus implements Serializable {

    private static final long serialVersionUID = 1L;
  @Id

    @Column(name = "COMMISSION_ISSUE_STATUS_ID", nullable = false)
    private Integer commissionIssueStatusId;

    @Column(name = "COMMISSION_ISSUE_STATUS_CODE", nullable = false)
    private String commissionIssueStatusCode;

    @Column(name = "COMMISSION_ISSUE_STATUS_NAME_TH")
    private String commissionIssueStatusNameTh;

    @Column(name = "COMMISSION_ISSUE_STATUS_NAME_EN")
    private String commissionIssueStatusNameEn;

    @Column(name = "COMMISSION_ISSUE_STATUS_ORDER", nullable = false)
    private Integer commissionIssueStatusOrder;

    @Column(name = "CANCEL_COMMISSION_ISSUE_STATUS_ID")
    private Integer cancelCommissionIssueStatusId;

    @Column(name = "NEXT_COMMISSION_ISSUE_STATUS_ID")
    private Integer nextCommissionIssueStatusId;

    @Column(name = "COMMISSION_ISSUE_BUTTON_NEXT_STATUS_NAME_TH")
    private String commissionIssueButtonNextStatusNameTh;

    @Column(name = "COMMISSION_ISSUE_BUTTON_NEXT_STATUS_NAME_EN")
    private String commissionIssueButtonNextStatusNameEn;

    @Column(name = "COMMISSION_ISSUE_BUTTON_CANCEL_STATUS_NAME_TH")
    private String commissionIssueButtonCancelStatusNameTh;

    @Column(name = "COMMISSION_ISSUE_BUTTON_CANCEL_STATUS_NAME_EN")
    private String commissionIssueButtonCancelStatusNameEn;

    @Column(name = "EDITABLE")
    private String EDITABLE;

    @Column(name = "RECORD_STATUS")
    private String recordStatus;

    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CREATE_USER")
    private String createUser;

    @Column(name = "CREATE_USER_TYPE")
    private String createUserType;

    @Column(name = "LAST_DATE")
    private Date lastDate;

    @Column(name = "LAST_USER")
    private String lastUser;

    @Column(name = "LAST_USER_TYPE")
    private String lastUserType;

}
