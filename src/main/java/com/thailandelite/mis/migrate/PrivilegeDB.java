package com.thailandelite.mis.migrate;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class PrivilegeDB {

    private String PRIVILEGE_ID;
    private String PRIVILEGE_NAME_EN;
    private String PRIVILEGE_GROUP_ID;
    private String PRIVILEGE_GROUP_NAME_EN;
    private String PRIVILEGE_CODE;
    private String PRIVILEGE_NAME_TH;
    private String REMARK;
    private String RECORD_STATUS;
    private ZonedDateTime CREATE_DATE;
    private String CREATE_USER;
    private String CREATE_USER_TYPE;
    private ZonedDateTime LAST_DATE;
    private String LAST_USER;
    private String LAST_USER_TYPE;
    private String SHOW_IN_ADD_JA;
}
