package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPackageUsed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPackageUsedRepository extends JpaRepository<MasPackageUsed, Void>, JpaSpecificationExecutor<MasPackageUsed> {

}
