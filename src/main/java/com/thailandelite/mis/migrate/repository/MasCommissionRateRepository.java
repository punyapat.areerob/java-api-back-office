package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasCommissionRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasCommissionRateRepository extends JpaRepository<MasCommissionRate, Void>, JpaSpecificationExecutor<MasCommissionRate> {

}
