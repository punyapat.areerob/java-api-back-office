package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberPrivilegeQuotaAdjust;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberPrivilegeQuotaAdjustRepository extends JpaRepository<SmmMemberPrivilegeQuotaAdjust, Void>, JpaSpecificationExecutor<SmmMemberPrivilegeQuotaAdjust> {

}
