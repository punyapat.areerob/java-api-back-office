package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPhoneType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPhoneTypeRepository extends JpaRepository<MasPhoneType, Void>, JpaSpecificationExecutor<MasPhoneType> {

}
