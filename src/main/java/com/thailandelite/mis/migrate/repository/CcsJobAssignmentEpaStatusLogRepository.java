package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentEpaStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentEpaStatusLogRepository extends JpaRepository<CcsJobAssignmentEpaStatusLog, Void>, JpaSpecificationExecutor<CcsJobAssignmentEpaStatusLog> {

}
