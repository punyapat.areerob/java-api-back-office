package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasReligion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasReligionRepository extends JpaRepository<MasReligion, Void>, JpaSpecificationExecutor<MasReligion> {

}
