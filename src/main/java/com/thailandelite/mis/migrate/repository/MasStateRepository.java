package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasStateRepository extends JpaRepository<MasState, Void>, JpaSpecificationExecutor<MasState> {

}
