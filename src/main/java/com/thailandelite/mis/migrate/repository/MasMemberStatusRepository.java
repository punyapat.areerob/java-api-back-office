package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasMemberStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasMemberStatusRepository extends JpaRepository<MasMemberStatus, Void>, JpaSpecificationExecutor<MasMemberStatus> {

}
