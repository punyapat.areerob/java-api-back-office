package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAnnualFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAnnualFeeRepository extends JpaRepository<MasAnnualFee, Integer>, JpaSpecificationExecutor<MasAnnualFee> {

}
