package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationAddressRepository extends JpaRepository<SmmApplicationAddress, Void>, JpaSpecificationExecutor<SmmApplicationAddress> {

}
