package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVendorStatusGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVendorStatusGroupRepository extends JpaRepository<MasVendorStatusGroup, Void>, JpaSpecificationExecutor<MasVendorStatusGroup> {

}
