package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationStatusLogRepository extends JpaRepository<SmmApplicationStatusLog, Void>, JpaSpecificationExecutor<SmmApplicationStatusLog> {

}
