package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsReservationStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsReservationStatusLogRepository extends JpaRepository<CcsReservationStatusLog, Void>, JpaSpecificationExecutor<CcsReservationStatusLog> {

}
