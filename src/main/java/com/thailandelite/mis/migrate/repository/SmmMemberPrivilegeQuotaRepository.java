package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberPrivilegeQuota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SmmMemberPrivilegeQuotaRepository extends JpaRepository<SmmMemberPrivilegeQuota, Integer>, JpaSpecificationExecutor<SmmMemberPrivilegeQuota> {
    List<SmmMemberPrivilegeQuota> findAllByMemberGroupId(Integer groupId);
}
