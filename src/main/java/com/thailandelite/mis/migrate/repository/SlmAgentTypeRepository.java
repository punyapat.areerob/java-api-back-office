package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentTypeRepository extends JpaRepository<SlmAgentType, Void>, JpaSpecificationExecutor<SlmAgentType> {

}
