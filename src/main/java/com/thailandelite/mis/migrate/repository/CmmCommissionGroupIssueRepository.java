package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CmmCommissionGroupIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmmCommissionGroupIssueRepository extends JpaRepository<CmmCommissionGroupIssue, Void>, JpaSpecificationExecutor<CmmCommissionGroupIssue> {

}
