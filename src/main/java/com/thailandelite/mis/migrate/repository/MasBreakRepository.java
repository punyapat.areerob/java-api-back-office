package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasBreak;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasBreakRepository extends JpaRepository<MasBreak, Void>, JpaSpecificationExecutor<MasBreak> {

}
