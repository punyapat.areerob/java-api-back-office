package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasOccupation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasOccupationRepository extends JpaRepository<MasOccupation, Void>, JpaSpecificationExecutor<MasOccupation> {

}
