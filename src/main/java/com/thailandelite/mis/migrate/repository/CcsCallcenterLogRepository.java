package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsCallcenterLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsCallcenterLogRepository extends JpaRepository<CcsCallcenterLog, Void>, JpaSpecificationExecutor<CcsCallcenterLog> {

}
