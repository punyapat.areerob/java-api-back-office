package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasNotificationConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasNotificationConfigRepository extends JpaRepository<MasNotificationConfig, Void>, JpaSpecificationExecutor<MasNotificationConfig> {

}
