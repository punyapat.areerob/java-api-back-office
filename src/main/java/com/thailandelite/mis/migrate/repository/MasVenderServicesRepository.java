package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVenderServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVenderServicesRepository extends JpaRepository<MasVenderServices, Void>, JpaSpecificationExecutor<MasVenderServices> {

}
