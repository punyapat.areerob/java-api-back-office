package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasProductVenderPriceMonth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasProductVenderPriceMonthRepository extends JpaRepository<MasProductVenderPriceMonth, Void>, JpaSpecificationExecutor<MasProductVenderPriceMonth> {

}
