package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAgeRepository extends JpaRepository<MasAge, Integer>, JpaSpecificationExecutor<MasAge> {

}
