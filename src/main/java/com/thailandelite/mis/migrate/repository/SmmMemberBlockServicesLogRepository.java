package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberBlockServicesLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberBlockServicesLogRepository extends JpaRepository<SmmMemberBlockServicesLog, Void>, JpaSpecificationExecutor<SmmMemberBlockServicesLog> {

}
