package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVisa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVisaRepository extends JpaRepository<MasVisa, Void>, JpaSpecificationExecutor<MasVisa> {

}
