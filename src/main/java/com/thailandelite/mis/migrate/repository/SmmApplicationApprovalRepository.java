package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationApproval;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationApprovalRepository extends JpaRepository<SmmApplicationApproval, Void>, JpaSpecificationExecutor<SmmApplicationApproval> {

}
