package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasNationality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasNationalityRepository extends JpaRepository<MasNationality, Integer>, JpaSpecificationExecutor<MasNationality> {

}
