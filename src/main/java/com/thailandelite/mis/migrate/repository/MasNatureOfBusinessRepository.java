package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasNatureOfBusiness;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasNatureOfBusinessRepository extends JpaRepository<MasNatureOfBusiness, Void>, JpaSpecificationExecutor<MasNatureOfBusiness> {

}
