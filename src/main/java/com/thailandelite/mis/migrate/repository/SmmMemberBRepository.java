package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberBRepository extends JpaRepository<SmmMemberB, Void>, JpaSpecificationExecutor<SmmMemberB> {

}
