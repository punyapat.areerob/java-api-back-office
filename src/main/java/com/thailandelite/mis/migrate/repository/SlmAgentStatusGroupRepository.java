package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgentStatusGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentStatusGroupRepository extends JpaRepository<SlmAgentStatusGroup, Void>, JpaSpecificationExecutor<SlmAgentStatusGroup> {

}
