package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgentContractRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentContractRateRepository extends JpaRepository<SlmAgentContractRate, Void>, JpaSpecificationExecutor<SlmAgentContractRate> {

}
