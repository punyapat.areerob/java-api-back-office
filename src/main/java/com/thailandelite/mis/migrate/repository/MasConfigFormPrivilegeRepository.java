package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasConfigFormPrivilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasConfigFormPrivilegeRepository extends JpaRepository<MasConfigFormPrivilege, Void>, JpaSpecificationExecutor<MasConfigFormPrivilege> {

}
