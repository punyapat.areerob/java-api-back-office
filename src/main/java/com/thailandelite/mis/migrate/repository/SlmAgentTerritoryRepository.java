package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgentTerritory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentTerritoryRepository extends JpaRepository<SlmAgentTerritory, Void>, JpaSpecificationExecutor<SlmAgentTerritory> {

}
