package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CmmCommissionIssueStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmmCommissionIssueStatusRepository extends JpaRepository<CmmCommissionIssueStatus, Void>, JpaSpecificationExecutor<CmmCommissionIssueStatus> {

}
