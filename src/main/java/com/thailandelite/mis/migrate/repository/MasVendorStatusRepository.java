package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVendorStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVendorStatusRepository extends JpaRepository<MasVendorStatus, Void>, JpaSpecificationExecutor<MasVendorStatus> {

}
