package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmFamilyRepository extends JpaRepository<SmmFamily, Void>, JpaSpecificationExecutor<SmmFamily> {

}
