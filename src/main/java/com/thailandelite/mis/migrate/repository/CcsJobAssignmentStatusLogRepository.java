package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentStatusLogRepository extends JpaRepository<CcsJobAssignmentStatusLog, Void>, JpaSpecificationExecutor<CcsJobAssignmentStatusLog> {

}
