package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAgeRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAgeRangeRepository extends JpaRepository<MasAgeRange, Void>, JpaSpecificationExecutor<MasAgeRange> {

}
