package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentImmediateFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentImmediateFamilyRepository extends JpaRepository<CcsJobAssignmentImmediateFamily, Void>, JpaSpecificationExecutor<CcsJobAssignmentImmediateFamily> {

}
