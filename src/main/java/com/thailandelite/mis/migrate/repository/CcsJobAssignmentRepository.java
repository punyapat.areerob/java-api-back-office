package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentRepository extends JpaRepository<CcsJobAssignment, Void>, JpaSpecificationExecutor<CcsJobAssignment> {

}
