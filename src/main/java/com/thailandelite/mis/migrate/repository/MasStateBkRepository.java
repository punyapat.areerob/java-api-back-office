package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasStateBk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasStateBkRepository extends JpaRepository<MasStateBk, Void>, JpaSpecificationExecutor<MasStateBk> {

}
