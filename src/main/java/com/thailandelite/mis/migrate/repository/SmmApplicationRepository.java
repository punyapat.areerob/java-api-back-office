package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationRepository extends JpaRepository<SmmApplication, Void>, JpaSpecificationExecutor<SmmApplication> {

}
