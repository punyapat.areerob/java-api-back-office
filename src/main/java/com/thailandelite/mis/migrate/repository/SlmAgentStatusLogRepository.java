package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgentStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentStatusLogRepository extends JpaRepository<SlmAgentStatusLog, Void>, JpaSpecificationExecutor<SlmAgentStatusLog> {

}
