package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsPrivilegePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsPrivilegePermissionRepository extends JpaRepository<CcsPrivilegePermission, Void>, JpaSpecificationExecutor<CcsPrivilegePermission> {

}
