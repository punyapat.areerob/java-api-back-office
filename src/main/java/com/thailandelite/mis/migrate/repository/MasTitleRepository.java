package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasTitle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasTitleRepository extends JpaRepository<MasTitle, Void>, JpaSpecificationExecutor<MasTitle> {

}
