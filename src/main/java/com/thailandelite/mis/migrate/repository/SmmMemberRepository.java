package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberRepository extends JpaRepository<SmmMember, Void>, JpaSpecificationExecutor<SmmMember> {
    Page<SmmMember> findAllByMemberStatusAndRecordStatus(String status, String recordStatus, Pageable pageable);
}
