package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberSignature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberSignatureRepository extends JpaRepository<SmmMemberSignature, Void>, JpaSpecificationExecutor<SmmMemberSignature> {

}
