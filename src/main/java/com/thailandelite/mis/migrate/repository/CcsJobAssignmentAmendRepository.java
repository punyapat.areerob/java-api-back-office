package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentAmend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentAmendRepository extends JpaRepository<CcsJobAssignmentAmend, Void>, JpaSpecificationExecutor<CcsJobAssignmentAmend> {

}
