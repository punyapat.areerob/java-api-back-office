package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasTransferFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasTransferFeeRepository extends JpaRepository<MasTransferFee, Integer>, JpaSpecificationExecutor<MasTransferFee> {

}
