package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAirport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAirportRepository extends JpaRepository<MasAirport, Void>, JpaSpecificationExecutor<MasAirport> {

}
