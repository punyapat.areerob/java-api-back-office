package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentLogRepository extends JpaRepository<CcsJobAssignmentLog, Void>, JpaSpecificationExecutor<CcsJobAssignmentLog> {

}
