package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberCampaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberCampaignRepository extends JpaRepository<SmmMemberCampaign, Void>, JpaSpecificationExecutor<SmmMemberCampaign> {

}
