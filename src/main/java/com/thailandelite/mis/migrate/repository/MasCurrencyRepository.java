package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasCurrencyRepository extends JpaRepository<MasCurrency, Void>, JpaSpecificationExecutor<MasCurrency> {

}
