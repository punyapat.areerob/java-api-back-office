package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgentContract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentContractRepository extends JpaRepository<SlmAgentContract, Void>, JpaSpecificationExecutor<SlmAgentContract> {

}
