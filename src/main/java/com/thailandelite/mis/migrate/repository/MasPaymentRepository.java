package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPaymentRepository extends JpaRepository<MasPayment, Void>, JpaSpecificationExecutor<MasPayment> {

}
