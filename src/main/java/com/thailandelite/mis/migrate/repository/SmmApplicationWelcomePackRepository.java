package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationWelcomePack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationWelcomePackRepository extends JpaRepository<SmmApplicationWelcomePack, Void>, JpaSpecificationExecutor<SmmApplicationWelcomePack> {

}
