package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasProductVenderPriceGender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasProductVenderPriceGenderRepository extends JpaRepository<MasProductVenderPriceGender, Void>, JpaSpecificationExecutor<MasProductVenderPriceGender> {

}
