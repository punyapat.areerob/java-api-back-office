package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasRestaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasRestaurantRepository extends JpaRepository<MasRestaurant, Void>, JpaSpecificationExecutor<MasRestaurant> {

}
