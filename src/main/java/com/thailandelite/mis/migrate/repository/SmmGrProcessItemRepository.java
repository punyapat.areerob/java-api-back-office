package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmGrProcessItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmGrProcessItemRepository extends JpaRepository<SmmGrProcessItem, Void>, JpaSpecificationExecutor<SmmGrProcessItem> {

}
