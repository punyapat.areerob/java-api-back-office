package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CmmCommissionIssueItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmmCommissionIssueItemRepository extends JpaRepository<CmmCommissionIssueItem, Void>, JpaSpecificationExecutor<CmmCommissionIssueItem> {

}
