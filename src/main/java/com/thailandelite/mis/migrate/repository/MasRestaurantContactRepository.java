package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasRestaurantContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasRestaurantContactRepository extends JpaRepository<MasRestaurantContact, Void>, JpaSpecificationExecutor<MasRestaurantContact> {

}
