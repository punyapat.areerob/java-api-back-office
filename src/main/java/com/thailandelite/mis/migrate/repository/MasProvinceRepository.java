package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasProvince;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasProvinceRepository extends JpaRepository<MasProvince, Void>, JpaSpecificationExecutor<MasProvince> {

}
