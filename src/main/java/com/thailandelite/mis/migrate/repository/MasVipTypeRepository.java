package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVipType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVipTypeRepository extends JpaRepository<MasVipType, Void>, JpaSpecificationExecutor<MasVipType> {

}
