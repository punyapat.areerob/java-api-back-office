package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationPhoneRepository extends JpaRepository<SmmApplicationPhone, Void>, JpaSpecificationExecutor<SmmApplicationPhone> {

}
