package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberRunning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberRunningRepository extends JpaRepository<SmmMemberRunning, Void>, JpaSpecificationExecutor<SmmMemberRunning> {

}
