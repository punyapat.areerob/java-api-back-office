package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPackageTimes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPackageTimesRepository extends JpaRepository<MasPackageTimes, Integer>, JpaSpecificationExecutor<MasPackageTimes> {

}
