package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMembershipNoRunning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMembershipNoRunningRepository extends JpaRepository<SmmMembershipNoRunning, Void>, JpaSpecificationExecutor<SmmMembershipNoRunning> {

}
