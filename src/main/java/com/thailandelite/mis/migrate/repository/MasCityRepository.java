package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasCityRepository extends JpaRepository<MasCity, Void>, JpaSpecificationExecutor<MasCity> {

}
