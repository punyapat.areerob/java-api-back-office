package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CmmCommissionIssueStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmmCommissionIssueStatusLogRepository extends JpaRepository<CmmCommissionIssueStatusLog, Void>, JpaSpecificationExecutor<CmmCommissionIssueStatusLog> {

}
