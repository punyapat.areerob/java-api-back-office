package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPackageRepository extends JpaRepository<MasPackage, Void>, JpaSpecificationExecutor<MasPackage> {

}
