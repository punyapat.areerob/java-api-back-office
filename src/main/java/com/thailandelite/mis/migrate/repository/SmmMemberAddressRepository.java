package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberAddressRepository extends JpaRepository<SmmMemberAddress, Void>, JpaSpecificationExecutor<SmmMemberAddress> {

}
