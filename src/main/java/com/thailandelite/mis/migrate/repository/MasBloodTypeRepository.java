package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasBloodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasBloodTypeRepository extends JpaRepository<MasBloodType, Void>, JpaSpecificationExecutor<MasBloodType> {

}
