package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationPromotionQuota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationPromotionQuotaRepository extends JpaRepository<SmmApplicationPromotionQuota, Void>, JpaSpecificationExecutor<SmmApplicationPromotionQuota> {

}
