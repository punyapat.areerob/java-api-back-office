package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsReservationStatusGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsReservationStatusGroupRepository extends JpaRepository<CcsReservationStatusGroup, Void>, JpaSpecificationExecutor<CcsReservationStatusGroup> {

}
