package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasCommissionRateItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasCommissionRateItemRepository extends JpaRepository<MasCommissionRateItem, Void>, JpaSpecificationExecutor<MasCommissionRateItem> {

}
