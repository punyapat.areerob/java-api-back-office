package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CmmCommissionIssueStatusGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmmCommissionIssueStatusGroupRepository extends JpaRepository<CmmCommissionIssueStatusGroup, Void>, JpaSpecificationExecutor<CmmCommissionIssueStatusGroup> {

}
