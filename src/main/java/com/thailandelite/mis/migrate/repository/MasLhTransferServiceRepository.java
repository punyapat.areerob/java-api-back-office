package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasLhTransferService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasLhTransferServiceRepository extends JpaRepository<MasLhTransferService, Void>, JpaSpecificationExecutor<MasLhTransferService> {

}
