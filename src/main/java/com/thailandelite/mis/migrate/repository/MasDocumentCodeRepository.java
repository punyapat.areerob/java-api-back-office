package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasDocumentCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasDocumentCodeRepository extends JpaRepository<MasDocumentCode, Void>, JpaSpecificationExecutor<MasDocumentCode> {

}
