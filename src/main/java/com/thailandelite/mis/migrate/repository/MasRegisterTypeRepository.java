package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasRegisterType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasRegisterTypeRepository extends JpaRepository<MasRegisterType, Void>, JpaSpecificationExecutor<MasRegisterType> {

}
