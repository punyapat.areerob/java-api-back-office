package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationStatusRepository extends JpaRepository<SmmApplicationStatus, Void>, JpaSpecificationExecutor<SmmApplicationStatus> {

}
