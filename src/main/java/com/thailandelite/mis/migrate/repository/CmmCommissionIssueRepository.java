package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CmmCommissionIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CmmCommissionIssueRepository extends JpaRepository<CmmCommissionIssue, Void>, JpaSpecificationExecutor<CmmCommissionIssue> {

}
