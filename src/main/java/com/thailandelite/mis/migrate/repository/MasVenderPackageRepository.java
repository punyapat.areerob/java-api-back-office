package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVenderPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVenderPackageRepository extends JpaRepository<MasVenderPackage, Void>, JpaSpecificationExecutor<MasVenderPackage> {

}
