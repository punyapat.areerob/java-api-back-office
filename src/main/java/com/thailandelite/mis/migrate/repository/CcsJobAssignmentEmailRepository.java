package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentEmailRepository extends JpaRepository<CcsJobAssignmentEmail, Void>, JpaSpecificationExecutor<CcsJobAssignmentEmail> {

}
