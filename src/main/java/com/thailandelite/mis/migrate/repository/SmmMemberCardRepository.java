package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberCardRepository extends JpaRepository<SmmMemberCard, Void>, JpaSpecificationExecutor<SmmMemberCard> {

}
