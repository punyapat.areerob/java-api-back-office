package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasProductRepository extends JpaRepository<MasProduct, Void>, JpaSpecificationExecutor<MasProduct> {

}
