package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmSalesSupport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmSalesSupportRepository extends JpaRepository<SmmSalesSupport, Void>, JpaSpecificationExecutor<SmmSalesSupport> {

}
