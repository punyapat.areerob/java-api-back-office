package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SlmAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SlmAgentRepository extends JpaRepository<SlmAgent, Void>, JpaSpecificationExecutor<SlmAgent> {

}
