package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPrivilegeGroupItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface MasPrivilegeGroupItemRepository extends JpaRepository<MasPrivilegeGroupItem, Void>, JpaSpecificationExecutor<MasPrivilegeGroupItem> {
    List<MasPrivilegeGroupItem> findByPrivilegeId(Integer privilegeId);
}
