package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasMembershipTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasMembershipTransferRepository extends JpaRepository<MasMembershipTransfer, Void>, JpaSpecificationExecutor<MasMembershipTransfer> {

}
