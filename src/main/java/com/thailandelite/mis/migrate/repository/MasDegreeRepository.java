package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasDegree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasDegreeRepository extends JpaRepository<MasDegree, Void>, JpaSpecificationExecutor<MasDegree> {

}
