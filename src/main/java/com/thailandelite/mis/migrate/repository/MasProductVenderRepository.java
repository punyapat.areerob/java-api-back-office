package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasProductVender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasProductVenderRepository extends JpaRepository<MasProductVender, Void>, JpaSpecificationExecutor<MasProductVender> {

}
