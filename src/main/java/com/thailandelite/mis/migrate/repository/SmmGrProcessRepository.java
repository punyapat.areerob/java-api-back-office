package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmGrProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmGrProcessRepository extends JpaRepository<SmmGrProcess, Void>, JpaSpecificationExecutor<SmmGrProcess> {

}
