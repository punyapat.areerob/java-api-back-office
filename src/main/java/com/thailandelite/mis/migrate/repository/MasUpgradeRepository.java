package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasUpgrade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasUpgradeRepository extends JpaRepository<MasUpgrade, Integer>, JpaSpecificationExecutor<MasUpgrade> {

}
