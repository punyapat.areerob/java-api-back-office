package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasMembershipVability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasMembershipVabilityRepository extends JpaRepository<MasMembershipVability, Integer>, JpaSpecificationExecutor<MasMembershipVability> {

}
