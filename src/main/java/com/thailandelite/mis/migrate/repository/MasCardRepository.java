package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasCardRepository extends JpaRepository<MasCard, Void>, JpaSpecificationExecutor<MasCard> {

}
