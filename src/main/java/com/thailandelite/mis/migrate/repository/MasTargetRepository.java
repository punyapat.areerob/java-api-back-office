package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasTargetRepository extends JpaRepository<MasTarget, Void>, JpaSpecificationExecutor<MasTarget> {

}
