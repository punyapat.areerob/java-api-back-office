package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVenderStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVenderStatusLogRepository extends JpaRepository<MasVenderStatusLog, Void>, JpaSpecificationExecutor<MasVenderStatusLog> {

}
