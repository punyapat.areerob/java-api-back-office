package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmMemberPassport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmMemberPassportRepository extends JpaRepository<SmmMemberPassport, Void>, JpaSpecificationExecutor<SmmMemberPassport> {

}
