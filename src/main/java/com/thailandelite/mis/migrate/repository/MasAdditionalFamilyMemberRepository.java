package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAdditionalFamilyMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAdditionalFamilyMemberRepository extends JpaRepository<MasAdditionalFamilyMember, Integer>, JpaSpecificationExecutor<MasAdditionalFamilyMember> {

}
