package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVenderContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVenderContactRepository extends JpaRepository<MasVenderContact, Void>, JpaSpecificationExecutor<MasVenderContact> {

}
