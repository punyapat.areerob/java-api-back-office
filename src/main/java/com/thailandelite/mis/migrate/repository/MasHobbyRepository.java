package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasHobby;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasHobbyRepository extends JpaRepository<MasHobby, Void>, JpaSpecificationExecutor<MasHobby> {

}
