package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsCalling;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsCallingRepository extends JpaRepository<CcsCalling, Void>, JpaSpecificationExecutor<CcsCalling> {

}
