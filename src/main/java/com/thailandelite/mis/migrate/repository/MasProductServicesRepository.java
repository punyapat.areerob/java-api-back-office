package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasProductServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasProductServicesRepository extends JpaRepository<MasProductServices, Void>, JpaSpecificationExecutor<MasProductServices> {

}
