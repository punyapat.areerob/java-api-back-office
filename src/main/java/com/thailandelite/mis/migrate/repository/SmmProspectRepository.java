package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmProspect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmProspectRepository extends JpaRepository<SmmProspect, Void>, JpaSpecificationExecutor<SmmProspect> {

}
