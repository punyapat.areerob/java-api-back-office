package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasServicesDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasServicesDepartmentRepository extends JpaRepository<MasServicesDepartment, Void>, JpaSpecificationExecutor<MasServicesDepartment> {

}
