package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.CcsJobAssignmentItemLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CcsJobAssignmentItemLogRepository extends JpaRepository<CcsJobAssignmentItemLog, Void>, JpaSpecificationExecutor<CcsJobAssignmentItemLog> {

}
