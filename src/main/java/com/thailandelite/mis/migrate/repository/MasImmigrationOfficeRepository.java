package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasImmigrationOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasImmigrationOfficeRepository extends JpaRepository<MasImmigrationOffice, Void>, JpaSpecificationExecutor<MasImmigrationOffice> {

}
