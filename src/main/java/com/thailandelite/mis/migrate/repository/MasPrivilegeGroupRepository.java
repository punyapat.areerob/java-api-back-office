package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPrivilegeGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPrivilegeGroupRepository extends JpaRepository<MasPrivilegeGroup, Void>, JpaSpecificationExecutor<MasPrivilegeGroup> {

}
