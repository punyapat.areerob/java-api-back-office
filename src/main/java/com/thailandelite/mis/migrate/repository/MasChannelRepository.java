package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasChannelRepository extends JpaRepository<MasChannel, Void>, JpaSpecificationExecutor<MasChannel> {

}
