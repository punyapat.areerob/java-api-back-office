package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasPackageItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasPackageItemRepository extends JpaRepository<MasPackageItem, Void>, JpaSpecificationExecutor<MasPackageItem> {

}
