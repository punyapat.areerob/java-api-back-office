package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAddressTypeRepository extends JpaRepository<MasAddressType, Void>, JpaSpecificationExecutor<MasAddressType> {

}
