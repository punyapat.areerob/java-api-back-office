package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasFlightClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasFlightClassRepository extends JpaRepository<MasFlightClass, Void>, JpaSpecificationExecutor<MasFlightClass> {

}
