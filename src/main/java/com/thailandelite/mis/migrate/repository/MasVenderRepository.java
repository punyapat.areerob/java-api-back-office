package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasVender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasVenderRepository extends JpaRepository<MasVender, Void>, JpaSpecificationExecutor<MasVender> {

}
