package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasSkillRepository extends JpaRepository<MasSkill, Void>, JpaSpecificationExecutor<MasSkill> {

}
