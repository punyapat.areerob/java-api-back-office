package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasAnnualFeeConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasAnnualFeeConfigRepository extends JpaRepository<MasAnnualFeeConfig, Void>, JpaSpecificationExecutor<MasAnnualFeeConfig> {

}
