package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasMarketing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasMarketingRepository extends JpaRepository<MasMarketing, Void>, JpaSpecificationExecutor<MasMarketing> {

}
