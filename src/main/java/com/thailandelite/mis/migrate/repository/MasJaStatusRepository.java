package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasJaStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasJaStatusRepository extends JpaRepository<MasJaStatus, Void>, JpaSpecificationExecutor<MasJaStatus> {

}
