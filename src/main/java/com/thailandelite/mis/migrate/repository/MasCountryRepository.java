package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasCountryRepository extends JpaRepository<MasCountry, Void>, JpaSpecificationExecutor<MasCountry> {

}
