package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.SmmApplicationStatusGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmmApplicationStatusGroupRepository extends JpaRepository<SmmApplicationStatusGroup, Void>, JpaSpecificationExecutor<SmmApplicationStatusGroup> {

}
