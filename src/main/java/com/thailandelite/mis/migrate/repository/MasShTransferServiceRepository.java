package com.thailandelite.mis.migrate.repository;

import com.thailandelite.mis.migrate.domain.MasShTransferService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MasShTransferServiceRepository extends JpaRepository<MasShTransferService, Void>, JpaSpecificationExecutor<MasShTransferService> {

}
