package com.thailandelite.mis.migrate;

public class MASTERVendorContact {
    private float CONTACT_TYPE_ID;
    private String CONTACT_TYPE_NAME_TH;
    private String CONTACT_TYPE_NAME_EN;
    private String REMARK = null;
    private String RECORD_STATUS;
    private String CREATE_DATE;
    private String CREATE_USER;
    private String CREATE_USER_TYPE;
    private String LAST_DATE;
    private String LAST_USER;
    private String LAST_USER_TYPE;


    // Getter Methods

    public float getCONTACT_TYPE_ID() {
        return CONTACT_TYPE_ID;
    }

    public String getCONTACT_TYPE_NAME_TH() {
        return CONTACT_TYPE_NAME_TH;
    }

    public String getCONTACT_TYPE_NAME_EN() {
        return CONTACT_TYPE_NAME_EN;
    }

    public String getREMARK() {
        return REMARK;
    }

    public String getRECORD_STATUS() {
        return RECORD_STATUS;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public String getCREATE_USER_TYPE() {
        return CREATE_USER_TYPE;
    }

    public String getLAST_DATE() {
        return LAST_DATE;
    }

    public String getLAST_USER() {
        return LAST_USER;
    }

    public String getLAST_USER_TYPE() {
        return LAST_USER_TYPE;
    }

    // Setter Methods

    public void setCONTACT_TYPE_ID(float CONTACT_TYPE_ID) {
        this.CONTACT_TYPE_ID = CONTACT_TYPE_ID;
    }

    public void setCONTACT_TYPE_NAME_TH(String CONTACT_TYPE_NAME_TH) {
        this.CONTACT_TYPE_NAME_TH = CONTACT_TYPE_NAME_TH;
    }

    public void setCONTACT_TYPE_NAME_EN(String CONTACT_TYPE_NAME_EN) {
        this.CONTACT_TYPE_NAME_EN = CONTACT_TYPE_NAME_EN;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public void setRECORD_STATUS(String RECORD_STATUS) {
        this.RECORD_STATUS = RECORD_STATUS;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public void setCREATE_USER_TYPE(String CREATE_USER_TYPE) {
        this.CREATE_USER_TYPE = CREATE_USER_TYPE;
    }

    public void setLAST_DATE(String LAST_DATE) {
        this.LAST_DATE = LAST_DATE;
    }

    public void setLAST_USER(String LAST_USER) {
        this.LAST_USER = LAST_USER;
    }

    public void setLAST_USER_TYPE(String LAST_USER_TYPE) {
        this.LAST_USER_TYPE = LAST_USER_TYPE;
    }
}
