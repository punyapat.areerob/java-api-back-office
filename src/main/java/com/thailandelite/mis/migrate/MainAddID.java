package com.thailandelite.mis.migrate;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class MainAddID {

    public static void main(String[] args) throws IOException {
        String pathname = "/Users/golfdigg05/elite-mis2/src/main/java/com/thailandelite/mis/migrate/domain";
        Collection<File> files = FileUtils.listFiles(new File(pathname), new String[]{"java"}, false);
        for (File file : files) {
            String content = FileUtils.readFileToString(file);
            String tableImport = "javax.persistence.Table;";
            String serialVersionUID = "serialVersionUID = 1L;";

            content = content.replaceFirst(tableImport, tableImport + "\n" + "import javax.persistence.Id;");
            content = content.replaceFirst(serialVersionUID, serialVersionUID + "\n  " + "@Id");
            FileUtils.write(new File("/Users/golfdigg05/java-out/"+ file.getName()), content);
        }
    }
}
