package com.thailandelite.mis.migrate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "MEMBER_ID",
    "MEMBER_PICTURE",
    "MEMBER_PATH",
    "APPLICATION_ID",
    "PACKAGE_ID",
    "APPLICATION_NO",
    "MEMBERSHIP_NO",
    "AGENT_ID",
    "MEMBERSHIP_ID_NO",
    "MEMBERSHIP_ID_NEW",
    "APPROVAL_NO",
    "GENDER_ID",
    "TITLE_ID",
    "FNAME",
    "MNAME",
    "LNAME",
    "NICK_NAME",
    "NAME_ON_CARD",
    "BIRTHDATE",
    "BLOOD_TYPE",
    "RELIGION_ID",
    "ALLERGIC",
    "NATIONALITY_ID",
    "PASSPORT_NO",
    "PASSPORT_ISSUE_BY",
    "PASSPORT_ISSUE_DATE",
    "PASSPORT_EXPIRY_DATE",
    "OCCUPATION_ID",
    "BUSINESS_TITLE",
    "COMPANY_NAME",
    "NATURE_OF_BUSINESS_ID",
    "NATURE_OF_BUSINESS_OTHER",
    "OCCUPATION_OTHER",
    "MEMBER_SINCE",
    "RACE_ID",
    "VISA_NO",
    "VISA_EXPIRY_DATE",
    "EMAIL",
    "MOBILE_IN_HOME_COUNTRY",
    "MOBILE_IN_THAI",
    "DEGREE_ID",
    "REVENUE",
    "BIRTHPLACE",
    "HOBBY",
    "COUNTRY_ID",
    "MEMBER_STATUS",
    "CARD_ISSUE_DATE",
    "CARD_EXPIRY_DATE",
    "REMARK",
    "MEMBER_TYPE",
    "MEMBER_CORE_ID",
    "MEMBER_FILE_APPLICATION",
    "MEMBER_FILE_OTHER",
    "MEMBER_FILE_PASSPORT",
    "MEMBER_GROUP_ID",
    "MEMBER_NAME_FAMILY",
    "MARKETING_ID",
    "ACTIVE_DATE",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "CORE_MEMBER_ID",
    "MEMBER_TRANSFER_ID",
    "EPA_MEMBER_REMARK",
    "CALLCENTER_MEMBER_REMARK",
    "VISA_AT_ISSUE",
    "VISA_ISSUE_DATE",
    "MEMBER_BLOCK_SERVICES_STATUS",
    "MEMBER_BLOCK_SERVICES_REMARK",
    "ATTACH_FILE1",
    "ATTACH_FILE2",
    "ATTACH_FILE3",
    "HOBBY_ID",
    "INCOME_ID",
    "MEMBER_NEXT",
    "MEMBER_BIRTH_CERTIFICATE",
    "MEMBER_MARRIAGE_CERTIFICATE",
    "MEMBER_PROOF_PAYMENT",
    "MEMBER_TYPE_ID",
    "EXPIRY_DATE",
    "IMMIGRATION_ATTACH_FILE",
    "REMARK_EDIT",
    "COMMISSION_PAYMENT_STATUS",
    "MEMBER_UPGRADE_ID",
    "MEMBERSHIP_ID_bk",
    "LEGAL_RELATIONSHIP_TYPE",
    "COVID19_REMEDY_1",
    "EXPIRY_DATE_BEFORE_COVID19",
    "EXPIRY_DATE_AFTER_COVID19"
})
@Data
public class MemberDB {

    @JsonProperty("MEMBER_ID")
    public Integer mEMBERID;
    @JsonProperty("MEMBER_PICTURE")
    public String mEMBERPICTURE;
    @JsonProperty("MEMBER_PATH")
    public String mEMBERPATH;
    @JsonProperty("APPLICATION_ID")
    public Integer aPPLICATIONID;
    @JsonProperty("PACKAGE_ID")
    public Integer pACKAGEID;
    @JsonProperty("APPLICATION_NO")
    public String aPPLICATIONNO;
    @JsonProperty("MEMBERSHIP_NO")
    public String mEMBERSHIPNO;
    @JsonProperty("AGENT_ID")
    public Integer aGENTID;
    @JsonProperty("MEMBERSHIP_ID_NO")
    public String mEMBERSHIPIDNO;
    @JsonProperty("MEMBERSHIP_ID_NEW")
    public Object mEMBERSHIPIDNEW;
    @JsonProperty("APPROVAL_NO")
    public String aPPROVALNO;
    @JsonProperty("GENDER_ID")
    public String gENDERID;
    @JsonProperty("TITLE_ID")
    public Integer tITLEID;
    @JsonProperty("FNAME")
    public String fNAME;
    @JsonProperty("MNAME")
    public String mNAME;
    @JsonProperty("LNAME")
    public String lNAME;
    @JsonProperty("NICK_NAME")
    public Object nICKNAME;
    @JsonProperty("NAME_ON_CARD")
    public String nAMEONCARD;
    @JsonProperty("BIRTHDATE")
    public ZonedDateTime bIRTHDATE;
    @JsonProperty("BLOOD_TYPE")
    public String bLOODTYPE;
    @JsonProperty("RELIGION_ID")
    public String rELIGIONID;
    @JsonProperty("ALLERGIC")
    public String aLLERGIC;
    @JsonProperty("NATIONALITY_ID")
    public Integer nATIONALITYID;
    @JsonProperty("PASSPORT_NO")
    public String pASSPORTNO;
    @JsonProperty("PASSPORT_ISSUE_BY")
    public String pASSPORTISSUEBY;
    @JsonProperty("PASSPORT_ISSUE_DATE")
    public ZonedDateTime pASSPORTISSUEDATE;
    @JsonProperty("PASSPORT_EXPIRY_DATE")
    public ZonedDateTime pASSPORTEXPIRYDATE;
    @JsonProperty("OCCUPATION_ID")
    public Integer oCCUPATIONID;
    @JsonProperty("BUSINESS_TITLE")
    public String bUSINESSTITLE;
    @JsonProperty("COMPANY_NAME")
    public String cOMPANYNAME;
    @JsonProperty("NATURE_OF_BUSINESS_ID")
    public Integer nATUREOFBUSINESSID;
    @JsonProperty("NATURE_OF_BUSINESS_OTHER")
    public String nATUREOFBUSINESSOTHER;
    @JsonProperty("OCCUPATION_OTHER")
    public String oCCUPATIONOTHER;
    @JsonProperty("MEMBER_SINCE")
    public Object mEMBERSINCE;
    @JsonProperty("RACE_ID")
    public Object rACEID;
    @JsonProperty("VISA_NO")
    public String vISANO;
    @JsonProperty("VISA_EXPIRY_DATE")
    public ZonedDateTime vISAEXPIRYDATE;
    @JsonProperty("EMAIL")
    public String eMAIL;
    @JsonProperty("MOBILE_IN_HOME_COUNTRY")
    public String mOBILEINHOMECOUNTRY;
    @JsonProperty("MOBILE_IN_THAI")
    public String mOBILEINTHAI;
    @JsonProperty("DEGREE_ID")
    public Integer dEGREEID;
    @JsonProperty("REVENUE")
    public Object rEVENUE;
    @JsonProperty("BIRTHPLACE")
    public Object bIRTHPLACE;
    @JsonProperty("HOBBY")
    public Object hOBBY;
    @JsonProperty("COUNTRY_ID")
    public Object cOUNTRYID;
    @JsonProperty("MEMBER_STATUS")
    public String mEMBERSTATUS;
    @JsonProperty("CARD_ISSUE_DATE")
    public Object cARDISSUEDATE;
    @JsonProperty("CARD_EXPIRY_DATE")
    public Object cARDEXPIRYDATE;
    @JsonProperty("REMARK")
    public String rEMARK;
    @JsonProperty("MEMBER_TYPE")
    public String mEMBERTYPE;
    @JsonProperty("MEMBER_CORE_ID")
    public Integer mEMBERCOREID;
    @JsonProperty("MEMBER_FILE_APPLICATION")
    public String mEMBERFILEAPPLICATION;
    @JsonProperty("MEMBER_FILE_OTHER")
    public String mEMBERFILEOTHER;
    @JsonProperty("MEMBER_FILE_PASSPORT")
    public String mEMBERFILEPASSPORT;
    @JsonProperty("MEMBER_GROUP_ID")
    public String mEMBERGROUPID;
    @JsonProperty("MEMBER_NAME_FAMILY")
    public Object mEMBERNAMEFAMILY;
    @JsonProperty("MARKETING_ID")
    public String mARKETINGID;
    @JsonProperty("ACTIVE_DATE")
    public ZonedDateTime aCTIVEDATE;
    @JsonProperty("RECORD_STATUS")
    public String rECORDSTATUS;
    @JsonProperty("CREATE_DATE")
    public Instant cREATEDATE;
    @JsonProperty("CREATE_USER")
    public String cREATEUSER;
    @JsonProperty("CREATE_USER_TYPE")
    public String cREATEUSERTYPE;
    @JsonProperty("LAST_USER_TYPE")
    public String lASTUSERTYPE;
    @JsonProperty("LAST_DATE")
    public Instant lASTDATE;
    @JsonProperty("LAST_USER")
    public String lASTUSER;
    @JsonProperty("CORE_MEMBER_ID")
    public String cOREMEMBERID;
    @JsonProperty("MEMBER_TRANSFER_ID")
    public String mEMBERTRANSFERID;
    @JsonProperty("EPA_MEMBER_REMARK")
    public String ePAMEMBERREMARK;
    @JsonProperty("CALLCENTER_MEMBER_REMARK")
    public String cALLCENTERMEMBERREMARK;
    @JsonProperty("VISA_AT_ISSUE")
    public String vISAATISSUE;
    @JsonProperty("VISA_ISSUE_DATE")
    public ZonedDateTime vISAISSUEDATE;
    @JsonProperty("MEMBER_BLOCK_SERVICES_STATUS")
    public String mEMBERBLOCKSERVICESSTATUS;
    @JsonProperty("MEMBER_BLOCK_SERVICES_REMARK")
    public String mEMBERBLOCKSERVICESREMARK;
    @JsonProperty("ATTACH_FILE1")
    public String aTTACHFILE1;
    @JsonProperty("ATTACH_FILE2")
    public String aTTACHFILE2;
    @JsonProperty("ATTACH_FILE3")
    public String aTTACHFILE3;
    @JsonProperty("HOBBY_ID")
    public String hOBBYID;
    @JsonProperty("INCOME_ID")
    public String iNCOMEID;
    @JsonProperty("MEMBER_NEXT")
    public String mEMBERNEXT;
    @JsonProperty("MEMBER_BIRTH_CERTIFICATE")
    public String mEMBERBIRTHCERTIFICATE;
    @JsonProperty("MEMBER_MARRIAGE_CERTIFICATE")
    public String mEMBERMARRIAGECERTIFICATE;
    @JsonProperty("MEMBER_PROOF_PAYMENT")
    public String mEMBERPROOFPAYMENT;
    @JsonProperty("MEMBER_TYPE_ID")
    public String mEMBERTYPEID;
    @JsonProperty("EXPIRY_DATE")
    public ZonedDateTime eXPIRYDATE;
    @JsonProperty("IMMIGRATION_ATTACH_FILE")
    public String iMMIGRATIONATTACHFILE;
    @JsonProperty("REMARK_EDIT")
    public String rEMARKEDIT;
    @JsonProperty("COMMISSION_PAYMENT_STATUS")
    public String cOMMISSIONPAYMENTSTATUS;
    @JsonProperty("MEMBER_UPGRADE_ID")
    public String mEMBERUPGRADEID;
    @JsonProperty("MEMBERSHIP_ID_bk")
    public String mEMBERSHIPIDBk;
    @JsonProperty("LEGAL_RELATIONSHIP_TYPE")
    public String lEGALRELATIONSHIPTYPE;
    @JsonProperty("COVID19_REMEDY_1")
    public String cOVID19REMEDY1;
    @JsonProperty("EXPIRY_DATE_BEFORE_COVID19")
    public ZonedDateTime eXPIRYDATEBEFORECOVID19;
    @JsonProperty("EXPIRY_DATE_AFTER_COVID19")
    public ZonedDateTime eXPIRYDATEAFTERCOVID19;

}
