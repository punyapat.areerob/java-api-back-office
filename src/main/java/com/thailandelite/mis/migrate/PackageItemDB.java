package com.thailandelite.mis.migrate;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class PackageItemDB {
    private float PACKAGE_ID;
    private String ID_PACKAGE;
    private String PACKAGE_NAME_EN;
    private ZonedDateTime START_DATE;
    private ZonedDateTime END_DATE;
    private String PRIVILEGE_NAME_EN;
    private String PACKAGE_TIMES_NAME_EN;
    private float PACKAGE_ITEM_QUANTITY;
    private float PACKAGE_ITEM_ID;
    private float PRIVILEGE_ID;
    private float PACKAGE_TIMES_ID;
    private Integer PACKAGE_ITEM_POINT;
    private Integer PACKAGE_ITEM_ORDER;
    private String REMARK = null;
    private String RECORD_STATUS;
    private ZonedDateTime CREATE_DATE;
    private String CREATE_USER;
    private String CREATE_USER_TYPE;
    private ZonedDateTime LAST_DATE;
    private String LAST_USER;
    private String LAST_USER_TYPE;
    private String QUAOTA_HIDE = null;
    private String SHOW_QUOTA_STATUS;

}
