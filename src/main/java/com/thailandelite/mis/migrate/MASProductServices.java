package com.thailandelite.mis.migrate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PRODUCT_SERVICES_ID" ,
    "PRODUCT_ID" ,
    "SERVICES_ID"
})
@Data
public class MASProductServices{
    @JsonProperty("PRODUCT_SERVICES_ID")
    private float PRODUCT_SERVICES_ID;
    @JsonProperty("PRODUCT_ID")
    private float PRODUCT_ID;
    @JsonProperty("SERVICES_ID")
    private float SERVICES_ID;


}
