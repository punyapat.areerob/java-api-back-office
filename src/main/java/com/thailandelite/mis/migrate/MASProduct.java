package com.thailandelite.mis.migrate;

import lombok.Data;

@Data
public class MASProduct extends MASDB{
    private float PRODUCT_ID;
    private String PRODUCT_NAME;
    private float PRIVILEGE_GROUP_ID;
    private String REMARK = null;

    private String SERVICE_TYPE_CODE_old;
    private String PRODUCT_STATUS;
}
