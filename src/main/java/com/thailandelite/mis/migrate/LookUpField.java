package com.thailandelite.mis.migrate;

import com.thailandelite.mis.model.domain.Member;

import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LookUpField {
    public static void main(String[] args) {
        Class myObjectClass = Member.class;
        Field[] fields   = myObjectClass.getDeclaredFields();
        System.out.println("------Start------");
        for (Field field : fields) {
            Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(field.getName());
            StringBuffer sb = new StringBuffer();
            while (m.find()) {
                m.appendReplacement(sb, "_"+m.group().toLowerCase());
            }
            m.appendTail(sb);
            System.out.println(sb.toString());
        }
        System.out.println("------END------");
    }
}
