package com.thailandelite.mis.migrate;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "APPLICATION_ID",
    "APPLICATION_PICTURE",
    "APPLICATION_PATH",
    "PACKAGE_ID",
    "APPLICATION_NO",
    "MEMBERSHIP_NO",
    "AGENT_ID",
    "MEMBERSHIP_ID_NO",
    "APPROVAL_NO",
    "GENDER_ID",
    "TITLE_ID",
    "FNAME",
    "MNAME",
    "LNAME",
    "NICK_NAME",
    "NAME_ON_CARD",
    "BIRTHDATE",
    "BLOOD_TYPE",
    "RELIGION_ID",
    "ALLERGIC",
    "NATIONALITY_ID",
    "PASSPORT_NO",
    "PASSPORT_ISSUE_BY",
    "PASSPORT_ISSUE_DATE",
    "PASSPORT_EXPIRY_DATE",
    "OCCUPATION_ID",
    "BUSINESS_TITLE",
    "COMPANY_NAME",
    "NATURE_OF_BUSINESS_ID",
    "NATURE_OF_BUSINESS_OTHER",
    "PAYMENT_DATE",
    "CARD_ISSUE_DATE",
    "CARD_EXPIRY_DATE",
    "APPLICATION_FILE_PASSPORT",
    "OCCUPATION_OTHER",
    "MEMBER_RECEIVE_CARD_STATUS",
    "WELCOME_PACK_STATUS",
    "PAYMENT_STATUS",
    "APPLICATION_STATUS_CODE",
    "APPLICATION_FILE_APPLICATION",
    "APPLICATION_FILE_OTHER",
    "IMMIGRATION_ATTACH_FILE",
    "IMMIGRATION_ATTACH_FILE_REMARK",
    "APPLICATION_TYPE",
    "MEMBER_TRANSFER_ID",
    "MEMBER_TYPE",
    "MEMBER_NAME_FAMILY",
    "MARKETING_ID",
    "GR_PROCESS_ITEM_ID",
    "MEMBER_UPGRAD_ID",
    "PAYMENT_ATTACH_FILE_REMARK",
    "PAYMENT_ATTACH_FILE",
    "CORE_MEMBER_ID",
    "ACTIVE_DATE",
    "APPROVE_DATE",
    "PACKAGE_ID_OLD",
    "PACKAGE_NAME_OLD",
    "REMARK",
    "RECORD_STATUS",
    "CREATE_DATE",
    "CREATE_USER",
    "CREATE_USER_TYPE",
    "LAST_DATE",
    "LAST_USER",
    "LAST_USER_TYPE",
    "PROJECT_ID",
    "TRANSFER_FILE_1",
    "TRANSFER_FILE_2",
    "TRANSFER_FILE_3",
    "Q_VISA_CURRENT",
    "Q_PASSPORT_EXPIRY_DATE",
    "Q_PASSPORT_LEAST3_EMPTY_PAGE",
    "Q_MEMBERSHIP_FEE_PAID_FROM",
    "Q_AFFIX_ELITE_VISA",
    "Q_YOUR_LATEST",
    "Q_KNOW_US",
    "URGENT_STATUS",
    "APPLICATION_BIRTH_CERTIFICATE",
    "APPLICATION_IDENTITY_DOCUMENT",
    "APPLICATION_MARRIAGE_CERTIFICATE",
    "APPLICATION_PROOF_PAYMENT",
    "EMAIL",
    "PACKAGE_TYPE_CODE_old",
    "id_old",
    "CORE_MEMBER_REMARK",
    "FINANCE_STATUS",
    "IMMIGRATION_APPROVE_DATE",
    "GR_DOC_NO",
    "DOC_NO_TM",
    "GR_DOC_DATE",
    "import_name",
    "NATIONALITY_PASSPORT_ID",
    "REMARK_EDIT",
    "LEGAL_RELATIONSHIP_TYPE",
    "PAYMENT_RATE",
    "CHANNEL",
    "MEMBER_RENEW_ID",
    "EARLY_BIRD",
    "ACTIVATE_DATE",
    "COUNTRY_OF_BIRTH",
    "Q_STAY_IN_THAI_PAST_3_YEAR",
    "Q_STAY_IN_THAI_TIMES",
    "ACCOMMODATION_IN_THAI_TYPE",
    "ACCOMMODATION_IN_THAI_OTHER"
})
@Generated("jsonschema2pojo")
public class ApplicationDB {

    @JsonProperty("APPLICATION_ID")
    private Integer applicationId;
    @JsonProperty("APPLICATION_PICTURE")
    private String applicationPicture;
    @JsonProperty("APPLICATION_PATH")
    private String applicationPath;
    @JsonProperty("PACKAGE_ID")
    private Integer packageId;
    @JsonProperty("APPLICATION_NO")
    private String applicationNo;
    @JsonProperty("MEMBERSHIP_NO")
    private Object membershipNo;
    @JsonProperty("AGENT_ID")
    private Integer agentId;
    @JsonProperty("MEMBERSHIP_ID_NO")
    private Object membershipIdNo;
    @JsonProperty("APPROVAL_NO")
    private Object approvalNo;
    @JsonProperty("GENDER_ID")
    private String genderId;
    @JsonProperty("TITLE_ID")
    private Integer titleId;
    @JsonProperty("FNAME")
    private String fname;
    @JsonProperty("MNAME")
    private String mname;
    @JsonProperty("LNAME")
    private String lname;
    @JsonProperty("NICK_NAME")
    private Object nickName;
    @JsonProperty("NAME_ON_CARD")
    private String nameOnCard;
    @JsonProperty("BIRTHDATE")
    private ZonedDateTime birthdate;
    @JsonProperty("BLOOD_TYPE")
    private Object bloodType;
    @JsonProperty("RELIGION_ID")
    private Object religionId;
    @JsonProperty("ALLERGIC")
    private String allergic;
    @JsonProperty("NATIONALITY_ID")
    private Integer nationalityId;
    @JsonProperty("PASSPORT_NO")
    private String passportNo;
    @JsonProperty("PASSPORT_ISSUE_BY")
    private String passportIssueBy;
    @JsonProperty("PASSPORT_ISSUE_DATE")
    private ZonedDateTime passportIssueDate;
    @JsonProperty("PASSPORT_EXPIRY_DATE")
    private ZonedDateTime passportExpiryDate;
    @JsonProperty("OCCUPATION_ID")
    private Integer occupationId;
    @JsonProperty("BUSINESS_TITLE")
    private String businessTitle;
    @JsonProperty("COMPANY_NAME")
    private String companyName;
    @JsonProperty("NATURE_OF_BUSINESS_ID")
    private Integer natureOfBusinessId;
    @JsonProperty("NATURE_OF_BUSINESS_OTHER")
    private String natureOfBusinessOther;
    @JsonProperty("PAYMENT_DATE")
    private Object paymentDate;
    @JsonProperty("CARD_ISSUE_DATE")
    private Object cardIssueDate;
    @JsonProperty("CARD_EXPIRY_DATE")
    private Object cardExpiryDate;
    @JsonProperty("APPLICATION_FILE_PASSPORT")
    private String applicationFilePassport;
    @JsonProperty("OCCUPATION_OTHER")
    private String occupationOther;
    @JsonProperty("MEMBER_RECEIVE_CARD_STATUS")
    private String memberReceiveCardStatus;
    @JsonProperty("WELCOME_PACK_STATUS")
    private Object welcomePackStatus;
    @JsonProperty("PAYMENT_STATUS")
    private Object paymentStatus;
    @JsonProperty("APPLICATION_STATUS_CODE")
    private String applicationStatusCode;
    @JsonProperty("APPLICATION_FILE_APPLICATION")
    private String applicationFileApplication;
    @JsonProperty("APPLICATION_FILE_OTHER")
    private Object applicationFileOther;
    @JsonProperty("IMMIGRATION_ATTACH_FILE")
    private String immigrationAttachFile;
    @JsonProperty("IMMIGRATION_ATTACH_FILE_REMARK")
    private Object immigrationAttachFileRemark;
    @JsonProperty("APPLICATION_TYPE")
    private String applicationType;
    @JsonProperty("MEMBER_TRANSFER_ID")
    private Object memberTransferId;
    @JsonProperty("MEMBER_TYPE")
    private String memberType;
    @JsonProperty("MEMBER_NAME_FAMILY")
    private Object memberNameFamily;
    @JsonProperty("MARKETING_ID")
    private Integer marketingId;
    @JsonProperty("GR_PROCESS_ITEM_ID")
    private Object grProcessItemId;
    @JsonProperty("MEMBER_UPGRAD_ID")
    private Object memberUpgradId;
    @JsonProperty("PAYMENT_ATTACH_FILE_REMARK")
    private Object paymentAttachFileRemark;
    @JsonProperty("PAYMENT_ATTACH_FILE")
    private Object paymentAttachFile;
    @JsonProperty("CORE_MEMBER_ID")
    private Object coreMemberId;
    @JsonProperty("ACTIVE_DATE")
    private Object activeDate;
    @JsonProperty("APPROVE_DATE")
    private Object approveDate;
    @JsonProperty("PACKAGE_ID_OLD")
    private Object packageIdOld;
    @JsonProperty("PACKAGE_NAME_OLD")
    private Object packageNameOld;
    @JsonProperty("REMARK")
    private String remark;
    @JsonProperty("RECORD_STATUS")
    private String recordStatus;
    @JsonProperty("CREATE_DATE")
    private String createDate;
    @JsonProperty("CREATE_USER")
    private String createUser;
    @JsonProperty("CREATE_USER_TYPE")
    private String createUserType;
    @JsonProperty("LAST_DATE")
    private String lastDate;
    @JsonProperty("LAST_USER")
    private String lastUser;
    @JsonProperty("LAST_USER_TYPE")
    private String lastUserType;
    @JsonProperty("PROJECT_ID")
    private Object projectId;
    @JsonProperty("TRANSFER_FILE_1")
    private Object transferFile1;
    @JsonProperty("TRANSFER_FILE_2")
    private Object transferFile2;
    @JsonProperty("TRANSFER_FILE_3")
    private Object transferFile3;
    @JsonProperty("Q_VISA_CURRENT")
    private String qVisaCurrent;
    @JsonProperty("Q_PASSPORT_EXPIRY_DATE")
    private String qPassportExpiryDate;
    @JsonProperty("Q_PASSPORT_LEAST3_EMPTY_PAGE")
    private String qPassportLeast3EmptyPage;
    @JsonProperty("Q_MEMBERSHIP_FEE_PAID_FROM")
    private String qMembershipFeePaidFrom;
    @JsonProperty("Q_AFFIX_ELITE_VISA")
    private String qAffixEliteVisa;
    @JsonProperty("Q_YOUR_LATEST")
    private String qYourLatest;
    @JsonProperty("Q_KNOW_US")
    private String qKnowUs;
    @JsonProperty("URGENT_STATUS")
    private String urgentStatus;
    @JsonProperty("APPLICATION_BIRTH_CERTIFICATE")
    private String applicationBirthCertificate;
    @JsonProperty("APPLICATION_IDENTITY_DOCUMENT")
    private Object applicationIdentityDocument;
    @JsonProperty("APPLICATION_MARRIAGE_CERTIFICATE")
    private String applicationMarriageCertificate;
    @JsonProperty("APPLICATION_PROOF_PAYMENT")
    private Object applicationProofPayment;
    @JsonProperty("EMAIL")
    private Object email;
    @JsonProperty("PACKAGE_TYPE_CODE_old")
    private Object pACKAGETYPECODEOld;
    @JsonProperty("id_old")
    private Object idOld;
    @JsonProperty("CORE_MEMBER_REMARK")
    private Object coreMemberRemark;
    @JsonProperty("FINANCE_STATUS")
    private Object financeStatus;
    @JsonProperty("IMMIGRATION_APPROVE_DATE")
    private String immigrationApproveDate;
    @JsonProperty("GR_DOC_NO")
    private String grDocNo;
    @JsonProperty("DOC_NO_TM")
    private String docNoTm;
    @JsonProperty("GR_DOC_DATE")
    private String grDocDate;
    @JsonProperty("import_name")
    private Object importName;
    @JsonProperty("NATIONALITY_PASSPORT_ID")
    private Object nationalityPassportId;
    @JsonProperty("REMARK_EDIT")
    private Object remarkEdit;
    @JsonProperty("LEGAL_RELATIONSHIP_TYPE")
    private Object legalRelationshipType;
    @JsonProperty("PAYMENT_RATE")
    private String paymentRate;
    @JsonProperty("CHANNEL")
    private String channel;
    @JsonProperty("MEMBER_RENEW_ID")
    private Object memberRenewId;
    @JsonProperty("EARLY_BIRD")
    private String earlyBird;
    @JsonProperty("ACTIVATE_DATE")
    private Object activateDate;
    @JsonProperty("COUNTRY_OF_BIRTH")
    private String countryOfBirth;
    @JsonProperty("Q_STAY_IN_THAI_PAST_3_YEAR")
    private Object qStayInThaiPast3Year;
    @JsonProperty("Q_STAY_IN_THAI_TIMES")
    private Object qStayInThaiTimes;
    @JsonProperty("ACCOMMODATION_IN_THAI_TYPE")
    private Object accommodationInThaiType;
    @JsonProperty("ACCOMMODATION_IN_THAI_OTHER")
    private Object accommodationInThaiOther;
}
