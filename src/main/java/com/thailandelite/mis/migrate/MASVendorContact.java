package com.thailandelite.mis.migrate;

public class MASVendorContact extends MASDB{
    private float VENDER_CONTACT_ID;
    private float VENDER_ID;
    private float CONTACT_TYPE_ID;
    private String VENDER_CONTACT_NAME;
    private String VENDER_CONTACT_PHONE;
    private String VENDER_CONTACT_FAX;
    private String VENDER_CONTACT_MOBILE = null;
    private String VENDER_CONTACT_EMAIL;
    private String REMARK;
    private String RECORD_STATUS;
    private String CREATE_DATE;
    private String CREATE_USER = null;
    private String CREATE_USER_TYPE = null;
    private String LAST_DATE;
    private String LAST_USER;
    private String LAST_USER_TYPE;
    private String VENDER_CONTACT_PRIMARY;
    private String VENDOR_ID_old;


    // Getter Methods

    public float getVENDER_CONTACT_ID() {
        return VENDER_CONTACT_ID;
    }

    public float getVENDER_ID() {
        return VENDER_ID;
    }

    public float getCONTACT_TYPE_ID() {
        return CONTACT_TYPE_ID;
    }

    public String getVENDER_CONTACT_NAME() {
        return VENDER_CONTACT_NAME;
    }

    public String getVENDER_CONTACT_PHONE() {
        return VENDER_CONTACT_PHONE;
    }

    public String getVENDER_CONTACT_FAX() {
        return VENDER_CONTACT_FAX;
    }

    public String getVENDER_CONTACT_MOBILE() {
        return VENDER_CONTACT_MOBILE;
    }

    public String getVENDER_CONTACT_EMAIL() {
        return VENDER_CONTACT_EMAIL;
    }

    public String getREMARK() {
        return REMARK;
    }

    public String getRECORD_STATUS() {
        return RECORD_STATUS;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public String getCREATE_USER_TYPE() {
        return CREATE_USER_TYPE;
    }

    public String getLAST_DATE() {
        return LAST_DATE;
    }

    public String getLAST_USER() {
        return LAST_USER;
    }

    public String getLAST_USER_TYPE() {
        return LAST_USER_TYPE;
    }

    public String getVENDER_CONTACT_PRIMARY() {
        return VENDER_CONTACT_PRIMARY;
    }

    public String getVENDOR_ID_old() {
        return VENDOR_ID_old;
    }

    // Setter Methods

    public void setVENDER_CONTACT_ID(float VENDER_CONTACT_ID) {
        this.VENDER_CONTACT_ID = VENDER_CONTACT_ID;
    }

    public void setVENDER_ID(float VENDER_ID) {
        this.VENDER_ID = VENDER_ID;
    }

    public void setCONTACT_TYPE_ID(float CONTACT_TYPE_ID) {
        this.CONTACT_TYPE_ID = CONTACT_TYPE_ID;
    }

    public void setVENDER_CONTACT_NAME(String VENDER_CONTACT_NAME) {
        this.VENDER_CONTACT_NAME = VENDER_CONTACT_NAME;
    }

    public void setVENDER_CONTACT_PHONE(String VENDER_CONTACT_PHONE) {
        this.VENDER_CONTACT_PHONE = VENDER_CONTACT_PHONE;
    }

    public void setVENDER_CONTACT_FAX(String VENDER_CONTACT_FAX) {
        this.VENDER_CONTACT_FAX = VENDER_CONTACT_FAX;
    }

    public void setVENDER_CONTACT_MOBILE(String VENDER_CONTACT_MOBILE) {
        this.VENDER_CONTACT_MOBILE = VENDER_CONTACT_MOBILE;
    }

    public void setVENDER_CONTACT_EMAIL(String VENDER_CONTACT_EMAIL) {
        this.VENDER_CONTACT_EMAIL = VENDER_CONTACT_EMAIL;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public void setRECORD_STATUS(String RECORD_STATUS) {
        this.RECORD_STATUS = RECORD_STATUS;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public void setCREATE_USER_TYPE(String CREATE_USER_TYPE) {
        this.CREATE_USER_TYPE = CREATE_USER_TYPE;
    }

    public void setLAST_DATE(String LAST_DATE) {
        this.LAST_DATE = LAST_DATE;
    }

    public void setLAST_USER(String LAST_USER) {
        this.LAST_USER = LAST_USER;
    }

    public void setLAST_USER_TYPE(String LAST_USER_TYPE) {
        this.LAST_USER_TYPE = LAST_USER_TYPE;
    }

    public void setVENDER_CONTACT_PRIMARY(String VENDER_CONTACT_PRIMARY) {
        this.VENDER_CONTACT_PRIMARY = VENDER_CONTACT_PRIMARY;
    }

    public void setVENDOR_ID_old(String VENDOR_ID_old) {
        this.VENDOR_ID_old = VENDOR_ID_old;
    }
}
