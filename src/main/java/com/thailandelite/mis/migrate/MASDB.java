package com.thailandelite.mis.migrate;

public class MASDB {
    public String RECORD_STATUS;
    public String CREATE_DATE;
    public String CREATE_USER = null;
    public String CREATE_USER_TYPE = null;
    public String LAST_DATE;
    public String LAST_USER;
    public String LAST_USER_TYPE;


    public String getRECORD_STATUS() {
        return RECORD_STATUS;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public String getCREATE_USER_TYPE() {
        return CREATE_USER_TYPE;
    }

    public String getLAST_DATE() {
        return LAST_DATE;
    }

    public String getLAST_USER() {
        return LAST_USER;
    }

    public String getLAST_USER_TYPE() {
        return LAST_USER_TYPE;
    }

    public void setRECORD_STATUS(String RECORD_STATUS) {
        this.RECORD_STATUS = RECORD_STATUS;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public void setCREATE_USER_TYPE(String CREATE_USER_TYPE) {
        this.CREATE_USER_TYPE = CREATE_USER_TYPE;
    }

    public void setLAST_DATE(String LAST_DATE) {
        this.LAST_DATE = LAST_DATE;
    }

    public void setLAST_USER(String LAST_USER) {
        this.LAST_USER = LAST_USER;
    }

    public void setLAST_USER_TYPE(String LAST_USER_TYPE) {
        this.LAST_USER_TYPE = LAST_USER_TYPE;
    }
}
