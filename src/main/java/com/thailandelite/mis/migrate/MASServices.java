package com.thailandelite.mis.migrate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SERVICES_ID" ,
    "SERVICES_CODE" ,
    "SERVICES_TYPE" ,
    "SERVICES_NAME" ,
    "REMARK" ,
    "SERVICES_CONFIRM_LETTER"
})
@Data
public class MASServices {

    @JsonProperty("SERVICES_ID")
    private float SERVICES_ID;
    @JsonProperty("SERVICES_CODE")
    private String SERVICES_CODE;
    @JsonProperty("SERVICES_TYPE")
    private String SERVICES_TYPE = null;
    @JsonProperty("SERVICES_NAME")
    private String SERVICES_NAME;
    @JsonProperty("REMARK")
    private String REMARK = null;
    @JsonProperty("SERVICES_CONFIRM_LETTER")
    private String SERVICES_CONFIRM_LETTER;
    @JsonProperty("SERVICES_CODE_OLD")
    private String SERVICES_CODE_OLD;
    @JsonProperty("EPA_SERVICE_CODE")
    private String EPA_SERVICE_CODE;
    @JsonProperty("EPA_SERVICE_NAME")
    private String EPA_SERVICE_NAME;
    @JsonProperty("AOT_REPORT_ORDER")
    private String AOT_REPORT_ORDER;
    @JsonProperty("AOT_REPORT_STATUS")
    private String AOT_REPORT_STATUS;
    @JsonProperty("GR_PROCESS_STATUS")
    private String GR_PROCESS_STATUS;
    @JsonProperty("SHOW_MEMBER_EXPIRE")
    private String SHOW_MEMBER_EXPIRE;
    @JsonProperty("PUBLICE_STATUS")
    private String PUBLICE_STATUS;

    @JsonProperty("RECORD_STATUS")
    public String RECORD_STATUS;
    @JsonProperty("CREATE_DATE")
    public String CREATE_DATE;
    @JsonProperty("CREATE_USER")
    public String CREATE_USER;
    @JsonProperty("CREATE_USER_TYPE")
    public String CREATE_USER_TYPE;
    @JsonProperty("LAST_DATE")
    public String LAST_DATE;
    @JsonProperty("LAST_USER")
    public String LAST_USER;
    @JsonProperty("LAST_USER_TYPE")
    public String LAST_USER_TYPE;
}
