package com.thailandelite.mis.migrate;

public class OLDVendorContact {
    private float VENDER_CONTRACT_ID;
    private float VENDER_ID;
    private String VENDER_CONTRACT_START_DATE;
    private String VENDER_CONTRACT_END_DATE;
    private String VENDER_CONTRACT_FILE;
    private String VENDER_CONTRACT_PATH;
    private String VENDER_CONTRACT_STATUS;
    private String REMARK = null;
    private String RECORD_STATUS;
    private String CREATE_DATE = null;
    private String CREATE_USER = null;
    private String CREATE_USER_TYPE = null;
    private String LAST_DATE;
    private String LAST_USER;
    private String LAST_USER_TYPE;


    // Getter Methods

    public float getVENDER_CONTRACT_ID() {
        return VENDER_CONTRACT_ID;
    }

    public float getVENDER_ID() {
        return VENDER_ID;
    }

    public String getVENDER_CONTRACT_START_DATE() {
        return VENDER_CONTRACT_START_DATE;
    }

    public String getVENDER_CONTRACT_END_DATE() {
        return VENDER_CONTRACT_END_DATE;
    }

    public String getVENDER_CONTRACT_FILE() {
        return VENDER_CONTRACT_FILE;
    }

    public String getVENDER_CONTRACT_PATH() {
        return VENDER_CONTRACT_PATH;
    }

    public String getVENDER_CONTRACT_STATUS() {
        return VENDER_CONTRACT_STATUS;
    }

    public String getREMARK() {
        return REMARK;
    }

    public String getRECORD_STATUS() {
        return RECORD_STATUS;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public String getCREATE_USER_TYPE() {
        return CREATE_USER_TYPE;
    }

    public String getLAST_DATE() {
        return LAST_DATE;
    }

    public String getLAST_USER() {
        return LAST_USER;
    }

    public String getLAST_USER_TYPE() {
        return LAST_USER_TYPE;
    }

    // Setter Methods

    public void setVENDER_CONTRACT_ID(float VENDER_CONTRACT_ID) {
        this.VENDER_CONTRACT_ID = VENDER_CONTRACT_ID;
    }

    public void setVENDER_ID(float VENDER_ID) {
        this.VENDER_ID = VENDER_ID;
    }

    public void setVENDER_CONTRACT_START_DATE(String VENDER_CONTRACT_START_DATE) {
        this.VENDER_CONTRACT_START_DATE = VENDER_CONTRACT_START_DATE;
    }

    public void setVENDER_CONTRACT_END_DATE(String VENDER_CONTRACT_END_DATE) {
        this.VENDER_CONTRACT_END_DATE = VENDER_CONTRACT_END_DATE;
    }

    public void setVENDER_CONTRACT_FILE(String VENDER_CONTRACT_FILE) {
        this.VENDER_CONTRACT_FILE = VENDER_CONTRACT_FILE;
    }

    public void setVENDER_CONTRACT_PATH(String VENDER_CONTRACT_PATH) {
        this.VENDER_CONTRACT_PATH = VENDER_CONTRACT_PATH;
    }

    public void setVENDER_CONTRACT_STATUS(String VENDER_CONTRACT_STATUS) {
        this.VENDER_CONTRACT_STATUS = VENDER_CONTRACT_STATUS;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public void setRECORD_STATUS(String RECORD_STATUS) {
        this.RECORD_STATUS = RECORD_STATUS;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public void setCREATE_USER_TYPE(String CREATE_USER_TYPE) {
        this.CREATE_USER_TYPE = CREATE_USER_TYPE;
    }

    public void setLAST_DATE(String LAST_DATE) {
        this.LAST_DATE = LAST_DATE;
    }

    public void setLAST_USER(String LAST_USER) {
        this.LAST_USER = LAST_USER;
    }

    public void setLAST_USER_TYPE(String LAST_USER_TYPE) {
        this.LAST_USER_TYPE = LAST_USER_TYPE;
    }
}
