package com.thailandelite.mis.migrate;

public class MASProductVendor extends MASDB{
    private float PRODUCT_VENDER_ID;
    private float VENDER_ID;
    private String PRODUCT_CODE;
    private String PRODUCT_NAME_TH;
    private String PRODUCT_NAME_EN;
    private String REMARK = null;
    private String PRODUCT_NAME;
    private String PRICE;
    private float PRODUCT_ID;
    private String VENDOR_ID_old;
    private String PRODUCT_CODE_bk;
    private String SERVICE_TYPE_CODE_old;
    private String NORMAL_PRICE;
    private String GUEST_PRICE;
    private String ACTIVE_STATUS;
    private String PACKAGE_USED = null;
    private String PACKAGE_USED_BACK = null;
    private String DESCRIPTION = null;
    private float PRIVILEGE_POINT;


    // Getter Methods

    public float getPRODUCT_VENDER_ID() {
        return PRODUCT_VENDER_ID;
    }

    public float getVENDER_ID() {
        return VENDER_ID;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public String getPRODUCT_NAME_TH() {
        return PRODUCT_NAME_TH;
    }

    public String getPRODUCT_NAME_EN() {
        return PRODUCT_NAME_EN;
    }

    public String getREMARK() {
        return REMARK;
    }

    public String getRECORD_STATUS() {
        return RECORD_STATUS;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public String getCREATE_USER_TYPE() {
        return CREATE_USER_TYPE;
    }

    public String getLAST_DATE() {
        return LAST_DATE;
    }

    public String getLAST_USER() {
        return LAST_USER;
    }

    public String getLAST_USER_TYPE() {
        return LAST_USER_TYPE;
    }

    public String getPRODUCT_NAME() {
        return PRODUCT_NAME;
    }

    public String getPRICE() {
        return PRICE;
    }

    public float getPRODUCT_ID() {
        return PRODUCT_ID;
    }

    public String getVENDOR_ID_old() {
        return VENDOR_ID_old;
    }

    public String getPRODUCT_CODE_bk() {
        return PRODUCT_CODE_bk;
    }

    public String getSERVICE_TYPE_CODE_old() {
        return SERVICE_TYPE_CODE_old;
    }

    public String getNORMAL_PRICE() {
        return NORMAL_PRICE;
    }

    public String getGUEST_PRICE() {
        return GUEST_PRICE;
    }

    public String getACTIVE_STATUS() {
        return ACTIVE_STATUS;
    }

    public String getPACKAGE_USED() {
        return PACKAGE_USED;
    }

    public String getPACKAGE_USED_BACK() {
        return PACKAGE_USED_BACK;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public float getPRIVILEGE_POINT() {
        return PRIVILEGE_POINT;
    }

    // Setter Methods

    public void setPRODUCT_VENDER_ID(float PRODUCT_VENDER_ID) {
        this.PRODUCT_VENDER_ID = PRODUCT_VENDER_ID;
    }

    public void setVENDER_ID(float VENDER_ID) {
        this.VENDER_ID = VENDER_ID;
    }

    public void setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
    }

    public void setPRODUCT_NAME_TH(String PRODUCT_NAME_TH) {
        this.PRODUCT_NAME_TH = PRODUCT_NAME_TH;
    }

    public void setPRODUCT_NAME_EN(String PRODUCT_NAME_EN) {
        this.PRODUCT_NAME_EN = PRODUCT_NAME_EN;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public void setRECORD_STATUS(String RECORD_STATUS) {
        this.RECORD_STATUS = RECORD_STATUS;
    }

    public void setCREATE_DATE(String CREATE_DATE) {
        this.CREATE_DATE = CREATE_DATE;
    }

    public void setCREATE_USER(String CREATE_USER) {
        this.CREATE_USER = CREATE_USER;
    }

    public void setCREATE_USER_TYPE(String CREATE_USER_TYPE) {
        this.CREATE_USER_TYPE = CREATE_USER_TYPE;
    }

    public void setLAST_DATE(String LAST_DATE) {
        this.LAST_DATE = LAST_DATE;
    }

    public void setLAST_USER(String LAST_USER) {
        this.LAST_USER = LAST_USER;
    }

    public void setLAST_USER_TYPE(String LAST_USER_TYPE) {
        this.LAST_USER_TYPE = LAST_USER_TYPE;
    }

    public void setPRODUCT_NAME(String PRODUCT_NAME) {
        this.PRODUCT_NAME = PRODUCT_NAME;
    }

    public void setPRICE(String PRICE) {
        this.PRICE = PRICE;
    }

    public void setPRODUCT_ID(float PRODUCT_ID) {
        this.PRODUCT_ID = PRODUCT_ID;
    }

    public void setVENDOR_ID_old(String VENDOR_ID_old) {
        this.VENDOR_ID_old = VENDOR_ID_old;
    }

    public void setPRODUCT_CODE_bk(String PRODUCT_CODE_bk) {
        this.PRODUCT_CODE_bk = PRODUCT_CODE_bk;
    }

    public void setSERVICE_TYPE_CODE_old(String SERVICE_TYPE_CODE_old) {
        this.SERVICE_TYPE_CODE_old = SERVICE_TYPE_CODE_old;
    }

    public void setNORMAL_PRICE(String NORMAL_PRICE) {
        this.NORMAL_PRICE = NORMAL_PRICE;
    }

    public void setGUEST_PRICE(String GUEST_PRICE) {
        this.GUEST_PRICE = GUEST_PRICE;
    }

    public void setACTIVE_STATUS(String ACTIVE_STATUS) {
        this.ACTIVE_STATUS = ACTIVE_STATUS;
    }

    public void setPACKAGE_USED(String PACKAGE_USED) {
        this.PACKAGE_USED = PACKAGE_USED;
    }

    public void setPACKAGE_USED_BACK(String PACKAGE_USED_BACK) {
        this.PACKAGE_USED_BACK = PACKAGE_USED_BACK;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public void setPRIVILEGE_POINT(float PRIVILEGE_POINT) {
        this.PRIVILEGE_POINT = PRIVILEGE_POINT;
    }
}
