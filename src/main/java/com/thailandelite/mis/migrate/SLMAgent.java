package com.thailandelite.mis.migrate;

import lombok.Data;

@Data
public class SLMAgent extends MASDB{
    private float AGENT_ID;
    private String AGENT_CODE;
    private String SALES_AGENT_TH;
    private String SALES_AGENT_EN;
    private String SALES_CONTACT_TH = null;
    private String SALES_CONTACT_EN = null;
    private float AGENT_TYPE_ID;
    private String TERRITORY = null;
    private String CONTRACT_NO;
    private String CONTRACT_DATE_START = null;
    private String CONTRACT_DATE_END = null;
    private String NO_STREET = null;
    private String COUNTRY_ID = null;
    private String STATE_ID_bk = null;
    private String STATE_ID = null;
    private String CITY_ID = null;
    private String POSTAL_CODE = null;
    private String AGENT_PHONE = null;
    private String AGENT_FAX = null;
    private String AGENT_WEBSITE = null;
    private String AGENT_EMAIL = null;
    private String AGENT_STATUS;
    private String COMMISSION_RATE_ID = null;
    private String REMARK = null;
    private String AGENT_STATUS_REGISTER;
    private String AGENT_FILE_BOOK_BANK = null;
    private String AGENT_FILE_PASSPORT = null;
    private String FILE_PATH;
    private String COMPANY_PROFILE = null;
    private String WEB_AGENT_ID = null;
    private String REMARK_AGENT_CODE = null;
    private String VAT_TYPE;
    private String COMPANY_REGIS_IN_THAI;
    private String TAX_ID = null;

}
