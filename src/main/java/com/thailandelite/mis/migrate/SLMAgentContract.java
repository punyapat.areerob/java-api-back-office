package com.thailandelite.mis.migrate;

import lombok.Data;

@Data
public class SLMAgentContract extends MASDB{
    private float AGENT_CONTRACT_ID;
    private float AGENT_ID;
    private String CONTRACT_DATE_START;
    private String CONTRACT_DATE_END;
    private String TARGET = null;
    private String TARGET_FIRST_YEAR = null;
    private String TARGET_SECOND_YEAR = null;
    private String AGENT_CONTRACT_PATH;
    private String FILE_PROPERTY_CONTRACT = null;
    private String FILE_CONTRACT_TH = null;
    private String FILE_CONTRACT_EN = null;
    private String FILE_COMPANY_CERTIFICATE = null;
    private String FILE_BOOK_BANK = null;
    private String FILE_ID_CARD_COPY = null;
    private String FILE_ADDRESS_COPY = null;
    private String FILE_PASSPORT = null;
    private String FILE_POWER_OF_ATTORNEY = null;
    private String AGENT_CONTRACT_STATUS;
    private float COMMISSION_RATE_ID;
    private String REMARK = null;
    private String CONTRACT_DATE_END_bk;

}
