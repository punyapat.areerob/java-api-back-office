package com.thailandelite.mis.be.common;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.query.UntypedExampleMatcher;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toSet;

public class Utils {

    public static final ExampleMatcher MATCHER = UntypedExampleMatcher.matching()
        .withStringMatcher(ExampleMatcher.StringMatcher.EXACT)
        .withIgnoreNullValues()
        .withIgnoreCase()
        .withIgnorePaths("createdDate", "lastModifiedDate");

    public static <T, S> Set<S> listToSet(List<T> list, Function<T, S> fn) {
        return list.stream().map(fn).collect(toSet());
    }

    public static <T> Example<T> getExample(T entity){
        return Example.of(entity, MATCHER);
    }

    public static <T> Example<T> getExactExample(T entity){
        return Example.of(entity, matcher());
    }

    public static ExampleMatcher matcher(){
        return ExampleMatcher.matching()
            .withStringMatcher(ExampleMatcher.StringMatcher.EXACT)
            .withIgnoreNullValues()
            .withIgnoreCase()
            .withIgnorePaths("createdDate", "lastModifiedDate");
    }
    public static String numberFormat(Object org){
        String pattern = "###,###.###";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(org);
    }
}
