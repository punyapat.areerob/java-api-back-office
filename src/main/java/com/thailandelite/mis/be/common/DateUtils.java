package com.thailandelite.mis.be.common;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    public static ZonedDateTime toDate(LocalDate ld){
        return ld.atStartOfDay(ZoneId.systemDefault());
    }

    public static String toDateString(ZonedDateTime dateTime, String format){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return dateTime.format(formatter);
    }
    public static ZonedDateTime stringToDate(String dateTime, String format){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDate date = LocalDate.parse(dateTime, formatter);

        return date.atStartOfDay(ZoneId.systemDefault());
    }
}
