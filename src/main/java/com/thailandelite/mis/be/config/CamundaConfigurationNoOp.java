package com.thailandelite.mis.be.config;

import com.thailandelite.mis.be.MisbeApp;
import io.github.jhipster.config.JHipsterConstants;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.impl.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@Configuration
@Profile({MisbeApp.PROFILE_MIGRATE, JHipsterConstants.SPRING_PROFILE_TEST})
public class CamundaConfigurationNoOp {
    public CamundaConfigurationNoOp() {
        log.warn("Init CamundaConfigurationNoOp");
    }

    @Bean
    public RuntimeService getRuntimeService() {
        return new RuntimeServiceImpl();
    }

    @Bean
    public TaskService getTaskService() {
        return new TaskServiceImpl();
    }

    @Bean
    public ManagementService getManagementService() {
        return new ManagementServiceImpl();
    }

    @Bean
    public RepositoryService getRepositoryService() {
        return new RepositoryServiceImpl();
    }

    @Bean
    public ExternalTaskService getExternalTaskService() {
        return new ExternalTaskServiceImpl();
    }
}
