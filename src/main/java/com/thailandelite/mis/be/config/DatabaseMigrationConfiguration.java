package com.thailandelite.mis.be.config;

import com.thailandelite.mis.be.MisbeApp;
import io.github.jhipster.config.JHipsterConstants;
import io.github.jhipster.domain.util.JSR310DateConverters.DateToZonedDateTimeConverter;
import io.github.jhipster.domain.util.JSR310DateConverters.ZonedDateTimeToDateConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Configuration
@EntityScan("com.thailandelite.mis.migrate")
@EnableJpaRepositories(basePackages = "com.thailandelite.mis.migrate", entityManagerFactoryRef = "migrateEntityManager", transactionManagerRef = "migrateTransactionManager")
@Profile({MisbeApp.PROFILE_MIGRATE})
public class DatabaseMigrationConfiguration {
    @Autowired
    Environment env;

    public DatabaseMigrationConfiguration() {
        log.warn("Init DatabaseMigrationConfiguration");
    }

    @Bean(name = "migrateDataSource")
    public BasicDataSource migrateDataSource() {
        BasicDataSource dataSource = DataSourceBuilder.create()
            .type(BasicDataSource.class)
            .url(env.getProperty("spring.datasource.migrate.url"))
            .username(env.getProperty("spring.datasource.migrate.username"))
            .password(env.getProperty("spring.datasource.migrate.password"))
            .driverClassName(env.getProperty("spring.datasource.migrate.driver-class-name"))
            .build();

        log.debug("secondDataSource: {}", dataSource);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean migrateEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(migrateDataSource());
        em.setPackagesToScan("com.thailandelite.mis.migrate");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "none");
        properties.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public PlatformTransactionManager migrateTransactionManager(@Qualifier("migrateDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

/*    @Bean(name="entityManagerFactory")
    public LocalSessionFactoryBean sessionFactory() {
        return new LocalSessionFactoryBean();
    }*/
}
