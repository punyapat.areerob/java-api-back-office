package com.thailandelite.mis.be.config;

import com.thailandelite.mis.be.MisbeApp;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Profile({"!" + MisbeApp.PROFILE_MIGRATE})
@Configuration
@EnableTransactionManagement
public class DatabaseBPMConfiguration {
    @Autowired
    Environment env;

    public DatabaseBPMConfiguration() {
        log.warn("Init DatabaseBPMConfiguration");
    }

    @Bean(name = "camundaBpmDataSource")
    public BasicDataSource secondDataSource() {
        BasicDataSource dataSource = DataSourceBuilder.create()
            .type(BasicDataSource.class)
            .url(env.getProperty("spring.datasource.bpm.url"))
            .username(env.getProperty("spring.datasource.bpm.username"))
            .password(env.getProperty("spring.datasource.bpm.password"))
            .driverClassName(env.getProperty("spring.datasource.bpm.driver-class-name"))
            .build();

        log.debug("secondDataSource: {}", dataSource);
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@Qualifier("camundaBpmDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
