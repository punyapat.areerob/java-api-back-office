package com.thailandelite.mis.be.config;

import com.google.common.base.Predicate;
import io.github.jhipster.config.JHipsterConstants;
import io.github.jhipster.config.JHipsterProperties;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.RequestHandler;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.nio.ByteBuffer;
import java.util.*;

@Profile({JHipsterConstants.SPRING_PROFILE_SWAGGER})
@Configuration
@AutoConfigureAfter(JHipsterProperties.class)
public class SwaggerConfiguration {

    static final String MANAGEMENT_TITLE_SUFFIX = "MIS API";
    static final String MANAGEMENT_GROUP_NAME = "mis";
    static final String MANAGEMENT_DESCRIPTION = "MIS endpoints documentation";

    final Set<String> misApiGET = new TreeSet<>();
    final Set<String> misApiPOST = new TreeSet<>();

    private final JHipsterProperties.Swagger properties;

    public SwaggerConfiguration(JHipsterProperties jHipsterProperties) {
        this.properties = jHipsterProperties.getSwagger();
        misApiGET.add("/api/blood-types");
        misApiGET.add("/api/business-industries");
        misApiGET.add("/api/countries");
        misApiGET.add("/api/genders");
        misApiGET.add("/api/nationalities");
        misApiGET.add("/api/occupations");
        misApiGET.add("/api/phone-prefixes");
        misApiGET.add("/api/religions");
        misApiGET.add("/api/titles");
        misApiGET.add("/api/accommodation-types");
        misApiGET.add("/api/visa-types");
        misApiGET.add("/api/channel-knows");

        misApiGET.add("/api/email-transactions/{id}");
        misApiPOST.add("/api/email-transactions/send");
    }

    @Bean
    public Docket swaggerSpringfoxManagementDocket2(@Value("${spring.application.name:application}") String appName) {

        ApiInfo apiInfo = new ApiInfo(
            StringUtils.capitalize(appName) + " " + MANAGEMENT_TITLE_SUFFIX,
            MANAGEMENT_DESCRIPTION,
            properties.getVersion(),
            "",
            ApiInfo.DEFAULT_CONTACT,
            "",
            "",
            new ArrayList<>()
        );

        return createDocket()
            .apiInfo(apiInfo)
            .useDefaultResponseMessages(properties.isUseDefaultResponseMessages())
            .groupName(MANAGEMENT_GROUP_NAME)
            .host(properties.getHost())
            .protocols(new HashSet<>(Arrays.asList(properties.getProtocols())))
            .forCodeGeneration(true)
            .directModelSubstitute(ByteBuffer.class, String.class)
            .genericModelSubstitutes(ResponseEntity.class)
            .ignoredParameterTypes(Pageable.class)
            .select()
//            .paths(regex("/api/.*"))
            .apis(new Predicate<RequestHandler>() {
                @Override
                public boolean apply(@Nullable RequestHandler input) {
                    Set<RequestMethod> methods = Objects.requireNonNull(input).supportedMethods();
                    Set<String> patterns = input.getPatternsCondition().getPatterns();

                    if(methods.contains(RequestMethod.GET)){
                        return misApiGET.containsAll(patterns);
                    }else if(methods.contains(RequestMethod.POST)){
                        return misApiPOST.containsAll(patterns);
                    }
                    return false;
                }
            })
            .build();
    }

    protected Docket createDocket() {
        return new Docket(DocumentationType.SWAGGER_2);
    }
}
