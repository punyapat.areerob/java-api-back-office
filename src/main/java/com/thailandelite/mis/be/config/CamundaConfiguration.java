package com.thailandelite.mis.be.config;

import com.thailandelite.mis.be.MisbeApp;
import io.github.jhipster.config.JHipsterConstants;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Slf4j
@Profile({
    JHipsterConstants.SPRING_PROFILE_PRODUCTION,
    JHipsterConstants.SPRING_PROFILE_DEVELOPMENT,
    MisbeApp.PROFILE_STAGING
})
@Configuration
public class CamundaConfiguration {
    public CamundaConfiguration() {
        log.warn("Init CamundaConfiguration");
    }

    @Bean
    public RuntimeService getRuntimeService() {
        return ProcessEngines.getDefaultProcessEngine().getRuntimeService();
    }

    @Bean
    public TaskService getTaskService() {
        return ProcessEngines.getDefaultProcessEngine().getTaskService();
    }

    @Bean
    public ManagementService getManagementService() {
        return ProcessEngines.getDefaultProcessEngine().getManagementService();
    }

    @Bean
    public RepositoryService getRepositoryService() {
        return ProcessEngines.getDefaultProcessEngine().getRepositoryService();
    }

    @Bean
    public ExternalTaskService getExternalTaskService() {
        return ProcessEngines.getDefaultProcessEngine().getExternalTaskService();
    }

}
