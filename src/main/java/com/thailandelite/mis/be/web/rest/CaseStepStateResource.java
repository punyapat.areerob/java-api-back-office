package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CaseStepState;
import com.thailandelite.mis.be.repository.CaseStepStateRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CaseStepStateResource {

    private final Logger log = LoggerFactory.getLogger(CaseStepStateResource.class);

    private static final String ENTITY_NAME = "caseStepState";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseStepStateRepository caseStepStateRepository;

    public CaseStepStateResource(CaseStepStateRepository caseStepStateRepository) {
        this.caseStepStateRepository = caseStepStateRepository;
    }


    @PostMapping("/case-step-states")
    public ResponseEntity<CaseStepState> createCaseStepState(@RequestBody CaseStepState caseStepState) throws URISyntaxException {
        log.debug("REST request to save CaseStepState : {}", caseStepState);
        if (caseStepState.getId() != null) {
            throw new BadRequestAlertException("A new caseStepState cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseStepState result = caseStepStateRepository.save(caseStepState);
        return ResponseEntity.created(new URI("/api/case-step-states/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/case-step-states")
    public ResponseEntity<CaseStepState> updateCaseStepState(@RequestBody CaseStepState caseStepState) throws URISyntaxException {
        log.debug("REST request to update CaseStepState : {}", caseStepState);
        if (caseStepState.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseStepState result = caseStepStateRepository.save(caseStepState);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseStepState.getId()))
            .body(result);
    }


    @GetMapping("/case-step-states")
    public List<CaseStepState> getAllCaseStepStates() {
        log.debug("REST request to get all CaseStepStates");
        return caseStepStateRepository.findAll();
    }


    @GetMapping("/case-step-states/{id}")
    public ResponseEntity<CaseStepState> getCaseStepState(@PathVariable String id) {
        log.debug("REST request to get CaseStepState : {}", id);
        Optional<CaseStepState> caseStepState = caseStepStateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseStepState);
    }


    @DeleteMapping("/case-step-states/{id}")
    public ResponseEntity<Void> deleteCaseStepState(@PathVariable String id) {
        log.debug("REST request to delete CaseStepState : {}", id);
        caseStepStateRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
