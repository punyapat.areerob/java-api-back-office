package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.JobApplicationRepository;
import com.thailandelite.mis.be.service.FontEndService;
import com.thailandelite.mis.be.service.flow.BPJobApplicationService;
import com.thailandelite.mis.model.domain.JobApplication;
import com.thailandelite.mis.model.dto.request.ContactUsFormReq;
import com.thailandelite.mis.model.dto.request.LandingPageFormReq;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/font-web-point")
public class FontWebPointResource {

    private final JobApplicationRepository jobApplicationRepository;
    private final FontEndService fontEndService;

    public FontWebPointResource(JobApplicationRepository jobApplicationRepository, FontEndService fontEndService) {
        this.fontEndService = fontEndService;
        this.jobApplicationRepository = jobApplicationRepository;
    }

    @GetMapping("/refresh-token")
    public ResponseEntity<String> refreshFontToken() {
        fontEndService.refreshToken();
        return ResponseEntity.ok("Success");
    }

    @PostMapping("/contact-us")
    public ResponseEntity<ContactUsFormReq> createJobApplication(@RequestBody ContactUsFormReq formReq) {
        //TODO create case or send email.
        return ResponseEntity.ok(formReq);
    }

    @PostMapping("/landing/{campaignName}")
    public ResponseEntity<LandingPageFormReq> createJobApplication(@PathVariable String campaignName, @RequestBody LandingPageFormReq formReq) {
        //TODO create case or send email.
        return ResponseEntity.ok(formReq);
    }
}
