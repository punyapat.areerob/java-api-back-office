package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.MemberRemarkInstance;
import com.thailandelite.mis.be.repository.MemberRemarkInstanceRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;


@RestController
@RequestMapping("/api")
public class MemberRemarkInstanceResource {

    private final Logger log = LoggerFactory.getLogger(MemberRemarkInstanceResource.class);

    private static final String ENTITY_NAME = "memberRemarkInstance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MemberRemarkInstanceRepository memberRemarkInstanceRepository;

    public MemberRemarkInstanceResource(MemberRemarkInstanceRepository memberRemarkInstanceRepository) {
        this.memberRemarkInstanceRepository = memberRemarkInstanceRepository;
    }


    @PostMapping("/member-remark-instances")
    public ResponseEntity<MemberRemarkInstance> createMemberRemarkInstance(@RequestBody MemberRemarkInstance memberRemarkInstance) throws URISyntaxException {
        log.debug("REST request to save MemberRemarkInstance : {}", memberRemarkInstance);
        if (memberRemarkInstance.getId() != null) {
            throw new BadRequestAlertException("A new memberRemarkInstance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MemberRemarkInstance result = memberRemarkInstanceRepository.save(memberRemarkInstance);
        return ResponseEntity.created(new URI("/api/member-remark-instances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/member-remark-instances")
    public ResponseEntity<MemberRemarkInstance> updateMemberRemarkInstance(@RequestBody MemberRemarkInstance memberRemarkInstance) throws URISyntaxException {
        log.debug("REST request to update MemberRemarkInstance : {}", memberRemarkInstance);
        if (memberRemarkInstance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MemberRemarkInstance result = memberRemarkInstanceRepository.save(memberRemarkInstance);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, memberRemarkInstance.getId()))
            .body(result);
    }


//    @GetMapping("/member-remark-instances")
//    public List<MemberRemarkInstance> getAllMemberRemarkInstances(String memberId, String teamId, String tagId) {
//        log.debug("REST request to get all MemberRemarkInstances");
//        if(hasText(memberId)){
//             return memberRemarkInstanceRepository.findByMemberIdAndAccessTeamsAndTag(memberId, teamId, tagId);
//        }else {
//            return memberRemarkInstanceRepository.findAll();
//        }
//    }

    @GetMapping("/member-remark-instances")
    public ResponseEntity<Page<MemberRemarkInstance>> getMemberRemarkInstances(Pageable pageable) {
        log.debug("REST request to get all MemberRemarkInstances: page {}", pageable);
        Page<MemberRemarkInstance> page = memberRemarkInstanceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page, headers, HttpStatus.OK);
    }

    @GetMapping("/member-remark-instance/{id}")
    public ResponseEntity<MemberRemarkInstance> getMemberRemarkInstance(@PathVariable String id) {
        log.debug("REST request to get MemberRemarkInstance : {}", id);
        Optional<MemberRemarkInstance> memberRemarkInstance = memberRemarkInstanceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(memberRemarkInstance);
    }

    @GetMapping("/member-remark-instances/pin")
    public ResponseEntity<Page<MemberRemarkInstance>> getPinMemberRemarkInstances(Pageable pageable) {
        log.debug("REST request to get pin MemberRemarkInstance :page {}", pageable);
        Page<MemberRemarkInstance> page = memberRemarkInstanceRepository.findByPinIsTrue(pageable);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/member-remark-instances/{id}/memberId")
    public List<MemberRemarkInstance> getMemberRemarkInstancesByMemberId(@PathVariable String id) {
        log.debug("REST request to get MemberRemarkInstance by MemberId : {}", id);
        return memberRemarkInstanceRepository.findByMemberId(id);
    }

    @GetMapping("/member-remark-instances/{tag}/tag")
    public ResponseEntity<Page<MemberRemarkInstance>> getMemberRemarkInstancesByTag(@PathVariable String tag, Pageable pageable) {
        log.debug("REST request to get pin MemberRemarkInstance by tag :page {}", pageable);
        Page<MemberRemarkInstance> page;
        switch(tag) {
            case("GENERAL"):
                page = memberRemarkInstanceRepository.findByTag(MemberRemarkInstance.RemarkTag.GENERAL, pageable);
                break;
            case("EPA"):
                page = memberRemarkInstanceRepository.findByTag(MemberRemarkInstance.RemarkTag.EPA, pageable);
                break;
            case("CALL_CENTER"):
                page = memberRemarkInstanceRepository.findByTag(MemberRemarkInstance.RemarkTag.CALL_CENTER, pageable);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + tag);
        }
        return ResponseEntity.ok(page);
    }


    @DeleteMapping("/member-remark-instances/{id}")
    public ResponseEntity<Void> deleteMemberRemarkInstance(@PathVariable String id) {
        log.debug("REST request to delete MemberRemarkInstance : {}", id);
        memberRemarkInstanceRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

}
