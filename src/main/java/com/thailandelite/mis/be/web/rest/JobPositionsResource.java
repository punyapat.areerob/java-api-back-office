package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.JobDescription;
import com.thailandelite.mis.model.domain.JobPositions;
import com.thailandelite.mis.be.repository.JobPositionsRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;


@RestController
@RequestMapping("/api")
public class JobPositionsResource {

    private final Logger log = LoggerFactory.getLogger(JobPositionsResource.class);

    private static final String ENTITY_NAME = "jobPositions";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JobPositionsRepository jobPositionsRepository;

    public JobPositionsResource(JobPositionsRepository jobPositionsRepository) {
        this.jobPositionsRepository = jobPositionsRepository;
    }


    @PostMapping("/job-positions")
    public ResponseEntity<JobPositions> createJobPositions(@Valid @RequestBody JobPositions jobPositions) throws URISyntaxException {
        log.debug("REST request to save JobPositions : {}", jobPositions);
        if (jobPositions.getId() != null) {
            throw new BadRequestAlertException("A new jobPositions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JobPositions result = jobPositionsRepository.save(jobPositions);
        return ResponseEntity.created(new URI("/api/job-positions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/job-positions")
    public ResponseEntity<JobPositions> updateJobPositions(@Valid @RequestBody JobPositions jobPositions) throws URISyntaxException {
        log.debug("REST request to update JobPositions : {}", jobPositions);
        if (jobPositions.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JobPositions result = jobPositionsRepository.save(jobPositions);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jobPositions.getId()))
            .body(result);
    }


    @GetMapping("/job-positions/active")
    public List<JobPositions> getAllJobPositions(String teamId) {
        log.debug("REST request to get all JobPositions: {}",teamId);
        if(hasText(teamId)){
            return jobPositionsRepository.findAllByTeamId(teamId);
        }else{
            return jobPositionsRepository.findAll();
        }
    }

    @GetMapping("/job-positions")
    public ResponseEntity<FlowResponse<Page<JobPositions>>>  getAllJobPositions(String teamId, Pageable page) {
        log.debug("REST request to get all JobPositions: {}",teamId);
        Page<JobPositions> jobPositions;
        if(hasText(teamId)){
            jobPositions =  jobPositionsRepository.findAllByTeamId(teamId, page);
        }else{
            jobPositions = jobPositionsRepository.findAll(page);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest()
            ,jobPositions );
        return new ResponseEntity<>(new FlowResponse<>("", 0, jobPositions), headers, HttpStatus.OK);
    }

    @GetMapping("/job-positions/{id}")
    public ResponseEntity<JobPositions> getJobPositions(@PathVariable String id) {
        log.debug("REST request to get JobPositions : {}", id);
        Optional<JobPositions> jobPositions = jobPositionsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jobPositions);
    }


    @DeleteMapping("/job-positions/{id}")
    public ResponseEntity<Void> deleteJobPositions(@PathVariable String id) {
        log.debug("REST request to delete JobPositions : {}", id);
        jobPositionsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
