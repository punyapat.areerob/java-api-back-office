package com.thailandelite.mis.be.web.rest.flows;


import com.thailandelite.mis.be.service.flow.BPApplicationAgentRiskService;
import com.thailandelite.mis.be.service.flow.BPApplicationAgentService;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.SearchRequest;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.annotation.MultipartConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/api/applications-agent-risk/flow")
@RequiredArgsConstructor
@MultipartConfig(location = "/tmp",
    fileSizeThreshold = 0,
    maxFileSize = 7242880,
    maxRequestSize = 20971520)
public class ApplicationAgentRiskFlowResource {

    private final BPApplicationAgentRiskService pbApplicationService;

    public String getFlowDefinitionKey(){
        return  "applicant_by_agent_risk";
    }


    @GetMapping("/{taskDefKey}/actions")
    public Enum<?>[] getActionByTaskDefKey(@PathVariable String taskDefKey) {
        log.debug("REST request to get JobApplication : {}", taskDefKey);
        return pbApplicationService.getTaskByTaskDefKey(taskDefKey);
    }

    @PostMapping
    public ResponseEntity<FlowResponse<Application>> create(@RequestBody Application application) {
        log.debug("REST request to save createApplication : {}", application);
        pbApplicationService.create(getFlowDefinitionKey(), application);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0 , application));
    }

    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }



    @GetMapping("/{caseId}")
    public ResponseEntity<FlowResponse<CaseInfoDTO>> getCaseDetail(@PathVariable String caseId) {
        log.debug("REST request to get JobApplication : {}", caseId);
        CaseInfoDTO caseInfoDTO = pbApplicationService.getDetail(caseId);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0, caseInfoDTO));
    }

    @GetMapping("/process/{processInstanceId}/{taskDefKey}/{action}")
    public ResponseEntity<Object> processTask(@PathVariable String processInstanceId,
                                              @PathVariable String taskDefKey,
                                              @PathVariable String action,
                                              @RequestParam String remark) {
        log.debug("REST request to get JobApplication : {}", processInstanceId);
        pbApplicationService.processTask(processInstanceId, taskDefKey, action, SecurityContextHolder.getContext().getAuthentication().getName(), remark);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0, "success"));
    }


    @GetMapping()
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(SearchRequest searchRequest,
                                                                  @RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                  @RequestParam(defaultValue = "") String status,
                                                                  Pageable pageable) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKey(getFlowDefinitionKey())
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .name(searchRequest.getName())
            .surname(searchRequest.getSurname())
            .from(searchRequest.getFrom())
            .to(searchRequest.getTo())
            .status(status).build();

        Page<CaseInfoDTO> applications = pbApplicationService.queryCaseWithEntity(caseQuery, Application.class);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(), pbApplicationService.queryUnseen(caseQuery), applications ));
    }
}
