package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.BroadcastMessage;
import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.domain.enumeration.BroadcastMessageStatus;
import com.thailandelite.mis.be.repository.BroadcastMessageRepository;
import com.thailandelite.mis.be.service.BroadcastMessageService;
import com.thailandelite.mis.be.service.helper.FormBuilderService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.easycm.framework.form.generator.FormlyFieldConfig;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class BroadcastMessageResource {

    private final Logger log = LoggerFactory.getLogger(BroadcastMessageResource.class);

    private static final String ENTITY_NAME = "broadcastMessage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BroadcastMessageRepository broadcastMessageRepository;
    private final BroadcastMessageService broadcastMessageService;

    private final FormBuilderService formBuilderService;

    @PostMapping("/broadcast-messages")
    public ResponseEntity<BroadcastMessage> createBroadcastMessage(@RequestBody BroadcastMessage broadcastMessage) throws URISyntaxException {
        log.debug("REST request to save BroadcastMessage : {}", broadcastMessage);
        if (broadcastMessage.getId() != null) {
            throw new BadRequestAlertException("A new broadcastMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BroadcastMessage result = broadcastMessageRepository.save(broadcastMessage);
        return ResponseEntity.created(new URI("/api/broadcast-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/broadcast-messages")
    public ResponseEntity<BroadcastMessage> updateBroadcastMessage(@RequestBody BroadcastMessage broadcastMessage) throws URISyntaxException {
        log.debug("REST request to update BroadcastMessage : {}", broadcastMessage);
        if (broadcastMessage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BroadcastMessage result = broadcastMessageRepository.save(broadcastMessage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, broadcastMessage.getId()))
            .body(result);
    }

    @GetMapping("/broadcast-messages")
    public List<BroadcastMessage> getAllBroadcastMessages(BroadcastMessageQuery query) {
        log.debug("REST request to get all BroadcastMessages: {}", query);
        return this.broadcastMessageService.search(query);
    }

    @GetMapping("/broadcast-messages/live")
    public List<BroadcastMessage> getAllBroadcastMessages(String teamId) {
        return this.broadcastMessageService.getLiveMessage(teamId);
    }

    @GetMapping("/broadcast-messages/{id}")
    public ResponseEntity<BroadcastMessage> getBroadcastMessage(@PathVariable String id) {
        log.debug("REST request to get BroadcastMessage : {}", id);
        Optional<BroadcastMessage> broadcastMessage = broadcastMessageRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(broadcastMessage);
    }

    @DeleteMapping("/broadcast-messages/{id}")
    public ResponseEntity<Void> deleteBroadcastMessage(@PathVariable String id) {
        log.debug("REST request to delete BroadcastMessage : {}", id);
        broadcastMessageRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/broadcast-messages/form")
    public List<FormlyFieldConfig> getForm() throws ClassNotFoundException {
        log.debug("REST request to get all EmailTemplates");
        return formBuilderService.build(BroadcastMessage.class);
    }

    @Data
    public static class BroadcastMessageQuery {
        String subject;
        String message;
        BroadcastMessageStatus status;
        String creatorTeamId;
    }
}
