package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.ProductVariantValueRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.ProductVariantGroup;
import com.thailandelite.mis.be.repository.ProductVariantGroupRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.ProductVariantValue;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class ProductVariantGroupResource {

    private final Logger log = LoggerFactory.getLogger(ProductVariantGroupResource.class);

    private static final String ENTITY_NAME = "productVariantGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductVariantGroupRepository productVariantGroupRepository;
    private final ProductVariantValueRepository productVariantValueRepository;

    public ProductVariantGroupResource(ProductVariantGroupRepository productVariantGroupRepository, ProductVariantValueRepository productVariantValueRepository) {
        this.productVariantGroupRepository = productVariantGroupRepository;
        this.productVariantValueRepository = productVariantValueRepository;
    }


    @PostMapping("/product-variant-groups")
    public ResponseEntity<ProductVariantGroup> createProductVariantGroup(@RequestBody ProductVariantGroup productVariantGroup) throws URISyntaxException {
        log.debug("REST request to save ProductVariantGroup : {}", productVariantGroup);
        if (productVariantGroup.getId() != null) {
            throw new BadRequestAlertException("A new productVariantGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductVariantGroup result = productVariantGroupRepository.save(productVariantGroup);
        return ResponseEntity.created(new URI("/api/product-variant-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/product-variant-groups")
    public ResponseEntity<ProductVariantGroup> updateProductVariantGroup(@RequestBody ProductVariantGroup productVariantGroup) throws URISyntaxException {
        log.debug("REST request to update ProductVariantGroup : {}", productVariantGroup);
        if (productVariantGroup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductVariantGroup result = productVariantGroupRepository.save(productVariantGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productVariantGroup.getId()))
            .body(result);
    }


    @GetMapping("/product-variant-groups")
    public List<ProductVariantGroup> getAllProductVariantGroups() {
        log.debug("REST request to get all ProductVariantGroups");
        return productVariantGroupRepository.findAll();
    }

    @GetMapping("/product-variant-groups/product-variant-value")
    public List<ProductVariantGroup> getAllProductVariantGroupsAll() {
        log.debug("REST request to get all ProductVariantGroups");
        List<ProductVariantGroup> productVariantGroup = productVariantGroupRepository.findAll().stream().map(c->{
            List<ProductVariantValue> productVariantValues = productVariantValueRepository.findByProductVariantGroupId(c.getId());
            c.setProductVariantValue(productVariantValues);
            return c;
        }).collect(Collectors.toList());
        return productVariantGroup;
    }

    @GetMapping("/product-variant-groups/product-variant-value/{id}")
    public ResponseEntity<ProductVariantGroup> getAllProductVariantGroupsAndvalue(@PathVariable String id) {
        log.debug("REST request to get all ProductVariantGroupsAndvalue");
        ProductVariantGroup productVariantGroup = productVariantGroupRepository.findById(id).orElseThrow(()->new NotFoundException("gg"));
        List<ProductVariantValue> productVariantValues = productVariantValueRepository.findByProductVariantGroupId(id);
        productVariantGroup.setProductVariantValue(productVariantValues);
        return ResponseEntity.ok().body(productVariantGroup);
    }


    @GetMapping("/product-variant-groups/{id}")
    public ResponseEntity<ProductVariantGroup> getProductVariantGroup(@PathVariable String id) {
        log.debug("REST request to get ProductVariantGroup : {}", id);
        Optional<ProductVariantGroup> productVariantGroup = productVariantGroupRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productVariantGroup);
    }


    @DeleteMapping("/product-variant-groups/{id}")
    public ResponseEntity<Void> deleteProductVariantGroup(@PathVariable String id) {
        log.debug("REST request to delete ProductVariantGroup : {}", id);
        productVariantGroupRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
