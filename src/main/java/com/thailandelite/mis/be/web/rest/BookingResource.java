package com.thailandelite.mis.be.web.rest;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.web.rest.flows.BookingFlowResource;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.booking.TicketForm;
import com.thailandelite.mis.model.domain.booking.dao.BookingRequest;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import com.thailandelite.mis.model.domain.booking.enums.BookingFlowDefinitionKey;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.opensky.api.OpenSkyApi;
import org.opensky.model.OpenSkyStates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.BadRequestException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BookingResource {

    private final Logger log = LoggerFactory.getLogger(BookingResource.class);

    private static final String ENTITY_NAME = "booking";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private final BookingFlowResource bookingFlowResource;
    private final CaseInfoService caseInfoService;

    public BookingResource(
        BookingRepository bookingRepository,
        BookingService bookingService,
        BookingFlowResource bookingFlowResource,
        CaseInfoService caseInfoService) {
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
        this.bookingFlowResource = bookingFlowResource;
        this.caseInfoService = caseInfoService;
    }


    @PostMapping("/bookings")
    public ResponseEntity<Booking> createBooking(@RequestBody Booking booking) throws URISyntaxException {
        log.debug("REST request to save Booking : {}", booking);
        if (booking.getId() != null) {
            throw new BadRequestAlertException("A new booking cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Booking result = bookingRepository.save(booking);
        return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

//    @PostMapping("/bookings/create")
//    public ResponseEntity<Booking> createProductModel(@RequestBody BookingFrom bookingFrom) throws URISyntaxException, DocumentException, IOException, UploadFailException {
//        log.debug("REST request to save booking : {}", bookingFrom);
//        Booking result = bookingService.createBooking(bookingFrom);
//        return ResponseEntity.created(new URI("/api/products/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
//            .body(result);
//    }


    @PutMapping("/bookings")
    public ResponseEntity<Booking> updateBooking(@RequestBody Booking booking) throws URISyntaxException {
        log.debug("REST request to update Booking : {}", booking);
        if (booking.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Booking result = bookingRepository.save(booking);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, booking.getId()))
            .body(result);
    }


    @GetMapping("/bookings")
    public List<Booking> getAllBookings() {
        log.debug("REST request to get all Bookings");
        return bookingRepository.findAll();
    }

    @GetMapping("/bookings/detail")
    public List<Booking> getAllBookingsDetail() {
        log.debug("REST request to get all Bookings");
        return bookingService.getBooking();
    }

    @GetMapping("/bookings/member/{memberId}/pending")
    public List<Booking> getMemberPendingBookingsDetail(@PathVariable String memberId) {
        log.debug("REST request to get all Bookings");
        return bookingService.getPendingBookingByMemberId(memberId);
    }

    @GetMapping("/bookings/detail/{id}")
    public Booking getAllBookingsDetailById(@PathVariable String id) {
        log.debug("REST request to get all Bookings");
        return bookingService.getBookingById(id);
    }

    @GetMapping("/booking/test")
    public OpenSkyStates getAllBookingsteest() throws IOException {
        OpenSkyApi api = new OpenSkyApi("golfdigg1", "golfdigg1234");
        OpenSkyStates os = api.getStates(0, null);
        return os;
    }

    @GetMapping("/bookings/{id}")
    public ResponseEntity<Booking> getBooking(@PathVariable String id) {
        log.debug("REST request to get Booking : {}", id);
        Optional<Booking> booking = bookingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(booking);
    }

    @GetMapping("/bookings/{id}/wrap")
    public ResponseEntity<BookingResponse> getWrapBooking(@PathVariable String id) {
        log.debug("REST request to get Booking : {}", id);
        return ResponseEntity.ok(bookingService.getWrapBookingDetailById(id));
    }

    @DeleteMapping("/bookings/{id}")
    public ResponseEntity<Void> deleteBooking(@PathVariable String id) {
        log.debug("REST request to delete Booking : {}", id);
        bookingRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PostMapping(path = "/bookings/cc/member/{memberId}/create")
    public ResponseEntity<Booking> createMemberBookingByCC(@PathVariable String memberId, @RequestBody BookingRequest bookingRequest) throws Exception {
        BookingFrom bookingFrom = bookingService.createBookingForm("CallCenter", bookingRequest, memberId);
        return createBooking(memberId, bookingFrom);
    }

    @PostMapping("/bookings/member/{memberId}/create")
    public ResponseEntity<Booking> createBooking(@PathVariable String memberId, @RequestBody BookingFrom bookingFrom) throws URISyntaxException, DocumentException, IOException, UploadFailException {
        log.debug("REST request to create Booking : {}", bookingFrom);
        bookingFrom.getBooking().setRequestMemberId(memberId);
        Booking result = bookingService.createBooking(bookingFrom);
        return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/bookings/member/{memberId}/open-ticket")
    public ResponseEntity<Booking> openTicket(@PathVariable String memberId, @RequestBody TicketForm ticketForm) throws URISyntaxException, DocumentException, IOException, UploadFailException {
        log.debug("REST request to create Booking : {}", ticketForm);
        Booking booking = bookingService.openTicket(memberId, ticketForm);
        BookingFlowDefinitionKey flowDefinitionKey = bookingService.getFlowDefinitionKey(booking.getId());
        BookingFrom bookingFrom = new BookingFrom();
        bookingFrom.setBooking(new Booking());
        bookingFrom.getBooking().setId(booking.getId());
        bookingFrom.getBooking().setRequestMemberId(booking.getRequestMemberId());
        bookingFrom.setFlowDefinitionKey(flowDefinitionKey.name());
        Booking result = bookingFlowResource.create(bookingFrom).getBody().getResult();
        bookingRepository.save(result);
        return ResponseEntity.created(new URI("/api/bookings/"+result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/bookings/{id}/submit")
    public ResponseEntity<Booking> submitBooking(@PathVariable String id) throws URISyntaxException, DocumentException, IOException, UploadFailException {
        log.debug("REST request to submit Booking : {}", id);
        Booking booking = bookingRepository.findById(id).orElseThrow(()->new NotFoundException("Not found booking"));

        BookingFlowDefinitionKey flowDefinitionKey = bookingService.getFlowDefinitionKey(id);
        BookingFrom bookingFrom = new BookingFrom();
        bookingFrom.setBooking(new Booking());
        bookingFrom.getBooking().setId(id);
        bookingFrom.getBooking().setRequestMemberId(booking.getRequestMemberId());
        bookingFrom.setFlowDefinitionKey(flowDefinitionKey.name());
        Booking result = bookingFlowResource.create(bookingFrom).getBody().getResult();
        bookingService.deductBooking(booking);
        bookingRepository.save(result);
        return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/bookings/{id}/confirm")
    public ResponseEntity<BookingResponse> confirmBooking(@PathVariable String id) throws URISyntaxException, DocumentException, IOException, UploadFailException {
        log.debug("REST request to submit Booking : {}", id);
        BookingResponse result = confirmServiceBooking(id);
        return ResponseEntity.created(new URI("/api/bookings/" + result.getBookingId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getBookingId()))
            .body(result);
    }

    @PostMapping("/bookings/{id}/cancel")
    public ResponseEntity<BookingResponse> cancelBooking(@PathVariable String id, String reason) throws URISyntaxException, DocumentException, IOException, UploadFailException {
        log.debug("REST request to submit Booking : {}", id);
        BookingResponse result = cancelServiceBooking(id, reason);
        return ResponseEntity.created(new URI("/api/bookings/" + result.getBookingId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getBookingId()))
            .body(result);
    }

    @GetMapping("/products/product-model/fix/keydef")
    public ResponseEntity<String> fixAddKeyDefToBooking() {
        log.debug("REST request to get Product!!");
        bookingService.addKeyDefToBooking();
        return ResponseEntity.ok().body("SUCCESS");
    }

    @PatchMapping("/bookings/booking-service-member/{id}/price/quota")
    public ResponseEntity<BookingServiceMember> updateBookingServiceMemberPriceAndQuota(@PathVariable String id, Float price, Integer quota) {
        return ResponseEntity.ok().body(bookingService.changePriceAndQuota(id, price, quota));
    }

//    @PutMapping("/bookings/{id}/booking-member")
//    public ResponseEntity<BookingResponse> updateBooking(@PathVariable String id, @RequestBody UpdateBookingRequest updateBookingRequest) throws URISyntaxException, DocumentException, IOException, UploadFailException {
//        log.debug("REST request to submit Booking : {}", id);
//        BookingFrom bookingFrom = new BookingFrom();
//        bookingFrom.setBooking(new Booking());
//        bookingFrom.getBooking().setId(id);
//        bookingFrom.setFlowDefinitionKey("booking_Golf");
//        Booking result = bookingFlowResource.create(bookingFrom).getBody().getResult();
//        bookingRepository.save(result);
//        return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
//            .body(result);
//    }

    private BookingResponse cancelServiceBooking(String bookingId, String reason) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(()-> new NotFoundException("Not found your booking"));
        CaseInfo caseInfo = caseInfoService.findByEntityId(bookingId);
        String flowDefKey = caseInfo.getFlowDefKey();
        String caseId = caseInfo.getId();
        ResponseEntity<FlowResponse<CaseInfoDTO>> caseInfoRes = bookingFlowResource.getApplicationTask(flowDefKey, caseId);
        CaseInfoDTO caseInfoDto = caseInfoRes.getBody().getResult();
        Enum<?>[] actions = caseInfoDto.getActions();
        String action = "";
        for (Enum<?> actionCase : actions) {
            if("USERCANCEL".equalsIgnoreCase(actionCase.name())){
                action = "USERCANCEL";
            }
        }
        if(StringUtils.isBlank(action))
            throw new BadRequestException("Not found confirm action.");

        String pInstanceId = caseInfoDto.getProcessInstanceId();
        String taskDefKey = caseInfoDto.getTaskDefKey();
        String remark = reason;
        bookingFlowResource.processTask(flowDefKey, pInstanceId, taskDefKey, action, remark);
        return bookingService.wrapBooking(booking);
    }

    private BookingResponse confirmServiceBooking(String bookingId) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(()-> new NotFoundException("Not found your booking"));
        CaseInfo caseInfo = caseInfoService.findByEntityId(bookingId);
        String flowDefKey = caseInfo.getFlowDefKey();
        String caseId = caseInfo.getId();
        ResponseEntity<FlowResponse<CaseInfoDTO>> caseInfoRes = bookingFlowResource.getApplicationTask(flowDefKey, caseId);
        CaseInfoDTO caseInfoDto = caseInfoRes.getBody().getResult();
        Enum<?>[] actions = caseInfoDto.getActions();
        String action = "";
        for (Enum<?> actionCase : actions) {
            if("APPROVE".equalsIgnoreCase(actionCase.name())){
                action = "APPROVE";
            }else if("USERCONFIRM".equalsIgnoreCase(actionCase.name())){
                action = "USERCONFIRM";
            }
        }
        if(StringUtils.isBlank(action))
            throw new BadRequestException("Not found confirm action.");

        String pInstanceId = caseInfoDto.getProcessInstanceId();
        String taskDefKey = caseInfoDto.getTaskDefKey();
        String remark = "";
        bookingFlowResource.processTask(flowDefKey, pInstanceId, taskDefKey, action, remark);
        return bookingService.wrapBooking(booking);
    }
}
