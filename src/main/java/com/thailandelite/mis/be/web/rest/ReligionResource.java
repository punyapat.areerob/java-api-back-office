package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.Religion;
import com.thailandelite.mis.be.repository.ReligionRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ReligionResource {

    private final Logger log = LoggerFactory.getLogger(ReligionResource.class);

    private static final String ENTITY_NAME = "religion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReligionRepository religionRepository;

    public ReligionResource(ReligionRepository religionRepository) {
        this.religionRepository = religionRepository;
    }


    @PostMapping("/religions")
    public ResponseEntity<Religion> createReligion(@RequestBody Religion religion) throws URISyntaxException {
        log.debug("REST request to save Religion : {}", religion);
        if (religion.getId() != null) {
            throw new BadRequestAlertException("A new religion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Religion result = religionRepository.save(religion);
        return ResponseEntity.created(new URI("/api/religions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/religions")
    public ResponseEntity<Religion> updateReligion(@RequestBody Religion religion) throws URISyntaxException {
        log.debug("REST request to update Religion : {}", religion);
        if (religion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Religion result = religionRepository.save(religion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, religion.getId()))
            .body(result);
    }


    @GetMapping("/religions")
    public List<Religion> getAllReligions() {
        log.debug("REST request to get all Religions");
        return religionRepository.findAll();
    }


    @GetMapping("/religions/{id}")
    public ResponseEntity<Religion> getReligion(@PathVariable String id) {
        log.debug("REST request to get Religion : {}", id);
        Optional<Religion> religion = religionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(religion);
    }


    @DeleteMapping("/religions/{id}")
    public ResponseEntity<Void> deleteReligion(@PathVariable String id) {
        log.debug("REST request to delete Religion : {}", id);
        religionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
