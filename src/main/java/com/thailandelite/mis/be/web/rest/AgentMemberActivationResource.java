package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentMemberActivation;
import com.thailandelite.mis.be.repository.AgentMemberActivationRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class AgentMemberActivationResource {

    private final Logger log = LoggerFactory.getLogger(AgentMemberActivationResource.class);

    private static final String ENTITY_NAME = "agentMemberActivation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentMemberActivationRepository agentMemberActivationRepository;

    public AgentMemberActivationResource(AgentMemberActivationRepository agentMemberActivationRepository) {
        this.agentMemberActivationRepository = agentMemberActivationRepository;
    }


    @PostMapping("/agent-member-activations")
    public ResponseEntity<AgentMemberActivation> createAgentMemberActivation(@RequestBody AgentMemberActivation agentMemberActivation) throws URISyntaxException {
        log.debug("REST request to save AgentMemberActivation : {}", agentMemberActivation);
        if (agentMemberActivation.getId() != null) {
            throw new BadRequestAlertException("A new agentMemberActivation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentMemberActivation result = agentMemberActivationRepository.save(agentMemberActivation);
        return ResponseEntity.created(new URI("/api/agent-member-activations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-member-activations")
    public ResponseEntity<AgentMemberActivation> updateAgentMemberActivation(@RequestBody AgentMemberActivation agentMemberActivation) throws URISyntaxException {
        log.debug("REST request to update AgentMemberActivation : {}", agentMemberActivation);
        if (agentMemberActivation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentMemberActivation result = agentMemberActivationRepository.save(agentMemberActivation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentMemberActivation.getId()))
            .body(result);
    }


    @GetMapping("/agent-member-activations")
    public List<AgentMemberActivation> getAllAgentMemberActivations() {
        log.debug("REST request to get all AgentMemberActivations");
        return agentMemberActivationRepository.findAll();
    }


    @GetMapping("/agent-member-activations/{id}")
    public ResponseEntity<AgentMemberActivation> getAgentMemberActivation(@PathVariable String id) {
        log.debug("REST request to get AgentMemberActivation : {}", id);
        Optional<AgentMemberActivation> agentMemberActivation = agentMemberActivationRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentMemberActivation);
    }


    @DeleteMapping("/agent-member-activations/{id}")
    public ResponseEntity<Void> deleteAgentMemberActivation(@PathVariable String id) {
        log.debug("REST request to delete AgentMemberActivation : {}", id);
        agentMemberActivationRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
