package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.BloodType;
import com.thailandelite.mis.be.repository.BloodTypeRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BloodTypeResource {

    private final Logger log = LoggerFactory.getLogger(BloodTypeResource.class);

    private static final String ENTITY_NAME = "bloodType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BloodTypeRepository bloodTypeRepository;

    public BloodTypeResource(BloodTypeRepository bloodTypeRepository) {
        this.bloodTypeRepository = bloodTypeRepository;
    }


    @PostMapping("/blood-types")
    public ResponseEntity<BloodType> createBloodType(@RequestBody BloodType bloodType) throws URISyntaxException {
        log.debug("REST request to save BloodType : {}", bloodType);
        if (bloodType.getId() != null) {
            throw new BadRequestAlertException("A new bloodType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BloodType result = bloodTypeRepository.save(bloodType);
        return ResponseEntity.created(new URI("/api/blood-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/blood-types")
    public ResponseEntity<BloodType> updateBloodType(@RequestBody BloodType bloodType) throws URISyntaxException {
        log.debug("REST request to update BloodType : {}", bloodType);
        if (bloodType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BloodType result = bloodTypeRepository.save(bloodType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bloodType.getId()))
            .body(result);
    }


    @GetMapping("/blood-types")
    public List<BloodType> getAllBloodTypes() {
        log.debug("REST request to get all BloodTypes");
        return bloodTypeRepository.findAll();
    }


    @GetMapping("/blood-types/{id}")
    public ResponseEntity<BloodType> getBloodType(@PathVariable String id) {
        log.debug("REST request to get BloodType : {}", id);
        Optional<BloodType> bloodType = bloodTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bloodType);
    }


    @DeleteMapping("/blood-types/{id}")
    public ResponseEntity<Void> deleteBloodType(@PathVariable String id) {
        log.debug("REST request to delete BloodType : {}", id);
        bloodTypeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
