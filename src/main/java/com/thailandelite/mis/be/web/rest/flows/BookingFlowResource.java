package com.thailandelite.mis.be.web.rest.flows;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.service.flow.*;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.SearchRequest;
import com.thailandelite.mis.model.domain.booking.enums.BookingFlowDefinitionKey;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

@Slf4j
@RestController
@RequestMapping("/api/booking/flow")
@RequiredArgsConstructor
public class BookingFlowResource {
    private final BPBookingEPAService bpBookingEPAService;
    private final BPBookingGolfService bpBookingGolfService;
    private final BPBookingSPS bpBookingSPSService;
    private final BPBooking90DayService bpBooking90DayService;
    private final BPBookingStayExtensionService bpBookingStayExtensionService;
    private final BPBookingVisaService bpBookingVisaService;
    private final BPBookingDrivingLicenseService bpBookingDrivingLicenseService;
    private final BPBookingBOIService bpBookingBOIService;
    private final BPBookingAirport bpBookingAirport;
    private final BPBookingBankEPLService bpBookingBankEPLService;
    private final BPBookingHospitalService bpBookingHospitalService;
    private final BPBookingOtherService bpBookingOtherService;
    private final BPBookingLoungeService bpBookingLoungeService;
    private final BPBookingTicketService bpBookingTicketService;
    private final BPBooking bpBooking;

    @GetMapping
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(SearchRequest searchRequest,
                                                                  @RequestParam(defaultValue = "") String[] flowDefinitionKey,
                                                                  @RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                  @RequestParam(defaultValue = "") String status,
                                                                  Pageable pageable
    ) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKeys(flowDefinitionKey)
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .vendorId(searchRequest.getVendorId())
            .from(searchRequest.getFrom())
            .to(searchRequest.getTo())
            .search(searchRequest.getSearch())
            .bookingStatus(searchRequest.getBookingStatus())
            .isArrival(searchRequest.getIsArrival())
            .status(status).build();

        Page<CaseInfoDTO> caseInfoDTOS = bpBooking.queryCaseBooking(caseQuery, Booking.class);
        return ResponseEntity.ok(new FlowResponse<>(Arrays.stream(flowDefinitionKey).findFirst().orElseGet(new Supplier<String>() {
            @Override
            public String get() {
                return "ALL";
            }
        }), 0, caseInfoDTOS ));
    }



    @GetMapping("/signature/{bookingId}")
    public ResponseEntity<FlowResponse<String>> getSignatureByBooking(@PathVariable String bookingId) {
        log.debug("REST request to get JobApplication : {}", bookingId);
        String signature = bpBooking.getSignature(bookingId);
        return ResponseEntity.ok(new FlowResponse<>("ALL" ,0 , signature));
    }




    @GetMapping("/{flowDefinitionKey}/{caseId}")
    public ResponseEntity<FlowResponse<CaseInfoDTO>> getApplicationTask(@PathVariable String flowDefinitionKey,@PathVariable String caseId) {
        log.debug("REST request to get JobApplication : {}", caseId);
        CaseInfoDTO caseInfoDTO = bpServiceFactory(flowDefinitionKey).getDetail(caseId);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey ,0 , caseInfoDTO));
    }

    @GetMapping("/{flowDefinitionKey}/{taskDefKey}/actions")
    public Enum<?>[] getActionByTaskDefKey(@PathVariable String flowDefinitionKey,@PathVariable String taskDefKey) {
        log.debug("REST request to get JobApplication : {}", taskDefKey);
        return bpServiceFactory(flowDefinitionKey).getTaskByTaskDefKey(taskDefKey);
    }

    @PostMapping
    public ResponseEntity<FlowResponse<Booking>> create(@RequestBody BookingFrom bookingFrom) throws UploadFailException, DocumentException, IOException {
            Booking booking = (Booking) bpServiceFactory(bookingFrom.getFlowDefinitionKey()).create(bookingFrom.getFlowDefinitionKey(), bookingFrom);
        log.debug("REST request to save createApplication : {}", booking);
        return ResponseEntity.ok(new FlowResponse<>(bookingFrom.getFlowDefinitionKey(),0 ,booking));
    }

    @GetMapping("/process/{flowDefinitionKey}/{processInstanceId}/{taskDefKey}/{action}")
    public ResponseEntity<Object> processTask( @PathVariable String flowDefinitionKey,
                                              @PathVariable String processInstanceId,
                                              @PathVariable String taskDefKey,
                                              @PathVariable String action,
                                              @RequestParam String remark) {
        log.debug("REST request to get JobApplication : {}", processInstanceId);
        bpServiceFactory(flowDefinitionKey).processTask(processInstanceId, taskDefKey, action, SecurityContextHolder.getContext().getAuthentication().getName(), remark);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey,0, "success"));
    }

    public BPService<Booking> bpServiceFactory(String flowDefinitionKey){
        if (BookingFlowDefinitionKey.EPA.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingEPAService;
        else if (BookingFlowDefinitionKey.GOLF.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingGolfService;
        else if (BookingFlowDefinitionKey.SPA.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingSPSService;
        else if (BookingFlowDefinitionKey.NINETY_REPORT.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBooking90DayService;
        else if (BookingFlowDefinitionKey.STAY_EXTENSION.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingStayExtensionService;
        else if (BookingFlowDefinitionKey.VISA.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingVisaService;
        else if (BookingFlowDefinitionKey.DRIVING.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingDrivingLicenseService;
        else if (BookingFlowDefinitionKey.BOI.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingBOIService;
        else if (BookingFlowDefinitionKey.AIRPORT_TRANSFER.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingAirport;
        else if (BookingFlowDefinitionKey.HOSPITAL.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingHospitalService;
        else if (BookingFlowDefinitionKey.BANK_EPL.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingBankEPLService;
        else if (BookingFlowDefinitionKey.LOUNGE.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingLoungeService;
        else if (BookingFlowDefinitionKey.OTHER.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingOtherService;
        else if (BookingFlowDefinitionKey.TICKET.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBookingTicketService;
        else if (BookingFlowDefinitionKey.ALL.name().equalsIgnoreCase(flowDefinitionKey) )
            return bpBooking;
        else
            throw new IllegalStateException("Unexpected value: " + flowDefinitionKey);
    }
}
