package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.SubDistrict;
import com.thailandelite.mis.be.repository.SubDistrictRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class SubDistrictResource {

    private final Logger log = LoggerFactory.getLogger(SubDistrictResource.class);

    private static final String ENTITY_NAME = "subDistrict";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubDistrictRepository subDistrictRepository;

    public SubDistrictResource(SubDistrictRepository subDistrictRepository) {
        this.subDistrictRepository = subDistrictRepository;
    }


    @PostMapping("/sub-districts")
    public ResponseEntity<SubDistrict> createSubDistrict(@RequestBody SubDistrict subDistrict) throws URISyntaxException {
        log.debug("REST request to save SubDistrict : {}", subDistrict);
        if (subDistrict.getId() != null) {
            throw new BadRequestAlertException("A new subDistrict cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubDistrict result = subDistrictRepository.save(subDistrict);
        return ResponseEntity.created(new URI("/api/sub-districts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/sub-districts")
    public ResponseEntity<SubDistrict> updateSubDistrict(@RequestBody SubDistrict subDistrict) throws URISyntaxException {
        log.debug("REST request to update SubDistrict : {}", subDistrict);
        if (subDistrict.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubDistrict result = subDistrictRepository.save(subDistrict);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subDistrict.getId()))
            .body(result);
    }


    @GetMapping("/sub-districts")
    public List<SubDistrict> getAllSubDistricts() {
        log.debug("REST request to get all SubDistricts");
        return subDistrictRepository.findAll();
    }


    @GetMapping("/sub-districts/{id}")
    public ResponseEntity<SubDistrict> getSubDistrict(@PathVariable String id) {
        log.debug("REST request to get SubDistrict : {}", id);
        Optional<SubDistrict> subDistrict = subDistrictRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(subDistrict);
    }


    @DeleteMapping("/sub-districts/{id}")
    public ResponseEntity<Void> deleteSubDistrict(@PathVariable String id) {
        log.debug("REST request to delete SubDistrict : {}", id);
        subDistrictRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
