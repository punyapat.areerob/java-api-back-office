package com.thailandelite.mis.be.web.rest.flows.model;

import lombok.Data;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Data
public class CaseQuery {
    private String flowDefKey;
    private List<String> processInstanceIds;
    private List<String> taskDefKeys;
    private String status;
    private String name;
    private String surname;
    private String search;
    private String vendorId;
    private String from;
    private String to;
    private Boolean isArrival;
    private String packageAction;
    private List<String> bookingStatus;
    private List<String> statuses;
    private Boolean finished;
    private String memberId;
    private Pageable pageable;
    private String[] flowDefKeys;
}
