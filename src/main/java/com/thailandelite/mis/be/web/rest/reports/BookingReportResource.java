package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.BookingReportService;
import com.thailandelite.mis.model.FlowResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class BookingReportResource {

    private final BookingReportService reportService;

    @GetMapping("/reports/booking-info")
    public ResponseEntity<FlowResponse<Page<BookingReportResponse>>> getPaymentInfoReports(BookingReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getBookingReport(searchRequest,pageable) ));
    }

    @GetMapping("/reports/booking-info/excel")
    public byte[] getPaymentInfoExcel(BookingReportRequest searchRequest, Pageable pageable) {
        return reportService.generateExcelBookingReport(searchRequest);
    }

}
