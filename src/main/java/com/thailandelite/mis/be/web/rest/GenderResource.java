package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.cdr.domain.Cdr;
import com.thailandelite.mis.be.cdr.repository.CdrRepository;
import com.thailandelite.mis.be.service.JAService;
import com.thailandelite.mis.model.domain.booking.JaFrom;
import com.thailandelite.mis.model.domain.master.Gender;
import com.thailandelite.mis.be.repository.GenderRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class GenderResource {

    private final Logger log = LoggerFactory.getLogger(GenderResource.class);

    private static final String ENTITY_NAME = "gender";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GenderRepository genderRepository;
    private final CdrRepository cdrRepository;
    private final JAService jaService;
    @PostMapping("/genders")
    public ResponseEntity<Gender> createGender(@RequestBody Gender gender) throws URISyntaxException {
        log.debug("REST request to save Gender : {}", gender);
        if (gender.getId() != null) {
            throw new BadRequestAlertException("A new gender cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Gender result = genderRepository.save(gender);
        return ResponseEntity.created(new URI("/api/genders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/genders")
    public ResponseEntity<Gender> updateGender(@RequestBody Gender gender) throws URISyntaxException {
        log.debug("REST request to update Gender : {}", gender);
        if (gender.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Gender result = genderRepository.save(gender);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, gender.getId()))
            .body(result);
    }


    @GetMapping("/genders")
    public List<Gender> getAllGenders() {
        log.debug("REST request to get all Genders");
        return genderRepository.findAll();
    }


    @GetMapping("/genders/{id}")
    public ResponseEntity<Gender> getGender(@PathVariable String id) {
        log.debug("REST request to get Gender : {}", id);
        Optional<Gender> gender = genderRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(gender);
    }


    @DeleteMapping("/genders/{id}")
    public ResponseEntity<Void> deleteGender(@PathVariable String id) {
        log.debug("REST request to delete Gender : {}", id);
        genderRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }


    @GetMapping("/cdrs")
    public List<Cdr> getAllCdr(Pageable pageable) {
        log.debug("REST request to get all Genders");
        return cdrRepository.findAll(pageable).getContent();
    }
    @GetMapping("/genders/jobAssingment")
    public List<JaFrom> getAllJobAssingment() {
        log.debug("REST request to get all jobAssingment");
        return jaService.getJAAll();
    }
}
