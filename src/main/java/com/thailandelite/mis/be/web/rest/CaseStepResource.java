package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CaseStep;
import com.thailandelite.mis.be.repository.CaseStepRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CaseStepResource {

    private final Logger log = LoggerFactory.getLogger(CaseStepResource.class);

    private static final String ENTITY_NAME = "caseStep";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseStepRepository caseStepRepository;

    public CaseStepResource(CaseStepRepository caseStepRepository) {
        this.caseStepRepository = caseStepRepository;
    }


    @PostMapping("/case-steps")
    public ResponseEntity<CaseStep> createCaseStep(@RequestBody CaseStep caseStep) throws URISyntaxException {
        log.debug("REST request to save CaseStep : {}", caseStep);
        if (caseStep.getId() != null) {
            throw new BadRequestAlertException("A new caseStep cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseStep result = caseStepRepository.save(caseStep);
        return ResponseEntity.created(new URI("/api/case-steps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/case-steps")
    public ResponseEntity<CaseStep> updateCaseStep(@RequestBody CaseStep caseStep) throws URISyntaxException {
        log.debug("REST request to update CaseStep : {}", caseStep);
        if (caseStep.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseStep result = caseStepRepository.save(caseStep);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseStep.getId()))
            .body(result);
    }


    @GetMapping("/case-steps")
    public List<CaseStep> getAllCaseSteps() {
        log.debug("REST request to get all CaseSteps");
        return caseStepRepository.findAll();
    }


    @GetMapping("/case-steps/{id}")
    public ResponseEntity<CaseStep> getCaseStep(@PathVariable String id) {
        log.debug("REST request to get CaseStep : {}", id);
        Optional<CaseStep> caseStep = caseStepRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseStep);
    }


    @DeleteMapping("/case-steps/{id}")
    public ResponseEntity<Void> deleteCaseStep(@PathVariable String id) {
        log.debug("REST request to delete CaseStep : {}", id);
        caseStepRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
