package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.common.Utils;
import com.thailandelite.mis.be.domain.CommissionGroupIssue;
import com.thailandelite.mis.be.domain.CommissionIssue;
import com.thailandelite.mis.be.domain.CommissionIssueItem;
import com.thailandelite.mis.be.repository.CommissionGroupIssueRepository;
import com.thailandelite.mis.be.repository.CommissionIssueItemRepository;
import com.thailandelite.mis.be.repository.CommissionIssueRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CommissionGroupIssueResource {

    private final Logger log = LoggerFactory.getLogger(CommissionGroupIssueResource.class);

    private static final String ENTITY_NAME = "commissionGroupIssue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionGroupIssueRepository commissionGroupIssueRepository;
    private final CommissionIssueRepository commissionIssueRepository;
    private final CommissionIssueItemRepository commissionIssueItemRepository;

    @PostMapping("/commission-group-issues")
    public ResponseEntity<CommissionGroupIssue> createCommissionGroupIssue(@RequestBody CommissionGroupIssue commissionGroupIssue) throws URISyntaxException {
        log.debug("REST request to save CommissionGroupIssue : {}", commissionGroupIssue);
        if (commissionGroupIssue.getId() != null) {
            throw new BadRequestAlertException("A new commissionGroupIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionGroupIssue result = commissionGroupIssueRepository.save(commissionGroupIssue);
        return ResponseEntity.created(new URI("/api/commission-group-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/commission-group-issues")
    public ResponseEntity<CommissionGroupIssue> updateCommissionGroupIssue(@RequestBody CommissionGroupIssue commissionGroupIssue) throws URISyntaxException {
        log.debug("REST request to update CommissionGroupIssue : {}", commissionGroupIssue);
        if (commissionGroupIssue.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommissionGroupIssue result = commissionGroupIssueRepository.save(commissionGroupIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionGroupIssue.getId()))
            .body(result);
    }

    @GetMapping("/commission-group-issues")
    public Page<CommissionGroupIssue> getAllCommissionGroupIssues(String agentId, Pageable pageable) {
        log.debug("REST request to get all CommissionGroupIssues");
        return commissionGroupIssueRepository.findAllByAgent_Id(agentId, pageable);
    }

    @GetMapping("/commission-group-issues/{id}")
    public ResponseEntity<CommissionGroupIssue> getCommissionGroupIssue(@PathVariable String id) {
        log.debug("REST request to get CommissionGroupIssue : {}", id);
        CommissionGroupIssue commissionGroupIssue = commissionGroupIssueRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("CommissionGroupIssue"));

        List<CommissionIssue> issueList =
            commissionIssueRepository.findAllByCommissionGroupIssue_Id(id)
                .stream().peek(issue -> {
                List<CommissionIssueItem> items = commissionIssueItemRepository.findByCommissionIssue_Id(issue.getId());
                issue.set_items(items);
            }).collect(Collectors.toList());

        commissionGroupIssue.set_issues(issueList);

        return ResponseEntity.ok(commissionGroupIssue);
    }

    @DeleteMapping("/commission-group-issues/{id}")
    public ResponseEntity<Void> deleteCommissionGroupIssue(@PathVariable String id) {
        log.debug("REST request to delete CommissionGroupIssue : {}", id);
        commissionGroupIssueRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
