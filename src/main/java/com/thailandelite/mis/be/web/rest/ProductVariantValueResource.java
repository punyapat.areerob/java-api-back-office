package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.ProductVariantValue;
import com.thailandelite.mis.be.repository.ProductVariantValueRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ProductVariantValueResource {

    private final Logger log = LoggerFactory.getLogger(ProductVariantValueResource.class);

    private static final String ENTITY_NAME = "productVariantValue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductVariantValueRepository productVariantValueRepository;

    public ProductVariantValueResource(ProductVariantValueRepository productVariantValueRepository) {
        this.productVariantValueRepository = productVariantValueRepository;
    }


    @PostMapping("/product-variant-values")
    public ResponseEntity<ProductVariantValue> createProductVariantValue(@RequestBody ProductVariantValue productVariantValue) throws URISyntaxException {
        log.debug("REST request to save ProductVariantValue : {}", productVariantValue);
        if (productVariantValue.getId() != null) {
            throw new BadRequestAlertException("A new productVariantValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductVariantValue result = productVariantValueRepository.save(productVariantValue);
        return ResponseEntity.created(new URI("/api/product-variant-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/product-variant-values")
    public ResponseEntity<ProductVariantValue> updateProductVariantValue(@RequestBody ProductVariantValue productVariantValue) throws URISyntaxException {
        log.debug("REST request to update ProductVariantValue : {}", productVariantValue);
        if (productVariantValue.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductVariantValue result = productVariantValueRepository.save(productVariantValue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productVariantValue.getId()))
            .body(result);
    }


    @GetMapping("/product-variant-values")
    public List<ProductVariantValue> getAllProductVariantValues() {
        log.debug("REST request to get all ProductVariantValues");
        return productVariantValueRepository.findAll();
    }


    @GetMapping("/product-variant-values/{id}")
    public ResponseEntity<ProductVariantValue> getProductVariantValue(@PathVariable String id) {
        log.debug("REST request to get ProductVariantValue : {}", id);
        Optional<ProductVariantValue> productVariantValue = productVariantValueRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productVariantValue);
    }


    @DeleteMapping("/product-variant-values/{id}")
    public ResponseEntity<Void> deleteProductVariantValue(@PathVariable String id) {
        log.debug("REST request to delete ProductVariantValue : {}", id);
        productVariantValueRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
