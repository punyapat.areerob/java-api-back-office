package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.NationalitiesRepository;
import com.thailandelite.mis.be.service.NotificationService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.Nationality;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class NotificationResource {

    private final Logger log = LoggerFactory.getLogger(NotificationResource.class);

    private static final String ENTITY_NAME = "notification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotificationService notificationService;

    public NotificationResource(NotificationService notificationService) {
        this.notificationService = notificationService;
    }


    @PostMapping("/notification/welcome")
    public void sendWelcome( @RequestParam String to, @RequestParam String firstName, @RequestParam String lastName, @RequestParam String middle, @RequestParam String detail) throws IOException {
        log.debug("REST send Welcome message");
        notificationService.sendWelcomeLetter(to, firstName, middle,lastName, detail);
    }

    @GetMapping("/notification/reset-password")
    public void test(@RequestParam String email, @RequestParam String firstName, @RequestParam String lastName, @RequestParam String middle, @RequestParam String duedate) throws IOException {
        log.debug("REST request to get all EmailTemplates");
        notificationService.resetPasswordEmail(email, firstName, middle,lastName, duedate);
    }
}


