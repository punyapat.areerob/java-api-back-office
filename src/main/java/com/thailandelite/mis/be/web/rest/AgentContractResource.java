package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentContract;
import com.thailandelite.mis.be.repository.AgentContractRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.thailandelite.mis.be.common.Utils.getExample;

@RestController
@RequestMapping("/api")
public class AgentContractResource {

    private final Logger log = LoggerFactory.getLogger(AgentContractResource.class);

    private static final String ENTITY_NAME = "agentContract";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentContractRepository agentContractRepository;

    public AgentContractResource(AgentContractRepository agentContractRepository) {
        this.agentContractRepository = agentContractRepository;
    }


    @PostMapping("/agent-contracts")
    public ResponseEntity<AgentContract> createAgentContract(@RequestBody AgentContract agentContract) throws URISyntaxException {
        log.debug("REST request to save AgentContract : {}", agentContract);
        if (agentContract.getId() != null) {
            throw new BadRequestAlertException("A new agentContract cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentContract result = agentContractRepository.save(agentContract);
        return ResponseEntity.created(new URI("/api/agent-contracts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-contracts")
    public ResponseEntity<AgentContract> updateAgentContract(@RequestBody AgentContract agentContract) throws URISyntaxException {
        log.debug("REST request to update AgentContract : {}", agentContract);
        if (agentContract.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentContract result = agentContractRepository.save(agentContract);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentContract.getId()))
            .body(result);
    }


    @GetMapping("/agent-contracts")
    public List<AgentContract> getAllAgentContracts(@ModelAttribute AgentContract cardPrivilege) {
        log.debug("REST request to get all AgentContracts: {}", cardPrivilege);
        Example<AgentContract> example = getExample(cardPrivilege);
        return agentContractRepository.findAll(example);
    }


    @GetMapping("/agent-contracts/{id}")
    public ResponseEntity<AgentContract> getAgentContract(@PathVariable String id) {
        log.debug("REST request to get AgentContract : {}", id);
        Optional<AgentContract> agentContract = agentContractRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentContract);
    }


    @DeleteMapping("/agent-contracts/{id}")
    public ResponseEntity<Void> deleteAgentContract(@PathVariable String id) {
        log.debug("REST request to delete AgentContract : {}", id);
        agentContractRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
