package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentTerritory;
import com.thailandelite.mis.be.repository.AgentTerritoryRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class AgentTerritoryResource {

    private final Logger log = LoggerFactory.getLogger(AgentTerritoryResource.class);

    private static final String ENTITY_NAME = "agentTerritory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentTerritoryRepository agentTerritoryRepository;

    public AgentTerritoryResource(AgentTerritoryRepository agentTerritoryRepository) {
        this.agentTerritoryRepository = agentTerritoryRepository;
    }


    @PostMapping("/agent-territories")
    public ResponseEntity<AgentTerritory> createAgentTerritory(@RequestBody AgentTerritory agentTerritory) throws URISyntaxException {
        log.debug("REST request to save AgentTerritory : {}", agentTerritory);
        if (agentTerritory.getId() != null) {
            throw new BadRequestAlertException("A new agentTerritory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentTerritory result = agentTerritoryRepository.save(agentTerritory);
        return ResponseEntity.created(new URI("/api/agent-territories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-territories")
    public ResponseEntity<AgentTerritory> updateAgentTerritory(@RequestBody AgentTerritory agentTerritory) throws URISyntaxException {
        log.debug("REST request to update AgentTerritory : {}", agentTerritory);
        if (agentTerritory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentTerritory result = agentTerritoryRepository.save(agentTerritory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentTerritory.getId()))
            .body(result);
    }


    @GetMapping("/agent-territories")
    public List<AgentTerritory> getAllAgentTerritories() {
        log.debug("REST request to get all AgentTerritories");
        return agentTerritoryRepository.findAll();
    }


    @GetMapping("/agent-territories/{id}")
    public ResponseEntity<AgentTerritory> getAgentTerritory(@PathVariable String id) {
        log.debug("REST request to get AgentTerritory : {}", id);
        Optional<AgentTerritory> agentTerritory = agentTerritoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentTerritory);
    }


    @DeleteMapping("/agent-territories/{id}")
    public ResponseEntity<Void> deleteAgentTerritory(@PathVariable String id) {
        log.debug("REST request to delete AgentTerritory : {}", id);
        agentTerritoryRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
