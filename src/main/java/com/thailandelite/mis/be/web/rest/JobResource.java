package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.web.rest.flows.JobApplicationResource;
import com.thailandelite.mis.model.domain.JobApplication;
import com.thailandelite.mis.model.domain.master.Department;
import com.thailandelite.mis.model.domain.master.Education;
import com.thailandelite.mis.model.domain.master.LineOfWork;
import com.thailandelite.mis.model.domain.master.Team;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.util.StringUtils.hasText;

@RestController
@RequestMapping("/api")
public class JobResource {

    private final JobApplicationResource jobApplicationResource;
    private final JobApplicationRepository jobApplicationRepository;
    private final DepartmentRepository departmentRepository;
    private final TeamRepository teamRepository;
    private final EducationRepository educationRepository;
    private final LineOfWorkRepository lineOfWorkRepository;

    public JobResource(JobApplicationResource jobApplicationResource, JobApplicationRepository jobApplicationRepository, DepartmentRepository departmentRepository, TeamRepository teamRepository, EducationRepository educationRepository, LineOfWorkRepository lineOfWorkRepository) {
        this.jobApplicationResource = jobApplicationResource;
        this.jobApplicationRepository = jobApplicationRepository;
        this.departmentRepository = departmentRepository;
        this.teamRepository = teamRepository;
        this.educationRepository = educationRepository;
        this.lineOfWorkRepository = lineOfWorkRepository;
    }

    @PostMapping("/jobs/application")
    public ResponseEntity<JobApplication> createJobApplication(@RequestBody JobApplication jobApplication) {
        jobApplication = jobApplicationRepository.save(jobApplication);
        //TODO Create job application on bpm.
        jobApplicationResource.create(jobApplication);
        return ResponseEntity.ok(jobApplication);
    }

    @PutMapping("/jobs/application/{id}/document")
    public ResponseEntity<JobApplication> updateJobApplicationDocument(@PathVariable String id,
                                                                       @RequestBody JobApplication jobApplicationReq) {
        JobApplication jobApplication = jobApplicationRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found job application."));
        jobApplication.setDocument(jobApplicationReq.getDocument());
        jobApplicationRepository.save(jobApplication);
        return ResponseEntity.ok(jobApplication);
    }


    @GetMapping("/jobs/ms/emp-department")
    public List<Department> fetchDepartment() {
        return departmentRepository.findAll();
    }

    @GetMapping("/jobs/ms/emp-team")
    public List<Team> fetchTeam() {
        return teamRepository.findAll();
    }

    @GetMapping("/jobs/ms/emp-education")
    public List<Education> fetchEducation() {
        return educationRepository.findAll();
    }

    @GetMapping("/jobs/ms/emp-line-of-work")
    public List<LineOfWork> fetchLineOfWork() {
        return lineOfWorkRepository.findAll();
    }


}
