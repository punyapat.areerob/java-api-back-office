package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.AccommodationType;
import com.thailandelite.mis.be.repository.AccommodationTypeRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class AccommodationTypeResource {

    private final Logger log = LoggerFactory.getLogger(AccommodationTypeResource.class);

    private static final String ENTITY_NAME = "accommodationType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AccommodationTypeRepository accommodationTypeRepository;

    public AccommodationTypeResource(AccommodationTypeRepository accommodationTypeRepository) {
        this.accommodationTypeRepository = accommodationTypeRepository;
    }


    @PostMapping("/accommodation-types")
    public ResponseEntity<AccommodationType> createAccommodationType(@RequestBody AccommodationType accommodationType) throws URISyntaxException {
        log.debug("REST request to save AccommodationType : {}", accommodationType);
        if (accommodationType.getId() != null) {
            throw new BadRequestAlertException("A new accommodationType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccommodationType result = accommodationTypeRepository.save(accommodationType);
        return ResponseEntity.created(new URI("/api/accommodation-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/accommodation-types")
    public ResponseEntity<AccommodationType> updateAccommodationType(@RequestBody AccommodationType accommodationType) throws URISyntaxException {
        log.debug("REST request to update AccommodationType : {}", accommodationType);
        if (accommodationType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AccommodationType result = accommodationTypeRepository.save(accommodationType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, accommodationType.getId()))
            .body(result);
    }


    @GetMapping("/accommodation-types")
    public List<AccommodationType> getAllAccommodationTypes() {
        log.debug("REST request to get all AccommodationTypes");
        return accommodationTypeRepository.findAll();
    }


    @GetMapping("/accommodation-types/{id}")
    public ResponseEntity<AccommodationType> getAccommodationType(@PathVariable String id) {
        log.debug("REST request to get AccommodationType : {}", id);
        Optional<AccommodationType> accommodationType = accommodationTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(accommodationType);
    }


    @DeleteMapping("/accommodation-types/{id}")
    public ResponseEntity<Void> deleteAccommodationType(@PathVariable String id) {
        log.debug("REST request to delete AccommodationType : {}", id);
        accommodationTypeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
