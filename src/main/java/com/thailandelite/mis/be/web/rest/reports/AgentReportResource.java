package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.AgentReportResponse;
import com.thailandelite.mis.be.service.dto.reports.BaseReportRequest;
import com.thailandelite.mis.be.service.dto.reports.TimePeriod;
import com.thailandelite.mis.be.service.report.AgentReportService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Application;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;
import java.time.ZonedDateTime;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AgentReportResource {

    private final AgentReportService reportService;
    private int checkDay = 30;


    @GetMapping("/reports/agent-application")
    public ResponseEntity<FlowResponse<Page<AgentReportService.AgentApplicationReport>>> getAgentApplication(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest();
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getAgentApplicantReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/agent-near-expired")
    public ResponseEntity<FlowResponse<Page<AgentReportResponse>>> getNearExpiredReports(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest(ZonedDateTime.now().with(LocalTime.MIDNIGHT), ZonedDateTime.now().with(LocalTime.MIDNIGHT).plusDays(checkDay), TimePeriod.DAY);
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getAgentExpireContractReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/agent-near-expired/excel")
    public byte[] getNearExpiredExcel(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest(ZonedDateTime.now().with(LocalTime.MIDNIGHT), ZonedDateTime.now().with(LocalTime.MIDNIGHT).plusDays(checkDay), TimePeriod.DAY);
        return reportService.generateExcelExpireReport("Near Expired", searchRequest);
    }

    @GetMapping("/reports/agent-alive")
    public ResponseEntity<FlowResponse<Page<AgentReportResponse>>> getAliveReports(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest(ZonedDateTime.now().with(LocalTime.MIDNIGHT), ZonedDateTime.now().with(LocalTime.MIDNIGHT).plusDays(checkDay/2), TimePeriod.DAY);
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getAgentRenewContractReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/agent-alive/excel")
    public byte[] getAliveExcel(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest(ZonedDateTime.now().with(LocalTime.MIDNIGHT), ZonedDateTime.now().with(LocalTime.MIDNIGHT).plusDays(checkDay/2), TimePeriod.DAY);
        return reportService.generateExcelRenewReport("Re-new", searchRequest);
    }

    @GetMapping("/reports/agent-expired")
    public ResponseEntity<FlowResponse<Page<AgentReportResponse>>> getExpiredReports(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest(ZonedDateTime.now().with(LocalTime.MIDNIGHT).plusDays(- checkDay*12), ZonedDateTime.now().with(LocalTime.MIDNIGHT), TimePeriod.DAY);
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getAgentExpireContractReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/agent-expired/excel")
    public byte[] getExpiredExcel(Pageable pageable) {
        BaseReportRequest searchRequest = new BaseReportRequest(ZonedDateTime.now().with(LocalTime.MIDNIGHT).plusDays(- checkDay*12), ZonedDateTime.now().with(LocalTime.MIDNIGHT), TimePeriod.DAY);
        return reportService.generateExcelExpireReport("Expired", searchRequest);
    }
}
