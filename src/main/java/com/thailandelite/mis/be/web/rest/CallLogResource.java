package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.cdr.domain.Cdr;
import com.thailandelite.mis.be.cdr.repository.CdrRepository;
import com.thailandelite.mis.be.domain.CallLog;
import com.thailandelite.mis.be.repository.CallLogRepository;
import com.thailandelite.mis.be.service.CallLogService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.agent.AgentLog;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.thailandelite.mis.be.domain.CallLog}.
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CallLogResource {

    private final Logger log = LoggerFactory.getLogger(CallLogResource.class);

    private static final String ENTITY_NAME = "callLog";
    private static final String flowDefinitionKey = "callLog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CallLogRepository callLogRepository;
    private final CdrRepository cdrRepository;

    private final CallLogService callLogService;


    @PostMapping("/call-logs")
    public ResponseEntity<CallLog> createCallLog(@RequestBody CallLog callLog) throws URISyntaxException {
        log.debug("REST request to save CallLog : {}", callLog);
        if (callLog.getId() != null) {
            throw new BadRequestAlertException("A new callLog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CallLog result = callLogRepository.save(callLog);
        return ResponseEntity.created(new URI("/api/call-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/call-logs")
    public ResponseEntity<CallLog> updateCallLog(@RequestBody CallLog callLog) throws URISyntaxException {
        log.debug("REST request to update CallLog : {}", callLog);
        if (callLog.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CallLog result = callLogRepository.save(callLog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, callLog.getId()))
            .body(result);
    }


    @GetMapping("/call-logs")
    public Page<Cdr> getAllCallLogs(Pageable pageable) {
        log.debug("REST request to get all CallLogs");
        return cdrRepository.findAll(pageable);
    }

    @GetMapping("/call-logs/desk-no/{deskNo}")
    public ResponseEntity<FlowResponse<Page<CallLog>>> getAllCallLogs(@PathVariable String deskNo,Pageable pageable) {
        log.debug("REST request to get all CallLogs");
        Page<CallLog> callLogs = callLogService.findCallLogByDeskNo(deskNo, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), callLogs);
        return new ResponseEntity<>(new FlowResponse<>(flowDefinitionKey, 0, callLogs), headers, HttpStatus.OK);
    }

    @GetMapping("/incoming-logs")
    public ResponseEntity<FlowResponse<Page<CallLog>>> getAllIncomingLogs(Pageable pageable) {
        log.debug("REST request to get all IncomingLogs");
        Page<CallLog> incomingLogs = callLogService.IncomingCallLogs(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), incomingLogs);
        return new ResponseEntity<>(new FlowResponse<Page<CallLog>>(flowDefinitionKey, 0, incomingLogs), headers, HttpStatus.OK);
    }

    @GetMapping("/outgoing-logs")
    public ResponseEntity<FlowResponse<Page<CallLog>>> getAllOutgoingLogs(Pageable pageable) {
        log.debug("REST request to get all OutgoingLogs");
        Page<CallLog> outgoingLogs = callLogService.OutgoingCallLogs(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), outgoingLogs);

        return new ResponseEntity<>(new FlowResponse<Page<CallLog>>(flowDefinitionKey, 0, outgoingLogs), headers, HttpStatus.OK);
    }

    @GetMapping("/call-logs/{id}")
    public ResponseEntity<CallLog> getCallLog(@PathVariable String id) {
        log.debug("REST request to get CallLog : {}", id);
        Optional<CallLog> callLog = callLogRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(callLog);
    }

    @GetMapping("/receive-call/{deskNo}")
    public ResponseEntity<CallLogService.CallingLog> getReceiveCall(@PathVariable String deskNo) {
        log.debug("REST request to get Receive call at desk no. : {}", deskNo);
        CallLogService.CallingLog receiveCall = callLogService.ReceiveCall(deskNo);
        return ResponseEntity.ok(receiveCall);
    }

    @GetMapping("/abandon-call/{deskNo}")
    public ResponseEntity<List<CallLogService.CallingLog>> getAbandonCall(@PathVariable String deskNo) {
        log.debug("REST request to get Receive call at desk no. : {}", deskNo);
        List<CallLogService.CallingLog> abandonCalls = callLogService.AbandonCall(deskNo);
        return ResponseEntity.ok(abandonCalls);
    }


    @DeleteMapping("/call-logs/{id}")
    public ResponseEntity<Void> deleteCallLog(@PathVariable String id) {
        log.debug("REST request to delete CallLog : {}", id);
        callLogRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
