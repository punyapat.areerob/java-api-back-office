package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.ProductModelRepository;
import com.thailandelite.mis.be.service.ProductModelService;
import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.be.repository.ProductRepository;
import com.thailandelite.mis.be.repository.SubProductCategoryRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.ProductModel;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import com.thailandelite.mis.be.service.ProductService;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ProductResource {

    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    private static final String ENTITY_NAME = "product";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductRepository productRepository;
    private final ProductModelRepository productModelRepository;
    private final SubProductCategoryRepository subProductCategoryRepository;
    private final ProductService productService;
    private final ProductModelService productModelService;

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product) throws URISyntaxException {
        log.debug("REST request to save Product : {}", product);
        if (product.getId() != null) {
            throw new BadRequestAlertException("A new product cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Product result = productRepository.save(product);

        return ResponseEntity.created(new URI("/api/products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PostMapping("/products/create")
    public ResponseEntity<Product> createProductModel(@RequestBody Product product) throws URISyntaxException {
        log.debug("REST request to save Product : {}", product);
        Product result = productService.createProduct(product);

        return ResponseEntity.created(new URI("/api/products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/products")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) throws URISyntaxException {
        log.debug("REST request to update Product : {}", product);
        if (product.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Product result = productRepository.save(product);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, product.getId()))
            .body(result);
    }


    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts(Pageable pageable) {
        log.debug("REST request to get all Products");
        Page<Product> page = productRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/products/model")
    public List<Product> getAllProductsModel() {
        log.debug("REST request to get all Products Model");
       List<Product> products = productService.getAllProduct();
        return products;
    }


    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable String id) {
        log.debug("REST request to get Product : {}", id);
        Optional<Product> product = productRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(product);
    }
//    @GetMapping("/products/vendor/{id}")
//    public ResponseEntity<List<Product>> getProductByVendor(@PathVariable String id) {
//        log.debug("REST request to get Product : {}", id);
//        List<Product> product = productRepository.findByVendorId(id);
//        return ResponseEntity.ok().body(product);
//    }

    @PostMapping("/products/product-model")
    public ResponseEntity<ProductModel> createProductModel(@RequestBody ProductModel model) throws URISyntaxException {
        ProductModel productModel = productModelRepository.save(model);
        return ResponseEntity.created(new URI("/products/product-model/" + productModel.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, productModel.getId()))
            .body(productModel);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable String id) {
        log.debug("REST request to delete Product : {}", id);
        Product product = productRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found"));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/products/product-model/fix/vendorId")
    public ResponseEntity<String> fixAddVendorIdToProductModel() {
        log.debug("REST request to get Product!!");
        productModelService.addVendorIdToProductModel();
        return ResponseEntity.ok().body("SUCCESS");
    }

}
