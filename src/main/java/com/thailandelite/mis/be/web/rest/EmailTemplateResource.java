package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.repository.EmailTemplateMetaRepository;
import com.thailandelite.mis.be.repository.EmailTemplateRepository;
import com.thailandelite.mis.be.repository.InvoiceRepository;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.EmailService;
import com.thailandelite.mis.be.service.InvoiceService;
import com.thailandelite.mis.be.service.NotificationService;
import com.thailandelite.mis.be.service.helper.FormBuilderService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import io.easycm.framework.form.generator.FormlyFieldConfig;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;



@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class EmailTemplateResource {

    private final Logger log = LoggerFactory.getLogger(EmailTemplateResource.class);

    private static final String ENTITY_NAME = "emailTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmailTemplateRepository emailTemplateRepository;
    private final EmailService emailService;
    private final EmailTemplateMetaRepository emailTemplateMetaRepository;
    private final FormBuilderService formBuilderService;
    private final NotificationService notificationService;
    private final BookingService bookingService;
    private final InvoiceService invoiceService;

    @PostMapping("/email-templates")
    public ResponseEntity<EmailTemplate> createEmailTemplate(@RequestBody EmailTemplate emailTemplate) throws URISyntaxException {
        log.debug("REST request to save EmailTemplate : {}", emailTemplate);
        if (emailTemplate.getId() != null) {
            throw new BadRequestAlertException("A new emailTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }

        EmailTemplate result = emailService.save(emailTemplate);


        return ResponseEntity.created(new URI("/api/email-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/email-templates")
    public ResponseEntity<EmailTemplate> updateEmailTemplate(@RequestBody EmailTemplate emailTemplate) throws URISyntaxException {
        log.debug("REST request to update EmailTemplate : {}", emailTemplate);
        if (emailTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        EmailTemplate result = emailTemplateRepository.save(emailTemplate);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, emailTemplate.getId()))
            .body(result);
    }


    @GetMapping("/email-templates")
    public List<EmailTemplate> getAllEmailTemplates() {
        log.debug("REST request to get all EmailTemplates");
        return emailTemplateRepository.findAll();
    }

    @GetMapping("/email-templates/{id}")
    public ResponseEntity<EmailTemplate> getEmailTemplate(@PathVariable String id) {
        log.debug("REST request to get EmailTemplate : {}", id);
        Optional<EmailTemplate> emailTemplate = emailTemplateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(emailTemplate);
    }

    @DeleteMapping("/email-templates/{id}")
    public ResponseEntity<Void> deleteEmailTemplate(@PathVariable String id) {
        log.debug("REST request to delete EmailTemplate : {}", id);
        emailTemplateRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/email-templates/form")
    public List<FormlyFieldConfig> getForm() throws ClassNotFoundException {
        log.debug("REST request to get all EmailTemplates");
        return formBuilderService.build(EmailTemplate.class);
    }

    @PostMapping("/email-templates/test")
    public ResponseEntity<Date> createEmailTemplateTest(String id) throws URISyntaxException, IOException {
        log.debug("REST request to save EmailTemplate : {}", id);
        this.notificationService.sendBookingEpa(bookingService.getBookingDetailById(id));
        return ResponseEntity.ok(new Date());
    }

    @GetMapping("/email-templates/test/booking/{bookingId}")
    public ResponseEntity<Date> sendBookingEmailTemplateTest(@PathVariable String bookingId, String email) throws URISyntaxException, IOException {
        log.debug("REST request to save EmailTemplate : {}", bookingId);
        BookingResponse booking = bookingService.getWrapBookingDetailById(bookingId);
        NotificationService.SendBookingEmail context = new NotificationService.SendBookingEmail();
        context.setBooking(booking);
        this.notificationService.sendBookingEmail(email, context,null,null);
        return ResponseEntity.ok(new Date());
    }

    @GetMapping("/email-templates/test/invoice/{invoiceId}")
    public ResponseEntity<String> testSendInvoiceEmail(@PathVariable String invoiceId, String email) throws URISyntaxException, IOException {
        log.debug("REST request testSendInvoiceEmail : {}", invoiceId);
        invoiceService.sendInvoiceEmail(invoiceId);
        return ResponseEntity.ok("Success");
    }
}
