package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.service.helper.FormBuilderService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.JobDescription;
import com.thailandelite.mis.be.repository.JobDescriptionRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.easycm.framework.form.generator.FormlyFieldConfig;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;


@RestController
@RequestMapping("/api")
public class JobDescriptionResource {

    private final Logger log = LoggerFactory.getLogger(JobDescriptionResource.class);

    private static final String ENTITY_NAME = "jobDescription";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JobDescriptionRepository jobDescriptionRepository;

    public JobDescriptionResource(JobDescriptionRepository jobDescriptionRepository) {
        this.jobDescriptionRepository = jobDescriptionRepository;
    }


    @PostMapping("/job-descriptions")
    public ResponseEntity<JobDescription> createJobDescription(@Valid @RequestBody JobDescription jobDescription) throws URISyntaxException {
        log.debug("REST request to save JobDescription : {}", jobDescription);
        if (jobDescription.getId() != null) {
            throw new BadRequestAlertException("A new jobDescription cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JobDescription result = jobDescriptionRepository.save(jobDescription);
        return ResponseEntity.created(new URI("/api/job-descriptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/job-descriptions")
    public ResponseEntity<JobDescription> updateJobDescription(@Valid @RequestBody JobDescription jobDescription) throws URISyntaxException {
        log.debug("REST request to update JobDescription : {}", jobDescription);
        if (jobDescription.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JobDescription result = jobDescriptionRepository.save(jobDescription);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jobDescription.getId()))
            .body(result);
    }


    @GetMapping("/job-descriptions")
    public ResponseEntity<FlowResponse<Page<JobDescription>>>  getAllJobDescriptions(String teamId, Pageable pageable) {
        log.debug("REST request to get all JobDescriptions: {}", teamId);

        Page<JobDescription> jobDescriptions = hasText(teamId)?jobDescriptionRepository.findAllByTeamId(teamId,pageable):jobDescriptionRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest()
            ,jobDescriptions );
        return new ResponseEntity<>(new FlowResponse<Page<JobDescription>>("", 0,jobDescriptions), headers, HttpStatus.OK);
    }


    @GetMapping("/job-descriptions/{id}")
    public ResponseEntity<JobDescription> getJobDescription(@PathVariable String id) {
        log.debug("REST request to get JobDescription : {}", id);
        Optional<JobDescription> jobDescription = jobDescriptionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jobDescription);
    }


    @DeleteMapping("/job-descriptions/{id}")
    public ResponseEntity<Void> deleteJobDescription(@PathVariable String id) {
        log.debug("REST request to delete JobDescription : {}", id);
        jobDescriptionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/job-descriptions/form")
    public List<FormlyFieldConfig> getForm(String teamId) throws ClassNotFoundException {
        FormBuilderService formBuilderService = new FormBuilderService(null);
        return formBuilderService.getForm(JobDescription.class);
    }
}
