package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.VendorContactRepository;
import com.thailandelite.mis.model.domain.Shop;
import com.thailandelite.mis.model.domain.Vendor;
import com.thailandelite.mis.be.repository.ShopRepository;
import com.thailandelite.mis.be.repository.VendorRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.VendorContact;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class VendorsContactResource {

    private final Logger log = LoggerFactory.getLogger(VendorsContactResource.class);

    private static final String ENTITY_NAME = "vendorsContact";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendorContactRepository vendorContactRepository;

    @GetMapping("/vendors-contacts")
    public List<VendorContact> getAllVendorsContacts() {
        log.debug("REST request to get all VendorsContacts");
        return vendorContactRepository.findAll();
    }

    @GetMapping("/vendors-contact/{id}")
    public ResponseEntity<VendorContact> getVendorsContact(@PathVariable String id) {
        log.debug("REST request to get VendorsContact : {}", id);
        Optional<VendorContact> vendorsContact = vendorContactRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(vendorsContact);
    }

    @GetMapping("/vendors-contacts/{vendorId}")
    public ResponseEntity<List<VendorContact>> getVendorsContactsByVendorId(@PathVariable String vendorId) {
        log.debug("REST request to get VendorsContact : {}", vendorId);
        List<VendorContact> vendorsContacts = vendorContactRepository.findByVendorId(vendorId);
        return ResponseEntity.ok(vendorsContacts);
    }

    @PostMapping("/vendors-contacts")
    public ResponseEntity<VendorContact> createVendorsContact(@RequestBody VendorContact vendorContact) throws URISyntaxException {
        log.debug("REST request to save VendorsContact : {}", vendorContact);
        if (vendorContact.getId() != null) {
            throw new BadRequestAlertException("A new vendorsContact cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorContact result = vendorContactRepository.save(vendorContact);
        return ResponseEntity.created(new URI("/api/vendors-contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/vendors-contacts")
    public ResponseEntity<VendorContact> updateVendorsContact(@RequestBody VendorContact vendorContact) throws URISyntaxException {
        log.debug("REST request to update VendorsContact : {}", vendorContact);
        if (vendorContact.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VendorContact result = vendorContactRepository.save(vendorContact);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vendorContact.getId()))
            .body(result);
    }
//    @DeleteMapping("/vendors-contacts/{id}")
//    public ResponseEntity<Void> deleteVendorsContact(@PathVariable String id) {
//        log.debug("REST request to delete VendorsContact : {}", id);
//        List<String> ids = new ArrayList<>();
//        ids.add(id);
//        VendorsContact vendorsContact = vendorsContactRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found"));
//
//        Vendor vendor = vendorRepository.findById(vendorsContact.getVendorId())
//            .orElseThrow(() -> new RuntimeException("Not found"));
//
//        List<Shop> shops = shopRepository.findByVendorId(vendorsContact.getVendorId());
//        log.debug("Fetch shop by contacts : {}", shops);
//
//        Set<VendorsContact> contactVendors = vendor.getContacts().stream()
//            .filter(p -> !p.getId().equals(id)).collect(Collectors.toSet());
//        vendor.setContacts(contactVendors);
//        vendorRepository.save(vendor);
//
//        for (Shop shop : shops) {
//            Set<VendorsContact> contactShops = shop.getContacts().stream()
//                .filter(p -> !p.getId().equals(id)).collect(Collectors.toSet());
//            shop.setContacts(contactShops);
//            shopRepository.save(shop);
//        }
//
//        vendorsContactRepository.deleteById(id);
//
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
//    }
}
