package com.thailandelite.mis.be.web.rest;


import com.thailandelite.mis.be.common.Utils;
import com.thailandelite.mis.be.repository.CardPrivilegeRepository;
import com.thailandelite.mis.be.repository.CardRepository;
import com.thailandelite.mis.be.service.CardService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.CardWithPrivilege;
import com.thailandelite.mis.model.domain.Membership;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CardResource {

    private final Logger log = LoggerFactory.getLogger(CardResource.class);

    private static final String ENTITY_NAME = "card";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CardRepository cardRepository;
    private final CardPrivilegeRepository cardPrivilegeRepository;
    private final CardService cardService;

    public CardResource(CardRepository cardRepository, CardPrivilegeRepository cardPrivilegeRepository, CardService cardService) {
        this.cardPrivilegeRepository = cardPrivilegeRepository;
        this.cardRepository = cardRepository;
        this.cardService = cardService;
    }


    @PostMapping("/cards")
    public ResponseEntity<Card> createCard(@RequestBody Card card) throws URISyntaxException {
        log.debug("REST request to save Card : {}", card);
        if (card.getId() != null) {
            throw new BadRequestAlertException("A new card cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Card result = cardRepository.save(card);
        return ResponseEntity.created(new URI("/api/cards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/cards")
    public ResponseEntity<Card> updateCard(@RequestBody Card card) throws URISyntaxException {
        log.debug("REST request to update Card : {}", card);
        if (card.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Card result = cardRepository.save(card);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, card.getId()))
            .body(result);
    }


    @GetMapping("/cards")
    public ResponseEntity<FlowResponse<Page<Card>>> getAll(Pageable pageable) {
        log.debug("REST request to get a page of Memberships");
        Page<Card> page = cardRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok(new FlowResponse<>("", 0, page ));
    }

    @GetMapping("/cards/list")
    public ResponseEntity<List<Card>> getAll() {
        log.debug("REST request to get a page of Memberships");
        return ResponseEntity.ok(cardRepository.findByStatusAndPublicStatus(Card.CardStatus.ACTIVE, Card.PublicStatus.ACTIVE));
    }

    @GetMapping("/cards/{id}")
    public CardWithPrivilege getCard(@PathVariable String id) {
        log.debug("REST request to get Card : {}", id);
        Optional<Card> card = cardRepository.findById(id);
        ModelMapper modelMapper = new ModelMapper();
        CardWithPrivilege cardWithPrivilege = modelMapper.map(card.get(), CardWithPrivilege.class);
        cardWithPrivilege.setCardPrivileges(cardPrivilegeRepository.findAllByCardId(card.get().getId()));
        return cardWithPrivilege;
    }

    @DeleteMapping("/cards/{id}")
    public ResponseEntity<Void> deleteCard(@PathVariable String id) {
        log.debug("REST request to delete Card : {}", id);
        cardRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PatchMapping("/cards/{id}/status")
    public ResponseEntity<Card> updateCardStatus(@PathVariable String id, Card.CardStatus status, String remark) {
        return ResponseEntity.ok().body(cardService.changeMemberStatus(id, status, remark));
    }
}
