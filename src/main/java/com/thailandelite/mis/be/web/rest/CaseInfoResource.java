package com.thailandelite.mis.be.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.CaseInfoRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.Report;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class CaseInfoResource {

    private final Logger log = LoggerFactory.getLogger(CaseInfoResource.class);

    private static final String ENTITY_NAME = "caseInfo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseInfoRepository caseInfoRepository;

    public CaseInfoResource(CaseInfoRepository caseInfoRepository) {
        this.caseInfoRepository = caseInfoRepository;
    }


    @PostMapping("/case-infos")
    public ResponseEntity<CaseInfo> createCaseInfo(@RequestBody CaseInfo caseInfo) throws URISyntaxException {
        log.debug("REST request to save CaseInfo : {}", caseInfo);
        if (caseInfo.getId() != null) {
            throw new BadRequestAlertException("A new caseInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseInfo result = caseInfoRepository.save(caseInfo);
        return ResponseEntity.created(new URI("/api/case-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/case-infos")
    public ResponseEntity<CaseInfo> updateCaseInfo(@RequestBody CaseInfo caseInfo) throws URISyntaxException {
        log.debug("REST request to update CaseInfo : {}", caseInfo);
        if (caseInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseInfo result = caseInfoRepository.save(caseInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseInfo.getId()))
            .body(result);
    }

    @PutMapping("/case-infos/flag")
    public ResponseEntity<CaseInfo> updateFlag(@RequestBody CaseInfoPartialUpdate caseInfo) throws URISyntaxException {
        log.debug("REST request to update CaseInfo : {}", caseInfo);
        CaseInfo caseInfoDB = caseInfoRepository.findById(caseInfo.getId()).get();
        if(caseInfo.getIsSeen() != null ) {
            caseInfoDB.setIsSeen(caseInfo.getIsSeen());
        }
        if(caseInfo.getUrgent() != null) {
            caseInfoDB.setUrgent(caseInfo.getUrgent());
        }
        CaseInfo result = caseInfoRepository.save(caseInfoDB);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseInfo.getId()))
            .body(result);
    }

    @GetMapping("/case-infos")
    public List<CaseInfo> getAllCaseInfos() {
        log.debug("REST request to get all CaseInfos");
        return caseInfoRepository.findAll();
    }

    @GetMapping("/case-infos/reports")
    public List<Report> getAllCaseReport() {
        log.debug("REST request to get all CaseInfos");
        List<Report> reports = new ArrayList<>();
        List<CaseInfo> infos = caseInfoRepository.findAll();
        Report report = new Report();
        report.setReportName("By Status");
        report.setReport(infos.stream().collect(
            Collectors.groupingBy(CaseInfo::getStatus, Collectors.counting())
        ));
        reports.add(report);
        Report report_byFlow = new Report();
        report_byFlow.setReportName("By Flow");
        report_byFlow.setReport(infos.stream().collect(
            Collectors.groupingBy(CaseInfo::getFlowDefKey, Collectors.counting())
        ));
        reports.add(report_byFlow);
        return reports;
    }

    public List<String> getFlowListByTeam(String team){
        List<String> flows = new ArrayList<>();
        flows.add("applicant");
        return flows;
    }

    @GetMapping("/case-infos/{team}")
    public  ResponseEntity<FlowResponse<Page<CaseInfo>>> getAllCaseInfos(@PathVariable String team, Pageable pageable) {
        log.debug("REST request to get all CaseInfos");
        return ResponseEntity.ok(new FlowResponse<>("", 0,  caseInfoRepository.findAllByFlowDefKeyIn(getFlowListByTeam(team),pageable)));
    }

    @GetMapping("/case-infos/{team}/download")
    public ResponseEntity<InputStreamResource>  getAllCaseInfos(@PathVariable String team) throws JsonProcessingException {
        log.debug("REST request to get all CaseInfos");
        ObjectMapper mapper = new ObjectMapper();
        Pageable wholePage = Pageable.unpaged();
        byte[] buf = mapper.writeValueAsBytes(caseInfoRepository.findAllByFlowDefKeyIn(getFlowListByTeam(team),wholePage));

        return ResponseEntity
            .ok()
            .contentLength(buf.length)
            .contentType(
                MediaType.parseMediaType("application/octet-stream"))
            .body(new InputStreamResource(new ByteArrayInputStream(buf)));

    }

    @GetMapping("/case-infos/id/{id}")
    public ResponseEntity<CaseInfo> getCaseInfo(@PathVariable String id) {
        log.debug("REST request to get CaseInfo : {}", id);
        Optional<CaseInfo> caseInfo = caseInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseInfo);
    }

    @DeleteMapping("/case-infos/{id}")
    public ResponseEntity<Void> deleteCaseInfo(@PathVariable String id) {
        log.debug("REST request to delete CaseInfo : {}", id);
        caseInfoRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @Data
    public static class CaseInfoPartialUpdate
    {
        private String id;
        private Boolean urgent;
        private Boolean isSeen;
    }
}
