package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.MemberRemarkTag;
import com.thailandelite.mis.be.repository.MemberRemarkTagRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class MemberRemarkTagResource {

    private final Logger log = LoggerFactory.getLogger(MemberRemarkTagResource.class);

    private static final String ENTITY_NAME = "memberRemarkTag";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MemberRemarkTagRepository memberRemarkTagRepository;

    public MemberRemarkTagResource(MemberRemarkTagRepository memberRemarkTagRepository) {
        this.memberRemarkTagRepository = memberRemarkTagRepository;
    }


    @PostMapping("/member-remark-tags")
    public ResponseEntity<MemberRemarkTag> createMemberRemarkTag(@RequestBody MemberRemarkTag memberRemarkTag) throws URISyntaxException {
        log.debug("REST request to save MemberRemarkTag : {}", memberRemarkTag);
        if (memberRemarkTag.getId() != null) {
            throw new BadRequestAlertException("A new memberRemarkTag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MemberRemarkTag result = memberRemarkTagRepository.save(memberRemarkTag);
        return ResponseEntity.created(new URI("/api/member-remark-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/member-remark-tags")
    public ResponseEntity<MemberRemarkTag> updateMemberRemarkTag(@RequestBody MemberRemarkTag memberRemarkTag) throws URISyntaxException {
        log.debug("REST request to update MemberRemarkTag : {}", memberRemarkTag);
        if (memberRemarkTag.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MemberRemarkTag result = memberRemarkTagRepository.save(memberRemarkTag);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, memberRemarkTag.getId()))
            .body(result);
    }

    @GetMapping("/member-remark-tags")
    public List<MemberRemarkTag> getAllMemberRemarkTags() {
        log.debug("REST request to get all MemberRemarkTags");
        return memberRemarkTagRepository.findAll();
    }

    @GetMapping("/member-remark-tags/{id}")
    public ResponseEntity<MemberRemarkTag> getMemberRemarkTag(@PathVariable String id) {
        log.debug("REST request to get MemberRemarkTag : {}", id);
        Optional<MemberRemarkTag> memberRemarkTag = memberRemarkTagRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(memberRemarkTag);
    }

    @DeleteMapping("/member-remark-tags/{id}")
    public ResponseEntity<Void> deleteMemberRemarkTag(@PathVariable String id) {
        log.debug("REST request to delete MemberRemarkTag : {}", id);
        memberRemarkTagRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
