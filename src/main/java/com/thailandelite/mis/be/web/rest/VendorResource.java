package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.ProductVariantValueRepository;
import com.thailandelite.mis.be.service.*;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.be.repository.VendorRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.request.VendorProfileUpdateDao;
import com.thailandelite.mis.model.dto.request.VendorRegisterDao;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class VendorResource {

    private final Logger log = LoggerFactory.getLogger(VendorResource.class);
    private static final String ENTITY_NAME = "vendor";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final VendorRepository vendorRepository;
    private final VendorService vendorService;
    private final VendorRequestService vendorRequestService;
    private final ProductVariantValueRepository productVariantValueRepository;
    private final ProductVariantGroupService productVariantGroupService;
    private final ProductVariantValueService productVariantValueService;
    private final BookingService bookingService;

    @PostMapping("/vendors/request-create")
    public Vendor requestCreateVendor(@RequestBody VendorRegisterDao registerDao) {
        log.debug("REST request to create Vendor : {}", registerDao);
        Vendor vendor = new Vendor();
        return vendor;
    }

    @PostMapping("/vendors/request-update")
    public Vendor requestUpdateVendor(@RequestBody VendorProfileUpdateDao updateDao) {
        log.debug("REST request to update Vendor : {}", updateDao);
        Vendor vendor = new Vendor();
        return vendor;
    }

    @GetMapping("/vendors")
    public ResponseEntity<Page<Vendor>> getAllVendors(Pageable pageable) {
        log.debug("REST request to get all Vendors: page {}", pageable);
        Page<Vendor> page = vendorRepository.findAllByStatus(Vendor.Status.ACTIVE,pageable);
//        Page<Vendor> page = vendorRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page, headers, HttpStatus.OK);
    }

    @PostMapping("/vendors")
    public ResponseEntity<Vendor> createVendor(@RequestBody Vendor vendor) throws URISyntaxException {
        log.debug("REST request to save Vendor : {}", vendor);
        if (vendor.getId() != null) {
            throw new BadRequestAlertException("A new vendor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        //fix bug
        vendor.setStatus(Vendor.Status.ACTIVE);
        Vendor result = vendorRepository.save(vendor);
        return ResponseEntity.created(new URI("/api/vendors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/vendors")
    public ResponseEntity<Vendor> updateVendor(@RequestBody Vendor vendor) throws URISyntaxException {
        log.debug("REST request to update Vendor : {}", vendor);
        if (vendor.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Vendor result = vendorRepository.save(vendor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vendor.getId()))
            .body(result);
    }

    @DeleteMapping("/vendors/{id}")
    public ResponseEntity<Void> deleteVendor(@PathVariable String id) {
        log.debug("REST request to delete Vendor : {}", id);
        vendorRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/vendors/{id}")
    public ResponseEntity<Vendor> getVendor(@PathVariable String id) {
        log.debug("REST request to get Vendor : {}", id);
        Optional<Vendor> vendor = vendorRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(vendor);
    }

    @GetMapping("/vendors/{id}/contracts")
    public ResponseEntity<Page<VendorContract>> getVendorContracts(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get Vendor : {}", id);
        Page<VendorContract> page = vendorService.fetchVendorContract(id, pageable);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/vendors/{id}/contracts/{contractId}")
    public ResponseEntity<VendorContract> getOneVendorContracts(@PathVariable String id, @PathVariable String contractId) {
        log.debug("REST request to get Vendor : {}, {}", id, contractId);
        return ResponseEntity.ok(vendorService.findByContractId(contractId));
    }

    @PutMapping("/vendors/{id}/contracts")
    public ResponseEntity<VendorContract> updateVendorShop(@RequestBody VendorContract vendorContract) {
        log.debug("REST request to update vendorContract: {}", vendorContract);
        VendorContract result = vendorService.updateVendorContract(vendorContract);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/vendors/{id}/contract/{contractId}/uploads")
    public ResponseEntity<VendorContract> uploadContractFile(@PathVariable String id,
                                                             @PathVariable String contractId,
                                                             @RequestParam("file") MultipartFile file) throws UploadFailException {
        return ResponseEntity.ok(vendorService.uploadContractFile(id, contractId, file));
    }

    @DeleteMapping("/vendors/{id}/contract/{contractId}/uploads")
    public ResponseEntity<VendorContract> uploadContractFile(@PathVariable String id,
                                                             @PathVariable String contractId,
                                                             @RequestParam int index) throws UploadFailException {
        return ResponseEntity.ok(vendorService.removeContractFileAt(id, contractId, index));
    }

    @DeleteMapping("/vendors/{id}/contracts/{contractId}")
    public ResponseEntity<Void> deleteVendorContract(@PathVariable String id, @PathVariable String contractId) {
        log.debug("REST request to get Vendor : {}", id);
        vendorService.deleteVendorContract(id, contractId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/vendors/{id}/billings")
    public ResponseEntity<Page<VendorBillInfo>> getVendorBillings(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get Vendor : {}", id);
        Page<VendorBillInfo> page = vendorService.fetchVendorBilling(id, pageable);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/vendors/{id}/billings/{billingId}")
    public ResponseEntity<VendorBillInfo> getOneVendorBillings(@PathVariable String id, @PathVariable String billingId) {
        log.debug("REST request to get Vendor : {}, {}", id, billingId);
        return ResponseEntity.ok(vendorService.findByBillingId(billingId));
    }

    @PutMapping("/vendors/{id}/billings")
    public ResponseEntity<VendorBillInfo> updateVendorBilling(@RequestBody VendorBillInfo vendorBillInfo) {
        log.debug("REST request to update vendorBillInfo: {}", vendorBillInfo);

        VendorBillInfo result = vendorService.updateVendorBilling(vendorBillInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @DeleteMapping("/vendors/{id}/billings/{billingId}")
    public ResponseEntity<Void> deleteVendorBilling(@PathVariable String id, @PathVariable String billingId) {
        log.debug("REST request to get Vendor : {}", id);
        vendorService.deleteVendorBillInfo(id, billingId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/vendors/{id}/shops")
    public ResponseEntity<Page<Shop>> getVendorShops(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get Vendor : {}", id);
        Page<Shop> page = vendorService.fetchVendorShop(id, pageable);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/vendors/{id}/shops/{shopId}")
    public ResponseEntity<Shop> getOneVendorShops(@PathVariable String id, @PathVariable String shopId) {
        log.debug("REST request to get Vendor : {}, {}", id, shopId);
        return ResponseEntity.ok(vendorService.findByShopId(shopId));
    }

    @PutMapping("/vendors/{id}/shops")
    public ResponseEntity<Shop> updateVendorShop(@RequestBody Shop shop) {
        log.debug("REST request to update shop: {}", shop);
        Shop result = vendorService.updateVendorShop(shop);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/vendors/{id}/shops/{shopId}/uploads")
    public ResponseEntity<Shop> uploadShopFile(@PathVariable String id,
                                               @PathVariable String shopId,
                                               @RequestParam("file") MultipartFile file) throws UploadFailException {
        return ResponseEntity.ok(vendorService.uploadShopFile(id, shopId, file));
    }

    @DeleteMapping("/vendors/{id}/shops/{shopId}/uploads")
    public ResponseEntity<Shop> uploadShopFile(@PathVariable String id,
                                               @PathVariable String shopId,
                                               @RequestParam int index) throws UploadFailException {
        return ResponseEntity.ok(vendorService.removeShopFileAt(id, shopId, index));
    }

    @DeleteMapping("/vendors/{id}/shops/{shopId}")
    public ResponseEntity<Void> deleteVendorShop(@PathVariable String id, @PathVariable String shopId) {
        log.debug("REST request to get Vendor : {}", id);
        vendorService.deleteVendorShop(id, shopId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/vendors/{id}/contacts")
    public ResponseEntity<Page<VendorContact>> getVendorContact(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get Vendor : {}", id);
        Page<VendorContact> page = vendorService.fetchVendorContact(id, pageable);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/vendors/{id}/contacts/{contactId}")
    public ResponseEntity<VendorContact> getOneVendorContact(@PathVariable String id, @PathVariable String contactId) {
        log.debug("REST request to get Vendor : {}, {}", id, contactId);
        return ResponseEntity.ok(vendorService.findByContactId(contactId));
    }

    @PutMapping("/vendors/{id}/contacts")
    public ResponseEntity<VendorContact> updateVendorContact(@RequestBody VendorContact vendorContact) {
        log.debug("REST request to update vendorContact: {}", vendorContact);
        VendorContact result = vendorService.updateVendorContact(vendorContact);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @DeleteMapping("/vendors/{id}/contacts/{contactId}")
    public ResponseEntity<Void> deleteVendorContact(@PathVariable String id, @PathVariable String contactId) {
        log.debug("REST request to get Vendor : {}", id);
        vendorService.deleteVendorContact(id, contactId);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/vendors/{id}/password")
    public ResponseEntity<Vendor> updateMemberPassword(@PathVariable String id, String password) {
        return ResponseEntity.ok().body(vendorRequestService.changePasswordRequest(id, password));
    }

    @GetMapping("/vendors/{id}/bookings")
    public List<BookingResponse> fetchMyVendorBookings(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        return bookingService.fetchWrapBookingByVendor(id);
    }

    @GetMapping("/vendors/{id}/bookings/status")
    public Page<BookingResponse> fetchMyVendorBookingsStatus(@PathVariable String id, BookingStatus status, Pageable pageable) {
        log.debug("REST request to get Membership : {}", id);
        return bookingService.fetchWrapBookingByVendor(id, status, pageable);
    }
    @GetMapping("/vendors-by-rating/{rating}")
    public ResponseEntity<List<Vendor>> getVendorsByRating(@PathVariable String rating) {
        log.debug("REST request to get Vendor by Rating: {}", rating);
        List<Vendor> vendors = vendorRepository.findByRating(rating);
        return ResponseEntity.ok(vendors);
    }

    @GetMapping("/vendors-by-status/{status}")
    public ResponseEntity<List<Vendor>> getVendorsByStatus(@PathVariable Vendor.Status status) {
        log.debug("REST request to get Vendor by Status: {}", status);
        List<Vendor> vendors = vendorRepository.findByStatus(status);
        return ResponseEntity.ok(vendors);
    }

    //////variantGroup/////////////////////////
    @GetMapping("/vendors/{id}/variantGroup")
    public ResponseEntity<Page<ProductVariantGroup>> getVariantGroupByVendor(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get variant Group By Vendor: {}", id);
        return ResponseEntity.ok(vendorService.findAllByVendorId(id, pageable));
    }

    @PostMapping("/vendors/variantGroup")
    public ResponseEntity<ProductVariantGroup> createVariantGroup(@RequestBody ProductVariantGroup productVariantGroup) {
        log.debug("REST request to Create variant Group: {}", productVariantGroup);
        ProductVariantGroup result = vendorService.insert(productVariantGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/vendors/variantGroup")
    public ResponseEntity<ProductVariantGroup> updateVariantGroup(@RequestBody ProductVariantGroup productVariantGroup) {
        log.debug("REST request to update variant Group: {}", productVariantGroup);
        ProductVariantGroup result = vendorService.save(productVariantGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @GetMapping("/vendors/variantGroup/{variantGroupId}")
    public ProductVariantGroup getVariantGroup(@PathVariable String variantGroupId) {
        log.debug("REST request to get variant Group: {}", variantGroupId);
        return productVariantGroupService.getById(variantGroupId);
    }

    @DeleteMapping("/vendors/{id}/variantGroup/{variantGroupId}")
    public ResponseEntity<Void> deleteVariantGroup(@PathVariable String id, @PathVariable String variantGroupId) {
        log.debug("REST request to delet variant Group: {}", variantGroupId);
        productVariantGroupService.deleteVendor(id, variantGroupId);
        return ResponseEntity.noContent().build();
    }

    //////variantValue/////////////////////////
    @GetMapping("/vendors/{id}/variantValue")
    public ResponseEntity<List<ProductVariantValue>> getVariantValueByVendor(@PathVariable String id) {
        log.debug("REST request to get variant Group By Vendor: {}", id);
        List<ProductVariantValue> productVariantValues = productVariantValueRepository.findByVendorId(id);
        return ResponseEntity.ok(productVariantValues);
    }

    @GetMapping("/vendors/{id}/variantGroup/{variantGroupId}/variantValue")
    public ResponseEntity<List<ProductVariantValue>> getVariantValueByVendor(@PathVariable String id, @PathVariable String variantGroupId) {
        log.debug("REST request to get variant Group By Vendor: {}", id);
        List<ProductVariantValue> productVariantGroups = productVariantValueRepository.findByVendorId(id).stream()
            .filter(src -> src.getProductVariantGroupId().equals(variantGroupId)).collect(Collectors.toList());
        return ResponseEntity.ok(productVariantGroups);
    }

    @PostMapping("/vendors/variantValue")
    public ResponseEntity<ProductVariantValue> CreateVariantValue(@RequestBody ProductVariantValue productVariantGroup) {
        log.debug("REST request to update vendorContact: {}", productVariantGroup);
        ProductVariantValue result = productVariantValueService.insert(productVariantGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/vendors/variantValue")
    public ResponseEntity<ProductVariantValue> updateVariantValue(@RequestBody ProductVariantValue productVariantGroup) {
        log.debug("REST request to update vendorContact: {}", productVariantGroup);
        ProductVariantValue result = productVariantValueService.save(productVariantGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @DeleteMapping("/vendors/{id}/variantValue/{variantValueId}")
    public ResponseEntity<Void> deleteVariantValue(@PathVariable String id, @PathVariable String variantValueId) {
        log.debug("REST request to update vendorContact: {}", variantValueId);
        productVariantValueService.deleteVendorVariantValue(id, variantValueId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/vendors/{id}/product")
    public ResponseEntity<Page<Product>> getVendorProduct(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get Vendor : {}", id);
        Page<Product> page = vendorService.fetchVendorProduct(id, pageable);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/vendors/{id}/product/{productId}")
    public ResponseEntity<Product> getOneVendorProduct(@PathVariable String id, @PathVariable String productId) {
        log.debug("REST request to get Vendor : {}, {}", id, productId);
        return ResponseEntity.ok(vendorService.findProductById(productId));
    }

    @PutMapping("/vendors/{id}/product")
    public ResponseEntity<Product> updateVendorContact(@RequestBody Product product) {
        log.debug("REST request to update vendorContact: {}", product);
        Product result = vendorService.updateVendorProduct(product);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @DeleteMapping("/vendors/{id}/product/{productId}")
    public ResponseEntity<Void> deleteVendorProduct(@PathVariable String id, @PathVariable String productId) {
        log.debug("REST request to get Vendor : {}", id);
        vendorService.deleteVendorProduct(id, productId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/vendors/{id}/product/{productId}/uploads")
    public ResponseEntity<Product> uploadVendorProduct(@PathVariable String id,
                                                       @PathVariable String productId,
                                                       @RequestParam("file") MultipartFile file) throws UploadFailException {
        Product product = vendorService.uploadProductFile(id, productId, file);
        return ResponseEntity.ok(product);
    }

    @GetMapping("/vendors/{id}/product/{productId}/product-models")
    public ResponseEntity<Page<ProductModel>> getVendorProductModelsInPorduct(@PathVariable String id, @PathVariable String productId, Pageable pageable) {
        log.debug("REST request to get Vendor : {}, {}", id, productId);
        return ResponseEntity.ok(vendorService.fetchVendorProductModel(id, pageable));
    }

    @GetMapping("/vendors/{id}/product-models")
    public ResponseEntity<Page<ProductModel>> getVendorProductModels(@PathVariable String id, Pageable pageable) {
        log.debug("REST request to get Vendor : {}, {}", id, pageable);
        return ResponseEntity.ok(vendorService.fetchVendorProductModel(id, pageable));
    }

    @GetMapping("/vendors/shop/{id}/product")
    public ResponseEntity<List<Product>> getshopProduct(@PathVariable String id) {
        log.debug("REST request to get Vendor : {}", id);
        List<Product> product = vendorService.fetchVendorShopProduct(id);
        return ResponseEntity.ok(product);
    }

//    @PostMapping("/vendors/vendorRequest/create")
//    public ResponseEntity<VendorRequest> CreateVendorRequeste(@RequestBody VendorRequest vendorRequest){
//        log.debug("REST request to update vendorContact: {}", vendorRequest);
//        VendorRequest result = vendorRequestService.VendorRequest(vendorRequest);
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
//            .body(result);
//    }
//    @GetMapping("/vendors/shop/{id}/product-models")
//    public ResponseEntity<List<ProductModel>> getshopProductModels(@PathVariable String id) {
//        log.debug("REST request to get Vendor : {}, {}", id);
//        List<ProductModel> productModel = vendorService.fetchVendorShopProductModel(id);
//        return ResponseEntity.ok(productModel);
//    }
//    @GetMapping("/vendors/shop/{id}/product-models")
//    public ResponseEntity<List<ProductModel>> getshopProductModels(@PathVariable String id) {
//        log.debug("REST request to get Vendor : {}, {}", id);
//        List<ProductModel> productModel = vendorService.fetchVendorShopProductModel(id);
//        return ResponseEntity.ok(productModel);
//    }

    @GetMapping("/vendors/{id}/product-models/{modelId}")
    public ResponseEntity<ProductModel> getVendorProductModelDetail(@PathVariable String id, @PathVariable String modelId) {
        log.debug("REST request to get Vendor : {}, {}", id, modelId);
        return ResponseEntity.ok(vendorService.getVendorProductModel(id, modelId));
    }

    @PutMapping("/vendors/{id}/product-models")
    public ResponseEntity<ProductModel> updateVendorProductModel(@RequestBody ProductModel productModel) {
        log.debug("REST request to update vendorContact: {}", productModel);
        ProductModel result = vendorService.updateVendorProductModel(productModel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @DeleteMapping("/vendors/{id}/product-models/{modelId}")
    public ResponseEntity<Void> deleteVendorProductModel(@PathVariable String id, @PathVariable String modelId) {
        log.debug("REST request to delete Vendor : {}, {}", id, modelId);
        vendorService.deleteVendorProductModel(modelId);
        return ResponseEntity.noContent().build();
    }
}
