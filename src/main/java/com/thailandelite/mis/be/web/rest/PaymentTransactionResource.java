package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.model.domain.PaymentTransaction;
import com.thailandelite.mis.be.repository.PaymentTransactionRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PaymentTransactionResource {

    private final Logger log = LoggerFactory.getLogger(PaymentTransactionResource.class);

    private static final String ENTITY_NAME = "paymentTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentTransactionRepository paymentTransactionRepository;
    private final FileService fileService;


    @PostMapping("/payment-transactions")
    public ResponseEntity<PaymentTransaction> createPaymentTransaction(@RequestBody PaymentTransaction paymentTransaction) throws URISyntaxException {
        log.debug("REST request to save PaymentTransaction : {}", paymentTransaction);
        if (paymentTransaction.getId() != null) {
            throw new BadRequestAlertException("A new paymentTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentTransaction result = paymentTransactionRepository.save(paymentTransaction);
        return ResponseEntity.created(new URI("/api/payment-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }



    @GetMapping("/payment-transactions")
    public List<PaymentTransaction> getAllPaymentTransactions() {
        log.debug("REST request to get all PaymentTransactions");
        return paymentTransactionRepository.findAll();
    }

    @PutMapping(path = "/payment-transactions", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<PaymentTransaction> updatePaymentTransaction(@RequestPart("bankStatement") MultipartFile file,
                                                                       @RequestPart("body") PaymentTransaction paymentTransaction) throws UploadFailException {
        log.debug("REST request to update PaymentTransaction : {}, {}", paymentTransaction, file);
        String transactionId = paymentTransaction.getId();
        if (transactionId == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        PaymentTransaction transaction = paymentTransactionRepository.findById(transactionId)
            .orElseThrow(() -> new RuntimeException("Not found"));

        if(file != null){
            String upload = fileService.upload("payments/" + transactionId, file);
            transaction.setBankStatement(upload);
        }

        PaymentTransaction result = paymentTransactionRepository.save(paymentTransaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, transactionId))
            .body(result);
    }



    @GetMapping("/payment-transactions/{id}")
    public ResponseEntity<PaymentTransaction> getPaymentTransaction(@PathVariable String id) {
        log.debug("REST request to get PaymentTransaction : {}", id);
        Optional<PaymentTransaction> paymentTransaction = paymentTransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(paymentTransaction);
    }


    @DeleteMapping("/payment-transactions/{id}")
    public ResponseEntity<Void> deletePaymentTransaction(@PathVariable String id) {
        log.debug("REST request to delete PaymentTransaction : {}", id);
        paymentTransactionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
