package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.CommissionIssueRepository;
import com.thailandelite.mis.be.repository.InvoiceRepository;
import com.thailandelite.mis.be.service.AgentRequestService;
import com.thailandelite.mis.be.service.dto.AgentContractRequest;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.be.repository.AgentRepository;
import com.thailandelite.mis.be.service.AgentService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class AgentResource {

    private final Logger log = LoggerFactory.getLogger(AgentResource.class);

    private static final String ENTITY_NAME = "agent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentRepository agentRepository;
    private final AgentService agentService;
    private final AgentRequestService agentRequestService;
    private final ApplicationRepository applicationRepository;
    private final InvoiceRepository invoiceRepository;

    @PostMapping("/agents")
    public ResponseEntity<Agent> createAgent(@RequestBody Agent agent) throws URISyntaxException {
        log.debug("REST request to save Agent : {}", agent);
        if (agent.getId() != null) {
            throw new BadRequestAlertException("A new agent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Agent result = agentRepository.save(agent);
        return ResponseEntity.created(new URI("/api/agents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agents")
    public ResponseEntity<Agent> updateAgent(@RequestBody Agent agent) throws URISyntaxException {
        log.debug("REST request to update Agent : {}", agent);
        if (agent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Agent result = agentRepository.save(agent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agent.getId()))
            .body(result);
    }


    @GetMapping("/agents")
    public ResponseEntity<FlowResponse<Page<Agent>>>  getAllAgents(@ModelAttribute AgentQuery query, Pageable pageable) {
        log.debug("REST request to get all Agents: {}", query);
        Page<Agent> page = agentService.search(query, pageable);
        return ResponseEntity.ok(new FlowResponse<>("", 0, page));
    }

    @GetMapping("/agents/{id}")
    public ResponseEntity<Agent> getAgent(@PathVariable String id) {
        log.debug("REST request to get Agent : {}", id);
        Optional<Agent> agent = agentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agent);
    }


    @DeleteMapping("/agents/{id}")
    public ResponseEntity<Void> deleteAgent(@PathVariable String id) {
        log.debug("REST request to delete Agent : {}", id);
        agentRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PatchMapping("/agents/{id}/email")
    public ResponseEntity<Agent> updateMemberEmail(@PathVariable String id, String email) {
        return ResponseEntity.ok().body(agentRequestService.changeEmailRequest(id, email));
    }

    @PatchMapping("/agents/{id}/password")
    public ResponseEntity<Agent> updateMemberPassword(@PathVariable String id, String password) {
        return ResponseEntity.ok().body(agentRequestService.changePasswordRequest(id, password));
    }

    @PatchMapping("/agents/{id}/status")
    public ResponseEntity<Agent> updateMemberStatus(@PathVariable String id, Agent.AgentStatus status, String remark) {
        return ResponseEntity.ok().body(agentRequestService.changeMemberStatus(id, status, remark));
    }

    @PostMapping("/agents/{id}/contract")
    public ResponseEntity<Agent> uploadVendorContract(@PathVariable String id, @RequestBody AgentContractRequest agentContractRequest){
        return ResponseEntity.ok().body(agentRequestService.changeVendorContract(id, agentContractRequest));
    }

    @GetMapping("/agents/applications")
    public Page<Application> getAllAgentApplication(Pageable page) {
        log.debug("REST request to get Agent member application.");
        return applicationRepository.findByRegisterAgentIdExists(page);
    }

    @GetMapping("/agents/{id}/applications")
    public Page<Application> getAgentApplication(@PathVariable String id, Pageable page) {
        log.debug("REST request to get Agent member application: {}", id);
        return applicationRepository.findByRegisterAgentId(id, page);
    }

    @GetMapping("/agents/invoices")
    public Page<Invoice> getAllAgentApplicationInvoices(Pageable page) {
        log.debug("REST request to get Agent member application.");
        List<Application> applications =  applicationRepository.findByRegisterAgentIdExists();
        List<String> appId = applications.stream().map((app)->app.getId()).collect(Collectors.toList());
        return invoiceRepository.findByDocumentIdInAndPayType(appId, PaymentType.MEMBERSHIP, page);
    }

    @GetMapping("/agents/{id}/invoices")
    public Page<Invoice> getAgentApplicationInvoices(@PathVariable String id, Pageable page) {
        log.debug("REST request to get Agent member application: {}", id);
        List<Application> applications =  applicationRepository.findByRegisterAgentId(id);
        List<String> appId = applications.stream().map((app)->app.getId()).collect(Collectors.toList());
        return invoiceRepository.findByDocumentIdInAndPayType(appId, PaymentType.MEMBERSHIP, page);
    }

    @Data
    public static class AgentQuery{
        String agentTypeId;
        String name;
        Agent.AgentStatus agentStatus;
        String agentCode;
        String agentPhone;
        String agentEmail;
        LocalDate contractDateStart;
        LocalDate contractDateEnd;
    }

    // @GetMapping("/agents/commission")
    // public Float getAllAgents(String commissionId, Integer member) {
    //     log.debug("REST request to get all commission by id {} and member {}", commissionId, member);
    //     return agentService.getCommission(commissionId, member);
    // }
}
