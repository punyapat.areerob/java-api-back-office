package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.be.repository.MemberPrivilegeQuotaRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class MemberPrivilegeQuotaResource {

    private final Logger log = LoggerFactory.getLogger(MemberPrivilegeQuotaResource.class);

    private static final String ENTITY_NAME = "memberPrivilege";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository;

    public MemberPrivilegeQuotaResource(MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository) {
        this.memberPrivilegeQuotaRepository = memberPrivilegeQuotaRepository;
    }

    @PostMapping("/member-privilege-quotas")
    public ResponseEntity<MemberPrivilegeQuota> createMemberPrivilege(@RequestBody MemberPrivilegeQuota memberPrivilegeQuota) throws URISyntaxException {
        log.debug("REST request to save MemberPrivilege : {}", memberPrivilegeQuota);
        if (memberPrivilegeQuota.getId() != null) {
            throw new BadRequestAlertException("A new memberPrivilege cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MemberPrivilegeQuota result = memberPrivilegeQuotaRepository.save(memberPrivilegeQuota);
        return ResponseEntity.created(new URI("/api/member-privilege-quotas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/member-privilege-quotas")
    public ResponseEntity<MemberPrivilegeQuota> updateMemberPrivilege(@RequestBody MemberPrivilegeQuota memberPrivilegeQuota) throws URISyntaxException {
        log.debug("REST request to update MemberPrivilege : {}", memberPrivilegeQuota);
        if (memberPrivilegeQuota.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MemberPrivilegeQuota result = memberPrivilegeQuotaRepository.save(memberPrivilegeQuota);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, memberPrivilegeQuota.getId()))
            .body(result);
    }


    @GetMapping("/member-privilege-quotas")
    public ResponseEntity<List<MemberPrivilegeQuota>> getAllMemberPrivileges(Pageable pageable) {
        log.debug("REST request to get a page of MemberPrivileges");
        Page<MemberPrivilegeQuota> page = memberPrivilegeQuotaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/member-privilege-quotas/{id}")
    public ResponseEntity<MemberPrivilegeQuota> getMemberPrivilege(@PathVariable String id) {
        log.debug("REST request to get MemberPrivilege : {}", id);
        Optional<MemberPrivilegeQuota> memberPrivilege = memberPrivilegeQuotaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(memberPrivilege);
    }


    @DeleteMapping("/member-privilege-quotas/{id}")
    public ResponseEntity<Void> deleteMemberPrivilege(@PathVariable String id) {
        log.debug("REST request to delete MemberPrivilege : {}", id);
        memberPrivilegeQuotaRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
