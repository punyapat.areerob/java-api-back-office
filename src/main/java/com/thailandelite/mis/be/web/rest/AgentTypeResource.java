package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentType;
import com.thailandelite.mis.be.repository.AgentTypeRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class AgentTypeResource {

    private final Logger log = LoggerFactory.getLogger(AgentTypeResource.class);

    private static final String ENTITY_NAME = "agentType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentTypeRepository agentTypeRepository;

    public AgentTypeResource(AgentTypeRepository agentTypeRepository) {
        this.agentTypeRepository = agentTypeRepository;
    }


    @PostMapping("/agent-types")
    public ResponseEntity<AgentType> createAgentType(@RequestBody AgentType agentType) throws URISyntaxException {
        log.debug("REST request to save AgentType : {}", agentType);
        if (agentType.getId() != null) {
            throw new BadRequestAlertException("A new agentType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentType result = agentTypeRepository.save(agentType);
        return ResponseEntity.created(new URI("/api/agent-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-types")
    public ResponseEntity<AgentType> updateAgentType(@RequestBody AgentType agentType) throws URISyntaxException {
        log.debug("REST request to update AgentType : {}", agentType);
        if (agentType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentType result = agentTypeRepository.save(agentType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentType.getId()))
            .body(result);
    }


    @GetMapping("/agent-types")
    public List<AgentType> getAllAgentTypes() {
        log.debug("REST request to get all AgentTypes");
        return agentTypeRepository.findAll();
    }


    @GetMapping("/agent-types/{id}")
    public ResponseEntity<AgentType> getAgentType(@PathVariable String id) {
        log.debug("REST request to get AgentType : {}", id);
        Optional<AgentType> agentType = agentTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentType);
    }


    @DeleteMapping("/agent-types/{id}")
    public ResponseEntity<Void> deleteAgentType(@PathVariable String id) {
        log.debug("REST request to delete AgentType : {}", id);
        agentTypeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
