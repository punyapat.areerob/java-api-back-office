package com.thailandelite.mis.be.web.rest.flows;


import com.thailandelite.mis.be.exception.UploadFailException;

import com.thailandelite.mis.model.dto.CaseInfoDTO;

import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.FlowResponse;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import com.itextpdf.text.DocumentException;

import com.thailandelite.mis.be.service.flow.*;
import com.thailandelite.mis.be.web.rest.IncidentResource;

import com.thailandelite.mis.model.domain.Incident;
import com.thailandelite.mis.model.domain.SearchRequest;
import com.thailandelite.mis.model.domain.booking.Booking;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;



@RestController
@RequiredArgsConstructor
@RequestMapping("/api/incident/flow")
public class IncidentFlowResource {
    private final BPIncidentService bpIncidentService;
    private final Logger log = LoggerFactory.getLogger(IncidentResource.class);

    private static final String ENTITY_NAME = "incident";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;


    @GetMapping
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(SearchRequest searchRequest,
                                                                  @RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,

                                                                  Pageable pageable
    ) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKey("incident")
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .from(searchRequest.getFrom())
            .to(searchRequest.getTo())
            .statuses(searchRequest.getStatus())
            .build();

        Page<CaseInfoDTO> caseInfoDTOS = null;
        caseInfoDTOS = bpIncidentService.queryCaseWithEntity(caseQuery, Incident.class);


        return ResponseEntity.ok(new FlowResponse<>("incident", 0, caseInfoDTOS ));
    }

    @GetMapping("/{caseId}")
    public ResponseEntity<FlowResponse<CaseInfoDTO>> getApplicationTask(@PathVariable String caseId) {
        log.debug("REST request to get JobApplication : {}", caseId);
        CaseInfoDTO caseInfoDTO = bpIncidentService.getDetail(caseId);
        return ResponseEntity.ok(new FlowResponse<>("incident" ,0 , caseInfoDTO));
    }

    @GetMapping("/{taskDefKey}/actions")
    public Enum<?>[] getActionByTaskDefKey(@PathVariable String taskDefKey) {
        log.debug("REST request to get JobApplication : {}", taskDefKey);
        return bpIncidentService.getTaskByTaskDefKey(taskDefKey);

    }


    @PostMapping("/uploads")
    public ResponseEntity<Object> uploadFile(@RequestParam("path") String path,@RequestParam("file") MultipartFile file) throws XPathExpressionException, ParserConfigurationException, UploadFailException {
        return ResponseEntity.ok(new FlowResponse<String>("incident",0, bpIncidentService.uploadFile(path, file)));
    }

    @PostMapping
    public ResponseEntity<FlowResponse<Incident>> create(@RequestBody Incident incident) throws UploadFailException, DocumentException, IOException {
        bpIncidentService.createNoneBooking("incident", incident);
        log.debug("REST request to save createApplication : {}", incident);
        return ResponseEntity.ok(new FlowResponse<>("incident",0 ,incident));
    }

    @GetMapping("/process/{processInstanceId}/{taskDefKey}/{action}")
    public ResponseEntity<Object> processTask(
                                              @PathVariable String processInstanceId,
                                              @PathVariable String taskDefKey,
                                              @PathVariable String action,
                                              @RequestParam String remark) {
        log.debug("REST request to get JobApplication : {}", processInstanceId);

        bpIncidentService.processTask(processInstanceId, taskDefKey, action, SecurityContextHolder.getContext().getAuthentication().getName(), remark);


           return ResponseEntity.ok(new FlowResponse<>("incident",0, "success"));
    }

}
