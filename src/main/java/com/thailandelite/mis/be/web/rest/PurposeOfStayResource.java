package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.PurposeOfStayRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.master.PurposeOfStay;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PurposeOfStayResource {

    private final Logger log = LoggerFactory.getLogger(PurposeOfStayResource.class);

    private static final String ENTITY_NAME = "PurposeOfStay";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PurposeOfStayRepository repository;

    public PurposeOfStayResource(PurposeOfStayRepository repository) {
        this.repository = repository;
    }


    @PostMapping("/purpose-of-stay")
    public ResponseEntity<PurposeOfStay> createPurposeOfStay(@RequestBody PurposeOfStay entity) throws URISyntaxException {
        log.debug("REST request to save BloodType : {}", entity);
        if (entity.getId() != null) {
            throw new BadRequestAlertException("A new bloodType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PurposeOfStay result = repository.save(entity);
        return ResponseEntity.created(new URI("/api/purpose-of-stay/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/purpose-of-stay")
    public ResponseEntity<PurposeOfStay> updatePurposeOfStay(@RequestBody PurposeOfStay entity) throws URISyntaxException {
        log.debug("REST request to update BloodType : {}", entity);
        if (entity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PurposeOfStay result = repository.save(entity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, entity.getId()))
            .body(result);
    }


    @GetMapping("/purpose-of-stay")
    public List<PurposeOfStay> getAllPurposeOfStay() {
        log.debug("REST request to get all BloodTypes");
        return repository.findAll();
    }


    @GetMapping("/purpose-of-stay/{id}")
    public ResponseEntity<PurposeOfStay> getPurposeOfStay(@PathVariable String id) {
        log.debug("REST request to get BloodType : {}", id);
        Optional<PurposeOfStay> entity = repository.findById(id);
        return ResponseUtil.wrapOrNotFound(entity);
    }


    @DeleteMapping("/purpose-of-stay/{id}")
    public ResponseEntity<Void> deletePurposeOfStay(@PathVariable String id) {
        log.debug("REST request to delete BloodType : {}", id);
        repository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
