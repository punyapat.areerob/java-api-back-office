package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.card.PhyCard;
import com.thailandelite.mis.be.repository.PhyCardRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PhyCardResource {

    private final Logger log = LoggerFactory.getLogger(PhyCardResource.class);

    private static final String ENTITY_NAME = "phyCard";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhyCardRepository phyCardRepository;

    public PhyCardResource(PhyCardRepository phyCardRepository) {
        this.phyCardRepository = phyCardRepository;
    }


    @PostMapping("/phy-cards")
    public ResponseEntity<PhyCard> createPhyCard(@RequestBody PhyCard phyCard) throws URISyntaxException {
        log.debug("REST request to save PhyCard : {}", phyCard);
        if (phyCard.getId() != null) {
            throw new BadRequestAlertException("A new phyCard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhyCard result = phyCardRepository.save(phyCard);
        return ResponseEntity.created(new URI("/api/phy-cards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/phy-cards")
    public ResponseEntity<PhyCard> updatePhyCard(@RequestBody PhyCard phyCard) throws URISyntaxException {
        log.debug("REST request to update PhyCard : {}", phyCard);
        if (phyCard.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhyCard result = phyCardRepository.save(phyCard);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, phyCard.getId()))
            .body(result);
    }


    @GetMapping("/phy-cards")
    public List<PhyCard> getAllPhyCards() {
        log.debug("REST request to get all PhyCards");
        return phyCardRepository.findAll();
    }


    @GetMapping("/phy-cards/{id}")
    public ResponseEntity<PhyCard> getPhyCard(@PathVariable String id) {
        log.debug("REST request to get PhyCard : {}", id);
        Optional<PhyCard> phyCard = phyCardRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(phyCard);
    }


    @DeleteMapping("/phy-cards/{id}")
    public ResponseEntity<Void> deletePhyCard(@PathVariable String id) {
        log.debug("REST request to delete PhyCard : {}", id);
        phyCardRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
