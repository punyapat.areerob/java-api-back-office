package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.VendorBillInfoRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.VendorBillInfo;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class VendorBillInfoResource {

    private final Logger log = LoggerFactory.getLogger(VendorBillInfoResource.class);

    private static final String ENTITY_NAME = "shopBillInfo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendorBillInfoRepository vendorBillInfoRepository;

    public VendorBillInfoResource(VendorBillInfoRepository vendorBillInfoRepository) {
        this.vendorBillInfoRepository = vendorBillInfoRepository;
    }

    @GetMapping("/shop-bill-infos")
    public List<VendorBillInfo> getAllVendorBillInfos() {
        log.debug("REST request to get all ShopBillInfos");
        return vendorBillInfoRepository.findAll();
    }


    @GetMapping("/shop-bill-infos/{id}")
    public ResponseEntity<VendorBillInfo> getVendorBillInfo(@PathVariable String id) {
        log.debug("REST request to get ShopBillInfo : {}", id);
        Optional<VendorBillInfo> vendorBillInfo = vendorBillInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(vendorBillInfo);
    }

    @PostMapping("/shop-bill-infos")
    public ResponseEntity<VendorBillInfo> createVendorBillInfo(@RequestBody VendorBillInfo vendorBillInfo) throws URISyntaxException {
        log.debug("REST request to save ShopBillInfo : {}", vendorBillInfo);
        if (vendorBillInfo.getId() != null) {
            throw new BadRequestAlertException("A new shopBillInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorBillInfo result = vendorBillInfoRepository.save(vendorBillInfo);
        return ResponseEntity.created(new URI("/api/shop-bill-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/shop-bill-infos")
    public ResponseEntity<VendorBillInfo> updateVendorBillInfo(@RequestBody VendorBillInfo vendorBillInfo) throws URISyntaxException {
        log.debug("REST request to update ShopBillInfo : {}", vendorBillInfo);
        if (vendorBillInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VendorBillInfo result = vendorBillInfoRepository.save(vendorBillInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vendorBillInfo.getId()))
            .body(result);
    }

    @DeleteMapping("/shop-bill-infos/{id}")
    public ResponseEntity<Void> deleteVendorBillInfo(@PathVariable String id) {
        log.debug("REST request to delete ShopBillInfo : {}", id);
        vendorBillInfoRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
