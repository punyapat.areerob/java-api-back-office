package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.be.domain.SubProductCategory;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.dto.HighlightProductCover;
import com.thailandelite.mis.model.dto.ProductCategoryCover;
import com.thailandelite.mis.model.dto.ProductCover;
import com.thailandelite.mis.model.dto.SubProductCategoryCover;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ProductCategoryResource {

    private final Logger log = LoggerFactory.getLogger(ProductCategoryResource.class);

    private static final String ENTITY_NAME = "productCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductCategoryRepository productCategoryRepository;
    private final SubProductCategoryRepository subProductCategoryRepository;
    private final ProductRepository productRepository;
    private final ShopRepository shopRepository;

    private final VendorContactRepository contactRepository;
    private final ProductModelRepository productModelRepository;
    private final PrivilegeRepository privilegeRepository;
    private final ProductVariantGroupRepository productVariantGroupRepository;
    private final ProductVariantValueRepository productVariantValueRepository;

    @PostMapping("/product-categories")
    public ResponseEntity<ProductCategory> createProductCategory(@RequestBody ProductCategory productCategory) throws URISyntaxException {
        log.debug("REST request to save ProductCategory : {}", productCategory);
        if (productCategory.getId() != null) {
            throw new BadRequestAlertException("A new productCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductCategory result = productCategoryRepository.save(productCategory);
        return ResponseEntity.created(new URI("/api/product-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/product-categories")
    public ResponseEntity<ProductCategory> updateProductCategory(@RequestBody ProductCategory productCategory) throws URISyntaxException {
        log.debug("REST request to update ProductCategory : {}", productCategory);
        if (productCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductCategory result = productCategoryRepository.save(productCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productCategory.getId()))
            .body(result);
    }


    @GetMapping("/product-categories")
    public List<ProductCategory> getAllProductCategories() {
        log.debug("REST request to get all ProductCategories");
        return productCategoryRepository.findAll();
    }


    @GetMapping("/product-categories/{id}")
    public ResponseEntity<ProductCategory> getProductCategory(@PathVariable String id) {
        log.debug("REST request to get ProductCategory : {}", id);
        Optional<ProductCategory> productCategory = productCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productCategory);
    }


    @DeleteMapping("/product-categories/{id}")
    public ResponseEntity<Void> deleteProductCategory(@PathVariable String id) {
        log.debug("REST request to delete ProductCategory : {}", id);
        productCategoryRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/product-categories/cover")
    public ResponseEntity<List<ProductCategoryCover>> getAllCategory() {
        log.debug("REST request to get Privileges");

        List<ProductCategory> productCategories = productCategoryRepository.findAll();
        log.debug("productCategories: {}", productCategories);
        List<ProductCategoryCover> productCategoryCovers = new ArrayList<>();
        for (ProductCategory productCategory : productCategories) {
            ProductCategoryCover productCategoryCover = new ProductCategoryCover();
            productCategoryCover.setProductCategoryId(productCategory.getId());
            productCategoryCover.setName(productCategory.getName());
            productCategoryCover.setIconUrl(productCategory.getIcon());

            List<SubProductCategory> subProductCategories = subProductCategoryRepository.findByProductCategoryId(productCategory.getId());
            log.debug("subProductCategories: {}", subProductCategories);
            List<SubProductCategoryCover> subProductCategoryCoverList = new ArrayList<>();
            for (SubProductCategory subProductCategory : subProductCategories) {
                SubProductCategoryCover subProductCategoryCover = new SubProductCategoryCover();
                subProductCategoryCover.setName(subProductCategory.getName());
                subProductCategoryCover.setSubProductCategoryId(subProductCategory.getId());
                subProductCategoryCoverList.add(subProductCategoryCover);
            }

            productCategoryCover.setSubProductCategoryLists(subProductCategoryCoverList);
            productCategoryCovers.add(productCategoryCover);
        }
        return ResponseEntity.ok().body(productCategoryCovers);
    }

    @PostMapping("/product-categories/sub-categories/products")
    public ResponseEntity<List<ProductCover>> getProductCover(@RequestBody List<String> ids) {
        List<ProductCover> productCovers = new ArrayList<>();
        Iterable<SubProductCategory> subProductCategories = subProductCategoryRepository.findAllById(ids);
        HashSet<Product> productSet = new HashSet<>();
        for (SubProductCategory subProductCategory : subProductCategories) {
            Set<Product> products = subProductCategory.getProductLists();
            for (Product product : products) {
                productSet.add(product);
            }
        }
        HashMap<String, Shop> shopHashMap = new HashMap<>();
        for (Product product : productSet) {
            if(product.getShopId() != null){
                Shop shop = shopHashMap.get(product.getShopId());
                if(shop == null){
                    Optional<Shop> shopOptional = shopRepository.findById(product.getShopId());
                    shop = shopOptional.isPresent() ? shopOptional.get() : null;
                }
                if(shop == null)
                    continue;

                ProductCover productCover = createProductCover(product, shop);
                productCovers.add(productCover);
                shopHashMap.put(product.getShopId(), shop);
            }
        }

        return ResponseEntity.ok().body(productCovers);
    }

    private ProductCover createProductCover(Product product, Shop shop) {
        ProductCover productCover = new ProductCover();
        productCover.setProduct(product);
        productCover.setShop(shop);
        List<VendorContact> vendorContacts = (List<VendorContact>) contactRepository.findAllById(shop.getContactId());
        productCover.setContacts(vendorContacts);
        List<ProductModel> productModels = (List<ProductModel>) productModelRepository.findAllById(product.getModelIds());

        Set<String> privilegeId = productModels.stream()
            .map(ProductModel::getPrivilegesId)
            .filter(Objects::nonNull)
            .flatMap(Collection::stream)
            .collect(Collectors.toSet());

        productCover.setProductModels(productModels);

        List<Privilege> privileges = (List<Privilege>) privilegeRepository.findAllById(privilegeId);
        List<String> privilegesName = privileges.stream().map(Privilege::getNameEN).collect(toList());
        productCover.setPrivilegeNames(privilegesName);

        List<ProductVariantGroup> productVariantGroups = (List<ProductVariantGroup>) productVariantGroupRepository.findAllById(product.getVariantGroup());
        for (ProductVariantGroup productVariantGroup : productVariantGroups) {
            List<ProductVariantValue> productVariantValues = productVariantValueRepository.findByProductVariantGroupId(productVariantGroup.getId());
            productVariantGroup.setProductVariantValue(productVariantValues);
        }
        productCover.setProductVariantGroups(productVariantGroups);

        return productCover;
    }

    @GetMapping("/product-categories/product/{id}")
    public ResponseEntity<ProductCover> getProductCover(@PathVariable String id) {
        Product product = productRepository.findById(id).orElseThrow(() ->  new NotFoundException("Not found product") );
        Shop shop = shopRepository.findById(product.getShopId()).orElseThrow(() ->  new NotFoundException("Not found shop") );
        ProductCover productCover = createProductCover(product, shop);
        return ResponseEntity.ok().body(productCover);
    }

    @GetMapping("/product-categories/product/highlight")
    public ResponseEntity<HighlightProductCover> getHighlightProductCovers(HighlightProductCover.HighlightType type) {
        log.debug("REST request to get HighlightProductCovers");
        HighlightProductCover highlightProductCover = new HighlightProductCover();
        List<Product> products = productRepository.findAll();
        if(HighlightProductCover.HighlightType.NEW.equals(type)){
            highlightProductCover.setName("NEW PRIVILEGE");
        }else if(HighlightProductCover.HighlightType.RECOMMENDED.equals(type)){
            highlightProductCover.setName("RECOMMENDED");
        }
        products = getRandomProduct(products, 10);
        List<ProductCover> productListCovers = new ArrayList<>();
        for (Product product : products) {
            Optional<Shop> shop = shopRepository.findById(product.getShopId());
            if(shop.isPresent()){
                ProductCover productCover = createProductCover(product, shop.get());
                productListCovers.add(productCover);
            }

        }
        highlightProductCover.setProductCoverList(productListCovers);
        return ResponseEntity.ok().body(highlightProductCover);
    }

    private List<Product> getRandomProduct(List<Product> list, int totalItems)
    {
        Random rand = new Random();
        List<Product> newList = new ArrayList<>();
        for (int i = 0; i < totalItems; i++) {
            int randomIndex = rand.nextInt(list.size());
            newList.add(list.get(randomIndex));
        }
        return newList;
    }

}
