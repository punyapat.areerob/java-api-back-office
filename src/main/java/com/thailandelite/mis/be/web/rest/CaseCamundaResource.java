package com.thailandelite.mis.be.web.rest;


import com.thailandelite.mis.be.service.CamundaService;

import com.thailandelite.mis.be.service.dto.MenuDTO;
import com.thailandelite.mis.model.CommonPaginationResponseDto;
import com.thailandelite.mis.model.Util;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.bind.annotation.*;

import java.util.*;



@RestController
@RequestMapping("/api")
public class CaseCamundaResource {
    @Autowired
    private CamundaService camundaService;

    private final Logger log = LoggerFactory.getLogger(CaseCamundaResource.class);

    @GetMapping("/case/group/{candidateGroup}")
    public CommonPaginationResponseDto<List<TaskDto>> getTaskByCandidateKeys(@PathVariable String candidateGroup) {
        log.debug("REST request to get all JobApplications");

        List<TaskDto> result = camundaService.findTaskByGroup(candidateGroup);
        Map<String, Object> queryStrings  = new HashMap() {{
            put("page", 1);
            put("limit", 100);
        }};

        Page<TaskDto> page = new PageImpl<>(result);

        CommonPaginationResponseDto<List<TaskDto>> paginationResponse = new CommonPaginationResponseDto<>();
        paginationResponse.setMeta(Util.getPaginationMetaData(page, queryStrings));
        paginationResponse.setData(result);

        return paginationResponse;
    }


    @GetMapping("/case/menus")
    public CommonPaginationResponseDto<List<MenuDTO>> getCandidateGroups() {
        log.debug("REST request to get all Menu");

        List<Group> result = camundaService.getCandidateGroups();
        Map<String, Object> queryStrings  = new HashMap() {{
            put("page", 1);
            put("limit", 100);
        }};

        ArrayList<MenuDTO> menuDTOS = new ArrayList<>();

        for (Group gr:
             result) {
            String [] team = gr.getName().split("-");
            if (team.length > 1)
            {
                MenuDTO menu = new MenuDTO();
                menu.setTeam(team[1]);
                menu.setFlow(team[0]);
                menu.setCandidateGroup(team[0]+"-"+team[1]);
                menuDTOS.add(menu);
            }
        }


        Page<MenuDTO> page = new PageImpl<>(menuDTOS);

        CommonPaginationResponseDto<List<MenuDTO>> paginationResponse = new CommonPaginationResponseDto<>();
        paginationResponse.setMeta(Util.getPaginationMetaData(page, queryStrings));
        paginationResponse.setData(menuDTOS);

        return paginationResponse;
    }
}

