package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.User;
import com.thailandelite.mis.be.security.DomainUserDetailsService;
import com.thailandelite.mis.be.security.jwt.JWTFilter;
import com.thailandelite.mis.be.security.jwt.TokenProvider;
import com.thailandelite.mis.be.service.core.ADService;
import com.thailandelite.mis.be.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.naming.NamingException;
import javax.validation.Valid;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserJWTController {
    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final DomainUserDetailsService domainUserDetailsService;

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) throws NamingException {
        User user = domainUserDetailsService.getUserByUsername(loginVM.getUsername());

        Authentication authentication = null;
        if(user.getLoginType() == User.LoginType.LDAP){
            authentication = domainUserDetailsService.authenticateAD(user, loginVM.getUsername(), loginVM.getPassword());
        }else {
            UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());
            authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();

//        Authentication authentication2 = new UsernamePasswordAuthenticationToken();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    public static class JWTToken {

        private String idToken;

        public JWTToken() {
        }

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        public String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }

}
