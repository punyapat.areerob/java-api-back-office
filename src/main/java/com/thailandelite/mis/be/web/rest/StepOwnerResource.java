package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.StepOwner;
import com.thailandelite.mis.be.repository.StepOwnerRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class StepOwnerResource {

    private final Logger log = LoggerFactory.getLogger(StepOwnerResource.class);

    private static final String ENTITY_NAME = "stepOwner";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StepOwnerRepository stepOwnerRepository;

    public StepOwnerResource(StepOwnerRepository stepOwnerRepository) {
        this.stepOwnerRepository = stepOwnerRepository;
    }


    @PostMapping("/step-owners")
    public ResponseEntity<StepOwner> createStepOwner(@RequestBody StepOwner stepOwner) throws URISyntaxException {
        log.debug("REST request to save StepOwner : {}", stepOwner);
        if (stepOwner.getId() != null) {
            throw new BadRequestAlertException("A new stepOwner cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StepOwner result = stepOwnerRepository.save(stepOwner);
        return ResponseEntity.created(new URI("/api/step-owners/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/step-owners")
    public ResponseEntity<StepOwner> updateStepOwner(@RequestBody StepOwner stepOwner) throws URISyntaxException {
        log.debug("REST request to update StepOwner : {}", stepOwner);
        if (stepOwner.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StepOwner result = stepOwnerRepository.save(stepOwner);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, stepOwner.getId()))
            .body(result);
    }


    @GetMapping("/step-owners")
    public List<StepOwner> getAllStepOwners() {
        log.debug("REST request to get all StepOwners");
        return stepOwnerRepository.findAll();
    }


    @GetMapping("/step-owners/{id}")
    public ResponseEntity<StepOwner> getStepOwner(@PathVariable String id) {
        log.debug("REST request to get StepOwner : {}", id);
        Optional<StepOwner> stepOwner = stepOwnerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(stepOwner);
    }


    @DeleteMapping("/step-owners/{id}")
    public ResponseEntity<Void> deleteStepOwner(@PathVariable String id) {
        log.debug("REST request to delete StepOwner : {}", id);
        stepOwnerRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
