package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentCommissionRate;
import com.thailandelite.mis.be.repository.AgentCommissionRateRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class AgentCommissionRateResource {

    private final Logger log = LoggerFactory.getLogger(AgentCommissionRateResource.class);

    private static final String ENTITY_NAME = "agentCommissionRate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentCommissionRateRepository agentCommissionRateRepository;

    public AgentCommissionRateResource(AgentCommissionRateRepository agentCommissionRateRepository) {
        this.agentCommissionRateRepository = agentCommissionRateRepository;
    }


    @PostMapping("/agent-commission-rates")
    public ResponseEntity<AgentCommissionRate> createAgentCommissionRate(@RequestBody AgentCommissionRate agentCommissionRate) throws URISyntaxException {
        log.debug("REST request to save AgentCommissionRate : {}", agentCommissionRate);
        if (agentCommissionRate.getId() != null) {
            throw new BadRequestAlertException("A new agentCommissionRate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentCommissionRate result = agentCommissionRateRepository.save(agentCommissionRate);
        return ResponseEntity.created(new URI("/api/agent-commission-rates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-commission-rates")
    public ResponseEntity<AgentCommissionRate> updateAgentCommissionRate(@RequestBody AgentCommissionRate agentCommissionRate) throws URISyntaxException {
        log.debug("REST request to update AgentCommissionRate : {}", agentCommissionRate);
        if (agentCommissionRate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentCommissionRate result = agentCommissionRateRepository.save(agentCommissionRate);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentCommissionRate.getId()))
            .body(result);
    }


    @GetMapping("/agent-commission-rates")
    public List<AgentCommissionRate> getAllAgentCommissionRates() {
        log.debug("REST request to get all AgentCommissionRates");
        return agentCommissionRateRepository.findAll();
    }


    @GetMapping("/agent-commission-rates/{id}")
    public ResponseEntity<AgentCommissionRate> getAgentCommissionRate(@PathVariable String id) {
        log.debug("REST request to get AgentCommissionRate : {}", id);
        Optional<AgentCommissionRate> agentCommissionRate = agentCommissionRateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentCommissionRate);
    }


    @DeleteMapping("/agent-commission-rates/{id}")
    public ResponseEntity<Void> deleteAgentCommissionRate(@PathVariable String id) {
        log.debug("REST request to delete AgentCommissionRate : {}", id);
        agentCommissionRateRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
