package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CommissionIssue;
import com.thailandelite.mis.be.repository.CommissionIssueRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CommissionIssueResource {

    private final Logger log = LoggerFactory.getLogger(CommissionIssueResource.class);

    private static final String ENTITY_NAME = "commissionIssue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionIssueRepository commissionIssueRepository;

    public CommissionIssueResource(CommissionIssueRepository commissionIssueRepository) {
        this.commissionIssueRepository = commissionIssueRepository;
    }


    @PostMapping("/commission-issues")
    public ResponseEntity<CommissionIssue> createCommissionIssue(@RequestBody CommissionIssue commissionIssue) throws URISyntaxException {
        log.debug("REST request to save CommissionIssue : {}", commissionIssue);
        if (commissionIssue.getId() != null) {
            throw new BadRequestAlertException("A new commissionIssue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionIssue result = commissionIssueRepository.save(commissionIssue);
        return ResponseEntity.created(new URI("/api/commission-issues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/commission-issues")
    public ResponseEntity<CommissionIssue> updateCommissionIssue(@RequestBody CommissionIssue commissionIssue) throws URISyntaxException {
        log.debug("REST request to update CommissionIssue : {}", commissionIssue);
        if (commissionIssue.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommissionIssue result = commissionIssueRepository.save(commissionIssue);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionIssue.getId()))
            .body(result);
    }


    @GetMapping("/commission-issues")
    public List<CommissionIssue> getAllCommissionIssues() {
        log.debug("REST request to get all CommissionIssues");
        return commissionIssueRepository.findAll();
    }


    @GetMapping("/commission-issues/{id}")
    public ResponseEntity<CommissionIssue> getCommissionIssue(@PathVariable String id) {
        log.debug("REST request to get CommissionIssue : {}", id);
        Optional<CommissionIssue> commissionIssue = commissionIssueRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(commissionIssue);
    }


    @DeleteMapping("/commission-issues/{id}")
    public ResponseEntity<Void> deleteCommissionIssue(@PathVariable String id) {
        log.debug("REST request to delete CommissionIssue : {}", id);
        commissionIssueRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
