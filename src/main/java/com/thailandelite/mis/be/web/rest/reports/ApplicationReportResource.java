package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.ApplicationReportResponse;
import com.thailandelite.mis.be.service.dto.reports.BaseReportRequest;
import com.thailandelite.mis.be.service.dto.reports.CountReportResponse;
import com.thailandelite.mis.be.service.report.ApplicationReportService;
import com.thailandelite.mis.be.service.report.MemberReportService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Application;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApplicationReportResource {

    private final ApplicationReportService reportService;

    @GetMapping("/reports/application")
    public ResponseEntity<FlowResponse<Page<Application>>> getApplicationReport(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getApplicationReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/application/excel")
    public byte[] getApplicationReportExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getApplicationExcel(searchRequest, pageable);
    }

    @GetMapping("/reports/application-time-period")
    public ResponseEntity<FlowResponse<Page<ApplicationReportResponse>>> getApplicationTimePeriodReport(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getApplicationTimePeriodReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/application-time-period/excel")
    public byte[] getApplicationTimePeriodReportExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getApplicationTimePeriodExcel(searchRequest, pageable);
    }

    @GetMapping("/reports/application-nationality")
    public ResponseEntity<FlowResponse<Page<CountReportResponse>>> getApplicationByNationalReport(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getApplicationByNationalReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/application-nationality/excel")
    public byte[] getApplicationByNationalReportExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getApplicationByNationalExcel(searchRequest, pageable);
    }

}
