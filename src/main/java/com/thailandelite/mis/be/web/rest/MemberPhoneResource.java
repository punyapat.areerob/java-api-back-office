package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.MemberPhone;
import com.thailandelite.mis.be.repository.MemberPhoneRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link MemberPhone}.
 */
@RestController
@RequestMapping("/api")
public class MemberPhoneResource {

    private final Logger log = LoggerFactory.getLogger(MemberPhoneResource.class);

    private static final String ENTITY_NAME = "memberPhone";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MemberPhoneRepository memberPhoneRepository;

    public MemberPhoneResource(MemberPhoneRepository memberPhoneRepository) {
        this.memberPhoneRepository = memberPhoneRepository;
    }

    /**
     * {@code POST  /member-phones} : Create a new memberPhone.
     *
     * @param memberPhone the memberPhone to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new memberPhone, or with status {@code 400 (Bad Request)} if the memberPhone has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/member-phones")
    public ResponseEntity<MemberPhone> createMemberPhone(@RequestBody MemberPhone memberPhone) throws URISyntaxException {
        log.debug("REST request to save MemberPhone : {}", memberPhone);
        if (memberPhone.getId() != null) {
            throw new BadRequestAlertException("A new memberPhone cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MemberPhone result = memberPhoneRepository.save(memberPhone);
        return ResponseEntity.created(new URI("/api/member-phones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /member-phones} : Updates an existing memberPhone.
     *
     * @param memberPhone the memberPhone to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated memberPhone,
     * or with status {@code 400 (Bad Request)} if the memberPhone is not valid,
     * or with status {@code 500 (Internal Server Error)} if the memberPhone couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/member-phones")
    public ResponseEntity<MemberPhone> updateMemberPhone(@RequestBody MemberPhone memberPhone) throws URISyntaxException {
        log.debug("REST request to update MemberPhone : {}", memberPhone);
        if (memberPhone.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MemberPhone result = memberPhoneRepository.save(memberPhone);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, memberPhone.getId()))
            .body(result);
    }

    /**
     * {@code GET  /member-phones} : get all the memberPhones.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of memberPhones in body.
     */
    @GetMapping("/member-phones")
    public List<MemberPhone> getAllMemberPhones() {
        log.debug("REST request to get all MemberPhones");
        return memberPhoneRepository.findAll();
    }

    /**
     * {@code GET  /member-phones/:id} : get the "id" memberPhone.
     *
     * @param id the id of the memberPhone to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the memberPhone, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/member-phones/{id}")
    public ResponseEntity<MemberPhone> getMemberPhone(@PathVariable String id) {
        log.debug("REST request to get MemberPhone : {}", id);
        Optional<MemberPhone> memberPhone = memberPhoneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(memberPhone);
    }

    /**
     * {@code DELETE  /member-phones/:id} : delete the "id" memberPhone.
     *
     * @param id the id of the memberPhone to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/member-phones/{id}")
    public ResponseEntity<Void> deleteMemberPhone(@PathVariable String id) {
        log.debug("REST request to delete MemberPhone : {}", id);
        memberPhoneRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
