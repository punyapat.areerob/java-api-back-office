package com.thailandelite.mis.be.web.rest.flows;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.InvoiceRepository;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.be.service.flow.BPMembershipTransactionService;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/api/member-payments/flow")
@RequiredArgsConstructor
public class MemberPaymentBySLSResource {

    private final Logger log = LoggerFactory.getLogger(MemberPaymentBySLSResource.class);

    private static final String ENTITY_NAME = "invoice";
    private final String flowDefinitionKey = "member_payment_by_sls";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InvoiceRepository invoiceRepository;
    private final BPMembershipTransactionService bpMembershipTransactionService;


    @PostMapping
    public ResponseEntity<FlowResponse<MemberPayment>> createMemberPayment(@RequestBody MemberPayment memberPayment) {
        log.debug("REST request to save createApplication : {}", memberPayment);
        bpMembershipTransactionService.create(flowDefinitionKey,memberPayment);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey,0,memberPayment));
    }

    @PutMapping
    public ResponseEntity<FlowResponse<MemberPayment>> updateMemberPayment(@RequestBody MemberPayment memberPayment) {
        log.debug("REST request to save createApplication : {}", memberPayment);
        bpMembershipTransactionService.save(memberPayment);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey,0,memberPayment));
    }

    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }


    @GetMapping
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(SearchRequest searchRequest,
                                                                  @RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                  @RequestParam(defaultValue = "") String status,
                                                                  @RequestParam(defaultValue = "") String memberId,
                                                                  Pageable pageable) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKey(flowDefinitionKey)
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .memberId(memberId)
            .name(searchRequest.getName())
            .surname(searchRequest.getSurname())
            .from(searchRequest.getFrom())
            .to(searchRequest.getTo())
            .status(status).build();

        Page<CaseInfoDTO> applications = bpMembershipTransactionService.queryCaseWithEntity(caseQuery, MemberPayment.class);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey, bpMembershipTransactionService.queryUnseen(caseQuery), applications ));
    }


/*
    @GetMapping
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(@RequestParam(defaultValue = "") String candidateGroup,
                                                                                                @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                                                @RequestParam(defaultValue = "") String status,
                                                                                                Pageable pageable) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKey(flowDefinitionKey)
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .status(status).build();

        Page<CaseInfoDTO> applications = bpMembershipTransactionService.queryCase(caseQuery);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey,bpMembershipTransactionService.queryUnseen(caseQuery), applications));
    }

 */

    @GetMapping("/{caseId}")
    public ResponseEntity<FlowResponse<CaseInfoDTO>> getApplicationTask(@PathVariable String caseId) {
        log.debug("REST request to get JobApplication : {}", caseId);
        CaseInfoDTO caseInfoDTO = bpMembershipTransactionService.getDetail(caseId);
        return ResponseEntity.ok(new FlowResponse<>(flowDefinitionKey,0, caseInfoDTO));
    }

    @GetMapping("/process/{processInstanceId}/{taskDefKey}/{action}")
    public ResponseEntity<Object> process(@PathVariable String processInstanceId, @PathVariable String taskDefKey, @PathVariable String action,@RequestParam String remark) {
        log.debug("REST request to get JobApplication : {}", processInstanceId);
        bpMembershipTransactionService.processTask(processInstanceId, taskDefKey, action , SecurityContextHolder.getContext().getAuthentication().getName(),remark);
        return ResponseEntity.ok(new FlowResponse<String>(flowDefinitionKey,0,"success"));
    }

    @PostMapping("/uploads")
    public ResponseEntity<Object> uploadFile(@RequestParam("path") String path,@RequestParam("file") MultipartFile file) throws XPathExpressionException, ParserConfigurationException, UploadFailException {
        return ResponseEntity.ok(new FlowResponse<String>(flowDefinitionKey,0, bpMembershipTransactionService.uploadFile(path, file)));
    }

    @PostMapping("/download")
    public @ResponseBody byte[] downloadFile(@RequestParam("path") String path) throws Exception, XPathExpressionException, ParserConfigurationException {
        return  bpMembershipTransactionService.downloadFile(path);
    }
}
