package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.ChannelKnow;
import com.thailandelite.mis.be.repository.ChannelKnowRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ChannelKnowResource {

    private final Logger log = LoggerFactory.getLogger(ChannelKnowResource.class);

    private static final String ENTITY_NAME = "channelKnow";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChannelKnowRepository channelKnowRepository;

    public ChannelKnowResource(ChannelKnowRepository channelKnowRepository) {
        this.channelKnowRepository = channelKnowRepository;
    }


    @PostMapping("/channel-knows")
    public ResponseEntity<ChannelKnow> createChannelKnow(@RequestBody ChannelKnow channelKnow) throws URISyntaxException {
        log.debug("REST request to save ChannelKnow : {}", channelKnow);
        if (channelKnow.getId() != null) {
            throw new BadRequestAlertException("A new channelKnow cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChannelKnow result = channelKnowRepository.save(channelKnow);
        return ResponseEntity.created(new URI("/api/channel-knows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/channel-knows")
    public ResponseEntity<ChannelKnow> updateChannelKnow(@RequestBody ChannelKnow channelKnow) throws URISyntaxException {
        log.debug("REST request to update ChannelKnow : {}", channelKnow);
        if (channelKnow.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChannelKnow result = channelKnowRepository.save(channelKnow);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, channelKnow.getId()))
            .body(result);
    }


    @GetMapping("/channel-knows")
    public List<ChannelKnow> getAllChannelKnows() {
        log.debug("REST request to get all ChannelKnows");
        return channelKnowRepository.findAll();
    }


    @GetMapping("/channel-knows/{id}")
    public ResponseEntity<ChannelKnow> getChannelKnow(@PathVariable String id) {
        log.debug("REST request to get ChannelKnow : {}", id);
        Optional<ChannelKnow> channelKnow = channelKnowRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(channelKnow);
    }


    @DeleteMapping("/channel-knows/{id}")
    public ResponseEntity<Void> deleteChannelKnow(@PathVariable String id) {
        log.debug("REST request to delete ChannelKnow : {}", id);
        channelKnowRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
