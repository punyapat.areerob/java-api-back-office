package com.thailandelite.mis.be.web.rest;
import com.thailandelite.mis.be.service.*;
import com.thailandelite.mis.be.service.model.MembershipInfo;
import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.be.repository.*;

import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.application.DocumentForm;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import com.thailandelite.mis.model.domain.enumeration.GetMemberState;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.domain.enumeration.TransferPackageRequestStatus;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


@RestController
@RequestMapping("/api")
public class MemberResource {

    private final Logger log = LoggerFactory.getLogger(MemberResource.class);

    private static final String ENTITY_NAME = "member";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MemberRepository memberRepository;
    private final InvoiceRepository invoiceRepository;
    private final ApplicationRepository applicationRepository;
    private final MembershipRepository membershipRepository;
    private final CardPrivilegeRepository cardPrivilegeRepository;

    private final MemberService memberService;
    private final MemberRequestService memberRequestService;
    private final TransferPackageRequestRepository transferPackageRequestRepository;
    private final NotificationService notificationService;
    private final BookingService bookingService;
    private final MemberFinderService memberFinderService;

    public MemberResource(MemberRepository memberRepository, InvoiceRepository invoiceRepository, ApplicationRepository applicationRepository, MembershipRepository membershipRepository, CardPrivilegeRepository cardPrivilegeRepository, MemberService memberService, MemberRequestService memberRequestService, TransferPackageRequestRepository transferPackageRequestRepository, NotificationService notificationService, BookingService bookingService, MemberFinderService memberFinderService) {
        this.memberRepository = memberRepository;
        this.invoiceRepository = invoiceRepository;
        this.applicationRepository = applicationRepository;
        this.membershipRepository = membershipRepository;
        this.cardPrivilegeRepository = cardPrivilegeRepository;
        this.memberService = memberService;
        this.memberRequestService = memberRequestService;
        this.transferPackageRequestRepository = transferPackageRequestRepository;
        this.notificationService = notificationService;
        this.bookingService = bookingService;
        this.memberFinderService = memberFinderService;
    }


    @PostMapping("/members")
    public ResponseEntity<Member> createMember(@RequestBody Member member) throws URISyntaxException {
        log.debug("REST request to save Member : {}", member);
        if (member.getId() != null) {
            throw new BadRequestAlertException("A new member cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Member result = memberRepository.save(member);
        return ResponseEntity.created(new URI("/api/members/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/members")
    public ResponseEntity<Member> updateMember(@RequestBody Member member) throws URISyntaxException {
        log.debug("REST request to update Member : {}", member);
        if (member.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Member result = memberRepository.save(member);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, member.getId()))
            .body(result);
    }


    @GetMapping("/members")
    public ResponseEntity<List<Member>> getAllMembers(Pageable pageable) {
        log.debug("REST request to get a page of Members");
        Page<Member> page = memberRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/members/search")
    public ResponseEntity<Page<Member>> getAllActiveMembers(
        @RequestParam(required = false) String email,
        @RequestParam(required = false) String membershipId,
        @RequestParam(required = false) String mobileInThai, GetMemberState state, Pageable pageable) {
//        members/search?email=222&membershipId=111&mobileInThai=0989898988&page=0&size=50&state=ALL
        log.debug("REST request to get a page of Members");
        Page<Member> page;
        if(state.equals(GetMemberState.ALL)) {
            page = memberRepository.findAllByOrderByCreatedDate(pageable);
//            page = memberFinderService.findMemberBy(email, membershipId, mobileInThai, pageable);
        }else if(state.equals(GetMemberState.PROSPECT)){
            page = memberRepository.findByStatus("SUBMIT APPLICATION", pageable);
        }else if(state.equals(GetMemberState.ACTIVE)){
            page = memberRepository.findByRecordStatus("N", pageable);
        }else if(state.equals(GetMemberState.EXPIRED)){
            page = memberRepository.findByRecordStatus("D", pageable);
        }else{
            page = memberRepository.findAll(pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/members/{id}")
    public ResponseEntity<Member> getMember(@PathVariable String id) {
        log.debug("REST request to get Member : {}", id);
        Optional<Member> member = memberRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(member);
    }


    @DeleteMapping("/members/{id}")
    public ResponseEntity<Void> deleteMember(@PathVariable String id) {
        log.debug("REST request to delete Member : {}", id);
        memberRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/members/{id}/invoices")
    public ResponseEntity<List<Invoice>> getMemberInvoice(@PathVariable String id,
                                                        Pageable pageable) {

        List<Invoice> page =  invoiceRepository.findByMemberId(id);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/members/{id}/applications")
    public ResponseEntity<Page<Application>> getMemberApplications(@PathVariable String id,
                                                          Pageable pageable) {

        Page<Application> page =  applicationRepository.findByMemberId(id, pageable);
        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/members/{id}/applications/documents")
    public ResponseEntity<Page<DocumentForm>> getMemberApplicationsDocuments(@PathVariable String id,
                                                                   Pageable pageable) {

        Page<Application> page =  applicationRepository.findByMemberId(id, pageable);
        List<DocumentForm> documentForms = new ArrayList<>();
        for (Application application : page) {
            documentForms.add(application.getDocumentForm());
        }
        Page<DocumentForm> documentFormPage = new PageImpl<>(documentForms);
        return ResponseEntity.ok().body(documentFormPage);
    }

    @PatchMapping("/members/{id}/email")
    public ResponseEntity<Member> updateMemberEmail(@PathVariable String id, String email) {
        return ResponseEntity.ok().body(memberRequestService.changeEmailRequest(id, email));
    }

    @PatchMapping("/members/{id}/password")
    public ResponseEntity<Member> updateMemberPassword(@PathVariable String id, String password) {
        return ResponseEntity.ok().body(memberRequestService.changePasswordRequest(id, password));
    }

    @PatchMapping("/members/{id}/status")
    public ResponseEntity<Member> updateMemberStatus(@PathVariable String id, MemberStatus status, String remark) {
        return ResponseEntity.ok().body(memberRequestService.changeMemberStatus(id, status, remark));
    }

    @PatchMapping("/members/{id}/card-package")
    public ResponseEntity<Member> updateMemberCardPackage(@PathVariable String id, String cardPackageId, String remark) throws HandlerException {
        return ResponseEntity.ok().body(memberRequestService.upgradeMemberPackageRequest(id, cardPackageId, remark));
    }

    @GetMapping("/members/{id}/quota")
    public List<MemberPrivilegeQuota> getMembershipQuota(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        return memberService.getPrivilegeQuota(id);
    }

    @GetMapping("/members/{id}/membership")
    public MembershipInfo getMembership(@PathVariable String id) throws HandlerException {
        log.debug("REST request to get Membership : {}", id);
        return memberService.getMembershipInfo(id);
    }


    @PatchMapping("/members/{id}/transfer")
    public ResponseEntity<Member> transferMemberPackage(@PathVariable String id, String email) throws HandlerException {
        return ResponseEntity.ok().body(memberRequestService.transferMemberRequest(id, email));
    }

    @GetMapping("/members/{id}/tranfer-package-request")
    public List<TransferPackageRequest> getTransferPackageRequest(@PathVariable String id, String emailRequest) {
        log.debug("REST request to get Membership : {}, {}", id, emailRequest);
        return transferPackageRequestRepository.findByToEmailAndStatus(emailRequest, TransferPackageRequestStatus.CREATE);
    }

    @PostMapping("/members/test/notify-email")
    public void testNotifyEmail(
        @RequestBody NotificationService.TransferPackageWelcome tpw, @RequestParam String to) throws IOException {
        log.debug("REST testNotifyEmail: {}, {}", tpw, to);
        notificationService.transferPackageWelcome(to, tpw);
    }

    @GetMapping("/members/{id}/bookings")
    public List<Booking> fetchMyBooking(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        return bookingService.fetchBookingByMember(id);
    }

    @GetMapping("/members/{id}/wrap-bookings")
    public List<BookingResponse> fetchWrapMyBooking(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        return bookingService.fetchWrapBookingByMember(id);
    }

    @GetMapping("/members/{id}/case/bookings")
    public List<BookingResponse> fetchWrapMyBookingInCase(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        return bookingService.fetchWrapMyBookingInCase(id);
    }

    @GetMapping("/members/card-no")
    public ResponseEntity<List<Member>> fetchMemberByMemberShipNo(@RequestParam String no) {
        log.debug("REST request to get Membership : {}", no);
        List<Member> members = memberService.fetchMemberByCardNo(no);
        return ResponseEntity.ok(members);
    }

    @GetMapping("/members/{id}/upgrade-package")
    public ResponseEntity<Member> createUpgradePackageApply(@PathVariable String id, @RequestParam String newPackageId) {
        log.debug("REST request to Change package:{} to package id", id, newPackageId);
        Member members = memberService.requestCreateChangePackageApplication(id, newPackageId, PackageAction.UPGRADE);
        return ResponseEntity.ok(members);
    }

    @GetMapping("/members/{id}/type-change-package")
    public ResponseEntity<Member> createTypeChangePackageApply(@PathVariable String id, @RequestParam String newPackageId) {
        log.debug("REST request to Change package:{} to package id", id, newPackageId);
        Member members = memberService.requestCreateChangePackageApplication(id, newPackageId, PackageAction.TYPECHANGE);
        return ResponseEntity.ok(members);
    }

    @GetMapping("/members/{id}/immediate-family")
    public ResponseEntity<List<Member>> getImmediateFamilyMember(@PathVariable String id) {
        log.debug("REST request to getImmediateFamilyMember:{}.", id);
        return ResponseEntity.ok(memberService.getImmediateFamilyMember(id));
    }

    @GetMapping("/members/fix-membership-id")
    public ResponseEntity<String> fixMemberShipId() {
        List<Member> members = memberRepository.findAll();
        for (Member member : members) {
            List<Membership> memberShips = membershipRepository.findByMemberId(member.getId());
            if( memberShips != null && memberShips.size() > 0 ) {
                member.setMembershipId(memberShips.get(0).getMembershipIdNo());
            }
        }
        memberRepository.saveAll(members);
        return ResponseEntity.ok("Success!!");
    }

    @GetMapping("/memberInfo/{id}")
    public ResponseEntity<MemberInfo> getMemberInfo(@PathVariable String id) {
        log.debug("REST request to get MemberInfo : {}", id);
       /* Member member = memberRepository.findById(id).orElseThrow(() ->  new NotFoundException("Not found member") );
        MemberInfo memberInfo =  new MemberInfo();
        memberInfo.setId(member.getId());
        memberInfo.setUserId(member.getUserId());
        memberInfo.setStatus(member.getStatus());
        memberInfo.setTitle(member.getTitle());
        memberInfo.setGender(member.getGender());
        memberInfo.setGivenName(member.getGivenName());
        memberInfo.setMiddleName(member.getMiddleName());
        memberInfo.setSurName(member.getSurName());
        memberInfo.setEmail(member.getEmail());
        memberInfo.setPicture(member.getPicture());
        memberInfo.setAreaCode(member.getAreaCode());
        memberInfo.setContactNo(member.getContactNo());
        memberInfo.setMobileInHomeCountry(member.getMobileInHomeCountry());
        memberInfo.setMobileInThai(member.getMobileInThai());
        memberInfo.setDateOfBirth(member.getDateOfBirth());
        memberInfo.setCountry(member.getCountry());
        memberInfo.setCountryOfBirth(member.getCountryOfBirth());
        memberInfo.setBloodType(member.getBloodType());
        memberInfo.setReligion(member.getReligion());
        memberInfo.setNationality(member.getNationality());
        memberInfo.setOccupation(member.getOccupation());
        memberInfo.setBusinessTitle(member.getBusinessTitle());
        memberInfo.setCompanyName(member.getCompanyName());
        memberInfo.setNatureOfBusiness(member.getNatureOfBusiness());
        memberInfo.setNatureOfBusinessOther(member.getNatureOfBusinessOther());
        memberInfo.setOccupationOther(member.getOccupationOther());
//        memberInfo.setHobby(Hobby.INTERNET);
        memberInfo.setAllergic(member.getAllergic());
        memberInfo.setPassport(member.getPassport());
        memberInfo.setVisa(member.getVisa());
        memberInfo.setPdpa(member.getPdpa());
//        memberInfo.setAllowContactMe(false);
        memberInfo.setApplicationId(member.getApplicationId());
        memberInfo.setApplicationNo(member.getApplicationNo());
        memberInfo.setAgentId(member.getAgentId());
        memberInfo.setApprovalNo(member.getApprovalNo());
//        memberInfo.setMemberType(MemberType.CORE);
        memberInfo.setMemberCoreId(member.getMemberCoreId());
        memberInfo.setMemberGroupId(member.getMemberGroupId());
//        memberInfo.setMemberStatus(member.getMemberStatus());
        memberInfo.setRemark(member.getRemark());
        memberInfo.setMemberFileApplication(member.getMemberFileApplication());
        memberInfo.setMemberFileOther(member.getMemberFileOther());
        memberInfo.setMemberFilePassport(member.getMemberFilePassport());
        memberInfo.setActiveDate(member.getActiveDate());
        memberInfo.setMemberBlockServicesStatus(member.getMemberBlockServicesStatus());
        memberInfo.setMemberBlockServicesRemark(member.getMemberBlockServicesRemark());
        memberInfo.setMemberNext(member.getMemberNext());
        memberInfo.setMemberBirthCertificate(member.getMemberBirthCertificate());
        memberInfo.setMemberMarriageCertificate(member.getMemberMarriageCertificate());
        memberInfo.setMemberProofPayment(member.getMemberProofPayment());
        memberInfo.setExpiryDate(member.getExpiryDate());
        memberInfo.setImmigrationAttachFile(member.getImmigrationAttachFile());
        memberInfo.setRemarkEdit(member.getRemarkEdit());
        memberInfo.setCommissionPaymentStatus(member.getCommissionPaymentStatus());
        memberInfo.setCovid19Remedy1(member.getCovid19Remedy1());

        Optional<Membership> membership = membershipRepository.findOneByMemberId(member.getId());
        memberInfo.setMembership(membership);

        List<CardPrivilege> cardPrivileges = cardPrivilegeRepository.findAllByCardId(membership.get().getCardId());
        List<Privilege> privileges = new ArrayList<>();
        for (CardPrivilege cardPrivilege : cardPrivileges) {
            privileges.add(cardPrivilege.getPrivilege());
        }
        memberInfo.setPrivileges(privileges);*/

        return ResponseEntity.ok(null);
    }

    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }
}
