package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.ContactTagMaster;
import com.thailandelite.mis.be.repository.ContactTagMasterRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ContactTagMasterResource {

    private final Logger log = LoggerFactory.getLogger(ContactTagMasterResource.class);

    private static final String ENTITY_NAME = "contactTagMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactTagMasterRepository contactTagMasterRepository;

    public ContactTagMasterResource(ContactTagMasterRepository contactTagMasterRepository) {
        this.contactTagMasterRepository = contactTagMasterRepository;
    }


    @PostMapping("/contact-tag-masters")
    public ResponseEntity<ContactTagMaster> createContactTagMaster(@RequestBody ContactTagMaster contactTagMaster) throws URISyntaxException {
        log.debug("REST request to save ContactTagMaster : {}", contactTagMaster);
        if (contactTagMaster.getId() != null) {
            throw new BadRequestAlertException("A new contactTagMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactTagMaster result = contactTagMasterRepository.save(contactTagMaster);
        return ResponseEntity.created(new URI("/api/contact-tag-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/contact-tag-masters")
    public ResponseEntity<ContactTagMaster> updateContactTagMaster(@RequestBody ContactTagMaster contactTagMaster) throws URISyntaxException {
        log.debug("REST request to update ContactTagMaster : {}", contactTagMaster);
        if (contactTagMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactTagMaster result = contactTagMasterRepository.save(contactTagMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, contactTagMaster.getId()))
            .body(result);
    }


    @GetMapping("/contact-tag-masters")
    public List<ContactTagMaster> getAllContactTagMasters() {
        log.debug("REST request to get all ContactTagMasters");
        return contactTagMasterRepository.findAll();
    }


    @GetMapping("/contact-tag-masters/{id}")
    public ResponseEntity<ContactTagMaster> getContactTagMaster(@PathVariable String id) {
        log.debug("REST request to get ContactTagMaster : {}", id);
        Optional<ContactTagMaster> contactTagMaster = contactTagMasterRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(contactTagMaster);
    }


    @DeleteMapping("/contact-tag-masters/{id}")
    public ResponseEntity<Void> deleteContactTagMaster(@PathVariable String id) {
        log.debug("REST request to delete ContactTagMaster : {}", id);
        contactTagMasterRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
