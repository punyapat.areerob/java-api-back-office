package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.RelationshipRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.master.Relationship;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class RelationshipResource {

    private final Logger log = LoggerFactory.getLogger(RelationshipResource.class);

    private static final String ENTITY_NAME = "relationship";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RelationshipRepository relationshipRepository;

    public RelationshipResource(RelationshipRepository relationshipRepository) {
        this.relationshipRepository = relationshipRepository;
    }


    @PostMapping("/relationships")
    public ResponseEntity<Relationship> createRelationship(@RequestBody Relationship relationship) throws URISyntaxException {
        log.debug("REST request to save Relationship : {}", relationship);
        if (relationship.getId() != null) {
            throw new BadRequestAlertException("A new relationship cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Relationship result = relationshipRepository.save(relationship);
        return ResponseEntity.created(new URI("/api/relationships/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/relationships")
    public ResponseEntity<Relationship> updateRelationship(@RequestBody Relationship relationship) throws URISyntaxException {
        log.debug("REST request to update Relationship : {}", relationship);
        if (relationship.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Relationship result = relationshipRepository.save(relationship);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, relationship.getId()))
            .body(result);
    }


    @GetMapping("/relationships")
    public List<Relationship> getAllRelationships() {
        log.debug("REST request to get all Relationships");
        return relationshipRepository.findAll();
    }


    @GetMapping("/relationships/{id}")
    public ResponseEntity<Relationship> getRelationship(@PathVariable String id) {
        log.debug("REST request to get Relationship : {}", id);
        Optional<Relationship> relationship = relationshipRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(relationship);
    }


    @DeleteMapping("/relationships/{id}")
    public ResponseEntity<Void> deleteRelationship(@PathVariable String id) {
        log.debug("REST request to delete Relationship : {}", id);
        relationshipRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
