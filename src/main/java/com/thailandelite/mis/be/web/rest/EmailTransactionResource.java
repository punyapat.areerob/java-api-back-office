package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.domain.SendEmailRequest;
import com.thailandelite.mis.be.domain.enumeration.EmailTransactionStatus;
import com.thailandelite.mis.be.repository.EmailTransactionRepository;
import com.thailandelite.mis.be.service.EmailService;
import com.thailandelite.mis.be.service.NotificationService;
import com.thailandelite.mis.be.service.helper.FormBuilderService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.easycm.framework.form.generator.FormlyFieldConfig;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class EmailTransactionResource {

    private final Logger log = LoggerFactory.getLogger(EmailTransactionResource.class);

    private static final String ENTITY_NAME = "emailTransaction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmailTransactionRepository emailTransactionRepository;
    private final EmailService emailService;
    private final NotificationService notificationService;

    private final FormBuilderService formBuilderService;

    @PostMapping("/email-transactions")
    public ResponseEntity<EmailTransaction> createEmailTransaction(@RequestBody EmailTransaction emailTransaction) throws URISyntaxException {
        log.debug("REST request to save EmailTransaction : {}", emailTransaction);
        if (emailTransaction.getId() != null) {
            throw new BadRequestAlertException("A new emailTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmailTransaction result = emailTransactionRepository.save(emailTransaction);
        return ResponseEntity.created(new URI("/api/email-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/email-transactions")
    public ResponseEntity<EmailTransaction> updateEmailTransaction(@RequestBody EmailTransaction emailTransaction) throws URISyntaxException {
        log.debug("REST request to update EmailTransaction : {}", emailTransaction);
        if (emailTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EmailTransaction result = emailTransactionRepository.save(emailTransaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, emailTransaction.getId()))
            .body(result);
    }


    @GetMapping("/email-transactions")
    public List<EmailTransaction> getAllEmailTransactions(String to, LocalDate date, EmailTransactionStatus status, Pageable page) {
        log.debug("REST request to get all EmailTransactions");
        return emailTransactionRepository.findByProperties(to, date, status, page);
    }


    @GetMapping("/email-transactions/{id}")
    public ResponseEntity<EmailTransaction> getEmailTransaction(@PathVariable String id) {
        log.debug("REST request to get EmailTransaction : {}", id);
        Optional<EmailTransaction> emailTransaction = emailTransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(emailTransaction);
    }


    @DeleteMapping("/email-transactions/{id}")
    public ResponseEntity<Void> deleteEmailTransaction(@PathVariable String id) {
        log.debug("REST request to delete EmailTransaction : {}", id);
        emailTransactionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PostMapping("/email-transactions/send")
    public ResponseEntity<SendEmailRequest> sendEmailTemplate(@RequestBody SendEmailRequest emailRequest) throws IOException {
        log.debug("REST request to save SendEmailRequest : {}", emailRequest);
        EmailTransaction transaction = emailService.sendEmailTemplate(emailRequest);

        SendStatus sendStatus = new SendStatus();
        sendStatus.setId(transaction.getId());
        sendStatus.setStatus(transaction.getStatus());
        sendStatus.setError(transaction.getError());

        return ResponseEntity.ok(emailRequest);
    }


    @PostMapping("/email-transactions/test")
    public ResponseEntity<SendEmailRequest> testSendEmailTemplate(int test) throws IOException {
        if(test == 1){
            notificationService.sendAccountRegistration("sedtawut@yahoo.com", "Sedtawut", "Aun", "Arunsri");
        }else if(test == 2){
            notificationService.sendSubmitApplicationForm("sedtawut@yahoo.com", "Sedtawut", "Aun", "Arunsri");
        }
        return ResponseEntity.ok().build();
    }


    @GetMapping("/email-transactions/form")
    public List<FormlyFieldConfig> getForm() throws ClassNotFoundException {
        log.debug("REST request to get all EmailTemplates");
        return formBuilderService.build(EmailTransaction.class);
    }

    @Data
    private static class SendStatus {
        String id;
        EmailTransactionStatus status;
        String error;
    }
}
