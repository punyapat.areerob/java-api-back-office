package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.service.AgentLogService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentLog;
import com.thailandelite.mis.be.repository.AgentLogRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AgentLog}.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AgentLogResource {

    private final Logger log = LoggerFactory.getLogger(AgentLogResource.class);

    private static final String ENTITY_NAME = "agentLog";
    private static final String flowDefinitionKey = "agentLog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentLogRepository agentLogRepository;
    private final AgentLogService agentLogService;


    @PostMapping("/agent-logs")
    public ResponseEntity<AgentLog> createAgentLog(String agentId, String deskNo, String status) throws URISyntaxException {
        log.debug("REST request to save AgentLog : {}", agentId);

        AgentLog result = agentLogService.newAgentLog(agentId, deskNo, status);
        return ResponseEntity.created(new URI("/api/agent-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-logs")
    public ResponseEntity<AgentLog> updateAgentLog(@RequestBody AgentLog agentLog) throws URISyntaxException {
        log.debug("REST request to update AgentLog : {}", agentLog);
        if (agentLog.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentLog result = agentLogRepository.save(agentLog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentLog.getId()))
            .body(result);
    }

    /**
     * {@code GET  /agent-logs} : get all the agentLogs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentLogs in body.
     */
    @GetMapping("/agent-logs")
    public ResponseEntity<FlowResponse<Page<AgentLog>>> getAllAgentLogs(Pageable pageable) {
        log.debug("REST request to get all AgentLogs");
        Page<AgentLog> agentLogs = agentLogRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), agentLogs);

        return new ResponseEntity<>(new FlowResponse<Page<AgentLog>>(flowDefinitionKey, 0, agentLogs), headers, HttpStatus.OK);
    }

    @GetMapping("/agent-logs/desk-no/{deskNo}")
    public ResponseEntity<FlowResponse<Page<AgentLog>>> getAllAgentLogsByDesk(@PathVariable String deskNo, Pageable pageable) {
        log.debug("REST request to get all AgentLogs");
        Page<AgentLog> agentLogs = agentLogRepository.findByDesk(deskNo, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), agentLogs);

        return new ResponseEntity<>(new FlowResponse<Page<AgentLog>>(flowDefinitionKey, 0, agentLogs), headers, HttpStatus.OK);
    }

    /**
     * {@code GET  /agent-logs/:id} : get the "id" agentLog.
     *
     * @param id the id of the agentLog to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentLog, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-logs/{id}")
    public ResponseEntity<AgentLog> getAgentLog(@PathVariable String id) {
        log.debug("REST request to get AgentLog : {}", id);
        Optional<AgentLog> agentLog = agentLogRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentLog);
    }

    /**
     * {@code DELETE  /agent-logs/:id} : delete the "id" agentLog.
     *
     * @param id the id of the agentLog to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-logs/{id}")
    public ResponseEntity<Void> deleteAgentLog(@PathVariable String id) {
        log.debug("REST request to delete AgentLog : {}", id);
        agentLogRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
