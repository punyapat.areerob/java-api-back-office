package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CaseStepInstance;
import com.thailandelite.mis.be.repository.CaseStepInstanceRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CaseStepInstanceResource {

    private final Logger log = LoggerFactory.getLogger(CaseStepInstanceResource.class);

    private static final String ENTITY_NAME = "caseStepInstance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseStepInstanceRepository caseStepInstanceRepository;

    public CaseStepInstanceResource(CaseStepInstanceRepository caseStepInstanceRepository) {
        this.caseStepInstanceRepository = caseStepInstanceRepository;
    }


    @PostMapping("/case-step-instances")
    public ResponseEntity<CaseStepInstance> createCaseStepInstance(@RequestBody CaseStepInstance caseStepInstance) throws URISyntaxException {
        log.debug("REST request to save CaseStepInstance : {}", caseStepInstance);
        if (caseStepInstance.getId() != null) {
            throw new BadRequestAlertException("A new caseStepInstance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseStepInstance result = caseStepInstanceRepository.save(caseStepInstance);
        return ResponseEntity.created(new URI("/api/case-step-instances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/case-step-instances")
    public ResponseEntity<CaseStepInstance> updateCaseStepInstance(@RequestBody CaseStepInstance caseStepInstance) throws URISyntaxException {
        log.debug("REST request to update CaseStepInstance : {}", caseStepInstance);
        if (caseStepInstance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseStepInstance result = caseStepInstanceRepository.save(caseStepInstance);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseStepInstance.getId()))
            .body(result);
    }


    @GetMapping("/case-step-instances")
    public List<CaseStepInstance> getAllCaseStepInstances() {
        log.debug("REST request to get all CaseStepInstances");
        return caseStepInstanceRepository.findAll();
    }


    @GetMapping("/case-step-instances/{id}")
    public ResponseEntity<CaseStepInstance> getCaseStepInstance(@PathVariable String id) {
        log.debug("REST request to get CaseStepInstance : {}", id);
        Optional<CaseStepInstance> caseStepInstance = caseStepInstanceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseStepInstance);
    }


    @DeleteMapping("/case-step-instances/{id}")
    public ResponseEntity<Void> deleteCaseStepInstance(@PathVariable String id) {
        log.debug("REST request to delete CaseStepInstance : {}", id);
        caseStepInstanceRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
