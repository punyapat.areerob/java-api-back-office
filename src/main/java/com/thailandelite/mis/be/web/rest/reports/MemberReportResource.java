package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.MemberReportService;
import com.thailandelite.mis.model.FlowResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
public class MemberReportResource {

    private final MemberReportService reportService;

    public MemberReportResource(MemberReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/reports/member")
    public ResponseEntity<FlowResponse<Page<MemberReportResponse>>> getMemberAmount(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getActiveMemberReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/member/excel")
    public byte[] getMemberAmountExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getActiveMemberExcel(searchRequest, pageable);
    }

    @GetMapping("/reports/member-package")
    public ResponseEntity<FlowResponse<Page<CountReportResponse>>> getMemberPackageAmount(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getMemberPackageReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/member-package/excel")
    public byte[] getMemberPackageAmountExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getMemberPackageExcel(searchRequest, pageable);
    }

    @GetMapping("/reports/member-nationality")
    public ResponseEntity<FlowResponse<Page<CountReportResponse>>> getMemberNationalityAmount(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getMemberNationalityReport(searchRequest, pageable) ));
    }
    @GetMapping("/reports/member-nationality/excel")
    public byte[] getMemberNationalityAmountExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getMemberNationalityExcel(searchRequest, pageable);
    }

}
