package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.JobApplication;
import com.thailandelite.mis.model.domain.JobApplicationConfig;
import com.thailandelite.mis.be.repository.JobApplicationConfigRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class JobApplicationConfigResource {

    private final Logger log = LoggerFactory.getLogger(JobApplicationConfigResource.class);

    private static final String ENTITY_NAME = "jobApplication";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JobApplicationConfigRepository jobApplicationConfigRepository;


    @PostMapping("/job-application0-configs")
    public ResponseEntity<JobApplicationConfig> createJobApplication(@RequestBody JobApplicationConfig jobApplication) throws URISyntaxException {
        log.debug("REST request to save JobApplication : {}", jobApplication);
        if (jobApplication.getId() != null) {
            throw new BadRequestAlertException("A new jobApplication cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JobApplicationConfig result = jobApplicationConfigRepository.save(jobApplication);
        return ResponseEntity.created(new URI("/api/job-application0-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/job-application0-configs")
    public ResponseEntity<JobApplicationConfig> updateJobApplication(@RequestBody JobApplicationConfig jobApplication) throws URISyntaxException {
        log.debug("REST request to update JobApplication : {}", jobApplication);
        if (jobApplication.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JobApplicationConfig result = jobApplicationConfigRepository.save(jobApplication);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jobApplication.getId()))
            .body(result);
    }


    @GetMapping("/job-application0-configs")
    public List<JobApplicationConfig> getAllJobApplications() {
        log.debug("REST request to get all JobApplications");
        return jobApplicationConfigRepository.findAll();
    }


    @GetMapping("/job-application0-configs/{id}")
    public ResponseEntity<JobApplicationConfig> getJobApplication(@PathVariable String id) {
        log.debug("REST request to get JobApplication : {}", id);
        Optional<JobApplicationConfig> jobApplication = jobApplicationConfigRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jobApplication);
    }


    @DeleteMapping("/job-application0-configs/{id}")
    public ResponseEntity<Void> deleteJobApplication(@PathVariable String id) {
        log.debug("REST request to delete JobApplication : {}", id);
        jobApplicationConfigRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
