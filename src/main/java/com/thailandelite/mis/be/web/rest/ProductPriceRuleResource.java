package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.ProductPriceRule;
import com.thailandelite.mis.be.repository.ProductPriceRuleRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ProductPriceRuleResource {

    private final Logger log = LoggerFactory.getLogger(ProductPriceRuleResource.class);

    private static final String ENTITY_NAME = "productPriceRule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductPriceRuleRepository productPriceRuleRepository;

    public ProductPriceRuleResource(ProductPriceRuleRepository productPriceRuleRepository) {
        this.productPriceRuleRepository = productPriceRuleRepository;
    }


    @PostMapping("/product-price-rules")
    public ResponseEntity<ProductPriceRule> createProductPriceRule(@RequestBody ProductPriceRule productPriceRule) throws URISyntaxException {
        log.debug("REST request to save ProductPriceRule : {}", productPriceRule);
        if (productPriceRule.getId() != null) {
            throw new BadRequestAlertException("A new productPriceRule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductPriceRule result = productPriceRuleRepository.save(productPriceRule);
        return ResponseEntity.created(new URI("/api/product-price-rules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/product-price-rules")
    public ResponseEntity<ProductPriceRule> updateProductPriceRule(@RequestBody ProductPriceRule productPriceRule) throws URISyntaxException {
        log.debug("REST request to update ProductPriceRule : {}", productPriceRule);
        if (productPriceRule.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductPriceRule result = productPriceRuleRepository.save(productPriceRule);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productPriceRule.getId()))
            .body(result);
    }


    @GetMapping("/product-price-rules")
    public List<ProductPriceRule> getAllProductPriceRules() {
        log.debug("REST request to get all ProductPriceRules");
        return productPriceRuleRepository.findAll();
    }


    @GetMapping("/product-price-rules/{id}")
    public ResponseEntity<ProductPriceRule> getProductPriceRule(@PathVariable String id) {
        log.debug("REST request to get ProductPriceRule : {}", id);
        Optional<ProductPriceRule> productPriceRule = productPriceRuleRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productPriceRule);
    }


    @DeleteMapping("/product-price-rules/{id}")
    public ResponseEntity<Void> deleteProductPriceRule(@PathVariable String id) {
        log.debug("REST request to delete ProductPriceRule : {}", id);
        productPriceRuleRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
