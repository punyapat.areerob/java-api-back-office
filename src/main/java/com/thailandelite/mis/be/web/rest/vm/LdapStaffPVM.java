package com.thailandelite.mis.be.web.rest.vm;

import lombok.Data;

@Data
public class LdapStaffPVM {
    private String firstName;
    private String LastName;
    private String department;
}
