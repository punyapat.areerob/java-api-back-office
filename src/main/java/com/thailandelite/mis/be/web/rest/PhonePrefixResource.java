package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.PhonePrefix;
import com.thailandelite.mis.be.repository.PhonePrefixRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PhonePrefixResource {

    private final Logger log = LoggerFactory.getLogger(PhonePrefixResource.class);

    private static final String ENTITY_NAME = "phonePrefix";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhonePrefixRepository phonePrefixRepository;

    public PhonePrefixResource(PhonePrefixRepository phonePrefixRepository) {
        this.phonePrefixRepository = phonePrefixRepository;
    }


    @PostMapping("/phone-prefixes")
    public ResponseEntity<PhonePrefix> createPhonePrefix(@RequestBody PhonePrefix phonePrefix) throws URISyntaxException {
        log.debug("REST request to save PhonePrefix : {}", phonePrefix);
        if (phonePrefix.getId() != null) {
            throw new BadRequestAlertException("A new phonePrefix cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PhonePrefix result = phonePrefixRepository.save(phonePrefix);
        return ResponseEntity.created(new URI("/api/phone-prefixes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/phone-prefixes")
    public ResponseEntity<PhonePrefix> updatePhonePrefix(@RequestBody PhonePrefix phonePrefix) throws URISyntaxException {
        log.debug("REST request to update PhonePrefix : {}", phonePrefix);
        if (phonePrefix.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PhonePrefix result = phonePrefixRepository.save(phonePrefix);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, phonePrefix.getId()))
            .body(result);
    }


    @GetMapping("/phone-prefixes")
    public List<PhonePrefix> getAllPhonePrefixes() {
        log.debug("REST request to get all PhonePrefixes");
        return phonePrefixRepository.findAll();
    }


    @GetMapping("/phone-prefixes/{id}")
    public ResponseEntity<PhonePrefix> getPhonePrefix(@PathVariable String id) {
        log.debug("REST request to get PhonePrefix : {}", id);
        Optional<PhonePrefix> phonePrefix = phonePrefixRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(phonePrefix);
    }


    @DeleteMapping("/phone-prefixes/{id}")
    public ResponseEntity<Void> deletePhonePrefix(@PathVariable String id) {
        log.debug("REST request to delete PhonePrefix : {}", id);
        phonePrefixRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
