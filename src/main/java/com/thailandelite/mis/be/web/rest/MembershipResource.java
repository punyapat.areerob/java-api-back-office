package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.MembershipService;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.be.service.MemberPrivilegeService;
import com.thailandelite.mis.model.domain.Membership;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.MembershipRepository;
import com.thailandelite.mis.be.service.HandlerException;
import com.thailandelite.mis.be.service.MemberService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MembershipResource {

    private final Logger log = LoggerFactory.getLogger(MembershipResource.class);

    private static final String ENTITY_NAME = "membership";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MembershipRepository membershipRepository;
    private final MemberService memberService;
    private final MemberPrivilegeService memberPrivilegeService;
    private final MembershipService membershipService;


    @PostMapping("/memberships")
    public ResponseEntity<Membership> createMembership(@RequestBody Membership membership) throws URISyntaxException {
        log.debug("REST request to save Membership : {}", membership);
        if (membership.getId() != null) {
            throw new BadRequestAlertException("A new membership cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Membership result = membershipRepository.save(membership);
        return ResponseEntity.created(new URI("/api/memberships/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/memberships")
    public ResponseEntity<Membership> updateMembership(@RequestBody Membership membership) throws URISyntaxException {
        log.debug("REST request to update Membership : {}", membership);
        if (membership.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Membership result = membershipRepository.save(membership);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, membership.getId()))
            .body(result);
    }

    @PutMapping("/memberships/{membershipId}/activateDate")
    public ResponseEntity<Membership> updateActivateDate(@PathVariable String membershipId, @RequestParam ZonedDateTime activateDate) throws URISyntaxException, HandlerException {
        log.debug("REST request to updateActivateDate : {}, {}", membershipId, activateDate);
        Membership membership = membershipRepository.findById(membershipId).orElseThrow(()->new NotFoundException("Not found MemberShip"));
        return ResponseEntity.ok().body(memberService.firstActivateMember(membership.getMemberId(), membershipId, activateDate));
    }


    @GetMapping("/memberships/generateIncome")
    public byte[] getAllMembershipsIncome() {
        log.debug("REST request to get a page of Memberships");

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            memberService.generateIncomeSheet().write(outputStream);
            return  outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @GetMapping("/memberships")
    public ResponseEntity<List<Membership>> getAllMemberships(Pageable pageable) {
        log.debug("REST request to get a page of Memberships");
        Page<Membership> page = membershipRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/memberships/no/{id}")
    public ResponseEntity<Membership> getMembershipNo(@RequestParam String id) {
        log.debug("REST request to get Membership : {}", id);
        Optional<Membership> membership = membershipRepository.findByMembershipNo(id);
        return ResponseUtil.wrapOrNotFound(membership);
    }

    @GetMapping("/memberships/{id}")
    public ResponseEntity<Membership> getMembership(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        Optional<Membership> membership = membershipRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(membership);
    }

    @GetMapping("/memberships/{id}/quota")
    public List<MemberPrivilegeQuota> getMembershipQuota(@PathVariable String id) {
        log.debug("REST request to get Membership : {}", id);
        return memberPrivilegeService.getPrivilegeQuota(id);
    }

    @GetMapping("/memberships/{id}/upgradeCard/{cardId}")
    public ResponseEntity<Membership> getTestUpgradeMembership(@PathVariable String id, @PathVariable String cardId) {
        log.debug("REST request to get test upgrade Membership : {} to Card : {}", id, cardId);
        Membership upgradeMembership = membershipService.upgradeMembership(id, cardId);
        return ResponseEntity.ok(upgradeMembership);
    }

    @GetMapping("/memberships/{id}/typeChangeCard/{cardId}")
    public ResponseEntity<Membership> getTestTypeChangeMembership(@PathVariable String id, @PathVariable String cardId) {
        log.debug("REST request to get test type change Membership : {} to Card : {}", id, cardId);
        Membership typeChangeMembership = membershipService.typeChangeMembership(id, cardId);
        return ResponseEntity.ok(typeChangeMembership);
    }

    @DeleteMapping("/memberships/{id}")
    public ResponseEntity<Void> deleteMembership(@PathVariable String id) {
        log.debug("REST request to delete Membership : {}", id);
        membershipRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PutMapping("/memberships/{membershipId}/welcomeEmailMessage")
    public ResponseEntity<Membership> updateWelcomeEmailMessage(@PathVariable String membershipId, @RequestParam String welcomeEmailMessage) throws URISyntaxException, HandlerException {
        log.debug("REST request to welcomeEmailMessage : {}, {}", membershipId, welcomeEmailMessage);
        return ResponseEntity.ok().body(memberService.updateEmailWelcomePackage(membershipId, welcomeEmailMessage));
    }

    @PutMapping("/memberships/{membershipId}/uploadTemporaryCard")
    public ResponseEntity<Membership> uploadTemporaryCard(@PathVariable String membershipId, @RequestParam("file") MultipartFile file) throws URISyntaxException, HandlerException, UploadFailException {
        log.debug("REST request to uploadTemporaryCard : {}", membershipId);
        return ResponseEntity.ok().body(memberService.uploadTemporaryMembershipCard(membershipId, file));
    }

    @PutMapping("/memberships/{membershipId}/uploadShippingReceiptUrl")
    public ResponseEntity<Membership> uploadShippingReceiptUrl(@PathVariable String membershipId, @RequestParam("file") MultipartFile file) throws URISyntaxException, HandlerException, UploadFailException {
        log.debug("REST request to uploadShippingReceiptUrl : {}", membershipId);
        return ResponseEntity.ok().body(memberService.uploadShippingReceiptUrl(membershipId, file));
    }

    @PutMapping("/memberships/{membershipId}/uploadApplicationReportUrl")
    public ResponseEntity<Membership> uploadApplicationReportUrl(@PathVariable String membershipId, @RequestParam("file") MultipartFile file) throws URISyntaxException, HandlerException, UploadFailException {
        log.debug("REST request to uploadApplicationReportUrl : {}, {}", membershipId);
        return ResponseEntity.ok().body(memberService.uploadApplicationReportUrl(membershipId, file));
    }




}
