package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentStatusGroup;
import com.thailandelite.mis.be.repository.AgentStatusGroupRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AgentStatusGroup}.
 */
@RestController
@RequestMapping("/api")
public class AgentStatusGroupResource {

    private final Logger log = LoggerFactory.getLogger(AgentStatusGroupResource.class);

    private static final String ENTITY_NAME = "agentStatusGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentStatusGroupRepository agentStatusGroupRepository;

    public AgentStatusGroupResource(AgentStatusGroupRepository agentStatusGroupRepository) {
        this.agentStatusGroupRepository = agentStatusGroupRepository;
    }

    /**
     * {@code POST  /agent-status-groups} : Create a new agentStatusGroup.
     *
     * @param agentStatusGroup the agentStatusGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentStatusGroup, or with status {@code 400 (Bad Request)} if the agentStatusGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-status-groups")
    public ResponseEntity<AgentStatusGroup> createAgentStatusGroup(@RequestBody AgentStatusGroup agentStatusGroup) throws URISyntaxException {
        log.debug("REST request to save AgentStatusGroup : {}", agentStatusGroup);
        if (agentStatusGroup.getId() != null) {
            throw new BadRequestAlertException("A new agentStatusGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentStatusGroup result = agentStatusGroupRepository.save(agentStatusGroup);
        return ResponseEntity.created(new URI("/api/agent-status-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-status-groups} : Updates an existing agentStatusGroup.
     *
     * @param agentStatusGroup the agentStatusGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentStatusGroup,
     * or with status {@code 400 (Bad Request)} if the agentStatusGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentStatusGroup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-status-groups")
    public ResponseEntity<AgentStatusGroup> updateAgentStatusGroup(@RequestBody AgentStatusGroup agentStatusGroup) throws URISyntaxException {
        log.debug("REST request to update AgentStatusGroup : {}", agentStatusGroup);
        if (agentStatusGroup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentStatusGroup result = agentStatusGroupRepository.save(agentStatusGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentStatusGroup.getId()))
            .body(result);
    }

    /**
     * {@code GET  /agent-status-groups} : get all the agentStatusGroups.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentStatusGroups in body.
     */
    @GetMapping("/agent-status-groups")
    public List<AgentStatusGroup> getAllAgentStatusGroups() {
        log.debug("REST request to get all AgentStatusGroups");
        return agentStatusGroupRepository.findAll();
    }

    /**
     * {@code GET  /agent-status-groups/:id} : get the "id" agentStatusGroup.
     *
     * @param id the id of the agentStatusGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentStatusGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-status-groups/{id}")
    public ResponseEntity<AgentStatusGroup> getAgentStatusGroup(@PathVariable String id) {
        log.debug("REST request to get AgentStatusGroup : {}", id);
        Optional<AgentStatusGroup> agentStatusGroup = agentStatusGroupRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentStatusGroup);
    }

    /**
     * {@code DELETE  /agent-status-groups/:id} : delete the "id" agentStatusGroup.
     *
     * @param id the id of the agentStatusGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-status-groups/{id}")
    public ResponseEntity<Void> deleteAgentStatusGroup(@PathVariable String id) {
        log.debug("REST request to delete AgentStatusGroup : {}", id);
        agentStatusGroupRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
