package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.common.BlankCamundaTask;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.repository.CardRepository;
import com.thailandelite.mis.be.repository.ProspectRepository;
import com.thailandelite.mis.be.service.MemberService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.dto.TaskWrapper;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.Prospect;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ProspectResource {

    private final Logger log = LoggerFactory.getLogger(ProspectResource.class);

    private static final String ENTITY_NAME = "Prospect";
    private static final String flowDefinitionKey = "prospect";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MemberService memberService;
    private final CaseActivityRepository caseActivityRepository;
    private final ProspectRepository prospectRepository;

    private enum PROSPECTACTION {NONE}

    @PostMapping("/prospect-data")
    public ResponseEntity<Prospect> createProspectData(@RequestBody Prospect prospectData) throws URISyntaxException {
        log.debug("REST request to save ProspectData : {}", prospectData);
        if (prospectData.getId() != null) {
            throw new BadRequestAlertException("A new prospectData cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Prospect result = memberService.createProspect(prospectData);

        caseActivityRepository.save(new CaseActivity("","System", prospectData.getId() ,"Done","Done"));

        return ResponseEntity.created(new URI("/api/prospect-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/prospect-data")
    public ResponseEntity<Prospect> updateProspectData(@RequestBody Prospect prospectData) throws URISyntaxException {
        log.debug("REST request to update ProspectData : {}", prospectData);
        if (prospectData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Prospect result = prospectRepository.save(prospectData);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, prospectData.getId()))
            .body(result);
    }

    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }



    @GetMapping("/prospect-data")
    public ResponseEntity<FlowResponse<Page<Prospect>>> getAllProspectData(Pageable pageable
                                                                           ) {
        Page<Prospect> prospects = prospectRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), prospects);

        return new ResponseEntity<>(new FlowResponse<Page<Prospect>>(flowDefinitionKey, 0,prospects), headers, HttpStatus.OK);
    }


    @GetMapping("/prospect-data/{id}")
    public ResponseEntity<TaskWrapper<Prospect>> getProspectData(@PathVariable String id) {
        log.debug("REST request to get ProspectData : {}", id);

        Optional<TaskWrapper<Prospect>> prospectData = Optional.of(new TaskWrapper<Prospect>(BlankCamundaTask.getBlankCamundaTask(),
            PROSPECTACTION.values()
        ,prospectRepository.findById(id).get()));
        return ResponseUtil.wrapOrNotFound(prospectData);
    }


    @DeleteMapping("/prospect-data/{id}")
    public ResponseEntity<Void> deleteProspectData(@PathVariable String id) {
        log.debug("REST request to delete ProspectData : {}", id);
        prospectRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
