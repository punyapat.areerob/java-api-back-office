package com.thailandelite.mis.be.web.rest;

import camundajar.impl.com.google.gson.Gson;
import camundajar.impl.com.google.gson.reflect.TypeToken;
import com.thailandelite.mis.be.domain.*;
import com.thailandelite.mis.be.domain.enumeration.AgentRegisterStatus;
import com.thailandelite.mis.be.domain.enumeration.AgentStatus;
import com.thailandelite.mis.be.domain.enumeration.AgentType;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.migrate.*;
import com.thailandelite.mis.model.domain.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MigrateResource {
    private static final String ENTITY_NAME = "member";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendorRepository vendorRepository;
    private final ShopRepository shopRepository;
//    private final ServiceRepository serviceRepository;
    private final ProductRepository productRepository;
    private final ContactTagMasterRepository contactTagMasterRepository;
    private final SubDistrictRepository subDistrictRepository;
    private final DistrictRepository districtRepository;
    private final ProvinceRepository provinceRepository;


    private void setAuditingEntity(MASDB row, AbstractAuditingEntity vendor) {
        vendor.setCreatedBy(row.getCREATE_USER());
        if(row.getCREATE_DATE() != null)
            vendor.setCreatedDate(Instant.parse(row.getCREATE_DATE()));
        vendor.setCreatedUserType(row.getCREATE_USER_TYPE());

        if(row.getLAST_DATE() != null)
            vendor.setLastModifiedDate(Instant.parse(row.getLAST_DATE()));
        vendor.setLastUserType(row.getLAST_USER_TYPE());

        vendor.setLastModifiedBy(row.getLAST_USER());
        vendor.setRecordStatus(row.getRECORD_STATUS());
    }

    @PostMapping("/migrate/master/vendor_cotact")
    public ResponseEntity<List<ContactTagMaster>> migrateMasterVendorContact(String fileName) throws URISyntaxException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/dodge/Documents/MIS-Migrate/"+fileName));
        MASTERVendorContactTable table = new Gson().fromJson(br, MASTERVendorContactTable.class);
        List<ContactTagMaster> contactTagMasters  = new ArrayList<>();
        log.debug("Vendor is : {} size", table.getRows());
        for (MASTERVendorContact row : table.getRows()) {
            log.debug("row: {}", row.getCONTACT_TYPE_NAME_EN());
            ContactTagMaster contactTagMaster = new ContactTagMaster();

            contactTagMaster.setId(String.valueOf(Math.round(row.getCONTACT_TYPE_ID())));
            contactTagMaster.setNameEn(row.getCONTACT_TYPE_NAME_EN());
            contactTagMaster.setNameTh(row.getCONTACT_TYPE_NAME_TH());
            contactTagMaster.setRemark(row.getREMARK());
            contactTagMaster.setActive("N".equalsIgnoreCase(row.getRECORD_STATUS()) ? true : false);

            contactTagMaster.setCreatedBy(row.getCREATE_USER());
            if(row.getCREATE_DATE() != null)
                contactTagMaster.setCreatedDate(Instant.parse(row.getCREATE_DATE()));
            contactTagMaster.setCreatedUserType(row.getCREATE_USER_TYPE());

            contactTagMaster.setLastModifiedBy(row.getLAST_USER());
            if(row.getLAST_DATE() != null)
                contactTagMaster.setLastModifiedDate(Instant.parse(row.getLAST_DATE()));
            contactTagMaster.setLastUserType(row.getLAST_USER_TYPE());

            contactTagMaster.setRecordStatus(row.getRECORD_STATUS());

            contactTagMasters.add(contactTagMaster);
        }
        contactTagMasterRepository.saveAll(contactTagMasters);
        return ResponseEntity.ok().body(contactTagMasters);
    }

    @PostMapping("/migrate/product")
    public ResponseEntity<List<Product>> migrateProduct() throws URISyntaxException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/dodge/Documents/MIS-Migrate/MAS_PRODUCT_VENDER.json"));
        MASProductVendorTable vendorProductTable = new Gson().fromJson(br, MASProductVendorTable.class);
        BufferedReader br2 = new BufferedReader(new FileReader("/Users/dodge/Documents/MIS-Migrate/MAS_PRODUCT.json"));
        MASProductTable productTable = new Gson().fromJson(br2, MASProductTable.class);
        BufferedReader br3 = new BufferedReader(new FileReader("/Users/dodge/Documents/MIS-Migrate/MAS_VENDER.json"));
        MASVendorTable vendorTable = new Gson().fromJson(br3, MASVendorTable.class);

        List<Product> products = new ArrayList<>();
        log.debug("Vendor is : {} size", vendorProductTable.getRows().size());
        for (MASProductVendor row : vendorProductTable.getRows()) {
            String vendorId = String.valueOf(Math.round(row.getVENDER_ID()));
            String shopId = String.valueOf(Math.round(row.getPRODUCT_VENDER_ID()));
            String productId = String.valueOf(Math.round(row.getPRODUCT_ID()));
            MASVendor vendor = this.getMasVendor(vendorId, vendorTable.getRows());
            if(vendor != null) {
                MASProduct product = getMasProduct(productId, productTable.getRows());
                Product copyItem = new Product();
                copyItem.setId(shopId+"_"+productId);
                copyItem.setShopId(shopId);
                copyItem.setVendorId(vendorId);
//                copyItem.setProductCode(row.getPRODUCT_CODE());

                String nameEn = row.getPRODUCT_NAME_EN() != null ? row.getPRODUCT_NAME_EN() : product.getPRODUCT_NAME();
                String nameTh = row.getPRODUCT_NAME_TH() != null ? row.getPRODUCT_NAME_TH() : product.getPRODUCT_NAME();
                copyItem.setNameEn(nameEn);
                copyItem.setNameTh(nameTh);

                String remark = row.getREMARK() != null ? row.getREMARK() : product.getREMARK();
                copyItem.setRemark(remark);
                copyItem.setDescription(row.getDESCRIPTION());

//                copyItem.setPolicy(vendor.getPOLICY());
//                copyItem.setBookInAdvanceHours(vendor.getVENDER_RESERVATION_POLICY_HOUR());
//                copyItem.setCancelInAdvanceHours(vendor.getVENDER_CANCELLATION_POLICY_HOUR());
//                copyItem.setProductStatus("T".equalsIgnoreCase(row.getACTIVE_STATUS()) ? true : false);


                setAuditingEntity(row, copyItem);

                products.add(copyItem);
            }
        }
        productRepository.saveAll(products);
        return ResponseEntity.ok().body(products);
    }








































































































    @PostMapping("/migrate/product/map/services")
    public ResponseEntity<List<Product>> migrateProductServices(String fileName) throws URISyntaxException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/dodge/Documents/MIS-Migrate/"+fileName));
        List<MASProductServices> productServices = new Gson().fromJson(br, new TypeToken<List<MASProductServices>>(){}.getType());
        for (MASProductServices productService : productServices) {
            String oldProductId = String.valueOf(Math.round(productService.getPRODUCT_ID()));
            String oldServiceId = String.valueOf(Math.round(productService.getSERVICES_ID()));
            List<Product> products = productRepository.findByProductId("_"+oldProductId);
            for (Product product : products) {
//                Optional<ProductService> service = serviceRepository.findById(oldServiceId);
//                if(service.isPresent()){
////                    product.setServiceId(oldServiceId);
////                    product.setServiceName(service.get().getName());
////                    product.setServiceCode(service.get().getServiceCode());
////                    product.setServiceTypeDes(service.get().getServiceTypeDes());
//                }
            }
            productRepository.saveAll(products);
        }





















        return null;

    }


    private MASProduct getMasProduct(String id, List<MASProduct> products){
        for (MASProduct product : products) {
            if(id.equalsIgnoreCase(String.valueOf(Math.round(product.getPRODUCT_ID()))))
                return product;
        }
        return  null;
    }

    private MASVendor getMasVendor(String id, List<MASVendor> vendors){
        for (MASVendor vendor : vendors) {
            if(id.equalsIgnoreCase(String.valueOf(Math.round(vendor.getVENDER_ID()))))
                return vendor;
        }
        return  null;
    }

    private Vendor.Status getVendorStatus(String status){
        if(Vendor.Status.WAITING.name().equalsIgnoreCase(status)){
            return Vendor.Status.WAITING;
        }else if(Vendor.Status.APPROVE.name().equalsIgnoreCase(status)){
            return Vendor.Status.APPROVE;
        }else{
            return Vendor.Status.DRAFT;
        }
    }

    private AgentType getAgentType(String type){
        if("1".equalsIgnoreCase(type)){
            return AgentType.SALE_AGENT;
        }else if("2".equalsIgnoreCase(type)){
            return AgentType.INDIVIDUAL_AGENT;
        }else if("4".equalsIgnoreCase(type)){
            return AgentType.GSSA;
        }else if("5".equalsIgnoreCase(type)){
            return AgentType.GSSA_GOBAL_PARTNERSHIP;
        }else if("6".equalsIgnoreCase(type)){
            return AgentType.CONCESSIONAIRE;
        }else if("7".equalsIgnoreCase(type)){
            return AgentType.OTHER;
        }else if("8".equalsIgnoreCase(type)){
            return AgentType.TPC;
        }else if("9".equalsIgnoreCase(type)){
            return AgentType.DEVELOPER;
        }else if("10".equalsIgnoreCase(type)){
            return AgentType.GSSA_UPGRADE;
        }else{
            return null;
        }
    }

    private AgentStatus getAgentStatus(String status){
        if(AgentStatus.DRAFT.name().equalsIgnoreCase(status)){
            return AgentStatus.DRAFT;
        }else if(AgentStatus.WAITING.name().equalsIgnoreCase(status)){
            return AgentStatus.WAITING;
        }else if(AgentStatus.APPROVE.name().equalsIgnoreCase(status)){
            return AgentStatus.APPROVE;
        }else if(AgentStatus.NOT_APPROVE.name().equalsIgnoreCase(status)){
            return AgentStatus.NOT_APPROVE;
        }else{
            return null;
        }
    }

    private AgentRegisterStatus getAgentRegisterStatus(String status){
        if(AgentRegisterStatus.DRAFT.name().equalsIgnoreCase(status)){
            return AgentRegisterStatus.DRAFT;
        }else if(AgentRegisterStatus.APPROVE.name().equalsIgnoreCase(status)){
            return AgentRegisterStatus.APPROVE;
        }else{
            return null;
        }
    }

    @Data
    private class MASVendorTable {
        List<MASVendor> rows;
    }
    @Data
    private class MASTERVendorContactTable {
        List<MASTERVendorContact> rows;
    }
    @Data
    private class MASVendorContactTable {
        List<MASVendorContact> rows;
    }
    @Data
    private class MASProductVendorTable {
        List<MASProductVendor> rows;
    }
    @Data
    private class MASProductTable {
        List<MASProduct> rows;
    }

    @Data
    private class SLMAgentTable {
        List<SLMAgent> rows;
    }
}
