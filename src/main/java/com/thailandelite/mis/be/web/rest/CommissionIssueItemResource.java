package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CommissionIssueItem;
import com.thailandelite.mis.be.repository.CommissionIssueItemRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CommissionIssueItemResource {

    private final Logger log = LoggerFactory.getLogger(CommissionIssueItemResource.class);

    private static final String ENTITY_NAME = "commissionIssueItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionIssueItemRepository commissionIssueItemRepository;

    public CommissionIssueItemResource(CommissionIssueItemRepository commissionIssueItemRepository) {
        this.commissionIssueItemRepository = commissionIssueItemRepository;
    }


    @PostMapping("/commission-issue-items")
    public ResponseEntity<CommissionIssueItem> createCommissionIssueItem(@RequestBody CommissionIssueItem commissionIssueItem) throws URISyntaxException {
        log.debug("REST request to save CommissionIssueItem : {}", commissionIssueItem);
        if (commissionIssueItem.getId() != null) {
            throw new BadRequestAlertException("A new commissionIssueItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionIssueItem result = commissionIssueItemRepository.save(commissionIssueItem);
        return ResponseEntity.created(new URI("/api/commission-issue-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/commission-issue-items")
    public ResponseEntity<CommissionIssueItem> updateCommissionIssueItem(@RequestBody CommissionIssueItem commissionIssueItem) throws URISyntaxException {
        log.debug("REST request to update CommissionIssueItem : {}", commissionIssueItem);
        if (commissionIssueItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CommissionIssueItem result = commissionIssueItemRepository.save(commissionIssueItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionIssueItem.getId()))
            .body(result);
    }


    @GetMapping("/commission-issue-items")
    public List<CommissionIssueItem> getAllCommissionIssueItems() {
        log.debug("REST request to get all CommissionIssueItems");
        return commissionIssueItemRepository.findAll();
    }


    @GetMapping("/commission-issue-items/{id}")
    public ResponseEntity<CommissionIssueItem> getCommissionIssueItem(@PathVariable String id) {
        log.debug("REST request to get CommissionIssueItem : {}", id);
        Optional<CommissionIssueItem> commissionIssueItem = commissionIssueItemRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(commissionIssueItem);
    }


    @DeleteMapping("/commission-issue-items/{id}")
    public ResponseEntity<Void> deleteCommissionIssueItem(@PathVariable String id) {
        log.debug("REST request to delete CommissionIssueItem : {}", id);
        commissionIssueItemRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
