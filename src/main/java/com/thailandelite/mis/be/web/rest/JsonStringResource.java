package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.agent.JsonString;
import com.thailandelite.mis.be.repository.JsonStringRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class JsonStringResource {

    private final Logger log = LoggerFactory.getLogger(JsonStringResource.class);

    private static final String ENTITY_NAME = "jsonString";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JsonStringRepository jsonStringRepository;

    public JsonStringResource(JsonStringRepository jsonStringRepository) {
        this.jsonStringRepository = jsonStringRepository;
    }


    @PostMapping("/json-strings")
    public ResponseEntity<JsonString> createJsonString(@RequestBody JsonString jsonString) throws URISyntaxException {
        log.debug("REST request to save JsonString : {}", jsonString);
        if (jsonString.getId() != null) {
            throw new BadRequestAlertException("A new jsonString cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JsonString result = jsonStringRepository.save(jsonString);
        return ResponseEntity.created(new URI("/api/json-strings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/json-strings")
    public ResponseEntity<JsonString> updateJsonString(@RequestBody JsonString jsonString) throws URISyntaxException {
        log.debug("REST request to update JsonString : {}", jsonString);
        if (jsonString.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JsonString result = jsonStringRepository.save(jsonString);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jsonString.getId()))
            .body(result);
    }


    @GetMapping("/json-strings")
    public List<JsonString> getAllJsonStrings() {
        log.debug("REST request to get all JsonStrings");
        return jsonStringRepository.findAll();
    }


    @GetMapping("/json-strings/{id}")
    public ResponseEntity<JsonString> getJsonString(@PathVariable String id) {
        log.debug("REST request to get JsonString : {}", id);
        Optional<JsonString> jsonString = jsonStringRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jsonString);
    }


    @DeleteMapping("/json-strings/{id}")
    public ResponseEntity<Void> deleteJsonString(@PathVariable String id) {
        log.debug("REST request to delete JsonString : {}", id);
        jsonStringRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
