package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.CityRepository;
import com.thailandelite.mis.be.repository.IncidentRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.Incident;
import com.thailandelite.mis.model.domain.master.City;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;


@RestController
@RequestMapping("/api")
public class IncidentResource {

    private final Logger log = LoggerFactory.getLogger(IncidentResource.class);

    private static final String ENTITY_NAME = "incident";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IncidentRepository incidentRepository;

    public IncidentResource(IncidentRepository incidentRepository) {
        this.incidentRepository = incidentRepository;
    }


    @PostMapping("/incident")
    public ResponseEntity<Incident> createCity(@RequestBody Incident incident) throws URISyntaxException {
        log.debug("REST request to save Incident : {}", incident);

        Incident result = incidentRepository.save(incident);
        return ResponseEntity.created(new URI("/api/incident/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/incident")
    public ResponseEntity<Incident> updateCity(@RequestBody Incident incident) throws URISyntaxException {
        log.debug("REST request to update Incident : {}", incident);
        Incident result = incidentRepository.save(incident);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, incident.getId()))
            .body(result);
    }


    @GetMapping("/incident/{id}")
    public ResponseEntity<Incident> getIncident(@PathVariable String id) {
        log.debug("REST request to get incident : {}", id);
        Optional<Incident> incident = incidentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(incident);
    }


    @DeleteMapping("/incident/{id}")
    public ResponseEntity<Void> deleteCity(@PathVariable String id) {
        log.debug("REST request to delete City : {}", id);
        incidentRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PatchMapping("/incident/{id}/closed")
    public ResponseEntity<Incident> closeIncidentCase(@PathVariable String id ,Integer closeByQuota, Float closeByPenalty) throws URISyntaxException {
        log.debug("REST request to close Incident : {}, {}", closeByQuota, closeByQuota);
        Incident incident = incidentRepository.findById(id).orElseThrow(()->new NotFoundException("Not found incident."));
        if( closeByQuota != null)
            incident.setCloseByQuota(closeByQuota);
        if(closeByPenalty != null)
            incident.setCloseByPenalty(closeByPenalty);
        incidentRepository.save(incident);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, id))
            .body(incident);
    }
}
