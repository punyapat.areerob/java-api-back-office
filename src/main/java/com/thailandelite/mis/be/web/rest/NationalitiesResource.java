package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.NationalitiesRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.Nationality;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class NationalitiesResource {

    private final Logger log = LoggerFactory.getLogger(NationalitiesResource.class);

    private static final String ENTITY_NAME = "nationalities";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NationalitiesRepository nationalitiesRepository;

    public NationalitiesResource(NationalitiesRepository nationalitiesRepository) {
        this.nationalitiesRepository = nationalitiesRepository;
    }


    @PostMapping("/nationalities")
    public ResponseEntity<Nationality> createNationalities(@RequestBody Nationality nationality) throws URISyntaxException {
        log.debug("REST request to save Nationalities : {}", nationality);
        if (nationality.getId() != null) {
            throw new BadRequestAlertException("A new nationalities cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Nationality result = nationalitiesRepository.save(nationality);
        return ResponseEntity.created(new URI("/api/nationalities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/nationalities")
    public ResponseEntity<Nationality> updateNationalities(@RequestBody Nationality nationality) throws URISyntaxException {
        log.debug("REST request to update Nationalities : {}", nationality);
        if (nationality.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Nationality result = nationalitiesRepository.save(nationality);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, nationality.getId()))
            .body(result);
    }


    @GetMapping("/nationalities")
    public List<Nationality> getAllNationalities() {
        log.debug("REST request to get all Nationalities");
        return nationalitiesRepository.findAll(Sort.by("name"));
    }


    @GetMapping("/nationalities/{id}")
    public ResponseEntity<Nationality> getNationalities(@PathVariable String id) {
        log.debug("REST request to get Nationalities : {}", id);
        Optional<Nationality> nationalities = nationalitiesRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(nationalities);
    }


    @DeleteMapping("/nationalities/{id}")
    public ResponseEntity<Void> deleteNationalities(@PathVariable String id) {
        log.debug("REST request to delete Nationalities : {}", id);
        nationalitiesRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
