package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.agent.JsonInteger;
import com.thailandelite.mis.be.repository.JsonIntegerRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class JsonIntegerResource {

    private final Logger log = LoggerFactory.getLogger(JsonIntegerResource.class);

    private static final String ENTITY_NAME = "jsonInteger";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JsonIntegerRepository jsonIntegerRepository;

    public JsonIntegerResource(JsonIntegerRepository jsonIntegerRepository) {
        this.jsonIntegerRepository = jsonIntegerRepository;
    }


    @PostMapping("/json-integers")
    public ResponseEntity<JsonInteger> createJsonInteger(@RequestBody JsonInteger jsonInteger) throws URISyntaxException {
        log.debug("REST request to save JsonInteger : {}", jsonInteger);
        if (jsonInteger.getId() != null) {
            throw new BadRequestAlertException("A new jsonInteger cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JsonInteger result = jsonIntegerRepository.save(jsonInteger);
        return ResponseEntity.created(new URI("/api/json-integers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/json-integers")
    public ResponseEntity<JsonInteger> updateJsonInteger(@RequestBody JsonInteger jsonInteger) throws URISyntaxException {
        log.debug("REST request to update JsonInteger : {}", jsonInteger);
        if (jsonInteger.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JsonInteger result = jsonIntegerRepository.save(jsonInteger);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jsonInteger.getId()))
            .body(result);
    }


    @GetMapping("/json-integers")
    public List<JsonInteger> getAllJsonIntegers() {
        log.debug("REST request to get all JsonIntegers");
        return jsonIntegerRepository.findAll();
    }


    @GetMapping("/json-integers/{id}")
    public ResponseEntity<JsonInteger> getJsonInteger(@PathVariable String id) {
        log.debug("REST request to get JsonInteger : {}", id);
        Optional<JsonInteger> jsonInteger = jsonIntegerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jsonInteger);
    }


    @DeleteMapping("/json-integers/{id}")
    public ResponseEntity<Void> deleteJsonInteger(@PathVariable String id) {
        log.debug("REST request to delete JsonInteger : {}", id);
        jsonIntegerRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
