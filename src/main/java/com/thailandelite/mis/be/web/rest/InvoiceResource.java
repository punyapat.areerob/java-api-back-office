package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.PaymentTransactionRepository;
import com.thailandelite.mis.be.service.PaymentService;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.web.rest.flows.MemberPaymentBySLSResource;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.be.repository.InvoiceRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.enumeration.InvoiceStatus;
import com.thailandelite.mis.model.domain.enumeration.PaymentMethod;
import com.thailandelite.mis.model.domain.enumeration.PaymentTransactionStatus;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import com.thailandelite.mis.model.dto.request.OnlinePaymentRequest;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.*;


@RestController
@RequestMapping("/api")
public class InvoiceResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceResource.class);

    private static final String ENTITY_NAME = "invoice";
    private String flowDefinitionKey = "member_payment_by_sls";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InvoiceRepository invoiceRepository;
    private final PaymentTransactionRepository paymentTransactionRepository;
    private final PaymentService paymentService;

    public InvoiceResource(InvoiceRepository invoiceRepository, PaymentTransactionRepository paymentTransactionRepository, PaymentService paymentService) {
        this.invoiceRepository = invoiceRepository;
        this.paymentTransactionRepository = paymentTransactionRepository;
        this.paymentService = paymentService;
    }


    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }



    @PostMapping("/invoices")
    public ResponseEntity<Invoice> createInvoice(@RequestBody Invoice invoice) throws URISyntaxException {
        log.debug("REST request to save Invoice : {}", invoice);
        if (invoice.getId() != null) {
            throw new BadRequestAlertException("A new invoice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Invoice result = invoiceRepository.save(invoice);
        return ResponseEntity.created(new URI("/api/invoices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/invoices")
    public ResponseEntity<Invoice> updateInvoice(@RequestBody Invoice invoice) throws URISyntaxException {
        log.debug("REST request to update Invoice : {}", invoice);
        if (invoice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Invoice result = invoiceRepository.save(invoice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, invoice.getId()))
            .body(result);
    }

    @GetMapping("/invoice/{invoiceId}/payment-transactions")
    public List<PaymentTransaction> getAllPaymentTransactions(@PathVariable String invoiceId) {
        log.debug("REST request to get all PaymentTransactions");
        return paymentTransactionRepository.findByInvoiceId(invoiceId);
    }

    @PostMapping("/invoice/{invoiceId}/payment-transactions")
    public List<PaymentTransaction> createPaymentTransaction(@PathVariable String invoiceId, @RequestBody OnlinePaymentRequest onlinePaymentRequest) {
        log.debug("REST request to get all PaymentTransactions");
        return paymentService.createPaymentTransaction(invoiceId, onlinePaymentRequest);
    }

    @PutMapping("/invoice/{invoiceId}/payment-transactions")
    public PaymentTransaction updatePaymentTransaction(@PathVariable String invoiceId, @RequestBody PaymentTransaction paymentTransaction) {
        log.debug("REST request to get all PaymentTransactions");
        return paymentService.updatePaymentTransaction(invoiceId, paymentTransaction);
    }

    @GetMapping("/invoices")
    public ResponseEntity<FlowResponse<Page<Invoice>>> getAllInvoices(@RequestParam(defaultValue = "") String packageAction, Pageable pageable) {
        log.debug("REST request to get all Invoices");

        Page<Invoice> invoices = packageAction==""?
            invoiceRepository.findAll(pageable):
            invoiceRepository.findByPackageAction(packageAction,pageable)
                                ;
          HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest()
              ,invoices );
        return new ResponseEntity<>(new FlowResponse<Page<Invoice>>(flowDefinitionKey, 0,invoices), headers, HttpStatus.OK);
    }


    @GetMapping("/invoices/{id}")
    public ResponseEntity<Invoice> getInvoice(@PathVariable String id) {
        log.debug("REST request to get Invoice : {}", id);
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(invoice);
    }


    @DeleteMapping("/invoices/{id}")
    public ResponseEntity<Void> deleteInvoice(@PathVariable String id) {
        log.debug("REST request to delete Invoice : {}", id);
        invoiceRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PutMapping("/invoices/{id}/member/payslip")
    public ResponseEntity<Invoice> updateInvoicePaySlip(@PathVariable String id, @RequestParam String path) throws URISyntaxException {
        log.debug("REST request to Update payslip Invoice : {}, {}", id, path);
        return ResponseEntity.ok().body(paymentService.uploadPayslip(id, path));
    }

    @PutMapping("/invoices/online/charge-complete/transaction/{transId}")
    public ResponseEntity<Invoice> invoiceChargeComplete(@PathVariable String transId) throws URISyntaxException {
        log.debug("REST request to Charge complete Invoice : {}", transId);
        return ResponseEntity.ok().body(paymentService.completeOnlineCharge(transId));
    }
}
