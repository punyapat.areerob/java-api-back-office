package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.model.domain.enumeration.CaseActivityEvent;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.CaseActivityService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CaseActivityResource {

    private final Logger log = LoggerFactory.getLogger(CaseActivityResource.class);

    private static final String ENTITY_NAME = "caseActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseActivityRepository caseActivityRepository;

    private final CaseActivityService caseActivityService;

    public CaseActivityResource(CaseActivityRepository caseActivityRepository, CaseActivityService caseActivityService) {
        this.caseActivityRepository = caseActivityRepository;
        this.caseActivityService = caseActivityService;
    }


    @PostMapping("/case-activities")
    public ResponseEntity<CaseActivity> createCaseActivity(@RequestBody CaseActivity caseActivity) throws URISyntaxException {
        log.debug("REST request to save CaseActivity : {}", caseActivity);
        if (caseActivity.getId() != null) {
            throw new BadRequestAlertException("A new caseActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseActivity result = caseActivityRepository.save(caseActivity);
        return ResponseEntity.created(new URI("/api/case-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/case-activities")
    public ResponseEntity<CaseActivity> updateCaseActivity(@RequestBody CaseActivity caseActivity) throws URISyntaxException {
        log.debug("REST request to update CaseActivity : {}", caseActivity);
        if (caseActivity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseActivity result = caseActivityRepository.save(caseActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseActivity.getId()))
            .body(result);
    }


    @GetMapping("/case-activities")
    public List<CaseActivity> getAllCaseActivities() {
        log.debug("REST request to get all CaseActivities");
        return caseActivityRepository.findAll();
    }


    @GetMapping("/case-activities/{id}")
    public ResponseEntity<CaseActivity> getCaseActivity(@PathVariable String id) {
        log.debug("REST request to get CaseActivity : {}", id);
        Optional<CaseActivity> caseActivity = caseActivityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseActivity);
    }


    @GetMapping("/case-activities/processInstanceId/{processInstanceId}")
    public List<CaseActivity> getCaseActivityByProcessInstanceId(@PathVariable String processInstanceId) {
        log.debug("REST request to get CaseActivity : {}", processInstanceId);
        List<CaseActivity> caseActivity = caseActivityRepository.findAllByCaseInstanceId(processInstanceId);
        return caseActivity;
    }



    @DeleteMapping("/case-activities/{id}")
    public ResponseEntity<Void> deleteCaseActivity(@PathVariable String id) {
        log.debug("REST request to delete CaseActivity : {}", id);
        caseActivityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/case-activities/search")
    public List<CaseActivity> getCaseActivities(@RequestParam String caseInstanceId, @RequestParam Optional<CaseActivityEvent> event) {
        log.debug("REST request to get all CaseActivities by caseInstanceId and event");
        return this.caseActivityService.getCaseActivities(caseInstanceId, event);
    }
}
