package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.VisaType;
import com.thailandelite.mis.be.repository.VisaTypeRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class VisaTypeResource {

    private final Logger log = LoggerFactory.getLogger(VisaTypeResource.class);

    private static final String ENTITY_NAME = "visaType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VisaTypeRepository visaTypeRepository;

    public VisaTypeResource(VisaTypeRepository visaTypeRepository) {
        this.visaTypeRepository = visaTypeRepository;
    }


    @PostMapping("/visa-types")
    public ResponseEntity<VisaType> createVisaType(@RequestBody VisaType visaType) throws URISyntaxException {
        log.debug("REST request to save VisaType : {}", visaType);
        if (visaType.getId() != null) {
            throw new BadRequestAlertException("A new visaType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VisaType result = visaTypeRepository.save(visaType);
        return ResponseEntity.created(new URI("/api/visa-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/visa-types")
    public ResponseEntity<VisaType> updateVisaType(@RequestBody VisaType visaType) throws URISyntaxException {
        log.debug("REST request to update VisaType : {}", visaType);
        if (visaType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VisaType result = visaTypeRepository.save(visaType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, visaType.getId()))
            .body(result);
    }


    @GetMapping("/visa-types")
    public List<VisaType> getAllVisaTypes() {
        log.debug("REST request to get all VisaTypes");
        return visaTypeRepository.findAll();
    }


    @GetMapping("/visa-types/{id}")
    public ResponseEntity<VisaType> getVisaType(@PathVariable String id) {
        log.debug("REST request to get VisaType : {}", id);
        Optional<VisaType> visaType = visaTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(visaType);
    }


    @DeleteMapping("/visa-types/{id}")
    public ResponseEntity<Void> deleteVisaType(@PathVariable String id) {
        log.debug("REST request to delete VisaType : {}", id);
        visaTypeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
