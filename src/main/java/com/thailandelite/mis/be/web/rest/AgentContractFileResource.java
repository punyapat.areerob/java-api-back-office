package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentContractFile;
import com.thailandelite.mis.be.repository.AgentContractFileRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.thailandelite.mis.be.common.Utils.getExample;

@RestController
@RequestMapping("/api")
public class AgentContractFileResource {

    private final Logger log = LoggerFactory.getLogger(AgentContractFileResource.class);

    private static final String ENTITY_NAME = "agentContractFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentContractFileRepository agentContractFileRepository;

    public AgentContractFileResource(AgentContractFileRepository agentContractFileRepository) {
        this.agentContractFileRepository = agentContractFileRepository;
    }


    @PostMapping("/agent-contract-files")
    public ResponseEntity<AgentContractFile> createAgentContractFile(@RequestBody AgentContractFile agentContractFile) throws URISyntaxException {
        log.debug("REST request to save AgentContractFile : {}", agentContractFile);
        if (agentContractFile.getId() != null) {
            throw new BadRequestAlertException("A new agentContractFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentContractFile result = agentContractFileRepository.save(agentContractFile);
        return ResponseEntity.created(new URI("/api/agent-contract-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/agent-contract-files")
    public ResponseEntity<AgentContractFile> updateAgentContractFile(@RequestBody AgentContractFile agentContractFile) throws URISyntaxException {
        log.debug("REST request to update AgentContractFile : {}", agentContractFile);
        if (agentContractFile.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentContractFile result = agentContractFileRepository.save(agentContractFile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentContractFile.getId()))
            .body(result);
    }


    @GetMapping("/agent-contract-files")
    public List<AgentContractFile> getAllAgentContractFiles(@ModelAttribute AgentContractFile agentContractFile) {
        log.debug("REST request to get all AgentContractFiles: {}", agentContractFile);
        Example<AgentContractFile> example = getExample(agentContractFile);
        return agentContractFileRepository.findAll(example);
    }


    @GetMapping("/agent-contract-files/{id}")
    public ResponseEntity<AgentContractFile> getAgentContractFile(@PathVariable String id) {
        log.debug("REST request to get AgentContractFile : {}", id);
        Optional<AgentContractFile> agentContractFile = agentContractFileRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentContractFile);
    }


    @DeleteMapping("/agent-contract-files/{id}")
    public ResponseEntity<Void> deleteAgentContractFile(@PathVariable String id) {
        log.debug("REST request to delete AgentContractFile : {}", id);
        agentContractFileRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
