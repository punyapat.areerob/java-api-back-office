package com.thailandelite.mis.be.web.rest.flows.model;

import lombok.Builder;
import lombok.Getter;
import java.time.Instant;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Getter
@Builder(builderMethodName = "hiddenBuilder")
public class CaseQueryRequest {
    String flowDefKey;
    String [] flowDefKeys;
    String candidateGroup;
    List<String> taskDefKeys;
    List<String> statuses;
    String status;
    String name;
    String surname;
    String vendorId;
    String search;
    String packageAction;
    Instant from;
    Instant to;
    String memberId;
    Boolean finished;
    Pageable pageable;
    Boolean isArrival;
    List<String> bookingStatus;

    public static CaseQueryRequestBuilder builder(Pageable pageable) {
        return hiddenBuilder().pageable(pageable);
    }
}
