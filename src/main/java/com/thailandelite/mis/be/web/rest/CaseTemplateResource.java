package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CaseTemplate;
import com.thailandelite.mis.be.repository.CaseTemplateRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CaseTemplateResource {

    private final Logger log = LoggerFactory.getLogger(CaseTemplateResource.class);

    private static final String ENTITY_NAME = "caseTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaseTemplateRepository caseTemplateRepository;

    public CaseTemplateResource(CaseTemplateRepository caseTemplateRepository) {
        this.caseTemplateRepository = caseTemplateRepository;
    }


    @PostMapping("/case-templates")
    public ResponseEntity<CaseTemplate> createCaseTemplate(@RequestBody CaseTemplate caseTemplate) throws URISyntaxException {
        log.debug("REST request to save CaseTemplate : {}", caseTemplate);
        if (caseTemplate.getId() != null) {
            throw new BadRequestAlertException("A new caseTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaseTemplate result = caseTemplateRepository.save(caseTemplate);
        return ResponseEntity.created(new URI("/api/case-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/case-templates")
    public ResponseEntity<CaseTemplate> updateCaseTemplate(@RequestBody CaseTemplate caseTemplate) throws URISyntaxException {
        log.debug("REST request to update CaseTemplate : {}", caseTemplate);
        if (caseTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CaseTemplate result = caseTemplateRepository.save(caseTemplate);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caseTemplate.getId()))
            .body(result);
    }


    @GetMapping("/case-templates")
    public List<CaseTemplate> getAllCaseTemplates() {
        log.debug("REST request to get all CaseTemplates");
        return caseTemplateRepository.findAll();
    }


    @GetMapping("/case-templates/{id}")
    public ResponseEntity<CaseTemplate> getCaseTemplate(@PathVariable String id) {
        log.debug("REST request to get CaseTemplate : {}", id);
        Optional<CaseTemplate> caseTemplate = caseTemplateRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caseTemplate);
    }


    @DeleteMapping("/case-templates/{id}")
    public ResponseEntity<Void> deleteCaseTemplate(@PathVariable String id) {
        log.debug("REST request to delete CaseTemplate : {}", id);
        caseTemplateRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
