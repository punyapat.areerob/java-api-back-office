package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.VendorContract;
import com.thailandelite.mis.be.repository.VendorContractRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class VendorContractResource {

    private final Logger log = LoggerFactory.getLogger(VendorContractResource.class);

    private static final String ENTITY_NAME = "vendorContract";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VendorContractRepository vendorContractRepository;

    public VendorContractResource(VendorContractRepository vendorContractRepository) {
        this.vendorContractRepository = vendorContractRepository;
    }


    @PostMapping("/vendor-contracts")
    public ResponseEntity<VendorContract> createVendorContract(@RequestBody VendorContract vendorContract) throws URISyntaxException {
        log.debug("REST request to save VendorContract : {}", vendorContract);
        if (vendorContract.getId() != null) {
            throw new BadRequestAlertException("A new vendorContract cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VendorContract result = vendorContractRepository.save(vendorContract);
        return ResponseEntity.created(new URI("/api/vendor-contracts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/vendor-contracts")
    public ResponseEntity<VendorContract> updateVendorContract(@RequestBody VendorContract vendorContract) throws URISyntaxException {
        log.debug("REST request to update VendorContract : {}", vendorContract);
        if (vendorContract.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VendorContract result = vendorContractRepository.save(vendorContract);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vendorContract.getId()))
            .body(result);
    }


    @GetMapping("/vendor-contracts")
    public List<VendorContract> getAllVendorContracts() {
        log.debug("REST request to get all VendorContracts");
        return vendorContractRepository.findAll();
    }


    @GetMapping("/vendor-contracts/{id}")
    public ResponseEntity<VendorContract> getVendorContract(@PathVariable String id) {
        log.debug("REST request to get VendorContract : {}", id);
        Optional<VendorContract> vendorContract = vendorContractRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(vendorContract);
    }


    @DeleteMapping("/vendor-contracts/{id}")
    public ResponseEntity<Void> deleteVendorContract(@PathVariable String id) {
        log.debug("REST request to delete VendorContract : {}", id);
        vendorContractRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
