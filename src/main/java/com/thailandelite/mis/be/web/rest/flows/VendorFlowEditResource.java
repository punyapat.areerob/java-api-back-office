package com.thailandelite.mis.be.web.rest.flows;

import com.thailandelite.mis.be.service.flow.BPEditVendorService;
import com.thailandelite.mis.be.service.flow.BPVendorRegisterService;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.model.dto.request.VendorProfileUpdateDao;
import com.thailandelite.mis.model.dto.request.VendorRegisterDao;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/api/vendor/edit/flow")
@RequiredArgsConstructor
public class VendorFlowEditResource {

    private final Logger log = LoggerFactory.getLogger(VendorFlowEditResource.class);

    private static final String ENTITY_NAME = "vendor";

    public String getFlowDefinitionKey(){
        return  "edit_vendor";
    }
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BPEditVendorService bpEditVendorService;
    private final BPVendorRegisterService bpVendorRegisterService;

    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }

    @PostMapping("/Create")
    public ResponseEntity<FlowResponse<VendorProfileUpdateDao>> Update(@RequestBody VendorProfileUpdateDao vendorProfileUpdateDao) {
        log.debug("REST request to save createApplication : {}", vendorProfileUpdateDao);
        bpEditVendorService.create(getFlowDefinitionKey(), vendorProfileUpdateDao);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0 , vendorProfileUpdateDao));
    }

    @GetMapping
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(@RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                  @RequestParam(defaultValue = "") String status,
                                                                  Pageable pageable) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKey(getFlowDefinitionKey())
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .status(status).build();

        Page<CaseInfoDTO> applications = bpEditVendorService.queryCase(caseQuery);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(), 0, applications));
    }

    @GetMapping("/{caseId}")
    public ResponseEntity<FlowResponse<CaseInfoDTO>> getApplicationTask(@PathVariable String caseId) {
        log.debug("REST request to get JobApplication : {}", caseId);
        CaseInfoDTO caseInfoDTO = bpEditVendorService.getDetail(caseId);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(), 0, caseInfoDTO));
    }

    @GetMapping("/process/{processInstanceId}/{taskDefKey}/{action}")
    public ResponseEntity<Object> process(@PathVariable String processInstanceId, @PathVariable String taskDefKey, @PathVariable String action,@RequestParam String remark) {
        log.debug("REST request to get JobApplication : {}", processInstanceId);
        bpEditVendorService.processTask(processInstanceId, taskDefKey, action , SecurityContextHolder.getContext().getAuthentication().getName(),remark);
        return ResponseEntity.ok(new FlowResponse<String>(getFlowDefinitionKey(), 0,"success"));
    }
}
