package com.thailandelite.mis.be.web.rest.test;

import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.be.service.HandlerException;
import com.thailandelite.mis.be.service.MemberService;
import com.thailandelite.mis.be.service.model.MembershipInfo;
import com.thailandelite.mis.model.domain.Application;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/test/member")
@RequiredArgsConstructor
public class MemberServiceTestResource {
    private final MemberService memberService;

    @PutMapping("/newMembership")
    public MembershipInfo newMembership(@RequestBody Application application, String cardId) {
        return null;
    }



    @DeleteMapping("/deleteMembership/{member_id}")
    public boolean deleteMembership(@PathVariable String memberId) throws HandlerException {
        log.debug("REST request to delete membership");
        return memberService.deleteMembership(memberId);
    }

    @PutMapping("/memberships/{member_id}/privilege/{privilege_id}")
    public MemberPrivilegeQuota commitQuota(@PathVariable String member_id, @PathVariable String privilege_id) throws HandlerException {
        log.debug("REST request to commit privilege");
        return memberService.commitPrivilegeQuota(member_id, privilege_id);
    }

/*    @PutMapping("/memberships/{member_id}/advance_annual_fee/{years}")
    public Membership paidAnnualFee(@PathVariable String member_id, @PathVariable Integer years) throws HandlerException {
        log.debug("REST request to append paid of advanced annual fee");
        return memberService.paidAnnualFee(member_id, years);
    }*/
}
