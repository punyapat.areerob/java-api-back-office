package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.repository.CaseInfoRepository;
import com.thailandelite.mis.be.service.dto.reports.FinanceRequest;
import com.thailandelite.mis.be.service.report.ReportService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/finance")
public class FinanceReportResource {

    private final CaseInfoRepository caseInfoRepository;
    private final ReportService reportService;


    @GetMapping("/invoices")
    public ResponseEntity<FlowResponse<Page<Invoice>>> getCaseInvoice(FinanceRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0,  reportService.queryInvoicePage(searchRequest,pageable) ));
    }

    @GetMapping("/invoices/excel")
    public byte[] getCaseInvoiceExcel(FinanceRequest searchRequest, Pageable pageable) {
        return reportService.queryInvoiceExcel(searchRequest,pageable);
    }


    @GetMapping("/activate")
    public ResponseEntity<FlowResponse<Page<Invoice>>> getCaseActivated(FinanceRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0,   reportService.queryActivateMemberPage(searchRequest,pageable) ));
    }


    @GetMapping("/activate/excel")
    public  byte[] getCaseActivatedExcel(FinanceRequest searchRequest, Pageable pageable) {
        return reportService.queryActivateMemberExcel(searchRequest,pageable);
    }

    @GetMapping("/invoices_not_complete")
    public ResponseEntity<FlowResponse<Page<Invoice>>> getNotCompleteInvoice(FinanceRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0,   reportService.queryInvoiceNotCompletePage(searchRequest,pageable) ));
    }


    @GetMapping("/invoices_not_complete/excel")
    public byte[] getNotCompleteInvoiceExcel(FinanceRequest searchRequest, Pageable pageable) {
        return reportService.queryInvoiceNotCompleteExcel(searchRequest,pageable);
    }

    @GetMapping("/wait_activate")
    public ResponseEntity<FlowResponse<Page<Invoice>>> getWaitActivate(FinanceRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0,    reportService.queryNotActivateMemberPage(searchRequest,pageable) ));
    }

    @GetMapping("/wait_activate/excel")
    public byte[] getWaitActivateExcel(FinanceRequest searchRequest, Pageable pageable) {
        return reportService.queryNotActivateMemberExcel(searchRequest,pageable);
    }

    @GetMapping("/income_report")
    public ResponseEntity<FlowResponse<Page<IncomeReport>>> getIncomeReport(FinanceRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0,    reportService.getIncomeReport(searchRequest,pageable) ));
    }

    @GetMapping("/income_report/excel")
    public byte[] getIncomeReportExcel(FinanceRequest searchRequest, Pageable pageable) {
        return reportService.getIncomeReportExcel(searchRequest,pageable) ;
    }

}
