package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.BaseReportRequest;
import com.thailandelite.mis.be.service.dto.reports.BookingReportResponse;
import com.thailandelite.mis.be.service.dto.reports.PaymentInfoRequest;
import com.thailandelite.mis.be.service.dto.reports.ProductReportResponse;
import com.thailandelite.mis.be.service.report.ProductReportService;
import com.thailandelite.mis.model.FlowResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ProductReportResource {

    private final ProductReportService reportService;

    @GetMapping("/reports/product-info")
    public ResponseEntity<FlowResponse<Page<ProductReportResponse>>> getPaymentInfoReports(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getProductReport(searchRequest,pageable) ));
    }

    @GetMapping("/reports/product-info/excel")
    public byte[] getPaymentInfoExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return reportService.generateExcelProductReport(searchRequest);
    }

}
