package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.InputField;
import com.thailandelite.mis.be.repository.InputFieldRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class InputFieldResource {

    private final Logger log = LoggerFactory.getLogger(InputFieldResource.class);

    private static final String ENTITY_NAME = "inputField";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InputFieldRepository inputFieldRepository;

    public InputFieldResource(InputFieldRepository inputFieldRepository) {
        this.inputFieldRepository = inputFieldRepository;
    }


    @PostMapping("/input-fields")
    public ResponseEntity<InputField> createInputField(@RequestBody InputField inputField) throws URISyntaxException {
        log.debug("REST request to save InputField : {}", inputField);
        if (inputField.getId() != null) {
            throw new BadRequestAlertException("A new inputField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InputField result = inputFieldRepository.save(inputField);
        return ResponseEntity.created(new URI("/api/input-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/input-fields")
    public ResponseEntity<InputField> updateInputField(@RequestBody InputField inputField) throws URISyntaxException {
        log.debug("REST request to update InputField : {}", inputField);
        if (inputField.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InputField result = inputFieldRepository.save(inputField);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, inputField.getId()))
            .body(result);
    }


    @GetMapping("/input-fields")
    public List<InputField> getAllInputFields() {
        log.debug("REST request to get all InputFields");
        return inputFieldRepository.findAll();
    }


    @GetMapping("/input-fields/{id}")
    public ResponseEntity<InputField> getInputField(@PathVariable String id) {
        log.debug("REST request to get InputField : {}", id);
        Optional<InputField> inputField = inputFieldRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(inputField);
    }


    @DeleteMapping("/input-fields/{id}")
    public ResponseEntity<Void> deleteInputField(@PathVariable String id) {
        log.debug("REST request to delete InputField : {}", id);
        inputFieldRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
