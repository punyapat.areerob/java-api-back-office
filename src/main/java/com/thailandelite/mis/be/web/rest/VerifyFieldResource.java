package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.VerifyField;
import com.thailandelite.mis.be.repository.VerifyFieldRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class VerifyFieldResource {

    private final Logger log = LoggerFactory.getLogger(VerifyFieldResource.class);

    private static final String ENTITY_NAME = "verifyField";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VerifyFieldRepository verifyFieldRepository;

    public VerifyFieldResource(VerifyFieldRepository verifyFieldRepository) {
        this.verifyFieldRepository = verifyFieldRepository;
    }


    @PostMapping("/verify-fields")
    public ResponseEntity<VerifyField> createVerifyField(@RequestBody VerifyField verifyField) throws URISyntaxException {
        log.debug("REST request to save VerifyField : {}", verifyField);
        if (verifyField.getId() != null) {
            throw new BadRequestAlertException("A new verifyField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VerifyField result = verifyFieldRepository.save(verifyField);
        return ResponseEntity.created(new URI("/api/verify-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/verify-fields")
    public ResponseEntity<VerifyField> updateVerifyField(@RequestBody VerifyField verifyField) throws URISyntaxException {
        log.debug("REST request to update VerifyField : {}", verifyField);
        if (verifyField.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VerifyField result = verifyFieldRepository.save(verifyField);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, verifyField.getId()))
            .body(result);
    }


    @GetMapping("/verify-fields")
    public List<VerifyField> getAllVerifyFields() {
        log.debug("REST request to get all VerifyFields");
        return verifyFieldRepository.findAll();
    }


    @GetMapping("/verify-fields/{id}")
    public ResponseEntity<VerifyField> getVerifyField(@PathVariable String id) {
        log.debug("REST request to get VerifyField : {}", id);
        Optional<VerifyField> verifyField = verifyFieldRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(verifyField);
    }


    @DeleteMapping("/verify-fields/{id}")
    public ResponseEntity<Void> deleteVerifyField(@PathVariable String id) {
        log.debug("REST request to delete VerifyField : {}", id);
        verifyFieldRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
