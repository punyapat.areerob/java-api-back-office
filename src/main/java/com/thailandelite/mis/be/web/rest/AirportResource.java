package com.thailandelite.mis.be.web.rest;
import com.amadeus.exceptions.ResponseException;
import com.thailandelite.mis.be.repository.AirportRepository;
import com.thailandelite.mis.be.service.AirportService;
import com.thailandelite.mis.be.service.FlightService;
import com.thailandelite.mis.model.domain.Airport;
import com.thailandelite.mis.model.domain.airport.AirLineRepons;
import com.thailandelite.mis.model.domain.airport.DatedFilghtReponse;
import com.thailandelite.mis.model.domain.airport.FlightOfferSearchReponse;
import com.thailandelite.mis.model.domain.airport.FlightOfferSearchRequest;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AirportResource {
    private final Logger log = LoggerFactory.getLogger(AgentTypeResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final AirportRepository airportRepository;
    private final AirportService airportService;
    private final FlightService flightService;
    @GetMapping("/save/Airport")
    public List<Airport> save() {
        log.debug("REST request to get all Cities");
            return airportService.canCreateAirportResource();
        }
    @GetMapping("/Airport")
    public  List<Airport> getAll(){
        log.debug("REST request to get all Airport");
        return airportRepository.findAll();
    }
    @GetMapping("/Airport/Search/")
    public List<FlightOfferSearchReponse> getFlight(String From, String To, String dateFrom, String dateTo) throws ResponseException {
        log.debug("REST request to get all Airport");
        //String From,String To,String dateFrom,String dateTo
        return  flightService.FlightOfferSearch(From,To,dateFrom,dateTo);
    }
    @PostMapping("/Airport/Search/")
    public List<FlightOfferSearchReponse> getFlightBody(@RequestBody FlightOfferSearchRequest body) throws ResponseException {
        log.debug("REST request to get all Airport");
        //String From,String To,String dateFrom,String dateTo
        return  flightService.FlightOfferSearch(body);
    }
    @GetMapping("/Airport/Search/filght")
    public List<DatedFilghtReponse> getFlightByFlightNumber(String Fromdate, String Flight) throws ResponseException {
        log.debug("REST request to get flight by flightnumber");
        //String From,String To,String dateFrom,String dateTo
        List<DatedFilghtReponse> result = new ArrayList<>();
        try{
            result =  flightService.GetFlight(Fromdate,Flight);
        }catch (Exception e){
            e.printStackTrace();;
        }

        return result;
    }
    @GetMapping("/Airport/ICAO/Detail")
    public List<AirLineRepons> getICAO(String airlineCodes) throws ResponseException {
        log.debug("REST request to get flight by flightnumber");
        //String From,String To,String dateFrom,String dateTo
        return  flightService.GetICAO(airlineCodes);
    }

}


