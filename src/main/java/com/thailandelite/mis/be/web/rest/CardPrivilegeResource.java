package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.BroadcastMessage;
import com.thailandelite.mis.be.service.dto.UserDTO;
import com.thailandelite.mis.model.domain.CardPrivilege;
import com.thailandelite.mis.be.repository.CardPrivilegeRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.UntypedExampleMatcher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.thailandelite.mis.be.common.Utils.getExample;

@RestController
@RequestMapping("/api")
public class CardPrivilegeResource {

    private final Logger log = LoggerFactory.getLogger(CardPrivilegeResource.class);

    private static final String ENTITY_NAME = "cardPrivilege";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CardPrivilegeRepository cardPrivilegeRepository;

    public CardPrivilegeResource(CardPrivilegeRepository cardPrivilegeRepository) {
        this.cardPrivilegeRepository = cardPrivilegeRepository;
    }


    @PostMapping("/card-privileges")
    public ResponseEntity<CardPrivilege> createCardPrivilege(@RequestBody CardPrivilege cardPrivilege) throws URISyntaxException {
        log.debug("REST request to save CardPrivilege : {}", cardPrivilege);
        if (cardPrivilegeRepository.existsById(cardPrivilege.getId())) {
            throw new BadRequestAlertException("A new cardPrivilege cannot already have an ID", ENTITY_NAME, "id exists");
        }
        CardPrivilege result = cardPrivilegeRepository.save(cardPrivilege);
        return ResponseEntity.created(new URI("/api/card-privileges/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/card-privileges")
    public ResponseEntity<CardPrivilege> updateCardPrivilege(@RequestBody CardPrivilege cardPrivilege) throws URISyntaxException {
        log.debug("REST request to update CardPrivilege : {}", cardPrivilege);
        if (cardPrivilege.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CardPrivilege result = cardPrivilegeRepository.save(cardPrivilege);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cardPrivilege.getId()))
            .body(result);
    }


    @GetMapping("/card-privileges")
    public List<CardPrivilege> getAllCardPrivileges(@ModelAttribute CardPrivilege cardPrivilege) {
        log.debug("REST request to get all CardPrivileges: {}", cardPrivilege);

        Example<CardPrivilege> example = getExample(cardPrivilege);
        return cardPrivilegeRepository.findAll(example);
    }

    @GetMapping("/card-privileges/{id}")
    public ResponseEntity<CardPrivilege> getCardPrivilege(@PathVariable String id) {
        log.debug("REST request to get CardPrivilege : {}", id);
        Optional<CardPrivilege> cardPrivilege = cardPrivilegeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cardPrivilege);
    }


    @DeleteMapping("/card-privileges/{id}")
    public ResponseEntity<Void> deleteCardPrivilege(@PathVariable String id) {
        log.debug("REST request to delete CardPrivilege : {}", id);
        cardPrivilegeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
