package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.Occupation;
import com.thailandelite.mis.be.repository.OccupationRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class OccupationResource {

    private final Logger log = LoggerFactory.getLogger(OccupationResource.class);

    private static final String ENTITY_NAME = "occupation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OccupationRepository occupationRepository;

    public OccupationResource(OccupationRepository occupationRepository) {
        this.occupationRepository = occupationRepository;
    }


    @PostMapping("/occupations")
    public ResponseEntity<Occupation> createOccupation(@RequestBody Occupation occupation) throws URISyntaxException {
        log.debug("REST request to save Occupation : {}", occupation);
        if (occupation.getId() != null) {
            throw new BadRequestAlertException("A new occupation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Occupation result = occupationRepository.save(occupation);
        return ResponseEntity.created(new URI("/api/occupations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/occupations")
    public ResponseEntity<Occupation> updateOccupation(@RequestBody Occupation occupation) throws URISyntaxException {
        log.debug("REST request to update Occupation : {}", occupation);
        if (occupation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Occupation result = occupationRepository.save(occupation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, occupation.getId()))
            .body(result);
    }


    @GetMapping("/occupations")
    public List<Occupation> getAllOccupations(Boolean publicStatus) {
        log.debug("REST request to get all Occupations");
        if(publicStatus != null){
            return occupationRepository.findAllByPublicStatus(publicStatus);
        }else {
            return occupationRepository.findAll();
        }
    }


    @GetMapping("/occupations/{id}")
    public ResponseEntity<Occupation> getOccupation(@PathVariable String id) {
        log.debug("REST request to get Occupation : {}", id);
        Optional<Occupation> occupation = occupationRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(occupation);
    }


    @DeleteMapping("/occupations/{id}")
    public ResponseEntity<Void> deleteOccupation(@PathVariable String id) {
        log.debug("REST request to delete Occupation : {}", id);
        occupationRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
