package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.ProductRepository;
import com.thailandelite.mis.be.repository.VendorRepository;
import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.Vendor;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.be.repository.BookingServiceMemberRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BookingServiceMemberResource {

    private final Logger log = LoggerFactory.getLogger(BookingServiceMemberResource.class);

    private static final String ENTITY_NAME = "bookingServiceMember";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookingServiceMemberRepository bookingServiceMemberRepository;
    private final ProductRepository productRepository;
    private final VendorRepository vendorRepository;

    public BookingServiceMemberResource(BookingServiceMemberRepository bookingServiceMemberRepository, ProductRepository productRepository, VendorRepository vendorRepository) {
        this.bookingServiceMemberRepository = bookingServiceMemberRepository;
        this.productRepository = productRepository;
        this.vendorRepository = vendorRepository;
    }


    @PostMapping("/booking-service-members")
    public ResponseEntity<BookingServiceMember> createBookingServiceMember(@RequestBody BookingServiceMember bookingServiceMember) throws URISyntaxException {
        log.debug("REST request to save BookingServiceMember : {}", bookingServiceMember);
        if (bookingServiceMember.getId() != null) {
            throw new BadRequestAlertException("A new bookingServiceMember cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookingServiceMember result = bookingServiceMemberRepository.save(bookingServiceMember);
        return ResponseEntity.created(new URI("/api/booking-service-members/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/booking-service-members")
    public ResponseEntity<BookingServiceMember> updateBookingServiceMember(@RequestBody BookingServiceMember bookingServiceMember) throws URISyntaxException {
        log.debug("REST request to update BookingServiceMember : {}", bookingServiceMember);
        if (bookingServiceMember.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BookingServiceMember result = bookingServiceMemberRepository.save(bookingServiceMember);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bookingServiceMember.getId()))
            .body(result);
    }


    @GetMapping("/booking-service-members")
    public List<BookingServiceMember> getAllBookingServiceMembers() {
        log.debug("REST request to get all BookingServiceMembers");
        return bookingServiceMemberRepository.findAll();
    }


    @GetMapping("/booking-service-members/{id}")
    public ResponseEntity<BookingServiceMember> getBookingServiceMember(@PathVariable String id) {
        log.debug("REST request to get BookingServiceMember : {}", id);
        Optional<BookingServiceMember> bookingServiceMember = bookingServiceMemberRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(bookingServiceMember);
    }


    @DeleteMapping("/booking-service-members/{id}")
    public ResponseEntity<Void> deleteBookingServiceMember(@PathVariable String id) {
        log.debug("REST request to delete BookingServiceMember : {}", id);
        bookingServiceMemberRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }



    @GetMapping("/booking-service-members/fix")
    public void fixBookingServiceMember() {
        List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findAll();
        for (BookingServiceMember booking : bookingServiceMembers) {
            if(!StringUtils.isEmpty(booking.getProductId())){
                Optional<Product> product = productRepository.findById(booking.getProductId());
                if(product.isPresent()){
                    booking.setFlowDefinitionKey(product.get().getFlowDefinitionKey());
                    Optional<Vendor> vendor = vendorRepository.findById(product.get().getVendorId());
                    if(vendor.isPresent()) {
                        booking.setVendorId(product.get().getVendorId());
                        booking.setVendorName(vendor.get().getNameEn());
                    }
                }
            }
        }
        bookingServiceMemberRepository.saveAll(bookingServiceMembers);
    }
}
