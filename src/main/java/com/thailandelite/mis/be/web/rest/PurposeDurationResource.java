package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.repository.BloodTypeRepository;
import com.thailandelite.mis.be.repository.PurposeDurationRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.master.BloodType;
import com.thailandelite.mis.model.domain.master.PurposeDuration;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PurposeDurationResource {

    private final Logger log = LoggerFactory.getLogger(PurposeDurationResource.class);

    private static final String ENTITY_NAME = "purposeDuration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PurposeDurationRepository repository;

    public PurposeDurationResource(PurposeDurationRepository repository) {
        this.repository = repository;
    }


    @PostMapping("/purpose-duration")
    public ResponseEntity<PurposeDuration> createPurposeDuration(@RequestBody PurposeDuration entity) throws URISyntaxException {
        log.debug("REST request to save BloodType : {}", entity);
        if (entity.getId() != null) {
            throw new BadRequestAlertException("A new bloodType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PurposeDuration result = repository.save(entity);
        return ResponseEntity.created(new URI("/api/purpose-duration/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/purpose-duration")
    public ResponseEntity<PurposeDuration> updatePurposeDuration(@RequestBody PurposeDuration entity) throws URISyntaxException {
        log.debug("REST request to update BloodType : {}", entity);
        if (entity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PurposeDuration result = repository.save(entity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, entity.getId()))
            .body(result);
    }


    @GetMapping("/purpose-duration")
    public List<PurposeDuration> getAllPurposeDuration() {
        log.debug("REST request to get all BloodTypes");
        return repository.findAll();
    }


    @GetMapping("/purpose-duration/{id}")
    public ResponseEntity<PurposeDuration> getPurposeDuration(@PathVariable String id) {
        log.debug("REST request to get BloodType : {}", id);
        Optional<PurposeDuration> entity = repository.findById(id);
        return ResponseUtil.wrapOrNotFound(entity);
    }


    @DeleteMapping("/purpose-duration/{id}")
    public ResponseEntity<Void> deletePurposeDuration(@PathVariable String id) {
        log.debug("REST request to delete BloodType : {}", id);
        repository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
