package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.SubProductCategory;
import com.thailandelite.mis.be.repository.ProductRepository;
import com.thailandelite.mis.be.repository.SubProductCategoryRepository;
import com.thailandelite.mis.be.service.ProductService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.model.domain.Product;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class SubProductCategoryResource {

    private final Logger log = LoggerFactory.getLogger(SubProductCategoryResource.class);

    private static final String ENTITY_NAME = "subProductCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubProductCategoryRepository subProductCategoryRepository;
    private final ProductRepository productRepository;
    private final ProductService productService;


    @PostMapping("/sub-product-categories")
    public ResponseEntity<SubProductCategory> createSubProductCategory(@RequestBody SubProductCategory subProductCategory) throws URISyntaxException {
        log.debug("REST request to save SubProductCategory : {}", subProductCategory);
        if (subProductCategory.getId() != null) {
            throw new BadRequestAlertException("A new subProductCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubProductCategory result = subProductCategoryRepository.save(subProductCategory);
        return ResponseEntity.created(new URI("/api/sub-product-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/sub-product-categories")
    public ResponseEntity<SubProductCategory> updateSubProductCategory(@RequestBody SubProductCategory subProductCategory) throws URISyntaxException {
        log.debug("REST request to update SubProductCategory : {}", subProductCategory);
        if (subProductCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubProductCategory result = subProductCategoryRepository.save(subProductCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subProductCategory.getId()))
            .body(result);
    }


    @GetMapping("/sub-product-categories")
    public List<SubProductCategory> getAllSubProductCategories() {
        log.debug("REST request to get all SubProductCategories");
        return subProductCategoryRepository.findAll();
    }


    @GetMapping("/sub-product-categories/{id}")
    public ResponseEntity<SubProductCategory> getSubProductCategory(@PathVariable String id) {
        log.debug("REST request to get SubProductCategory : {}", id);
        Optional<SubProductCategory> subProductCategory = subProductCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(subProductCategory);
    }


    @DeleteMapping("/sub-product-categories/{id}")
    public ResponseEntity<Void> deleteSubProductCategory(@PathVariable String id) {
        log.debug("REST request to delete SubProductCategory : {}", id);
        subProductCategoryRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @GetMapping("/sub-product-categories-menu/{productCategoryId}")
    public ResponseEntity<List<SubProductCategory>> getSubProductCategoryByProductCategory(@PathVariable String productCategoryId) {
        log.debug("REST request to get SubProductCategory : {}", productCategoryId);
        List<SubProductCategory> subCatByCategoryId = productService.getSubCatByCategoryId(productCategoryId);





        return ResponseEntity.ok(subCatByCategoryId);
    }


    @PutMapping("/sub-product-categories/{id}/product")
    public ResponseEntity<Integer> addProductToSubCategory(@PathVariable String id, String productName) {
        Integer count = 0;
        List<Product> products = productRepository.findAll();
        Optional<SubProductCategory> subProductCategoryOptional = subProductCategoryRepository.findById(id);
        if(subProductCategoryOptional.isPresent()) {
            SubProductCategory subProductCategory = subProductCategoryOptional.get();
            HashSet<Product> newProduct = new HashSet<>();
            for (Product product : products) {
                if (product != null && product.getNameEn() != null && product.getNameEn().toLowerCase().contains(productName)) {
                    newProduct.add(product);
                }
            }
            subProductCategory.setProductLists(newProduct);
            subProductCategoryRepository.save(subProductCategory);
        }
        return ResponseEntity.ok().body(count);
    }
}
