package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.service.core.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLConnection;

@Slf4j
@RestController
@RequestMapping(path = "/api/files")
@RequiredArgsConstructor
public class FileController {
    private final FileService fileService;

    @PostMapping(path = "/upload")
    public void upload(MultipartFile file, @RequestParam String pathToFile) throws UploadFailException {
        log.info("Upload: {}", pathToFile);
        fileService.upload(pathToFile, file);
    }

    @GetMapping()
    public void getObject(@RequestParam String pathToFile, HttpServletResponse response) throws Exception {
        log.info("Get file: {}", pathToFile);
        InputStream inputStream = fileService.getFileAsStream(pathToFile);
        response.setContentType(URLConnection.guessContentTypeFromName(pathToFile));
        IOUtils.copy(inputStream, response.getOutputStream());
        response.flushBuffer();
    }
}
