package com.thailandelite.mis.be.web.rest;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.config.dbmigrations.InitialSetupMigration;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.ldap.LdapHelper;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.service.BarcodeService;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.JAService;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.booking.JaFrom;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperties;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperty;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.context.Context;

import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.hasText;

@Slf4j
@RestController
@RequestMapping("/api/test")
@RequiredArgsConstructor
public class TestResource {
    private final MongoTemplate mongoTemplate;
    private final LdapHelper ldapHelper = new LdapHelper();
    protected final JAService jaService;
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private final BarcodeService barcodeService;


    @GetMapping("/init")
    public void activateAccount(@RequestParam(value = "key") String key) {
        InitialSetupMigration initialSetupMigration = new InitialSetupMigration();
        initialSetupMigration.addAuthorities(mongoTemplate);

        initialSetupMigration.addUsers(mongoTemplate);
    }

    @GetMapping("/testldap")
    public String testLdap() throws NamingException {
        return ldapHelper.authenticate("thaielite\\jirapongw", "Golfdigg123*");
    }

    @GetMapping("/testBarcode")
    public String testBarcode() throws Exception {
        FileUtils.writeByteArrayToFile(new File("testbarcode.jpg"), barcodeService.generateBarcodeImage("EA1234", "pi123", "10000"), false);
        return "success";
    }

    @GetMapping("/ja/booking")
    public String testGeneratePDFJabooking(String bookingId) throws UploadFailException, DocumentException, IOException {
        bookingId = hasText(bookingId) ? bookingId : "60be554240c8dd1204978093";

        Booking booking = bookingRepository.findById(bookingId).orElseThrow();
        return jaService.createJA(booking.getId(),"EPA");
    }

    @GetMapping("/ja/booking/last")
    public JaFrom testJabooking(@RequestParam(value = "bookingId") String bookingId){
        return jaService.getJABookingLast(bookingId);
    }

    @GetMapping("/testCreateProcess")
    public String testProcess(@RequestParam(value = "processDefinitionId") String processDefinitionId, @RequestParam(value = "businessKey") String businessKey) throws NamingException {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
        ManagementService managementService = ProcessEngines.getDefaultProcessEngine().getManagementService();
        ProcessInstance pi = runtimeService.startProcessInstanceById(processDefinitionId, businessKey);
        List<Task> taskList = taskService.createTaskQuery().processInstanceId(pi.getId()).list();
        Task task = taskList.get(0);
        return task.getId();
    }

    @GetMapping("/testclaim")
    public String testclaim(@RequestParam(value = "key") String key) throws NamingException {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
        ManagementService managementService = ProcessEngines.getDefaultProcessEngine().getManagementService();
        taskService.claim(key, "demo");
        return "success";
    }

    @GetMapping("/testGetTaskNextStage")
    public Map<String, String> testGetTaskId(@RequestParam(value = "taskId") String taskId) throws NamingException {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
        ManagementService managementService = ProcessEngines.getDefaultProcessEngine().getManagementService();
        var taskQuery = taskService.createTaskQuery().taskId(taskId).list();
        var processDefId = taskQuery.get(0).getProcessDefinitionId();
        var taskKey = taskQuery.get(0).getTaskDefinitionKey();
        return testGetProcessDefinition(processDefId, taskKey);
    }

    @GetMapping("/testGetProcessDefinition")
    public Map<String, String> testGetProcessDefinition(@RequestParam(value = "processDefinitionId") String processDefinitionId, @RequestParam(value = "key") String key) throws NamingException {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
        TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
        ManagementService managementService = ProcessEngines.getDefaultProcessEngine().getManagementService();

        BpmnModelInstance bpmnModelInstance = repositoryService.getBpmnModelInstance(processDefinitionId);
        UserTask userTask = bpmnModelInstance.getModelElementById(key);
        var ex = userTask.getExtensionElements().getElementsQuery().filterByType(CamundaProperties.class).singleResult();
        Map<String, String> result = new HashMap<String, String>();
        for (CamundaProperty cm : ex.getCamundaProperties()) {
            String name = cm.getCamundaName();
            String value = cm.getCamundaValue();
            result.put(name, value);
        }
        return result;
    }

    @GetMapping("/testGetVariable")
    public Map<String, Object> testGetVariable(@RequestParam(value = "key") String key) throws NamingException {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        RepositoryService repo = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
        TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
        ManagementService managementService = ProcessEngines.getDefaultProcessEngine().getManagementService();

        return taskService.getVariables(key);
    }

    @GetMapping("/testMove")
    public String testMove(@RequestParam(value = "key") String key, @RequestParam(value = "action") String action) throws NamingException {
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        TaskService taskService = ProcessEngines.getDefaultProcessEngine().getTaskService();
        ManagementService managementService = ProcessEngines.getDefaultProcessEngine().getManagementService();
        Map<String, Object> actionV = new HashMap<String, Object>();
        actionV.put("action", action);
        taskService.complete(key, actionV);
        return "success";
    }

    @GetMapping("/testGetUsers")
    public List<LdapHelper.MemberAD> getLdap(@RequestParam(value = "search") String search, @RequestParam(value = "limit") int     limit) throws NamingException {
        String returnedAttrs[] = {"cn", "member", "name", "memberOf","mail"};
        String searchFilter = search;

        return ldapHelper.getGroupUsers(
            "ldap://10.4.5.1:389","golfdigg@thailandelite.co.th","@reenju1ce",
            "DC=thailandelite,DC=co,DC=th", searchFilter, returnedAttrs, limit); }

    @GetMapping("/auth")
    public String auth(@RequestParam(value = "user") String user, @RequestParam(value = "password") String  password) throws NamingException {

        return ldapHelper.auth(
            "ldap://10.4.5.1:389", user, password);
    }

    @GetMapping("/initCases")
    public void initCases() throws Exception {
        InitialSetupMigration initialSetupMigration = new InitialSetupMigration();
        initialSetupMigration.addCaseProspectCaseTemplate(mongoTemplate);
    }

    @GetMapping("/createJAFile")
    public void createJAFile(String fileName) throws Exception {
        jaService.generatePdf(new Context(), null, fileName,"test");
    }

    @GetMapping(value = "/welcome", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String welcomeAsHTML(String id) throws IOException {
        log.debug("Welcome {}", id);
        Booking booking = bookingService.getBookingDetailById(id);
        log.debug("Booking {}", booking);

        SPABookingEmail context = new SPABookingEmail();
        Member member = booking.getRequestMember();

        context.setTitle(member.getTitle().getName());
        context.setFirstname(member.getGivenName());
        context.setLastname(member.getSurName());
        context.setBookingNumber(booking.getNo());

        BookingServiceMember item = booking.getBookingServiceMembers().get(0);

        context.setDuteDate(item.getStartTime().format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
        context.setDueTime(item.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        context.setProvince(item.getShopName());
        context.setPersons(booking.getBookingServiceMembers().size());
        context.setTotal(1000);

        List<SPABookingItem> spaBookingItemList = booking.getBookingServiceMembers().stream().map(bsm -> {
            SPABookingItem spaBookingItem = new SPABookingItem();
            spaBookingItem.setMemberName(bsm.getMemberName());
            spaBookingItem.setIdperson(bsm.getMemberCardId());
            spaBookingItem.setEmailperson(bsm.getMemberEmail());
            spaBookingItem.setCosttbd("-");

            return spaBookingItem;
        }).collect(Collectors.toList());

        context.setPersonList(spaBookingItemList);

        log.debug("Context {}", context);

        String html = FileUtils.readFileToString(new File("template.html"));

        Handlebars hb = new Handlebars();
        Template template = hb.compileInline(html);
        String htmlOutput = template.apply(context);

        return htmlOutput;
    }

    @Data
    public static class SPABookingEmail{
        String title;
        String firstname;
        String lastname;
        String bookingNumber;
        String duteDate;
        String dueTime;
        String province;
        Integer persons;

        Integer total;

        List<SPABookingItem> personList = new ArrayList<>();
    }

    @Data
    public static class SPABookingItem {
        String memberName;
        String idperson;
        String emailperson;
        String costtbd;
    }

}
