package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.agent.AgentStatus;
import com.thailandelite.mis.be.repository.AgentStatusRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link AgentStatus}.
 */
@RestController
@RequestMapping("/api")
public class AgentStatusResource {

    private final Logger log = LoggerFactory.getLogger(AgentStatusResource.class);

    private static final String ENTITY_NAME = "agentStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentStatusRepository agentStatusRepository;

    public AgentStatusResource(AgentStatusRepository agentStatusRepository) {
        this.agentStatusRepository = agentStatusRepository;
    }

    /**
     * {@code POST  /agent-statuses} : Create a new agentStatus.
     *
     * @param agentStatus the agentStatus to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentStatus, or with status {@code 400 (Bad Request)} if the agentStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-statuses")
    public ResponseEntity<AgentStatus> createAgentStatus(@RequestBody AgentStatus agentStatus) throws URISyntaxException {
        log.debug("REST request to save AgentStatus : {}", agentStatus);
        if (agentStatus.getId() != null) {
            throw new BadRequestAlertException("A new agentStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentStatus result = agentStatusRepository.save(agentStatus);
        return ResponseEntity.created(new URI("/api/agent-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-statuses} : Updates an existing agentStatus.
     *
     * @param agentStatus the agentStatus to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentStatus,
     * or with status {@code 400 (Bad Request)} if the agentStatus is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentStatus couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-statuses")
    public ResponseEntity<AgentStatus> updateAgentStatus(@RequestBody AgentStatus agentStatus) throws URISyntaxException {
        log.debug("REST request to update AgentStatus : {}", agentStatus);
        if (agentStatus.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentStatus result = agentStatusRepository.save(agentStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, agentStatus.getId()))
            .body(result);
    }

    /**
     * {@code GET  /agent-statuses} : get all the agentStatuses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentStatuses in body.
     */
    @GetMapping("/agent-statuses")
    public List<AgentStatus> getAllAgentStatuses() {
        log.debug("REST request to get all AgentStatuses");
        return agentStatusRepository.findAll();
    }

    /**
     * {@code GET  /agent-statuses/:id} : get the "id" agentStatus.
     *
     * @param id the id of the agentStatus to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentStatus, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-statuses/{id}")
    public ResponseEntity<AgentStatus> getAgentStatus(@PathVariable String id) {
        log.debug("REST request to get AgentStatus : {}", id);
        Optional<AgentStatus> agentStatus = agentStatusRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentStatus);
    }

    /**
     * {@code DELETE  /agent-statuses/:id} : delete the "id" agentStatus.
     *
     * @param id the id of the agentStatus to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-statuses/{id}")
    public ResponseEntity<Void> deleteAgentStatus(@PathVariable String id) {
        log.debug("REST request to delete AgentStatus : {}", id);
        agentStatusRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
