package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.Title;
import com.thailandelite.mis.be.repository.TitleRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class TitleResource {

    private final Logger log = LoggerFactory.getLogger(TitleResource.class);

    private static final String ENTITY_NAME = "title";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TitleRepository titleRepository;

    public TitleResource(TitleRepository titleRepository) {
        this.titleRepository = titleRepository;
    }


    @PostMapping("/titles")
    public ResponseEntity<Title> createTitle(@RequestBody Title title) throws URISyntaxException {
        log.debug("REST request to save Title : {}", title);
        if (title.getId() != null) {
            throw new BadRequestAlertException("A new title cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Title result = titleRepository.save(title);
        return ResponseEntity.created(new URI("/api/titles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/titles")
    public ResponseEntity<Title> updateTitle(@RequestBody Title title) throws URISyntaxException {
        log.debug("REST request to update Title : {}", title);
        if (title.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Title result = titleRepository.save(title);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, title.getId()))
            .body(result);
    }


    @GetMapping("/titles")
    public List<Title> getAllTitles(Boolean publicStatus) {
        log.debug("REST request to get all Titles: publicStatus={}", publicStatus);
        if(publicStatus != null){
            return titleRepository.findAllByPublicStatus(publicStatus);
        }else {
            return titleRepository.findAll();
        }
    }


    @GetMapping("/titles/{id}")
    public ResponseEntity<Title> getTitle(@PathVariable String id) {
        log.debug("REST request to get Title : {}", id);
        Optional<Title> title = titleRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(title);
    }


    @DeleteMapping("/titles/{id}")
    public ResponseEntity<Void> deleteTitle(@PathVariable String id) {
        log.debug("REST request to delete Title : {}", id);
        titleRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
