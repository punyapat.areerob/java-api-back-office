package com.thailandelite.mis.be.web.rest.test;

import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.service.EmailService;
import com.thailandelite.mis.be.service.HandlerException;
import com.thailandelite.mis.be.service.MemberService;
import com.thailandelite.mis.be.service.model.MembershipInfo;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import io.easycm.framework.form.generator.FormlyFieldConfig;
import io.github.jhipster.web.util.HeaderUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/test/email")
@RequiredArgsConstructor
public class EmailTestResource {
    private final MemberService memberService;

    private final EmailService emailService;

    @PostMapping("/send")
    public ResponseEntity<String> sendEmail(String email, String templateName) {
        try {
            Map<String, Object> vars = new HashMap<>();
            vars.put("firstName", "John");
            vars.put("middleName", "-");
            vars.put("lastName", "Vich");
            vars.put("textColor", "#000");
//            String templateName = "SubmitApplicationForm";

            emailService.sendEmailTemplate(email, "", templateName, vars);
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
        return ResponseEntity.ok().body("Success");
    }



}
