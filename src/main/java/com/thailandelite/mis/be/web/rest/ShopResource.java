package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.Shop;
import com.thailandelite.mis.be.repository.ShopRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ShopResource {

    private final Logger log = LoggerFactory.getLogger(ShopResource.class);
    private static final String ENTITY_NAME = "shop";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final ShopRepository shopRepository;

    @PostMapping("/shops")
    public ResponseEntity<Shop> createShop(@RequestBody Shop shop) throws URISyntaxException {
        log.debug("REST request to save Shop : {}", shop);
        if (shop.getId() != null) {
            throw new BadRequestAlertException("A new shop cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Shop result = shopRepository.save(shop);
        return ResponseEntity.created(new URI("/api/shops/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/shops")
    public ResponseEntity<Shop> updateShop(@RequestBody Shop shop) throws URISyntaxException {
        log.debug("REST request to update Shop : {}", shop);
        if (shop.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Shop result = shopRepository.save(shop);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shop.getId()))
            .body(result);
    }

//    @PutMapping("/shops/{id}/contactIds")
//    public ResponseEntity<Shop> updateVendorContact(@PathVariable String id, @RequestBody Set<String> contactIds) throws URISyntaxException {
//        log.debug("REST request to update VendorContact in Shop: {}", id);
//        List<VendorsContact> vendorsContacts = (List<VendorsContact>) vendorsContactRepository.findAllById(contactIds);
//        Set vendorsContactsSet = new HashSet(vendorsContacts);
//        Shop shop = shopRepository.findById(id).get();
//        shop.setContacts(vendorsContactsSet);
//        shop = shopRepository.save(shop);
//
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shop.getId()))
//            .body(shop);
//    }


    @GetMapping("/shops")
    public ResponseEntity<List<Shop>> getAllShops(Pageable pageable) {
        log.debug("REST request to get all Shops");
        Page<Shop> page = shopRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/shops/vendor/{id}")
    public  ResponseEntity<List<Shop>> getProductByVendor(@PathVariable String id) {
        List<Shop> product =shopRepository.findByVendorId(id);
        return ResponseEntity.ok().body(product);
    }


    @GetMapping("/shops/{id}")
    public ResponseEntity<Shop> getShop(@PathVariable String id) {
        log.debug("REST request to get Shop : {}", id);
        Optional<Shop> shop = shopRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(shop);
    }

    @GetMapping("/shops-by-vendorId/{vendorId}")
    public ResponseEntity<List<Shop>> getShopsByVendorId(@PathVariable String vendorId) {
        log.debug("REST request to get Shops by VendorId : {}", vendorId);
        List<Shop> shops = shopRepository.findByVendorId(vendorId);
        return ResponseEntity.ok(shops);
    }


    @DeleteMapping("/shops/{id}")
    public ResponseEntity<Void> deleteShop(@PathVariable String id) {
        log.debug("REST request to delete Shop : {}", id);
        shopRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
