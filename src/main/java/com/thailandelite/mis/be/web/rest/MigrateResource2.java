package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.service.migrate.MigrateAgentService;
import com.thailandelite.mis.be.service.migrate.MigrateCardService2;
import com.thailandelite.mis.be.service.migrate.MigrateMemberService;
import com.thailandelite.mis.be.service.migrate.MigrateMemberService2;
import com.thailandelite.mis.model.domain.Member;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Profile("migrate")
@Slf4j
@RestController
@RequestMapping("/api/migrate2")
@RequiredArgsConstructor
public class MigrateResource2 {
    private final MigrateCardService2 migrateCardService2;
    private final MigrateAgentService migrateAgentService;
    private final MigrateMemberService2 migrateMemberService2;
    private final MigrateMemberService migrateMemberService;

    public enum CMD {
        MasPrivilegeGroup, MasPrivilege, PHY_CARD, CARD, CARD_PRIVILEGE
    }

    @PostMapping("/migrate/package")
    public long migratePackage(@RequestParam CMD cmd){
        switch (cmd) {
            case MasPrivilegeGroup:
                return migrateCardService2.privilegeGroup();
            case MasPrivilege:
                return migrateCardService2.privilege();
            case PHY_CARD:
                return migrateCardService2.masCard();
            case CARD:
                return migrateCardService2.card();
            case CARD_PRIVILEGE:
                return migrateCardService2.cardPrivilege();
        }
        return -1;
    }

    public enum AgentCMD {
        Agent, AgentType, AgentCommissionRate, AgentTerritory, AgentContract, CommissionGroupIssue, CommissionIssue, CommissionIssueItem, AgentStatus, AgentStatusGroup
    }

    @PostMapping("/migrate/agent")
    public long migrateAgent(@RequestParam AgentCMD cmd) {
        switch (cmd) {
        case AgentType:
            return migrateAgentService.agentType();
        case AgentCommissionRate:
            return migrateAgentService.agentCommissionRate();
        case Agent:
            return migrateAgentService.agent();
        case AgentTerritory:
            return migrateAgentService.agentTerritory();
        case AgentContract:
            return migrateAgentService.agentContract();
        case CommissionGroupIssue:
            return migrateAgentService.commissionGroupIssue();
        case CommissionIssue:
            return migrateAgentService.commissionIssue();
        case CommissionIssueItem:
            return migrateAgentService.commissionIssueItem();
        case AgentStatus:
            return migrateAgentService.agentStatus();
        case AgentStatusGroup:
            return migrateAgentService.agentStatusGroup();
        }
        return -1;
    }

    public enum MemberCMD {
        MemberPrivilege, Passport, Member
    }

    @PostMapping("/migrate/member")
    public long migrateMember(@RequestParam MemberCMD cmd, Pageable page) {
        switch (cmd) {
            case MemberPrivilege:
                return migrateMemberService2.memberPrivilege();
            case Passport:
                return migrateMemberService2.passport();
            case Member:
                return migrateMemberService.member(page);
        }
        return -1;
    }
}
