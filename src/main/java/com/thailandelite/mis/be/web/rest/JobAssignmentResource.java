package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.JobAssignment;
import com.thailandelite.mis.be.repository.JobAssignmentRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class JobAssignmentResource {

    private final Logger log = LoggerFactory.getLogger(JobAssignmentResource.class);

    private static final String ENTITY_NAME = "jobAssignment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JobAssignmentRepository jobAssignmentRepository;

    public JobAssignmentResource(JobAssignmentRepository jobAssignmentRepository) {
        this.jobAssignmentRepository = jobAssignmentRepository;
    }


    @PostMapping("/job-assignments")
    public ResponseEntity<JobAssignment> createJobAssignment(@RequestBody JobAssignment jobAssignment) throws URISyntaxException {
        log.debug("REST request to save JobAssignment : {}", jobAssignment);
        if (jobAssignment.getId() != null) {
            throw new BadRequestAlertException("A new jobAssignment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JobAssignment result = jobAssignmentRepository.save(jobAssignment);
        return ResponseEntity.created(new URI("/api/job-assignments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/job-assignments")
    public ResponseEntity<JobAssignment> updateJobAssignment(@RequestBody JobAssignment jobAssignment) throws URISyntaxException {
        log.debug("REST request to update JobAssignment : {}", jobAssignment);
        if (jobAssignment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JobAssignment result = jobAssignmentRepository.save(jobAssignment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jobAssignment.getId()))
            .body(result);
    }


    @GetMapping("/job-assignments")
    public List<JobAssignment> getAllJobAssignments() {
        log.debug("REST request to get all JobAssignments");
        return jobAssignmentRepository.findAll();
    }


    @GetMapping("/job-assignments/{id}")
    public ResponseEntity<JobAssignment> getJobAssignment(@PathVariable String id) {
        log.debug("REST request to get JobAssignment : {}", id);
        Optional<JobAssignment> jobAssignment = jobAssignmentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jobAssignment);
    }


    @DeleteMapping("/job-assignments/{id}")
    public ResponseEntity<Void> deleteJobAssignment(@PathVariable String id) {
        log.debug("REST request to delete JobAssignment : {}", id);
        jobAssignmentRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
