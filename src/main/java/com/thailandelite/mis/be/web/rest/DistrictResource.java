package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.master.District;
import com.thailandelite.mis.be.repository.DistrictRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;


@RestController
@RequestMapping("/api")
public class DistrictResource {

    private final Logger log = LoggerFactory.getLogger(DistrictResource.class);

    private static final String ENTITY_NAME = "district";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DistrictRepository districtRepository;

    public DistrictResource(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;
    }


    @PostMapping("/districts")
    public ResponseEntity<District> createDistrict(@RequestBody District district) throws URISyntaxException {
        log.debug("REST request to save District : {}", district);
        if (district.getId() != null) {
            throw new BadRequestAlertException("A new district cannot already have an ID", ENTITY_NAME, "idexists");
        }
        District result = districtRepository.save(district);
        return ResponseEntity.created(new URI("/api/districts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/districts")
    public ResponseEntity<District> updateDistrict(@RequestBody District district) throws URISyntaxException {
        log.debug("REST request to update District : {}", district);
        if (district.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        District result = districtRepository.save(district);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, district.getId()))
            .body(result);
    }


    @GetMapping("/districts")
    public List<District> getAllDistricts(String provinceID) {
        log.debug("REST request to get all Districts: provinceID={}", provinceID);
        if(hasText(provinceID)){
            return districtRepository.findAllByProvinceID(provinceID, Sort.by("nameEN"));
        }else{
            return districtRepository.findAll();
        }
    }


    @GetMapping("/districts/{id}")
    public ResponseEntity<District> getDistrict(@PathVariable String id) {
        log.debug("REST request to get District : {}", id);
        Optional<District> district = districtRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(district);
    }


    @DeleteMapping("/districts/{id}")
    public ResponseEntity<Void> deleteDistrict(@PathVariable String id) {
        log.debug("REST request to delete District : {}", id);
        districtRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
