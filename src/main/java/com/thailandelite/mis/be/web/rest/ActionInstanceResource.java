package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.ActionInstance;
import com.thailandelite.mis.be.repository.ActionInstanceRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ActionInstanceResource {

    private final Logger log = LoggerFactory.getLogger(ActionInstanceResource.class);

    private static final String ENTITY_NAME = "actionInstance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ActionInstanceRepository actionInstanceRepository;

    public ActionInstanceResource(ActionInstanceRepository actionInstanceRepository) {
        this.actionInstanceRepository = actionInstanceRepository;
    }


    @PostMapping("/action-instances")
    public ResponseEntity<ActionInstance> createActionInstance(@RequestBody ActionInstance actionInstance) throws URISyntaxException {
        log.debug("REST request to save ActionInstance : {}", actionInstance);
        if (actionInstance.getId() != null) {
            throw new BadRequestAlertException("A new actionInstance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ActionInstance result = actionInstanceRepository.save(actionInstance);
        return ResponseEntity.created(new URI("/api/action-instances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/action-instances")
    public ResponseEntity<ActionInstance> updateActionInstance(@RequestBody ActionInstance actionInstance) throws URISyntaxException {
        log.debug("REST request to update ActionInstance : {}", actionInstance);
        if (actionInstance.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ActionInstance result = actionInstanceRepository.save(actionInstance);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, actionInstance.getId()))
            .body(result);
    }


    @GetMapping("/action-instances")
    public List<ActionInstance> getAllActionInstances() {
        log.debug("REST request to get all ActionInstances");
        return actionInstanceRepository.findAll();
    }


    @GetMapping("/action-instances/{id}")
    public ResponseEntity<ActionInstance> getActionInstance(@PathVariable String id) {
        log.debug("REST request to get ActionInstance : {}", id);
        Optional<ActionInstance> actionInstance = actionInstanceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(actionInstance);
    }


    @DeleteMapping("/action-instances/{id}")
    public ResponseEntity<Void> deleteActionInstance(@PathVariable String id) {
        log.debug("REST request to delete ActionInstance : {}", id);
        actionInstanceRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
