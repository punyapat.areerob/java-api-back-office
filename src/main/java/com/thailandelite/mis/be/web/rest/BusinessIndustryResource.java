package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.model.domain.BusinessIndustry;
import com.thailandelite.mis.be.repository.BusinessIndustryRepository;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class BusinessIndustryResource {

    private final Logger log = LoggerFactory.getLogger(BusinessIndustryResource.class);

    private static final String ENTITY_NAME = "businessIndustry";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BusinessIndustryRepository businessIndustryRepository;

    public BusinessIndustryResource(BusinessIndustryRepository businessIndustryRepository) {
        this.businessIndustryRepository = businessIndustryRepository;
    }


    @PostMapping("/business-industries")
    public ResponseEntity<BusinessIndustry> createBusinessIndustry(@RequestBody BusinessIndustry businessIndustry) throws URISyntaxException {
        log.debug("REST request to save BusinessIndustry : {}", businessIndustry);
        if (businessIndustry.getId() != null) {
            throw new BadRequestAlertException("A new businessIndustry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BusinessIndustry result = businessIndustryRepository.save(businessIndustry);
        return ResponseEntity.created(new URI("/api/business-industries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }


    @PutMapping("/business-industries")
    public ResponseEntity<BusinessIndustry> updateBusinessIndustry(@RequestBody BusinessIndustry businessIndustry) throws URISyntaxException {
        log.debug("REST request to update BusinessIndustry : {}", businessIndustry);
        if (businessIndustry.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BusinessIndustry result = businessIndustryRepository.save(businessIndustry);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, businessIndustry.getId()))
            .body(result);
    }


    @GetMapping("/business-industries")
    public List<BusinessIndustry> getAllBusinessIndustries() {
        log.debug("REST request to get all BusinessIndustries");
        return businessIndustryRepository.findAll();
    }


    @GetMapping("/business-industries/{id}")
    public ResponseEntity<BusinessIndustry> getBusinessIndustry(@PathVariable String id) {
        log.debug("REST request to get BusinessIndustry : {}", id);
        Optional<BusinessIndustry> businessIndustry = businessIndustryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(businessIndustry);
    }


    @DeleteMapping("/business-industries/{id}")
    public ResponseEntity<Void> deleteBusinessIndustry(@PathVariable String id) {
        log.debug("REST request to delete BusinessIndustry : {}", id);
        businessIndustryRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
