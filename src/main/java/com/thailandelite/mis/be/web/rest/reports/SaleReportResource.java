package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.SaleReportService;
import com.thailandelite.mis.model.FlowResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
public class SaleReportResource {

    private final SaleReportService reportService;

    public SaleReportResource(SaleReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/reports/payment-info")
    public ResponseEntity<FlowResponse<Page<PaymentInfoResponse>>> getPaymentInfoReports(PaymentInfoRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getPaymentInfo(searchRequest,pageable) ));
    }

    @GetMapping("/reports/payment-info/excel")
    public byte[] getPaymentInfoExcel(PaymentInfoRequest searchRequest, Pageable pageable) {
        return reportService.generateExcelPaymentInfoReport(searchRequest);
    }


    @GetMapping("/reports/revenues/agent")
    public ResponseEntity<FlowResponse<Page<RevenueReportResponse>>> getRevenueGroupByAgent(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getRevenueGroupByAgent(searchRequest, pageable) ));
    }

    @GetMapping("/reports/revenues/agent/excel")
    public byte[] getRevenueGroupByAgentExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return reportService.generateExcelRevenueGroupByAgent(searchRequest);
    }

    @GetMapping("/reports/revenues/package")
    public ResponseEntity<FlowResponse<Page<RevenueReportResponse>>> getRevenueGroupByPackage(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getRevenueGroupByPackage(searchRequest, pageable) ));
    }

    @GetMapping("/reports/revenues/package/excel")
    public byte[] getRevenueGroupByPackageExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return reportService.generateExcelRevenueGroupByPackage(searchRequest);
    }
}
