package com.thailandelite.mis.be.web.rest.reports;

import com.thailandelite.mis.be.service.dto.reports.ApplicationReportResponse;
import com.thailandelite.mis.be.service.dto.reports.BaseReportRequest;
import com.thailandelite.mis.be.service.dto.reports.CountReportResponse;
import com.thailandelite.mis.be.service.dto.reports.JobApplicationReportResponse;
import com.thailandelite.mis.be.service.report.ApplicationReportService;
import com.thailandelite.mis.be.service.report.JobApplicationReportService;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.JobApplication;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class JobApplicationReportResource {

    private final JobApplicationReportService reportService;

    @GetMapping("/reports/job-application")
    public ResponseEntity<FlowResponse<Page<JobApplicationReportResponse>>> getApplicationReport(BaseReportRequest searchRequest, Pageable pageable) {
        return ResponseEntity.ok(new FlowResponse<>("", 0, reportService.getJobApplicationReport(searchRequest, pageable) ));
    }

    @GetMapping("/reports/job-application/excel")
    public byte[] getApplicationReportExcel(BaseReportRequest searchRequest, Pageable pageable) {
        return  reportService.getJobApplicationExcel(searchRequest, pageable);
    }


}
