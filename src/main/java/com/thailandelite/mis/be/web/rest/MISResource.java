package com.thailandelite.mis.be.web.rest;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Verify;
import com.thailandelite.mis.be.service.migrate.MasterMigrateService;
import com.thailandelite.mis.be.service.migrate.MigrateCardService;
import com.thailandelite.mis.migrate.master.*;
import com.thailandelite.mis.migrate.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;


@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MISResource {
    private static final String ENTITY_NAME = "member";

    public enum MasterTable {
        TITLE, BLOOD, RELIGION, NATIONALITIES, OCCUPATION, BUSINESSINDUSTRY,
        ACCOMMODATIONTYPE, COUNTRY, CITY, PROVINCE, DISTRICT, SUBDISTRICT, RELATIONSHIP, STATE
    }
    public enum VendorTable {
        CONTRACT , BILLINFO, SERVICE
    }
    public enum PrivilegeTable {
        PRIVILEGE
    }
    private final MasterMigrateService masterMigrateService;
    private final MigrateCardService migrateCardService;


    @PostMapping("/migrate/privilege")
    public void migrateMaster(PrivilegeTable table, MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        switch (table) {
            case PRIVILEGE:
                migrateCardService.privilege(readFile(fis, new TypeReference<List<PrivilegeDB>>() {}));
                break;
        }
    }

    @PostMapping("/migrate/master")
    public void migrateMaster(MasterTable table, MultipartFile file, String filePath) throws IOException {
        InputStream fis = null;
        if(file != null){
            fis = file.getInputStream();
        }else {
            Verify.verify(hasText(filePath),"No source fis");
            fis = new FileInputStream(filePath);
        }

        switch (table) {
            case TITLE:
                masterMigrateService.title(readFile(fis, new TypeReference<List<TitleDB>>() {}));
                break;
            case BLOOD:
                masterMigrateService.bloodType(readFile(fis, new TypeReference<List<BloodTypeDB>>() {}));
                break;
            case RELIGION:
                masterMigrateService.religion(readFile(fis, new TypeReference<List<ReligionDB>>() {} ));
                break;
            case NATIONALITIES:
                masterMigrateService.nationalities(readFile(fis, new TypeReference<List<NationalitiesDB>>() {} ));
                break;
            case OCCUPATION:
                masterMigrateService.occupation(readFile(fis, new TypeReference<List<OccupationDB>>() {} ));
                break;
            case BUSINESSINDUSTRY:
                masterMigrateService.businessIndustry(readFile(fis, new TypeReference<List<BusinessIndustryDB>>() {} ));
                break;
            case ACCOMMODATIONTYPE:
                masterMigrateService.accommodation(readFile(fis, new TypeReference<List<AccommodationTypeDB>>() {}));
                break;
            case COUNTRY:
                masterMigrateService.country(readFile(fis, new TypeReference<List<CountryDB>>() {}));
                break;
            case CITY:
                masterMigrateService.city(fis);
                break;
            case PROVINCE:
                masterMigrateService.province(readFile(fis, new TypeReference<List<ProvinceDB>>() {} ));
                break;
            case DISTRICT:
                masterMigrateService.district(readFile(fis, new TypeReference<List<DistrictDB>>() {} ));
                break;
            case SUBDISTRICT:
                masterMigrateService.subDistrict(readFile(fis, new TypeReference<List<SubDistrictDB>>() {} ));
                break;
            case RELATIONSHIP:
                masterMigrateService.relationship(readFile(fis, new TypeReference<List<RelationshipDB>>() {} ));
            case STATE:
                masterMigrateService.state(readFile(fis, new TypeReference<List<StateDB>>() {} ));
                break;
        }
    }

    public static <T> T readFile(MultipartFile file, TypeReference<T> valueTypeRef) throws IOException {
        return MISResource.readFile(file.getInputStream(), valueTypeRef);
    }
    public static <T> T readFile(InputStream src, TypeReference<T> valueTypeRef) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(src, valueTypeRef);
    }

    public static void readFileLargeFile(InputStream src, OnItem callback) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonParser parser = mapper.getFactory().createParser(src);
        if(parser.nextToken() != JsonToken.START_ARRAY) {
            throw new IllegalStateException("Expected an array");
        }
        while(parser.nextToken() == JsonToken.START_OBJECT) {


            callback.onItem(mapper, parser);

        }

        parser.close();
    }

    public interface OnItem {
        void onItem(ObjectMapper mapper, JsonParser parser) throws IOException;
    }












}
