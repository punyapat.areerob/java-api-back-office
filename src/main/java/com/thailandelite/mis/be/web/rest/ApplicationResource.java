package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.ApplicationService;
import com.thailandelite.mis.be.service.EmailService;
import com.thailandelite.mis.be.service.NotificationService;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.service.flow.BPApplicationService;
import com.thailandelite.mis.be.service.flow.BPTransferMemberService;
import com.thailandelite.mis.be.service.flow.BPTypeChangeService;
import com.thailandelite.mis.be.service.flow.BPUpgradeMemberService;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;

import com.thailandelite.mis.be.web.rest.flows.*;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.application.*;
import com.thailandelite.mis.model.domain.enumeration.ApplicationStatus;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.domain.enumeration.TransferPackageRequestStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZonedDateTime;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.util.StringUtils.hasText;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApplicationResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationResource.class);

    private static final String ENTITY_NAME = "application";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationFlowResource applicationFlowResource;
    private final ApplicationRiskFlowResource applicationRiskFlowResource;
    private final TransferMembershipResource transferMembershipResource;
    private final TypeChangeMemberResource typeChangeMemberResource;
    private final UpgradeMemberResource upgradeMemberResource;
    private final ApplicationAgentFlowResource applicationAgentFlowResource;

    private final ApplicationRepository applicationRepository;
    private final ApplicationService applicationService;
    private final CardRepository cardRepository;
    private final MemberRepository memberRepository;

    private final TransferPackageRequestRepository transferPackageRequestRepository;
    private final NotificationService notificationService;
    private final MongoTemplate mongoTemplate;

    @PostMapping("/applications")
    public ResponseEntity<Application> createApplication(@RequestBody Application application) throws URISyntaxException, IOException {
        log.debug("REST request to save Application : {}", application);
        if (application.getId() != null) {
            throw new BadRequestAlertException("A new application cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Application result = applicationRepository.save(application);
        Member member = memberRepository.findById(result.getMemberId()).get();
        notificationService.sendAccountRegistration(member.getEmail(),member.getGivenName(), member.getMiddleName(), member.getSurName());
        return ResponseEntity.created(new URI("/api/applications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PostMapping("/applications/register")
    public ResponseEntity<Application> createApplicationByRegister(@RequestBody CreateApplicationReq req) throws URISyntaxException, IOException {
        log.debug("REST request to create Register Application : {}", req);
        Application result;
        result = applicationService.createRegisterApplication(req);
        return ResponseEntity.created(new URI("/api/applications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    @PutMapping("/applications")
    public ResponseEntity<Application> updateApplication(@RequestBody Application application) throws URISyntaxException {
        log.debug("REST request to update Application : {}", application);
        if (application.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Application result = applicationRepository.save(application);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, application.getId()))
            .body(result);
    }

    @GetMapping("/applications")
    public ResponseEntity<FlowResponse<Page<Application>>> getAllApplications(@RequestParam(defaultValue = "") String packageAction , SearchRequest searchRequest, Pageable pageable) {
        log.debug("REST request to get all Applications");
        boolean getAll = true;
        Query query = new Query().with(pageable);

        if(searchRequest.getName() != null && searchRequest.getName() != ""){
            getAll = false;
            query.addCriteria(where("userProfileForm.givenName").is(searchRequest.getName()));
        }
        if(searchRequest.getSurname() != null && searchRequest.getSurname() != ""){
            getAll = false;
            query.addCriteria(where("userProfileForm.surName").is(searchRequest.getSurname()));
        }
        if (searchRequest.getFrom()!=null && searchRequest.getTo()!=null  ) {
            getAll = false;
            query.addCriteria(where("last_modified_date").gt(Date.from(searchRequest.getFrom())).lt(Date.from(searchRequest.getTo())));
        }
        else if (searchRequest.getFrom()!=null ) {
            getAll = false;
            query.addCriteria(where("last_modified_date").gt(Date.from(searchRequest.getFrom())));
        }
        else if (searchRequest.getTo()!=null) {
            getAll = false;
            query.addCriteria(where("last_modified_date").lt(Date.from(searchRequest.getTo())));
        }

        if (searchRequest.getAgent()!=null) {
            getAll = false;
            query.addCriteria(where("registerAgentId").exists(true));
        }


        if(getAll) {
            if (packageAction.isEmpty()) {
                return ResponseEntity.ok(new FlowResponse<>("", 0, applicationRepository.findAll(pageable)));
            } else
                return ResponseEntity.ok(new FlowResponse<>("", 0, applicationRepository.findAllByPackageAction(packageAction, pageable)));
        }
        else
        {
           return  ResponseEntity.ok(new FlowResponse<>("", 0,  PageableExecutionUtils.getPage(mongoTemplate.find(query, Application.class),
            pageable,
            () -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), Application.class))));
        }
    }

    @GetMapping("/applications/{id}")
    public ResponseEntity<FlowResponse<Application>> getApplication(@PathVariable String id) {
        log.debug("REST request to get Application : {}", id);
        Application application = applicationRepository.findById(id).orElseThrow(()->new NotFoundException("Not found application"));
        return ResponseEntity.ok(new FlowResponse<>("", 0,application));
    }

    @GetMapping("/applications/{id}/active")
    public List<Application> getActiveApplication(@PathVariable String id) {
        log.debug("REST request to get Application : {}", id);
        Application activeApplication = applicationRepository.findById(id).orElseThrow(()-> new NotFoundException("Not found application form"));
        List<Application> activeApplications = setAdditionalApplication(activeApplication);
        return activeApplications;
    }

    @GetMapping("/applications/member/{id}")
    public List<Application> getMemberApplication(@PathVariable String id) {
        log.debug("REST request to get Application member: {}", id);
        return applicationRepository.findByMemberId(id);
    }

    @GetMapping("/applications/application-ref/{id}")
    public List<Application> getAdditionalApplication(@PathVariable String applicationId) {
        log.debug("REST request to get Ref Application: {}", applicationId);
        return applicationRepository.findByRefApplicationId(applicationId);
    }

    @GetMapping("/applications/member/{id}/active")
    public List<Application> getMemberActiveApplication(@PathVariable String id) {
        log.debug("REST request to get Application member: {}", id);
        List<Application> applications = applicationRepository.findByMemberId(id, Sort.by("created_date"));
        if(applications == null || applications.size() <= 0 )
            throw new NotFoundException("Not found application form.");
        Application activeApplication = applications.get(0);
        List<Application> activeApplications = setAdditionalApplication(activeApplication);

        return activeApplications;
    }

    private List<Application> setAdditionalApplication(Application activeApplication) {
        List<Application> activeApplications = new ArrayList<>();
        activeApplications.add(activeApplication);

        List<Application> additionalApplications = applicationRepository.findByRefApplicationId(activeApplication.getId());
        if (additionalApplications != null && additionalApplications.size() > 0)
            activeApplications.addAll(additionalApplications);

        return activeApplications;
    }

    @GetMapping("/applications/agent/{id}")
    public List<Application> getAgentApplication(@PathVariable String id) {
        log.debug("REST request to get Agent member application: {}", id);
        return applicationRepository.findByRegisterAgentId(id);
    }

    @DeleteMapping("/applications/{id}")
    public ResponseEntity<Void> deleteApplication(@PathVariable String id) {
        log.debug("REST request to delete Application : {}", id);
        applicationRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @PostMapping("/applications/{id}/package/{packageId}")
    public ResponseEntity<Application> changeApplicationPackage(@PathVariable String id,
                                                                @PathVariable String packageId) {
        Application application = applicationRepository.findById(id).orElseThrow(() -> new NotFoundException("Application not found"));
        application.setPackageId(packageId);
        Card card = cardRepository.findById(packageId).orElseThrow(() -> new NotFoundException("Card package not found"));
        application.setCard(card);
        applicationRepository.save(application);
        return ResponseEntity.ok(application);
    }

    @GetMapping("/applications/{id}/documents")
    public ResponseEntity<DocumentForm> getMemberApplicationDocuments(@PathVariable String id) {
        Application application =  applicationRepository.findById(id).orElseThrow(()->new NotFoundException("Not found application."));
        return ResponseEntity.ok().body(application.getDocumentForm());
    }

    @GetMapping("/applicants/e-doc")
    public ResponseEntity<EDocId> genEDoc() {
        return ResponseEntity.ok(new EDocId("TPC:GR" + ZonedDateTime.now().getYear() + "/" + Instant.now().getEpochSecond()));
    }

    @PostMapping("/applicants/{id}/uploads/{type}")
    public ResponseEntity<FileURI> uploadFile(@PathVariable String id, @PathVariable String type, @RequestParam("file") MultipartFile file) throws XPathExpressionException, ParserConfigurationException, UploadFailException {
        return ResponseEntity.ok(new FileURI(applicationService.uploadFile(id, type, file)));
    }

    @PostMapping("/applicants/submit")
    public ResponseEntity<FlowResponse<Application>> submitApplication(@RequestBody Application application) {
        log.debug("REST request to submitApplication : {}, {}", application.getPackageAction(), application);
        Member member = memberRepository.findById(application.getMemberId()).orElseThrow(()->new NotFoundException("Not found member profile."));
        List<TransferPackageRequest> transferReq = transferPackageRequestRepository.findByToEmailAndStatus(member.getEmail(), TransferPackageRequestStatus.CREATE);
        if(transferReq != null && transferReq.size() > 0 ){
            application.setPackageAction(PackageAction.TRANSFER);
        }

        if (PackageAction.TRANSFER.equals(application.getPackageAction())) {
            return transferMembershipResource.create(application);
        }else if (PackageAction.UPGRADE.equals(application.getPackageAction())) {
            return upgradeMemberResource.create(application);
        }else if (PackageAction.TYPECHANGE.equals(application.getPackageAction())) {
            return typeChangeMemberResource.create(application);
        }else{
            if ( StringUtils.isNotBlank(application.getRegisterAgentId()) ) {
                return applicationAgentFlowResource.create(application);
                //TODO creat ApplicationAgentRiskFlowResource and put logic check RiskCountry
            }else{
                if (member.getPassport().getNationality().getRiskCountry().equals(false)) {
                    return applicationFlowResource.create(application);
                }else{
                    return applicationRiskFlowResource.create(application);
                }
            }
        }
    }

    @GetMapping("/applicants/{id}/action")
    public ResponseEntity<Application> memberCaseAction(@PathVariable String id,
                                                                      String action) {
        Application application = applicationRepository.findById(id).orElseThrow(() -> new RuntimeException("Application not found"));
        if("RESUBMIT".equalsIgnoreCase(action)){
            if (application.getPackageAction().equals(PackageAction.TRANSFER)) {
                CaseInfoDTO caseInfoDTO = transferMembershipResource.getApplicationTask(application.getCaseId()).getBody().getResult();
                transferMembershipResource.process(caseInfoDTO.getProcessInstanceId(), caseInfoDTO.getTaskDefKey(), "RESUBMIT", "");
            }else if(application.getPackageAction().equals(PackageAction.UPGRADE)) {
                CaseInfoDTO caseInfoDTO = upgradeMemberResource.getApplicationTask(application.getCaseId()).getBody().getResult();
                upgradeMemberResource.process(caseInfoDTO.getProcessInstanceId(), caseInfoDTO.getTaskDefKey(), "RESUBMIT", "");
            }else if(application.getPackageAction().equals(PackageAction.TYPECHANGE)) {
                CaseInfoDTO caseInfoDTO = typeChangeMemberResource.getApplicationTask(application.getCaseId()).getBody().getResult();
                typeChangeMemberResource.process(caseInfoDTO.getProcessInstanceId(), caseInfoDTO.getTaskDefKey(), "RESUBMIT", "");
            }else{
                CaseInfoDTO caseInfoDTO = applicationFlowResource.getCaseDetail(application.getCaseId()).getBody().getResult();
                applicationFlowResource.processTask(caseInfoDTO.getProcessInstanceId(), caseInfoDTO.getTaskDefKey(), "RESUBMIT", "");
            }

            application = applicationRepository.findById(id).orElseThrow(() -> new RuntimeException("Application not found"));
        }
        return ResponseEntity.ok(application);
    }

    @Data
    @AllArgsConstructor
    public static class EDocId {
        String id;
    }

    @Data
    @AllArgsConstructor
    public static class FileURI {
        String id;
    }

    @Data
    @AllArgsConstructor
    public static class CreateApplicationReq {
        String memberId;
        String cardId;
        Boolean isCoreMember;
        String refApplicationId;
    }
}
