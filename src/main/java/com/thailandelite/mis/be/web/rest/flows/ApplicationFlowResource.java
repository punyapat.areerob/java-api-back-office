package com.thailandelite.mis.be.web.rest.flows;


import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.be.service.flow.BPApplicationService;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


import javax.servlet.annotation.MultipartConfig;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toList;


@Slf4j
@RestController
@RequestMapping("/api/applications/flow")
@RequiredArgsConstructor
@MultipartConfig(location = "/tmp",
    fileSizeThreshold = 0,
    maxFileSize = 7242880,
    maxRequestSize = 20971520)
public class ApplicationFlowResource {

    private final BPApplicationService pbApplicationService;
    private final ApplicationRepository applicationRepository;

    public String getFlowDefinitionKey(){
        return  "applicant";
    }


    @GetMapping("/{taskDefKey}/actions")
    public Enum<?>[] getActionByTaskDefKey(@PathVariable String taskDefKey) {
        log.debug("REST request to get JobApplication : {}", taskDefKey);
        return pbApplicationService.getTaskByTaskDefKey(taskDefKey);
    }

    @PostMapping
    public ResponseEntity<FlowResponse<Application>> create(@RequestBody Application application) {
        log.debug("REST request to save createApplication : {}", application);
        pbApplicationService.create(getFlowDefinitionKey(), application);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0 , application));
    }

    private List<Sort.Order> createOrderFromSortString(String sort)
    {
        List<Sort.Order> orders = new ArrayList<Sort.Order>() ;
        if(!sort.isEmpty()) {
            List<String> sorts = Arrays.asList(sort.split(",").clone());

            for (String s : sorts) {
                String[] query = s.split("\\+");
                Sort.Order order = new Sort.Order(Sort.Direction.valueOf(query[1]), query[0]);
                orders.add(order);
            }
        }
        return orders;
    }


    @GetMapping("/{caseId}/generateDoc")
    public byte[] generateDoc(@PathVariable String caseId) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            return  pbApplicationService.generateApplicationSheet(caseId);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (UploadFailException e) {
            e.printStackTrace();
        }
        return null;
    }


    @GetMapping("/{caseId}")
    public ResponseEntity<FlowResponse<CaseInfoDTO>> getCaseDetail(@PathVariable String caseId) {
        log.debug("REST request to get JobApplication : {}", caseId);
        CaseInfoDTO caseInfoDTO = pbApplicationService.getDetail(caseId);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0, caseInfoDTO));
    }

    @GetMapping("/process/{processInstanceId}/{taskDefKey}/{action}")
    public ResponseEntity<Object> processTask(@PathVariable String processInstanceId,
                                              @PathVariable String taskDefKey,
                                              @PathVariable String action,
                                              @RequestParam String remark) {
        log.debug("REST request to get JobApplication : {}", processInstanceId);
        pbApplicationService.processTask(processInstanceId, taskDefKey, action, SecurityContextHolder.getContext().getAuthentication().getName(), remark);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(),0, "success"));
    }


    @GetMapping()
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAll(SearchRequest searchRequest,
                                                                  @RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                     @RequestParam(defaultValue = "") String status,
                                                                  Pageable pageable) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKey(getFlowDefinitionKey())
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .name(searchRequest.getName())
            .surname(searchRequest.getSurname())
            .from(searchRequest.getFrom())
            .to(searchRequest.getTo())
            .status(status).build();

        Page<CaseInfoDTO> applications = pbApplicationService.queryCaseWithEntity(caseQuery, Application.class);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(), pbApplicationService.queryUnseen(caseQuery), applications ));
    }

    @GetMapping("/gr")
    public ResponseEntity<FlowResponse<Page<CaseInfoDTO>>> getAlls(SearchRequest searchRequest,
                                                                  @RequestParam(defaultValue = "") String candidateGroup,
                                                                  @RequestParam(defaultValue = "") List<String> taskDefKey,
                                                                  @RequestParam(defaultValue = "") String status,
                                                                  Pageable pageable) {
        log.debug("REST request to get all Invoices");
        CaseQueryRequest caseQuery = CaseQueryRequest.builder(pageable)
            .flowDefKeys(new String[]{"applicant","applicant-agent"})
            .candidateGroup(candidateGroup)
            .taskDefKeys(taskDefKey)
            .name(searchRequest.getName())
            .surname(searchRequest.getSurname())
            .from(searchRequest.getFrom())
            .to(searchRequest.getTo())
            .status(status).build();

        Page<CaseInfoDTO> applications = pbApplicationService.queryCaseWithEntityMultiFlows(caseQuery, Application.class);
        return ResponseEntity.ok(new FlowResponse<>(getFlowDefinitionKey(), pbApplicationService.queryUnseen(caseQuery), applications ));
    }

}
