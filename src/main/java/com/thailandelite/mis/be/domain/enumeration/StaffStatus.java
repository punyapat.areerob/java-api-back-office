package com.thailandelite.mis.be.domain.enumeration;

/**
 * The StaffStatus enumeration.
 */
public enum StaffStatus {
    ACTIVE, PENDING, INACTIVE
}
