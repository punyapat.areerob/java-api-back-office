package com.thailandelite.mis.be.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A EmailTemplateMeta.
 */
@Document(collection = "email_template_meta")
public class EmailTemplateMeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private Map<String, String> meta;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here


    public Map<String, String> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, String> meta) {
        this.meta = meta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmailTemplateMeta)) {
            return false;
        }
        return id != null && id.equals(((EmailTemplateMeta) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmailTemplateMeta{" +
            "id=" + getId() +
            "}";
    }
}
