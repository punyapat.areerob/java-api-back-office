package com.thailandelite.mis.be.domain;
import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A ProductPriceRule.
 */
@Data
@Document(collection = "product_price_rule")
public class ProductPriceRule extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("product_id")
    private String productId;

    @Field("product_variant_key")
    private String productVariantKey;

    @Field("used_quota")
    private String usedQuota;

    @Field("price_sale")
    private String priceSale;

    @Field("price_base")
    private String priceBase;

    @Field("status")
    private String status;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public ProductPriceRule productId(String productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUsedQuota() {
        return usedQuota;
    }

    public ProductPriceRule usedQuota(String usedQuota) {
        this.usedQuota = usedQuota;
        return this;
    }

    public void setUsedQuota(String usedQuota) {
        this.usedQuota = usedQuota;
    }

    public String getPriceSale() {
        return priceSale;
    }

    public ProductPriceRule priceSale(String priceSale) {
        this.priceSale = priceSale;
        return this;
    }

    public void setPriceSale(String priceSale) {
        this.priceSale = priceSale;
    }

    public String getPriceBase() {
        return priceBase;
    }

    public ProductPriceRule priceBase(String priceBase) {
        this.priceBase = priceBase;
        return this;
    }

    public void setPriceBase(String priceBase) {
        this.priceBase = priceBase;
    }

    public String getStatus() {
        return status;
    }

    public ProductPriceRule status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public LocalDate getActiveStartDate() {
//        return activeStartDate;
//    }
//
//    public ProductPriceRule activeStartDate(LocalDate activeStartDate) {
//        this.activeStartDate = activeStartDate;
//        return this;
//    }
//
//    public void setActiveStartDate(LocalDate activeStartDate) {
//        this.activeStartDate = activeStartDate;
//    }
//
//    public LocalDate getActiveEndDate() {
//        return activeEndDate;
//    }
//
//    public ProductPriceRule activeEndDate(LocalDate activeEndDate) {
//        this.activeEndDate = activeEndDate;
//        return this;
//    }
//
//    public void setActiveEndDate(LocalDate activeEndDate) {
//        this.activeEndDate = activeEndDate;
//    }
//
//    public Boolean isActive() {
//        return active;
//    }
//
//    public ProductPriceRule active(Boolean active) {
//        this.active = active;
//        return this;
//    }
//
//    public void setActive(Boolean active) {
//        this.active = active;
//    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductPriceRule)) {
            return false;
        }
        return id != null && id.equals(((ProductPriceRule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
//    @Override
//    public String toString() {
//        return "ProductPriceRule{" +
//            "id=" + getId() +
//            ", productId='" + getProductId() + "'" +
//            ", usedQuota='" + getUsedQuota() + "'" +
//            ", priceSale='" + getPriceSale() + "'" +
//            ", priceBase='" + getPriceBase() + "'" +
//            ", status='" + getStatus() + "'" +
//            ", activeStartDate='" + getActiveStartDate() + "'" +
//            ", activeEndDate='" + getActiveEndDate() + "'" +
//            ", active='" + isActive() + "'" +
//            "}";
//    }
}
