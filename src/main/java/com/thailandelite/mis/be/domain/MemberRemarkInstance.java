package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * A MemberRemarkInstance.
 */
@Data
@Document(collection = "member_remark_instance")
public class MemberRemarkInstance extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public static enum RemarkTag {
        GENERAL, EPA, CALL_CENTER, SYSTEM
    }

    @Id
    private String id;
    private Boolean pin;
    private String memberId;
    private String text;
    private RemarkTag tag;
    private List<String> accessTeams;
    private Boolean active;

}
