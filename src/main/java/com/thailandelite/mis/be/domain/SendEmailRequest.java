package com.thailandelite.mis.be.domain;

import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class SendEmailRequest {
    String to;
    String subject;
    String templateName;
    Object context;

    private Map<String, Object> vars = new HashMap<>();

    public void addVar(String key, Object value){
        vars.put(key, value);
    }
}
