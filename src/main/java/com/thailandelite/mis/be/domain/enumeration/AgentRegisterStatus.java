package com.thailandelite.mis.be.domain.enumeration;

/**
 * The AgentStatus enumeration.
 */
public enum AgentRegisterStatus {
    DRAFT, APPROVE
//    ACTIVE, INACTIVE
}
