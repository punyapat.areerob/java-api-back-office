package com.thailandelite.mis.be.domain.enumeration;

public enum CaseEntityType {
    APPLICATION, MEMBER, MEMBER_PAYMENT, MEMBERSHIP,BOOKING
}
