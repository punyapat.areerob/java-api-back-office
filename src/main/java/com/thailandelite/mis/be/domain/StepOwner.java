package com.thailandelite.mis.be.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * A StepOwner.
 */
@Document(collection = "step_owner")
public class StepOwner implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("staff_id")
    private String staffId;

    @Field("given_name")
    private String givenName;

    @Field("middle_name")
    private String middleName;

    @Field("sur_name")
    private String surName;

    @Field("email")
    private String email;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStaffId() {
        return staffId;
    }

    public StepOwner staffId(String staffId) {
        this.staffId = staffId;
        return this;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getGivenName() {
        return givenName;
    }

    public StepOwner givenName(String givenName) {
        this.givenName = givenName;
        return this;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public StepOwner middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurName() {
        return surName;
    }

    public StepOwner surName(String surName) {
        this.surName = surName;
        return this;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public StepOwner email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StepOwner)) {
            return false;
        }
        return id != null && id.equals(((StepOwner) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StepOwner{" +
            "id=" + getId() +
            ", staffId='" + getStaffId() + "'" +
            ", givenName='" + getGivenName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", surName='" + getSurName() + "'" +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
