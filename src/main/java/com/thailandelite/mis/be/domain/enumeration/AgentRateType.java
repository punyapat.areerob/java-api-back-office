package com.thailandelite.mis.be.domain.enumeration;

/**
 * The AgentRateType enumeration.
 */
public enum AgentRateType {
    STEP, FIXED, HENLEY
}
