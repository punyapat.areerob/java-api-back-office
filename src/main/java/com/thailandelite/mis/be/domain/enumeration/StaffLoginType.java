package com.thailandelite.mis.be.domain.enumeration;

/**
 * The StaffLoginType enumeration.
 */
public enum StaffLoginType {
    PASSWORD, LDAP
}
