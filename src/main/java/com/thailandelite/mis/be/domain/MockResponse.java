package com.thailandelite.mis.be.domain;

import lombok.Data;

@Data
public class MockResponse {
    private int cost=2500000;
    private int count=1;
    private String pack="EUP";
}
