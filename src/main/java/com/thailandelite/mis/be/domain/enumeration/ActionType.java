package com.thailandelite.mis.be.domain.enumeration;

/**
 * The ActionType enumeration.
 */
public enum ActionType {
    START, DONE, REJECT, MOVE_TO, SKIP
}
