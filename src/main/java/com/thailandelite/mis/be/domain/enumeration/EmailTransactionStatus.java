package com.thailandelite.mis.be.domain.enumeration;

/**
 * The EmailTransactionStatus enumeration.
 */
public enum EmailTransactionStatus {
    SUCCESS, FAIL
}
