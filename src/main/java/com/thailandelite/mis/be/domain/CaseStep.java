package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.Team;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CaseStep.
 */
@Document(collection = "case_step")
@Deprecated
public class CaseStep implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("case_id")
    private String caseId;

    @Field("begin")
    private Boolean begin;

    @DBRef
    @Field("team")
    private Team team;

    @DBRef
    @Field("actions")
    private Set<Action> actions = new HashSet<>();

    @DBRef
    @Field("caseStepState")
    private Set<CaseStepState> caseStepStates = new HashSet<>();

    @DBRef
    @Field("verifyFields")
    private Set<VerifyField> verifyFields = new HashSet<>();

    @DBRef
    @Field("inputFields")
    private Set<InputField> inputFields = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CaseStep name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaseId() {
        return caseId;
    }

    public CaseStep caseId(String caseId) {
        this.caseId = caseId;
        return this;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public Boolean isBegin() {
        return begin;
    }

    public CaseStep begin(Boolean begin) {
        this.begin = begin;
        return this;
    }

    public void setBegin(Boolean begin) {
        this.begin = begin;
    }

    public Team getTeam() {
        return team;
    }

    public CaseStep team(Team team) {
        this.team = team;
        return this;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Set<Action> getActions() {
        return actions;
    }

    public CaseStep actions(Set<Action> actions) {
        this.actions = actions;
        return this;
    }

    public CaseStep addActions(Action action) {
        this.actions.add(action);
        action.setCaseStep(this);
        return this;
    }

    public CaseStep removeActions(Action action) {
        this.actions.remove(action);
        action.setCaseStep(null);
        return this;
    }

    public void setActions(Set<Action> actions) {
        this.actions = actions;
    }

    public Set<CaseStepState> getCaseStepStates() {
        return caseStepStates;
    }

    public CaseStep caseStepStates(Set<CaseStepState> caseStepStates) {
        this.caseStepStates = caseStepStates;
        return this;
    }

    public CaseStep addCaseStepState(CaseStepState caseStepState) {
        this.caseStepStates.add(caseStepState);
        caseStepState.setStep(this);
        return this;
    }

    public CaseStep removeCaseStepState(CaseStepState caseStepState) {
        this.caseStepStates.remove(caseStepState);
        caseStepState.setStep(null);
        return this;
    }

    public void setCaseStepStates(Set<CaseStepState> caseStepStates) {
        this.caseStepStates = caseStepStates;
    }

    public Set<VerifyField> getVerifyFields() {
        return verifyFields;
    }

    public CaseStep verifyFields(Set<VerifyField> verifyFields) {
        this.verifyFields = verifyFields;
        return this;
    }

    public CaseStep addVerifyFields(VerifyField verifyField) {
        this.verifyFields.add(verifyField);
        verifyField.setCaseStep(this);
        return this;
    }

    public CaseStep removeVerifyFields(VerifyField verifyField) {
        this.verifyFields.remove(verifyField);
        verifyField.setCaseStep(null);
        return this;
    }

    public void setVerifyFields(Set<VerifyField> verifyFields) {
        this.verifyFields = verifyFields;
    }

    public Set<InputField> getInputFields() {
        return inputFields;
    }

    public CaseStep inputFields(Set<InputField> inputFields) {
        this.inputFields = inputFields;
        return this;
    }

    public CaseStep addInputFields(InputField inputField) {
        this.inputFields.add(inputField);
        inputField.setCaseStep(this);
        return this;
    }

    public CaseStep removeInputFields(InputField inputField) {
        this.inputFields.remove(inputField);
        inputField.setCaseStep(null);
        return this;
    }

    public void setInputFields(Set<InputField> inputFields) {
        this.inputFields = inputFields;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaseStep)) {
            return false;
        }
        return id != null && id.equals(((CaseStep) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CaseStep{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", caseId='" + getCaseId() + "'" +
            ", begin='" + isBegin() + "'" +
            "}";
    }
}
