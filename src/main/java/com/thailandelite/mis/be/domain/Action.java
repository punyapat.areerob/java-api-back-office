package com.thailandelite.mis.be.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;

import com.thailandelite.mis.be.domain.enumeration.ActionType;

/**
 * A Action.
 */
@Document(collection = "action")
@Deprecated
public class Action implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("next")
    private String next;

    @Field("type")
    private ActionType type;

    @DBRef
    @Field("caseStep")
    @JsonIgnoreProperties(value = "actions", allowSetters = true)
    private CaseStep caseStep;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Action name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNext() {
        return next;
    }

    public Action next(String next) {
        this.next = next;
        return this;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public ActionType getType() {
        return type;
    }

    public Action type(ActionType type) {
        this.type = type;
        return this;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public CaseStep getCaseStep() {
        return caseStep;
    }

    public Action caseStep(CaseStep caseStep) {
        this.caseStep = caseStep;
        return this;
    }

    public void setCaseStep(CaseStep caseStep) {
        this.caseStep = caseStep;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Action)) {
            return false;
        }
        return id != null && id.equals(((Action) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Action{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", next='" + getNext() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
