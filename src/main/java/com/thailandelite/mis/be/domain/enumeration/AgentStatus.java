package com.thailandelite.mis.be.domain.enumeration;

/**
 * The AgentStatus enumeration.
 */
public enum AgentStatus {
    DRAFT, WAITING, APPROVE, NOT_APPROVE
//    ACTIVE, INACTIVE
}
