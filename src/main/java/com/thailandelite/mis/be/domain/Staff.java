package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.Team;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;

import com.thailandelite.mis.be.domain.enumeration.StaffStatus;

import com.thailandelite.mis.be.domain.enumeration.StaffLoginType;

/**
 * A Staff.
 */
@Document(collection = "staff")
public class Staff implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("first_name")
    private String firstName;

    @Field("last_name")
    private String lastName;

    @Field("email")
    private String email;

    @Field("password")
    private String password;

    @Field("status")
    private StaffStatus status;

    @Field("login_type")
    private StaffLoginType loginType;

    @DBRef
    @Field("team")
    private Team team;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Staff firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Staff lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public Staff email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public Staff password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public StaffStatus getStatus() {
        return status;
    }

    public Staff status(StaffStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(StaffStatus status) {
        this.status = status;
    }

    public StaffLoginType getLoginType() {
        return loginType;
    }

    public Staff loginType(StaffLoginType loginType) {
        this.loginType = loginType;
        return this;
    }

    public void setLoginType(StaffLoginType loginType) {
        this.loginType = loginType;
    }

    public Team getTeam() {
        return team;
    }

    public Staff team(Team team) {
        this.team = team;
        return this;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Staff)) {
            return false;
        }
        return id != null && id.equals(((Staff) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Staff{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", password='" + getPassword() + "'" +
            ", status='" + getStatus() + "'" +
            ", loginType='" + getLoginType() + "'" +
            "}";
    }
}
