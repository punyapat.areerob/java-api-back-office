package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.be.domain.enumeration.BroadcastMessageStatus;
import com.thailandelite.mis.be.domain.enumeration.BroadcastMessageVisible;
import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import io.easycm.framework.form.form.FieldType;
import io.easycm.framework.form.form.FormField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * A BroadcastMessage.
 */
@Data
@EqualsAndHashCode(of = "id", callSuper = false)
@Document(collection = "broadcast_message")
public class BroadcastMessage extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String subject;
    private String message;
    private ZonedDateTime from;
    private ZonedDateTime to;
    private BroadcastMessageStatus status;
    private String creatorTeamId;
    private List<String> teamList;
}
