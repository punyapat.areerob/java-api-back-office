package com.thailandelite.mis.be.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;

import com.thailandelite.mis.be.domain.enumeration.StepState;

/**
 * A CaseInstance.
 */
@Document(collection = "case_instance")
@Deprecated
public class CaseInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("case_no")
    private String caseNo;

    @Field("case_id")
    private String caseId;

    @Field("current_step_id")
    private String currentStepId;

    @Field("remark")
    private String remark;

    @Field("member_id")
    private String memberId;

    @Field("case_data_id")
    private String caseDataId;

    @Field("step_status")
    private StepState stepStatus;

    @Field("created_by")
    private String createdBy;

    @Field("updated_by")
    private String updatedBy;

    @Field("created_at")
    private Instant createdAt;

    @Field("updated_at")
    private Instant updatedAt;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public CaseInstance caseNo(String caseNo) {
        this.caseNo = caseNo;
        return this;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseId() {
        return caseId;
    }

    public CaseInstance caseId(String caseId) {
        this.caseId = caseId;
        return this;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getCurrentStepId() {
        return currentStepId;
    }

    public CaseInstance currentStepId(String currentStepId) {
        this.currentStepId = currentStepId;
        return this;
    }

    public void setCurrentStepId(String currentStepId) {
        this.currentStepId = currentStepId;
    }

    public String getRemark() {
        return remark;
    }

    public CaseInstance remark(String remark) {
        this.remark = remark;
        return this;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMemberId() {
        return memberId;
    }

    public CaseInstance memberId(String memberId) {
        this.memberId = memberId;
        return this;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getCaseDataId() {
        return caseDataId;
    }

    public CaseInstance caseDataId(String caseDataId) {
        this.caseDataId = caseDataId;
        return this;
    }

    public void setCaseDataId(String caseDataId) {
        this.caseDataId = caseDataId;
    }

    public StepState getStepStatus() {
        return stepStatus;
    }

    public CaseInstance stepStatus(StepState stepStatus) {
        this.stepStatus = stepStatus;
        return this;
    }

    public void setStepStatus(StepState stepStatus) {
        this.stepStatus = stepStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CaseInstance createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public CaseInstance updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public CaseInstance createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public CaseInstance updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaseInstance)) {
            return false;
        }
        return id != null && id.equals(((CaseInstance) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CaseInstance{" +
            "id=" + getId() +
            ", caseNo='" + getCaseNo() + "'" +
            ", caseId='" + getCaseId() + "'" +
            ", currentStepId='" + getCurrentStepId() + "'" +
            ", remark='" + getRemark() + "'" +
            ", memberId='" + getMemberId() + "'" +
            ", caseDataId='" + getCaseDataId() + "'" +
            ", stepStatus='" + getStepStatus() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
