package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.agent.Agent;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;

/**
 * A CommissionIssue.
 */
@Data
@Document(collection = "commission_issue")
public class CommissionIssue extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String docNo;

    private ZonedDateTime docDate;

    @DBRef(lazy = true)
    private Agent agent;

    private List<String> issueFiles;

    private String issueStatusCode;

    private String invoiceNo;

    private ZonedDateTime invoiceDate;

    private Float invoiceAmount;

    @DBRef(lazy = true)
    private CommissionGroupIssue commissionGroupIssue;

    private String issueStatusGroupPayment;

    private String optionVat;

    private String remark;

    @Transient
    private List<CommissionIssueItem> _items;
}
