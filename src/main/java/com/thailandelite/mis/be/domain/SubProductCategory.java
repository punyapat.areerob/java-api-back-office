package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import com.thailandelite.mis.model.domain.Product;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A SubProductCategory.
 */
@Data
@Document(collection = "sub_product_category")
public class SubProductCategory extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("product_category_id")
    private String productCategoryId;

    @Field("name")
    private String name;

    @Field("name_th")
    private String nameTH;

    @Field("icon")
    private String icon;

    @Field("description")
    private String description;

    @Field("active")
    private Boolean active;

    @DBRef
    @Field("productList")
    private Set<Product> productLists = new HashSet<>();
}
