package com.thailandelite.mis.be.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * A CallLog.
 */
@Data
@Document(collection = "call_log")
public class CallLog implements Serializable {

    public static enum CallLogAction {
        RECEIVED, MISSED_CALL, NO_ANSWER
    }

    public static enum CallLogType {
        INCOMING, OUTGOING
    }

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private ZonedDateTime startTime;

    private Integer duration;

    private String phone;

    private List<MemberName> memberNames;

    private CallLogAction action;

    private String agentName;

    @Data
    public static class MemberName {

        private String memberId;

        private String firstName;

        private String lastName;
    }
}
