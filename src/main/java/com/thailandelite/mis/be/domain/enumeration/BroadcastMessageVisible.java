package com.thailandelite.mis.be.domain.enumeration;

/**
 * The BroadcastMessageVisible enumeration.
 */
public enum BroadcastMessageVisible {
    TEAM, ALL, CUSTOM
}
