package com.thailandelite.mis.be.domain.enumeration;

/**
 * The BroadcastMessageStatus enumeration.
 */
public enum BroadcastMessageStatus {
    DRAFT, ACTIVE, EXPIRED
}
