package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * A ContactTagMaster.
 */
@Data
@Document(collection = "contact_tag_master")
public class ContactTagMaster extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name_en")
    private String nameEn;

    @Field("name_th")
    private String nameTh;

    @Field("remark")
    private String remark;

    @Field("color")
    private String color;

    @Field("active")
    private Boolean active;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return nameEn;
    }

    public ContactTagMaster name(String name) {
        this.nameEn = name;
        return this;
    }

    public void setName(String name) {
        this.nameEn = name;
    }

    public String getColor() {
        return color;
    }

    public ContactTagMaster color(String color) {
        this.color = color;
        return this;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean isActive() {
        return active;
    }

    public ContactTagMaster active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameTh() {
        return nameTh;
    }

    public void setNameTh(String nameTh) {
        this.nameTh = nameTh;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getActive() {
        return active;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactTagMaster)) {
            return false;
        }
        return id != null && id.equals(((ContactTagMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactTagMaster{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", color='" + getColor() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
