package com.thailandelite.mis.be.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;

/**
 * A InputField.
 */
@Document(collection = "input_field")
public class InputField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("label")
    private String label;

    @Field("link")
    private String link;

    @DBRef
    @Field("caseStep")
    @JsonIgnoreProperties(value = "inputFields", allowSetters = true)
    private CaseStep caseStep;

    @DBRef
    @Field("caseStepInstance")
    @JsonIgnoreProperties(value = "inputFields", allowSetters = true)
    private CaseStepInstance caseStepInstance;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public InputField name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public InputField label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLink() {
        return link;
    }

    public InputField link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public CaseStep getCaseStep() {
        return caseStep;
    }

    public InputField caseStep(CaseStep caseStep) {
        this.caseStep = caseStep;
        return this;
    }

    public void setCaseStep(CaseStep caseStep) {
        this.caseStep = caseStep;
    }

    public CaseStepInstance getCaseStepInstance() {
        return caseStepInstance;
    }

    public InputField caseStepInstance(CaseStepInstance caseStepInstance) {
        this.caseStepInstance = caseStepInstance;
        return this;
    }

    public void setCaseStepInstance(CaseStepInstance caseStepInstance) {
        this.caseStepInstance = caseStepInstance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InputField)) {
            return false;
        }
        return id != null && id.equals(((InputField) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InputField{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", label='" + getLabel() + "'" +
            ", link='" + getLink() + "'" +
            "}";
    }
}
