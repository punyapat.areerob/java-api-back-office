package com.thailandelite.mis.be.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

import java.time.ZonedDateTime;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import com.thailandelite.mis.model.domain.Member;

/**
 * A CommissionIssueItem.
 */
@Data
@Document(collection = "commission_issue_item")
public class CommissionIssueItem extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @DBRef(lazy = true)
    private CommissionIssue commissionIssue;

    private Float commissionRatePercent;

    private String memberId;

    private Float commissionAmount;

    private String invoiceNo;

    private ZonedDateTime invoiceDate;

    private Integer commissionOrder;

    private String vatType;

    private String currency;

    private Float rate;

    private Float membershipBeforeVatAmount;

    private Float membershipVatAmount;

    private Float membershipTotalAmount;

    private Float commissionBeforeVatAmount;

    private Float commissionVatAmount;

    private Float commissionTotalAmount;
}
