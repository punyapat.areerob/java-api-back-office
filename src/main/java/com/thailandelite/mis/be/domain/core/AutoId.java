package com.thailandelite.mis.be.domain.core;

public interface AutoId {
    String sequenceName();
    default int length(){
        return 10;
    }

    String getId();

    void setId(String id);
}
