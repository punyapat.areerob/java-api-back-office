package com.thailandelite.mis.be.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;

import com.thailandelite.mis.be.domain.enumeration.ActionType;

/**
 * A ActionInstance.
 */
@Document(collection = "action_instance")
public class ActionInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("next")
    private String next;

    @Field("type")
    private ActionType type;

    @DBRef
    @Field("caseStepInstance")
    @JsonIgnoreProperties(value = "actionInstances", allowSetters = true)
    private CaseStepInstance caseStepInstance;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ActionInstance name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNext() {
        return next;
    }

    public ActionInstance next(String next) {
        this.next = next;
        return this;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public ActionType getType() {
        return type;
    }

    public ActionInstance type(ActionType type) {
        this.type = type;
        return this;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public CaseStepInstance getCaseStepInstance() {
        return caseStepInstance;
    }

    public ActionInstance caseStepInstance(CaseStepInstance caseStepInstance) {
        this.caseStepInstance = caseStepInstance;
        return this;
    }

    public void setCaseStepInstance(CaseStepInstance caseStepInstance) {
        this.caseStepInstance = caseStepInstance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActionInstance)) {
            return false;
        }
        return id != null && id.equals(((ActionInstance) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ActionInstance{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", next='" + getNext() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
