package com.thailandelite.mis.be.domain.enumeration;

/**
 * The StepState enumeration.
 */
public enum StepState {
    NEW, IN_PROGRESS, DONE, BLOCK
}
