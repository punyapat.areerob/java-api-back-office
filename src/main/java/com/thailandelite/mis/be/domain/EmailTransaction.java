package com.thailandelite.mis.be.domain;

import io.easycm.framework.form.form.FieldType;
import io.easycm.framework.form.form.FormField;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;

import com.thailandelite.mis.be.domain.enumeration.EmailTransactionStatus;

/**
 * A EmailTransaction.
 */
@Document(collection = "email_transaction")
public class EmailTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("template_name")
    private String templateName;

    @Field("to")
    private String to;

    @Field("subject")
    private String subject;

    @FormField(type = FieldType.TEXTAREA)
    @Field("body")
    private String body;

    @Field("send_date")
    private ZonedDateTime sendDate;

    @Field("status")
    private EmailTransactionStatus status;

    @Field("error")
    private String error;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public EmailTransaction templateName(String templateName) {
        this.templateName = templateName;
        return this;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTo() {
        return to;
    }

    public EmailTransaction to(String to) {
        this.to = to;
        return this;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public EmailTransaction subject(String subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public EmailTransaction body(String body) {
        this.body = body;
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public ZonedDateTime getSendDate() {
        return sendDate;
    }

    public EmailTransaction sendDate(ZonedDateTime sendDate) {
        this.sendDate = sendDate;
        return this;
    }

    public void setSendDate(ZonedDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public EmailTransactionStatus getStatus() {
        return status;
    }

    public EmailTransaction status(EmailTransactionStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(EmailTransactionStatus status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public EmailTransaction error(String error) {
        this.error = error;
        return this;
    }

    public void setError(String error) {
        this.error = error;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmailTransaction)) {
            return false;
        }
        return id != null && id.equals(((EmailTransaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmailTransaction{" +
            "id=" + getId() +
            ", templateName='" + getTemplateName() + "'" +
            ", to='" + getTo() + "'" +
            ", subject='" + getSubject() + "'" +
            ", body='" + getBody() + "'" +
            ", sendDate='" + getSendDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", error='" + getError() + "'" +
            "}";
    }
}
