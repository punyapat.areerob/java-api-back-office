package com.thailandelite.mis.be.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thailandelite.mis.model.domain.master.Team;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CaseStepInstance.
 */
@Document(collection = "case_step_instance")
@Deprecated
public class CaseStepInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("step_id")
    private String stepId;

    @Field("case_instance_id")
    private String caseInstanceId;

    @Field("begin")
    private Boolean begin;

    @DBRef
    @Field("team")
    private Team team;

    @DBRef
    @Field("stepOwner")
    private StepOwner stepOwner;

    @DBRef
    @Field("actionInstances")
    private Set<ActionInstance> actionInstances = new HashSet<>();

    @DBRef
    @Field("verifyFields")
    private Set<VerifyField> verifyFields = new HashSet<>();

    @DBRef
    @Field("inputFields")
    private Set<InputField> inputFields = new HashSet<>();

    @DBRef
    @Field("state")
    @JsonIgnoreProperties(value = "caseStepInstances", allowSetters = true)
    private CaseStepState state;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CaseStepInstance name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStepId() {
        return stepId;
    }

    public CaseStepInstance stepId(String stepId) {
        this.stepId = stepId;
        return this;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getCaseInstanceId() {
        return caseInstanceId;
    }

    public CaseStepInstance caseInstanceId(String caseInstanceId) {
        this.caseInstanceId = caseInstanceId;
        return this;
    }

    public void setCaseInstanceId(String caseInstanceId) {
        this.caseInstanceId = caseInstanceId;
    }

    public Boolean isBegin() {
        return begin;
    }

    public CaseStepInstance begin(Boolean begin) {
        this.begin = begin;
        return this;
    }

    public void setBegin(Boolean begin) {
        this.begin = begin;
    }

    public Team getTeam() {
        return team;
    }

    public CaseStepInstance team(Team team) {
        this.team = team;
        return this;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public StepOwner getStepOwner() {
        return stepOwner;
    }

    public CaseStepInstance stepOwner(StepOwner stepOwner) {
        this.stepOwner = stepOwner;
        return this;
    }

    public void setStepOwner(StepOwner stepOwner) {
        this.stepOwner = stepOwner;
    }

    public Set<ActionInstance> getActionInstances() {
        return actionInstances;
    }

    public CaseStepInstance actionInstances(Set<ActionInstance> actionInstances) {
        this.actionInstances = actionInstances;
        return this;
    }

    public CaseStepInstance addActionInstances(ActionInstance actionInstance) {
        this.actionInstances.add(actionInstance);
        actionInstance.setCaseStepInstance(this);
        return this;
    }

    public CaseStepInstance removeActionInstances(ActionInstance actionInstance) {
        this.actionInstances.remove(actionInstance);
        actionInstance.setCaseStepInstance(null);
        return this;
    }

    public void setActionInstances(Set<ActionInstance> actionInstances) {
        this.actionInstances = actionInstances;
    }

    public Set<VerifyField> getVerifyFields() {
        return verifyFields;
    }

    public CaseStepInstance verifyFields(Set<VerifyField> verifyFields) {
        this.verifyFields = verifyFields;
        return this;
    }

    public CaseStepInstance addVerifyFields(VerifyField verifyField) {
        this.verifyFields.add(verifyField);
        verifyField.setCaseStepInstance(this);
        return this;
    }

    public CaseStepInstance removeVerifyFields(VerifyField verifyField) {
        this.verifyFields.remove(verifyField);
        verifyField.setCaseStepInstance(null);
        return this;
    }

    public void setVerifyFields(Set<VerifyField> verifyFields) {
        this.verifyFields = verifyFields;
    }

    public Set<InputField> getInputFields() {
        return inputFields;
    }

    public CaseStepInstance inputFields(Set<InputField> inputFields) {
        this.inputFields = inputFields;
        return this;
    }

    public CaseStepInstance addInputFields(InputField inputField) {
        this.inputFields.add(inputField);
        inputField.setCaseStepInstance(this);
        return this;
    }

    public CaseStepInstance removeInputFields(InputField inputField) {
        this.inputFields.remove(inputField);
        inputField.setCaseStepInstance(null);
        return this;
    }

    public void setInputFields(Set<InputField> inputFields) {
        this.inputFields = inputFields;
    }

    public CaseStepState getState() {
        return state;
    }

    public CaseStepInstance state(CaseStepState caseStepState) {
        this.state = caseStepState;
        return this;
    }

    public void setState(CaseStepState caseStepState) {
        this.state = caseStepState;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaseStepInstance)) {
            return false;
        }
        return id != null && id.equals(((CaseStepInstance) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CaseStepInstance{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stepId='" + getStepId() + "'" +
            ", caseInstanceId='" + getCaseInstanceId() + "'" +
            ", begin='" + isBegin() + "'" +
            "}";
    }
}
