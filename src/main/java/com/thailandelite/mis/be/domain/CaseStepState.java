package com.thailandelite.mis.be.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.thailandelite.mis.be.domain.enumeration.StepState;

/**
 * A CaseStepState.
 */
@Document(collection = "case_step_state")
@Deprecated
public class CaseStepState implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("state")
    private StepState state;

    @DBRef
    @Field("caseStepInstance")
    private Set<CaseStepInstance> caseStepInstances = new HashSet<>();

    @DBRef
    @Field("step")
    @JsonIgnoreProperties(value = "caseStepStates", allowSetters = true)
    private CaseStep step;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CaseStepState name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StepState getState() {
        return state;
    }

    public CaseStepState state(StepState state) {
        this.state = state;
        return this;
    }

    public void setState(StepState state) {
        this.state = state;
    }

    public Set<CaseStepInstance> getCaseStepInstances() {
        return caseStepInstances;
    }

    public CaseStepState caseStepInstances(Set<CaseStepInstance> caseStepInstances) {
        this.caseStepInstances = caseStepInstances;
        return this;
    }

    public CaseStepState addCaseStepInstance(CaseStepInstance caseStepInstance) {
        this.caseStepInstances.add(caseStepInstance);
        caseStepInstance.setState(this);
        return this;
    }

    public CaseStepState removeCaseStepInstance(CaseStepInstance caseStepInstance) {
        this.caseStepInstances.remove(caseStepInstance);
        caseStepInstance.setState(null);
        return this;
    }

    public void setCaseStepInstances(Set<CaseStepInstance> caseStepInstances) {
        this.caseStepInstances = caseStepInstances;
    }

    public CaseStep getStep() {
        return step;
    }

    public CaseStepState step(CaseStep caseStep) {
        this.step = caseStep;
        return this;
    }

    public void setStep(CaseStep caseStep) {
        this.step = caseStep;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaseStepState)) {
            return false;
        }
        return id != null && id.equals(((CaseStepState) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CaseStepState{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", state='" + getState() + "'" +
            "}";
    }
}
