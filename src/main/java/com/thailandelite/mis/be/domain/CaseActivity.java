package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import lombok.Data;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;

import com.thailandelite.mis.model.domain.enumeration.CaseActivityEvent;

/**
 * A CaseActivity.
 */
@Data
@Document(collection = "case_activity")
public class CaseActivity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String detail;

    @Field("case_instance_id")
    private String caseInstanceId;

    @Field("event")
    private CaseActivityEvent event;

    @Field("remark")
    private String remark;

    @Field("action")
    private String action;

    private String createdBy;

    private Instant createdDate = Instant.now();

    private String lastModifiedBy;

    private Instant lastModifiedDate = Instant.now();

    private String createdUserType;

    private String lastUserType;

    private String recordStatus;

    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    public Instant getCreatedDate() {
        return super.getCreatedDate();
    }

    public String getLastModifiedBy() {
        return super.getLastModifiedBy();
    }

    public Instant getLastModifiedDate() {
        return super.getLastModifiedDate();
    }

    public String getCreatedUserType() {
        return super.getCreatedUserType();
    }

    public String getLastUserType() {
        return super.getLastUserType();
    }

    public String getRecordStatus() {
        return super.getRecordStatus();
    }



    public  CaseActivity(){
        super();
    }

    public CaseActivity(String remark,String user,String processInstanceId,String status , String action ) {
        this.detail = user+" changed CASE("+processInstanceId+") status to "+status;
        this.remark = remark;
        this.caseInstanceId = processInstanceId;
        this.action = action;
    }
}
