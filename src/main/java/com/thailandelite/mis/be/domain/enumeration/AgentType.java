package com.thailandelite.mis.be.domain.enumeration;

/**
 * The AgentType enumeration.
 */
public enum AgentType {
    SALE_AGENT,INDIVIDUAL_AGENT,GSSA,GSSA_GOBAL_PARTNERSHIP,CONCESSIONAIRE,OTHER,TPC,DEVELOPER,GSSA_UPGRADE
//    GSSA_UPGRADE, GSSA, Individual, HenleyAndPartner
}
