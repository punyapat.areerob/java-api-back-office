package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.agent.Agent;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;

/**
 * A CommissionGroupIssue.
 */
@Data
@Document(collection = "commission_group_issue")
public class CommissionGroupIssue extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private String docNo;

    private ZonedDateTime invoiceReceiveDate;

    private String issueStatusCode;

    @DBRef(lazy = true)
    private Agent agent;

    private String remark;

    @Transient
    List<CommissionIssue> _issues;
}
