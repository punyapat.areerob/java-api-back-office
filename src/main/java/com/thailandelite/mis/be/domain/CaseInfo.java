package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.enumeration.CaseEntityType;
import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import com.thailandelite.mis.model.domain.enumeration.IncidentType;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * A CaseInfo.
 */
@Data
@Document(collection = "case_info")
public class CaseInfo extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String flowDefKey;
    private String taskDefKey;
    private String processInstanceId;
    private String caseIdRef;
    private String caseNo;
    private String caseNoRef;
    private Boolean urgent;
    private Boolean isSeen;
    private String status;
    private Boolean finished = false;
    private PackageAction packageAction;
    private CaseEntityType entityType;
    private IncidentType incidentType;
    private String entityId;
    private String remark;
}
