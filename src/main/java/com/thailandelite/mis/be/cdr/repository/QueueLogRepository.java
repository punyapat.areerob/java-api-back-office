package com.thailandelite.mis.be.cdr.repository;

import com.thailandelite.mis.be.cdr.domain.QueueLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface QueueLogRepository extends JpaRepository<QueueLog, Integer>, JpaSpecificationExecutor<QueueLog> {

    Optional<QueueLog> findAllByCallidAndEvent(String callId, String event);
}
