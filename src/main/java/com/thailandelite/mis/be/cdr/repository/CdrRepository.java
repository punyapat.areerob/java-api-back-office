package com.thailandelite.mis.be.cdr.repository;

import com.thailandelite.mis.be.cdr.domain.Cdr;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CdrRepository extends JpaRepository<Cdr, Integer>, JpaSpecificationExecutor<Cdr> {

    List<Cdr> findAllByCalldateAfter(Date callDate);

    Page<Cdr> findByDst(String deskNo, Pageable pageable);
    List<Cdr> findByDst(String deskNo);
    long countCdrByDst(String deskNo);

//    @Query(value = "{'dst' : ?0 , 'disposition' : ?1 , $sort : {'calldate' : -1}}")
    Optional<Cdr> findTopByDstAndDisposition(String deskNo, String disposition, Sort sort);

    List<Cdr> findAllByDstAndCalldateAfter(String deskNo, Date callDate);

    Optional<Cdr> findOneByUniqueid(String uniqueid);
}
