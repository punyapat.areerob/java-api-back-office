package com.thailandelite.mis.be.cdr.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "queue_log")
public class QueueLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "time")
    private String time;

    @Column(name = "callid")
    private String callid;

    @Column(name = "queuename")
    private String queuename;

    @Column(name = "agent")
    private String agent;

    @Column(name = "event")
    private String event;

    @Column(name = "data")
    private String data;

    @Column(name = "data1")
    private String data1;

    @Column(name = "data2")
    private String data2;

    @Column(name = "data3")
    private String data3;

    @Column(name = "data4")
    private String data4;

    @Column(name = "data5")
    private String data5;

    @Column(name = "check_remove_q")
    private Integer checkRemoveQ;

}
