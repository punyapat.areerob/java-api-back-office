package com.thailandelite.mis.be.cdr.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "cdr")
public class Cdr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cdr_id", nullable = false)
    private Integer cdrId;

    @Column(name = "calldate")
    private Date calldate;

    @Column(name = "clid")
    private String clid;

    @Column(name = "src")
    private String src;

    @Column(name = "dst")
    private String dst;

    @Column(name = "dcontext")
    private String dcontext;

    @Column(name = "channel")
    private String channel;

    @Column(name = "dstchannel")
    private String dstchannel;

    @Column(name = "lastapp")
    private String lastapp;

    @Column(name = "lastdata")
    private String lastdata;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "billsec")
    private Integer billsec;

    @Column(name = "disposition")
    private String disposition;

    @Column(name = "amaflags")
    private Integer amaflags;

    @Column(name = "accountcode")
    private String accountcode;

    @Column(name = "uniqueid")
    private String uniqueid;

    @Column(name = "userfield")
    private String userfield;

    @Column(name = "recordingfile")
    private String recordingfile;

    @Column(name = "cnum")
    private String cnum;

    @Column(name = "cnam")
    private String cnam;

    @Column(name = "outbound_cnum")
    private String outboundCnum;

    @Column(name = "outbound_cnam")
    private String outboundCnam;

    @Column(name = "dst_cnam")
    private String dstCnam;

    @Column(name = "did")
    private String did;

}
