package com.thailandelite.mis.be.camunda.rest.client;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JobAplicationProcessClient implements JavaDelegate {
    private final Logger log = LoggerFactory.getLogger(JobAplicationProcessClient.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("DelegateExecution business key: " + execution.getCurrentActivityName());
    }
}
