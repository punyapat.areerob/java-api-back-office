package com.thailandelite.mis.be.events;

import com.thailandelite.mis.be.domain.core.AutoId;
import com.thailandelite.mis.be.service.SequenceGeneratorService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import java.util.Objects;


@Component
@RequiredArgsConstructor
public class AutoIdListener extends AbstractMongoEventListener<AutoId> {

    private final SequenceGeneratorService sequenceGenerator;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<AutoId> event) {
        AutoId autoId = event.getSource();
        if (Objects.isNull(autoId.getId())) {
            long newSequence = sequenceGenerator.generateSequence(autoId.sequenceName());
            String id = StringUtils.leftPad(String.valueOf(newSequence), autoId.length(), '0');
            autoId.setId(id);
        }
    }
}
