package com.thailandelite.mis.be.ldap;

import lombok.Data;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;



public class LdapHelper   {
    @Data
    public class MemberAD{
     public String email;
     public String group;
     public String dn;
     public MemberAD(String email,String group,String dn){
         this.email = email;
         this.group = group;
         this.dn = dn;
     }
    }


    public String authenticate(String username,String password){

        String url = "ldap://ec2-13-212-224-124.ap-southeast-1.compute.amazonaws.com:389"; // ldap://1.2.3.4:389 or ldaps://1.2.3.4:636
        String principalName = username; // firstname.lastname@mydomain.com
        String domainName = ""; // mydomain.com or empty

        if (domainName==null || "".equals(domainName)) {
            int delim = principalName.indexOf('@');
            domainName = principalName.substring(delim+1);
        }

        Properties props = new Properties();

        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, url);
        props.put(Context.SECURITY_PRINCIPAL, principalName);
        props.put(Context.SECURITY_CREDENTIALS, password); // secretpwd
        if (url.toUpperCase().startsWith("LDAPS://")) {
            props.put(Context.SECURITY_PROTOCOL, "ssl");
            props.put(Context.SECURITY_AUTHENTICATION, "simple");
            props.put("java.naming.ldap.factory.socket", "test.DummySSLSocketFactory");
        }
        try {
            InitialDirContext context = new InitialDirContext(props);

        } catch(Exception ex){

            return  ex.getMessage();
        }
        return "success";
    }


    public List<MemberAD> getEmail(String url, String principalName, String password, List<MemberAD> dns, String[] returnedAttrs, int maxResults)
    {
        List<MemberAD> list =new ArrayList<MemberAD>();

        Properties props = new Properties();

        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, url);
        props.put(Context.SECURITY_PRINCIPAL, principalName);
        props.put(Context.SECURITY_CREDENTIALS, password); // secretpwd
        if (url.toUpperCase().startsWith("LDAPS://")) {
            props.put(Context.SECURITY_PROTOCOL, "ssl");
            props.put(Context.SECURITY_AUTHENTICATION, "simple");
            props.put("java.naming.ldap.factory.socket", "test.DummySSLSocketFactory");
        }

        String member="";
        try{
            InitialDirContext ctx = new InitialDirContext(props);

            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
            searchCtls.setReturningAttributes(returnedAttrs);
            try{
                for (MemberAD m : dns
                     ) {
                    String dn = m.dn;
                    String[] query = dn.split(",");
                    NamingEnumeration users = ctx.search(dn.replace(query[0]+",",""),query[0], searchCtls);
                    int k = 0;
                    String attValue = "";
                    while (users.hasMoreElements()){
                        if(k >= maxResults)
                            break;
                        SearchResult sr = (SearchResult)users.next();
                        Attributes attrs = sr.getAttributes();
                        if (attrs.size() == 0){
                            System.out.println("Could not find attribute " + returnedAttrs[0] + " for this object.");
                        }else{

                            try{
                                for (NamingEnumeration ae = attrs.getAll();ae.hasMore();){
                                    Attribute attr = (Attribute)ae.next();
                                    String id = attr.getID();
                                    for (NamingEnumeration e = attr.getAll();e.hasMore();) {
                                        attValue = (String) e.next();

                                        if (id.equalsIgnoreCase("mail")) {
                                            list.add(new MemberAD(attValue,m.group,dn));
                                        }
                                        else
                                        {
                                            System.out.println("empty");
                                        }
                                    }
                                }
                            }catch(NamingException e){
                                System.out.println("Problem listing membership:"+e);
                            }
                        }
                        k++;
                    }
                }

            }catch (NamingException e){
                System.out.println("Problem searching directory: "+e);
            }
            ctx.close();
            ctx=null;
        }catch (Exception namEx){
            System.out.println("Exception while fetching the users from LDAP::"+namEx);
        }

        return list;
    }

    public String auth(String url, String principalName, String password)
    {
        Properties props = new Properties();

        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, url);
        props.put(Context.SECURITY_PRINCIPAL, principalName);
        props.put(Context.SECURITY_CREDENTIALS, password); // secretpwd
        if (url.toUpperCase().startsWith("LDAPS://")) {
            props.put(Context.SECURITY_PROTOCOL, "ssl");
            props.put(Context.SECURITY_AUTHENTICATION, "simple");
            props.put("java.naming.ldap.factory.socket", "test.DummySSLSocketFactory");
        }


        Hashtable userEntries = null;
        String member="";
        try {
            InitialDirContext ctx = new InitialDirContext(props);


        }catch (Exception e){

            return e.getMessage();
        }
        return "success";
    }

            public List<MemberAD> getGroupUsers(String url, String principalName, String password,String searchBase, String searchFilter, String returnedAttrs[], int maxResults)
    {
        List<MemberAD> list =new ArrayList<MemberAD>();

        //  String url = "ldap://ec2-13-212-224-124.ap-southeast-1.compute.amazonaws.com:389"; // ldap://1.2.3.4:389 or ldaps://1.2.3.4:636
        // String principalName = "thaielite\\jirapongw"; // firstname.lastname@mydomain.com
        //    String password = "Golfdigg123*"; // mydomain.com or empty

        Properties props = new Properties();

        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, url);
        props.put(Context.SECURITY_PRINCIPAL, principalName);
        props.put(Context.SECURITY_CREDENTIALS, password); // secretpwd
        if (url.toUpperCase().startsWith("LDAPS://")) {
            props.put(Context.SECURITY_PROTOCOL, "ssl");
            props.put(Context.SECURITY_AUTHENTICATION, "simple");
            props.put("java.naming.ldap.factory.socket", "test.DummySSLSocketFactory");
        }


        Hashtable userEntries = null;
        String member="";
        try{
            InitialDirContext ctx = new InitialDirContext(props);

            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
            searchCtls.setReturningAttributes(returnedAttrs);
            try{
                System.out.println("Search Base: "+searchBase);
                System.out.println("Search Filter: "+searchFilter);
                NamingEnumeration users = ctx.search(searchBase, searchFilter, searchCtls);
                if(users.hasMoreElements() == false){
                    System.out.println("Not find any object with this filter " + searchFilter + " and searchBase " + searchBase);
                }

                int k = 0;
                String attValue = "";
                userEntries = new Hashtable();
                while (users.hasMoreElements()){
                    if(k >= maxResults)
                        break;
                    SearchResult sr = (SearchResult)users.next();
                    Attributes attrs = sr.getAttributes();
                    if (attrs.size() == 0){
                        System.out.println("Could not find attribute " + returnedAttrs[0] + " for this object.");
                    }else{

                        try{
                            for (NamingEnumeration ae = attrs.getAll();ae.hasMore();){
                                Attribute attr = (Attribute)ae.next();
                                String id = attr.getID();
                                for (NamingEnumeration e = attr.getAll();e.hasMore();) {
                                    attValue = (String) e.next();

                                    if (id.equalsIgnoreCase("member")) {
                                        member = attValue;
                                        System.out.println("member :" + member);
                                        list.add(new MemberAD("",sr.getName(),attValue));
                                    }

                                    else
                                    {
                                        System.out.println("empty");
                                    }
                                }
                            }
                        }catch(NamingException e){
                            System.out.println("Problem listing membership:"+e);
                        }
                    }
                    k++;
                }
            }catch (NamingException e){
                System.out.println("Problem searching directory: "+e);
            }
            ctx.close();
            ctx=null;
        }catch (Exception namEx){
            System.out.println("Exception while fetching the users from LDAP::"+namEx);
        }
        return getEmail( "ldap://10.4.5.1:389","golfdigg@thailandelite.co.th","@reenju1ce",
        list ,  new String[]{"mail"}, 10);

    }



}
