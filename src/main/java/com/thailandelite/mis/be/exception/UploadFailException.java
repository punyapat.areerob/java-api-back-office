package com.thailandelite.mis.be.exception;

public class UploadFailException extends Exception {
    public UploadFailException(Throwable cause) {
        super(cause);
    }
}
