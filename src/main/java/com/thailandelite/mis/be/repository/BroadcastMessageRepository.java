package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.BroadcastMessage;

import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.domain.enumeration.BroadcastMessageStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the BroadcastMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BroadcastMessageRepository extends MongoRepository<BroadcastMessage, String> {
    @Query("{'from': { $lt: ?1},'to': { $gt: ?1}, 'teamList' : ?0 ,'status':?2 }")
    List<BroadcastMessage> findAllByActivePeriod(String teamId, Instant now, BroadcastMessageStatus status);

    List<BroadcastMessage> findAllByCreatorTeamId(String teamId);

    List<BroadcastMessage> findAllByFromAfterAndStatus(ZonedDateTime now, BroadcastMessageStatus status);

    List<BroadcastMessage> findAllByToBeforeAndStatus(ZonedDateTime now, BroadcastMessageStatus status);
}
