package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.ProductCategory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ProductCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductCategoryRepository extends MongoRepository<ProductCategory, String> {
//    List<ProductCategory> findByActive(Boolean active);
}
