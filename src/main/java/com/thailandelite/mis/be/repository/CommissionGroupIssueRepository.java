package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CommissionGroupIssue;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the CommissionGroupIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommissionGroupIssueRepository extends MongoRepository<CommissionGroupIssue, String> {
    Page<CommissionGroupIssue> findAllByAgent_Id(String agentId, Pageable pageable);
}
