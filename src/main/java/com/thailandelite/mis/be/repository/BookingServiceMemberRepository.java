package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data MongoDB repository for the BookingServiceMember entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingServiceMemberRepository extends MongoRepository<BookingServiceMember, String> {
    List<BookingServiceMember> findByBookingId(String id);
    List<BookingServiceMember> findByMemberId(String id);
    List<BookingServiceMember> findByVendorId(String id);
    Page<BookingServiceMember> findByVendorId(String id, Pageable page);

    @Query(value = "{'memberId' : ?0 , 'startTime' : { $lt: ?2 }, 'end' : { $gt : ?1 }}}")
    List<BookingServiceMember> findAllByMemberIdAndStartTimeBetween(String memberId, ZonedDateTime startTime, ZonedDateTime endTime);
    @Query(value = "{'memberId' : ?0 , 'endTime' : { $lt: ?2 }, 'end' : { $gt : ?1 }}}")
    List<BookingServiceMember> findAllByMemberIdAndEndTimeBetween(String memberId, ZonedDateTime startTime, ZonedDateTime endTime);
}
