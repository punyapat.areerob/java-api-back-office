package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.card.PhyCard;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the PhyCard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhyCardRepository extends MongoRepository<PhyCard, String> {
}
