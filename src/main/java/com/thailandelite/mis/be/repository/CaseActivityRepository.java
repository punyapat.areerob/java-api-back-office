package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseActivity;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the CaseActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseActivityRepository extends MongoRepository<CaseActivity, String> {
    List<CaseActivity> findAllByCaseInstanceId(String processInstanceId);

}
