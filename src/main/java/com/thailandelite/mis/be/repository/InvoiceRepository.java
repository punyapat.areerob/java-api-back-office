package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Invoice;

import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Invoice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceRepository extends MongoRepository<Invoice, String> {
//    List<Invoice> findByMemberId(String memberId, Pageable pageable);
    List<Invoice> findByMemberId(String memberId);
    Page<Invoice> findByPackageAction(String packageAction, Pageable pageable);
    Page<Invoice> findByDocumentIdInAndPayType(List<String> documentIds, PaymentType paymentType, Pageable pageable);
}
