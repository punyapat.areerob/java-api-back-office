package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Vendor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Vendor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendorRepository extends MongoRepository<Vendor, String> {
    List<Vendor> findByRating(String rating);
    List<Vendor> findByStatus(Vendor.Status status);
    Page<Vendor> findAllByStatus(Vendor.Status status, Pageable pageable);
}
