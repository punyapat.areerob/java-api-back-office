package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.ProductVariantGroup;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the ProductVariantGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductVariantGroupRepository extends MongoRepository<ProductVariantGroup, String> {
    List<ProductVariantGroup> findAllByVendorId(String id);
    Page<ProductVariantGroup> findAllByVendorId(String id, Pageable pageable);
}
