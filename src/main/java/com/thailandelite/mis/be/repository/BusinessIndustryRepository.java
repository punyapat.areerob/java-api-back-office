package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.BusinessIndustry;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the BusinessIndustry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusinessIndustryRepository extends MongoRepository<BusinessIndustry, String> {
}
