package com.thailandelite.mis.be.repository.custom;

import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.domain.enumeration.EmailTransactionStatus;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

public interface EmailTransactionCustomRepository {
    public List<EmailTransaction> findByProperties(String to, LocalDate date, EmailTransactionStatus status, Pageable page);
}
