package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.agent.JsonInteger;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the JsonInteger entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JsonIntegerRepository extends MongoRepository<JsonInteger, String> {
}
