package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.dto.request.VendorProfileUpdateDao;
import com.thailandelite.mis.model.dto.request.VendorRegisterDao;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Vendor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendorProdileUpdateRepository extends MongoRepository<VendorProfileUpdateDao, String> {

}
