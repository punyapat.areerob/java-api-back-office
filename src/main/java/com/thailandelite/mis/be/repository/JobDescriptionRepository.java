package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.JobDescription;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * Spring Data MongoDB repository for the JobDescription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobDescriptionRepository extends MongoRepository<JobDescription, String> {
    List<JobDescription> findAllByTeamId(String teamId);

    Page<JobDescription> findAllByTeamId(String teamId, Pageable pageable);

    Page<JobDescription> findAll(Pageable pageable);
}
