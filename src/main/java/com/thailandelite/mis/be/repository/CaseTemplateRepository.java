package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseTemplate;

import com.thailandelite.mis.model.domain.enumeration.CaseStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the CaseTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseTemplateRepository extends MongoRepository<CaseTemplate, String> {
    Optional<CaseTemplate> findByIdAndStatus(String id, CaseStatus status);
}
