package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.ContactTagMaster;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ContactTagMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactTagMasterRepository extends MongoRepository<ContactTagMaster, String> {
}
