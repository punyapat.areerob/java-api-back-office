package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseInstance;

import com.thailandelite.mis.be.domain.enumeration.StepState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the CaseInstance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseInstanceRepository extends MongoRepository<CaseInstance, String> {
    Page<CaseInstance> findByCaseId(String caseId, Pageable page);
    Page<CaseInstance> findByCaseIdAndStepStatus(String caseId, StepState state, Pageable page);
    Optional<CaseInstance> findById(String id);
}
