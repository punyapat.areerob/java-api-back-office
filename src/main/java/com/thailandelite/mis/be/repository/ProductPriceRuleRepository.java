package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.ProductPriceRule;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ProductPriceRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductPriceRuleRepository extends MongoRepository<ProductPriceRule, String> {
}
