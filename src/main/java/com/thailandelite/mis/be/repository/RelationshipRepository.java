package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.Relationship;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Relationship entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RelationshipRepository extends MongoRepository<Relationship, String> {
}
