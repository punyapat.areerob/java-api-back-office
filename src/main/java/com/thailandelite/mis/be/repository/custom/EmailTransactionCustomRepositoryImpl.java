package com.thailandelite.mis.be.repository.custom;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.domain.enumeration.EmailTransactionStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;
import static org.springframework.util.StringUtils.hasText;

@Repository
@RequiredArgsConstructor
public class EmailTransactionCustomRepositoryImpl implements EmailTransactionCustomRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public List<EmailTransaction> findByProperties(String to, LocalDate sendDate, EmailTransactionStatus status, Pageable page) {
        final Query query = new Query().with(page);

        final List<Criteria> criteria = new ArrayList<>();
        if (hasText(to)) {
            criteria.add(Criteria.where("to").regex(containing(to)));
        }
        if (nonNull(sendDate)){
            ZonedDateTime dateStart = DateUtils.toDate(sendDate);
            ZonedDateTime dateEnd = dateStart.plusDays(1);

            criteria.add(Criteria.where("sendDate").gte(dateStart).lt(dateEnd));
        }

        if (nonNull(status))
            criteria.add(Criteria.where("status").is(status));

        if (!criteria.isEmpty())
            query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));

        return mongoTemplate.find(query, EmailTransaction.class);
    }

    private String containing(String what){
        return MongoRegexCreator.INSTANCE.toRegularExpression(what, MongoRegexCreator.MatchMode.CONTAINING);
    }
}
