package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPhone;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the MemberPhone entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MemberPhoneRepository extends MongoRepository<MemberPhone, String> {
    List<MemberPhone> findByPhoneNo(String phoneNo);
    MemberPhone findFirstByPhoneNo(String phoneNo);
}
