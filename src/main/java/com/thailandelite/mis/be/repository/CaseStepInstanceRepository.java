package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseStepInstance;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the CaseStepInstance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseStepInstanceRepository extends MongoRepository<CaseStepInstance, String> {
}
