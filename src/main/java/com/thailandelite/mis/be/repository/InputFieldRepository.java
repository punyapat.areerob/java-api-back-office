package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.InputField;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the InputField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InputFieldRepository extends MongoRepository<InputField, String> {
}
