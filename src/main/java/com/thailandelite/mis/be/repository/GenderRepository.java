package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.Gender;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Gender entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GenderRepository extends MongoRepository<Gender, String> {
}
