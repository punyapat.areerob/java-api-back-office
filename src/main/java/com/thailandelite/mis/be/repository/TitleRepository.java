package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.Title;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Title entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TitleRepository extends MongoRepository<Title, String> {
    Optional<Title> findByName(String name);
    List<Title> findAllByPublicStatus(Boolean publicStatus);
}
