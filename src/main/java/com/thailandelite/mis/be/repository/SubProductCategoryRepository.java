package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.SubProductCategory;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the SubProductCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubProductCategoryRepository extends MongoRepository<SubProductCategory, String> {
    List<SubProductCategory> findByProductCategoryId(String productCategoryId);

}
