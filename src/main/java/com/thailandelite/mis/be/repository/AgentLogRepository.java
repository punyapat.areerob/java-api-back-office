package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentLog;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentLogRepository extends MongoRepository<AgentLog, String> {
    Page<AgentLog> findByDesk(String desk, Pageable page);
}
