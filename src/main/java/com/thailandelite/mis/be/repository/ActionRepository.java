package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.Action;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Action entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActionRepository extends MongoRepository<Action, String> {
}
