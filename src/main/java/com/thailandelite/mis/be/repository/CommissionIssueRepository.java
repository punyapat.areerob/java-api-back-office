package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CommissionIssue;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the CommissionIssue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommissionIssueRepository extends MongoRepository<CommissionIssue, String> {
    Page<CommissionIssue> findAllByAgent_Id(String id, Pageable pageable);
    List<CommissionIssue> findAllByCommissionGroupIssue_Id(String commissionGroupIssueId);

}
