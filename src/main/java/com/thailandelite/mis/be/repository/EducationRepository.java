package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.Education;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the District entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EducationRepository extends MongoRepository<Education, String> {
}
