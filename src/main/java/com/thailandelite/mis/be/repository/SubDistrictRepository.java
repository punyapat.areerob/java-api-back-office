package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.SubDistrict;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the SubDistrict entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubDistrictRepository extends MongoRepository<SubDistrict, String> {
}
