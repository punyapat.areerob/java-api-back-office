package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.JobApplication;

import com.thailandelite.mis.model.domain.MemberPayment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the JobApplication entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobApplicationRepository extends MongoRepository<JobApplication, String> {
    List<JobApplication> findByIdIn(List<String> entityIds);
}
