package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.ProductVariantValue;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the ProductVariantValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductVariantValueRepository extends MongoRepository<ProductVariantValue, String> {
    List<ProductVariantValue> findByProductVariantGroupId(String Id);
    List<ProductVariantValue> findByProductVariantGroupIdIn(List<String> Ids);
    List<ProductVariantValue> findByVendorId(String Id);
    void deleteByProductVariantGroupId(String groupId);
}
