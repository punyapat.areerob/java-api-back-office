package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.agent.JsonString;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the JsonString entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JsonStringRepository extends MongoRepository<JsonString, String> {
}
