package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.LineOfWork;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the District entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LineOfWorkRepository extends MongoRepository<LineOfWork, String> {
}
