package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.JobAssignment;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the JobAssignment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobAssignmentRepository extends MongoRepository<JobAssignment, String> {
}
