package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentStatusGroup;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentStatusGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentStatusGroupRepository extends MongoRepository<AgentStatusGroup, String> {
}
