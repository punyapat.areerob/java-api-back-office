package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.MemberRemarkTag;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the MemberRemarkTag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MemberRemarkTagRepository extends MongoRepository<MemberRemarkTag, String> {
}
