package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.PurposeDuration;
import com.thailandelite.mis.model.domain.master.PurposeOfStay;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Province entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurposeOfStayRepository extends MongoRepository<PurposeOfStay, String> {
}
