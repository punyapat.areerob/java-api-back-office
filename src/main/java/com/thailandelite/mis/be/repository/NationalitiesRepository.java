package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Nationality;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Nationalities entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NationalitiesRepository extends MongoRepository<Nationality, String> {
}
