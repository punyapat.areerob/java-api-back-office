package com.thailandelite.mis.be.repository;


import com.thailandelite.mis.model.domain.Passport;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportRepository extends MongoRepository<Passport, String> {

}
