package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
//    List<Product> findBySubProductCategoryId(String subProductCategoryId);

    @Query(value = "{'_id' : {$regex:?0, $options: 'i'}}")
    List<Product> findByProductId(String productId);

    List<Product> findByVendorId(String vendorId);
    List<Product> findByShopId(String shopId);
    Page<Product> findByVendorId(String vendorId, Pageable pageable);

}
