package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the MemberPrivilege entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MemberPrivilegeQuotaRepository extends MongoRepository<MemberPrivilegeQuota, String> {
    @Query(value = "{ 'memberId' : ?0 }")
    List<MemberPrivilegeQuota> findAllByMemberId(String memberId);

    @Query(value = "{ 'memberGroupId' : ?0 }")
    List<MemberPrivilegeQuota> findAllByMemberGroupId(String memberGroupId);

    @Query(value = "{ 'cardId' : ?0 }")
    List<MemberPrivilegeQuota> findAllByCardId(String cardId);

    @Query(value = "{'memberId' : ?0 , 'start' : { $lt: ?1 }, 'end' : { $gt : ?1 }}")
    List<MemberPrivilegeQuota> findAllByMemberIdAndBetweenDate(String memberId, ZonedDateTime year);

    @Query(value = "{'memberId' : ?0 , 'privilegeId' : ?1 , 'start' : { $lt: ?2 }, 'end' : { $gt : ?2 }}}")
    Optional<MemberPrivilegeQuota> findOneByMemberIdAndPrivilegeId(String memberId, String privilegeId, ZonedDateTime year);

    @Query(value = "{'membershipId' : ?0 , 'privilegeId' : ?1 , 'start' : { $lt: ?2 }, 'end' : { $gt : ?2 }}}")
    Optional<MemberPrivilegeQuota> findByMembershipIdAndPrivilegeId(String memberShipId, String privilegeId, ZonedDateTime date);

    @Query(value = "{'membershipId' : ?0 , 'start' : { $lt: ?1 }, 'end' : { $gt : ?1 }}")
    List<MemberPrivilegeQuota> findAllByMembershipId(String memberId, ZonedDateTime now);

    List<MemberPrivilegeQuota> findAllByMembershipIdAndYearQuota(String memberId, Integer year);
}
