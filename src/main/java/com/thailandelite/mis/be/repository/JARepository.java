package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.booking.JaFrom;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JARepository extends MongoRepository<JaFrom, String> {
    List<JaFrom> findAllByBookingId(String Id);

}
