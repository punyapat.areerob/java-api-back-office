package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.VendorContact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface VendorContactRepository extends MongoRepository<VendorContact, String> {
    List<VendorContact> findByVendorId(String vendorId);

    Page<VendorContact> findByVendorId(String vendorId, Pageable page);
}
