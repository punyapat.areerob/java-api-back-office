package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.ActionInstance;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ActionInstance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActionInstanceRepository extends MongoRepository<ActionInstance, String> {
}
