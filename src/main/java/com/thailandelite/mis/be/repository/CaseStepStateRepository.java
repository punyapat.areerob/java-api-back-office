package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseStep;
import com.thailandelite.mis.be.domain.CaseStepState;

import com.thailandelite.mis.be.domain.enumeration.StepState;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the CaseStepState entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseStepStateRepository extends MongoRepository<CaseStepState, String> {
    CaseStepState findByStepAndState(CaseStep caseStep, StepState state);
}
