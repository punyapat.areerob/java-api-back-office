package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Card;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Card entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CardRepository extends MongoRepository<Card, String> {
    List<Card> findByStatusAndPublicStatus(Card.CardStatus status, Card.PublicStatus publicStatus);
}
