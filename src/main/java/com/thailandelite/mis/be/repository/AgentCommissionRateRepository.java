package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentCommissionRate;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentCommissionRate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentCommissionRateRepository extends MongoRepository<AgentCommissionRate, String> {
}
