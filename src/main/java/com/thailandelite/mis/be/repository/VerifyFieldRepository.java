package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.VerifyField;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the VerifyField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VerifyFieldRepository extends MongoRepository<VerifyField, String> {
}
