package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.JobApplicationConfig;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the JobApplication entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobApplicationConfigRepository extends MongoRepository<JobApplicationConfig, String> {
}
