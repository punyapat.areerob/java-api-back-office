package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.MemberRemarkInstance;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the MemberRemarkInstance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MemberRemarkInstanceRepository extends MongoRepository<MemberRemarkInstance, String> {
    List<MemberRemarkInstance> findByMemberIdAndAccessTeamsAndTag(String memberId, String team, String tagId);

    Page<MemberRemarkInstance> findByPinIsTrue(Pageable pageable);
    List<MemberRemarkInstance> findByMemberId(String memberId);
    Page<MemberRemarkInstance> findByTag(MemberRemarkInstance.RemarkTag tag, Pageable pageable);
}
