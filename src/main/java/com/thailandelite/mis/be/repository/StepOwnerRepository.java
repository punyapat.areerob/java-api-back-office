package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.StepOwner;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the StepOwner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StepOwnerRepository extends MongoRepository<StepOwner, String> {
}
