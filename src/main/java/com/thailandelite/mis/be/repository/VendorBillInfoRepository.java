package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.VendorBillInfo;
import com.thailandelite.mis.model.domain.VendorContact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the ShopBillInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendorBillInfoRepository extends MongoRepository<VendorBillInfo, String> {
    Page<VendorBillInfo> findByVendorId(String vendorId, Pageable page);
}
