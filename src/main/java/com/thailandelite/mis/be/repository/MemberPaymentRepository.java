package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.MemberPayment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Card entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MemberPaymentRepository extends MongoRepository<MemberPayment, String> {
    List<MemberPayment> findByIdIn(List<String> entityIds);
}
