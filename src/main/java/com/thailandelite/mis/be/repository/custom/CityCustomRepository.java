package com.thailandelite.mis.be.repository.custom;

import com.thailandelite.mis.model.domain.master.City;

import java.util.List;

public interface CityCustomRepository {
    List<City> findByProperties(String countryId, String stateId);
}
