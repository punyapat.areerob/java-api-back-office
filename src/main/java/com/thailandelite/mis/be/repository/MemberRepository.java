package com.thailandelite.mis.be.repository;


import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPayment;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends MongoRepository<Member, String> {
    Member findAllByUserId(String userId);
    List<Member> findByIdIn(List<String> entityIds);
    Page<Member> findAllByOrderByCreatedDate(Pageable pageable);

    List<Member> findAllByStatus(MemberStatus status);
    Page<Member> findByStatus(String status, Pageable pageable);
    Page<Member> findByRecordStatus(String recordStatus, Pageable pageable);

    Optional<Member> findByEmail(String email);
}
