package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.VendorContract;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the VendorContract entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendorContractRepository extends MongoRepository<VendorContract, String> {
    Page<VendorContract> findByVendorId(String vendorId, Pageable page);

}
