package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Application entity.
 */

@Repository
public interface ApplicationRepository extends MongoRepository<Application, String> {
    /*Page<Application> findAllByFlowDefKey(String flowDefKey, Pageable pageable);
    Page<Application> findAllByTaskDefKeyInAndFlowDefKey(List<String> status,String flowDefKey, Pageable pageable);
    Page<Application> findAllByTaskDefKeyAndFlowDefKey(String status,String flowDefKey, Pageable pageable);
    Page<Application> findAllByStatusAndFlowDefKey(String status,String flowDefKey, Pageable pageable);
    Page<Application> findAllByProcessInstanceIdIn(List<String> ids, Pageable pageable);
    Optional<Application> findByProcessInstanceId(String id);*/

    List<Application> findByIdIn(List<String> entityIds);

    List<Application> findByMemberId(String memberId);

    List<Application> findByMemberId(String memberId, Sort sort);

    List<Application> findByRegisterAgentId(String agentId);

    Page<Application> findByRegisterAgentId(String agentId, Pageable pageable);

    @Query("{'registerAgentId': { $exists: true}}")
    List<Application> findByRegisterAgentIdExists();

    List<Application> findByRefApplicationId(String agentId);

    @Query("{'registerAgentId': { $exists: true}}")
    Page<Application> findByRegisterAgentIdExists(Pageable pageable);

    Page<Application> findAllByPackageAction(String packageAction, Pageable pageable);

    Page<Application> findByMemberId(String memberId, Pageable pageable);


}
