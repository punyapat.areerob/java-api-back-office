package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.JobPositions;

import com.thailandelite.mis.model.domain.hr.JobPositionsStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data MongoDB repository for the JobPositions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobPositionsRepository extends MongoRepository<JobPositions, String> {
    List<JobPositions> findAllByTeamId(String teamId);

    Page<JobPositions> findAllByTeamId(String teamId, Pageable page);

    @Query("{'from': { $lt: ?0},'to': { $gt: ?0} ,'status':?1 }")
    List<JobPositions> findAllActiveJob(ZonedDateTime now, JobPositionsStatus status);
}
