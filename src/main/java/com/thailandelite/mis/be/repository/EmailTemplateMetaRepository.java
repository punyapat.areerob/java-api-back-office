package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.EmailTemplateMeta;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the EmailTemplateMeta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmailTemplateMetaRepository extends MongoRepository<EmailTemplateMeta, String> {
}
