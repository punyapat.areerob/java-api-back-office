package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.ChannelKnow;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ChannelKnow entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChannelKnowRepository extends MongoRepository<ChannelKnow, String> {
}
