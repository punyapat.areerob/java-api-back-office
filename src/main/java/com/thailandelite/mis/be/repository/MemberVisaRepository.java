package com.thailandelite.mis.be.repository;


import com.thailandelite.mis.model.domain.MemberVisa;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberVisaRepository extends MongoRepository<MemberVisa, String> {
    MemberVisa findOneByMemberId(String memberId);
}
