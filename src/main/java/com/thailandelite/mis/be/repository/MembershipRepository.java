package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Membership;
import com.thailandelite.mis.model.domain.Membership.AnnualFeeStatus;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface MembershipRepository extends MongoRepository<Membership, String> {
//Find One not work.
    Optional<Membership> findOneByMemberIdAndStatus(String memberId, Membership.MembershipStatus status);

    List<Membership> findByMemberIdAndStatus(String memberId, Membership.MembershipStatus status, Sort sort);

    List<Membership> findByMemberIdAndStatusIn(String memberId, List<Membership.MembershipStatus> status, Sort sort);

    Optional<Membership> findByMembershipNo(String memberNo);

    Optional<Membership> findByMembershipIdNo(String membershipIdNo);

    @Query("{'membershipIdNo': {$regex: ?0, $options: 'i'}}")
    List<Membership> searchMembershipNo(String memberNo);

    List<Membership> findByIdIn(List<String> entityIds);

    @Query("{'annualFee_status': 'REQUIRE', 'annualFee_remain': {$gt: 0}, 'annualFee_next': { $gte: ?0, $lte: ?1}}")
    List<Membership> findByAnnualFeetBetween(ZonedDateTime from, ZonedDateTime to);

    List<Membership> findByEndDateBetween(ZonedDateTime from, ZonedDateTime to);

    List<Membership> findByStatus(Membership.MembershipStatus status);

    List<Membership> findByCardIdIn(List<String> cardIdList);

    List<Membership> findByMemberId(String memberId);
}
