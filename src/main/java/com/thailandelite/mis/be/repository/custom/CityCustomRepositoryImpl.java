package com.thailandelite.mis.be.repository.custom;

import com.thailandelite.mis.model.domain.master.City;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.util.StringUtils.hasText;

@Repository
@RequiredArgsConstructor
public class CityCustomRepositoryImpl implements CityCustomRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public List<City> findByProperties(String countryId, String stateId) {
        final Query query = new Query();

        if (hasText(countryId)) {
            query.addCriteria(where("countryId").is(countryId));
        }

        if (hasText(stateId)) {
            query.addCriteria(where("stateId").is(stateId));
        }

        return mongoTemplate.find(query, City.class);
    }
}
