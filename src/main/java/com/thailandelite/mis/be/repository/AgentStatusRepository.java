package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentStatus;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentStatusRepository extends MongoRepository<AgentStatus, String> {
}
