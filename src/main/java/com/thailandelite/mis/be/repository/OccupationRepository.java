package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Occupation;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Occupation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OccupationRepository extends MongoRepository<Occupation, String> {
    List<Occupation> findAllByPublicStatus(boolean publicStatus);
}
