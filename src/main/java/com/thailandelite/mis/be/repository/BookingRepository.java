package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.booking.Booking;

import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Booking entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingRepository extends MongoRepository<Booking, String> {
    List<Booking> findByIdIn(List<String> entityIds);

    List<Booking> findAllByRequestMemberIdAndBookingStatus(String memberId, BookingStatus bookingStatus);
}
