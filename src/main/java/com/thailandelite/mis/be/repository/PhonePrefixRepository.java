package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.PhonePrefix;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the PhonePrefix entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhonePrefixRepository extends MongoRepository<PhonePrefix, String> {
}
