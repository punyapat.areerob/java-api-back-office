package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Vendor;
import com.thailandelite.mis.model.domain.VendorRequest;
import com.thailandelite.mis.model.dto.request.VendorRegisterDao;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Vendor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendorRegisterRequestRepository extends MongoRepository<VendorRegisterDao, String> {

}
