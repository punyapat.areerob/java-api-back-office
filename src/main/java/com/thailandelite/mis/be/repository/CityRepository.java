package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.City;

import com.thailandelite.mis.be.repository.custom.CityCustomRepository;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the City entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityRepository extends MongoRepository<City, String>, CityCustomRepository {
    List<City> findAllByStateId(String stateId, Sort sort);
}
