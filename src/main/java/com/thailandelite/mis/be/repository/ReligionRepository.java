package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.Religion;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Religion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReligionRepository extends MongoRepository<Religion, String> {
}
