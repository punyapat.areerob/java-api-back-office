package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.ProductModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@SuppressWarnings("unused")
@Repository
public interface ProductModelRepository extends MongoRepository<ProductModel, String> {
    Page<ProductModel> findByVendorId(String vendorId, Pageable page);
    List<ProductModel> findByVendorId(String vendorId);
    ProductModel findByVendorIdAndName(String vendorId, String name);
    List<ProductModel> findByVendorIdAndProductVariantValue(String vendorId, String variantValueId);
}
