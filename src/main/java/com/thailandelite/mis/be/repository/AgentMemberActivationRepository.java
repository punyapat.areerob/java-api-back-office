package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentMemberActivation;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentMemberActivation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentMemberActivationRepository extends MongoRepository<AgentMemberActivation, String> {
}
