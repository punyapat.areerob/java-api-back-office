package com.thailandelite.mis.be.repository;

import java.util.Optional;

import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentContract;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentContract entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentContractRepository extends MongoRepository<AgentContract, String> {
    public Optional<AgentContract> findByAgent(Agent agentId);
}
