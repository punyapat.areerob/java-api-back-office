package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.IncomeReport;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Card entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IncomeReportRepository extends MongoRepository<IncomeReport, String> {
}
