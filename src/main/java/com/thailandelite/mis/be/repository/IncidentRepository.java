package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Incident;
import com.thailandelite.mis.model.domain.booking.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Booking entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IncidentRepository extends MongoRepository<Incident, String> {
    List<Incident> findByIdIn(List<String> entityIds);

}
