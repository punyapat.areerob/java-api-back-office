package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.State;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the State entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StateRepository extends MongoRepository<State, String> {
    List<State> findAllByCountryId(String countryId, Sort name);
}
