package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseStep;

import com.thailandelite.mis.model.domain.master.Team;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Spring Data MongoDB repository for the CaseStep entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseStepRepository extends MongoRepository<CaseStep, String> {
    ArrayList<CaseStep> findAllByCaseId(String caseId);
    List<CaseStep> findByTeam(Team team);
}
