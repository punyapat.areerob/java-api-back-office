package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.TransferPackageRequest;
import com.thailandelite.mis.model.domain.enumeration.TransferPackageRequestStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Occupation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransferPackageRequestRepository extends MongoRepository<TransferPackageRequest, String> {

    List<TransferPackageRequest> findByToEmail(String emailRequest);

    List<TransferPackageRequest> findByToEmailAndStatus(String emailRequest, TransferPackageRequestStatus status);
}
