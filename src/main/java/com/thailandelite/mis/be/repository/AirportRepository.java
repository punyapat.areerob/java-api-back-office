package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Airport;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface AirportRepository extends MongoRepository<Airport, String> {
    Airport findByCodeIataAirport(String id);
}
