package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.VisaType;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the VisaType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VisaTypeRepository extends MongoRepository<VisaType, String> {
}
