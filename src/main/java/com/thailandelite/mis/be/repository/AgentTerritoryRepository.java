package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentTerritory;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentTerritory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentTerritoryRepository extends MongoRepository<AgentTerritory, String> {
}
