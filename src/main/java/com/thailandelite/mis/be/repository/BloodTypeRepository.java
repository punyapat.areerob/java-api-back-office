package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.master.BloodType;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the BloodType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BloodTypeRepository extends MongoRepository<BloodType, String> {
}
