package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Prospect;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Prospect entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProspectRepository extends MongoRepository<Prospect, String> {
    List<Prospect> findByMemberId(String memberId);
}
