package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CommissionIssueItem;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the CommissionIssueItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommissionIssueItemRepository extends MongoRepository<CommissionIssueItem, String> {
    List<CommissionIssueItem> findByCommissionIssue_Id(String commissionIssueId);
}
