package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.Shop;

import com.thailandelite.mis.model.domain.VendorContact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Spring Data MongoDB repository for the Shop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopRepository extends MongoRepository<Shop, String> {
    List<Shop> findByVendorId(String vendorId);
//    List<Shop> findByContacts_IdIn(List<String> contactIds);
    Page<Shop> findByVendorId(String vendorId, Pageable page);
}
