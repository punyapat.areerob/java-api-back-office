package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.agent.AgentContractFile;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the AgentContractFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentContractFileRepository extends MongoRepository<AgentContractFile, String> {
}
