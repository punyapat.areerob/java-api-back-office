package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.model.domain.CardPrivilege;

import com.thailandelite.mis.model.domain.Privilege;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the CardPrivilege entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CardPrivilegeRepository extends MongoRepository<CardPrivilege, String> {
    List<CardPrivilege> findAllByCardIdAndActive(String cardId, Boolean active);
    List<CardPrivilege> findAllByCardId(String cardId);
}
