package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CallLog;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the CallLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CallLogRepository extends MongoRepository<CallLog, String> {
    Page<CallLog> findAll(Pageable pageable);
}
