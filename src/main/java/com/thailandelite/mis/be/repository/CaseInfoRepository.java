package com.thailandelite.mis.be.repository;

import com.thailandelite.mis.be.domain.CaseInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the CaseInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaseInfoRepository extends MongoRepository<CaseInfo, String> {
    Page<CaseInfo> findAllByFlowDefKeyIn(List<String> flowDefKey, Pageable pageable);
    Page<CaseInfo> findAllByFlowDefKey(String flowDefKey, Pageable pageable);
    Page<CaseInfo> findAllByTaskDefKeyInAndFlowDefKey(List<String> status, String flowDefKey, Pageable pageable);
    Page<CaseInfo> findAllByTaskDefKeyAndFlowDefKey(String taskDefKey,String flowDefKey, Pageable pageable);
    Page<CaseInfo> findAllByStatusAndFlowDefKey(String status,String flowDefKey, Pageable pageable);
    Page<CaseInfo> findAllByProcessInstanceIdIn(List<String> ids, Pageable pageable);
    Page<CaseInfo> findAllByEntityIdIn(List<String> entityId, Pageable pageable);
    Optional<CaseInfo> findByProcessInstanceId(String id);
    List<CaseInfo> findAllByEntityIdIn(List<String> entityId);
    List<CaseInfo> findByEntityId(String bookingId);
}
