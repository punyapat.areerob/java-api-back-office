package com.thailandelite.mis.be.service.mapper;
import com.thailandelite.mis.be.repository.TitleRepository;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.application.PassportForm;
import com.thailandelite.mis.model.domain.application.UserProfileForm;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberMapper {
    private final TitleRepository titleRepository;

    public Member prospectToMember(Prospect prospect){
        Member member = new Member();
        member.setTitle(prospect.getTitle());
        member.setGivenName(prospect.getGivenName());
        member.setMiddleName(prospect.getMiddleName());
        member.setSurName(prospect.getSurName());
        member.setNationality(prospect.getPassportNationality());
        member.setAreaCode(prospect.getAreaCode().getDialCode());
        member.setContactNo(prospect.getContactNo());
        member.setEmail(prospect.getAccountEmail());
        member.setStatus(MemberStatus.REGISTER);
        member.setPdpa(prospect.getIsAgreePdpa().toString());
        member.setAllowContactMe(prospect.getIsAllowContactMe());
        return member;
    }

    public  void mergeApplicationToMember(Application application, Member member){
        UserProfileForm profile = application.getUserProfileForm();

        member.setApplicationId(application.getId());
        member.setApplicationNo(application.getApplicationNo());
        member.setAgentId(application.getAgentId());

        member.setTitle(profile.getTitleId());
        member.setGender(profile.getGender());
        member.setGivenName(profile.getGivenName());
        member.setMiddleName(profile.getMiddleName());
        member.setSurName(profile.getSurName());
        member.setPicture(profile.getProfilePhoto());
        member.setDateOfBirth(profile.getDateOfBirth());
        member.setCountryOfBirth(profile.getCountryOfBirthId());

        PassportForm pf = application.getPassportForm();
        Passport passport = new Passport();
        passport.setPassportNo(pf.getPassportNo());
        passport.setIssueBy(pf.getIssueBy());
        passport.setIssueDate(pf.getIssueDate());
        passport.setExpireDate(pf.getExpireDate());
        passport.setNationality(pf.getNationalityId());
        passport.setFilePassport(pf.getPassportPhoto());
        member.setPassport(passport);
    }
}
