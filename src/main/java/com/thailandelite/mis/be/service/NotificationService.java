package com.thailandelite.mis.be.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.thailandelite.mis.be.domain.SendEmailRequest;
import com.thailandelite.mis.be.repository.EmailTransactionRepository;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.booking.enums.BookingInfoType;
import com.thailandelite.mis.model.domain.enumeration.BookingServiceMemberStatus;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.thailandelite.mis.model.domain.enumeration.BookingStatus;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationService {
    public static final String TEMPLATE_ACCOUNT_REGISTRATION = "AccountRegistration";
    public static final String TEMPLATE_APPLICATION_UPGRADE_CREATE = "AccountRegistration";
    public static final String TEMPLATE_APPLICATION_TYPE_CHANGE_CREATE = "AccountRegistration";
    public static final String TEMPLATE_APPLICATION_TRANSFER_CREATE = "AccountRegistration";

    public static final String TEMPLATE_SUBMIT_APPLICATION_FORM = "SubmitApplicationForm";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_INVOICE = "MembershipPaymentInvoice";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_REMINDER = "MembershipPaymentReminder";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_RECEIVED = "MembershipPaymentReceived";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_CONFIRMED = "MembershipPaymentConfirmed";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_REJECTED = "MembershipPaymentRejected";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_COMPLETED = "MembershipPaymentCompleted";
    public static final String TEMPLATE_MEMBERSHIP_PAYMENT_CANCELLED = "MembershipPaymentCancelled";

    public static final String TEMPLATE_SEND_WELCOME_LETTER = "SendWelcomeLetter";
    public static final String TEMPLATE_RESET_PASSWORD_EMAIL = "ResetPasswordEmail";
    public static final String TEMPLATE_PASSWORD_CHANGED_EMAIL = "PasswordChangedEmail";
    public static final String TEMPLATE_NAME_CHANGED_EMAIL = "NameChangedEmail";
    public static final String TEMPLATE_PASSPORT_CHANGED_EMAIL = "PassportChangedEmail";
    public static final String TEMPLATE_PROFILE_CHANGED = "ProfileChanged";

    public static final String TEMPLATE_PENDING_BOOKING_CONFIRMATION = "PendingBookingConfirmation";
    public static final String TEMPLATE_PENDING_JOIN_BOOKING = "PendingJoinBooking";
    public static final String TEMPLATE_BOOKING_CANCELLATION = "BookingCancellation";
    public static final String TEMPLATE_VENDOR_CHANGED_BOOKING = "VendorChangedBooking";
    public static final String TEMPLATE_MEMBER_CHANGED_BOOKING = "MemberChangeBooking";
    public static final String TEMPLATE_MEMBER_BOOKING_REQUEST = "MemberBookingRequest";
    public static final String TEMPLATE_MEMBER_BOOKING_CHANGE_REQUEST = "MemberBookingChangeRequest";
    public static final String TEMPLATE_MEMBER_BOOKING_CONFIRMATION = "MemberBookingConfirmation";
    public static final String TEMPLATE_MEMBER_BOOKING_CANCELLATION = "MemberBookingCancellation";
    public static final String TEMPLATE_REVIEW_BOOKING= "ReviewBooking";

    public static final String TEMPLATE_PENALTY_FEE_INVOICE = "PenaltyFeeInvoice";
    public static final String TEMPLATE_PENALTY_FEE_REMINDER = "PenaltyFeeReminder";
    public static final String TEMPLATE_PENALTY_FEE_RECEIVED = "PenaltyFeeReceived";
    public static final String TEMPLATE_PENALTY_FEE_CONFIRMED = "PenaltyFeeConfirmed";
    public static final String TEMPLATE_PENALTY_FEE_REJECTED = "PenaltyFeeRejected";
    public static final String TEMPLATE_PENALTY_FEE_COMPLETED = "PenaltyFeeCompleted";
    public static final String TEMPLATE_ANNUAL_FEE_INVOICE = "AnnualFeeInvoice";
    public static final String TEMPLATE_ANNUAL_FEE_REMINDER = "AnnualFeeReminder";
    public static final String TEMPLATE_ANNUAL_FEE_RECEIVED = "AnnualFeeReceived";
    public static final String TEMPLATE_ANNUAL_FEE_CONFIRMED = "AnnualFeeConfirmed";
    public static final String TEMPLATE_ANNUAL_FEE_REJECTED = "AnnualFeeRejected";
    public static final String TEMPLATE_ANNUAL_FEE_COMPLETED = "AnnualFeeCompleted";

    public static final String TEMPLATE_MEMBER_EXPIRED_REMINDED = "MemberExpiredReminded";
    public static final String TEMPLATE_MEMBER_AGENT_REMINDED = "MemberAgentReminded";
    public static final String TEMPLATE_SUBMIT_APPLICATION = "SubmitApplication";
    public static final String TEMPLATE_E_CARD_BIRTHDAY = "ECardBirthday";
    public static final String TEMPLATE_TRANSFER_PACKAGE_WELCOME = "TransferPackageWelcome";

    private final EmailService emailService;
    private final EmailTransactionRepository emailTemplateService;
//    private final BookingService bookingService;

    public void sendAccountRegistration(String to, String firstName, String middleName, String lastName) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_ACCOUNT_REGISTRATION);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);

        emailService.sendEmailTemplate(request);
    }

    public void sendSubmitApplicationForm(String to, String firstName, String middleName, String lastName) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_SUBMIT_APPLICATION_FORM);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        emailService.sendEmailTemplate(request);
    }
    public void sendCreateApplicationForm(String to, String firstName, String middleName, String lastName, String templateName, Application application) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(templateName);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.setContext(application);
        emailService.sendEmailTemplate(request);
    }
    public void sendMembershipPaymentInvoice(String to, SendMembershipPaymentInvoice context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_INVOICE);
        emailService.sendEmailTemplate(request);
    }

    public void sendMembershipPaymentReminder(String to, SendMembershipPaymentReminder context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_REMINDER);

        emailService.sendEmailTemplate(request);
    }

    public void sendMembershipPaymentReceived(String to, SendMembershipPaymentReceived context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_RECEIVED);


        emailService.sendEmailTemplate(request);
    }

    public void sendMembershipPaymentConfirmed(String to, SendMembershipPaymentConfirmed context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_CONFIRMED);

        emailService.sendEmailTemplate(request);
    }

    public void sendMembershipPaymentRejected(String to, SendMembershipPaymentRejected context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_REJECTED);

        emailService.sendEmailTemplate(request);
    }

    public void sendMembershipPaymentCompleted(String to, SendMembershipPaymentCompleted context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_COMPLETED);

        emailService.sendEmailTemplate(request);
    }

    public void sendMembershipPaymentCancelled(String to, SendMembershipPaymentCancelled context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBERSHIP_PAYMENT_CANCELLED);

        emailService.sendEmailTemplate(request);
    }

    public void sendWelcomeLetter(String to, String firstName, String middleName, String lastName, String welcomeLetterDetail) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_SEND_WELCOME_LETTER);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("welcomeLetterDetail", welcomeLetterDetail);

        emailService.sendEmailTemplate(request);
    }

    public void resetPasswordEmail(String to, String firstName, String middleName, String lastName, String dueDateAndTime) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_RESET_PASSWORD_EMAIL);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("dueDateAndTime", dueDateAndTime);

        emailService.sendEmailTemplate(request);
    }

    public void passwordChangedEmail(String to, String firstName, String middleName, String lastName, String dueDateAndTime) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_PASSWORD_CHANGED_EMAIL);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("dueDateAndTime", dueDateAndTime);

        emailService.sendEmailTemplate(request);
    }

    public void nameChangeEmail(String to, NameChangeEmail context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_NAME_CHANGED_EMAIL);

        emailService.sendEmailTemplate(request);
    }

    public void passportChangedEmail(String to, String firstName, String middleName, String lastName, String dueDateAndTime) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_PASSPORT_CHANGED_EMAIL);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("dueDateAndTime", dueDateAndTime);

        emailService.sendEmailTemplate(request);
    }

    public void profileChanged(String to, String firstName, String middleName, String lastName, String dueDateAndTime) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_PROFILE_CHANGED);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("dueDateAndTime", dueDateAndTime);

        emailService.sendEmailTemplate(request);
    }

    public void sendBookingEpa(Booking booking) throws IOException {
        BookingEpa context = new BookingEpa();
        Member member = booking.getRequestMember();

        context.setTitle(member.getTitle().getName());
        context.setFirstName(member.getGivenName());
        context.setLastName(member.getSurName());
        context.setBookingNumber(booking.getNo());

        BookingServiceMember item = booking.getBookingServiceMembers().get(0);
        context.setDueDate(item.getStartTime().format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
        context.setDueTime(item.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        context.setAirport(item.getShopName());
        context.setFlight(item.getBookingDetail().getFlightNumber());
//        context.setMeetTime(item);
        context.setPersons(booking.getBookingServiceMembers().size()+item.getBookingGuest().size());
        context.setPickupTime(item.getBookingDetail().getPickupTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        context.setVehicle(item.getModelName());

        context.setFrom(item.getBookingDetail().getFromName());
        context.setDestination(item.getBookingDetail().getToName()+" "+item.getShopName());

        List<PersonBookingDetail> personDetailList = booking.getBookingServiceMembers().stream().map(personD -> {
            PersonBookingDetail personDetail = new PersonBookingDetail();
            personDetail.setMemberName(personD.getMemberName());
            personDetail.setIdperson(personD.getMemberCardId());
            personDetail.setEmailperson(personD.getMemberEmail());
            personDetail.setCosttbd(personD.getPrice());
            return personDetail;
        }).collect(Collectors.toList());
        context.setPersonList(personDetailList);

        SendEmailRequest request = new SendEmailRequest();
        if (item.getStatus() == BookingServiceMemberStatus.CREATE){
            request.setTemplateName(TEMPLATE_PENDING_JOIN_BOOKING);
        }
        else if (item.getStatus() == BookingServiceMemberStatus.CANCEL){
            request.setTemplateName(TEMPLATE_BOOKING_CANCELLATION);
        }

        request.setTo("natnaree@golfdigg.com");
        request.setContext(context);

        emailService.sendEmailTemplate(request);

    }

    public void memberBooking(Booking booking) throws IOException {
        MemberBooking context = new MemberBooking();
        Member member = booking.getRequestMember();

        context.setTitle(member.getTitle().getName());
        context.setFirstName(member.getGivenName());
        context.setLastName(member.getSurName());
        context.setBookingNumber(booking.getNo());

        BookingServiceMember item = new BookingServiceMember();////////////////

        SendEmailRequest request = new SendEmailRequest();
        if (item.getStatus() == BookingServiceMemberStatus.CREATE){
            request.setTemplateName(TEMPLATE_MEMBER_BOOKING_REQUEST);
        }
        else if (item.getStatus() == BookingServiceMemberStatus.CANCEL){
            request.setTemplateName(TEMPLATE_MEMBER_BOOKING_CANCELLATION);
        }

        request.setTo("natnaree@golfdigg.com");
        request.setContext(context);

        emailService.sendEmailTemplate(request);
    }

    public void pendingBookingConfirmation(String to, SendMembershipPaymentInvoice context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENDING_BOOKING_CONFIRMATION);


//
        emailService.sendEmailTemplate(request);
    }

    public void bookingConfirmation(String to, BookingConfirmation context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_PENDING_BOOKING_CONFIRMATION);
        request.setContext(context);

        emailService.sendEmailTemplate(request);
    }

    public void bookingCancellation(String to, BookingCancellation context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_BOOKING_CANCELLATION);

        emailService.sendEmailTemplate(request);
    }


    public void vendorChangedBooking(String to, VendorChangeBooking context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_VENDOR_CHANGED_BOOKING);
        request.setContext(context);

        emailService.sendEmailTemplate(request);
    }

    public void memberChangedBooking(String to, MemberChangeBooking context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_CHANGED_BOOKING);

        emailService.sendEmailTemplate(request);
    }

    public void pendingJoinBooking(String to, PendingJoinBooking context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENDING_JOIN_BOOKING);

        emailService.sendEmailTemplate(request);
    }

    public void penaltyFeeInvoice(String to, PenaltyFeeInvoice context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENALTY_FEE_INVOICE);

        emailService.sendEmailTemplate(request);
    }

    public void penaltyFeeReminder(String to, PenaltyFeeReminder context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENALTY_FEE_REMINDER);

        emailService.sendEmailTemplate(request);
    }

    public void penaltyFeeReceived(String to, PenaltyFeeReceived context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENALTY_FEE_RECEIVED);

        emailService.sendEmailTemplate(request);
    }

    public void penaltyFeeConfirmed(String to, PenaltyFeeConfirmed context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENALTY_FEE_CONFIRMED);

        emailService.sendEmailTemplate(request);
    }

    public void penaltyFeeRejected(String to, PenaltyFeeRejected context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENALTY_FEE_REJECTED);

        emailService.sendEmailTemplate(request);
    }

    public void penaltyFeeCompleted(String to, PenaltyFeeCompleted context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_PENALTY_FEE_COMPLETED);

        emailService.sendEmailTemplate(request);
    }

    public void reviewBooking(String to, ReviewBooking context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_REVIEW_BOOKING);

        emailService.sendEmailTemplate(request);
    }

    public void memberBookingRequest(String to, MemberBookingRequest context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_BOOKING_REQUEST);

        emailService.sendEmailTemplate(request);
    }

    public void sendBookingEmail(String to, SendBookingEmail context,String fileName,byte[] file) throws IOException {
        context.setDueDate(context.getBooking().getReserveDateTime().format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
        context.setDueTime(context.getBooking().getReserveDateTime().format(DateTimeFormatter.ofPattern("HH:mm")));

        context.setTitleDatetime("");
        if (context.getBooking().getType() == BookingInfoType.DEPARTURE){
            context.setTitleDatetime("DEPARTURE ");
        }
        else if (context.getBooking().getType() == BookingInfoType.ARRIVAL){
            context.setTitleDatetime("ARRIVAL ");
        }

        if ((context.getBooking().getStatus() == BookingStatus.CREATE) || (context.getBooking().getStatus() == BookingStatus.WAITING_CONFIRM)){
            context.setWaiting(true);

        }
        else if ((context.getBooking().getStatus() == BookingStatus.CONFIRMED) || (context.getBooking().getStatus() == BookingStatus.COMPLETE)){
            context.setConfirm(true);
        }
        else if ((context.getBooking().getStatus() == BookingStatus.REJECTED) || (context.getBooking().getStatus() == BookingStatus.CANCEL)){
            context.setCancel(true);
        }

        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_BOOKING_CHANGE_REQUEST);
        if ((context.getBooking().getStatus() == BookingStatus.CREATE)) {
            emailService.sendEmailTemplate(request, fileName, file);
        } else {
            emailService.sendEmailTemplate(request);
        }

    }

    public void memberBookingConfirmation(String to, MemberBookingConfirmation context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_BOOKING_CONFIRMATION);

        emailService.sendEmailTemplate(request);
    }

    public void memberBookingCancellation(String to, MemberBookingCancellation context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_BOOKING_CANCELLATION);

        emailService.sendEmailTemplate(request);
    }

    public void annualFeeInvoice(String to, AnnualFeeInvoice context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_ANNUAL_FEE_INVOICE);

        emailService.sendEmailTemplate(request);
    }

    public void annualFeeReminder(String to, AnnualFeeReminder context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_ANNUAL_FEE_REMINDER);

        emailService.sendEmailTemplate(request);
    }

    public void annualFeeReceived(String to, AnnualFeeReceived context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_ANNUAL_FEE_RECEIVED);

        emailService.sendEmailTemplate(request);
    }

    public void annualFeeConfirmed (String to, AnnualFeeConfirmed context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_ANNUAL_FEE_CONFIRMED);

        emailService.sendEmailTemplate(request);
    }

    public void annualFeeRejected(String to, AnnualFeeRejected context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_ANNUAL_FEE_REJECTED);

        emailService.sendEmailTemplate(request);
    }

    public void annualFeeCompleted (String to, AnnualFeeCompleted context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_ANNUAL_FEE_COMPLETED);

        emailService.sendEmailTemplate(request);
    }

    public void memberExpiredReminded (String to, MemberExpiredReminded context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_EXPIRED_REMINDED);

        emailService.sendEmailTemplate(request);
    }

    public void memberAgentReminded (String to, MemberAgentReminded context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_MEMBER_AGENT_REMINDED);

        emailService.sendEmailTemplate(request);
    }

    public void submitApplication (String to, String firstName, String middleName, String lastName, String dueDate) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_SUBMIT_APPLICATION);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("dueDate", dueDate);

        emailService.sendEmailTemplate(request);
    }

    public void eCardBirthday (String to, String firstName, String middleName, String lastName, String birthday) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setTemplateName(TEMPLATE_E_CARD_BIRTHDAY);
        request.addVar("firstName", firstName);
        request.addVar("middleName", middleName);
        request.addVar("lastName", lastName);
        request.addVar("birthday", birthday);

        emailService.sendEmailTemplate(request);
    }

    public void transferPackageWelcome(String to, TransferPackageWelcome context) throws IOException {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(to);
        request.setContext(context);
        request.setTemplateName(TEMPLATE_TRANSFER_PACKAGE_WELCOME);

        emailService.sendEmailTemplate(request);
    }

    @Data
    public static class BookingConfirmation {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String dueTime;
        String province;
        String persons;
//        String bookingDetails;
        String amount;
        String cost;
        String total;
        String dateConfirm;
    }

    @Data
    public static class BookingCancellation {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String dueTime;
        String airport;
        String flight;
        String meetTime;
        String persons;
        String pickupTime;
        String vehicle;
        String from;
        String destination;

        //        String bookingDetails;
        String amount;
        String cost;
        String total;
        String dateConfirm;
    }

    @Data
    public static class VendorChangeBooking {
        String nameVendor;
        String bookingNumber;
        String dueDate;
        String bookingDetails;
        String amount;
        String cost;
        String howToPlay;
    }

    @Data
    public static class PendingBookingConfirmation {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String dueTime;
        String airport;
        String flight;
        String meetTime;
        String persons;
        String pickupTime;
        String vehicle;
        String from;
        String destination;

//        String bookingDetails;
        String amount;
        String cost;
        String total;
        String dateconfirm;

    }

    @Data
    public static class NameChangeEmail {
        String firstName;
        String middleName;
        String lastName;
        String newFirstName;
        String newMiddleName;
        String newLastName;
        String dueDateAndTime;
    }

    @Data
    public static class SendMembershipPaymentInvoice {
        String title;
        String firstName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
        String barcodeImg;
    }

    @Data
    public static class SendMembershipPaymentReminder {
        String title;
        String firstName;
        String lastName;
        String day;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class SendMembershipPaymentReceived {
        String firstName;
        String title;
        String lastName;
    }

    @Data
    public static class SendMembershipPaymentConfirmed {
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class SendMembershipPaymentRejected {
        String firstName;
        String title;
        String lastName;
    }

    @Data
    public static class SendMembershipPaymentCompleted {
        String firstName;
        String title;
        String lastName;
    }

    @Data
    public static class SendMembershipPaymentCancelled {
        String firstName;
        String title;
        String lastName;
    }

    @Data
    public static class MemberChangeBooking {
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String bookingDetails;
        String amount;
        String cost;
        String howToPlay;
    }

    @Data
    public static class PendingJoinBooking {
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String bookingDetails;
        String amount;
        String cost;
    }

    @Data
    public static class PenaltyFeeInvoice {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class PenaltyFeeReminder {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class PenaltyFeeReceived {
        String title;
        String firstName;
        String middleName;
        String lastName;
    }

    @Data
    public static class PenaltyFeeConfirmed {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class PenaltyFeeRejected {
        String title;
        String firstName;
        String middleName;
        String lastName;
    }

    @Data
    public static class PenaltyFeeCompleted {
        String title;
        String firstName;
        String middleName;
        String lastName;
    }

    @Data
    public static class ReviewBooking {
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String Package;
        String amount;
        String cost;
    }

    @Data
    public static class MemberBookingRequest {
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String bookingDetails;
        String vendorName;
        String vendorAddress;
        String conditions;
    }

    @Data
    public static class SendBookingEmail {
        BookingResponse booking;
        String dueDate;
        String dueTime;
        String titleDatetime;
        boolean waiting;
        boolean confirm;
        boolean cancel;
    }

    @Data
    public static class MemberBookingConfirmation {
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String bookingDetails;
        String vendorName;
        String vendorAddress;
        String conditions;
    }

    @Data
    public static class MemberBookingCancellation{
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String bookingDetails;
        String vendorName;
        String vendorAddress;
        String conditions;
    }

    @Data
    public static class AnnualFeeInvoice {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class AnnualFeeReminder {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class AnnualFeeReceived  {
        String title;
        String firstName;
        String middleName;
        String lastName;
    }

    @Data
    public static class AnnualFeeConfirmed {
        String title;
        String firstName;
        String middleName;
        String lastName;
        String dueDate;
        String memberPackage;
        String amount;
        String cost;
        String sumCost;
    }

    @Data
    public static class AnnualFeeRejected {
        String title;
        String firstName;
        String middleName;
        String lastName;
    }

    @Data
    public static class AnnualFeeCompleted {
        String title;
        String firstName;
        String middleName;
        String lastName;
    }

    @Data
    public static class MemberExpiredReminded {
        String firstName;
        String middleName;
        String lastName;
        String memberID;
        String Package;
        String expiredDate;
    }

    @Data
    public static class MemberAgentReminded {
        String firstName;
        String middleName;
        String lastName;
        String memberAgentID;
        String expiredDate;
    }

    @Data
    public static class TransferPackageWelcome  {
        String fromName;
        String toName;
        String packageName;
        String notifyUrl;

    }

    @Data
    public static class BookingEpa{
        String title;
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String dueTime;
        String airport;
        String flight;
        String meetTime;
        Integer persons;
        String pickupTime;
        String vehicle;

        String from;
        String destination;
        List<PersonBookingDetail> personList = new ArrayList<>();

    }

    @Data
    public static class PersonBookingDetail{
        String memberName;
        String idperson;
        String emailperson;
        Float costtbd;
    }

    @Data
    public static class MemberBooking{
        String title;
        String firstName;
        String middleName;
        String lastName;
        String bookingNumber;
        String dueDate;
        String dueTime;
        String province;
        Integer persons;

        String total;

        List<PersonBookingDetail> personList = new ArrayList<>();
    }

    public static void main(String[] args) throws IOException {
        String html = FileUtils.readFileToString(new File("template.html"));
        Booking booking = new ObjectMapper().readValue(new File("booking.json"), Booking.class);

        Handlebars hb = new Handlebars();
        Template template = hb.compileInline(html);
        String htmlOutput = template.apply(booking);
        FileUtils.write(new File("template-out.html"), htmlOutput);
    }


}
