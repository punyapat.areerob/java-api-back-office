package com.thailandelite.mis.be.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.booking.JaFrom;
import com.thailandelite.mis.be.service.model.MembershipInfo;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.booking.enums.BookingFlowDefinitionKey;
import com.thailandelite.mis.model.domain.enumeration.JAStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class JAService {
    private final SpringTemplateEngine templateEngine;
    private final FileService fileService;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final BookingRepository bookingRepository;
    private final BookingServiceMemberRepository bookingServiceMemberRepository;
    private final ProductRepository productRepository;
    private final ProductModelRepository productModelRepository;
    private final VendorRepository vendorRepository;
    private final ShopRepository shopRepository;
    private final VendorContactRepository vendorContactRepository;
    private final MemberService memberService;
    private final CardRepository cardRepository;
    private final PrivilegeRepository privilegeRepository;
    private final JARepository jaRepository;

    public enum Type{
        EPA,SPA,GOLF,SHORTHAUL,LONGHAUL,AIRPORTSERVICE,ANNUALHEALTHCHECK,BANKING,COOKINGCLASS,HOTEL,RESTAURANT,SHOPPING,CONSULTANT,SERVICERESIDENCE,VISA,DAYSREPORT,ARRIVALLOUNGE,DRIVINGLICENSE,
        EPACONECTING,EPAARRUVAL,TRANSFERIN,EPADEPARTURE,TRANSFEROUT,STAYEXTENSION,BANKJOB,VISAJOB
    };

    public String createJA(String bookingId, String type) throws UploadFailException, DocumentException, IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        Booking booking = bookingRepository.findById(bookingId).orElseThrow();
        List<JaFrom> jaBooking = jaRepository.findAllByBookingId(bookingId);
        Integer version = jaBooking.size();
        List<BookingServiceMember> bookingServiceMember =  bookingServiceMemberRepository.findByBookingId(booking.getId());
        List<String> JaPathFile = bookingServiceMember.stream().map(c->{
            Product result = productRepository.findById(c.getProductId()).orElseThrow(()->new NotFoundException("Not found product"));
            Shop shop =  shopRepository.findById(result.getShopId()).orElseThrow(()->new NotFoundException("Not found shop"));
//            VendorContact vendorContactShop = vendorContactRepository.findById(shop.getContactId().get(0)).orElseThrow(()->new NotFoundException("Not found ContactShop"));
            ProductModel productModel = productModelRepository.findById(c.getModelId()).orElseThrow(()->new NotFoundException("Not found productModel"));
            Vendor vendor = vendorRepository.findById(result.getVendorId()).orElseThrow(()->new NotFoundException("Not found vendor"));
            VendorContact vendorContact = vendor.getContact()==null?null:vendorContactRepository.findById(vendor.getContact().get(0)).orElseThrow(()->new NotFoundException("Not found Vendor Contact"));
            Privilege privilege =productModel.getPrivilegesId()==null?null:privilegeRepository.findById(productModel.getPrivilegesId().get(0)).orElseThrow(()->new NotFoundException("Not found privilege"));
            MembershipInfo membershipInfo = null;
            try {
                membershipInfo = memberService.getMembershipInfo(c.getMemberId());

            } catch (HandlerException e) {
                e.printStackTrace();
            }
            String JAnumber = (version == 0 ) ? generateJA():jaBooking.get(0).getJaNumber();
            JaFrom jaFrom = new JaFrom();
            jaFrom.setStatus(JAStatus.CREATE);
            jaFrom.setBookingId(bookingId);
            jaFrom.setVersion(version+1);
            Card card = cardRepository.findById(membershipInfo.getMembership().getCardId()).orElseThrow();
            jaFrom.setDate(ZonedDateTime.now().format(formatter));
            jaFrom.setTo(vendorContact==null?null:vendorContact.getName());
            jaFrom.setTel(vendorContact==null?null:vendorContact.getPhone());
            jaFrom.setInternalCode("123");
            jaFrom.setVendorname(shop.getNameEn());
            jaFrom.setCompany(vendor.getNameEn());
            jaFrom.setFax(vendorContact==null?null:vendorContact.getFax());
            jaFrom.setJaNumber(JAnumber);
            jaFrom.setEmail(vendorContact==null?null:vendorContact.getEmail());
            jaFrom.setFrom("nittaya.s");
            jaFrom.setFromTel("02 352 3098 (8:30 - 20:00),\n" + "02 352 3000 (20:00 - 8:30)");
            jaFrom.setFromFax("02 352 3099");
            jaFrom.setFromEmail("rsvn@thailandelite.com");
            jaFrom.setMemberName(booking.getName());
            jaFrom.setMemberId(booking.getNo());
            jaFrom.setNationality(membershipInfo.getMember().getNationality()==null?null:membershipInfo.getMember().getNationality().getName());
            jaFrom.setDateOfBirth(membershipInfo.getMember().getDateOfBirth()==null?null:membershipInfo.getMember().getDateOfBirth().toString());
            jaFrom.setMemberShipType(card.getName());
            jaFrom.setAccompanyingImmediate("Immediate Name");///////*******************
            jaFrom.setFamily("Family");
            jaFrom.setActivedDate("Mock Data");
            jaFrom.setNoOfImmediate("0 Immediate Family");
            jaFrom.setImmediateName("Mock Data");
            jaFrom.setMemberIdOld("1004005918");
            jaFrom.setPassport(membershipInfo.getVisa()==null?null:membershipInfo.getVisa().getPassportNo());
            jaFrom.setAnnualSelection("Individual(Group1)");
            jaFrom.setRelationship("");
            jaFrom.setPassportNo(membershipInfo.getVisa()==null?null:membershipInfo.getVisa().getPassportNo());
//            jaFrom.setAccompanyingGuest();
//            jaFrom.setNoOfGuest();
//            jaFrom.setTotalNoOfPerson();
//            jaFrom.setDateOfServiceRequired();
//            jaFrom.setServiceRequired();
//            jaFrom.setRemark();
//            jaFrom.setGuestName();
//            jaFrom.setRelationshipmember();
//            jaFrom.setPassportNomember();
//            jaFrom.setTime();
            switch (Type.valueOf(type)) {
                case GOLF:
                    jaFrom.setType(BookingFlowDefinitionKey.GOLF);
                    jaFrom.setHeader("Golf Job Assignment");//////Topic
//                    jaFrom.setImmigrationOffice();
                    break;
                case SPA:
                    jaFrom.setType(BookingFlowDefinitionKey.SPA);
                    jaFrom.setHeader("SPA Job Assignment");//////Topic
//                    jaFrom.setImmigrationOffice();
                    break;
                case EPACONECTING:
                    jaFrom.setHeader("EPA Conecting Flight Job Assignment");//////Topic
//                    jaFrom.setFlight();
//                    jaFrom.setMeetingPoint();
//                    jaFrom.setMemberContactNo();
//                    jaFrom.setPaymentBy();
//                    jaFrom.setAmendmentCancellation();
//                    jaFrom.setPolicy();
                    break;
                case EPAARRUVAL:
                    jaFrom.setHeader("EPA Arruval Job Assignment");//////Topic
//                    jaFrom.setFlight();
//                    jaFrom.setMemberContactNo();
//                    jaFrom.setPaymentBy();
//                    jaFrom.setAmendmentCancellation();
//                    jaFrom.setPolicy();
//                    jaFrom.setArrival();
//                    jaFrom.setVipType();
                    break;
                case TRANSFERIN:
                    jaFrom.setHeader("Transfer In Job Assignment");//////Topic
//                    jaFrom.setFlight();
//                    jaFrom.setMemberContactNo();
//                    jaFrom.setAmendmentCancellation();
//                    jaFrom.setPolicy();
//                    jaFrom.setFromMember();
//                    jaFrom.setToMember();
//                    jaFrom.setAddress();
//                    jaFrom.setVehicleRequirement();
                    break;
                case EPADEPARTURE:
                    jaFrom.setHeader("EPA Departure Job Assignment");//////Topic
//                    jaFrom.setFlight();
//                    jaFrom.setMemberContactNo();
//                    jaFrom.setAmendmentCancellation();
//                    jaFrom.setPolicy();
//                    jaFrom.setDeparture();
                    break;
                case TRANSFEROUT:
                    jaFrom.setHeader("Transfer Out Job Assignment");//////Topic
//                    jaFrom.setFlight();
//                    jaFrom.setMemberContactNo();
//                    jaFrom.setAmendmentCancellation();
//                    jaFrom.setPolicy();
//                    jaFrom.setFromMember();
//                    jaFrom.setToMember();
//                    jaFrom.setAddress();
//                    jaFrom.setVehicleRequirement();
                    break;
                case STAYEXTENSION:
                    jaFrom.setHeader("Stay Extension Job Assignment");//////Topic
//                    jaFrom.setImmigrationOffice();
                    break;
                case BANKJOB:
                    jaFrom.setHeader("Bank Job Assignment");//////Topic
//                    jaFrom.setPaymentBy();
                    break;
                case EPA:
                    jaFrom.setHeader("Elite personal  Job Assignment");//////Topic
//                    jaFrom.setPaymentBy();
                    break;
                case VISAJOB:
                    jaFrom.setHeader("Visa Job Assignment");//////Topic
//                    jaFrom.setImmigrationOffice();
                    break;

            }
            //jaFrom.setHeader();//////Topi
//            jaFrom.setFlight();
//            jaFrom.setMeetingPoint();
//            jaFrom.setMemberContactNo();
//            jaFrom.setPaymentBy();
//            jaFrom.setAmendmentCancellation();
//            jaFrom.setPolicy();
//            jaFrom.setArrival();
//            jaFrom.setVipType();
//            jaFrom.setFromMember();
//            jaFrom.setToMember();
//            jaFrom.setAddress();
//            jaFrom.setVehicleRequirement();
//            jaFrom.setDeparture();
//            jaFrom.setImmigrationOffice();
            try {
                byte[] qrBytes = generateQRCodeImage("hello");
                String base64 = Base64.getEncoder().encodeToString(qrBytes);
                jaFrom.setQrImage(base64);
            } catch (Exception e) {
                log.error("Generate QR fail", e);
            }

            Context context = new Context();
            context.setVariable("model",jaFrom);


            String tai =null;
            try {
                tai =  generatePdf(context,null,"JA.html",JAnumber+"-v"+jaFrom.getVersion());
            } catch (Exception e) {
                log.error("Generate Pdf fail", e);
            }

            jaFrom.setPathPdfUrl(tai);
            c.setJobAssignmentId(jaRepository.save(jaFrom));
            bookingServiceMemberRepository.save(c);
            return tai;
        }).collect(Collectors.toList());

        return JaPathFile.get(0);
    }

    public Optional<JaFrom> getJAById(String id){
        return jaRepository.findById(id);
    }
    public List<JaFrom> getJAByBooking(String bookingId){
        return jaRepository.findAllByBookingId(bookingId);
    }
    public JaFrom getJABookingLast(String bookingId){
        return jaRepository.findAllByBookingId(bookingId).stream().reduce((a,b)->a.getVersion() > b.getVersion()?a:b).get();
    }
    public List<JaFrom> getJAAll(){
        return jaRepository.findAll();
    }


    public String generatePdf(Context context, String path, String fileName,String outputFilename) throws IOException, DocumentException, UploadFailException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-");
        String html = templateEngine.process(fileName, context);
        Document document = Jsoup.parse(html);
        document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
        String xhtml = document.html();
        ITextRenderer iTextRenderer = new ITextRenderer();
        iTextRenderer.setDocumentFromString(xhtml);
        iTextRenderer.layout();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        OutputStream os = new FileOutputStream(outputFilename+".pdf");
//        iTextRenderer.createPDF(os);
        iTextRenderer.createPDF(out);
        InputStream data = new ByteArrayInputStream(out.toByteArray());
//        os.close();
        return  fileService.upload("ja/"+ZonedDateTime.now().format(formatter)+outputFilename+".pdf","pdf",data);
    }

    public String generateJA(){
        return "JA"+ZonedDateTime.now().getYear() +"-"+ String.format("%04d", sequenceGeneratorService.generateSequence("JA"));
    }

    private static byte[] generateQRCodeImage(String barcodeText) throws WriterException, IOException {
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "JPG", outputStream);
        return outputStream.toByteArray();
    }
}
