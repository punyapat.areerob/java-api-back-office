package com.thailandelite.mis.be.service.dto;

import lombok.Data;

@Data
public class JobApplicationDTO {
    private String firstName;
    private String lastName;
}
