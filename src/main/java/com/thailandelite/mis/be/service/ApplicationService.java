package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.web.rest.ApplicationResource;
import com.thailandelite.mis.model.UploadRequest;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.Prospect;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.application.*;
import com.thailandelite.mis.model.domain.enumeration.ApplicationStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.thailandelite.mis.be.service.NotificationService.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApplicationService {
    private final ApplicationRepository applicationRepository;
    private final MemberRepository memberRepository;
    private final ProspectRepository prospectRepository;
    private final CardRepository cardRepository;
    private final AgentRepository agentRepository;
    private final NotificationService notificationService;

    public String uploadFile(String id,String type , MultipartFile file) throws UploadFailException {
        FileService fileService = new FileService();
        Application application = applicationRepository.findById(id).get();
        String url = "";
        switch (type) {
            case "signature":
                url = UploadRequest.application(id, file).signature().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).signature());
                break;
            case "profile":
                url = UploadRequest.application(id, file).profile().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).profile());
                break;
            case "visa":
                url = UploadRequest.application(id, file).visa().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).visa());
                break;
            case "consent":
                url = UploadRequest.application(id, file).consent().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).consent());
                break;
            case "passport":
                url = UploadRequest.application(id, file).passport().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).passport());
                break;
            case "pdpa":
                url = UploadRequest.application(id, file).pdpa().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).pdpa());
                break;
            case "grim":
                url = UploadRequest.application(id, file).grim().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).grim());
                break;
            case "cs":
                url = UploadRequest.application(id, file).cs().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).cs());
                break;
            case "im":
                url = UploadRequest.application(id, file).im().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).im());
                break;
            case "grcs":
                url = UploadRequest.application(id, file).grcs().pathWithExtention();
                fileService.upload(UploadRequest.application(id, file).grcs());
                break;
        }

        applicationRepository.save(application);

        return url;
    }

    public Application createApplication(ApplicationResource.CreateApplicationReq req) {
        Application result;
        Application application = new Application();
        Application applicationRef = new Application();
        String cardId = req.getCardId();
        if(!req.getIsCoreMember()){
            applicationRef = applicationRepository.findById(req.getRefApplicationId()).orElseThrow(()->new NotFoundException("Not found core application."));
            cardId = applicationRef.getCard().getId();
        }
        Card card = cardRepository.findById(cardId).orElseThrow(() -> new RuntimeException("Card not found") );

        if(req.getIsCoreMember()) {
            Member member = memberRepository.findById(req.getMemberId()).orElseThrow(() -> new RuntimeException("Member not found"));
            List<Prospect> prospects = prospectRepository.findByMemberId(req.getMemberId());


            application.setMemberId(req.getMemberId());
            application.setIsCoreMember(req.getIsCoreMember());

            application.setCard(card);
            application.setAddressForm(new AddressForm());

            UserProfileForm userProfileForm = new UserProfileForm();
            userProfileForm.setTitleId(member.getTitle());
            userProfileForm.setGivenName(member.getGivenName());
            userProfileForm.setMiddleName(member.getMiddleName());
            userProfileForm.setSurName(member.getSurName());
            application.setUserProfileForm(userProfileForm);

            AddressForm addressForm = new AddressForm();
            PassportForm passportForm = new PassportForm();
            if (prospects != null && prospects.size() > 0) {
                Prospect prospect = prospects.get(0);
                addressForm.setPhoneCountryCodeId(prospect.getAreaCode());
                addressForm.setTelephoneNo(prospect.getContactNo());
                addressForm.setEmailMainContact(prospect.getAccountEmail());
                passportForm.setPassportNo(prospect.getPassportNo());
            }
            passportForm.setNationalityId(member.getNationality());

            application.setAddressForm(addressForm);
            application.setOccupationForm(new OccupationForm());
            application.setDocumentForm(new DocumentForm());
            application.setSurveyForm(new SurveyForm());
            application.setTermConditionForm(new TermConditionForm());
            application.setPassportForm(passportForm);
            application.setRegisterAgentId(member.getRegisterAgentId());
            if ( StringUtils.isNotBlank(member.getRegisterAgentId()) ){
                Optional<Agent> agent = agentRepository.findById(member.getRegisterAgentId());
                if(agent.isPresent())
                    application.setAgentName( agent.get().getSalesAgentEn());
            }
            application.setStatus(ApplicationStatus.CREATE);
            result = applicationRepository.save(application);
            member.setApplicationId(result.getId());
            memberRepository.save(member);
        }else{

            application.setMemberId(req.getMemberId());
            application.setIsCoreMember(req.getIsCoreMember());
            application.setRefApplicationId(req.getRefApplicationId());

            application.setCard(card);
            application.setAddressForm(new AddressForm());

            UserProfileForm userProfileForm = new UserProfileForm();
            application.setUserProfileForm(userProfileForm);
            application.setAddressForm(new AddressForm());
            application.setOccupationForm(new OccupationForm());
            application.setDocumentForm(new DocumentForm());
            application.setSurveyForm(new SurveyForm());
            application.setTermConditionForm(new TermConditionForm());
            application.setPassportForm(new PassportForm());
            application.setRegisterAgentId(applicationRef.getRegisterAgentId());
            application.setAgentName(applicationRef.getAgentName());
            application.setStatus(ApplicationStatus.CREATE);
            result = applicationRepository.save(application);
        }
        return result;
    }

    public Application save(Application application) {
        return applicationRepository.save(application);
    }

    public Application createRegisterApplication(ApplicationResource.CreateApplicationReq req) {
        Application application = createApplication(req);
        if (req.getIsCoreMember())
            sendCreateApplicationNotify(application, TEMPLATE_ACCOUNT_REGISTRATION);

        return application;
    }
    public Application createUpgradeApplication(ApplicationResource.CreateApplicationReq req) {
        Application application = createApplication(req);
        sendCreateApplicationNotify(application, TEMPLATE_APPLICATION_UPGRADE_CREATE);
        return application;
    }
    public Application createTypeChangeApplication(ApplicationResource.CreateApplicationReq req) {
        Application application = createApplication(req);
        sendCreateApplicationNotify(application, TEMPLATE_APPLICATION_TYPE_CHANGE_CREATE);
        return application;
    }

    private void sendCreateApplicationNotify(Application application, String templateName) {
        Optional<Member> optional = memberRepository.findById(application.getMemberId());
        if (optional.isPresent()) {
            Member member = optional.get();
            try {
                if(TEMPLATE_ACCOUNT_REGISTRATION.equalsIgnoreCase(templateName)) {
                    notificationService.sendAccountRegistration(member.getEmail(), member.getGivenName(), member.getMiddleName(), member.getSurName());
                }else{
                    notificationService.sendCreateApplicationForm(member.getEmail(), member.getGivenName(), member.getMiddleName(), member.getSurName(), templateName, application);
                }
            } catch (IOException e) {
                log.warn("Cannot send {} to email: {}, ", templateName, member.getEmail());
                e.printStackTrace();
            }
        }
    }
}
