package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.application.*;
import com.thailandelite.mis.model.domain.enumeration.ApplicationStatus;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.domain.enumeration.TransferPackageRequestStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberRequestService {
    @Value("${mis.fontOnlineUrl}")
    public String fontOnlineUrl;

    private final MemberRepository memberRepository;
    private final MembershipRepository membershipRepository;
    private final FontEndService fontEndService;
    private final MemberService memberService;
    private final CardRepository cardRepository;
    private final NotificationService notificationService;
    private final ApplicationRepository applicationRepository;
    private final TransferPackageRequestRepository transferPackageRequestRepository;

//    public Member updateMember(Member from, String memberId) {
//        Member member = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
//        if (StringUtils.isNotBlank(from.getEmail()) && !from.getEmail().equalsIgnoreCase(member.getEmail()) ) {
//            member.setEmail(from.getEmail());
//        }else if ( from.getMemberStatus() != null &&  from.getMemberStatus() != member.getMemberStatus()) {
//            member.setMemberStatus(from.getMemberStatus());
//        }
//        member = memberRepository.save(member);
//        return member;
//    }

    public Member changePasswordRequest(String memberId, String newPassword) {
        Member member = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
        fontEndService.changePassword(member.getEmail(), newPassword);
        return member;
    }

    public Member changeEmailRequest(String memberId, String newEmail) {
        Member member = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
        fontEndService.changeEmail(member.getEmail(), newEmail);
        return member;
    }

    public Member changeMemberStatus(String memberId, MemberStatus status, String remark) {
        Member member = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
        member.setStatus(status);
        memberRepository.save(member);
        return member;
    }

    public Member upgradeMemberPackageRequest(String memberId, String upgradeCardId, String remark) throws HandlerException {
        Member member = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
        memberService.upgradeMembership(memberId, upgradeCardId);
        return member;
    }

    public Member typeChangeMemberRequest(String memberId, String typeChangeCardId) throws HandlerException {
        Member member = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
        memberService.typeChangeMembership(memberId, typeChangeCardId);
        return member;
    }

    public Member transferMemberRequest(String memberId, String email){
        List<TransferPackageRequest> transferPackageRequests = transferPackageRequestRepository.findByToEmailAndStatus(email, TransferPackageRequestStatus.CREATE);
        if(transferPackageRequests != null && transferPackageRequests.size() > 0 )
            throw new BadRequestException("Exists transfer package: ["+ transferPackageRequests.get(0).getFromMemberId() +"]");

        Member memberA = memberRepository.findById(memberId).orElseThrow( () -> new NotFoundException("Not found member"));
        Membership membershipA = membershipRepository.findById(memberA.getMembershipId()).orElseThrow(()-> new NotFoundException("Not found membership record."));
        Card cardA = cardRepository.findById(membershipA.getCardId()).orElseThrow(()->new NotFoundException("Not found card."));

        Optional<Member> memberB = memberRepository.findByEmail(email);
        NotificationService.TransferPackageWelcome tpw = new NotificationService.TransferPackageWelcome();
        tpw.setFromName(memberA.getGivenName()+ " "+ memberA.getSurName());
        tpw.setPackageName(cardA.getName());
        TransferPackageRequest request = new TransferPackageRequest();

        if(memberB.isPresent()){
            Member member = memberB.get();
            Card card = cardRepository.findById("900").orElseThrow(() -> new RuntimeException("Card not found") );
            request.setToMemberId(member.getId());
            Application application = new Application();
            application.setMemberId(member.getId());
            application.setIsCoreMember(true);
            application.setCard(card);
            application.setPackageId(card.getId());

            UserProfileForm userProfileForm = new UserProfileForm();
            userProfileForm.setTitleId(member.getTitle());
            userProfileForm.setGivenName(member.getGivenName());
            userProfileForm.setMiddleName(member.getMiddleName());
            userProfileForm.setSurName(member.getSurName());
            userProfileForm.setGender(member.getGender());
            userProfileForm.setCountryOfBirthId(member.getCountryOfBirth());
            userProfileForm.setDateOfBirth(member.getDateOfBirth());
            application.setUserProfileForm(userProfileForm);

            AddressForm addressForm = new AddressForm();
            addressForm.setCountryId(member.getCountry());
            addressForm.setTelephoneNo(member.getMobileInHomeCountry());
            addressForm.setThaiTelephoneNo(member.getMobileInThai());
            addressForm.setMailAddress(member.getEmail());
            addressForm.setEmailMainContact(member.getEmail());
            application.setAddressForm(addressForm);

            PassportForm passportForm = new PassportForm();
            passportForm.setPassportPhoto(member.getPassport().getFilePassport());
            passportForm.setNationalityId(member.getNationality());
            passportForm.setPassportNo(member.getPassport().getPassportNo());
            passportForm.setIssueBy(member.getPassport().getIssueBy());
            passportForm.setIssueDate(member.getPassport().getIssueDate());
            passportForm.setExpireDate(member.getPassport().getExpireDate());
            application.setPassportForm(passportForm);

            OccupationForm occupationForm = new OccupationForm();
            occupationForm.setOccupationId(member.getOccupation());
            occupationForm.setOccupationOther(member.getOccupationOther());
            occupationForm.setTypeOfBusinessId(member.getNatureOfBusiness());
            occupationForm.setTypeOfBusinessOther(member.getNatureOfBusinessOther());

            application.setOccupationForm(occupationForm);

            application.setDocumentForm(new DocumentForm());
            application.setSurveyForm(new SurveyForm());
            application.setTermConditionForm(new TermConditionForm());
            application.setRegisterAgentId("");
            application.setStatus(ApplicationStatus.CREATE);
            application.setPackageAction(PackageAction.TRANSFER);
            application.setTransferFromId(memberA.getId());
            application = applicationRepository.save(application);
            member.setApplicationId(application.getId());
            memberRepository.save(member);

            tpw.setToName(member.getGivenName() + " " + member.getSurName());
            tpw.setNotifyUrl(fontOnlineUrl+"/member/membership/application-form");
        }else{
            tpw.setToName(email);
            tpw.setNotifyUrl(fontOnlineUrl+"/registration");
        }
        try {
            notificationService.transferPackageWelcome(email, tpw);
        } catch (IOException e) {
            e.printStackTrace();
        }

        request.setCardId(cardA.getId());
        request.setFromMemberId(memberA.getId());
        request.setToEmail(email);
        request.setStatus(TransferPackageRequestStatus.CREATE);
        request.setExpire(ZonedDateTime.now().plusDays(30));
        transferPackageRequestRepository.save(request);

        return memberA;
    }
}
