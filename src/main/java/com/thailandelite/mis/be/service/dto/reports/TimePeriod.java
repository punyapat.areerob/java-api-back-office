package com.thailandelite.mis.be.service.dto.reports;

public enum TimePeriod {
    DAY,MONTH,QUARTER,YEAR
}
