package com.thailandelite.mis.be.service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.rest.dto.task.TaskDto;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;


/**
 * Service class for managing users.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CamundaService {
    private IdentityService identityService;
    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final RepositoryService repositoryService;
    private Map<String, Collection<String>> menuList;

    @PostConstruct
    public void init() {
        try {
            this.identityService = ProcessEngines.getDefaultProcessEngine().getIdentityService();
        } catch (Exception e) {
            log.error("Init identityService fail {}", e.getMessage());
        }
    }

    public void newGroup(String groupId, String name) {
        Group group = identityService.newGroup(groupId);
        group.setName(name);
        identityService.saveGroup(group);
    }

    public void addCamundaUser(String userId, String email, String firstname, String lastname) {
        User user = identityService.newUser(userId);
        user.setEmail(email);
        user.setFirstName(firstname);
        user.setLastName(lastname);
        identityService.saveUser(user);
    }

    public void addUserToGroup(String userId, String groupId) {
        identityService.createMembership(userId, groupId);
    }

    public List<Group> getCandidateGroups() {
        return identityService.createGroupQuery().list();
    }

    public ProcessInstance startProcess(String processDefKey, String businessKey) {
        return runtimeService.startProcessInstanceByKey(processDefKey, businessKey);
    }

    public List<TaskDto> findTaskByGroup(String group) {
        return taskService.createTaskQuery().taskCandidateGroup(group).list().stream()
            .map(TaskDto::fromEntity)
            .collect(toList());
    }

    @Deprecated
    public TaskQuery createTaskQuery() {
        return taskService.createTaskQuery();
    }

    public void complete(String taskId, Map<String, Object> actionV) {
        taskService.complete(taskId, actionV);
    }

    public void claim(String taskId, String userId) {
        taskService.claim(taskId, userId);
    }

    public String getTaskIdFromProcessId(String id) {
        TaskQuery taskQuery = taskService.createTaskQuery().processInstanceId(id);
        return taskQuery.list().get(0).getId();
    }

    public List<Task> getTasks(String [] processDefKey, String group) {
        return taskService.createTaskQuery().processDefinitionKeyIn(processDefKey).taskCandidateGroup(group).list();
    }

    public Map<String, Collection<String>> getCandidateGroup() throws ParserConfigurationException, XPathExpressionException {
        if (menuList == null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("/*[local-name()='definitions']/*[local-name()='process']/*[local-name()='userTask']");

            SetMultimap<String, String> myMultimap = HashMultimap.create();

            List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().latestVersion().list();
            for (ProcessDefinition processDefinition : processDefinitions) {
                try {
                    String deploymentId = processDefinition.getDeploymentId();
                    String resourceName = processDefinition.getResourceName();

                    InputStream inputStream = repositoryService.getResourceAsStream(deploymentId, resourceName);
                    Document doc = builder.parse(inputStream);
                    NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

                    for (int i = 0; i < nodes.getLength(); i++) {
                        String nodeValue = nodes.item(i).getAttributes().getNamedItem("camunda:candidateGroups").getNodeValue();
                        String[] split = nodeValue.split(",");
                        for (String candidate : split) {
                            myMultimap.put(candidate.trim(), processDefinition.getKey());
                        }
                    }
                } catch (Exception e) {
                    log.error("Parse xml fail", e);
                }
            }
            menuList = myMultimap.asMap();
        }

        return menuList;
    }

    @Data
    @AllArgsConstructor
    public static class GroupAndProcess {
        String group;
        String processDefKey;
    }
}
