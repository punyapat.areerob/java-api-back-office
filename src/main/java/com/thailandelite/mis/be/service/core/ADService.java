package com.thailandelite.mis.be.service.core;

import com.thailandelite.mis.be.domain.User;
import com.thailandelite.mis.be.ldap.LdapHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Slf4j
@Service
public class ADService {
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private String adURL = "ldap://10.4.5.1:389";
    private String adEmail = "golfdigg@thailandelite.co.th";
    private String adPassword = "@reenju1ce";

    private static final String searchBase = "DC=thailandelite,DC=co,DC=th";
    private static final String returnedAttrs[] = {"cn", "member", "name", "memberOf","mail"};

    public void authenticate(String email, String password) throws NamingException {
        getContext(adURL, email, password);
    }

    private InitialDirContext getContext(String url, String email, String password) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, url);
        props.put(Context.SECURITY_PRINCIPAL, email);
        props.put(Context.SECURITY_CREDENTIALS, password);

        if (url.toUpperCase().startsWith("LDAPS://")) {
            props.put(Context.SECURITY_PROTOCOL, "ssl");
            props.put(Context.SECURITY_AUTHENTICATION, "simple");
            props.put("java.naming.ldap.factory.socket", "test.DummySSLSocketFactory");
        }

        return new InitialDirContext(props);
    }

    public List<MemberAD> getGroupUsers(String searchFilter) {
        List<MemberAD> list = new ArrayList<>();
        try {
            InitialDirContext ctx = getContext(adURL, adEmail, adPassword);
            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
            searchCtls.setReturningAttributes(returnedAttrs);
            try {
                NamingEnumeration<SearchResult> users = ctx.search(searchBase, searchFilter, searchCtls);
                if (!users.hasMoreElements()) {
                    log.warn("Not find any object with this filter " + searchFilter + " and searchBase " + searchBase);
                }

                String attValue;
                while (users.hasMoreElements()) {
                    SearchResult sr = users.next();
                    Attributes attrs = sr.getAttributes();
                    if (attrs.size() > 0) {
                        try {
                            for (NamingEnumeration<? extends Attribute> ae = attrs.getAll(); ae.hasMore(); ) {
                                Attribute attr = ae.next();
                                if ("member".equalsIgnoreCase(attr.getID())) {
                                    for (NamingEnumeration<?> e = attr.getAll(); e.hasMore(); ) {
                                        attValue = (String) e.next();
                                        list.add(new MemberAD(null, sr.getName(), attValue));
                                    }
                                }
                            }
                        } catch (NamingException e) {
                            log.debug("Problem listing membership:" + e);
                        }
                    } else {
                        log.warn("Could not find attribute " + returnedAttrs[0] + " for this object.");
                    }
                }
            } catch (NamingException e) {
                log.debug("Problem searching directory: " + e);
            }
            ctx.close();
        } catch (Exception namEx) {
            log.debug("Exception while fetching the users from LDAP::" + namEx);
        }
        return getEmail(list);

    }

    public List<MemberAD> getEmail(List<MemberAD> dns) {
        List<MemberAD> list = new ArrayList<>();
        try {
            InitialDirContext ctx = getContext(adURL, adEmail, adPassword);

            SearchControls searchCtls = new SearchControls();
            searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
            searchCtls.setReturningAttributes(new String[]{"email"});
            try {
                for (MemberAD m : dns) {
                    String dn = m.dn;
                    String[] query = dn.split(",");
                    NamingEnumeration<SearchResult> users = ctx.search(dn.replace(query[0] + ",", ""), query[0], searchCtls);
                    String attValue;
                    while (users.hasMoreElements()) {
                        SearchResult sr = users.next();
                        Attributes attrs = sr.getAttributes();
                        if (attrs.size() != 0) {
                            try {
                                for (NamingEnumeration<? extends Attribute> ae = attrs.getAll(); ae.hasMore(); ) {
                                    Attribute attr = ae.next();
                                    if ("mail".equalsIgnoreCase(attr.getID())) {
                                        for (NamingEnumeration<?> e = attr.getAll(); e.hasMore(); ) {
                                            attValue = (String) e.next();
                                            list.add(new MemberAD(attValue, m.group, dn));
                                        }
                                    }
                                }
                            } catch (NamingException e) {
                                log.debug("Problem listing membership:" + e);
                            }
                        }
                    }
                }

            } catch (NamingException e) {
                log.debug("Problem searching directory: " + e);
            }
            ctx.close();
            ctx = null;
        } catch (Exception namEx) {
            log.debug("Exception while fetching the users from LDAP::" + namEx);
        }

        return list;
    }

    @Data
    @AllArgsConstructor
    public class MemberAD {
        public String email;
        public String group;
        public String dn;
    }
}
