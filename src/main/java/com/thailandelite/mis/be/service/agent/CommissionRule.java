package com.thailandelite.mis.be.service.agent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommissionRule {
    Integer from;
    Integer to;
    Float commission;
}
