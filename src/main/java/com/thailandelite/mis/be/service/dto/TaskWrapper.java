package com.thailandelite.mis.be.service.dto;

import lombok.Data;
import org.apache.commons.text.WordUtils;
import org.camunda.bpm.engine.task.Task;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

@Data
public class TaskWrapper<T> {
    String id;
    String taskDefKey;
    T data;
    List<TaskAction> actions;
    String taskName;
    String taskDescription;

    public TaskWrapper(Task task, Enum<?>[] actions, T data) {
        this.id = task.getId();
        this.taskDefKey = task.getTaskDefinitionKey();
        this.taskName = task.getName();
        this.taskDescription = task.getDescription();
        this.actions = stream(actions).map(TaskAction::new).collect(toList());
        this.data = data;
    }

    @Deprecated
    public TaskWrapper(Task task, Enum<?>[] actions) {
        this.id = task.getId();
        this.taskDefKey = task.getTaskDefinitionKey();
        this.taskName = task.getName();
        this.taskDescription = task.getDescription();
        this.actions = stream(actions).map(TaskAction::new).collect(toList());
    }

    @Data
    public static class TaskAction {
        String action;
        String label;

        public TaskAction(Enum<?> e) {
            action = e.name();
            label = enumToWord(e.name());
        }
    }

    private static String enumToWord(String str) {
        return WordUtils.capitalizeFully(str, '_')
            .replaceAll("_", " ");
    }
}
