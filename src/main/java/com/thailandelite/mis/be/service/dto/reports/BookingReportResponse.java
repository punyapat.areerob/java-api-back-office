package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import lombok.Data;

@Data
public class BookingReportResponse {
    String dateTimePeriod;
    String memberId;
    Member member;
    BookingServiceMember bookingServiceMember;
    Product product;
    Integer count;
}
