package com.thailandelite.mis.be.service;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import com.thailandelite.mis.model.dto.request.VendorRegisterDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class VendorService {

    private final VendorRepository vendorRepository;
    private final VendorContractRepository contractRepository;
    private final VendorContactRepository contactRepository;
    private final VendorBillInfoRepository vendorBillInfoRepository;
    private final ShopRepository shopRepository;
    private final ProductRepository productRepository;
    private final ProductVariantGroupRepository productVariantGroupRepository;
    private final ProductVariantValueRepository productVariantValueRepository;
    private final ProductModelRepository productModelRepository;
    private final FileService fileService;
    private final VendorRegisterRequestRepository vendorRegisterRequestRepository;
    private final BookingService bookingService;
    private final NotificationService notificationService;
    private final JAService jaService;


    public Page<VendorContract> fetchVendorContract(String vendorId, Pageable pageable) {
        return contractRepository.findByVendorId(vendorId, pageable);
    }
    public Page<VendorContact> fetchVendorContact(String vendorId, Pageable pageable) {
        return contactRepository.findByVendorId(vendorId, pageable);
    }
    public Page<VendorBillInfo> fetchVendorBilling(String vendorId, Pageable pageable) {
        return vendorBillInfoRepository.findByVendorId(vendorId, pageable);
    }
    public Page<Shop> fetchVendorShop(String vendorId, Pageable pageable) {
        return shopRepository.findByVendorId(vendorId, pageable);
    }
    public List<Product> fetchVendorShopProduct(String shopId) {
        return productRepository.findByShopId(shopId);
    }

//    public List<Product> fetchVendorProductModel(String productId) {
//
//        return productRepository.findByShopId(productId);
//    }
//    public List<ProductModel> fetchVendorShopProductModel(String shopId) {
//
//       productRepository.findByShopId(shopId).stream().map(c)
//        return
//    }

//
//    List<ProductModel> productModels =null;
//        products.stream().map(c->{
//        return c
//    }).collect(Collectors.toList());
//    public VendorRequest updateVendorRequest(VendorRequest vendorRequest){
//        Vendor vendor = vendorRepository.findById(vendorRequest.getVendorId()).orElseThrow(() ->new RuntimeException("Not found vendorId"));
//        List<Shop> shop = shopRepository.findByVendorId(vendor.getId());
//        vendor.setEmail(vendorRequest.getMainContact().getEmail());
//        vendor.setTelephoneNo(vendorRequest.getMainContact().getPhone());
//        vendor.setImageanner(vendorRequest.getImage());
//        vendor.setMainContact(vendorRequest.getMainContact());
//        vendor.setAddress(vendorRequest.getAddress());
//        vendor.set
//            description
//            banner
//        ProflieImage
//        map lat long
//        return vendorRequest;
//    }
    public VendorRegisterDao vendorCreateRequest(VendorRegisterDao vendorRegisterDao){
            return vendorRegisterRequestRepository.save(vendorRegisterDao);
    }
    public VendorContract updateVendorContract(VendorContract vendorContract) {
        return contractRepository.save(vendorContract);
    }
    public VendorContact updateVendorContact(VendorContact vendorContact) {
        return contactRepository.save(vendorContact);
    }
    public VendorBillInfo updateVendorBilling(VendorBillInfo vendorBillInfo) {
        return vendorBillInfoRepository.save(vendorBillInfo);
    }
    public Shop updateVendorShop(Shop shop) {
        return shopRepository.save(shop);
    }

    public VendorContract uploadContractFile(String vendorId, String contractId, MultipartFile file) throws UploadFailException {
        VendorContract vendorContract = contractRepository.findById(contractId).orElseThrow(()->new NotFoundException("Not found contract."));
        String pathToFile = "vendors/"+vendorId+"/contracts/"+contractId+"/"+ FilenameUtils.getBaseName(file.getOriginalFilename());
        String url = fileService.upload(pathToFile, file);
        Set<String> paths = vendorContract.getContractFileList();
        if(paths == null)
            paths = new HashSet<>();
        paths.add(url);
        vendorContract.setContractFileList(paths);
        contractRepository.save(vendorContract);
        return vendorContract;
    }

    public VendorContract  removeContractFileAt(String vendorId, String contractId, int fileIndex){
        VendorContract vendorContract = contractRepository.findById(contractId).orElseThrow(()->new NotFoundException("Not found contract."));
        Set<String> paths = vendorContract.getContractFileList();
        if(paths != null) {
            paths.remove(fileIndex);
        }else{
            paths = new HashSet<>();
        }
        vendorContract.setContractFileList(paths);
        contractRepository.save(vendorContract);
        return vendorContract;
    }

    public Shop uploadShopFile(String vendorId, String shopId, MultipartFile file) throws UploadFailException {
        Shop shop = shopRepository.findById(shopId).orElseThrow(()->new NotFoundException("Not found contract."));
        String pathToFile = "vendors/"+vendorId+"/shop/"+shopId+"/images/"+ FilenameUtils.getBaseName(file.getOriginalFilename());
        String url = fileService.upload(pathToFile, file);
        Set<String> paths = shop.getImageList();
        if(paths == null)
            paths = new HashSet<>();
        paths.add(url);
        shop.setImageList(paths);
        shopRepository.save(shop);
        return shop;
    }

    public Shop removeShopFileAt(String id, String shopId, int index) {
        Shop shop = shopRepository.findById(shopId).orElseThrow(()->new NotFoundException("Not found shop."));
        Set<String> paths = shop.getImageList();
        if(paths != null) {
            paths.remove(index);
        }else{
            paths = new HashSet<>();
        }
        shop.setImageList(paths);
        shopRepository.save(shop);
        return shop;
    }

    public void deleteVendorContract(String id, String contractId) {
        contractRepository.deleteById(contractId);
    }
    public void deleteVendorContact(String id, String contactId) {
        contactRepository.deleteById(contactId);
    }

    public void deleteVendorBillInfo(String id, String billInfo) {
        vendorBillInfoRepository.deleteById(billInfo);
    }
    public void deleteVendorShop(String id, String shopId) {
        //TODO clear product.
        shopRepository.deleteById(shopId);
    }

    public VendorContract findByContractId(String contractId) {
        return contractRepository.findById(contractId).orElseThrow(()->new NotFoundException("Not found Contract"));
    }
    public VendorBillInfo findByBillingId(String billingId) {
        return vendorBillInfoRepository.findById(billingId).orElseThrow(()->new NotFoundException("Not found Billing info"));
    }
    public Shop findByShopId(String shopId) {
        return shopRepository.findById(shopId).orElseThrow(()->new NotFoundException("Not found Shop"));
    }
    public VendorContact findByContactId(String contactId) {
        return contactRepository.findById(contactId).orElseThrow(()->new NotFoundException("Not found Contact"));
    }

    public Page<Product> fetchVendorProduct(String id, Pageable pageable) {
        Page<Product> product = productRepository.findByVendorId(id, pageable);
        return product;
    }

    public Product findProductById(String productId) {
        Product product = productRepository.findById(productId).orElseThrow(()->new NotFoundException("Not found product"));
        return patchProductModel(product);
    }

    public Product updateVendorProduct(Product product) {
        return productRepository.save(patchProductModel(product));
    }

    public void deleteVendorProduct(String id, String productId) {
        productRepository.deleteById(productId);
    }

    public Page<ProductModel> fetchVendorProductModel(String vendorId, Pageable pageable) {
        return productModelRepository.findByVendorId(vendorId, pageable);
    }

    public Product uploadProductFile(String vendorId, String productId, MultipartFile file) throws UploadFailException {
        Product product = productRepository.findById(productId).orElseThrow(() -> new NotFoundException("Product"));
        String pathToFile = "vendors/"+vendorId+"/products/"+productId+"/"+ FilenameUtils.getBaseName(file.getOriginalFilename());

        String url = fileService.upload(pathToFile, file);
        product.setImage(url);
        return productRepository.save(product);
    }

    public ProductModel updateVendorProductModel(ProductModel productModel) {
        return productModelRepository.save(productModel);
    }

    public void deleteVendorProductModel(String productModelId) {
        productModelRepository.deleteById(productModelId);
    }

    public ProductModel uploadProductModelFile(String vendorId, String productModelId, MultipartFile file) throws UploadFailException {
        ProductModel productModel = productModelRepository.findById(productModelId).orElseThrow(() -> new NotFoundException("ProductModel"));
        String pathToFile = "vendors/"+vendorId+"/product-models/"+productModelId+"/"+ FilenameUtils.getBaseName(file.getOriginalFilename());

        String url = fileService.upload(pathToFile, file);
        productModel.setImages(url);
        return productModelRepository.save(productModel);
    }


    private Product patchProductModel(Product product){
        List<ProductModel> productModels = (List<ProductModel>) productModelRepository.findAllById(product.getModelIds());
        product.setModels(productModels);
        return product;
    }

    public ProductModel getVendorProductModel(String id, String modelId) {
        return productModelRepository.findById(modelId).orElseThrow(()->new NotFoundException("Not found Product Model"));
    }

    public Page<ProductVariantGroup> findAllByVendorId(String vendorId, Pageable pageable) {
        Page<ProductVariantGroup> variantGroups = productVariantGroupRepository.findAllByVendorId(vendorId, pageable);
        for (ProductVariantGroup variantGroup : variantGroups) {
            List<ProductVariantValue> variantValues = productVariantValueRepository.findByProductVariantGroupId(variantGroup.getId());
            if (variantValues == null)  variantValues = new ArrayList<>();
            variantGroup.setProductVariantValue(variantValues);
        }
        return variantGroups;
    }

    public ProductVariantGroup insert(ProductVariantGroup productVariantGroup) {
        ProductVariantGroup newVariantGroups =  productVariantGroupRepository.insert(productVariantGroup);
        for (ProductVariantValue variantValue : productVariantGroup.getProductVariantValue()) {
            variantValue.setProductVariantGroupId(newVariantGroups.getId());
            productVariantValueRepository.save(variantValue);
        }
        productVariantGroup.setId(newVariantGroups.getId());
        return productVariantGroup;
    }

    public ProductVariantGroup save(ProductVariantGroup productVariantGroup) {
        productVariantValueRepository.deleteByProductVariantGroupId(productVariantGroup.getId());

        for (ProductVariantValue variantValue : productVariantGroup.getProductVariantValue()) {
            variantValue.setProductVariantGroupId(productVariantGroup.getId());
            productVariantValueRepository.save(variantValue);
        }

        productVariantGroupRepository.save(productVariantGroup);
        return productVariantGroup;
    }
    public void VendorConfirmNoti(CaseInfo caseInfo,String type) {
        try {
            Booking booking = bookingService.getBookingDetailById(caseInfo.getEntityId());
            BookingResponse booking2 = bookingService.getWrapBookingDetailById(booking.getId());
            NotificationService.SendBookingEmail context = new NotificationService.SendBookingEmail();
            context.setBooking(booking2);
            String janame = jaService.createJA(booking.getId(),type);
            notificationService.sendBookingEmail("punyapat.areerob@mail.kmutt.ac.th", context,"ja.pdf",fileService.getFile(janame));
        } catch (UploadFailException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public String uploadFile(String vendorId, MultipartFile file) throws UploadFailException {
//        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow(()->new NotFoundException("Not found Vendor profile."));
//
//        String pathToFile = "vendors/"+vendorId+"/upload/"+FilenameUtils.getBaseName(file.getOriginalFilename());
//        String url = fileService.upload(pathToFile, file);
////        Set<String> paths = vendor.getPathUrlDocuments();
//        if(paths == null)
//            paths = new HashSet<>();
//        paths.add(url);
//        vendor.setPathUrlDocuments(paths);
//        vendorRepository.save(vendor);
//        return url;
//    }
}
