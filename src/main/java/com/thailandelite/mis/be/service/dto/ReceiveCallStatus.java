package com.thailandelite.mis.be.service.dto;

public enum ReceiveCallStatus {
    RECEIVED, MISS_CALL
}
