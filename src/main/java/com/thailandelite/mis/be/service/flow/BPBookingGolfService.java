package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.*;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBookingGolfService extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private final NotificationService notificationService;
    private final BPIncidentService bpIncidentService;
    private final VendorService vendorService;
    public BPBookingGolfService(CamundaService camundaService,
                                BookingRepository bookingRepository,
                                BookingService bookingService,
                                VendorService vendorService,
                                NotificationService notificationService,
                                CaseActivityRepository caseActivityRepository,
                                BPIncidentService bpIncidentService) {
        super(camundaService,
            new VendorConfirmBooking(bookingRepository, caseActivityRepository ,vendorService),
            new VendorBookingConfirmed(bookingRepository, caseActivityRepository),
            new ALYPendingResolve(bookingRepository, caseActivityRepository,bpIncidentService),
            new UserRecheckBooking(bookingRepository, caseActivityRepository),
            new Create_Incident_Case(bookingRepository, caseActivityRepository));
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
        this.notificationService = notificationService;
        this.bpIncidentService = bpIncidentService;
        this.vendorService = vendorService;


    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) throws UploadFailException, DocumentException, IOException {
        BookingFrom bookingFrom = (BookingFrom) object;
        Booking result = bookingService.createBooking(bookingFrom);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        result.setCaseId(this.caseInfoService.createBooking(processDefKey, VendorConfirmBooking.taskDefKey , process,result).getId());
        result.setBookingStatus(BookingStatus.CREATE);
        bookingRepository.save(result);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());

        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);

            Booking booking = bookingService.getBookingDetailById
                (caseInfo.getEntityId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder().booking(booking).build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        Booking booking = bookingService.getBookingDetailById(caseInfoDTO.getEntityId());


        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .booking(booking)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class VendorConfirmBooking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final VendorService vendorService;
        public static final String taskDefKey = "Vendor_Confirm_Booking";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE, RESUBMIT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("BOOK CONFIRM");
                    vendorService.VendorConfirmNoti(caseInfo,"GOLF");
                    caseInfo.setTaskDefKey("Vendor_Booking_Confirmed");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus("USER RECHECK");
                    caseInfo.setTaskDefKey("User_Recheck_Booking");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class VendorBookingConfirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "Vendor_Booking_Confirmed";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {USERCHANGE,USERCANCEL,NEXT,VENDORCHANGE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("WAITING CONFIRM");
                    //Gen JA service
                    caseInfo.setTaskDefKey("Vendor_Confirm_Booking");
                    break;
                case USERCANCEL:
                    //done
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
                case NEXT:
                    caseInfo.setStatus("ALY PENDING");
                    caseInfo.setTaskDefKey("ALY_Pending_Resolve");
                    break;
                case VENDORCHANGE:
                    caseInfo.setStatus("USER RECHECK");
                    caseInfo.setTaskDefKey("User_Recheck_Booking");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class ALYPendingResolve implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final BPIncidentService bpIncidentService;
        public static final String taskDefKey = "ALY_Pending_Resolve";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {INCIDENT,DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case INCIDENT:
                    caseInfo.setStatus("INCIDENT");
                    bpIncidentService.create("incident",caseInfo.getEntityId());
                    //Gen JA service
                    caseInfo.setTaskDefKey("Create_Incident_Case");
                    break;
                case DONE:
                    caseInfo.setStatus("BOOK DONE");
                    caseInfo.setTaskDefKey("");

                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class UserRecheckBooking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "User_Recheck_Booking";
        }
        public enum Action {USERCHANGE, USERCONFIRM,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("WAITING CONFIRM");
                    caseInfo.setTaskDefKey("Vendor_Confirm_Booking");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus("BOOK CONFIRMED");
                    caseInfo.setTaskDefKey("Vendor_Booking_Confirmed");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Create_Incident_Case implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Create_Incident_Case";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("BOOK DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

}
