package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import org.joda.time.Instant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.time.ZonedDateTime;
import java.util.*;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
public class SaleReportService {

    private final InvoiceRepository invoiceRepository;
    private final ApplicationRepository applicationRepository;
    private final MongoTemplate mongoTemplate;
    private final ExcelService excelService;

    public SaleReportService(InvoiceRepository invoiceRepository, ApplicationRepository applicationRepository, MongoTemplate mongoTemplate, ExcelService excelService) {
        this.invoiceRepository = invoiceRepository;
        this.applicationRepository = applicationRepository;
        this.mongoTemplate = mongoTemplate;
        this.excelService = excelService;
    }

    public Page<PaymentInfoResponse> getPaymentInfo(PaymentInfoRequest arg, Pageable pageable){
        final Query queryInfo = new Query().with(pageable);
        return PageableExecutionUtils.getPage(generatePaymentInfoReport(arg, queryInfo), pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), PaymentInfoResponse.class));
    }

    public Page<RevenueReportResponse> getRevenueGroupByAgent(BaseReportRequest arg, Pageable pageable) {
        Query queryInfo = new Query().with(pageable);
        List<RevenueReportResponse> listResult = revenueGroupByAgent(arg, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), RevenueReportResponse.class));
    }

    public Page<RevenueReportResponse> getRevenueGroupByPackage(BaseReportRequest arg, Pageable pageable) {
        Query queryInfo = new Query().with(pageable);
        List<RevenueReportResponse> listResult =revenueGroupByPackage(arg, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), RevenueReportResponse.class));
    }

    private List<RevenueReportResponse> revenueGroupByAgent(BaseReportRequest arg, Query queryInfo) {
        List<RevenueReportResponse> listResult = new ArrayList<>();
        List<Invoice> invoices = queryInvoices(queryInfo, arg.getFrom(), arg.getTo(), PaymentType.MEMBERSHIP);

        for (Invoice invoice : invoices) {
            Optional<Application> application = applicationRepository.findById(invoice.getDocumentId());
            if(!application.isPresent() || !StringUtils.hasText(application.get().getRegisterAgentId()))
                continue;
            RevenueReportResponse paymentInfoResponse = new RevenueReportResponse();
            paymentInfoResponse.setName(application.get().getAgentName());
            paymentInfoResponse.setDescription(application.get().getCard().getName());
            paymentInfoResponse.setPaidAmount(invoice.getPaidAmount());
            paymentInfoResponse.setPaidDate(invoice.getPayDate());

            listResult.add(paymentInfoResponse);
        }
        return listResult;
    }

    private List<RevenueReportResponse> revenueGroupByPackage(BaseReportRequest arg, Query queryInfo) {
        List<RevenueReportResponse> listResult = new ArrayList<>();
        List<Invoice> invoices = queryInvoices(queryInfo, arg.getFrom(), arg.getTo(), PaymentType.MEMBERSHIP);

        for (Invoice invoice : invoices) {
            Optional<Application> application = applicationRepository.findById(invoice.getDocumentId());

            if(!application.isPresent())
                continue;
            RevenueReportResponse paymentInfoResponse = new RevenueReportResponse();
            paymentInfoResponse.setName(application.get().getCard().getName());
            paymentInfoResponse.setDescription("-");
            paymentInfoResponse.setPaidAmount(invoice.getPaidAmount());
            paymentInfoResponse.setPaidDate(invoice.getPayDate());

            Member member = invoice.getMember();
            String nationality = member != null && member.getNationality() != null ? member.getNationality().getName() : "-";
            String country = member != null && member.getCountry() != null ? member.getCountry().getName() : "-";

            paymentInfoResponse.setOther1(nationality);
            paymentInfoResponse.setOther2(country);

            listResult.add(paymentInfoResponse);
        }
        return listResult;
    }

    private List<Invoice> queryInvoices(Query queryInfo, ZonedDateTime from, ZonedDateTime to, PaymentType paymentType){
        if (from !=null && to !=null)  {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(from.toString()).toDate()).lt(Instant.parse(to.toString()).toDate()));
        }
        else if (from!= null) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(from.toString()).toDate()));
        }
        else if ( to != null) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(to.toString()).toDate()));
        }
        queryInfo.addCriteria(where("status").is("SUCCESS"));
        queryInfo.addCriteria(where("pay_type").is(paymentType.name()));
        return mongoTemplate.find(queryInfo, Invoice.class);
    }

    public byte[] generateExcelPaymentInfoReport(PaymentInfoRequest arg){
        List<String> headers = Arrays.asList("Verify Date", "Invoice ID",
            "Verified amount","Total amount",
            "Fee type","Name","Transaction No.",
            "Vai","Statement","Status","Update");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        final Query queryInfo = new Query();
        List<PaymentInfoResponse> paymentInfos = generatePaymentInfoReport(arg, queryInfo);
        for (int i=0;i<paymentInfos.size();i++) {
            PaymentInfoResponse paymentInfo = paymentInfos.get(i);
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getVerifyDate()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getInvoiceId()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getVerifyAmount()));

            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getAmount()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPayType()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getMember().getGivenName()));

            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getTransactionNo()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentMethod()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getBankStatement()));

            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getStatus().name()));
            cells.add(new ExcelService.ExcelValue(paymentInfo.getPaymentTransaction().getLastModifiedDate()));

            rows.add(cells);
        }
        return excelService.generateExcelReport("Payment", headers, rows);
    }

    private List<PaymentInfoResponse> generatePaymentInfoReport(PaymentInfoRequest arg, Query queryInfo) {

        if (arg.getFrom() != null && arg.getTo() != null) {
            queryInfo.addCriteria(where("verify_date").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo().toString()).toDate()));
        } else if (arg.getFrom() != null) {
            queryInfo.addCriteria(where("verify_date").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        } else if (arg.getTo() != null) {
            queryInfo.addCriteria(where("verify_date").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }

        List<PaymentInfoResponse> listResult = new ArrayList<>();
        HashMap<String, Invoice> invoiceHashMap = new HashMap<>();
        List<PaymentTransaction> paymentTransactions = mongoTemplate.find(queryInfo, PaymentTransaction.class);

        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            String invoiceId = paymentTransaction.getInvoiceId();
            Invoice invoice = null;
            if (invoiceHashMap.containsKey(invoiceId)) {
                invoice = invoiceHashMap.get(invoiceId);
            } else {
                Optional<Invoice> optional = invoiceRepository.findById(invoiceId);
                if (optional.isPresent()) {
                    invoice = optional.get();
                    invoiceHashMap.put(invoiceId, invoice);
                }
            }
            if (invoice == null)
                continue;

            PaymentInfoResponse paymentInfo = new PaymentInfoResponse();
            paymentInfo.setPaymentTransaction(paymentTransaction);
            paymentInfo.setMember(invoice.getMember());
            paymentInfo.setPayType(invoice.getPayType());
            paymentInfo.setInvoice(invoice);
            paymentInfo.setPaymentMethod(invoice.getPaymentMethod());
            listResult.add(paymentInfo);
        }
        return listResult;
    }

    public byte[] generateExcelRevenueGroupByAgent(BaseReportRequest searchRequest) {
        List<String> headers = Arrays.asList("Paid date", "Amount",
            "Agent", "Description");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        Query queryInfo = new Query();
        List<RevenueReportResponse> listResult = revenueGroupByAgent(searchRequest, queryInfo);
        for (int i=0;i<listResult.size();i++) {
            RevenueReportResponse revenue = listResult.get(i);
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(revenue.getPaidDate()));
            cells.add(new ExcelService.ExcelValue(revenue.getPaidAmount()));
            cells.add(new ExcelService.ExcelValue(revenue.getName()));
            cells.add(new ExcelService.ExcelValue(revenue.getDescription()));
            rows.add(cells);
        }
        return excelService.generateExcelReport("RevenueGroupByAgent", headers, rows);
    }

    public byte[] generateExcelRevenueGroupByPackage(BaseReportRequest searchRequest) {
        List<String> headers = Arrays.asList("Paid date", "Amount",
            "Name","National", "Country", "Description");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        Query queryInfo = new Query();
        List<RevenueReportResponse> listResult = revenueGroupByPackage(searchRequest, queryInfo);
        for (int i=0; i<listResult.size(); i++) {
            RevenueReportResponse revenue = listResult.get(i);
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(revenue.getPaidDate()));
            cells.add(new ExcelService.ExcelValue(revenue.getPaidAmount()));
            cells.add(new ExcelService.ExcelValue(revenue.getName()));
            cells.add(new ExcelService.ExcelValue(revenue.getOther1()));
            cells.add(new ExcelService.ExcelValue(revenue.getOther2()));
            cells.add(new ExcelService.ExcelValue(revenue.getDescription()));
            rows.add(cells);
        }
        return excelService.generateExcelReport("RevenueGroupByPackage", headers, rows);
    }
}
