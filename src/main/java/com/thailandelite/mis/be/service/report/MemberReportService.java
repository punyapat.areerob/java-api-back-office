package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.repository.CardRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.utils.Generater;
import com.thailandelite.mis.model.domain.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.LongSupplier;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
public class MemberReportService {

    private final MongoTemplate mongoTemplate;
    private final MemberRepository memberRepository;
    private final CardRepository cardRepository;
    private final ExcelService excelService;

    private static String fieldGroupPackage = "packageName";
    private static String fieldGroupNationality = "nationality";
    public MemberReportService(MongoTemplate mongoTemplate, MemberRepository memberRepository, CardRepository cardRepository, ExcelService excelService) {
        this.mongoTemplate = mongoTemplate;
        this.memberRepository = memberRepository;
        this.cardRepository = cardRepository;
        this.excelService = excelService;
    }

    public Page<MemberReportResponse> getActiveMemberReport(BaseReportRequest arg, Pageable pageable) {

        final Query queryInfo = new Query().with(pageable);
        List<MemberReportResponse> listResult = generateActiveMember(arg, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    private List<MemberReportResponse> generateActiveMember(BaseReportRequest arg, Query queryInfo) {
        List<MemberReportResponse> listResult = new ArrayList<>();
        List<Membership> memberships = getMembershipList(arg, queryInfo);
        HashMap<String, List<Membership>> groupData = getGroupData(arg, memberships);
        for (String groupBy : groupData.keySet()) {
            List<Membership> groupMemberShips = groupData.get(groupBy);
            for (Membership membership : groupMemberShips) {
                MemberReportResponse memberReport = new MemberReportResponse();
                Optional<Member> memberOptional = memberRepository.findById(membership.getMemberId());
                if(memberOptional.isPresent()) {
                    memberReport.setMember(memberOptional.get());
                    memberReport.setMembership(membership);
                    memberReport.setDateTimePeriod(groupBy);
                    listResult.add(memberReport);
                }
            }
        }
        return listResult;
    }

    public Page<CountReportResponse> getMemberPackageReport(BaseReportRequest searchRequest, Pageable pageable) {
        return countMemberReport(searchRequest, pageable, fieldGroupPackage);
    }

    public Page<CountReportResponse> getMemberNationalityReport(BaseReportRequest searchRequest, Pageable pageable) {
        return countMemberReport(searchRequest, pageable, fieldGroupNationality);
    }

    public Page<CountReportResponse> countMemberReport(BaseReportRequest arg, Pageable pageable, String fieldGroupBy){
        final Query queryInfo = new Query().with(pageable);
        List<CountReportResponse> listResult = getMemberReport(arg, fieldGroupBy, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    private List<CountReportResponse> getMemberReport(BaseReportRequest arg, String fieldGroupBy, Query queryInfo) {
        List<CountReportResponse> listResult = new ArrayList<>();
        List<Membership> memberships = getMembershipList(arg, queryInfo);
        HashMap<String, HashMap<String, List<Membership>>> groupData = getGroupDataAndFiled(arg, fieldGroupBy, memberships);
        for (String groupBy : groupData.keySet()) {
            HashMap<String, List<Membership>> groupMemberShips = groupData.get(groupBy);
            for (String fieldGroupByKey : groupMemberShips.keySet()) {
                List<Membership> membershipList = groupMemberShips.get(fieldGroupByKey);

                CountReportResponse memberReport = new CountReportResponse();
                memberReport.setAmount(membershipList.size());
                memberReport.setName(fieldGroupByKey);
                memberReport.setDateTimePeriod(groupBy);
                listResult.add(memberReport);
            }
        }
        return listResult;
    }

    @NotNull
    private HashMap<String, List<Membership>> getGroupData(BaseReportRequest arg, List<Membership> memberships) {
        HashMap<String, List<Membership>> groupData = new HashMap<>();
        String groupByValue = Generater.getGroupByFormat(arg);
        for (Membership membership : memberships) {
            if(membership.getStartDate() == null)
                continue;
            String groupBy = DateUtils.toDateString(membership.getStartDate(), groupByValue);
            List<Membership> membershipList = groupData.get(groupBy);
            if(membershipList == null)
                membershipList = new ArrayList<>();
            membershipList.add(membership);

            groupData.put(groupBy, membershipList);
        }
        return groupData;
    }

    @NotNull
    private HashMap<String, HashMap<String, List<Membership>>> getGroupDataAndFiled(BaseReportRequest arg, String fieldGroupBy, List<Membership> memberships) {
        HashMap<String, HashMap<String, List<Membership>>> groupData = new HashMap<>();
        String groupByValue = Generater.getGroupByFormat(arg);
        for (Membership membership : memberships) {
            if(membership.getStartDate() == null)
                continue;
            String groupBy = DateUtils.toDateString(membership.getStartDate(), groupByValue);
            String fieldGroupByKey = getFieldGroupByKey(membership, fieldGroupBy);
            HashMap<String, List<Membership>> groupMemberShips = groupData.get(groupBy);
            if(groupMemberShips == null){
                groupMemberShips = new HashMap<>();
                groupMemberShips.put(fieldGroupByKey, new ArrayList<>());
            }
            List<Membership> membershipList = groupMemberShips.get(fieldGroupByKey);
            if(membershipList == null)
                membershipList = new ArrayList<>();
            membershipList.add(membership);
            groupMemberShips.put(fieldGroupByKey, membershipList);
            groupData.put(groupBy, groupMemberShips);
        }
        return groupData;
    }

    @NotNull
    private List<Membership> getMembershipList(BaseReportRequest arg, Query queryInfo) {
        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("startDate").gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("startDate").gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("startDate").lt(arg.getTo()));
        }
        List<Membership> memberships = mongoTemplate.find(queryInfo, Membership.class);
        return memberships;
    }

    private String getFieldGroupByKey(Membership membership, String fieldGroupBy) {
        if ( fieldGroupNationality.equalsIgnoreCase(fieldGroupBy) ) {
            Optional<Member> member = memberRepository.findById(membership.getMemberId());
            if(member.isPresent())
                return member.get().getNationality().getName();
            else
                return "-";
        }else {
            Optional<Card> card = cardRepository.findById(membership.getCardId());
            if(card.isPresent())
                return card.get().getName();
            else
                return "-";
        }
    }

    public byte[] getActiveMemberExcel(BaseReportRequest searchRequest, Pageable pageable) {
        List<String> headers = Arrays.asList("Paid date", "Package", "Amount");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        generateExcel(searchRequest, fieldGroupPackage, rows);
        return excelService.generateExcelReport("ActiveMember", headers, rows);
    }

    public byte[] getMemberPackageExcel(BaseReportRequest searchRequest, Pageable pageable) {
        List<String> headers = Arrays.asList("Period", "Card No.", "Name", "Nationality", "Country", "Gender", "Age", "Mobile", "Start at");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        final Query queryInfo = new Query();
        List<MemberReportResponse> listResult = generateActiveMember(searchRequest, queryInfo);
        for (int i=0; i<listResult.size(); i++) {
            MemberReportResponse cellValue = listResult.get(i);
            if(cellValue.getMember() != null && cellValue.getMembership() != null) {
                List<ExcelService.ExcelValue> cells = new ArrayList<>();
                String name = cellValue.getMember().getTitle().getName() + " "+ cellValue.getMember().getGivenName() + " " +cellValue.getMember().getSurName();
                String nationality = cellValue.getMember() != null && cellValue.getMember().getNationality() != null ? cellValue.getMember().getNationality().getName() : "-";
                String country = cellValue.getMember() != null && cellValue.getMember().getCountry() != null ? cellValue.getMember().getCountry().getName() : "-";
                String gender = cellValue.getMember() != null && cellValue.getMember().getGender() != null? cellValue.getMember().getGender().getName() : "-";
                cells.add(new ExcelService.ExcelValue(cellValue.getDateTimePeriod()));
                cells.add(new ExcelService.ExcelValue(cellValue.getMembership().getMembershipIdNo()));
                cells.add(new ExcelService.ExcelValue(name));
                cells.add(new ExcelService.ExcelValue(nationality));
                cells.add(new ExcelService.ExcelValue(country));
                cells.add(new ExcelService.ExcelValue(gender));
                cells.add(new ExcelService.ExcelValue(cellValue.getMember().getDateOfBirth()));
                cells.add(new ExcelService.ExcelValue(cellValue.getMember().getMobileInThai()));
                cells.add(new ExcelService.ExcelValue(cellValue.getMembership().getStartDate()));
                rows.add(cells);
            }
        }
        return excelService.generateExcelReport("MemberByPackage", headers, rows);
    }

    public byte[] getMemberNationalityExcel(BaseReportRequest searchRequest, Pageable pageable) {
        List<String> headers = Arrays.asList("Period", "Nationality", "Amount");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        generateExcel(searchRequest, fieldGroupNationality, rows);
        return excelService.generateExcelReport("MemberByNationality", headers, rows);
    }

    private void generateExcel(BaseReportRequest searchRequest, String groupBy, List<List<ExcelService.ExcelValue>> rows) {
        Query queryInfo = new Query();
        List<CountReportResponse> listResult = getMemberReport(searchRequest, groupBy, queryInfo);

        for (int i=0; i<listResult.size(); i++) {
            CountReportResponse cellValue = listResult.get(i);
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(cellValue.getDateTimePeriod()));
            cells.add(new ExcelService.ExcelValue(cellValue.getName()));
            cells.add(new ExcelService.ExcelValue(cellValue.getAmount()));
            rows.add(cells);
        }
    }
}
