package com.thailandelite.mis.be.service.flow;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBookingTicketService extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    public BPBookingTicketService(CamundaService camundaService,
                                  BookingRepository bookingRepository,
                                  BookingService bookingService,
                                  CaseActivityRepository caseActivityRepository,
                                  BPIncidentService bpIncidentService
    ) {
        super(camundaService,
            new RSVN_pending_ticket(bookingRepository, caseActivityRepository)
            );
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) throws UploadFailException, DocumentException, IOException {
        BookingFrom bookingFrom = (BookingFrom) object;
        Booking result = bookingService.createBooking(bookingFrom);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        result.setCaseId(this.caseInfoService.createBooking(processDefKey, RSVN_pending_ticket.taskDefKey , process,result).getId());
        result.setBookingStatus(BookingStatus.CREATE);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        Booking booking = bookingService.getBookingDetailById(caseInfoDTO.getEntityId());


        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .booking(booking)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class RSVN_pending_ticket implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "RSVN_pending_ticket";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {REMARK, CLOSE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case REMARK:
                    caseInfo.setStatus("PENDING RSVN");
                    //Gen JA service
                    caseInfo.setTaskDefKey("RSVN_pending_ticket");
                    break;
                case CLOSE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }



}
