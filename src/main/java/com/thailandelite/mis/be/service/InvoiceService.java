package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.common.Utils;
import com.thailandelite.mis.be.domain.User;
import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.InvoiceRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.enumeration.InvoiceStatus;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import io.github.jhipster.config.JHipsterProperties;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Locale;
import java.util.Optional;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
@RequiredArgsConstructor
public class InvoiceService {

    private final Logger log = LoggerFactory.getLogger(InvoiceService.class);

    private final InvoiceRepository invoiceRepository;
    private final MemberRepository memberRepository;
    private final ApplicationRepository applicationRepository;
    private final NotificationService notificationService;
    private final BarcodeService barcodeService;
    private final BookingService bookingService;

    private Invoice createInvoiceFromBooking(Booking booking)
    {
        Member member = booking.getRequestMember();
        Invoice invoice = new Invoice();

        invoice.setDocumentId("PENALTY-"+booking.getId());
        invoice.setMemberId(member.getId());
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setGivenName(member.getGivenName());
        memberInfo.setSurName(member.getSurName());
        invoice.setMember(member);
        invoice.setPayType(PaymentType.PENALTY);
        invoice.setPackageAction(PackageAction.PENALTY);
        invoice.setTotalAmount(booking.getBookingServiceMembers().stream().findFirst().get().getPrice());
        invoice.setTotalVat(booking.getBookingServiceMembers().stream().findFirst().get().getPrice());
        invoice.setTotalAmountIncVat(booking.getBookingServiceMembers().stream().findFirst().get().getPrice());
        invoice.setStatus(InvoiceStatus.INITIAL);
        invoice.setPaidAmount(0f);
        Invoice.OrderDetail orderDetail = new Invoice.OrderDetail();
        orderDetail.setName("PENALTY-"+booking.getId());
        orderDetail.setAmount(invoice.getTotalAmount());
        orderDetail.setAmountVat(invoice.getTotalVat());
        orderDetail.setAmountIncVat(invoice.getTotalAmountIncVat());
        orderDetail.setQuantity(1);
        ArrayList<Invoice.OrderDetail> orderDetailArrayList  = new ArrayList<>();
        orderDetailArrayList.add(orderDetail);
        invoice.setOrderDetails(orderDetailArrayList);

        return  invoice;
    }
    public void createPenaltyInvoice(String booking ) {
        invoiceRepository.save(createInvoiceFromBooking(bookingService.getBookingById(booking)));
    }

    public void sendInvoiceEmail(String invoiceId) {
        Optional<Invoice> opt = invoiceRepository.findById(invoiceId);
        if (opt.isPresent()) {
            Invoice invoice = opt.get();
            if(PackageAction.REGISTER.equals(invoice.getPackageAction())
                || PackageAction.REGISTER_BY_AGENT.equals(invoice.getPackageAction())
                ||PackageAction.RENEW.equals(invoice.getPackageAction())) {
                NotificationService.SendMembershipPaymentInvoice context = new NotificationService.SendMembershipPaymentInvoice();

                Member member = invoice.getMember();
                String toEmail = member.getEmail();
                Optional<Application> applicationOptional = applicationRepository.findById(invoice.getDocumentId());
                if (applicationOptional.isPresent()) {
                    Application application = applicationOptional.get();
                    context.setTitle(member.getTitle().getName());
                    context.setFirstName(member.getGivenName());
                    context.setLastName(member.getSurName());
                    if(invoice.getExpire() != null)
                        context.setDueDate(DateUtils.toDateString(invoice.getExpire(), "yyyy-MM-dd"));
                    context.setMemberPackage(application.getCard().getName());
                    context.setAmount("1");
                    context.setCost(Utils.numberFormat(application.getCard().getMembershipFee().getAmount()));
                    context.setSumCost(Utils.numberFormat(application.getCard().getMembershipFee().getAmount()));
                    try {
                        byte[] barcodeBytes = barcodeService.generateBarcodeImage("MEMFEE", invoice.getId(), String.valueOf(invoice.getTotalAmountIncVat()));
                        String base64 = Base64.getEncoder().encodeToString(barcodeBytes);
                        context.setBarcodeImg(base64);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        notificationService.sendMembershipPaymentInvoice(toEmail, context);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }
}
