package com.thailandelite.mis.be.service.core;

import com.thailandelite.mis.model.domain.agent.Agent;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PageQueryService {
    private final MongoTemplate mongoTemplate;

    public <T> Page<T> find(Query query, Class<T> entityClass, Pageable pageable){
        List<T> list = mongoTemplate.find(query, entityClass);
        return PageableExecutionUtils.getPage(list, pageable, () -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), entityClass));
    }
}
