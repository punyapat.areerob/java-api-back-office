package com.thailandelite.mis.be.service.dto;

import com.thailandelite.mis.model.domain.AbstractAuditingEntity;
import com.thailandelite.mis.be.domain.MockResponse;
import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.application.*;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "applicant")
public class Applicant extends AbstractAuditingEntity {
    @Id
    private String id;
    private String applicantId;
    private Boolean isCoreMember;
    private String memberId;
    private String cardPackageId;
    private UserProfileForm userProfileForm;
    private AddressForm addressForm;
    private OccupationForm occupationForm;
    private DocumentForm documentForm;
    private SurveyForm surveyForm;
    private TermConditionForm termConditionForm;
    private PassportForm passportForm;
    private RemarkForm remarkForm;
    private Card card;
    private String processInstanceId;
    private Boolean urgent;
    private Boolean seen;
    private String status;
    private MockResponse mockResponse;
}
