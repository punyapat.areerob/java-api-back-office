package com.thailandelite.mis.be.service.migrate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class MigrateUtils {
    public static ZonedDateTime zdt(Date date){
        try {
            return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        } catch (Exception e) {
            return null;
        }
    }

    public static ZonedDateTime zdt(LocalDate date) {
        try {
            return date.atStartOfDay(ZoneId.systemDefault());
        } catch (Exception e) {
            return null;
        }
    }

    public static String intToString(Integer v) {
        return v != null ? v.toString() : null;
    }

    public static Float bigDecimalToFloat(BigDecimal v) {
        return v != null ? v.floatValue() : null;
    }
}
