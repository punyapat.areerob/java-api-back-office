package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.model.domain.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.LongSupplier;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@RequiredArgsConstructor
public class ProductReportService {

    private final VendorRepository vendorRepository;
    private final ShopRepository shopRepository;
    private final MongoTemplate mongoTemplate;
    private final ExcelService excelService;

    public Page<ProductReportResponse> getProductReport(BaseReportRequest arg, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<ProductReportResponse> listResult = generateEntityList(arg, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] generateExcelProductReport(BaseReportRequest searchRequest) {
        List<String> headers = Arrays.asList("Rating", "Product", "Branch", "Vendor", "Remark");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        generateExcel(searchRequest, rows);
        return excelService.generateExcelReport("Product rating", headers, rows);
    }

    private List<ProductReportResponse> generateEntityList(BaseReportRequest arg, Query queryInfo) {
        List<ProductReportResponse> listResult = new ArrayList<>();
        List<Product> products = getEntityList(arg, queryInfo);
        for (Product product : products) {
            Optional<Vendor> vendor = vendorRepository.findById(product.getVendorId());
            if(!vendor.isPresent()) continue;
            Optional<Shop> shop = shopRepository.findById(product.getShopId());
            if(!shop.isPresent()) continue;

            ProductReportResponse productReportResponse = new ProductReportResponse();
            productReportResponse.setProduct(product);
            productReportResponse.setVendor(vendor.get());
            productReportResponse.setShop(shop.get());
            listResult.add(productReportResponse);
        }
        return listResult;
    }

    private List<Product> getEntityList(BaseReportRequest arg, Query queryInfo) {

        if ( arg.getFrom() !=null && arg.getTo() != null)  {
            queryInfo.addCriteria(where("last_modified_date").gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("last_modified_date").gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("last_modified_date").lt(arg.getTo()));
        }
        List<Product> list = mongoTemplate.find(queryInfo, Product.class);
        return list;
    }

    private void generateExcel(BaseReportRequest arg, List<List<ExcelService.ExcelValue>> rows) {
        Query queryInfo = new Query();
        List<ProductReportResponse> listResult = generateEntityList(arg, queryInfo);

        for (int i=0; i<listResult.size(); i++) {
            ProductReportResponse cellValue = listResult.get(i);
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            Product product = cellValue.getProduct();
            Vendor vendor = cellValue.getVendor();
            Shop shop = cellValue.getShop();
            cells.add(new ExcelService.ExcelValue(product.getRating()));
            cells.add(new ExcelService.ExcelValue(product.getNameEn()));
            cells.add(new ExcelService.ExcelValue(shop.getNameEn()));
            cells.add(new ExcelService.ExcelValue(vendor.getNameEn()));
            cells.add(new ExcelService.ExcelValue(product.getRemark()));
            rows.add(cells);
        }
    }
}
