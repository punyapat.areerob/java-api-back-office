package com.thailandelite.mis.be.service.core;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.model.UploadRequest;
import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

@Slf4j
@Service
public class FileService {
    public static final String BUCKET_MIS = "mis";
    private MinioClient minioClient = MinioClient.builder()
        .endpoint("elite-ub-3.golfdigg.com", 9000, false)
        .credentials("elite-access-key", "elite-access-secret123")
        .build();;

    public String upload(String pathToFile,String contentType , InputStream stream) throws UploadFailException {
        try {
            PutObjectArgs args = PutObjectArgs.builder()
                .bucket(BUCKET_MIS)
                .object(pathToFile)
                .contentType(contentType)
                .stream(stream,stream.available(), -1).build();

            ObjectWriteResponse objectWriteResponse = minioClient.putObject(args);
            log.info("ObjectWriteResponse: {}", objectWriteResponse);
            return pathToFile;
        } catch (Exception e) {
            log.error("Upload fail", e);
            throw new UploadFailException(e);
        }
    }


    public String upload(String pathToFile, MultipartFile file) throws UploadFailException {
        try {
            pathToFile = pathToFile + "." + getExtension(file);
            PutObjectArgs args = PutObjectArgs.builder()
                .bucket(BUCKET_MIS)
                .object(pathToFile)
                .contentType(file.getContentType())
                .stream(file.getInputStream(),file.getInputStream().available(), -1).build();

            ObjectWriteResponse objectWriteResponse = minioClient.putObject(args);
            log.info("ObjectWriteResponse: {}", objectWriteResponse);
            return pathToFile;
        } catch (Exception e) {
            log.error("Upload fail", e);
            throw new UploadFailException(e);
        }
    }

    public byte[] getFile(String pathToFile) throws Exception {
        GetObjectArgs getObjectArgs = GetObjectArgs.builder()
            .bucket(BUCKET_MIS)
            .object(pathToFile
            ).build();

        InputStream stream = minioClient.getObject(getObjectArgs);
        return IOUtils.toByteArray(stream);
    }

    public InputStream getFileAsStream(String pathToFile) throws Exception {
        GetObjectArgs getObjectArgs = GetObjectArgs.builder()
            .bucket(BUCKET_MIS)
            .object(pathToFile
            ).build();

        return minioClient.getObject(getObjectArgs);
    }

    public String upload(UploadRequest request) throws UploadFailException {
        return upload(request.getPath(), request.getFile());
    }

    private static String getExtension(MultipartFile file) {
        return FilenameUtils.getExtension(file.getOriginalFilename());
    }
}
