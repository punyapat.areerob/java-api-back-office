package com.thailandelite.mis.be.service.migrate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentCommissionRate;
import com.thailandelite.mis.model.domain.agent.AgentContract;
import com.thailandelite.mis.model.domain.agent.AgentContractFile;
import com.thailandelite.mis.model.domain.agent.AgentStatus;
import com.thailandelite.mis.model.domain.agent.AgentStatusGroup;
import com.thailandelite.mis.model.domain.agent.AgentTerritory;
import com.thailandelite.mis.model.domain.agent.AgentType;
import com.thailandelite.mis.be.domain.CommissionGroupIssue;
import com.thailandelite.mis.be.domain.CommissionIssue;
import com.thailandelite.mis.be.domain.CommissionIssueItem;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.migrate.domain.CmmCommissionGroupIssue;
import com.thailandelite.mis.migrate.domain.CmmCommissionIssue;
import com.thailandelite.mis.migrate.domain.CmmCommissionIssueItem;
import com.thailandelite.mis.migrate.domain.MasCommissionRate;
import com.thailandelite.mis.migrate.domain.MasCommissionRateItem;
import com.thailandelite.mis.migrate.domain.SlmAgent;
import com.thailandelite.mis.migrate.domain.SlmAgentContract;
import com.thailandelite.mis.migrate.domain.SlmAgentContractFile;
import com.thailandelite.mis.migrate.domain.SlmAgentStatus;
import com.thailandelite.mis.migrate.domain.SlmAgentStatusGroup;
import com.thailandelite.mis.migrate.domain.SlmAgentTerritory;
import com.thailandelite.mis.migrate.domain.SlmAgentType;
import com.thailandelite.mis.migrate.repository.CmmCommissionGroupIssueRepository;
import com.thailandelite.mis.migrate.repository.CmmCommissionIssueItemRepository;
import com.thailandelite.mis.migrate.repository.CmmCommissionIssueRepository;
import com.thailandelite.mis.migrate.repository.MasCommissionRateItemRepository;
import com.thailandelite.mis.migrate.repository.MasCommissionRateRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentContractFileRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentContractRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentStatusGroupRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentStatusRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentTerritoryRepository;
import com.thailandelite.mis.migrate.repository.SlmAgentTypeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Profile("migrate")
@Slf4j
@Service
@RequiredArgsConstructor
public class MigrateAgentService {
    private final MigrateCacheService cache;
    private final SlmAgentTypeRepository slmAgentTypeRepository;
    private final AgentTypeRepository agentTypeRepository;
    private final MasCommissionRateRepository masCommissionRateRepository;
    private final MasCommissionRateItemRepository masCommissionRateItemRepository;
    private final AgentCommissionRateRepository agentCommissionRateRepository;
    private final SlmAgentRepository slmAgentRepository;
    private final AgentRepository agentRepository;
    private final SlmAgentTerritoryRepository slmAgentTerritoryRepository;
    private final AgentTerritoryRepository agentTerritoryRepository;
    private final SlmAgentContractRepository slmAgentContractRepository;
    private final AgentContractRepository agentContractRepository;
    private final SlmAgentContractFileRepository slmAgentContractFileRepository;
    private final AgentContractFileRepository agentContractFileRepository;
    private final CmmCommissionGroupIssueRepository cmmCommissionGroupIssueRepository;
    private final CommissionGroupIssueRepository commissionGroupIssueRepository;
    private final CmmCommissionIssueRepository cmmCommissionIssueRepository;
    private final CommissionIssueRepository commissionIssueRepository;
    private final CmmCommissionIssueItemRepository cmmCommissionIssueItemRepository;
    private final CommissionIssueItemRepository commissionIssueItemRepository;
    private final SlmAgentStatusRepository slmAgentStatusRepository;
    private final AgentStatusRepository agentStatusRepository;
    private final SlmAgentStatusGroupRepository slmAgentStatusGroupRepository;
    private final AgentStatusGroupRepository agentStatusGroupRepository;

    public long agentType() {
        List<SlmAgentType> srcList = slmAgentTypeRepository.findAll();
        List<AgentType> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentType dest = new AgentType();
            dest.setId(src.getAgentTypeId().toString());
            dest.setNameEn(src.getAgentTypeNameEn());
            dest.setNameTh(src.getAgentTypeNameTh());
            dest.setCode(src.getAgentTypeCode());
            dest.setREMARK(src.getREMARK());
            dest.setRunningCode(src.getRunningCode());
            return dest;
        }
        ).collect(Collectors.toList());

        agentTypeRepository.deleteAll();
        return agentTypeRepository.saveAll(list).size();
    }

    public long agentCommissionRate() {
        final List<MasCommissionRateItem> rateList = masCommissionRateItemRepository.findAll();
        List<MasCommissionRate> srcList = masCommissionRateRepository.findAll();
        List<AgentCommissionRate> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentCommissionRate dest = new AgentCommissionRate();
            dest.setId(src.getCommissionRateId().toString());
            dest.setName(src.getCommissionRateName());
            dest.setRemark(src.getREMARK());

            List<AgentCommissionRate.Rate> items = rateList.stream().filter(r -> r.getCommissionRateId().equals(src.getCommissionRateId())).filter(r -> r.getRecordStatus().equals("N")).map(r -> {
                AgentCommissionRate.Rate rate = new AgentCommissionRate.Rate();
                rate.setId(r.getCommissionRateItemId().toString());
                rate.setQuantityStart(r.getQuantityStart());
                rate.setCommissionRatePercent(r.getCommissionRatePercent());
                rate.setQuantityEnd(r.getQuantityEnd());
                rate.setRemark(r.getREMARK());
                return rate;
            }).collect(Collectors.toList());
            dest.setRates(items);

            return dest;
        }).collect(Collectors.toList());

        agentCommissionRateRepository.deleteAll();
        return agentCommissionRateRepository.saveAll(list).size();
    }

    public long agent() {
        List<SlmAgent> srcList = slmAgentRepository.findAll();
        List<Agent> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            Agent dest = new Agent();

            dest.setId(src.getAgentId().toString());
            dest.setAgentCode(src.getAgentCode());
            dest.setSalesAgentTh(src.getSalesAgentTh());
            dest.setSalesAgentEn(src.getSalesAgentEn());
            dest.setSalesContactTh(src.getSalesContactTh());
            dest.setSalesContactEn(src.getSalesContactEn());

            AgentType agentType = agentTypeRepository.findById(src.getAgentTypeId().toString()).orElse(null);
            dest.setAgentType(agentType);

            dest.setContractNo(src.getContractNo());

            dest.setContractDateStart(MigrateUtils.zdt(src.getContractDateStart()));
            dest.setContractDateEnd(MigrateUtils.zdt(src.getContractDateEnd()));

            dest.setNoStreet(src.getNoStreet());

            dest.setCountry(cache.getCountry(src.getCountryId()));

            dest.setState(cache.getState(src.getStateId()));

            dest.setCity(cache.getCity(src.getCityId()));

            dest.setPostalCode(src.getPostalCode());
            dest.setAgentPhone(src.getAgentPhone());
            dest.setAgentFax(src.getAgentFax());
            dest.setAgentWebsite(src.getAgentWebsite());
            dest.setAgentEmail(src.getAgentEmail());

            if (src.getAgentStatus() != null)
                dest.setAgentStatus("ACTIVE".equals(src.getAgentStatus()) ? Agent.AgentStatus.ACTIVE : Agent.AgentStatus.INACTIVE);

            if (src.getAgentStatusRegister().equals("DRAFT"))
                dest.setAgentRegisterStatus(Agent.AgentRegisterStatus.DRAFT);
            else if (src.getAgentStatusRegister().equals("WAITING"))
                dest.setAgentRegisterStatus(Agent.AgentRegisterStatus.WAITING);
            else if (src.getAgentStatusRegister().equals("APPROVE"))
                dest.setAgentRegisterStatus(Agent.AgentRegisterStatus.APPROVE);
            else if (src.getAgentStatusRegister().equals("NOT_APPROVE"))
                dest.setAgentRegisterStatus(Agent.AgentRegisterStatus.NOT_APPROVE);

            dest.setAgentFileBookBank(src.getAgentFileBookBank());
            dest.setAgentFilePassport(src.getAgentFilePassport());
            dest.setCompanyProfile(src.getCompanyProfile());
            dest.setWebAgentId(src.getWebAgentId());
            dest.setRemarkAgentCode(src.getRemarkAgentCode());

            if (src.getVatType() != null)
                dest.setVat("T".equals(src.getVatType()));

            if (src.getCompanyRegisInThai() != null)
            dest.setCompanyRegisInThai("T".equals(src.getCompanyRegisInThai()));

            dest.setTaxId(src.getTaxId());
            dest.setRemark(src.getREMARK());

            return dest;
        }).collect(Collectors.toList());

        agentRepository.deleteAll();
        return agentRepository.saveAll(list).size();
    }

    public long agentTerritory() {
        List<SlmAgentTerritory> srcList = slmAgentTerritoryRepository.findAll();
        List<AgentTerritory> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentTerritory dest = new AgentTerritory();
            dest.setId(src.getAgentTerritoryId().toString());
            dest.setRemark(src.getREMARK());
            dest.setTerritoryId(src.getTerritoryId());

            dest.setAgent(src.getAgentId() != null ? agentRepository.findById(src.getAgentId().toString()).orElse(null) : null);

            return dest;
        }).collect(Collectors.toList());

        agentTerritoryRepository.deleteAll();
        return agentTerritoryRepository.saveAll(list).size();
    }

    public long agentContract() {
        final List<SlmAgentContractFile> fileList = slmAgentContractFileRepository.findAll();
        List<SlmAgentContract> srcList = slmAgentContractRepository.findAll();
        List<AgentContract> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentContract dest = new AgentContract();
            dest.setId(src.getAgentContractId().toString());

            dest.setAgent(src.getAgentId() != null ? agentRepository.findById(src.getAgentId().toString()).orElse(null) : null);

            dest.setContractDateStart(MigrateUtils.zdt(src.getContractDateStart()));
            dest.setContractDateEnd(MigrateUtils.zdt(src.getContractDateEnd()));
            dest.setTarget(src.getTARGET());
            dest.setTargetFirstYear(src.getTargetFirstYear());
            dest.setTargetSecondYear(src.getTargetSecondYear());

            if (src.getAgentContractStatus() != null)
                dest.setContractStatus("T".equals(src.getAgentContractStatus()));

            dest.setAgentCommissionRate(src.getCommissionRateId() != null ? agentCommissionRateRepository.findById(src.getCommissionRateId().toString()).orElse(null) : null);

            List<AgentContract.File> items = fileList.stream().filter(r -> r.getAgentContractId().equals(src.getAgentContractId())).filter(r -> r.getRecordStatus().equals("N")).map(r -> {
                AgentContract.File file = new AgentContract.File();
                file.setId(r.getAgentContractFileId().toString());
                file.setAgentContractFile(r.getAgentContractFile());
                file.setAgentContractFileName(r.getAgentContractFileName());
                file.setAgentContractFileOrder(r.getAgentContractFileOrder());
                file.setRemark(r.getREMARK());
                return file;
            }).collect(Collectors.toList());
            dest.setFiles(items);
            
            dest.setRemark(src.getREMARK());

            return dest;
        }).collect(Collectors.toList());

        agentContractRepository.deleteAll();
        return agentContractRepository.saveAll(list).size();
    }

    public long agentContractFile() {
        List<SlmAgentContractFile> srcList = slmAgentContractFileRepository.findAll();
        List<AgentContractFile> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentContractFile dest = new AgentContractFile();
            dest.setId(src.getAgentContractFileId().toString());

            dest.setAgentContract(src.getAgentContractId() != null ? agentContractRepository.findById(src.getAgentContractId().toString()).orElse(null) : null);

            dest.setAgent(src.getAgentId() != null ? agentRepository.findById(src.getAgentId().toString()).orElse(null) : null);

            dest.setAgentContractFile(src.getAgentContractFile());
            dest.setAgentContractFileName(src.getAgentContractFileName());
            dest.setAgentContractFileOrder(src.getAgentContractFileOrder());
            dest.setRemark(src.getREMARK());

            return dest;
        }).collect(Collectors.toList());

        agentContractFileRepository.deleteAll();
        return agentContractFileRepository.saveAll(list).size();
    }

    public long commissionGroupIssue() {
        List<CmmCommissionGroupIssue> srcList = cmmCommissionGroupIssueRepository.findAll();
        List<CommissionGroupIssue> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            CommissionGroupIssue dest = new CommissionGroupIssue();
            dest.setId(src.getCommissionGroupIssueId().toString());
            dest.setDocNo(src.getDocNo());
            dest.setInvoiceReceiveDate(MigrateUtils.zdt(src.getInvoiceReceiveDate()));
            dest.setIssueStatusCode(src.getIssueStatusCode());

            dest.setAgent(src.getAgentId() != null ? agentRepository.findById(src.getAgentId().toString()).orElse(null) : null);

            dest.setRemark(src.getREMARK());

            return dest;
        }).collect(Collectors.toList());

        commissionGroupIssueRepository.deleteAll();
        return commissionGroupIssueRepository.saveAll(list).size();
    }

    public long commissionIssue() {
        List<CmmCommissionIssue> srcList = cmmCommissionIssueRepository.findAll();
        List<CommissionIssue> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            CommissionIssue dest = new CommissionIssue();
            dest.setId(src.getCommissionIssueId().toString());

            dest.setDocNo(src.getDocNo());

            dest.setDocDate(MigrateUtils.zdt(src.getDocDate()));

            dest.setAgent(src.getAgentId() != null ? agentRepository.findById(src.getAgentId().toString()).orElse(null) : null);

            if (src.getIssueFile() != null) {
                List<String> issueFiles = new ArrayList<String>();
                issueFiles.add(src.getIssueFile());

                if (src.getIssueFile2() != null)
                    issueFiles.add(src.getIssueFile2());

                if (src.getIssueFile3() != null)
                    issueFiles.add(src.getIssueFile3());

                if (src.getIssueFile4() != null)
                    issueFiles.add(src.getIssueFile4());

                dest.setIssueFiles(issueFiles);
            }

            dest.setIssueStatusCode(src.getIssueStatusCode());

            dest.setInvoiceNo(src.getInvoiceNo());

            dest.setInvoiceDate(MigrateUtils.zdt(src.getInvoiceDate()));

            dest.setInvoiceAmount(MigrateUtils.bigDecimalToFloat(src.getInvoiceAmount()));

            dest.setCommissionGroupIssue(src.getCommissionGroupIssueId() != null ? commissionGroupIssueRepository.findById(src.getCommissionGroupIssueId().toString()).orElse(null) : null);

            dest.setIssueStatusGroupPayment(src.getIssueStatusGroupPayment());

            dest.setOptionVat(src.getOptionVat());

            dest.setRemark(src.getREMARK());

            return dest;
        }).collect(Collectors.toList());

        commissionIssueRepository.deleteAll();
        return commissionIssueRepository.saveAll(list).size();
    }

    public long commissionIssueItem() {
        List<CmmCommissionIssueItem> srcList = cmmCommissionIssueItemRepository.findAll();
        List<CommissionIssueItem> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            CommissionIssueItem dest = new CommissionIssueItem();

            dest.setId(src.getCommissionIssueItemId().toString());

            dest.setCommissionIssue(cache.getCommissionIssue(src.getCommissionIssueId()));

            dest.setCommissionRatePercent(src.getCommissionRatePercent());

            dest.setMemberId(MigrateUtils.intToString(src.getMemberId()));

            dest.setCommissionAmount(MigrateUtils.bigDecimalToFloat(src.getCommissionAmount()));

            dest.setInvoiceNo(src.getInvoiceNo());

            dest.setInvoiceDate(MigrateUtils.zdt(src.getInvoiceDate()));

            dest.setCommissionOrder(src.getCommissionOrder());

            dest.setVatType(src.getVatType());

            dest.setCurrency(src.getCURRENCY());

            dest.setRate(MigrateUtils.bigDecimalToFloat(src.getRATE()));

            dest.setMembershipBeforeVatAmount(MigrateUtils.bigDecimalToFloat(src.getMembershipBeforeVatAmount()));

            dest.setMembershipVatAmount((MigrateUtils.bigDecimalToFloat(src.getMembershipVatAmount())));

            dest.setMembershipTotalAmount(MigrateUtils.bigDecimalToFloat(src.getMembershipTotalAmount()));

            dest.setCommissionBeforeVatAmount(MigrateUtils.bigDecimalToFloat(src.getCommissionBeforeVatAmount()));

            dest.setCommissionVatAmount(MigrateUtils.bigDecimalToFloat(src.getCommissionVatAmount()));

            dest.setCommissionTotalAmount(MigrateUtils.bigDecimalToFloat(src.getCommissionTotalAmount()));

            return dest;
        }).collect(Collectors.toList());

        commissionIssueItemRepository.deleteAll();
        return commissionIssueItemRepository.saveAll(list).size();
    }

    public long agentStatus() {
        List<SlmAgentStatus> srcList = slmAgentStatusRepository.findAll();
        List<AgentStatus> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentStatus dest = new AgentStatus();

            dest.setId(src.getAgentStatusId().toString());

            dest.setAgentStatusCode(src.getAgentStatusCode());

            dest.setAgentStatusNameTh(src.getAgentStatusNameTh());

            dest.setAgentStatusNameEn(src.getAgentStatusNameEn());

            dest.setAgentStatusOrder(src.getAgentStatusOrder());

            dest.setCancelAgentStatusId(MigrateUtils.intToString(src.getCancelAgentStatusId()));

            dest.setNextAgentStatusId(MigrateUtils.intToString(src.getNextAgentStatusId()));

            dest.setAgentButtonNextStatusNameTh(src.getAgentButtonNextStatusNameTh());

            dest.setAgentButtonNextStatusNameEn(src.getAgentButtonNextStatusNameEn());

            dest.setAgentButtonCancelStatusNameTh(src.getAgentButtonCancelStatusNameTh());

            dest.setAgentButtonCancelStatusNameEn(src.getAgentButtonCancelStatusNameEn());

            dest.setEditable(src.getEDITABLE());

            return dest;
        }).collect(Collectors.toList());

        agentStatusRepository.deleteAll();
        return agentStatusRepository.saveAll(list).size();
    }

    public long agentStatusGroup() {
        List<SlmAgentStatusGroup> srcList = slmAgentStatusGroupRepository.findAll();
        List<AgentStatusGroup> list = srcList.stream().filter(src -> src.getRecordStatus().equals("N")).map(src -> {
            AgentStatusGroup dest = new AgentStatusGroup();

            dest.setId(src.getAgentStatusGroupId().toString());

            dest.setAgentStatus(src.getAgentStatusId() != null ? agentStatusRepository.findById(src.getAgentStatusId().toString()).orElse(null) : null);

            dest.setSysGroupId(src.getSysGroupId() != null ? src.getSysGroupId().toString() : null);

            dest.setCanView(src.getCanView());

            dest.setCanApprove(src.getCanApprove());

            dest.setGroupEditable(src.getGroupEditable());

            dest.setGroupPending(src.getGroupPending());

            dest.setGroupAssign(src.getGroupAssign());

            dest.setGroupAmend(src.getGroupAmend());

            return dest;
        }).collect(Collectors.toList());

        agentStatusGroupRepository.deleteAll();
        return agentStatusGroupRepository.saveAll(list).size();
    }
}
