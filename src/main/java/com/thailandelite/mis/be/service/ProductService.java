package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.domain.SubProductCategory;
import com.thailandelite.mis.be.repository.ProductRepository;
import com.thailandelite.mis.be.repository.ProductModelRepository;
import com.thailandelite.mis.be.repository.ProductVariantValueRepository;
import com.thailandelite.mis.be.repository.SubProductCategoryRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.ProductModel;
import com.thailandelite.mis.model.domain.ProductVariantValue;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductModelRepository productModelRepository;
    private final ProductVariantValueRepository productVariantValueRepository;
    private final SubProductCategoryRepository subProductCategoryRepository;
//    private final ProductCategoryRepository productCategoryRepository;

    public List<SubProductCategory> getSubCatByCategoryId(String id) {
//        ProductCategory productCategory = productCategoryRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found"));

//        List<Product> productList = new ArrayList<>();
        List<SubProductCategory> subCats = subProductCategoryRepository.findByProductCategoryId(id);

//        for (SubProductCategory subCat : subCats) {
//            List<Product> products = productRepository.findBySubProductCategoryId(subCat.getId());
//            subCat.setProductLists(Sets.newHashSet(products));
//        }

        return subCats;
    }
    public Product createProduct(Product product) {
        Product result = productRepository.save(product);
        List<ProductModel> productModel= productModelRepository.saveAll(product.getModels());
        List<String> modelId = new ArrayList<>();

        productModel.stream().map(c->{
            modelId.add(c.getId());
            return c;
        }).collect(Collectors.toList());
        result.setModelIds(modelId);
        productModel.stream().map(c->{
            List<String> productVariantName = new ArrayList<>();
            c.getProductVariantValue().stream().map(b-> {
                ProductVariantValue productVariantValue= productVariantValueRepository.findById(b).orElseThrow(() -> new RuntimeException("Not found"));
                productVariantName.add(productVariantValue.getName());
                return 0;
            }).collect(Collectors.toList());
            c.setProductVariantName(productVariantName);
            productModelRepository.save(c);
            return 0;
        }).collect(Collectors.toList());
        productRepository.save(result);
        return result;
    }
    public List<Product> getAllProduct() {
        List<Product> result = productRepository.findAll().stream().map(product-> {
                List<ProductModel> productModels = product.getModelIds().stream().map(modelId -> {
                    return productModelRepository.findById(modelId).orElseThrow(() -> new RuntimeException("Not found"));
                }).collect(Collectors.toList());
                product.setModels(productModels);
                return product;
            }).collect(Collectors.toList());
        return result;
    }
    public Product getProductDetail(String productId) {
        Product product = productRepository.findById(productId).orElseThrow(()->new NotFoundException("Not found product"));
        List<ProductModel> productModels = (List<ProductModel>) productModelRepository.findAllById(product.getModelIds());
        product.setModels(productModels);
        return product;
    }
}
