package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.domain.CommissionIssue;
import com.thailandelite.mis.be.repository.AgentContractRepository;
import com.thailandelite.mis.be.repository.AgentRepository;
import com.thailandelite.mis.be.repository.CommissionIssueRepository;
import com.thailandelite.mis.be.service.core.PageQueryService;
import com.thailandelite.mis.be.service.helper.QueryHelper;
import com.thailandelite.mis.be.web.rest.AgentResource;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentContract;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.thailandelite.mis.be.service.helper.QueryHelper.containing;
import static com.thailandelite.mis.be.service.helper.QueryHelper.or;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Slf4j
@Service
@RequiredArgsConstructor
public class AgentService {
    private final AgentRepository agentRepository;
    private final AgentContractRepository agentContractRepository;
    private final MongoTemplate mongoTemplate;
    private final PageQueryService pageQueryService;

    private final CommissionIssueRepository commissionIssueRepository;

    public String doDoSomething(){
        log.debug("doDoSomething {}", new Date());
        return "Hello";
    }

    public Float getCommissionPercentByAgent(String agentId, Integer member) {
        return getContractByAgent(agentId).getAgentCommissionRate().getRates().stream().filter(r -> r.getQuantityStart() <= member && r.getQuantityEnd() >= member).findFirst().orElseThrow(() -> new RuntimeException("Member out of range.")).getCommissionRatePercent();
    }

    public AgentContract getContractByAgent(String agentId) {
        return agentContractRepository.findByAgent(agentRepository.findById(agentId).orElseThrow(() -> new RuntimeException("Agent not found."))).orElseThrow(() -> new RuntimeException("Contract not found."));
    }

    public List<MemberActivation> getActivateMemberListByAgent(String agentId) {
        List<MemberActivation> lists = new ArrayList<>();
        lists.add(new MemberActivation("1", "A", "A", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 1000000f, 65420.56f, 934579.44f, 61));
        lists.add(new MemberActivation("2", "B", "B", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 62));
        lists.add(new MemberActivation("3", "C", "C", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 63));
        lists.add(new MemberActivation("4", "D", "D", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 64));
        lists.add(new MemberActivation("5", "E", "E", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 65));
        lists.add(new MemberActivation("6", "F", "F", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 1000000f, 65420.56f, 934579.44f, 66));
        lists.add(new MemberActivation("7", "G", "G", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 67));
        lists.add(new MemberActivation("8", "H", "H", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 1000000f, 65420.56f, 934579.44f, 68));
        lists.add(new MemberActivation("9", "I", "I", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 69));
        lists.add(new MemberActivation("10", "J", "J", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 70));
        lists.add(new MemberActivation("11", "K", "K", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 800000f, 52336.45f, 747663.55f, 71));
        lists.add(new MemberActivation("12", "L", "L", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 0f, 0f, 0f, 72));
        lists.add(new MemberActivation("13", "M", "M", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 73));
        lists.add(new MemberActivation("14", "N", "N", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 74));
        return lists;
    }

    public List<MemberActivation> calCommisstion(String agentId, List<MemberActivation> lists) {
        Agent agent = agentRepository.findById(agentId).orElseThrow(() -> new RuntimeException("Agent not found."));
        for (MemberActivation list: lists) {
            list.setCommissionPercent(getCommissionPercentByAgent(agentId, list.getSequence()));
            if (list.getMemberFee().equals(0f))
                continue;
            list.setCommissionAmount(0.01f*Math.round(list.getMemberFeeNet()*list.getCommissionPercent()));
            if (agent.getVat() != null)
                list.setCommissionVat(agent.getVat() ? list.getCommissionAmount()*0.07f : 0f);
            else
                list.setCommissionVat(0f);
            list.setCommissionNet(list.getCommissionAmount()-list.getCommissionVat());
        }
        return lists;
    }

    public void getIss(String id, Pageable pageable){
        Page<CommissionIssue> commissionIssues = this.commissionIssueRepository.findAllByAgent_Id(id, pageable);
    }

    public Page<Agent> search(AgentResource.AgentQuery param, Pageable pageable) {
        Query query = new QueryHelper(pageable)
            .and(param.getAgentTypeId(), (value) -> where("agentType.id").is(value))
            .and(param.getAgentStatus(), (value) -> where("agentStatus").is(value))
            .and(param.getAgentCode(), (value) -> where("agentCode").regex(containing(value)))
            .and(param.getAgentPhone(), (value) -> where("phone").regex(containing(value)))
            .and(param.getAgentEmail(), (value) -> where("email").regex(containing(value)))
            .and(param.getName(), (value) -> or(
                where("salesAgentTh").regex(containing(value)),
                where("salesAgentEn").regex(containing(value)))
            )
            .build();

        return pageQueryService.find(query, Agent.class, pageable);
    }

    @Data
    @RequiredArgsConstructor
    public static class MemberActivation {
        final String memberId;
        final String memberCode;
        final String name;
        final ZonedDateTime activateDate;
        final Float memberFee;
        final Float memberFeeVat;
        final Float memberFeeNet;
        final Integer sequence;

        Float commissionPercent;
        Float commissionAmount;
        Float commissionVat;
        Float commissionNet;

        String remark;
    }
}
