package com.thailandelite.mis.be.service;


import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.enumeration.CaseEntityType;
import com.thailandelite.mis.be.repository.CardPrivilegeRepository;
import com.thailandelite.mis.be.repository.CaseInfoRepository;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQuery;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.model.dto.request.VendorProfileUpdateDao;
import com.thailandelite.mis.model.dto.request.VendorRegisterDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.jetbrains.annotations.NotNull;
import org.joda.time.Instant;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.util.StringUtils.hasText;

@Slf4j
@Service
@RequiredArgsConstructor
public class CaseInfoService {
    private final SequenceGeneratorService sequenceGeneratorService;
    private final CaseInfoRepository caseInfoRepository;
    private final CamundaService camundaService;
    private final ModelMapper modelMapper = new ModelMapper();
    private CardPrivilegeRepository cardPrivilegeRepository;
    private final MongoTemplate mongoTemplate;


    @NotNull
    private CaseInfo createCaseInfo(String processDefKey, String taskDefKey, ProcessInstance process, String caseNo) {
        CaseInfo caseInfo = new CaseInfo();
        caseInfo.setCaseNo(caseNo);
        caseInfo.setFlowDefKey(processDefKey);
        caseInfo.setTaskDefKey(taskDefKey);
        caseInfo.setProcessInstanceId(process.getId());
        caseInfo.setUrgent(false);
        caseInfo.setIsSeen(false);
        return caseInfo;
    }

    public void createMemberUpgradeCase(String processDefKey, String taskDefKey, ProcessInstance process, Application application) {
        String caseNo = "UPGRADE-" + sequenceGeneratorService.generateSequence("UPGRADE");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("PENDING CRM CHECK DOC");
        caseInfo.setEntityType(CaseEntityType.APPLICATION);
        caseInfo.setEntityId(application.getId());
        caseInfoRepository.insert(caseInfo);
    }


    public CaseInfo createJobApplicationCase(String processDefKey, String taskDefKey, ProcessInstance process, JobApplication application) {
        String caseNo = "JOBAPP-" + sequenceGeneratorService.generateSequence("JOBAPP");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("PENDING HR REVIEW");
        caseInfo.setEntityType(CaseEntityType.JOBAPPLICATION);
        caseInfo.setEntityId(application.getId());
        caseInfoRepository.insert(caseInfo);
        return caseInfo;

    }
    public void createMemberTypeChangeCase(String processDefKey, String taskDefKey, ProcessInstance process, Member membership) {
        String caseNo = "TYPECHANGE-" + sequenceGeneratorService.generateSequence("TYPECHANGE");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("PENDING CRM CHECK DOC");
        caseInfo.setEntityType(CaseEntityType.APPLICATION);
        caseInfo.setEntityId(membership.getId());
        caseInfoRepository.insert(caseInfo);
    }


    public void createMemberTypeChangeCase(String processDefKey, String taskDefKey, ProcessInstance process, Application application) {
        String caseNo = "TYPECHANGE-" + sequenceGeneratorService.generateSequence("TYPECHANGE");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("PENDING CRM CHECK DOC");
        caseInfo.setEntityType(CaseEntityType.APPLICATION);
        caseInfo.setEntityId(application.getId());
        caseInfoRepository.insert(caseInfo);
    }




    public void createMemberTransferCase(String processDefKey, String taskDefKey, ProcessInstance process, Application application) {
        String caseNo = "TRANSFER-" + sequenceGeneratorService.generateSequence("TRANSFER");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("PENDING CRM CHECK DOC");
        caseInfo.setEntityType(CaseEntityType.APPLICATION);
        caseInfo.setEntityId(application.getId());
        caseInfoRepository.insert(caseInfo);
    }

    public void createMemberActivationCase(String processDefKey, String taskDefKey, ProcessInstance process, Membership membership, String status, PackageAction packageAction) {
        String caseNo = "ACTIVATE-" + sequenceGeneratorService.generateSequence("ACTIVATE");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus(status);
        caseInfo.setEntityType(CaseEntityType.MEMBERSHIP);
        caseInfo.setPackageAction(packageAction);
        caseInfo.setEntityId(membership.getId());
        caseInfoRepository.insert(caseInfo);
    }

    public CaseInfo createApplication(String processDefKey, String taskDefKey, ProcessInstance process, Application application) {
        String caseNo = "APP-" + sequenceGeneratorService.generateSequence("APP");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("SLS PROCESS");
        caseInfo.setEntityType(CaseEntityType.APPLICATION);
        caseInfo.setEntityId(application.getId());
        return caseInfoRepository.insert(caseInfo);

    }


    public void createIncident(String processDefKey, ProcessInstance process, Booking booking,String taskDefKey) {
        String caseNo = "INCIDENT-" + sequenceGeneratorService.generateSequence("Incident");
        CaseInfo caseInfo = new CaseInfo();
        caseInfo.setCaseNo(caseNo);
        caseInfo.setFlowDefKey(processDefKey);
        caseInfo.setProcessInstanceId(process.getId());
        caseInfo.setUrgent(false);
        caseInfo.setIsSeen(false);
        caseInfo.setStatus("Pending Incident");
        caseInfo.setTaskDefKey(taskDefKey);
        caseInfo.setEntityType(CaseEntityType.MEMBER_PAYMENT);
        caseInfo.setEntityId(booking.getId());

        caseInfoRepository.insert(caseInfo);
    }


    public void createMemberPayment(String processDefKey, ProcessInstance process, MemberPayment memberPayment,String taskDefKey) {
        String caseNo = "MemberPayment-" + sequenceGeneratorService.generateSequence("MemberPayment");
        CaseInfo caseInfo = new CaseInfo();
        caseInfo.setCaseNo(caseNo);
        caseInfo.setFlowDefKey(processDefKey);
        caseInfo.setProcessInstanceId(process.getId());
        caseInfo.setUrgent(false);
        caseInfo.setIsSeen(false);
        caseInfo.setStatus("Pending FIN");
        caseInfo.setTaskDefKey(taskDefKey);
        caseInfo.setEntityType(CaseEntityType.MEMBER_PAYMENT);
        caseInfo.setEntityId(memberPayment.getId());

        caseInfoRepository.insert(caseInfo);
    }

    public CaseInfo createIncident(String processDefKey, String taskDefKey, ProcessInstance process, String id) {
        String caseNo = "INCIDENT-" + sequenceGeneratorService.generateSequence("INCIDENT"
        );
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("Waiting");
        caseInfo.setEntityType(CaseEntityType.INCIDENT);
        caseInfo.setEntityId(id);
        return  caseInfoRepository.insert(caseInfo);
    }

    public CaseInfo createBooking(String processDefKey, String taskDefKey, ProcessInstance process, Booking booking) {
        String caseNo = "Booking-" + sequenceGeneratorService.generateSequence("Booking");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("Waiting");
        caseInfo.setEntityType(CaseEntityType.BOOKINGS);
        caseInfo.setEntityId(booking.getId());
        return  caseInfoRepository.insert(caseInfo);
    }
    public CaseInfo createVendorRequest(String processDefKey, String taskDefKey, ProcessInstance process, VendorRegisterDao vendorRegisterDao) {
        String caseNo = "VendorRequest-" + sequenceGeneratorService.generateSequence("VendorRequest");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("Waiting");
        caseInfo.setEntityType(CaseEntityType.VENDORREQUEST);
        caseInfo.setEntityId(vendorRegisterDao.getId());
        return  caseInfoRepository.insert(caseInfo);
    }
    public CaseInfo EditVendorRequest(String processDefKey, String taskDefKey, ProcessInstance process, VendorProfileUpdateDao vendorProfileUpdateDao) {
        String caseNo = "EditVendorRequest-" + sequenceGeneratorService.generateSequence("EditVendorRequest");
        CaseInfo caseInfo = createCaseInfo(processDefKey, taskDefKey, process, caseNo);
        caseInfo.setStatus("Waiting");
        caseInfo.setEntityType(CaseEntityType.EDITVENDOR);
        caseInfo.setEntityId(vendorProfileUpdateDao.getId());
        return  caseInfoRepository.insert(caseInfo);
    }


    public List<String> getActiveProcessIds(String [] processDefKey,
                                            String candidateGroup) {
        return camundaService.getTasks(processDefKey, candidateGroup).stream()
            .map(Task::getProcessInstanceId)
            .collect(toList());
    }

    public long queryUnseen(CaseQuery arg) {

        final Query query = new Query();

        if (hasText(arg.getFlowDefKey())) {
            query.addCriteria(where("flowDefKey").is(arg.getFlowDefKey()));
        }

        if (hasText(arg.getPackageAction())) {
            query.addCriteria(where("packageAction").is(arg.getPackageAction()));
        }

        return mongoTemplate.find(query, CaseInfo.class).size();
    }


    public Page<CaseInfo> queryByEntityByEntity(CaseQuery arg) {

        List<String> entityList = new ArrayList<>();
        final Query query = new Query().with(arg.getPageable());
        if (hasText(arg.getFrom()) && hasText(arg.getTo()) ) {
            query.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom()).toDate()).lt(Instant.parse(arg.getTo()).toDate()));
        }
        else if (hasText(arg.getFrom())) {
            query.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom()).toDate()));
        }
        else if (hasText(arg.getTo())) {
            query.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo()).toDate()));
        }

        if (hasText(arg.getStatus())) {
            query.addCriteria(where("status").is(arg.getStatus()));
        }


        query.addCriteria(where("entityType").is("BOOKINGS"));
        return PageableExecutionUtils.getPage(mongoTemplate.find(query, CaseInfo.class), arg.getPageable(), () -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), com.thailandelite.mis.be.domain.CaseInfo.class));

    }

    public Page<CaseInfo> queryByEntityByClass(CaseQuery arg,Class c) {
        List<String> list = new ArrayList<>();
        List<String> entityList = new ArrayList<>();
        final Query query = new Query().with(arg.getPageable());
        if (c.getName().contains("Incident")) {
            if (arg.getStatuses()!= null &&  arg.getStatuses().stream().count() >0) {
                query.addCriteria(where("entityType").is("INCIDENT"));
                query.addCriteria(where("status").in(arg.getStatuses()));

                entityList = mongoTemplate.find(query, CaseInfo.class).stream().map(CaseInfo::getEntityId).collect(toList());
                if(entityList.size() ==0 ) {
                    entityList.add("-1");
                }
            }

        }
        else
        if(hasText(arg.getName()) || hasText(arg.getSurname())) {

            if (c.getName().contains("Member")) {
                if (hasText(arg.getName())) {
                    query.addCriteria(where("givenName").regex(".*"+arg.getName()+".*"));
                }

                if (hasText(arg.getSurname())) {
                    query.addCriteria(where("surName").regex(".*"+arg.getSurname()+".*"));
                }

                list = mongoTemplate.find(query, Member.class).stream().map(Member::getId).collect(toList());
                final Query queryMemberShip = new Query().with(arg.getPageable());
                if (list.size() > 0) {
                    queryMemberShip.addCriteria(where("memberId").in(list));
                }
                entityList = mongoTemplate.find(queryMemberShip, Membership.class).stream().map(Membership::getId).collect(toList());
            } else if (c.getName().contains("MemberPayment")) {
                if (hasText(arg.getName())) {
                    query.addCriteria(where("givenName").regex(".*"+arg.getName()+".*"));
                }

                if (hasText(arg.getSurname())) {
                    query.addCriteria(where("surName").regex(".*"+arg.getSurname()+".*"));
                }

                list = mongoTemplate.find(query, Member.class).stream().map(Member::getId).collect(toList());
                final Query queryMemberShip = new Query().with(arg.getPageable());
                if (list.size() > 0) {
                    queryMemberShip.addCriteria(where("memberId").in(list));
                }
                entityList = mongoTemplate.find(queryMemberShip, MemberPayment.class).stream().map(MemberPayment::getId).collect(toList());
            }
            else if (c.getName().contains("JobApplication")) {
                if (hasText(arg.getName())) {
                    query.addCriteria(where("personal.firstName").regex(".*"+arg.getName()+".*"));
                }

                if (hasText(arg.getSurname())) {
                    query.addCriteria(where("personal.lastName").regex(".*"+arg.getSurname()+".*"));
                }

                entityList = mongoTemplate.find(query, JobApplication.class).stream().map(JobApplication::getId).collect(toList());
                if((hasText(arg.getName()) || hasText(arg.getSurname()) )&&entityList.size() ==0 ) {
                    entityList.add("-1");
                }
            }
            else {
                if (hasText(arg.getName())) {
                    query.addCriteria(where("userProfileForm.givenName").regex(".*"+arg.getName()+".*"));
                }
                if (hasText(arg.getSurname())) {
                    query.addCriteria(where("userProfileForm.surName").regex(".*"+arg.getSurname()+".*"));
                }
                entityList = mongoTemplate.find(query, Application.class).stream().map(Application::getId).collect(toList());
            }
        }

        return  query(arg, entityList);
    }

    public Page<CaseInfo> queryByEntityBookingByClass(CaseQuery arg,Class c) {
        List<String> list = new ArrayList<>();
        List<String> entityList = new ArrayList<>();
        final Query query = new Query();

        if (c.getName().contains("Booking")) {
            if(arg.getFlowDefKeys()!= null && Arrays.stream(arg.getFlowDefKeys()).findFirst().get().equals("ALL"))
            {
                list = mongoTemplate.find(query, BookingServiceMember.class).stream().map(BookingServiceMember::getBookingId).collect(toList());
                entityList.addAll(list);

            }

            if (hasText(arg.getVendorId())){

                query.addCriteria(where("vendorId").is(arg.getVendorId()));

                list = mongoTemplate.find(query, BookingServiceMember.class).stream().map(BookingServiceMember::getBookingId).collect(toList());
                entityList.addAll(list);

                if(entityList.size() ==0 ) {
                    entityList.add("-1");
                }
            }

            if (hasText(arg.getSearch())){

                List<Criteria> criteria = new ArrayList<>();

                criteria.add(where("shopName").regex(".*"+arg.getSearch()+".*"));
                criteria.add(where("productName").regex(".*"+arg.getSearch()+".*"));
                criteria.add(where("modelName").regex(".*"+arg.getSearch()+".*"));
                criteria.add(where("memberName").regex(".*"+arg.getSearch()+".*"));
                query.addCriteria(new Criteria().orOperator(criteria.toArray(new Criteria[criteria.size()])));

                list = mongoTemplate.find(query, BookingServiceMember.class).stream().map(BookingServiceMember::getBookingId).collect(toList());
                entityList.addAll(list);

                if(entityList.size() ==0 ) {
                    entityList.add("-1");
                }
            }

            if (arg.getIsArrival() != null) {
                query.addCriteria(where("bookingDetail.isArrival").is(arg.getIsArrival()));
                list = mongoTemplate.find(query, BookingServiceMember.class).stream().map(BookingServiceMember::getBookingId).collect(toList());
                entityList = list;
                if(entityList.size() ==0 ) {
                    entityList.add("-1");
                }
            }

            if(arg.getBookingStatus()!=null)
            {
                final Query queryBooking = new Query().with(arg.getPageable());
                if(list.size()> 0)
                {
                    queryBooking.addCriteria(where("id").in(list));
                }
                queryBooking.addCriteria(where("bookingStatus").in(arg.getBookingStatus()));
                entityList = mongoTemplate.find(queryBooking, Booking.class).stream().map(Booking::getId).collect(toList());
                if(entityList.size() ==0 ) {
                    entityList.add("-1");
                }
            }


        }
        return  queryBookings(arg, entityList);
    }


    public Page<CaseInfo> queryBookings(CaseQuery arg,List<String> entityId ){
        final Query queryInfo = new Query().with(arg.getPageable());

        if (entityId.size() > 0) {
            queryInfo.addCriteria(where("entityId").in(entityId));
        }

        if (arg.getFlowDefKeys()!=null && arg.getFlowDefKeys().length >0
            &&  !Arrays.stream(arg.getFlowDefKeys()).findFirst().get().equals("ALL")) {
            queryInfo.addCriteria(where("flowDefKey").in(arg.getFlowDefKeys()));
        }

        if (arg.getTaskDefKeys() != null && !arg.getTaskDefKeys().isEmpty()) {
            queryInfo.addCriteria(where("taskDefKey").in(arg.getTaskDefKeys()));
        }

        if (arg.getProcessInstanceIds() != null && !arg.getProcessInstanceIds().isEmpty()) {
            queryInfo.addCriteria(where("processInstanceId").in(arg.getProcessInstanceIds()));
        }

        if (hasText(arg.getStatus())) {
            queryInfo.addCriteria(where("status").is(arg.getStatus()));
        }

        if (nonNull(arg.getFinished())) {
            queryInfo.addCriteria(where("finished").is(arg.getFinished()));
        }

        if (hasText(arg.getFrom()) && hasText(arg.getTo()) ) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom()).toDate()).lt(Instant.parse(arg.getTo()).toDate()));
        }
        else if (hasText(arg.getFrom())) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom()).toDate()));
        }
        else if (hasText(arg.getTo())) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo()).toDate()));
        }

        if (hasText(arg.getStatus())) {
            queryInfo.addCriteria(where("status").is(arg.getStatus()));
        }

        if (hasText(arg.getPackageAction())) {
            queryInfo.addCriteria(where("packageAction").is(arg.getPackageAction()));
        }


        List<com.thailandelite.mis.be.domain.CaseInfo> listResult = mongoTemplate.find(queryInfo, com.thailandelite.mis.be.domain.CaseInfo.class);

        return PageableExecutionUtils.getPage(listResult, arg.getPageable(), () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), com.thailandelite.mis.be.domain.CaseInfo.class));
    }



    public Page<CaseInfo> query(CaseQuery arg,List<String> entityId ){
        final Query queryInfo = new Query().with(arg.getPageable());

        if (entityId.size() > 0) {
            queryInfo.addCriteria(where("entityId").in(entityId));
        }

        if (hasText(arg.getFlowDefKey())) {
            queryInfo.addCriteria(where("flowDefKey").is(arg.getFlowDefKey()));
        }

        if (arg.getTaskDefKeys() != null && !arg.getTaskDefKeys().isEmpty()) {
            queryInfo.addCriteria(where("taskDefKey").in(arg.getTaskDefKeys()));
        }

        if (arg.getProcessInstanceIds() != null && !arg.getProcessInstanceIds().isEmpty()) {
            queryInfo.addCriteria(where("processInstanceId").in(arg.getProcessInstanceIds()));
        }


        if (nonNull(arg.getFinished())) {
            queryInfo.addCriteria(where("finished").is(arg.getFinished()));
        }

        if (hasText(arg.getFrom()) && hasText(arg.getTo()) ) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom()).toDate()).lt(Instant.parse(arg.getTo()).toDate()));
        }
        else if (hasText(arg.getFrom())) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom()).toDate()));
        }
        else if (hasText(arg.getTo())) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo()).toDate()));
        }

        if (hasText(arg.getStatus())) {
            queryInfo.addCriteria(where("status").is(arg.getStatus()));
        }

        if (hasText(arg.getPackageAction())) {
            queryInfo.addCriteria(where("packageAction").is(arg.getPackageAction()));
        }


        List<com.thailandelite.mis.be.domain.CaseInfo> listResult = mongoTemplate.find(queryInfo, com.thailandelite.mis.be.domain.CaseInfo.class);

        return PageableExecutionUtils.getPage(listResult, arg.getPageable(), () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), com.thailandelite.mis.be.domain.CaseInfo.class));
    }


    public Page<CaseInfo> query(CaseQuery arg) {

        final Query query = new Query().with(arg.getPageable());

        if (hasText(arg.getFlowDefKey())) {
            query.addCriteria(where("flowDefKey").is(arg.getFlowDefKey()));
        }

        if (arg.getTaskDefKeys() != null && !arg.getTaskDefKeys().isEmpty()) {
            query.addCriteria(where("taskDefKey").in(arg.getTaskDefKeys()));
        }

        if (arg.getProcessInstanceIds() != null && !arg.getProcessInstanceIds().isEmpty()) {
            query.addCriteria(where("processInstanceId").in(arg.getProcessInstanceIds()));
        }

        if (hasText(arg.getStatus())) {
            query.addCriteria(where("status").is(arg.getStatus()));
        }

        if (nonNull(arg.getFinished())) {
            query.addCriteria(where("finished").is(arg.getFinished()));
        }


        if (hasText(arg.getFrom())) {
            query.addCriteria(where("last_modified_date").gte(arg.getFrom()));
        }

        if (hasText(arg.getTo())) {
            query.addCriteria(where("last_modified_date").lte(arg.getTo()));
        }


        if (hasText(arg.getStatus())) {
            query.addCriteria(where("status").is(arg.getStatus()));
        }

        List<CaseInfo> list = mongoTemplate.find(query, CaseInfo.class);
        return PageableExecutionUtils.getPage(list, arg.getPageable(), () -> mongoTemplate.count(Query.of(query).limit(-1).skip(-1), CaseInfo.class));
    }

    private String containing(String what){
        return MongoRegexCreator.INSTANCE.toRegularExpression(what, MongoRegexCreator.MatchMode.CONTAINING);
    }

    public CaseInfo findByEntityId(String bookingId) {
        List<CaseInfo> caseInfos = caseInfoRepository.findByEntityId(bookingId);
        if(caseInfos != null && caseInfos.size() >0 ){
            return caseInfos.get(0);
        }
        throw new NotFoundException("Not found your booking.");
    }
}
