package com.thailandelite.mis.be.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
public class CaseMemberBeginDTO {
    @JsonInclude(Include.NON_NULL)
    private Prospect prospect;
}
