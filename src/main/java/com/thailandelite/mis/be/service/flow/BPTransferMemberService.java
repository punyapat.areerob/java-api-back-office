package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.*;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.enumeration.InvoiceStatus;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class BPTransferMemberService extends  BPService<Member> {
    private final MemberRepository memberRepository;
    private final ApplicationService applicationService;
    private final ApplicationRepository applicationRepository;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceService invoiceService;
    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPTransferMemberService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , MemberRepository memberRepository
        , ApplicationRepository applicationRepository
        , ApplicationService applicationService
        , InvoiceRepository invoiceRepository
        , ApplicationContext applicationContext, InvoiceService invoiceService) {

        super(camundaService,
            new BPTransferMemberService.Pending_CRM_Check_Transfer_DOC(caseActivityRepository, memberRepository),
            new BPTransferMemberService.Pending_User_Resubmit(caseActivityRepository, memberRepository),
            new BPTransferMemberService.Pending_GR_create_E_Doc(caseActivityRepository, memberRepository),
            new BPTransferMemberService.GR_Process_to_IM_National_PR(caseActivityRepository, memberRepository),
            new BPTransferMemberService.GR_Pending_IM_National_PR_Response(caseActivityRepository, memberRepository),
            new BPTransferMemberService.CRM_Approve_Close_Case(caseActivityRepository, memberRepository ,applicationRepository, invoiceRepository, invoiceService) ,
            new BPTransferMemberService.CRM_Reject_Close_Case(caseActivityRepository, memberRepository)
        );
        this.memberRepository = memberRepository;
        this.applicationRepository = applicationRepository;
        this.applicationService = applicationService;
        this.invoiceRepository = invoiceRepository;
        this.invoiceService = invoiceService;
    }

    @Override
    public Object create(String flowDefKey, Object object) {
        Application application = (Application) object;
        ProcessInstance process = super.startProcess(flowDefKey, application.getId());
        this.caseInfoService.createMemberTransferCase(flowDefKey, BPTransferMemberService.Pending_CRM_Check_Transfer_DOC.taskDefKey, process, application);
        return  application;
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        Application application = applicationRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        //// TODO: 10/4/2021 AD call CardService
        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .application(application)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<Application> applications = applicationRepository.findByIdIn(entityIds);

        ImmutableMap<String, Application> idMap = Maps.uniqueIndex(applications, Application::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            Application application = idMap.get(caseInfo.getEntityId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .application(application)
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @RequiredArgsConstructor
    private static class Pending_CRM_Check_Transfer_DOC implements ITask {
        public static final String taskDefKey = "Pending_CRM_Check_Transfer_DOC";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,REJECT,SKIPTO}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTransferMemberService.Pending_CRM_Check_Transfer_DOC.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.Pending_CRM_Check_Transfer_DOC.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E_Doc");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING USER RESUBMIT");
                    caseInfo.setTaskDefKey("Pending_User_Resubmit");
                    break;
                case SKIPTO:
                    caseInfo.setStatus("PENDING CRM APPROVE");
                    caseInfo.setTaskDefKey("CRM_Approve_Close_Case");
                    break;
            }


        }
    }

    @RequiredArgsConstructor
    private static class Pending_User_Resubmit implements ITask {
        public static final String taskDefKey = "Pending_User_Resubmit";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {RESUBMIT,CANCEL}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTransferMemberService.Pending_User_Resubmit.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.Pending_User_Resubmit.Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("PENDING CRM CHECK DOC");
                    caseInfo.setTaskDefKey("Pending_CRM_Check_Transfer_DOC");
                    break;
                case CANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }


        }
    }

    @RequiredArgsConstructor
    private static class Pending_GR_create_E_Doc implements ITask {
        public static final String taskDefKey = "Pending_GR_create_E_Doc";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,REJECT,SKIPTO}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTransferMemberService.Pending_GR_create_E_Doc.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.Pending_GR_create_E_Doc.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR TO IM");
                    caseInfo.setTaskDefKey("GR_Process_to_IM_National_PR");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING USER RESUBMIT");
                    caseInfo.setTaskDefKey("Pending_User_Resubmit");
                    break;
                case SKIPTO:
                    caseInfo.setStatus("PENDING CRM APPROVE");
                    caseInfo.setTaskDefKey("CRM_Approve_Close_Case");
                    break;
            }

        }
    }

    @RequiredArgsConstructor
    private static class GR_Process_to_IM_National_PR implements ITask {
        public static final String taskDefKey = "GR_Process_to_IM_National_PR";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,MOVE}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTransferMemberService.GR_Process_to_IM_National_PR.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.GR_Process_to_IM_National_PR.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR RESPONSE");
                    caseInfo.setTaskDefKey("GR_Pending_IM_National_PR_Response");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E_Doc");
                    break;
            }


        }
    }

    @RequiredArgsConstructor
    private static class GR_Pending_IM_National_PR_Response implements ITask {
        public static final String taskDefKey = "GR_Pending_IM_National_PR_Response";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,MOVE, REJECT}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTransferMemberService.GR_Pending_IM_National_PR_Response.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.GR_Pending_IM_National_PR_Response.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING CRM APPROVE");
                    caseInfo.setTaskDefKey("CRM_Approve_Close_Case");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR TO IM");
                    caseInfo.setTaskDefKey("GR_Process_to_IM_National_PR");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING CRM REJECT");
                    caseInfo.setTaskDefKey("CRM_Reject_Close_Case");
                    break;
            }


        }
    }


    @RequiredArgsConstructor
    private static class CRM_Approve_Close_Case implements ITask {
        public static final String taskDefKey = "CRM_Approve_Close_Case";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;
        final ApplicationRepository applicationRepository;
        final InvoiceRepository invoiceRepository;
        final InvoiceService invoiceService;

        public enum Action {DONE,MOVE }

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTransferMemberService.CRM_Approve_Close_Case.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.CRM_Approve_Close_Case.Action.valueOf(actionName)) {
                case DONE:
                    Invoice invoice = invoiceRepository.insert(createInvoiceFromApplicant(applicationRepository.findById(caseInfo.getEntityId()).get()));
                    invoiceService.sendInvoiceEmail(invoice.getId());
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E_Doc");
                    break;
            }

        }

        private Invoice createInvoiceFromApplicant(Application application)
        {
            Member member = memberRepository.findById(application.getMemberId()).get();
            Invoice invoice = new Invoice();

            invoice.setDocumentId(application.getId());
            invoice.setMemberId(application.getMemberId());
            MemberInfo memberInfo = new MemberInfo();
            memberInfo.setGivenName(member.getGivenName());
            memberInfo.setSurName(member.getSurName());
            invoice.setMember(member);
            invoice.setPayType(PaymentType.MEMBERSHIP);
            invoice.setPackageAction(PackageAction.TRANSFER);
            Card.MembershipFeeSetting feeSetting = application.getCard().getMembershipFee();
            invoice.setTotalAmount(feeSetting.getAmount());
            invoice.setTotalVat(feeSetting.getVat());
            Float includePrice = Card.VatType.INCLUDE.equals(feeSetting.getVatType()) ? feeSetting.getAmount() : feeSetting.getAmount() + feeSetting.getVat();
            invoice.setTotalAmountIncVat(includePrice);
            invoice.setStatus(InvoiceStatus.INITIAL);
            invoice.setPaidAmount(0f);
            Invoice.OrderDetail orderDetail = new Invoice.OrderDetail();
            orderDetail.setName(application.getCard().getName());
            orderDetail.setAmount(invoice.getTotalAmount());
            orderDetail.setAmountVat(invoice.getTotalVat());
            orderDetail.setAmountIncVat(invoice.getTotalAmountIncVat());
            orderDetail.setQuantity(1);
            ArrayList<Invoice.OrderDetail> orderDetailArrayList  = new ArrayList<>();
            orderDetailArrayList.add(orderDetail);
            invoice.setOrderDetails(orderDetailArrayList);

            return  invoice;
        }
    }


    @RequiredArgsConstructor
    private static class CRM_Reject_Close_Case implements ITask {
        public static final String taskDefKey = "CRM_Reject_Close_Case";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {REJECT }

        public String name() {
            return taskDefKey;
        }
        public Enum<?>[] actions() {
            return BPTransferMemberService.CRM_Reject_Close_Case.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTransferMemberService.CRM_Reject_Close_Case.Action.valueOf(actionName)) {
                case REJECT:
                    caseInfo.setStatus("REJECT");
                    caseInfo.setTaskDefKey("");
                    break;
            }

        }
    }
}
