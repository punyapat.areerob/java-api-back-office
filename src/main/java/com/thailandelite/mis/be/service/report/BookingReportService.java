package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.utils.Generater;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.booking.dao.BookingResponse;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.joda.time.Instant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.LongSupplier;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@RequiredArgsConstructor
public class BookingReportService {

    private final MemberRepository memberRepository;
    private final ProductRepository productRepository;
    private final MongoTemplate mongoTemplate;
    private final ExcelService excelService;

    public Page<BookingReportResponse> getBookingReport(BookingReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<BookingReportResponse> listResult = getTimePeriodReport(searchRequest, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] generateExcelBookingReport(BookingReportRequest searchRequest) {
        final Query queryInfo = new Query();
        List<BookingReportResponse> listResult = getTimePeriodReport(searchRequest, queryInfo);
        List<String> headers = Arrays.asList("Used date", "Service", "Branch", "Detail", "Type", "Amount", "Member", "Gender", "Nationality","Country");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        for (int i=0; i<listResult.size(); i++) {
            BookingReportResponse cellValue = listResult.get(i);
            BookingServiceMember booking = cellValue.getBookingServiceMember();
            Member member = cellValue.getMember();
            Product product = cellValue.getProduct();
            String name = member.getTitle().getName()+ " " + member.getGivenName() + " "+ member.getSurName();
            String nationality = member.getNationality() != null ? member.getNationality().getName() : "-";
            String country = member.getCountry() != null ? member.getCountry().getName() : "-";
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(cellValue.getDateTimePeriod()));
            cells.add(new ExcelService.ExcelValue(booking.getProductName()));
            cells.add(new ExcelService.ExcelValue(booking.getShopName()));
            cells.add(new ExcelService.ExcelValue(booking.getModelName()));
            cells.add(new ExcelService.ExcelValue(product.getFlowDefinitionKey().name()));
            cells.add(new ExcelService.ExcelValue(cellValue.getCount()));
            cells.add(new ExcelService.ExcelValue(name));
            cells.add(new ExcelService.ExcelValue(member.getGender()));
            cells.add(new ExcelService.ExcelValue(nationality));
            cells.add(new ExcelService.ExcelValue(country));
            rows.add(cells);
        }
        return excelService.generateExcelReport("Application", headers, rows);
    }

    private List<BookingReportResponse> getTimePeriodReport(BookingReportRequest searchRequest, Query queryInfo) {
        List<BookingReportResponse> listResult = new ArrayList<>();
        List<BookingServiceMember> applicationQuery = getReportList(searchRequest, queryInfo);
        HashMap<String, List<BookingReportResponse>> groupTimePeriod = new HashMap<>();
        String groupByFormat = Generater.getGroupByFormat(searchRequest.getTimePeriod());
        HashMap<String, Member> memberCache = new HashMap<>();
        HashMap<String, Product> productCache = new HashMap<>();
        for (BookingServiceMember row : applicationQuery) {
            Member member = getMemberHash(memberCache, row);
            if (member == null) continue;

            Product product = getProduct(productCache, row);
            if (product == null) continue;

            ZonedDateTime zonedDateTime = row.getCreatedDate().atZone(ZoneId.systemDefault());
            String timePeriodKey = DateUtils.toDateString(zonedDateTime, groupByFormat);
            List<BookingReportResponse> item = groupTimePeriod.get(timePeriodKey);
            if (item == null)
                item = new ArrayList<>();
            BookingReportResponse bookingRes = new BookingReportResponse();
            bookingRes.setDateTimePeriod(timePeriodKey);
            bookingRes.setBookingServiceMember(row);
            bookingRes.setMemberId(row.getMemberId());
            bookingRes.setMember(member);
            bookingRes.setProduct(product);
            bookingRes.setCount(1);
            for (BookingReportResponse bookingReportResponse : item) {
                if(bookingReportResponse.getMemberId().equalsIgnoreCase(row.getBookingId())) {
                    bookingRes = bookingReportResponse;
                    bookingRes.setCount(bookingReportResponse.getCount()+1);
                }
            }
            item.add(bookingRes);
            groupTimePeriod.put(timePeriodKey, item);
        }

        for (String timePeriodKey : groupTimePeriod.keySet()) {
            List<BookingReportResponse> bookings = groupTimePeriod.get(timePeriodKey);
            listResult.addAll(bookings);
        }

        return listResult;
    }

    private Member getMemberHash(HashMap<String, Member> memberTmp, BookingServiceMember row) {
        if (row.getMemberId() == null)
            return null;
        Member member = memberTmp.get(row.getMemberId());
        if (member == null){
            Optional<Member> optional = memberRepository.findById(row.getMemberId());
            if( !optional.isPresent() )
                return null;
            member = optional.get();
        }
        memberTmp.put(row.getMemberId(), member);
        return member;
    }

    private Product getProduct(HashMap<String, Product> cache, BookingServiceMember row) {
        String productId = row.getProductId();
        if (productId == null)
            return null;
        Product product = cache.get(productId);
        if (product == null){
            Optional<Product> optional = productRepository.findById(productId);
            if( !optional.isPresent() )
                return null;
            product = optional.get();
        }
        cache.put(productId, product);
        return product;
    }
    private List<BookingServiceMember> getReportList(BookingReportRequest arg, Query queryInfo) {
        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("startTime").gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("startTime").gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("startTime").lt(arg.getTo()));
        }
        if ( !StringUtils.isEmpty(arg.getVendorName()) )
            queryInfo.addCriteria(where("vendorName").regex(arg.getVendorName(), "iu"));

        if ( !StringUtils.isEmpty(arg.getServiceType()) )
            queryInfo.addCriteria(where("flowDefinitionKey").regex(arg.getServiceType().name(), "iu"));

        List<BookingServiceMember> rows = mongoTemplate.find(queryInfo, BookingServiceMember.class);
        return rows;
    }
}
