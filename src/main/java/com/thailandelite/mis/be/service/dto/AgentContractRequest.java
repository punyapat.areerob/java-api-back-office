package com.thailandelite.mis.be.service.dto;

import lombok.Data;

import java.time.ZonedDateTime;

/**
 * A DTO representing a user, with his authorities.
 */
@Data
public class AgentContractRequest {
    ZonedDateTime startContractDate;
    ZonedDateTime endContractDate;
    String remark;
}
