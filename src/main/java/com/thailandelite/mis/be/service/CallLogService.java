package com.thailandelite.mis.be.service;
import com.google.common.collect.Lists;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.thailandelite.mis.be.cdr.domain.Cdr;
import com.thailandelite.mis.be.cdr.domain.QueueLog;
import com.thailandelite.mis.be.cdr.repository.CdrRepository;
import com.thailandelite.mis.be.cdr.repository.QueueLogRepository;
import com.thailandelite.mis.be.domain.CallLog.CallLogType;
import com.thailandelite.mis.be.domain.CallLog.CallLogAction;

import com.thailandelite.mis.be.domain.CallLog;
import com.thailandelite.mis.be.domain.Staff;
import com.thailandelite.mis.be.repository.AgentLogRepository;
import com.thailandelite.mis.be.repository.MemberPhoneRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.repository.StaffRepository;
import com.thailandelite.mis.be.service.dto.ReceiveCallStatus;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.FlowResponse;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPhone;
import com.thailandelite.mis.model.domain.agent.AgentLog;
import com.thailandelite.mis.model.domain.agent.AgentLog.CallCenterStatus;
import com.thailandelite.mis.model.domain.master.Title;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

import static com.thailandelite.mis.be.service.migrate.MigrateUtils.zdt;

@Slf4j
@Service
@RequiredArgsConstructor
public class CallLogService {
    private final CdrRepository cdrRepository;
    private final MemberPhoneRepository memberPhoneRepository;
    private final MemberRepository memberRepository;
    private final QueueLogRepository queueLogRepository;

    public Page<CallLog> IncomingCallLogs (Pageable pageable) {
        List<CallLog> callLogs = new ArrayList<>();
        LocalDate date = LocalDate.now().minusDays(22);
        List<Cdr> cdrs = cdrRepository.findAllByCalldateAfter(Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()))
            .stream().filter(cdr -> cdr.getSrc().trim().length() != 4 && cdr.getDst().trim().length() == 4).collect(Collectors.toList());
        for (Cdr cdr : cdrs) {
            CallLog src = new CallLog();
            src.setId(cdr.getCdrId().toString());
            src.setStartTime(zdt(cdr.getCalldate()));
            src.setDuration(cdr.getDuration());
            src.setPhone(cdr.getSrc().trim());
            List<MemberPhone> memberPhones = memberPhoneRepository.findByPhoneNo(cdr.getSrc().trim());
            List<CallLog.MemberName> memberNames = new ArrayList<>();
            for (MemberPhone memberPhone : memberPhones) {
                Member member = memberRepository.findById(memberPhone.getMemberId()).orElse(null);
                if (member != null) {
                    CallLog.MemberName memberName = new CallLog.MemberName();
                    memberName.setMemberId(member.getId());
                    memberName.setFirstName(member.getGivenName());
                    memberName.setLastName(member.getSurName());
                    memberNames.add(memberName);
                }
            }
            src.setMemberNames(memberNames);
            src.setAction(cdr.getDisposition().startsWith("ANSWERED") ? CallLogAction.RECEIVED : CallLogAction.MISSED_CALL);
//            src.setAgentName("");
            callLogs.add(src);
        }
//        Page<CallLog> result = new PageImpl<>(callLogs, pageable, callLogs.size());
//        return result;
        return PageableExecutionUtils.getPage(callLogs, pageable, (LongSupplier) callLogs::size);
    }

    public Page<CallLog> OutgoingCallLogs (Pageable pageable) {
        List<CallLog> callLogs = new ArrayList<>();
        LocalDate date = LocalDate.now().minusDays(22);
        List<Cdr> cdrs = cdrRepository.findAllByCalldateAfter(Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant()))
            .stream().filter(cdr -> cdr.getSrc().trim().length() == 4 && cdr.getDst().trim().length() != 4).collect(Collectors.toList());
        convertFromDstToCallLogs(callLogs, cdrs);
        return PageableExecutionUtils.getPage(callLogs, pageable, (LongSupplier) callLogs::size);
    }

    private void convertFromDstToCallLogs(List<CallLog> callLogs, List<Cdr> cdrs) {
        for (Cdr cdr : cdrs) {
            CallLog src = new CallLog();
            src.setId(cdr.getCdrId().toString());
            src.setStartTime(zdt(cdr.getCalldate()));
            src.setDuration(cdr.getDuration());
            src.setPhone(cdr.getDst().trim());
            List<MemberPhone> memberPhones = memberPhoneRepository.findByPhoneNo(cdr.getDst().trim());
            List<CallLog.MemberName> memberNames = new ArrayList<>();
            for (MemberPhone memberPhone : memberPhones) {
                Member member = memberRepository.findById(memberPhone.getMemberId()).orElse(null);
                if (member != null) {
                    CallLog.MemberName memberName = new CallLog.MemberName();
                    memberName.setMemberId(member.getId());
                    memberName.setFirstName(member.getGivenName());
                    memberName.setLastName(member.getSurName());
                    memberNames.add(memberName);
                }
            }
            src.setMemberNames(memberNames);
            src.setAction(cdr.getDisposition().startsWith("ANSWERED") ? CallLogAction.RECEIVED : CallLogAction.NO_ANSWER);
            callLogs.add(src);
        }
    }

    public CallingLog ReceiveCall (String deskNo) {
        CallingLog receiveCall = new CallingLog();
        List<CallingMemberName> res = new ArrayList<>();

        Cdr cdr = cdrRepository.findTopByDstAndDisposition(deskNo, "ANSWERED", Sort.by(Sort.Direction.DESC, "calldate")).orElseThrow(()->new NotFoundException("Not found cdr"));
        List<MemberPhone> memberPhones = memberPhoneRepository.findByPhoneNo(cdr.getSrc().trim());
        if(memberPhones != null) {
            for (MemberPhone memberPhone : memberPhones) {
                CallingMemberName name = new CallingMemberName();
                Member member = memberRepository.findById(memberPhone.getMemberId()).orElse(null);
                if(member != null) {
                    name.setMemberId(member.getId());
                    name.setTitle(member.getTitle().getName());
                    name.setFirstName(member.getGivenName());
                    name.setLastName(member.getSurName());
                    res.add(name);
                }
            }
        }
        receiveCall.setPhone(cdr.getSrc().trim());
        receiveCall.setStatus(ReceiveCallStatus.RECEIVED);
        receiveCall.setCallingMemberNames(res);
        return receiveCall;
    }

    public List<CallingLog> AbandonCall (String deskNo) {
        List<CallingLog> abandonCalls = new ArrayList<>();

        Cdr cdr = cdrRepository.findTopByDstAndDisposition(deskNo, "ANSWERED", Sort.by(Sort.Direction.DESC, "calldate")).orElseThrow(()->new NotFoundException("Not found cdr"));
        List<Cdr> abandons = cdrRepository.findAllByDstAndCalldateAfter(deskNo, cdr.getCalldate()).stream().map(a -> {
            QueueLog queueLog = queueLogRepository.findAllByCallidAndEvent(a.getUniqueid(), "ABANDON").orElse(null);
            a = cdrRepository.findOneByUniqueid(queueLog.getCallid()).orElse(null);
            return a;
        }).collect(Collectors.toList());
        for (Cdr abandon : abandons) {
            CallingLog abandonCall = new CallingLog();
            List<CallingMemberName> memberNames = new ArrayList<>();
            List<MemberPhone> memberPhones = memberPhoneRepository.findByPhoneNo(abandon.getSrc().trim());
            if(memberPhones != null) {
                for (MemberPhone memberPhone : memberPhones) {
                    CallingMemberName name = new CallingMemberName();
                    Member member = memberRepository.findById(memberPhone.getMemberId()).orElse(null);
                    if(member != null) {
                        name.setMemberId(member.getId());
                        name.setTitle(member.getTitle().getName());
                        name.setFirstName(member.getGivenName());
                        name.setLastName(member.getSurName());
                        memberNames.add(name);
                    }
                }
            }
            abandonCall.setPhone(cdr.getSrc().trim());
            abandonCall.setStatus(ReceiveCallStatus.MISS_CALL);
            abandonCall.setCallingMemberNames(memberNames);
            abandonCalls.add(abandonCall);
        }
        return abandonCalls;
    }

    public Page<CallLog> findCallLogByDeskNo(String deskNo, Pageable pageable) {
        List<CallLog> callLogs = new ArrayList<>();
        Page<Cdr> cdrs = cdrRepository.findByDst(deskNo, pageable);
        List<Cdr> cdrList = cdrs.getContent();
        convertFromDstToCallLogs(callLogs, cdrList);
        LongSupplier sup = () -> (int)(cdrs.getTotalElements());
        return PageableExecutionUtils.getPage(callLogs, pageable, sup);
    }


    @Data
    public static class CallingLog {
        private String phone;
        private List<CallingMemberName> callingMemberNames;
        private ReceiveCallStatus status;
    }

    @Data
    public static class CallingMemberName {
        private String memberId;
        private String title;
        private String firstName;
        private String lastName;
    }
}
