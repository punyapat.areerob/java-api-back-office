package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.model.domain.enumeration.CaseActivityEvent;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CaseActivityService {
    private final Logger log = LoggerFactory.getLogger(CaseActivityService.class);

    @Autowired
    private CaseActivityRepository caseActivityRepository;

    public List<CaseActivity> getCaseActivities(String caseInstanceId, Optional<CaseActivityEvent> event) {
        ExampleMatcher customMatcher = ExampleMatcher.matchingAll().withIgnoreNullValues()
            .withMatcher("caseInstanceId", ExampleMatcher.GenericPropertyMatchers.exact().ignoreCase())
            .withMatcher("event", ExampleMatcher.GenericPropertyMatchers.exact());

        CaseActivity caseActivityEx = new CaseActivity();
        caseActivityEx.setCaseInstanceId(caseInstanceId);

        if (event.isPresent()) {
            caseActivityEx.setEvent(event.get());
        }

        Example<CaseActivity> example = Example.of(caseActivityEx, customMatcher);

        return caseActivityRepository.findAll(example);
    }
}
