package com.thailandelite.mis.be.service;
import camundajar.impl.com.google.gson.Gson;
import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.Airline;
import com.amadeus.resources.DatedFlight;
import com.amadeus.resources.FlightOfferSearch;
import com.thailandelite.mis.be.repository.AirportRepository;
import com.thailandelite.mis.model.domain.Airport;
import com.thailandelite.mis.model.domain.airport.AirLineRepons;
import com.thailandelite.mis.model.domain.airport.DatedFilghtReponse;
import com.thailandelite.mis.model.domain.airport.FlightOfferSearchReponse;
import com.thailandelite.mis.model.domain.airport.FlightOfferSearchRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class FlightService {
    private final AirportRepository airportRepository;
        Amadeus amadeus = Amadeus
            .builder("tg6XUHkAlj3SS33pPlAoqvCKJZsGHJkz","FdpWkPjrFzq8H4Ct")
            .setHostname("production")
            .build();
    public List<FlightOfferSearchReponse>FlightOfferSearch(String From, String To, String dateFrom, String dateTo) throws ResponseException {
        FlightOfferSearch[] flightOffersSearches = amadeus.shopping.flightOffersSearch.get(
            Params.with("originLocationCode", From)
                .and("destinationLocationCode",To)
                .and("departureDate",dateFrom)
                .and("returnDate",dateTo)
                .and("adults", 2)
                .and("max",250));
        List<FlightOfferSearchReponse> flights =   Arrays.stream(flightOffersSearches).map(c-> {
            FlightOfferSearchReponse flight = new FlightOfferSearchReponse();
            flight.setId(c.getId());
            flight.setType(c.getType());
            flight.setSource(c.getSource());
            flight.setInstantTicketingRequired(c.isInstantTicketingRequired());
            flight.setNonHomogeneous(c.isNonHomogeneous());
            flight.setOneWay(c.isOneWay());
            flight.setLastTicketingDate(c.getLastTicketingDate());
            flight.setNumberOfBookableSeats(c.getNumberOfBookableSeats());
            flight.setItineraries(c.getItineraries());
            flight.setPrice(c.getPrice());
            flight.setPricingOptions(c.getPricingOptions());
            flight.setValidatingAirlineCodes(c.getValidatingAirlineCodes());
            flight.setTravelerPricings(c.getTravelerPricings());
            flight.setChoiceProbability(c.getChoiceProbability());
            flight.setCreatedBy(c.getChoiceProbability());
            return  flight ;
        }).collect(Collectors.toList());
        return flights;
    }

    public List<AirLineRepons>GetICAO(String airlineCodes) throws ResponseException {
        Airline[] airlines = amadeus.referenceData.airlines.get(Params
            .with("airlineCodes",airlineCodes));
        List<AirLineRepons> airLineRepons = Arrays.stream(airlines).map(c-> {
            AirLineRepons airLineRepons1 = new AirLineRepons();
            airLineRepons1.setType(c.getType());
            airLineRepons1.setIataCode(c.getIataCode());
            airLineRepons1.setIcaoCode(c.getIcaoCode());
            airLineRepons1.setBusinessName(c.getBusinessName());
            airLineRepons1.setCommonName(c.getCommonName());
            return airLineRepons1;
        }).collect(Collectors.toList());
        return airLineRepons;
    }
    public List<FlightOfferSearchReponse>FlightOfferSearch(FlightOfferSearchRequest body) throws ResponseException {
        String jsongg = new Gson().toJson(body);
        FlightOfferSearch[] flightOffersSearches = amadeus.shopping.flightOffersSearch.post(new Gson().toJson(body));
        List<FlightOfferSearchReponse> flights =   Arrays.stream(flightOffersSearches).map(c-> {
            FlightOfferSearchReponse flight = new FlightOfferSearchReponse();
            flight.setId(c.getId());
            flight.setType(c.getType());
            flight.setSource(c.getSource());
            flight.setInstantTicketingRequired(c.isInstantTicketingRequired());
            flight.setNonHomogeneous(c.isNonHomogeneous());
            flight.setOneWay(c.isOneWay());
            flight.setLastTicketingDate(c.getLastTicketingDate());
            flight.setNumberOfBookableSeats(c.getNumberOfBookableSeats());
            flight.setItineraries(c.getItineraries());
            flight.setPrice(c.getPrice());
            flight.setPricingOptions(c.getPricingOptions());
            flight.setValidatingAirlineCodes(c.getValidatingAirlineCodes());
            flight.setTravelerPricings(c.getTravelerPricings());
            flight.setChoiceProbability(c.getChoiceProbability());
            flight.setCreatedBy(c.getChoiceProbability());
            return  flight ;
        }).collect(Collectors.toList());
        return flights;
        }
    public List<DatedFilghtReponse>GetFlight(String Fromdate, String Flight) throws ResponseException {
        String[] part = Flight.split("(?<=\\D)(?=\\d)");
        DatedFlight[] flightStatus = amadeus.schedule.flights.get(Params
            .with("flightNumber",part[1])
            .and("carrierCode",part[0])
            .and("scheduledDepartureDate",Fromdate));
        List<DatedFilghtReponse> flights =   Arrays.stream(flightStatus).map(c-> {
            DatedFilghtReponse flight = new DatedFilghtReponse();
            flight.setType(c.getType());
            flight.setScheduledDepartureDate(c.getScheduledDepartureDate());
            flight.setFlightDesignator(c.getFlightDesignator());
            List<DatedFilghtReponse.FlightPoint> flightPoints = Arrays.stream(c.getFlightPoints()).map(f->{
                DatedFilghtReponse.FlightPoint flightPoint = new DatedFilghtReponse.FlightPoint();
                flightPoint.setIataCode(f.getIataCode());
                flightPoint.setDeparture(f.getDeparture());
                flightPoint.setArrival(f.getArrival());
                Airport airport = airportRepository.findByCodeIataAirport(f.getIataCode());
                flightPoint.setLat(airport.getLatitudeAirport());
                flightPoint.setLon(airport.getLongitudeAirport());
                flightPoint.setAirportname(airport.getNameAirport());
                return flightPoint;
            }).collect(Collectors.toList());
            flight.setFlightPoints(flightPoints);
            flight.setSegments(c.getSegments());
            flight.setLegs(c.getLegs());
            return  flight ;
        }).collect(Collectors.toList());
        return flights;
    }
}

