package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.utils.Generater;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.JobApplication;
import com.thailandelite.mis.model.domain.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.LongSupplier;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@RequiredArgsConstructor
public class JobApplicationReportService {

    private final ExcelService excelService;
    private final MongoTemplate mongoTemplate;

    public Page<JobApplicationReportResponse> getJobApplicationReport(BaseReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<JobApplicationReportResponse> listResult = getJobApplication(searchRequest, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] getJobApplicationExcel(BaseReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query();
        List<JobApplicationReportResponse> listResult = getJobApplication(searchRequest, queryInfo);
        List<String> headers = Arrays.asList("Period", "Position", "Name", "Email", "Tel", "Status","Submit date");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        for (int i=0; i<listResult.size(); i++) {
            JobApplicationReportResponse cellValue = listResult.get(i);
            JobApplication job = cellValue.getJobApplication();
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            String name = job.getPersonal() != null ? job.getPersonal().getTitle() + " " + job.getPersonal().getFirstName() + " " + job.getPersonal().getLastName() : "-";
            String email = job.getPersonal() != null ? job.getPersonal().getEmail() : "-";
            String tel = job.getPersonal() != null ? job.getPersonal().getTelephoneNo() : "-";
            cells.add(new ExcelService.ExcelValue(cellValue.getDateTimePeriod()));
            cells.add(new ExcelService.ExcelValue(job.getJobPositionName()));
            cells.add(new ExcelService.ExcelValue(name));
            cells.add(new ExcelService.ExcelValue(email));
            cells.add(new ExcelService.ExcelValue(tel));
            cells.add(new ExcelService.ExcelValue(job.getStatus()));
            cells.add(new ExcelService.ExcelValue(job.getCreatedDate()));
            rows.add(cells);
        }
        return excelService.generateExcelReport("ApplicationByNation", headers, rows);

    }

    private List<JobApplicationReportResponse> getJobApplication(BaseReportRequest searchRequest, Query queryInfo) {
        List<JobApplicationReportResponse> listResult = new ArrayList<>();
        List<JobApplication> applicationQuery = getApplicationList(searchRequest, queryInfo);
        HashMap<String, List<JobApplication>> groupTimePeriod = new HashMap<>();
        String groupByFormat = Generater.getGroupByFormat(searchRequest);
        for (JobApplication application : applicationQuery) {
            ZonedDateTime zonedDateTime = application.getCreatedDate().atZone(ZoneId.systemDefault());
            String timePeriodKey = DateUtils.toDateString(zonedDateTime, groupByFormat);
            List<JobApplication> applications = groupTimePeriod.get(timePeriodKey);
            if (applications == null)
                applications = new ArrayList<>();
            applications.add(application);
            groupTimePeriod.put(timePeriodKey, applications);
        }

        for (String timePeriodKey : groupTimePeriod.keySet()) {
            List<JobApplication> applications = groupTimePeriod.get(timePeriodKey);
            for (JobApplication application : applications) {
                JobApplicationReportResponse job = new JobApplicationReportResponse();
                job.setDateTimePeriod(timePeriodKey);
                job.setJobApplication(application);
                listResult.add(job);
            }
        }

        return listResult;
    }

    private List<JobApplication> getApplicationList(BaseReportRequest arg, Query queryInfo) {
        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("created_date").gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("created_date").gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("created_date").lt(arg.getTo()));
        }
        List<JobApplication> memberships = mongoTemplate.find(queryInfo, JobApplication.class);
        return memberships;
    }

}
