package com.thailandelite.mis.be.service.flow;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.VendorService;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBookingDrivingLicenseService extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private  final BPIncidentService bpIncidentService;
    private final VendorService vendorService;

    public BPBookingDrivingLicenseService(CamundaService camundaService,
                                          BookingRepository bookingRepository,
                                          BookingService bookingService,
                                          VendorService vendorService,
                                          CaseActivityRepository caseActivityRepository,
                                          BPIncidentService bpIncidentService) {
        super(camundaService,
            new RSVN_Check_Document(bookingRepository, caseActivityRepository,vendorService),
            new EPL_Confirm_Booking(bookingRepository, caseActivityRepository),
            new GR_Send_Doc(bookingRepository, caseActivityRepository),
            new FIN_Issue_Cash_Receipt(bookingRepository, caseActivityRepository),
            new EPL_Booking_Confirmed(bookingRepository, caseActivityRepository),
            new EPL_Pending_Resolve(bookingRepository, caseActivityRepository, bpIncidentService),
            new User_Recheck_Booking_Epl(bookingRepository, caseActivityRepository),
            new Create_Incident_Case(bookingRepository, caseActivityRepository));
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
        this.bpIncidentService = bpIncidentService;
        this.vendorService = vendorService;
    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) throws UploadFailException, DocumentException, IOException {
        BookingFrom bookingFrom = (BookingFrom) object;
        Booking result = bookingService.createBooking(bookingFrom);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        result.setCaseId(this.caseInfoService.createBooking(processDefKey, RSVN_Check_Document.taskDefKey , process,result).getId());
        result.setBookingStatus(BookingStatus.CREATE);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        Booking booking = bookingService.getBookingDetailById(caseInfoDTO.getEntityId());


        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .booking(booking)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class RSVN_Check_Document implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final VendorService vendorService;
        public static final String taskDefKey = "RSVN_Check_Document";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING EPL");
                    vendorService.VendorConfirmNoti(caseInfo,"GOLF");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }



    @RequiredArgsConstructor
    private static class User_Recheck_Booking_Epl implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "User_Recheck_Booking_Epl";
        }
        public enum Action {USERCHANGE,USERCONFIRM,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Sendoc");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus("PENDING VENDOR");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("USERCANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


    @RequiredArgsConstructor
    private static class GR_Send_Doc implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc";
        }
        public enum Action {APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING FIN");
                    caseInfo.setTaskDefKey("FIN_Issue_Cash_Receipt");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


    @RequiredArgsConstructor
    private static class EPL_Confirm_Booking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;


          public String name() {
            return "EPL_Confirm_Booking";
        }
        public enum Action {APPROVE,RESUBMIT, RESUBMITRSVN}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus("PENDING USER");
                    caseInfo.setTaskDefKey("User_Recheck_Booking_Epl");
                    break;
                case RESUBMITRSVN:
                    caseInfo.setStatus("PENDING RSVN");
                    caseInfo.setTaskDefKey("RSVN_Check_Document");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GR_Send_Doc_Epl implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_Epl";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E-Doc");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class FIN_Issue_Cash_Receipt implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "FIN_Issue_Cash_Receipt";
        }
        public enum Action {APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Booking_Confirmed");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPL_Booking_Confirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPL_Booking_Confirmed";
        }
        public enum Action {NEXT,USERCHANGE,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case NEXT:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Pending_Resolve");
                    break;
                case USERCHANGE:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("CANCEL");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPL_Pending_Resolve implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final BPIncidentService bpIncidentService;

        public String name() {
            return "EPL_Pending_Resolve";
        }
         public enum Action {DONE,INCIDENT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case INCIDENT:
                    caseInfo.setStatus("INCIDENT");
                    bpIncidentService.create("incident",caseInfo.getEntityId());
                    caseInfo.setTaskDefKey("Create_Incident_Case");
                    break;
            }
        }

        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


    @RequiredArgsConstructor
    private static class Create_Incident_Case implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Create_Incident_Case";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

}
