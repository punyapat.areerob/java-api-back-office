package com.thailandelite.mis.be.service.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String objectName) {
        super(objectName + " not found.");
    }

    public NotFoundException(String objectName, String id) {
        super(String.format("%s not found (%s)", objectName, id));
    }
}
