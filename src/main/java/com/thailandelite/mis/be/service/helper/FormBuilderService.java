package com.thailandelite.mis.be.service.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.thailandelite.mis.be.domain.BroadcastMessage;
import com.thailandelite.mis.be.repository.TeamRepository;
import io.easycm.framework.form.form.FieldType;
import io.easycm.framework.form.form.FormField;
import io.easycm.framework.form.generator.FieldGroup;
import io.easycm.framework.form.generator.FormlyFieldConfig;
import io.easycm.framework.form.generator.FormlyTemplateOptions;
import io.easycm.framework.form.generator.RadioOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.glassfish.hk2.utilities.reflection.ParameterizedTypeImpl;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;

import static java.lang.reflect.Modifier.isStatic;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.*;
import static org.springframework.util.StringUtils.hasText;

/**
 * Created by Fernando on 28/12/2014.
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class FormBuilderService {
    public static final String OPTIONS_TEAM = "OPTIONS_TEAM";

    public static final Set<Class> NUMBERS = Sets.newHashSet(String.class, Integer.class, Long.class, Float.class);
    public static final Set<Class> DATES = Sets.newHashSet(ZonedDateTime.class, Date.class, Instant.class);

    private final TeamRepository teamRepository;

    public List<FormlyFieldConfig> build(Class<?> cls) throws ClassNotFoundException {
        return this.getForm(cls);
    }

    private List<RadioOptions> getEntityOptions(String entity) {
        if (entity.equals(OPTIONS_TEAM)) {
   /*         return teamRepository.findAll().stream()
                .map(v -> new RadioOptions(v.getId(), v.getJobTitleEN()))
                .collect(toList());*/
        }

        return Collections.emptyList();
    }

    public List<FormlyFieldConfig> getForm(Class<?> current) throws ClassNotFoundException {
        // make form
        List<FormlyFieldConfig> formlyFieldConfigs = new ArrayList<>();
        List<Field> fields = Arrays.stream(current.getDeclaredFields()).
            filter(f -> !isStatic(f.getModifiers()))
            .collect(toList());

        for (Field field : fields) {
            FormlyFieldConfig formlyFieldConfig = new FormlyFieldConfig();
//            formlyFieldConfig.setClassName("form-group");
            formlyFieldConfig.setKey(field.getName());

            FormlyTemplateOptions templateOptions = formlyFieldConfig.getTemplateOptions();

            String label = null;
            FormField fieldAnnotation = null;
            if (field.isAnnotationPresent(FormField.class)) {
                fieldAnnotation = field.getAnnotation(FormField.class);

                label = fieldAnnotation.label();
                templateOptions.setRequired(fieldAnnotation.required());
                if (hasText(fieldAnnotation.description())) {
                    templateOptions.setDescription(fieldAnnotation.description());
                }

                if (fieldAnnotation.hide()) {
                    formlyFieldConfig.setHide(true);
                }
            }

            templateOptions.setLabel(hasText(label) ? label : camelToWord(field));

            Class<?> type = field.getType();
            if (type.equals(String.class)) {
                buildStringField(formlyFieldConfig, templateOptions, fieldAnnotation, field);

                formlyFieldConfigs.add(formlyFieldConfig);
            } else if (type.equals(Boolean.class)) {
                formlyFieldConfig.setType("checkbox");//toggle
                setValidation(field, templateOptions);

                formlyFieldConfigs.add(formlyFieldConfig);
            } else if (type.isEnum()) {
                List<RadioOptions> options = getEnumValue(type);
                templateOptions.setOptions(options);
                setValidation(field, templateOptions);
                formlyFieldConfig.setType(options.size() > 2 ? "select" : "radio");

                formlyFieldConfigs.add(formlyFieldConfig);
            } else if (NUMBERS.contains(type)) {
                formlyFieldConfig.setType("input");
                formlyFieldConfig.getTemplateOptions().setType("number");
                setValidation(field, templateOptions);

                formlyFieldConfigs.add(formlyFieldConfig);
            } else if (DATES.contains(type)) {
                formlyFieldConfig.setType("datepicker");
                setValidation(field, templateOptions);

                formlyFieldConfigs.add(formlyFieldConfig);
            } else if (type.equals(List.class)) {
                buildListField(formlyFieldConfig, templateOptions, fieldAnnotation, field);

                formlyFieldConfigs.add(formlyFieldConfig);
            } else if (type.getName().startsWith("com.thailandelite")) {
                FormlyFieldConfig subForm = new FormlyFieldConfig();
                subForm.setKey(field.getName());
//                subForm.setWrappers(Lists.newArrayList("panel"));
                subForm.getTemplateOptions().setLabel(field.getName());
                subForm.setFieldGroup(this.getForm(field.getType()));

                formlyFieldConfigs.add(subForm);
            }
        }

        return formlyFieldConfigs;
    }

    private void buildStringField(FormlyFieldConfig formlyFieldConfig, FormlyTemplateOptions templateOptions, FormField fieldAnnotation, Field field) {
        if (fieldAnnotation != null) {
            FieldType typeAnnotation = fieldAnnotation.type();
            if (typeAnnotation == FieldType.AUTO) {
                formlyFieldConfig.setType("input");
            } else if (typeAnnotation == FieldType.SELECT) {
                formlyFieldConfig.setType("select");
                buildSelectOptions(templateOptions, fieldAnnotation);
            } else {
                formlyFieldConfig.setType(typeAnnotation.getType());//textarea
            }
        } else {
            formlyFieldConfig.setType("input");//textarea
        }

        if (field.getName().equals("id")) {
            formlyFieldConfig.setHide(true);
        } else {
            setValidation(field, templateOptions);
        }
    }

    private void buildListField(FormlyFieldConfig formlyFieldConfig, FormlyTemplateOptions templateOptions, FormField fieldAnnotation, Field field) throws ClassNotFoundException {
        formlyFieldConfig.setKey(field.getName());
        formlyFieldConfig.getTemplateOptions().setLabel(field.getName());

        if (fieldAnnotation != null && fieldAnnotation.type() == FieldType.SELECT) {
            FieldType type1 = fieldAnnotation.type();

            buildSelectOptions(templateOptions, fieldAnnotation);

            templateOptions.setMultiple(true);
            formlyFieldConfig.setType("select");
        } else {
            formlyFieldConfig.setType("repeat");
            formlyFieldConfig.getTemplateOptions().setAddText("Add new item");

            Type genericType = field.getGenericType();

            Type actualTypeArgument = ((ParameterizedTypeImpl) genericType).getActualTypeArguments()[0];
            Class<?> aClass1 = Class.forName(actualTypeArgument.getTypeName());
            FieldGroup formGroup = new FieldGroup();
            formGroup.setFieldGroup(this.getForm(aClass1));
            formlyFieldConfig.setFieldArray(formGroup);
        }
    }

    private void buildSelectOptions(FormlyTemplateOptions templateOptions, FormField fieldAnnotation) {
        String[] selectOptions = fieldAnnotation.options();
        if (selectOptions.length != 0) {
            List<RadioOptions> radios = Arrays.stream(selectOptions).map(s -> new RadioOptions(s, s)).collect(toList());
            templateOptions.setOptions(radios);
        } else if (hasText(fieldAnnotation.entity())) {
            List<RadioOptions> radios = this.getEntityOptions(fieldAnnotation.entity());
            templateOptions.setOptions(radios);
        }

        List<RadioOptions> options = templateOptions.getOptions();
        if (options == null || options.isEmpty()) {
            log.warn("RadioOptions is empty: {}", templateOptions.getLabel());
        }
    }

    private String camelToWord(Field field) {
        return capitalize(join(splitByCharacterTypeCamelCase(field.getName()), StringUtils.SPACE));
    }

    private void setValidation(Field field, FormlyTemplateOptions templateOptions) {
        // check JPA annotations for number
        if (field.isAnnotationPresent(Size.class)) {
            Size size = field.getAnnotation(Size.class);
            templateOptions.setMax((long) size.max());
            templateOptions.setMin((long) size.min());
        } else {
            if (field.isAnnotationPresent(Min.class)) {
                Min min = field.getAnnotation(Min.class);
                templateOptions.setMin(min.value());
            }
            if (field.isAnnotationPresent(Max.class)) {
                Max max = field.getAnnotation(Max.class);
                templateOptions.setMax(max.value());
            }
        }
    }

    private static List<RadioOptions> getEnumValue(Class cls1) {
        Class<Enum> cls = cls1;
        return Arrays.stream(cls.getEnumConstants()).map(e -> {
            RadioOptions radioOptions = new RadioOptions();
            radioOptions.setLabel(enumToWord(e.name()));
            radioOptions.setValue(e.name());
            return radioOptions;
        }).collect(toList());
    }

    private static String enumToWord(String str) {
        return WordUtils.capitalizeFully(str, '_')
            .replaceAll("_", " ");
    }
}
