package com.thailandelite.mis.be.service;

import com.google.common.base.Verify;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.CardPrivilege;
import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.model.domain.Membership;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.thailandelite.mis.be.common.Utils.listToSet;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberPrivilegeService {
    private final CardService cardService;
    private final MembershipRepository membershipRepository;
    private final MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository;

    public List<MemberPrivilegeQuota> getPrivilegeQuota(String membershipId) {
        Membership membership = membershipRepository.findById(membershipId)
            .orElseThrow(() -> new RuntimeException("Membership not found"));

        List<MemberPrivilegeQuota> memberPrivilegeQuotas = memberPrivilegeQuotaRepository.findAllByMembershipId(membershipId, ZonedDateTime.now());

        Set<String> memberPrivilegeIds = listToSet(memberPrivilegeQuotas, MemberPrivilegeQuota::getPrivilegeId);

        List<MemberPrivilegeQuota> cardPrivileges = cardService.getPrivilege(membership.getCardId())
            .stream()
            .filter(cp -> !memberPrivilegeIds.contains(cp.getPrivilege().getId()))
            .map(cardPrivilege -> map(membership, cardPrivilege))
            .collect(toList());

        memberPrivilegeQuotas.addAll(cardPrivileges);
        return memberPrivilegeQuotas;
    }

    public boolean deductQuota(String membershipId, String privilegeId) {
        int year = ZonedDateTime.now().getYear();

        MemberPrivilegeQuota quota = memberPrivilegeQuotaRepository
            .findByMembershipIdAndPrivilegeId(membershipId, privilegeId, ZonedDateTime.now())
            .orElse(this.newMemberPrivilegeQuota(membershipId, privilegeId, year));

//        Verify.verify(quota.getQuotaUsed() < quota.getQuotaQuantity(), "Quota limit");
        if(quota.getQuotaUsed() < quota.getQuotaQuantity()) {
            quota.setQuotaUsed(quota.getQuotaUsed() + 1);
            memberPrivilegeQuotaRepository.save(quota);
            return true;
        }else{
            return false;
        }
    }

    public void returnQuota(String membershipId, String privilegeId) {
        MemberPrivilegeQuota quota = memberPrivilegeQuotaRepository
            .findByMembershipIdAndPrivilegeId(membershipId, privilegeId, ZonedDateTime.now())
            .orElseThrow(() -> new RuntimeException("Privilege quota is not used"));

        quota.setQuotaUsed(quota.getQuotaUsed() - 1);
        memberPrivilegeQuotaRepository.save(quota);
    }

    @NotNull
    private MemberPrivilegeQuota newMemberPrivilegeQuota(String membershipId, String privilegeId, int year) {
        Membership membership = membershipRepository.findById(membershipId)
            .orElseThrow(() -> new RuntimeException("Membership not found"));

        CardPrivilege cardPrivilege = cardService.getPrivilege(membership.getCardId()).stream()
            .filter(p -> Objects.equals(p.getPrivilege().getId(), privilegeId))
            .findFirst().orElseThrow(() -> new NotFoundException("Privilege"));

        MemberPrivilegeQuota quota = new MemberPrivilegeQuota();
        quota.setMembershipId(membershipId);
        quota.setPrivilegeId(privilegeId);
        quota.setPrivilegeName(cardPrivilege.getPrivilege().getNameEN());
        quota.setYearQuota(String.valueOf(year));
        quota.setQuotaAdjust(0);
        quota.setQuotaUsed(0);
        quota.setActive(true);

        CardPrivilege.QuotaCycle quotaCycle = cardPrivilege.getQuotaCycle();
        if (quotaCycle == CardPrivilege.QuotaCycle.YEAR) {
            ZonedDateTime startOfYear = LocalDate.of(year, 1, 1).atStartOfDay(ZoneId.systemDefault());
            quota.setStart(startOfYear);
            quota.setEnd(startOfYear.plusYears(1));
        } else {
            quota.setStart(membership.getStartDate());
            quota.setEnd(membership.getEndDate());
        }

        if (quotaCycle == CardPrivilege.QuotaCycle.NONE) {
            quota.setQuotaQuantity(0);
        } else if (quotaCycle == CardPrivilege.QuotaCycle.UNLIMIT) {
            quota.setQuotaQuantity(9999);
        } else {
            quota.setQuotaQuantity(cardPrivilege.getQuota());
        }

        log.info("New membershipPrivilegeQuota create: {}", quota);
        return quota;
    }

    private MemberPrivilegeQuota map(Membership membership, CardPrivilege cardPrivilege) {
        MemberPrivilegeQuota memberPrivilegeQuota = new MemberPrivilegeQuota();
        memberPrivilegeQuota.setMembershipId(membership.getId());
        memberPrivilegeQuota.setMemberId(membership.getMemberId());
        memberPrivilegeQuota.setPrivilegeId(cardPrivilege.getPrivilege().getId());
        memberPrivilegeQuota.setPrivilegeName(cardPrivilege.getPrivilege().getNameEN());
        memberPrivilegeQuota.setYearQuota(String.valueOf(ZonedDateTime.now().getYear()));
        memberPrivilegeQuota.setQuotaQuantity(cardPrivilege.getQuota());
        memberPrivilegeQuota.setQuotaAdjust(0);
        memberPrivilegeQuota.setQuotaUsed(0);
        memberPrivilegeQuota.setActive(true);

        switch (cardPrivilege.getQuotaCycle()) {
            case YEAR:
                long between = ChronoUnit.YEARS.between(ZonedDateTime.now(), membership.getStartDate());
                memberPrivilegeQuota.setStart(membership.getStartDate());
                memberPrivilegeQuota.setEnd(membership.getStartDate().plusYears(between + 1));
                break;
            case LIFE_TIME:
                memberPrivilegeQuota.setStart(membership.getStartDate());
                memberPrivilegeQuota.setEnd(membership.getEndDate());
                break;
            case UNLIMIT:
                memberPrivilegeQuota.setQuotaQuantity(9999);
                memberPrivilegeQuota.setStart(membership.getStartDate());
                memberPrivilegeQuota.setEnd(membership.getEndDate());
                break;
        }

        return memberPrivilegeQuota;
    }
}
