package com.thailandelite.mis.be.service;
import com.thailandelite.mis.model.domain.agent.AgentLog.CallCenterStatus;
import java.time.ZonedDateTime;

import com.thailandelite.mis.model.domain.agent.AgentLog;
import com.thailandelite.mis.be.domain.Staff;
import com.thailandelite.mis.be.repository.AgentLogRepository;
import com.thailandelite.mis.be.repository.StaffRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class AgentLogService {
    private final AgentLogRepository agentLogRepository;
    private final StaffRepository staffRepository;

    public AgentLog newAgentLog(String agentId, String deskNo, String status) {
        Staff staff = staffRepository.findById(agentId).orElseThrow(() -> new RuntimeException("Staff not found"));

        AgentLog agentLog = new AgentLog();
        agentLog.setTimeLog(ZonedDateTime.now());
        agentLog.setAgentName(staff.getFirstName());
//        agentLog.setAgentName(agentName);
        switch (status) {
            case "available":
                agentLog.setStatus(CallCenterStatus.AVAILABLE);
                break;
            case "break":
                agentLog.setStatus(CallCenterStatus.BREAK);
                break;
            case "lunch":
                agentLog.setStatus(CallCenterStatus.LUNCH);
                break;
            case "persn":
                agentLog.setStatus(CallCenterStatus.PERSONAL);
                break;
            case "train":
                agentLog.setStatus(CallCenterStatus.TRAINING);
                break;
            case "assmnt":
                agentLog.setStatus(CallCenterStatus.ASSIGNMENT);
                break;
            case "logout":
                agentLog.setStatus(CallCenterStatus.LOGOUT);
                break;
            case "login":
                agentLog.setStatus(CallCenterStatus.LOGIN);
                break;
        }
        agentLog.setDesk(deskNo);

        agentLogRepository.save(agentLog);
        return agentLog;
    }
}
