package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentContract;
import lombok.Data;

@Data
public class AgentReportResponse {
    private Agent agent;
    private AgentContract agentContract;
}
