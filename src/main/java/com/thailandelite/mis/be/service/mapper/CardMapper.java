package com.thailandelite.mis.be.service.mapper;


import com.thailandelite.mis.be.service.dto.CardDTO;

import com.thailandelite.mis.model.domain.Card;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {})
public interface CardMapper extends EntityMapper<CardDTO, Card> {

    default Card fromId(String id) {
        if (id == null) {
            return null;
        }
        Card card = new Card();
        card.setId(id);
        return card;
    }
}
