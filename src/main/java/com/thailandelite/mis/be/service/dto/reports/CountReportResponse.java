package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.Nationality;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class CountReportResponse {
    private Integer amount;
    private String name;
    private String dateTimePeriod;
}
