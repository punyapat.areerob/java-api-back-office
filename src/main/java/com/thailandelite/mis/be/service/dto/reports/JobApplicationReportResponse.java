package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.JobApplication;
import lombok.Data;

@Data
public class JobApplicationReportResponse {
    String dateTimePeriod;
    JobApplication jobApplication;
}
