package com.thailandelite.mis.be.service.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailandelite.mis.be.domain.BroadcastMessage;
import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.web.rest.EmailTransactionResource;
import io.easycm.framework.form.generator.FormlyFieldConfig;

import java.util.List;

public class TestFormBuilder {
    public static void main(String[] args) throws JsonProcessingException, ClassNotFoundException {
        FormBuilderService formBuilderService = new FormBuilderService(null);
        List<FormlyFieldConfig> form = formBuilderService.getForm(EmailTransaction.class);
//        List<FormlyFieldConfig> form = formBuilderService.getForm(BroadcastMessage.class);

        ObjectMapper objectMapper = new ObjectMapper();
//        String s = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(form);
        String s = objectMapper.writeValueAsString(form);
        System.out.println(s);
    }
}
