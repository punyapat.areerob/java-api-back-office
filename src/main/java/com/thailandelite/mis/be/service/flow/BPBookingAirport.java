package com.thailandelite.mis.be.service.flow;

import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.VendorService;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBookingAirport extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private  final  BPIncidentService bpIncidentService;
    private final VendorService vendorService;
    public BPBookingAirport(CamundaService camundaService,
                            BookingRepository bookingRepository,
                            VendorService vendorService,
                            CaseActivityRepository caseActivityRepository,
                            BookingService bookingService,
                            BPIncidentService bpIncidentService
                            ) {
        super(camundaService,
            new RSVN_Confirm_Booking(bookingRepository, caseActivityRepository,vendorService),
            new Vendor_Booking_Confirmed(bookingRepository, caseActivityRepository),
            new User_Recheck_Booking(bookingRepository, caseActivityRepository),
            new ALY_Pending_Resolve(bookingRepository, caseActivityRepository,bpIncidentService),
            new Create_Incident_Case(bookingRepository, caseActivityRepository)
        );
        this.bpIncidentService = bpIncidentService;
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
        this.vendorService = vendorService;
    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) {
        BookingFrom bookingFrom = (BookingFrom) object;
        Booking result = bookingService.createBooking(bookingFrom);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        result.setCaseId(this.caseInfoService.createBooking(processDefKey, RSVN_Confirm_Booking.taskDefKey , process,result).getId());
        result.setBookingStatus(BookingStatus.CREATE);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        Booking booking = bookingService.getBookingDetailById(caseInfoDTO.getEntityId());


        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .booking(booking)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class RSVN_Confirm_Booking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final VendorService vendorService;
        public static final String taskDefKey = "RSVN_Confirm_Booking";
        public String name() {
            return "RSVN_Confirm_Booking";
        }
        public enum Action {RECHECK,RESUBMIT, APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {

                case RESUBMIT:
                    caseInfo.setStatus("PENDING RE-SUBMIT");
                    caseInfo.setTaskDefKey("User_Recheck_Booking");
                    break;
                case APPROVE:
                    caseInfo.setStatus("PENDING VENDOR");
                    vendorService.VendorConfirmNoti(caseInfo,"GOLF");
                    caseInfo.setTaskDefKey("Vendor_Booking_Confirmed");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Vendor_Booking_Confirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Vendor_Booking_Confirmed";
        }
        public enum Action {NEXT,USERCHANGE,USERCANCEL,VENDORCHANGE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("PENDING RSVN");
                    caseInfo.setTaskDefKey("RSVN_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
                case NEXT:
                    caseInfo.setStatus("PENDING ALY");
                    caseInfo.setTaskDefKey("ALY_Pending_Resolve");
                    break;
                case VENDORCHANGE:
                    caseInfo.setStatus("PENDING USER");
                    caseInfo.setTaskDefKey("User_Recheck_Booking");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class ALY_Pending_Resolve implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
    final BPIncidentService bpIncidentService;
        public String name() {
            return "ALY_Pending_Resolve";
        }
        public enum Action {DONE,INCIDENT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case INCIDENT:
                    caseInfo.setStatus("INCIDENT");
                    bpIncidentService.create("incident",caseInfo.getEntityId());

                    caseInfo.setTaskDefKey("Create_Incident_Case");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class User_Recheck_Booking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "User_Recheck_Booking";
        }
        public enum Action {USERCHANGE,USERCONFIRM,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("PENDING RSVN");
                    caseInfo.setTaskDefKey("RSVN_Confirm_Booking");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus("PENDING EPA");
                    caseInfo.setTaskDefKey("Vendor_Booking_Confirmed");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


    @RequiredArgsConstructor
    private static class Create_Incident_Case implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Create_Incident_Case";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

}
