package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.Application;
import lombok.Data;

@Data
public class ApplicationReportResponse extends CountReportResponse{
    private String dateTimePeriod;
    private Application application;

}
