package com.thailandelite.mis.be.service.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DoActionDto {
    @NotNull
    private String actionId;
    @NotNull
    private String remark;
    @NotNull
    private String caseInstanceId;
}
