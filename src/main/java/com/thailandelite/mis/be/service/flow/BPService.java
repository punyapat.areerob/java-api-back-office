package com.thailandelite.mis.be.service.flow;
import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.model.domain.JobApplication;
import com.thailandelite.mis.model.domain.enumeration.CaseActivityEvent;

import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.repository.CaseInfoRepository;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.be.service.dto.TaskWrapper;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQuery;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.domain.ApplicationMemberResponse;
import com.thailandelite.mis.model.domain.MemberPaymentWithMemberResponse;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.IOException;
import java.util.*;

import static io.jsonwebtoken.lang.Strings.hasText;
import static java.util.stream.Collectors.toList;

@Slf4j
public abstract class BPService<T> {

    protected final CamundaService camundaService;
    protected final List<ITask> taskDefs;
    @Autowired
    protected CaseInfoRepository caseInfoRepository;
    @Autowired
    private CaseActivityRepository caseActivityRepository;
    @Autowired
    protected CaseInfoService caseInfoService;
    @Autowired
    private ModelMapper modelMapper;

    public abstract Object create(String flowDefKey, Object object) throws UploadFailException, DocumentException, IOException;

    public abstract CaseInfoDTO getDetail(String caseId);

    protected abstract Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList);



    public BPService(CamundaService camundaService, ITask... iTasks) {
        this.modelMapper = new ModelMapper();
        this.camundaService = camundaService;
        this.taskDefs = Arrays.asList(iTasks);
    }

    protected TaskWrapper<T> mapToTaskWrapper(Task task, T data) {
        Enum<?>[] actions = taskDefs.stream()
            .filter(taskDef -> isNameEquals(task, taskDef))
            .findAny()
            .map(ITask::actions)
            .orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));

        return new TaskWrapper<>(task, actions, data);
    }

    private boolean isNameEquals(Task task, ITask taskDef) {
        return task.getTaskDefinitionKey().equalsIgnoreCase(taskDef.name());
    }

    boolean isNameEquals(String taskDefinitionKe, ITask taskDef) {
        return taskDefinitionKe.equalsIgnoreCase(taskDef.name());
    }

    public ProcessInstance startProcess(String proccDefKey,String businessKey) {
        return camundaService.startProcess(proccDefKey, businessKey);
    }

    public void claimTask(String processInstanceId, String user) {
        String taskId = camundaService.getTaskIdFromProcessId(processInstanceId);
        camundaService.claim(taskId, user);
    }

    public void unClaimTask(String taskId) {
        camundaService.claim(taskId, null);
    }

    public void processTask(String processInstanceId, String taskDefKey, String action , String user, String remark)  {
        CaseInfo caseInfo = caseInfoRepository.findByProcessInstanceId(processInstanceId).
            orElseThrow(() -> new RuntimeException("Case not found"));

        String taskId = camundaService.getTaskIdFromProcessId(processInstanceId);
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));

        HashMap<String, Object> vars = Maps.newHashMap();
        vars.put("action", action);

        CaseActivity caseLog = new CaseActivity();
        caseLog.setCaseInstanceId(caseInfo.getProcessInstanceId());
        caseLog.setEvent(CaseActivityEvent.ACTION);
        caseLog.setAction(action);
        caseLog.setRemark(remark);

        iTask.process(action, vars, taskId , user, remark, caseInfo, caseLog);

        caseInfoRepository.save(caseInfo);

        caseLog.setDetail(user + " changed CASE("+processInstanceId+") status to " + caseInfo.getStatus());
        caseActivityRepository.save(caseLog);

        camundaService.complete(taskId, vars);
    }


    public List<String> getActiveProcessIds(String [] processDefKey,
                                            String candidateGroup) {
        return camundaService.getTasks(processDefKey, candidateGroup).stream()
            .map(Task::getProcessInstanceId)
            .collect(toList());
    }


    public Enum<?>[] getTaskByTaskDefKey( String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));

        return iTask.actions();
    }

    public CaseInfoDTO getCaseDetail(String caseId){
        CaseInfo caseInfo = caseInfoRepository.findById(caseId)
            .orElseThrow(() -> new RuntimeException("Not found"));

        CaseInfoDTO caseInfoDTO = modelMapper.map(caseInfo, CaseInfoDTO.class);
        try {
            List<Task> list = camundaService.createTaskQuery().processInstanceId(caseInfo.getProcessInstanceId()).list();
            Task task1 = list.get(0);
            Enum<?>[] actions = this.getActions(task1);
            caseInfoDTO.setActions(actions);
        } catch (Exception e) {
            log.debug("Get task action fail", e);
        }

        return caseInfoDTO;
    }

    protected Optional<TaskWrapper<T>> wrapTaskWithData(String processInstanceId, T applicant) {
        List<Task> list = camundaService.createTaskQuery().processInstanceId(processInstanceId).list();
        return list.stream().findAny().map(task -> mapToTaskWrapper(task, applicant));
    }

    protected Optional<TaskWrapper<ApplicationMemberResponse>> wrapTaskWithApplicantResponse(String processInstanceId, ApplicationMemberResponse applicant) {
        List<Task> list = camundaService.createTaskQuery().processInstanceId(processInstanceId).list();
        return (Optional<TaskWrapper<ApplicationMemberResponse>>) list.stream().findAny().map(task -> mapToTaskWrapperApplicationMemberResponse(task, applicant));
    }

    protected Optional<TaskWrapper<MemberPaymentWithMemberResponse>> wrapTaskWithMemberPaymentResponse(String processInstanceId, MemberPaymentWithMemberResponse memberResponse) {
        List<Task> list = camundaService.createTaskQuery().processInstanceId(processInstanceId).list();
        return (Optional<TaskWrapper<MemberPaymentWithMemberResponse>>) list.stream().findAny().map(task -> mapToTaskWrapperMemberPaymentWithMemberResponse(task, memberResponse));
    }

    protected TaskWrapper<MemberPaymentWithMemberResponse> mapToTaskWrapperMemberPaymentWithMemberResponse(Task task, MemberPaymentWithMemberResponse data) {
        Enum<?>[] actions = taskDefs.stream()
            .filter(taskDef -> isNameEquals(task, taskDef))
            .findAny()
            .map(ITask::actions)
            .orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));

        return new TaskWrapper<>(task, actions, data);
    }

    protected TaskWrapper<ApplicationMemberResponse> mapToTaskWrapperApplicationMemberResponse(Task task, ApplicationMemberResponse data) {
        Enum<?>[] actions = taskDefs.stream()
            .filter(taskDef -> isNameEquals(task, taskDef))
            .findAny()
            .map(ITask::actions)
            .orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));

        return new TaskWrapper<>(task, actions, data);
    }

    public Enum<?>[] getActions(Task task) {
        return taskDefs.stream()
            .filter(taskDef -> isNameEquals(task, taskDef))
            .findAny()
            .map(ITask::actions)
            .orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
    }

    public Page<CaseInfoDTO> queryCase(CaseQueryRequest request){
        CaseQuery caseQuery = modelMapper.map(request, CaseQuery.class);

        String candidateGroup = request.getCandidateGroup();
        if(hasText(candidateGroup)){
            List<String> processIds = this.getActiveProcessIds(new String[]{request.getFlowDefKey()}, candidateGroup);
            caseQuery.setProcessInstanceIds(processIds);
            if(processIds.size() == 0 )
            {
                return new PageImpl<CaseInfoDTO>(new ArrayList<>());
            }
        }

        Page<CaseInfo> caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery , JobApplication.class);
        return this.mapToDTO(caseInfoPage);
    }

    public Page<CaseInfoDTO> queryCaseWithEntity(CaseQueryRequest request, Class c){
        CaseQuery caseQuery = modelMapper.map(request, CaseQuery.class);

        String candidateGroup = request.getCandidateGroup();
        if(hasText(candidateGroup)){
            List<String> processIds = this.getActiveProcessIds(new String[]{request.getFlowDefKey()}, candidateGroup);
            caseQuery.setProcessInstanceIds(processIds);
            if(processIds.size() == 0 )
            {
                return new PageImpl<CaseInfoDTO>(new ArrayList<>());
            }
        }
        Page<CaseInfo> caseInfoPage;
        if(c.getName().contains("Membership")) {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }
        else if(c.getName().contains("JobApplication")) {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }
        else if(c.getName().contains("Application")) {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }
        else if(c.getName().contains("Incident")) {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }
        else {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }

        return this.mapToDTO(caseInfoPage);
    }

    public Page<CaseInfoDTO> queryCaseBooking(CaseQueryRequest request, Class c){
        CaseQuery caseQuery = modelMapper.map(request, CaseQuery.class);
        Page<CaseInfo> caseInfoPage = caseInfoService.queryByEntityBookingByClass(caseQuery, c);
        return this.mapToDTO(caseInfoPage);
    }


    public Page<CaseInfoDTO> queryCaseWithEntityMultiFlows(CaseQueryRequest request, Class c){
        CaseQuery caseQuery = modelMapper.map(request, CaseQuery.class);

        String candidateGroup = request.getCandidateGroup();
        if(hasText(candidateGroup)){
            List<String> processIds = this.getActiveProcessIds(request.getFlowDefKeys(), candidateGroup);
            caseQuery.setProcessInstanceIds(processIds);
            if(processIds.size() == 0 )
            {
                return new PageImpl<CaseInfoDTO>(new ArrayList<>());
            }
        }
        Page<CaseInfo> caseInfoPage;
        if(c.getName().contains("Membership")) {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }
        else if(c.getName().contains("Application")) {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }
        else {
            caseInfoPage = caseInfoService.queryByEntityByClass(caseQuery, c);
        }

        return this.mapToDTO(caseInfoPage);
    }

    public long queryUnseen(CaseQueryRequest request){
        CaseQuery caseQuery = modelMapper.map(request, CaseQuery.class);

        return caseInfoService.queryUnseen(caseQuery);
    }
}
