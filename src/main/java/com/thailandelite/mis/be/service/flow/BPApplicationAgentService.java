package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.InvoiceService;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.enumeration.*;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPApplicationAgentService extends BPService<Application> {

    public final ApplicationRepository applicationRepository;
    public final MemberRepository memberRepository;
    public final CardPrivilegeRepository cardPrivilegeRepository;
    public final CardRepository cardRepository;
    private final ApplicationContext applicationContext;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceService invoiceService;

    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPApplicationAgentService(CamundaService camundaService, ApplicationRepository applicationRepository
        , CaseActivityRepository caseActivityRepository
        , MemberRepository memberRepository
        , CardPrivilegeRepository cardPrivilegeRepository
        , InvoiceRepository invoiceRepository
        , CardRepository cardRepository
        , ApplicationContext applicationContext, InvoiceService invoiceService) {

        super(camundaService,
            new WaitingSlsCheckDoc(applicationRepository,caseActivityRepository,memberRepository),
            new UserUploadNewDoc(applicationRepository,caseActivityRepository,memberRepository),
            new SLSRejectApplication(applicationRepository,caseActivityRepository),
            new SLSApproveCloseCase(applicationRepository,caseActivityRepository, invoiceRepository,memberRepository, invoiceService),
            new GRCreatEdoc(applicationRepository,caseActivityRepository,memberRepository),
            new GRProcessToIM(applicationRepository,caseActivityRepository),
            new GRPendingIMResponse(applicationRepository,caseActivityRepository)
        );
        this.applicationContext = applicationContext;
        this.applicationRepository = applicationRepository;
        this.memberRepository = memberRepository;
        this.cardPrivilegeRepository = cardPrivilegeRepository;
        this.invoiceRepository = invoiceRepository;
        this.cardRepository = cardRepository;
        this.invoiceService = invoiceService;
    }

    public Application create(String processDefKey, Object object) {
        Application application = createCase(processDefKey, (Application) object);
        Card card = application.getCard();
        if(card != null
            && card.getAdditionalFamilyMemberSetting() != null
            && card.getAdditionalFamilyMemberSetting().getType().equals(Card.AdditionalFamilyMemberType.YES)){
            List<Application> addApplications = applicationRepository.findByRefApplicationId(application.getId());
            if(addApplications != null && addApplications.size() > 0) {
                for (Application addApplication : addApplications) {
                    this.createCase(processDefKey, addApplication);
                }
            }
        }
        return application;
    }

    @NotNull
    private Application createCase(String processDefKey, Application application) {
        applicationRepository.save(application);

        ProcessInstance process = super.startProcess(processDefKey, application.getId());

        application.setCaseId(this.caseInfoService.createApplication(processDefKey, WaitingSlsCheckDoc.taskDefKey, process, application).getId());
        application.setStatus(ApplicationStatus.VERIFY);
        applicationRepository.save(application);

        //// TODO: 9/4/2021 AD move to applicationService
        if(application.getIsCoreMember()) {
            String memberId = application.getMemberId();
            Member member = memberRepository.findById(memberId).orElseThrow(() -> new RuntimeException("Not found Member"));
            member.setStatus(MemberStatus.REGISTER);
            memberRepository.save(member);
        }
        return application;
    }
    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<Application> applications = applicationRepository.findByIdIn(entityIds);

        ImmutableMap<String, Application> idMap = Maps.uniqueIndex(applications, Application::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);

            Application application = idMap.get(caseInfo.getEntityId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder().application(application).build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        Application application = applicationRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        Member member = memberRepository.findById(application.getMemberId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        //// TODO: 10/4/2021 AD call CardService
        List<CardPrivilege> privileges = cardPrivilegeRepository.findAllByCardIdAndActive(application.getCard().getId(), true);

        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .application(application)
            .member(member)
            .privileges(privileges)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class WaitingSlsCheckDoc implements ITask {

        public static final String taskDefKey = "Pending_SLS_Check_Doc";

        public enum Action {APPROVE, RESUBMIT, SKIPTO}

        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));

            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E-Doc");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus("REQUEST RE-SUBMIT");
                    caseInfo.setTaskDefKey("Pending_Agent_Resubmit");
                    rejectApplication(application,user,remark,memberRepository);
                    break;
                case SKIPTO:
                    caseInfo.setStatus("GR APPROVED");
                    caseInfo.setTaskDefKey("SLS_Approve___Close_Case");
                    break;
            }

            applicationRepository.save(application);
        }
    }

    @RequiredArgsConstructor
    private static class UserUploadNewDoc implements ITask {
        public Application data;
        public enum Action {RESUBMIT}
        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;
        public String name() {
            return "Pending_Agent_Resubmit";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));

            switch (Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("NEW APPLICATION");
                    caseInfo.setTaskDefKey("Pending_SLS_Check_Doc");
                    String memberId = application.getMemberId();
                    Member member = memberRepository.findById(memberId).orElseThrow(() -> new RuntimeException("Not found Member"));
                    member.setStatus(MemberStatus.PENDING);
                    memberRepository.save(member);
                    application.setStatus(ApplicationStatus.VERIFY);
                    break;
            }
            applicationRepository.save(application);
        }

        @Override
        public Enum<?>[] actions() {
            return Action.values();
        }
    }

    @RequiredArgsConstructor
    private static class SLSRejectApplication implements ITask {
        public Application data;
        public enum Action {DONE}
        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;
        public String name() {
            return "SLS_Reject___Close_Case";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));

            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("SLS REJECTED");
                    break;
            }
            applicationRepository.save(application);
        }

        @Override
        public Enum<?>[] actions() {
            return Action.values();
        }
    }

    @RequiredArgsConstructor
    private static class SLSApproveCloseCase implements ITask {
        public Application data;
        public enum Action {MOVE, DONE}
        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;
        final InvoiceRepository invoiceRepository;
        final MemberRepository memberRepository;
        final InvoiceService invoiceService;
        public String name() {
            return "SLS_Approve___Close_Case";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));

            switch (Action.valueOf(actionName)) {
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Process_to_IM");
                    break;
                case DONE:
                    caseInfo.setTaskDefKey("");
                    Invoice invoice = invoiceRepository.insert(createInvoiceFromApplicant(application));
                    invoiceService.sendInvoiceEmail(invoice.getId());
                    caseInfo.setStatus("COMPLETE");
                    break;
            }

            applicationRepository.save(application);
        }
        private Invoice createInvoiceFromApplicant(Application application)
        {
            Member member = memberRepository.findById(application.getMemberId()).get();
            Invoice invoice = new Invoice();

            invoice.setDocumentId(application.getId());
            invoice.setMemberId(application.getMemberId());
            MemberInfo memberInfo = new MemberInfo();
            memberInfo.setGivenName(member.getGivenName());
            memberInfo.setSurName(member.getSurName());
            invoice.setMember(member);
            invoice.setPayType(PaymentType.MEMBERSHIP);
            invoice.setPackageAction(PackageAction.REGISTER_BY_AGENT);
            Card.MembershipFeeSetting feeSetting = application.getCard().getMembershipFee();
            invoice.setTotalAmount(feeSetting.getAmount());
            invoice.setTotalVat(feeSetting.getVat());
            Float includePrice = Card.VatType.INCLUDE.equals(feeSetting.getVatType()) ? feeSetting.getAmount() : feeSetting.getAmount() + feeSetting.getVat();
            invoice.setTotalAmountIncVat(includePrice);
            invoice.setStatus(InvoiceStatus.INITIAL);
            invoice.setPaidAmount(0f);
            Invoice.OrderDetail orderDetail = new Invoice.OrderDetail();
            orderDetail.setName(application.getCard().getName());
            orderDetail.setAmount(invoice.getTotalAmount());
            orderDetail.setAmountVat(invoice.getTotalVat());
            orderDetail.setAmountIncVat(invoice.getTotalAmountIncVat());
            orderDetail.setQuantity(1);
            ArrayList<Invoice.OrderDetail> orderDetailArrayList  = new ArrayList<>();
            orderDetailArrayList.add(orderDetail);
            invoice.setOrderDetails(orderDetailArrayList);

            return  invoice;
        }
        @Override
        public Enum<?>[] actions() {
            return Action.values();
        }
    }

    @RequiredArgsConstructor
    private static class GRPendingIMResponse implements ITask {

        public enum Action {APPROVE,REJECT,MOVE}

        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Pending_IM_Response";
        }

        public Enum<?>[] actions() {
            return Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("IM APPROVED");
                    caseInfo.setTaskDefKey("SLS_Approve___Close_Case");
                    break;
                case REJECT:
                    caseInfo.setStatus("IM REJECT");
                    caseInfo.setTaskDefKey("SLS_Reject___Close_Case");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Process_to_IM");
                    break;
            }
            applicationRepository.save(application);
        }
    }


    @RequiredArgsConstructor
    private static class GRProcessToIM implements ITask {

        public enum Action {APPROVE,MOVE}

        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Process_to_IM";
        }

        public Enum<?>[] actions() {
            return Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));

            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING IM");
                    caseInfo.setTaskDefKey("GR_Pending_IM_Response");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E-Doc");
                    break;
            }
            applicationRepository.save(application);
        }
    }


    @RequiredArgsConstructor
    private static class GRCreatEdoc implements ITask {

        public enum Action {APPROVE, REJECT, SKIPTO}

        final ApplicationRepository applicationRepository;
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public String name() {
            return "Pending_GR_create_E-Doc";
        }

        public Enum<?>[] actions() {
            return Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            Application application = applicationRepository.findById(caseInfo.getEntityId())
                .orElseThrow(() ->new RuntimeException("Not found application"));

            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Process_to_IM");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING USER RESUBMIT");
                    caseInfo.setTaskDefKey("Pending_Agent_Resubmit");
                    rejectApplication(application,user,remark,memberRepository);
                    break;
                case SKIPTO:
                    caseInfo.setStatus("GR APPROVED");
                    caseInfo.setTaskDefKey("SLS_Approve___Close_Case");
                    break;
            }
            applicationRepository.save(application);
        }
    }

    public String[] getBeans() {
        return applicationContext.getBeanNamesForType(BPService.class);
    }
}
