package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.AgentRepository;
import com.thailandelite.mis.be.repository.VendorRepository;
import com.thailandelite.mis.be.service.dto.AgentContractRequest;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.agent.Agent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AgentRequestService {
    private final AgentRepository agentRepository;
    private final FontEndService fontEndService;

    public Agent changePasswordRequest(String id, String newPassword) {
        Agent agent = agentRepository.findById(id).orElseThrow( () -> new NotFoundException("Not found vendor"));
        fontEndService.changePassword(agent.getAgentEmail(), newPassword);
        return agent;
    }

    public Agent changeEmailRequest(String id, String newEmail) {
        Agent agent = agentRepository.findById(id).orElseThrow( () -> new NotFoundException("Not found vendor"));
        fontEndService.changeEmail(agent.getAgentEmail(), newEmail);
        return agent;
    }

    public Agent changeMemberStatus(String id, Agent.AgentStatus status, String remark) {
        Agent agent = agentRepository.findById(id).orElseThrow( () -> new NotFoundException("Not found vendor"));
        agent.setAgentStatus(status);
        agentRepository.save(agent);
        return agent;
    }

    public Agent changeVendorContract(String id, AgentContractRequest agentContractRequest) {
        Agent agent = agentRepository.findById(id).orElseThrow( () -> new NotFoundException("Not found vendor"));
        agent.setContractDateStart(agentContractRequest.getStartContractDate());
        agent.setContractDateEnd(agentContractRequest.getEndContractDate());
        agent.setRemark(agentContractRequest.getRemark());
        agent = agentRepository.save(agent);
        return agent;
    }
}
