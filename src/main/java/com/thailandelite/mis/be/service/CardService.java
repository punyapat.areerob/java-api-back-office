package com.thailandelite.mis.be.service;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.CardPrivilege;
import com.thailandelite.mis.be.repository.CardPrivilegeRepository;
import com.thailandelite.mis.be.repository.CardRepository;
import com.thailandelite.mis.be.repository.PrivilegeRepository;
import com.thailandelite.mis.be.service.model.CardInfo;
import com.thailandelite.mis.model.domain.Member;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class CardService {
    private final CardRepository cardRepository;
    private final PrivilegeRepository privilegeRepository;
    private final CardPrivilegeRepository cardPrivilegeRepository;

    public CardInfo getCardInfo(String cardId){
        Card card = cardRepository.findById(cardId).orElseThrow(() -> new RuntimeException("Card not found"));
        List<CardPrivilege> cardPrivileges = cardPrivilegeRepository.findAllByCardIdAndActive(cardId, true);

        CardInfo cardInfo = new CardInfo();
        cardInfo.setCard(card);
        cardInfo.setCardPrivileges(cardPrivileges);

        return cardInfo;
    }


    public Card getCard(String cardId) {
        return cardRepository.findById(cardId).orElseThrow(() -> new NotFoundException("Card"));
    }

    public List<CardPrivilege> getPrivilege(String cardId){
        return cardPrivilegeRepository.findAllByCardIdAndActive(cardId, true);
    }

    public void isAllowToUpgrade(Card card, String upgradeCardId){
        Card.UpgradeSetting upgradeSetting = card.getUpgradeSettings().stream()
            .filter(Card.UpgradeSetting::getActive)
            .filter(setting -> Objects.equals(setting.getToCardId(), upgradeCardId))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("upgrade not allowed"));

        log.info("UpgradeSetting: {}", upgradeSetting);
    }

    public void isAllowToTypeChange(Card typeChangeCard, String oldCardId){
        Card oldCard = this.getCard(oldCardId);
        boolean isLifeTimeCard = oldCard.getValidityYear().equals(99);
        boolean is20YearsCard = typeChangeCard.getValidityYear().equals(20);

        if(!(isLifeTimeCard && is20YearsCard)) throw new RuntimeException("type change not allowed");

        log.info("TypeChangeCard: {}", typeChangeCard);
    }

    public Card changeMemberStatus(String id, Card.CardStatus status, String remark) {
        Card card = cardRepository.findById(id).orElseThrow( () -> new NotFoundException("Not found member"));
        card.setStatus(status);
        cardRepository.save(card);
        return card;
    }
}
