package com.thailandelite.mis.be.service;

import com.thailandelite.mis.model.domain.JobPositions;
import com.thailandelite.mis.model.domain.hr.JobPositionsStatus;
import com.thailandelite.mis.be.repository.JobPositionsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class HRService {
    private final JobPositionsRepository jobPositionsRepository;

    public List<JobPositions> findActiveJobPosition(){
        ZonedDateTime now = ZonedDateTime.now();
        return jobPositionsRepository.findAllActiveJob(now, JobPositionsStatus.ACTIVE);

        //helo
    }
}
