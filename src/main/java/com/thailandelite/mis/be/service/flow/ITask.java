package com.thailandelite.mis.be.service.flow;

import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.Remark;
import com.thailandelite.mis.model.domain.enumeration.ApplicationStatus;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Map;

public interface ITask {
    String name();

    void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog);
    Enum<?>[] actions();

    default void rejectApplication(Application application, String remarkBy, String remark, MemberRepository memberRepository){
        application.setStatus(ApplicationStatus.REVISE);
        application.getAddressForm().setStatus("Revise");
        application.getDocumentForm().setStatus("Revise");
        application.getOccupationForm().setStatus("Revise");
        application.getUserProfileForm().setStatus("Revise");
        application.getPassportForm().setStatus("Revise");

        if(application.getRejectRemarks() == null) {
            application.setRejectRemarks(new ArrayList<>());
        }

        Remark rem = new Remark();
        rem.setRemark(remark);
        rem.setRemarkBy(remarkBy);
        rem.setRemarkTime(ZonedDateTime.now());
        application.getRejectRemarks().add(rem);

        String memberId = application.getMemberId();
        Member member = memberRepository.findById(memberId).orElseThrow(() -> new RuntimeException("Not found Member"));
        member.setStatus(MemberStatus.REGISTER);
        memberRepository.save(member);
    }
}
