package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.repository.ProductRepository;
import com.thailandelite.mis.be.service.dto.reports.BookingReportRequest;
import com.thailandelite.mis.be.service.dto.reports.BookingReportResponse;
import com.thailandelite.mis.be.service.report.utils.Generater;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.LongSupplier;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@RequiredArgsConstructor
public class MemberFinderService {

    private final MemberRepository memberRepository;
    private final MongoTemplate mongoTemplate;


    public Page<Member> findMemberBy(String email, String membershipId, String mobileInThai, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<Member> listResult = getMemberList(email, membershipId, mobileInThai, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    private List<Member> getMemberList(String email, String membershipId, String mobileInThai, Query queryInfo) {

        if ( !StringUtils.isEmpty(email) )
            queryInfo.addCriteria(where("email").regex(email, "iu"));

        if ( !StringUtils.isEmpty(membershipId) )
            queryInfo.addCriteria(where("membershipId").regex(membershipId, "iu"));

        if ( !StringUtils.isEmpty(mobileInThai) )
            queryInfo.addCriteria(where("mobileInThai").regex(mobileInThai, "iu"));

        return mongoTemplate.find(queryInfo, Member.class);
    }
}
