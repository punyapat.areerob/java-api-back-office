package com.thailandelite.mis.be.service;
import com.google.common.base.Verify;
import com.thailandelite.mis.be.domain.enumeration.EmailTransactionStatus;

import java.io.File;
import java.time.ZonedDateTime;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.domain.SendEmailRequest;
import com.thailandelite.mis.be.repository.EmailTemplateRepository;
import com.thailandelite.mis.be.repository.EmailTransactionRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Application;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Nullable;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.util.StringUtils.hasText;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService {

    private final EmailTemplateRepository emailTemplateRepository;
    private final EmailTransactionRepository emailTransactionRepository;
    private final MailService mailService;

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public EmailTransaction sendEmailTemplate(SendEmailRequest request) throws IOException {
        Object context = request.getContext() != null ? request.getContext() : request.getVars();
        return sendEmailTemplate(request.getTo(), request.getSubject(), request.getTemplateName(), context);
    }
    public EmailTransaction sendEmailTemplate(SendEmailRequest request,String fileName,byte [] file) throws IOException {
        Object context = request.getContext() != null ? request.getContext() : request.getVars();
        return sendEmailTemplate(request.getTo(), request.getSubject(), request.getTemplateName(), context,fileName,file);
    }

    public EmailTransaction sendEmailTemplate(String to, @Nullable String subject, String templateName, Object context) throws IOException {
        EmailTemplate emailTemplate = emailTemplateRepository.findByName(templateName)
            .orElseThrow(() -> new NotFoundException("Not found Email Template"));

        Verify.verify(isValidEmailAddress(to), "Email is not valid");

        Handlebars handlebars = new Handlebars();
        if (isEmpty(subject)) {
            subject = emailTemplate.getSubject();
        }

        String applySubject = handlebars.compileInline(subject).apply(subject);
        String applyHtml = handlebars.compileInline(emailTemplate.getContent()).apply(context);

        EmailTransaction transaction = new EmailTransaction();
        try {
            mailService.sendEmailSync(to, applySubject, applyHtml, false, true , null);
            transaction.setStatus(EmailTransactionStatus.SUCCESS);
        } catch (MailException | MessagingException e) {
            log.warn("Send mail fail", e);
            transaction.setError(e.getMessage());
            transaction.setStatus(EmailTransactionStatus.FAIL);
        }

        transaction.setTemplateName(templateName);
        transaction.setTo(to);
        transaction.setSubject(applySubject);
        transaction.setBody(applyHtml);
        transaction.setSendDate(ZonedDateTime.now());

        return emailTransactionRepository.save(transaction);
    }

    public EmailTransaction sendEmailTemplate(String to, @Nullable String subject, String templateName, Object context,String filename, byte [] bytes) throws IOException {
        EmailTemplate emailTemplate = emailTemplateRepository.findByName(templateName)
            .orElseThrow(() -> new NotFoundException("Not found Email Template"));
        Verify.verify(isValidEmailAddress(to), "Email is not valid");
        Handlebars handlebars = new Handlebars();
        if (isEmpty(subject)) {
            subject = emailTemplate.getSubject();
        }
        String applySubject = handlebars.compileInline(subject).apply(subject);
        String applyHtml = handlebars.compileInline(emailTemplate.getContent()).apply(context);

        EmailTransaction transaction = new EmailTransaction();
        try {
            mailService.sendEmailSync(to, applySubject, applyHtml, false, true , filename,bytes);
            transaction.setStatus(EmailTransactionStatus.SUCCESS);
        } catch (MailException | MessagingException e) {
            log.warn("Send mail fail", e);
            transaction.setError(e.getMessage());
            transaction.setStatus(EmailTransactionStatus.FAIL);
        }

        transaction.setTemplateName(templateName);
        transaction.setTo(to);
        transaction.setSubject(applySubject);
        transaction.setBody(applyHtml);
        transaction.setSendDate(ZonedDateTime.now());

        return emailTransactionRepository.save(transaction);
    }


    public EmailTemplate save(EmailTemplate emailTemplate) {
        String name = emailTemplate.getName();
        Verify.verify(!name.contains(" "), "Not allow to have space bar");

        long count = emailTemplateRepository.countByName(name);
        Verify.verify(count == 0, "Duplicate name");

        emailTemplateRepository.save(emailTemplate);
        return emailTemplate;
    }


    public void sendInvoiceApplication(Application application)  {
        try {
            Map<String, Object> vars = new HashMap<>();
            vars.put("firstname", application.getUserProfileForm().getGivenName());
            vars.put("middlename", application.getUserProfileForm().getMiddleName());
            vars.put("surname", application.getUserProfileForm().getSurName());

            sendEmailTemplate("", "subject", "template", vars);
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
    }
}
