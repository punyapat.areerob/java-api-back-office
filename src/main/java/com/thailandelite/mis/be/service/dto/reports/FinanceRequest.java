package com.thailandelite.mis.be.service.dto.reports;

import lombok.Data;

import java.time.Instant;
import java.time.ZonedDateTime;

@Data
public class FinanceRequest {
    private String flowApplication;
    private ZonedDateTime from;
    private ZonedDateTime to;
}
