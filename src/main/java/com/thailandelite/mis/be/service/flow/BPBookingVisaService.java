package com.thailandelite.mis.be.service.flow;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.VendorService;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBookingVisaService extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private final  BPIncidentService bpIncidentService;
    private final VendorService vendorService;
    public BPBookingVisaService(CamundaService camundaService,
                                BookingRepository bookingRepository,
                                BookingService bookingService,
                                VendorService vendorService,
                                CaseActivityRepository caseActivityRepository,
                                BPIncidentService bpIncidentService) {
        super(camundaService,
            new RSVN_Check_Document(bookingRepository, caseActivityRepository,vendorService),
            new EPA_Confirm_Booking(bookingRepository, caseActivityRepository),
            new GR_Send_Doc_Epa(bookingRepository, caseActivityRepository),
            new EPA_Booking_Confirmed(bookingRepository, caseActivityRepository),
            new EPL_Pending_Resolve_Epa(bookingRepository, caseActivityRepository),
            new User_Recheck_Booking_Epa(bookingRepository, caseActivityRepository),
            new EPL_Confirm_Booking(bookingRepository, caseActivityRepository),
            new User_Recheck_Booking_Epa(bookingRepository, caseActivityRepository),
            new GR_Send_Doc_Epl(bookingRepository, caseActivityRepository),
            new FIN_Issue_Cash_Receipt(bookingRepository, caseActivityRepository),
            new EPL_Booking_Confirmed(bookingRepository, caseActivityRepository),
            new EPL_Pending_Resolve(bookingRepository, caseActivityRepository , bpIncidentService),
            new User_Recheck_Booking_Epl(bookingRepository, caseActivityRepository),
            new Create_Incident_Case(bookingRepository, caseActivityRepository),
            new GR_Send_Doc_User(bookingRepository, caseActivityRepository),
            new GR_Send_Doc_Vendor(bookingRepository, caseActivityRepository),
            new Vendor_Confirm_Booking(bookingRepository, caseActivityRepository ),
            new Vendor_Booking_Confirmed(bookingRepository, caseActivityRepository),
            new ALY_Pending_Resolve(bookingRepository, caseActivityRepository ,bpIncidentService),
            new User_Recheck_Booking_Vendor(bookingRepository, caseActivityRepository),
            new Create_Incident_Case_Vendor(bookingRepository, caseActivityRepository),
            new EPL_Pending_Resolve(bookingRepository, caseActivityRepository ,bpIncidentService));
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
        this.bpIncidentService = bpIncidentService;
        this.vendorService = vendorService;
    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) throws UploadFailException, DocumentException, IOException {
        BookingFrom bookingFrom = (BookingFrom) object;
        Booking result = bookingService.createBooking(bookingFrom);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        result.setCaseId(this.caseInfoService.createBooking(processDefKey, RSVN_Check_Document.taskDefKey , process,result).getId());
        result.setBookingStatus(BookingStatus.CREATE);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        Booking booking = bookingService.getBookingDetailById(caseInfoDTO.getEntityId());


        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .booking(booking)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class RSVN_Check_Document implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final VendorService vendorService;
        public static final String taskDefKey = "RSVN_Check_Document";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {EPL, VENDOR, USER, EPA}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (RSVN_Check_Document.Action.valueOf(actionName)) {
                case EPL:
                    caseInfo.setStatus("PENDING EPL");
                    vendorService.VendorConfirmNoti(caseInfo,"GOLF");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case EPA:
                    caseInfo.setStatus("PENDING EPA");
                    vendorService.VendorConfirmNoti(caseInfo,"GOLF");
                    caseInfo.setTaskDefKey("EPA_Confirm_Booking");
                    break;
                case VENDOR:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc_Vendor");
                    break;
                case USER:
                    caseInfo.setStatus("PENDING USER");
                    caseInfo.setTaskDefKey("GR_Send_Doc_User");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return RSVN_Check_Document.Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPA_Confirm_Booking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPA_Confirm_Booking";
        }
        public enum Action {APPROVE, RESUBMIT,RESUBMITRSVN}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc_Epa");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus("PENDING USER");
                    caseInfo.setTaskDefKey("User_Recheck_Booking_Epa");
                    break;
                case RESUBMITRSVN:
                    caseInfo.setStatus("PENDING RSVN");
                    caseInfo.setTaskDefKey("RSVN_Check_Document");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GR_Send_Doc_Epa implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_Epa";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("EPA_Booking_Confirmed");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPA_Booking_Confirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPA_Booking_Confirmed";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Pending_Resolve_Epa");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPL_Pending_Resolve_Epa implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPL_Pending_Resolve_Epa";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class User_Recheck_Booking_Epa implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "User_Recheck_Booking_Epa";
        }
        public enum Action {USERCHANGE,USERCONFIRM,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("PENDING EPA");
                    caseInfo.setTaskDefKey("EPA_Confirm_Booking");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc_Epa");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("USERCANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPL_Confirm_Booking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;


          public String name() {
            return "EPL_Confirm_Booking";
        }
        public enum Action {APPROVE,RESUBMIT, RESUBMITRSVN}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc_Epl");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus("PENDING USER");
                    caseInfo.setTaskDefKey("User_Recheck_Booking_Epl");
                    break;
                case RESUBMITRSVN:
                    caseInfo.setStatus("PENDING RSVN");
                    caseInfo.setTaskDefKey("RSVN_Check_Document");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GR_Send_Doc_Epl implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_Epl";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("PENDING FIN");
                    caseInfo.setTaskDefKey("FIN_Issue_Cash_Receipt");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class FIN_Issue_Cash_Receipt implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "FIN_Issue_Cash_Receipt";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Booking_Confirmed");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPL_Booking_Confirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPL_Booking_Confirmed";
        }
        public enum Action {NEXT,USERCHANGE,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case NEXT:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Pending_Resolve");
                    break;
                case USERCHANGE:
                    caseInfo.setStatus("PENDING EPL");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("CANCEL");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPL_Pending_Resolve implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final  BPIncidentService bpIncidentService;
        public String name() {
            return "EPL_Pending_Resolve";
        }
         public enum Action {DONE,INCIDENT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case INCIDENT:
                    caseInfo.setStatus("INCIDENT");
                    bpIncidentService.create("incident",caseInfo.getEntityId());

                    caseInfo.setTaskDefKey("Create_Incident_Case");
                    break;
            }
        }

        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class User_Recheck_Booking_Epl implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

         public String name() {
            return "User_Recheck_Booking_Epl";
        }
        public enum Action {USERCHANGE,USERCONFIRM,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Sendoc");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus("PENDING VENDOR");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("USERCANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Create_Incident_Case implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Create_Incident_Case";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GR_Send_Doc_User implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_User";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GR_Send_Doc_Vendor implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_Vendor";
        }
        public enum Action {RESUBMITRSVN,RESUBMIT,APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING VENDOR");
                    caseInfo.setTaskDefKey("Vendor_Confirm_Booking");
                    break;
                case RESUBMITRSVN:
                    caseInfo.setStatus("PENDING RSVN");
                    caseInfo.setTaskDefKey("RSVN_Check_Document");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus("PENDING USER");
                    caseInfo.setTaskDefKey("User_Recheck_Booking_Vendor");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Vendor_Confirm_Booking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Vendor_Confirm_Booking";
        }
         public enum Action {VENDORCHANGE,APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING VENDOR");
                    caseInfo.setTaskDefKey("Vendor_Booking_Confirmed");
                    break;
                case VENDORCHANGE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("User_Recheck_Booking_Vendor");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Vendor_Booking_Confirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Vendor_Booking_Confirmed";
        }
        public enum Action {NEXT,USERCANCEL,USERCHANGE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case NEXT:
                    caseInfo.setStatus("PENDING ALY");
                    caseInfo.setTaskDefKey("ALY_Pending_Resolve");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
                case USERCHANGE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc_Vendor");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class ALY_Pending_Resolve implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final  BPIncidentService bpIncidentService;
        public String name() {
            return "ALY_Pending_Resolve";
        }
         public enum Action {DONE,INCIDENT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case INCIDENT:
                    caseInfo.setStatus("INCIDENT");
                    bpIncidentService.create("incident",caseInfo.getEntityId());

                    caseInfo.setTaskDefKey("Create_Incident_Case_Vendor");
                    break;
            }
        }

        @Override
        public Enum<?>[] actions() { return Create_Incident_Case_Vendor.Action.values();}
    }

    @RequiredArgsConstructor
    private static class Create_Incident_Case_Vendor implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Create_Incident_Case_Vendor";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


    @RequiredArgsConstructor
    private static class User_Recheck_Booking_Vendor implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "User_Recheck_Booking_Vendor";
        }
        public enum Action {USERCHANGE,USERCONFIRM,USERCANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("GR_Send_Doc_Vendor");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus("PENDING VENDOR");
                    caseInfo.setTaskDefKey("Vendor_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus("USERCANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


}
