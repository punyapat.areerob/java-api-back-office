package com.thailandelite.mis.be.service.dto;

import lombok.ToString;

/**
 * A DTO representing a user, with his authorities.
 */
@ToString
public class MenuDTO {

    private String candidateGroup;

    public String getCandidateGroup() {
        return candidateGroup;
    }
    public void setCandidateGroup(String candidateGroup) {
        this.candidateGroup = candidateGroup;
    }

    private String team;

    public String getTeam() {
        return team;
    }
    public void setTeam(String team) {
        this.team = team;
    }

    private String flow;

    public String getFlow() {
        return flow;
    }
    public void setFlow(String flow) {
        this.flow = flow;
    }


}
