package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPUpgradePaymentService extends BPService<MemberPayment> {

    private final MemberPaymentRepository memberPaymentRepository;
    private final MemberRepository memberRepository;
    private final CardRepository cardRepository;
    private final InvoiceRepository invoiceRepository;
    private final PaymentTransactionRepository paymentTransactionRepository;
    private final BPActivateMembershipCRMService bpActivateMembershipService;
    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPUpgradePaymentService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , MemberPaymentRepository memberPaymentRepository
        , MemberRepository memberRepository
        , CardRepository cardRepository
        , InvoiceRepository invoiceRepository
        , PaymentTransactionRepository paymentTransactionRepository
        , BPActivateMembershipCRMService bpActivateMembershipService
        , ApplicationContext applicationContext) {

        super(camundaService,
            new FIN_verify_Upgrade_transaction(caseActivityRepository, memberPaymentRepository),
            new User_submit_for_review_Payment(caseActivityRepository, memberPaymentRepository ,memberRepository ),
            new CRM_approve_Upgrade_Transaction(caseActivityRepository, memberPaymentRepository,bpActivateMembershipService ),
            new User_submit_for_Verification_Payment(caseActivityRepository, memberPaymentRepository)
        );

        this.memberPaymentRepository = memberPaymentRepository;
        this.memberRepository = memberRepository;
        this.cardRepository = cardRepository;
        this.invoiceRepository = invoiceRepository;
        this.paymentTransactionRepository = paymentTransactionRepository;
        this.bpActivateMembershipService = bpActivateMembershipService;
    }

    @RequiredArgsConstructor
    private static class FIN_verify_Upgrade_transaction implements ITask {
        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;

        public enum Action {APPROVE, REJECT, INCOMPLETE}

        public String name() {
            return "FIN_verify_Upgrade_transaction";
        }

        public Enum<?>[] actions() {
            return Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING CRM");
                    caseInfo.setTaskDefKey("CRM_approve_Upgrade_Transaction");
                    break;
                case REJECT:
                    caseInfo.setStatus("PAYMENT REJECT");
                    caseInfo.setTaskDefKey("User_submit_for_Verification_Payment");
                    break;
                case INCOMPLETE:
                    caseInfo.setStatus("PAYMENT INCOMPLETE");
                    caseInfo.setTaskDefKey("User_submit_for_review_Payment");
                    break;
            }

            memberPaymentRepository.save(memberPayment);
        }
    }

    @RequiredArgsConstructor
    private static class User_submit_for_Verification_Payment implements ITask {
        public enum Action {RESUBMIT, CANCEL}

        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;


        public String name() {
            return "User_submit_for_Verification_Payment";
        }


        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("PENDING FIN");
                    caseInfo.setTaskDefKey("FIN_verify_Upgrade_transaction");
                    break;
                case CANCEL:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("CANCELLED");
                    break;
            }


            memberPaymentRepository.save(memberPayment);
        }

        @Override
        public Enum<?>[] actions() {
            return Action.values();
        }
    }

    @RequiredArgsConstructor
    private static class CRM_approve_Upgrade_Transaction implements ITask {
        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;
        final BPActivateMembershipCRMService bpActivateMembershipService;
        public Application data;

        public enum Action {DONE}

        public String name() {
            return "CRM_approve_Upgrade_Transaction";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
            }
            bpActivateMembershipService.create("activate_membership_by_crm" , memberPayment);
            memberPaymentRepository.save(memberPayment);

        }

        @Override
        public Enum<?>[] actions() {
            return Action.values();
        }

    }

    @RequiredArgsConstructor
    private static class User_submit_for_review_Payment implements ITask {
        public Application data;
        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;
        final MemberRepository memberRepository;

        public enum Action {CANCEL,RESUBMIT}


        public String name() {
            return "User_submit_for_review_Payment";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (User_submit_for_Verification_Payment.Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("PENDING FIN");
                    caseInfo.setTaskDefKey("FIN_verify_Upgrade_transaction");
                    break;
                case CANCEL:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("CANCELLED");
                    break;
            }
            memberPaymentRepository.save(memberPayment);
        }

        @Override
        public Enum<?>[] actions() {
            return Action.values();
        }
    }

    @Override
    public MemberPayment create(String processDefKey, Object object) {
        MemberPayment memberPayment = (MemberPayment) object;
        ProcessInstance process = super.startProcess(processDefKey, memberPayment.getId());
        memberPaymentRepository.insert(memberPayment);
        caseInfoService.createMemberPayment(processDefKey,process,memberPayment,"FIN_verify_Upgrade_transaction");

        return memberPayment;
    }

    public MemberPayment save(MemberPayment memberPayment) {
        memberPaymentRepository.save(memberPayment);
        return memberPayment;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<MemberPayment> applications = memberPaymentRepository.findByIdIn(entityIds);

        ImmutableMap<String, MemberPayment> idMap = Maps.uniqueIndex(applications, MemberPayment::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            MemberPayment memberPayment = idMap.get(caseInfo.getEntityId());
            Member member = memberRepository.findById(memberPayment.getMemberId())
                .orElseThrow(() -> new RuntimeException("Not found"));

            Invoice invoice = invoiceRepository.findById(memberPayment.getInvoiceId()).orElseThrow(() -> new RuntimeException("Not found"));
            Card card = cardRepository.findById(memberPayment.getCardId()).orElseThrow(() -> new RuntimeException("Not found"));
            List<PaymentTransaction> paymentTransactions = paymentTransactionRepository.findByInvoiceId(memberPayment.getInvoiceId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .memberPayment(memberPayment)
                .member(member)
                .invoice(invoice)
                .card(card)
                .paymentTransactions(paymentTransactions)
                .build();
            dest.setData(data);
            return dest;
        });
    }

    public byte[] downloadFile(String path) throws Exception {
        FileService fileService = new FileService();
        return fileService.getFile(path);
    }

    public String uploadFile(String path, MultipartFile file) throws UploadFailException {
        FileService fileService = new FileService();
        return fileService.upload(path, file);
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        MemberPayment memberPayment = memberPaymentRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        Member member = memberRepository.findById(memberPayment.getMemberId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        Invoice invoice = invoiceRepository.findById(memberPayment.getInvoiceId()).orElseThrow(() -> new RuntimeException("Not found"));

        Card card  = cardRepository.findById(memberPayment.getCardId()).orElseThrow(() -> new RuntimeException("Not found"));

        List<PaymentTransaction> paymentTransactions = paymentTransactionRepository.findByInvoiceId(memberPayment.getInvoiceId());

        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .memberPayment(memberPayment)
            .member(member)
            .invoice(invoice)
            .card(card)
            .paymentTransactions(paymentTransactions)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }
}
