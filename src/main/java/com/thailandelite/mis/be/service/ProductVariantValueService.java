package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.ProductVariantValueRepository;
import com.thailandelite.mis.model.domain.ProductVariantValue;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductVariantValueService {
    private final ProductVariantValueRepository productVariantValueRepository;
    private final ProductModelService productModelService;

    public void deleteVendorVariantValue(String id , String productVariantValueId) {
        productModelService.removeVendorProductModel(id, productVariantValueId);
        productVariantValueRepository.deleteById(productVariantValueId);
    }

    public ProductVariantValue insert(ProductVariantValue productVariantGroup) {
        ProductVariantValue vv = productVariantValueRepository.insert(productVariantGroup);
        productModelService.updateVendorProductModel(vv.getVendorId());
        return vv;
    }

    public ProductVariantValue save(ProductVariantValue productVariantGroup) {
        ProductVariantValue vv =  productVariantValueRepository.save(productVariantGroup);
        productModelService.updateVendorProductModel(vv.getVendorId());
        return vv;
    }
}
