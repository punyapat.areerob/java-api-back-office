package com.thailandelite.mis.be.service.dto.reports;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Data
public class BaseReportRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private ZonedDateTime from;
    private ZonedDateTime to;
    private TimePeriod timePeriod;
    public BaseReportRequest(){}
    public BaseReportRequest(ZonedDateTime from, ZonedDateTime to, TimePeriod timePeriod) {
        this.from = from;
        this.to = to;
        this.timePeriod = timePeriod;
    }
}
