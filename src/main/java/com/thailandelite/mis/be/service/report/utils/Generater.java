package com.thailandelite.mis.be.service.report.utils;

import com.thailandelite.mis.be.service.dto.reports.BaseReportRequest;
import com.thailandelite.mis.be.service.dto.reports.TimePeriod;

public class Generater {
    public static String getGroupByFormat(BaseReportRequest arg) {
        return Generater.getGroupByFormat(arg.getTimePeriod());
    }
    public static String getGroupByFormat(TimePeriod timePeriod) {
        String groupByValue = "yyyy-MM-dd";
        if(TimePeriod.MONTH.equals(timePeriod)){
            groupByValue = "yyyy-MM";
        }else if(TimePeriod.QUARTER.equals(timePeriod)){
            groupByValue = "yyyy-q";
        }else if(TimePeriod.YEAR.equals(timePeriod)){
            groupByValue = "yyyy";
        }
        return groupByValue;
    }
}
