package com.thailandelite.mis.be.service;
import com.thailandelite.mis.model.domain.*;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.agent.Agent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class IncomeReportService {
    private final IncomeReportRepository incomeReportRepository;
    private final MembershipRepository membershipRepository;
    private final MemberRepository memberRepository;
    private final AgentRepository agentRepository;
    private final CardRepository cardRepository;

    public List<IncomeReport> getAnnualFeeReport() {
        List<IncomeReport> annualFeeReports = new ArrayList<>();
        ZonedDateTime oneMonthToAnnualDate = LocalDate.now().plusMonths(1).atStartOfDay(ZoneId.systemDefault());
        membershipRepository.findByAnnualFeetBetween(oneMonthToAnnualDate, oneMonthToAnnualDate.plusDays(1)).stream().map(m -> {
            Member member = memberRepository.findById(m.getMemberId()).orElseThrow(() -> new NotFoundException("member"));
            Agent agent = agentRepository.findById(member.getRegisterAgentId()).orElseThrow(() -> new NotFoundException("agent"));
            Card card = cardRepository.findById(m.getCardId()).orElseThrow(() -> new NotFoundException("card"));

            IncomeReport annualFee = new IncomeReport();
            annualFee.setMemberId(m.getMemberId());
            annualFee.setMembershipId(m.getMembershipIdNo());
            annualFee.setMemberName(member.getGivenName());
            annualFee.setMemberSurname(member.getSurName());
            annualFee.setNationality(member.getNationality().getName());
            annualFee.setTypeOfAgent(agent.getAgentType().getNameEn());
            annualFee.setAgentName(agent.getAgentCode());
            annualFee.setMemberType(member.getMemberType().name());
            annualFee.setPaid(card.getAnnualFeeSetting().getTotal().longValue());
            annualFee.setRemain(card.getAnnualFeeSetting().getTotal().longValue() * m.getAnnualFee().getRemain());
            annualFee.setReportType(IncomeReport.IncomeReportType.ANNUAL_FEE);

            annualFeeReports.add(annualFee);
            return 0;
        }).collect(Collectors.toList());
        return annualFeeReports;
    }

}
