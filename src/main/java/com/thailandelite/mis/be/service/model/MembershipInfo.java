package com.thailandelite.mis.be.service.model;

import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.model.domain.MemberVisa;
import com.thailandelite.mis.model.domain.Membership;
import lombok.Data;

import java.util.List;

@Data
public class MembershipInfo {
    private Member member;
    private Membership membership;
    private List<MemberPrivilegeQuota> privileges;
    private MemberVisa visa;
}
