package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.domain.SubProductCategory;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.ProductModel;
import com.thailandelite.mis.model.domain.ProductVariantGroup;
import com.thailandelite.mis.model.domain.ProductVariantValue;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductModelService {
    private final ProductRepository productRepository;
    private final ProductModelRepository productModelRepository;
    private final ProductVariantGroupRepository productVariantGroupRepository;
    private final ProductVariantValueRepository productVariantValueRepository;
    private final SubProductCategoryRepository subProductCategoryRepository;

    public List<ProductModel> updateVendorProductModel(String vendorId){
        List<ProductModel> updatedModel = new ArrayList<>();

        HashMap<Integer,List<String>> groupingVariantId = new HashMap<>();
        HashMap<Integer,List<String>> groupingVariantName = new HashMap<>();
        setUpVariantNameAndId(vendorId, groupingVariantId, groupingVariantName);

        //TODO Not support delete variant case.
        for (Integer orderIndex : groupingVariantName.keySet()) {
            List<String> modelNameList = groupingVariantName.get(orderIndex);
            List<String> variantValueList = groupingVariantId.get(orderIndex);
            String modelName = String.join(", ", modelNameList);
            ProductModel pm = productModelRepository.findByVendorIdAndName(vendorId, modelName);
            if (pm == null) {
                pm = new ProductModel();
                pm.setVendorId(vendorId);
                pm.setName(modelName);
                pm.setProductVariantName(modelNameList);
                pm.setProductVariantValue(variantValueList);
                pm.setCreatedBy("SYSTEM");
                pm.setCreatedDate(Instant.now());
                pm.setLastModifiedBy("SYSTEM");
                pm.setLastModifiedDate(Instant.now());
                pm.setCreatedUserType("SYSTEM");
                pm.setLastUserType("SYSTEM");
                pm.setRecordStatus("N");
                productModelRepository.save(pm);
            }
        }
        updatedModel = productModelRepository.saveAll(updatedModel);
        return updatedModel;
    }

    private void  setUpVariantNameAndId(String vendorId, HashMap<Integer, List<String>> groupingVariantId, HashMap<Integer, List<String>> groupingVariantName) {
        List<ProductVariantGroup> pvg = productVariantGroupRepository.findAllByVendorId(vendorId);
        for (ProductVariantGroup productVariantGroup : pvg) {
            List<ProductVariantValue> pv = productVariantValueRepository.findByProductVariantGroupId(productVariantGroup.getId());
            for (int i=0; i<pv.size(); i++){
                List<String> valueList = groupingVariantId.get(i);
                if (valueList == null)
                    valueList = new ArrayList<>();
                valueList.add(pv.get(i).getId());
                groupingVariantId.put(i, valueList);

                List<String> valueNameList = groupingVariantName.get(i);
                if (valueNameList == null)
                    valueNameList = new ArrayList<>();
                valueNameList.add(pv.get(i).getName());
                groupingVariantName.put(i, valueNameList);
            }
        }
    }

    public void removeVendorProductModel(String vendorId, String productVariantValueId){
        List<ProductModel> productModel = productModelRepository.findByVendorIdAndProductVariantValue(vendorId, productVariantValueId);
        for (ProductModel model : productModel) {
            List<String> variantValue = model.getProductVariantValue();
            int indexRemove = variantValue.indexOf(productVariantValueId);
            variantValue.remove(indexRemove);
            model.setProductVariantValue(variantValue);
            List<String> variantName = model.getProductVariantName();
            variantName.remove(indexRemove);
            model.setProductVariantName(variantName);
        }
        productModelRepository.saveAll(productModel);
    }
    public void addVendorIdToProductModel(){
        List<ProductModel> productModels = productModelRepository.findAll();
        List<ProductModel> noVariantValue = new ArrayList<>();
        for (ProductModel productModel : productModels) {
            List<String> pvvs = productModel.getProductVariantValue();
            if(pvvs != null && pvvs.size() > 0 ){
                Optional<ProductVariantValue> v = productVariantValueRepository.findById(pvvs.get(0));
                if(v.isPresent()){
                    productModel.setVendorId(v.get().getVendorId());
                    productModelRepository.save(productModel);
                }else{
                    noVariantValue.add(productModel);
                }
            }else{
                noVariantValue.add(productModel);
            }
        }
        for (ProductModel productModel : noVariantValue) {
            log.debug("No variant {}", productModel.getId());
            productModelRepository.deleteById(productModel.getId());
        }
    }
}
