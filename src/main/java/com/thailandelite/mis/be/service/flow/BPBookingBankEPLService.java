package com.thailandelite.mis.be.service.flow;

import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.VendorService;
import com.thailandelite.mis.model.domain.booking.BookingFrom;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.enumeration.BPBookingStatus;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBookingBankEPLService extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private final BPIncidentService bpIncidentService;
    private final VendorService vendorService;
    public BPBookingBankEPLService(CamundaService camundaService,
                                    BookingRepository bookingRepository,
                                    BookingService bookingService,
                                   VendorService vendorService,
                                    CaseActivityRepository caseActivityRepository,
                                    BPIncidentService bpIncidentService
                                   ) {
        super(camundaService,
            new RSVNCheckDocument(bookingRepository, caseActivityRepository,vendorService),
            new EPLConfirmBooking(bookingRepository, caseActivityRepository),
            new GRSendDocEPL(bookingRepository, caseActivityRepository),
            new FINIssueCashReceipt(bookingRepository, caseActivityRepository),
            new EPLBookingConfirmed(bookingRepository, caseActivityRepository),
            new EPLPendingResolve(bookingRepository, caseActivityRepository,bpIncidentService),
            new UserRecheckBooking(bookingRepository, caseActivityRepository),
            new CreateIncidentCase(bookingRepository, caseActivityRepository),
            new GRSendDocUser(bookingRepository, caseActivityRepository));
        this.bookingRepository = bookingRepository;
        this.bookingService = bookingService;
        this.bpIncidentService = bpIncidentService;
        this.vendorService = vendorService;
    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) throws UploadFailException, DocumentException, IOException {
        BookingFrom bookingFrom = (BookingFrom) object;
        Booking result = bookingService.createBooking(bookingFrom);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        result.setCaseId(this.caseInfoService.createBooking(processDefKey, RSVNCheckDocument.taskDefKey , process,result).getId());
        result.setBookingStatus(BookingStatus.CREATE);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        Booking booking = bookingService.getBookingDetailById(caseInfoDTO.getEntityId());


        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .booking(booking)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }

    @RequiredArgsConstructor
    private static class RSVNCheckDocument implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final VendorService vendorService;
        public static final String taskDefKey = "RSVN_Check_Document";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {EPL,USER}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case EPL:
                    caseInfo.setStatus(BPBookingStatus.PENDING_EPL.toString());
                    vendorService.VendorConfirmNoti(caseInfo,"GOLF");
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USER:
                    caseInfo.setStatus(BPBookingStatus.PENDING_GR.toString());
                    caseInfo.setTaskDefKey("GR_Send_Doc_User");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPLConfirmBooking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPL_Confirm_Booking";
        }
        public enum Action {APPROVE,RESUBMITRSVN,RESUBMIT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus(BPBookingStatus.PENDING_GR.toString());
                    caseInfo.setTaskDefKey("GR_Send_Doc_EPL");
                    break;
                case RESUBMIT:
                    caseInfo.setStatus(BPBookingStatus.PENDING_USER.toString());
                    caseInfo.setTaskDefKey("User_Recheck_Booking");
                    break;
                case RESUBMITRSVN:
                    caseInfo.setStatus(BPBookingStatus.PENDING_RSVN.toString());
                    caseInfo.setTaskDefKey("RSVN_Check_Document");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GRSendDocEPL implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_EPL";
        }
        public enum Action {APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus(BPBookingStatus.PENDING_FIN.toString());
                    caseInfo.setTaskDefKey("FIN_Issue_Cash_Receipt");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class FINIssueCashReceipt implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "FIN_Issue_Cash_Receipt";
        }
        public enum Action {APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus(BPBookingStatus.PENDING_EPL.toString());
                    caseInfo.setTaskDefKey("EPL_Booking_Confirmed");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPLBookingConfirmed implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "EPL_Booking_Confirmed";
        }
        public enum Action {USERCHANGE,USERCANCEL,NEXT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus(BPBookingStatus.PENDING_EPL.toString());
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USERCANCEL:
                    caseInfo.setStatus(BPBookingStatus.CANCEL.toString());
                    break;
                case NEXT:
                    caseInfo.setStatus(BPBookingStatus.PENDING_EPL.toString());
                    caseInfo.setTaskDefKey("EPL_Pending_Resolve");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class EPLPendingResolve implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;
        final BPIncidentService bpIncidentService;

        public String name() {
            return "EPL_Pending_Resolve";
        }
        public enum Action {INCIDENT,DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case INCIDENT:
                    caseInfo.setStatus(BPBookingStatus.INCIDENT.toString());
                    bpIncidentService.create("incident",caseInfo.getEntityId());

                    caseInfo.setTaskDefKey("Create_Incident_Case");
                    break;
                case DONE:
                    caseInfo.setStatus(BPBookingStatus.PENDING_GR.toString());
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class UserRecheckBooking implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "User_Recheck_Booking";
        }
        public enum Action {USERCHANGE,USERCONFIRM}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case USERCHANGE:
                    caseInfo.setStatus(BPBookingStatus.PENDING_USER.toString());
                    caseInfo.setTaskDefKey("EPL_Confirm_Booking");
                    break;
                case USERCONFIRM:
                    caseInfo.setStatus(BPBookingStatus.PENDING_GR.toString());
                    caseInfo.setTaskDefKey("GR_Send_Doc_EPL");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class CreateIncidentCase implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "Create_Incident_Case";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus(BPBookingStatus.DONE.toString());
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class GRSendDocUser implements ITask {
        final BookingRepository bookingRepository;
        final CaseActivityRepository caseActivityRepository;

        public String name() {
            return "GR_Send_Doc_User";
        }
        public enum Action {DONE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus(BPBookingStatus.DONE.toString());
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


}
