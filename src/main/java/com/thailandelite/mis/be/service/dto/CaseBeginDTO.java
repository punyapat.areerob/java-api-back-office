package com.thailandelite.mis.be.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
public class CaseBeginDTO extends CaseMemberBeginDTO {
    @NotNull
    private String caseId;
    @NotNull
    private String memberId;
}
