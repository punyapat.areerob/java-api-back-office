package com.thailandelite.mis.be.service;
import com.bitofcode.oss.sdk.com.aviationedge.dtos.AirportDto;
import com.bitofcode.oss.sdk.com.aviationedge.resources.*;
import com.thailandelite.mis.be.repository.AirportRepository;
import com.thailandelite.mis.model.domain.Airport;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AirportService {
    private ApiResourceFactory apiResourceFactory;
    private ApiConfigurationRepository uriRepository;

    private final AirportRepository airportRepository;

    @PostConstruct
    void setUp() {
        String apiKey = "377b5e-f087a2";
        uriRepository = new ApiConfigurationRepository(apiKey);
        apiResourceFactory = new ApiResourceFactoryImpl(uriRepository);
    }
//https://aviation-edge.com/v2/public/airportDatabase?key=377b5e-f087a2&codeIso2Country=TH
    public List<Airport> canCreateAirportResource() {

        ApiResource<AirportDto> apiResource = apiResourceFactory.createAirportResource();
        ResourceRequestWithQueryParameter request = new ResourceRequestWithQueryParameter();
        request.add(new KeyValuePair("codeIso2Country", "TH"));

        List<AirportDto> list = apiResource.retrieve(request);
        List<Airport> list1 = list.stream().map(v -> {
            Airport target = new Airport();
            BeanUtils.copyProperties(v, target);
            return target;
//            return modelMapper.map(v, Airport.class);
        })
            .collect(Collectors.toList());

        log.debug("{}", list1);
        airportRepository.deleteAll();
        return airportRepository.saveAll(list1);
    }
//    public List<Airport> GetAllAirport(){
//
//    }

//    public static void main(String[] args) {
//        AirportService apAirportService = new AirportService();
//         apAirportService.setUp();
//        apAirportService.canCreateAirportResource();
//    }
}
