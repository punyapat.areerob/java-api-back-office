package com.thailandelite.mis.be.service.migrate;

import com.thailandelite.mis.be.domain.CommissionIssue;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.migrate.domain.SmmMemberPrivilegeQuota;
import com.thailandelite.mis.migrate.repository.SmmMemberPrivilegeQuotaRepository;
import com.thailandelite.mis.model.domain.BusinessIndustry;
import com.thailandelite.mis.model.domain.Nationality;
import com.thailandelite.mis.model.domain.Occupation;
import com.thailandelite.mis.model.domain.master.BloodType;
import com.thailandelite.mis.model.domain.master.City;
import com.thailandelite.mis.model.domain.master.Country;
import com.thailandelite.mis.model.domain.master.Religion;
import com.thailandelite.mis.model.domain.master.State;
import com.thailandelite.mis.model.domain.master.Title;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Profile("migrate")
@Slf4j
@Service
@RequiredArgsConstructor
public class MigrateCacheService {
    public static final String NATURE_OF_BUSINESS = "NatureOfBusiness";
    public static final String OCCUPATION = "Occupation";
    public static final String RELIGION = "Religion";
    public static final String BLOOD_TYPE = "BloodType";
    public static final String NATIONALITY = "Nationality";
    public static final String TITLE = "Title";
    public static final String COUNTRY = "Country";
    public static final String STATE = "State";
    public static final String CITY = "City";
    public static final String COMMISSIONISSUE = "CommissionIssue";

    public static final String MEMBER_PRIVILEGE_QUOTA = "MemberPrivilegeQuota";

    private final BusinessIndustryRepository businessIndustryRepository;
    private final OccupationRepository occupationRepository;
    private final ReligionRepository religionRepository;
    private final BloodTypeRepository bloodTypeRepository;
    private final NationalitiesRepository nationalitiesRepository;
    private final TitleRepository titleRepository;
    private final CountryRepository countryRepository;
    private final StateRepository stateRepository;
    private final CityRepository cityRepository;
    private final CommissionIssueRepository commissionIssueRepository;

    private final SmmMemberPrivilegeQuotaRepository smmMemberPrivilegeQuotaRepository;

    @Cacheable(cacheNames = NATURE_OF_BUSINESS)
    public BusinessIndustry getNatureOfBusiness(Integer natureOfBusinessId) {
        return natureOfBusinessId != null ? businessIndustryRepository.findById(natureOfBusinessId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = OCCUPATION)
    public Occupation getOccupation(Integer occupationId) {
        return occupationId != null ? occupationRepository.findById(occupationId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = RELIGION)
    public Religion getReligion(Integer religionId) {
        return religionId != null ? religionRepository.findById(religionId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = BLOOD_TYPE)
    public BloodType getBloodType(String bloodType) {
        return bloodType != null ? bloodTypeRepository.findById(bloodType.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = NATIONALITY)
    public Nationality getNationality(Integer nationalityId) {
        return nationalityId != null ? nationalitiesRepository.findById(nationalityId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = TITLE)
    public Title getTitle(Integer titleId) {
        return titleId != null ? titleRepository.findById(titleId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = COUNTRY)
    public Country getCountry(Integer countryId) {
        return countryId != null ? countryRepository.findById(countryId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = MEMBER_PRIVILEGE_QUOTA)
    public List<SmmMemberPrivilegeQuota> getMemberPrivilegeQuota(Integer memberGroupId){
        return smmMemberPrivilegeQuotaRepository.findAllByMemberGroupId(memberGroupId);
    }

    @Cacheable(cacheNames = STATE)
    public State getState(Integer stateId) {
        return stateId != null ? stateRepository.findById(stateId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = CITY)
    public City getCity(Integer cityId) {
        return cityId != null ? cityRepository.findById(cityId.toString()).orElse(null) : null;
    }

    @Cacheable(cacheNames = COMMISSIONISSUE)
    public CommissionIssue getCommissionIssue(Integer commissionIssueId) {
        return commissionIssueId != null ? commissionIssueRepository.findById(commissionIssueId.toString()).orElse(null) : null;
    }
}
