package com.thailandelite.mis.be.service.migrate;

import com.thailandelite.mis.model.domain.CardPrivilege.QuotaCycle;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.model.domain.Card.AgeConditionType;
import com.thailandelite.mis.model.domain.Card.AdditionalFamilyMemberType;

import com.thailandelite.mis.model.domain.Card.TransferType;
import com.thailandelite.mis.model.domain.Card.VatType;
import com.google.common.collect.Sets;

import com.google.common.collect.Lists;
import com.thailandelite.mis.model.domain.Privilege;
import com.thailandelite.mis.be.domain.SubProductCategory;
import com.thailandelite.mis.migrate.PrivilegeDB;
import com.thailandelite.mis.migrate.domain.*;
import com.thailandelite.mis.migrate.repository.*;
import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.Card.*;
import com.thailandelite.mis.model.domain.CardPrivilege;
import com.thailandelite.mis.model.domain.card.PhyCard;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.thailandelite.mis.be.service.migrate.MigrateUtils.zdt;

@Profile("migrate")
@Slf4j
@Service
@RequiredArgsConstructor
public class MigrateCardService2 {
    public final CardRepository cardRepository;
    public final CardPrivilegeRepository cardPrivilegeRepository;
    public final PrivilegeRepository privilegeRepository;

    private final MasPrivilegeRepository masPrivilegeRepository;
    private final MasPrivilegeGroupRepository masPrivilegeGroupRepository;
    private final MasPrivilegeGroupItemRepository masPrivilegeGroupItemRepository;
    private final MasCardRepository masCardRepository;
    private final MasPackageRepository masPackageRepository;
    private final MasMembershipVabilityRepository masMembershipVabilityRepository;
    private final MasMembershipFeeRepository masMembershipFeeRepository;
    private final MasTransferFeeRepository masTransferFeeRepository;
    private final MasAdditionalFamilyMemberRepository additionalFamilyMemberRepository;
    private final MasAnnualFeeRepository annualFeeRepository;
    private final MasUpgradeRepository masUpgradeRepository;
    private final MasPackageItemRepository masPackageItemRepository;
    private final MasPackageTimesRepository masPackageTimesRepository;

    private final SubProductCategoryRepository subProductCategoryRepository;
    private final PhyCardRepository phyCardRepository;

    public long privilegeGroup() {
        List<MasPrivilegeGroup> srcList = masPrivilegeGroupRepository.findAll();
        List<SubProductCategory> list = srcList.stream().map(this::map).collect(Collectors.toList());

        subProductCategoryRepository.deleteAll();
        return subProductCategoryRepository.saveAll(list).size();
    }

    public long privilege() {
        List<MasPrivilege> srcList = masPrivilegeRepository.findAll();
        List<Privilege> list = srcList.stream()
            .filter( v -> v.getRecordStatus().equals("N"))
            .map(src -> {
            Privilege dest = new Privilege();
            dest.setId(src.getPrivilegeId().toString());
            dest.setNameEN(src.getPrivilegeNameEn());
            dest.setNameTH(src.getPrivilegeNameTh());
            dest.setPrivilegeCode(src.getPrivilegeCode());
            dest.setRemark(src.getREMARK());
            dest.setActive(src.getRecordStatus().equals("N"));
            return dest;
        }).collect(Collectors.toList());

        privilegeRepository.deleteAll();
        return privilegeRepository.saveAll(list).size();
    }

    public long masCard() {
        ModelMapper modelMapper = new ModelMapper();
        List<MasCard> srcList = masCardRepository.findAll();
        List<PhyCard> list = srcList.stream().map(src -> {
            return modelMapper.map(src, PhyCard.class);
        }).collect(Collectors.toList());

        return phyCardRepository.saveAll(list).stream().count();
    }

    public int card() {
        List<MasPackage> srclist = masPackageRepository.findAll();
        List<Card> list = srclist.stream().filter(src -> src.getRecordStatus().equals("N"))
            .map(src -> {
                Card dest = new Card();
                dest.setId(src.getPackageId().toString());
                dest.setName(src.getPackageNameEn());
                dest.setAbbreviation(src.getIdPackage());
                dest.setPublicStatus(src.getPubliceStatus().equals("T") ? PublicStatus.ACTIVE : PublicStatus.INACTIVE);
                dest.setStatus(src.getPubliceStatus().equals("T") ? CardStatus.ACTIVE : CardStatus.INACTIVE);
//            dest.setPhoto(src.getCardPicture());
                dest.setMemberContactCenter(src.getMemberContactCenter().equals("Y"));
                dest.setGovernmentConcierges(src.getGovernmentConcierges().equals("Y"));
                dest.setNinetyReportDay(src.getNinetyDayReport().equals("Y"));
                dest.setActiveStartDate(zdt(src.getStartDate()));
                dest.setActiveEndDate(zdt(src.getEndDate()));
//        dest.setMemberInformationAgreement("");
//        dest.setMemberInformationAgreementFile("");
                dest.setRemark(src.getREMARK());
                dest.setRecordStatus(src.getRecordStatus());
                dest.setCreatedBy(src.getCreateUser());
                dest.setCreatedDate(zdt(src.getCreateDate()));
                dest.setUpdatedBy(src.getLastUser());
                dest.setUpdatedDate(zdt(src.getLastDate()));

                PhyCard phyCard = new PhyCard();
                phyCard.setId(src.getCardId().toString());

                dest.setPhyCard(phyCard);

                Integer membershipId = src.getMembershipId();
                MembershipType membershipType = null;
                switch (membershipId) {
                    case 1:
                        membershipType = MembershipType.REGULAR;
                        break;
                    case 2:
                        membershipType = MembershipType.WITH_PROPERTY;
                        break;
                    case 3:
                        membershipType = MembershipType.AT_LEAST_2P;
                        break;
                    case 7:
                        membershipType = MembershipType.WITH_CORE_MEMBER;
                        break;
                }

                dest.setMembershipType(membershipType);

                MasMembershipVability vability = masMembershipVabilityRepository.findById(src.getMembershipVabilityId()).get();
                dest.setValidityYear(vability.getMembershipVabilityQuantity());

                MasMembershipFee membershipFee = masMembershipFeeRepository.findById(src.getMembershipFeeId()).get();
                MembershipFeeSetting membershipFeeSetting = new MembershipFeeSetting();
                membershipFeeSetting.setAmount(membershipFee.getMembershipFeeAmount().floatValue());
                membershipFeeSetting.setVatType(membershipFee.getVatType().equals("I") ? VatType.INCLUDE : VatType.EXCLUDE);
                dest.setMembershipFee(membershipFeeSetting);

                TransferType transferType = TransferType.NONE;
                Integer membershipTransferId = src.getMembershipTransferId();

                TransferSetting transferSetting = new TransferSetting();
                if (membershipTransferId == 3) {
                    transferSetting.setType(TransferType.NONE);
                    transferSetting.setTimes(0);
                } else if (membershipTransferId == 4) {
                    transferSetting.setType(TransferType.YES);
                    transferSetting.setTimes(0);
                } else if (membershipTransferId == 6) {
                    transferSetting.setType(TransferType.FAMILY);
                    transferSetting.setTimes(1);
                } else if (membershipTransferId == 8) {
                    transferSetting.setType(TransferType.UNLIMIT);
                    transferSetting.setTimes(99);
                }

                MasTransferFee masTransferFee = masTransferFeeRepository.findById(src.getTransferFeeId()).get();
                transferSetting.setFeePercent(masTransferFee.getTransferFeePercent());
                dest.setTransferSetting(transferSetting);

                Integer additionalFamilyMemberId = src.getAdditionalFamilyMemberId();
                MasAdditionalFamilyMember additionalFamilyMember = additionalFamilyMemberRepository.findById(additionalFamilyMemberId).get();

                AdditionalFamilyMemberSetting additionalFamilyMemberSetting = new AdditionalFamilyMemberSetting();
                additionalFamilyMemberSetting.setType(AdditionalFamilyMemberType.PERSON_PER_UNI_2);
                additionalFamilyMemberSetting.setAmount(additionalFamilyMember.getAdditionalFamilyMemberAmount().floatValue());
                additionalFamilyMemberSetting.setVatType(VatType.INCLUDE);

                dest.setAdditionalFamilyMemberSetting(additionalFamilyMemberSetting);


                Integer ageId = src.getAgeId();
                AgeCondition ageCondition = new AgeCondition();
                if (ageId == 5) {
                    ageCondition.setType(AgeConditionType.MINIMUM);
                    ageCondition.setAge(20);
                } else {
                    ageCondition.setType(AgeConditionType.ANY);
                    ageCondition.setAge(1);
                }

                dest.setAgeCondition(ageCondition);

                Integer annualFeeId = src.getAnnualFeeId();
                MasAnnualFee masAnnualFee = annualFeeRepository.findById(annualFeeId).get();

                AnnualFeeSetting annualFeeSetting = new AnnualFeeSetting();
                try {
                    annualFeeSetting.setVatType(masAnnualFee.getVatType().equals("I") ? VatType.INCLUDE : VatType.EXCLUDE);
                    annualFeeSetting.setAmount(masAnnualFee.getAnnualFeeAmount().floatValue());
                } catch (Exception e) {
                    annualFeeSetting.setAmount(0f);
                }
                dest.setAnnualFeeSetting(annualFeeSetting);

                VisaSetting visaSetting = new VisaSetting();
                visaSetting.setYear(5);
                Integer visaId = src.getVisaId();
                visaSetting.setRenewAble(visaId == 2);

                dest.setVisaSetting(visaSetting);

                Integer toCardId = src.getUpgradeToPackageId();

                Integer upgradeId = src.getUpgradeId();
                MasUpgrade masUpgrade = masUpgradeRepository.findById(upgradeId).get();

                UpgradeSetting upgradeSetting = new UpgradeSetting();
                upgradeSetting.setToCardId(intToString(src.getUpgradeToPackageId()));
                upgradeSetting.setAmount(masUpgrade.getUpgradeAmount().floatValue());
                upgradeSetting.setVatType(masUpgrade.getVatType().equals("I") ? VatType.INCLUDE : VatType.EXCLUDE);
                upgradeSetting.setActive(masUpgrade.getUpgardeStatus().equals("T"));

                dest.setUpgradeSettings(Lists.newArrayList(upgradeSetting));
                return dest;
            }).collect(Collectors.toList());

        //cardRepository.deleteAll();
        return cardRepository.saveAll(list).size();
    }

    public int cardPrivilege() {
        Set<String> cardIs = cardRepository.findAll().stream()
            .map(Card::getId)
            .collect(Collectors.toSet());

        List<MasPackageItem> items = masPackageItemRepository.findAll();
        List<CardPrivilege> list = items.stream()
            .filter(cp -> cp.getRecordStatus().equals("N"))
            .filter(cp -> cp.getPackageId() != null)
            .filter(cp -> cardIs.contains(cp.getPackageId().toString()))
            .map(src -> {
                CardPrivilege dest = new CardPrivilege();
                dest.setId(String.valueOf(src.getPackageItemId()));
                dest.setCardId(String.valueOf(src.getPackageId()));

                Privilege privilege = new Privilege();
                privilege.setId(src.getPrivilegeId().toString());
                dest.setPrivilege(privilege);

                dest.setQuota(src.getPackageItemQuantity());

                MasPackageTimes masPackageTimes = masPackageTimesRepository.findById(src.getPackageTimesId()).get();
                Integer packageTimesId = masPackageTimes.getPackageTimesId();
                if (packageTimesId == 1) {
                    dest.setQuotaCycle(QuotaCycle.NONE);
                } else if (packageTimesId == 8) {
                    dest.setQuotaCycle(QuotaCycle.UNLIMIT);
                } else {
                    dest.setQuotaCycle(QuotaCycle.YEAR);
                }
                dest.setQuotaCycleDescription(masPackageTimes.getPackageTimesNameEn());

                dest.setPoint(src.getPackageItemPoint());
                dest.setOrder(src.getPackageItemOrder());
                dest.setQuotaHide("T".equals(src.getQuaotaHide()));
                dest.setShowQuotaStatus("T".equals(src.getShowQuotaStatus()));
                dest.setRemark(src.getREMARK());
                dest.setActive("N".equals(src.getRecordStatus()));

                return dest;
            }).collect(Collectors.toList());

        cardPrivilegeRepository.deleteAll();
        return cardPrivilegeRepository.saveAll(list).size();
    }

    private static String intToString(Integer v) {
        return v != null ? v.toString() : null;
    }

    public void privilege(List<PrivilegeDB> srclist) {
//        List<Privilege> list = srclist.stream().map(this::map).collect(Collectors.toList());
//        privilegeRepository.deleteAll();
//        privilegeRepository.saveAll(list);
    }

    private SubProductCategory map(MasPrivilegeGroup src) {
        SubProductCategory dest = new SubProductCategory();
        dest.setId(src.getPrivilegeGroupId().toString());
        dest.setProductCategoryId(null);
        dest.setName(src.getPrivilegeGroupNameEn());
        dest.setNameTH(src.getPrivilegeGroupNameTh());
        dest.setIcon(null);
        dest.setDescription(src.getREMARK());
        dest.setActive(false);
        dest.setProductLists(Sets.newHashSet());
        dest.setActive(src.getRecordStatus().equals("N"));

//        dest.setCreatedBy(src.getCreateUser());
//        dest.setCreatedDate(src.getCreateDate());
//        dest.setLastModifiedBy(src.getLastAccountUser());
//        dest.setLastModifiedDate(src.getLastDate());
//        dest.setRecordStatus(src.getRecordStatus());

        return dest;
    }

}
