package com.thailandelite.mis.be.service.helper;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

public class QueryHelper {

    public interface ConditionalFn<T>{
        Criteria where(T object);
    }

    private final List<Criteria> criteriaList = new ArrayList<>();
    private Pageable page;

    public QueryHelper() {
    }

    public QueryHelper(Pageable page) {
        this.page = page;
    }

    public <T> QueryHelper and(T value, ConditionalFn<T> conditionalFn) {
        if (nonNull(value)) {
            Criteria doti = conditionalFn.where(value);
            criteriaList.add(doti);
        }

        return this;
    }

    public QueryHelper and(Criteria criteria) {
        criteriaList.add(criteria);
        return this;
    }

    public Query build() {
        final Query query = page != null ? new Query().with(page) : new Query();
        if (!criteriaList.isEmpty()) {
            query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));
        }
        return query;
    }

    public static Criteria or(Criteria... criteria){
        return new Criteria().orOperator(criteria);
    }

    public static String containing(String what) {
        return MongoRegexCreator.INSTANCE.toRegularExpression(what, MongoRegexCreator.MatchMode.CONTAINING);
    }
}
