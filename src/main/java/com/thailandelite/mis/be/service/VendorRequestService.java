package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.VendorRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Vendor;
import com.thailandelite.mis.model.domain.VendorRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Slf4j
@Service
@RequiredArgsConstructor
public class VendorRequestService {
//    private final VendorRequestRepository vendorRequestRepository;
    private final VendorRepository vendorRepository;
    private final FontEndService fontEndService;
//
//    public VendorRequest VendorRequest(@RequestBody VendorRequest vendorRequest) {
//        return vendorRequestRepository.save(vendorRequest);
//    }
//
        public Vendor changePasswordRequest(String vendorId, String newPassword) {
        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow( () -> new NotFoundException("Not found vendor"));
        fontEndService.changePassword(vendor.getEmail(), newPassword);
        return vendor;
    }
////
//    public Vendor changeEmailRequest(String vendorId, String newEmail) {
//        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow( () -> new NotFoundException("Not found vendor"));
//        fontEndService.changeEmail(vendor.getEmail(), newEmail);
//        return vendor;
//    }

//    public Vendor changeMemberStatus(String vendorId, VendorStatus status, String remark) {
//        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow( () -> new NotFoundException("Not found vendor"));
//        vendor.setStatus(status);
//        vendorRepository.save(vendor);
//        return vendor;
//    }
//
//    public Vendor changeVendorContract(String vendorId, VendorContractRequest vendorContractRequest) {
//        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow( () -> new NotFoundException("Not found vendor"));
//        vendor.setContractStartDate(vendorContractRequest.getStartContractDate());
//        vendor.setContractEndDate(vendorContractRequest.getEndContractDate());
//        vendor.setContractRemark(vendorContractRequest.getRemark());
//        vendor = vendorRepository.save(vendor);
//        return vendor;
//    }
}
