package com.thailandelite.mis.be.service.dto;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.ZonedDateTime;

@Data
public class MemberShipInfoDTO {
    private String id;

    private String cardId;

    private String userId;

    private String status;

    private ZonedDateTime startDate;

    private ZonedDateTime endDate;

    private boolean isActive;

    private ZonedDateTime renewQuotaDate;

    private Integer transferCount;

    private String createdBy;

    private ZonedDateTime createdDate;

    private String updatedBy;

    private ZonedDateTime updatedDate;

}
