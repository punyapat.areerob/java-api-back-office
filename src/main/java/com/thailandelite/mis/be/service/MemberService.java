package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.service.mapper.MemberMapper;
import com.thailandelite.mis.be.web.rest.ApplicationResource;
import com.thailandelite.mis.model.domain.*;

import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.be.service.model.CardInfo;
import com.thailandelite.mis.be.service.model.MembershipInfo;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.jdbc.Work;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import static com.thailandelite.mis.be.common.Utils.getExample;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberService {
    private final CardService cardService;
    private final MemberRepository memberRepository;
    private final MembershipRepository membershipRepository;
    private final MemberPrivilegeService memberPrivilegeService;
    private final MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository;
    private final MemberVisaRepository memberVisaRepository;
    private final InvoiceRepository invoiceRepository;
    private final FileService fileService;
    private final MemberMapper memberMapper;
    private final CardRepository cardRepository;
    private final ModelMapper modelMapper = new ModelMapper();

    private final ProspectRepository prospectRepository;
    private final MembershipService membershipService;
    private final ApplicationService applicationService;

    public Membership uploadTemporaryMembershipCard(String membershipId, MultipartFile file) throws HandlerException, UploadFailException {
        Membership membership = membershipRepository.findById(membershipId).orElseThrow(() -> new HandlerException("MS.Membership", "find memberId(" + membershipId + ") not found"));
        membership.setTemporaryCardUrl(fileService.upload("member/" + membership.getMemberId() + "/temporaryMembershipCard/tempCard", file));
        membershipRepository.save(membership);
        return membership;
    }

    public Membership uploadShippingReceiptUrl(String membershipId, MultipartFile file) throws HandlerException, UploadFailException {
        Membership membership = membershipRepository.findById(membershipId).orElseThrow(() -> new HandlerException("MS.Membership", "find memberId(" + membershipId + ") not found"));
        membership.setShippingReceiptUrl(fileService.upload("member/" + membership.getMemberId() + "/uploadshippingReceiptUrl/receipt", file));
        membershipRepository.save(membership);
        return membership;
    }

    public Membership uploadApplicationReportUrl(String membershipId, MultipartFile file) throws HandlerException, UploadFailException {
        Membership membership = membershipRepository.findById(membershipId).orElseThrow(() -> new HandlerException("MS.Membership", "find memberId(" + membershipId + ") not found"));
        membership.setApplicationReportUrl(fileService.upload("member/" + membership.getMemberId() + "/applicationReportUrl/report", file));
        membershipRepository.save(membership);
        return membership;
    }

    public Membership updateEmailWelcomePackage(String membershipId, String welcomeMessage) throws HandlerException {
        Membership membership = membershipRepository.findById(membershipId).orElseThrow(() -> new HandlerException("MS.Membership", "find memberId(" + membershipId + ") not found"));
        membership.setEmailWelcomeMessage(welcomeMessage);
        membershipRepository.save(membership);
        return membership;
    }

    public Prospect createProspect(Prospect prospect) {
        String cardPackageId = prospect.getCardPackageId();
        Card card = cardRepository.findById(cardPackageId).orElseThrow(() -> new RuntimeException("Package not found"));
        prospect.setCard(card);
        prospect.setStatus("DONE");
        return prospectRepository.save(prospect);
    }

    public Member addApplication(String memberId, Application application) {
        Member member = memberRepository.findById(memberId).orElseThrow(() -> new RuntimeException("Member not found"));
        memberMapper.mergeApplicationToMember(application, member);
        memberRepository.save(member);
        return member;
    }

    public Membership newMembership(String memberId, String cardId, String invoiceId) {
        return membershipService.newMembership(memberId, cardId, invoiceId);
    }

    public Membership upgradeMembership(String memberId, String upgradeCardId) {
        return membershipService.upgradeMembership(memberId, upgradeCardId);
    }

    public Membership typeChangeMembership(String memberId, String typeChangeCardId) {
        return membershipService.typeChangeMembership(memberId, typeChangeCardId);
    }

    public Membership newMembershipByTransfer(String memberId, String transferMemberId) {
        return membershipService.newMembershipByTransfer(memberId, transferMemberId);
    }


    public Membership firstActivateMember(String memberId, String membershipId, ZonedDateTime activateDate) {
        Member member = memberRepository.findById(memberId)
            .orElseThrow(() -> new NotFoundException("Member", memberId));
        member.setStatus(MemberStatus.MEMBER);
        memberRepository.save(member);

        return this.membershipService.activateMember(membershipId, activateDate);
    }


    public MemberPrivilegeQuota commitPrivilegeQuota(String memberId, String privilegeId) throws HandlerException {
/*        Membership membership = membershipRepository.findOneByMemberId(memberId).orElseThrow(() -> new HandlerException("MS.Membership", "find memberId(" + memberId + ") not found"));
        if ("SUSPEND".equalsIgnoreCase(membership.getStatus())) {
            throw new HandlerException("MS.Membership.status", "Please renew annual fee to unblock service");
        }

        MemberPrivilege memberPrivilege = memberPrivilegeRepository.findOneByMemberIdAndPrivilegeId(memberId, privilegeId, ZonedDateTime.now());

        if (memberPrivilege == null) {
            throw new HandlerException("MS.MemberPrivilege", "Not found matched MemberPrivilegeId");
        } else {
            if (memberPrivilege.getRemainQuota() > 0) {
                memberPrivilege.decreaseQuota();
                memberPrivilegeRepository.save(memberPrivilege);
            } else {
                throw new HandlerException("MS.MemberPrivilege.quota", "Quota exhausted!");
            }Passport
        }*/
//        return memberPrivilege;
        return null;
    }

    public boolean isDueDateToAnnualFee(String memberId) throws HandlerException {
        return true;
    }

    public MembershipInfo getMembershipInfo(String memberId) throws HandlerException {
        Member member = memberRepository.findById(memberId).orElseThrow(() -> new HandlerException("MS.Member", "find memberId(" + memberId + ") not found"));
        Membership membership = membershipService.getActiveMembership(memberId);

        List<MemberPrivilegeQuota> memberPrivilegeQuotas = memberPrivilegeQuotaRepository.findAllByMemberId(memberId);
        MemberVisa memberVisa = memberVisaRepository.findOneByMemberId(memberId);

        MembershipInfo membershipInfo = new MembershipInfo();
        membershipInfo.setMember(member);
        membershipInfo.setMembership(membership);
        membershipInfo.setPrivileges(memberPrivilegeQuotas);
        membershipInfo.setVisa(memberVisa);
        return membershipInfo;
    }

    private void toUpdateMemberPrivilege(String memberId, ZonedDateTime zonedDateTime) {
        List<MemberPrivilegeQuota> memberPrivilegeQuotas = memberPrivilegeQuotaRepository.findAllByMemberIdAndBetweenDate(memberId, ZonedDateTime.now());
        List<MemberPrivilegeQuota> updatedList = memberPrivilegeQuotas.stream().map(memberPrivilege -> updateMemberPrivilege(memberPrivilege, zonedDateTime)).collect(toList());
        memberPrivilegeQuotaRepository.saveAll(updatedList);
    }

    private MemberPrivilegeQuota updateMemberPrivilege(MemberPrivilegeQuota memberPrivilegeQuota, ZonedDateTime zonedDateTime) {
        /*Duration privilegeValidity = Duration.between(memberPrivilege.getStart(), memberPrivilege.getEnd());
        memberPrivilege.setStart(zonedDateTime);
        memberPrivilege.setEnd(zonedDateTime.plus(privilegeValidity));*/
        return memberPrivilegeQuota;
    }

    private MemberPrivilegeQuota toMemberPrivilege(Member member, CardPrivilege cardPrivilege, ZonedDateTime zonedDateTime, int memberValidity) {
        MemberPrivilegeQuota memberPrivilegeQuota = new MemberPrivilegeQuota();
/*        if (cardPrivilege.getValidityPeriod() == CardPrivilegeValidity.YEARLY) {
            memberPrivilege.setMemberId(member.getId());
            memberPrivilege.setCardId(cardPrivilege.getCardId());
            memberPrivilege.setPrivilegeId(cardPrivilege.getPrivilegeId());
            memberPrivilege.setQuotas(cardPrivilege.getQuota());
            memberPrivilege.setRemainQuota(cardPrivilege.getQuota());
            memberPrivilege.setMemberOwners(Lists.newArrayList(member.getId()));
            memberPrivilege.setStart(zonedDateTime);
            memberPrivilege.setEnd(zonedDateTime.plusYears(1));
        } else if (cardPrivilege.getValidityPeriod() == CardPrivilegeValidity.LIFE_TIME) {
            memberPrivilege.setMemberId(member.getId());
            memberPrivilege.setCardId(cardPrivilege.getCardId());
            memberPrivilege.setPrivilegeId(cardPrivilege.getPrivilegeId());
            memberPrivilege.setQuotas(cardPrivilege.getQuota()); // How can I retrieve quota via membershipType??
            memberPrivilege.setRemainQuota(cardPrivilege.getQuota()); // ??
            memberPrivilege.setMemberOwners(Lists.newArrayList(member.getId()));
            memberPrivilege.setStart(zonedDateTime);
            memberPrivilege.setEnd(zonedDateTime.plusYears(memberValidity));
        }*/ //AUN
        return memberPrivilegeQuota;
    }

    public void updateMembershipInfo(MembershipInfo membershipInfo) {

        memberRepository.save(membershipInfo.getMember());
        membershipRepository.save(membershipInfo.getMembership());
        memberPrivilegeQuotaRepository.saveAll(membershipInfo.getPrivileges());
        memberVisaRepository.save(membershipInfo.getVisa());

    }

    private void isValidAgeCondition(Application application, CardInfo cardInfo) throws HandlerException {
/*        Date dateOfBirth = application.getUserProfileForm().getDateOfBirth();
        Date date = new Date();
        long age = date.getTime() - dateOfBirth.getTime();

        Integer ageMin = cardInfo.getCard().getAgeMin();
        Integer ageMax = cardInfo.getCard().getAgeMax();

        if (age > ageMin) {
            throw new HandlerException("MS.NEW", "Age is lower than the condition");
        } else if (ageMax < age) {
            throw new HandlerException("MS.NEW", "Age is higher than the condition");
        }*/
    }

    public boolean isDueDateToRenewQuota(String memberId) throws Exception {
        MembershipInfo membershipInfo = getMembershipInfo(memberId);
//        Membership membership = membershipRepository.findOneByMemberId(memberId).orElseThrow(() -> new HandlerException("MS.Membership", "find (" + memberId + ")  not found"));

        ZonedDateTime renewQuotaDate = membershipInfo.getMembership().getRenewQuotaDate();
        ZonedDateTime now = ZonedDateTime.now();
        if (renewQuotaDate == null) {
            return false;
        } else {
            return now.isAfter(renewQuotaDate);
        }
    }

   /* public void renewQuota(String memberId) throws Exception {
        MembershipInfo membershipInfo = getMembershipInfo(memberId);

        List<MemberPrivilegeQuota> memberPrivilegeQuotas = membershipInfo.getMembership().getCardPrivileges().stream().map(
            cardPrivilege -> toMemberPrivilege(
                membershipInfo.getMember(), cardPrivilege, membershipInfo.getMembership().getRenewQuotaDate(), 1)).collect(toList());
        membershipInfo.getPrivileges().addAll(memberPrivilegeQuotas);

        // check renewal quota is in duration of validity date
        if (membershipInfo.getMembership().getRenewQuotaDate().isBefore(membershipInfo.getMembership().getEndDate())) {
            membershipInfo.getMembership().setRenewQuotaDate(membershipInfo.getMembership().getRenewQuotaDate().plusYears(1));
        }

        updateMembershipInfo(membershipInfo);
    }*/

    public boolean deleteMembership(String memberId) {
        try {
            Membership p = new Membership();
            p.setMemberId(memberId);
            Example<Membership> match = Example.of(p);
            Membership membership = membershipRepository.findOne(match).orElseThrow(() -> new HandlerException("MS.Member", "find (" + memberId + ")  not found"));
            membershipRepository.deleteById(membership.getId());
            List<MemberPrivilegeQuota> memberPrivilegeQuotas = memberPrivilegeQuotaRepository.findAllByMemberId(memberId);
            memberPrivilegeQuotas.stream().forEach((mp) -> deleteMemberPrivilege(mp));
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<Member> fetchMemberByCardNo(String no) {
        List<Membership> memberships = membershipRepository.searchMembershipNo(no);
        List<String> memberIds = new ArrayList<>();
        for (Membership membership : memberships) {
            memberIds.add(membership.getMemberId());
        }
        return memberRepository.findByIdIn(memberIds);
    }

    private void deleteMemberPrivilege(MemberPrivilegeQuota mp) {
        memberPrivilegeQuotaRepository.deleteById(mp.getId());
    }

    public List<MemberPrivilegeQuota> getPrivilegeQuota(String memberId) {
        try {
            Membership membership = membershipService.getActiveMembership(memberId);
            return memberPrivilegeService.getPrivilegeQuota(membership.getId());
        }catch (Exception e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }


    public Workbook generateIncomeSheet()
    {
        List<Membership> memberships = membershipRepository.findAll().stream().filter(x -> x.getInvoiceId() != "" && x.getInvoiceId() != null).collect(toList());

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("income");
        Row header = sheet.createRow(0);
        generateHeader(workbook,header, new Date(),10);

        for (var i =0 ;i< memberships.size();i++ ) {
            generateMember(memberships.get(i),sheet,i);
        }
        return workbook;
    }

    public  void generateMember(Membership membership,Sheet sheet,int i )
    {
        Row row = sheet.createRow(i+1);
        Member member = memberRepository.findById(membership.getMemberId()).get();
        Invoice invoice = invoiceRepository.findById(membership.getInvoiceId()).get();
        Cell headerCell = row.createCell(0);
        headerCell.setCellValue(membership.getMembershipNo());

        headerCell = row.createCell(1);
        headerCell.setCellValue(membership.getMemberId());

        headerCell = row.createCell(2);
        headerCell.setCellValue(member.getTitle().getName());

        headerCell = row.createCell(3);
        headerCell.setCellValue(member.getGivenName());

        headerCell = row.createCell(4);
        headerCell.setCellValue(member.getSurName());

        headerCell = row.createCell(5);
        headerCell.setCellValue(member.getNationality().getName());

        headerCell = row.createCell(6);
        //headerCell.setCellValue(member.getAgentId());

        headerCell = row.createCell(7);
        //headerCell.setCellValue(member.getSurName());

        if (membership.getStartDate()!= null) {
            headerCell = row.createCell(8);
            headerCell.setCellValue(membership.getStartDate().toLocalDate().toString());
        }

        if(membership.getAnnualFee() != null && membership.getAnnualFee().getTotal()!=null) {
            headerCell = row.createCell(9);
            headerCell.setCellValue(membership.getAnnualFee().getTotal());
        }

        if(membership.getStatus() !=null) {
            headerCell = row.createCell(10);
            headerCell.setCellValue(membership.getStatus().name());
        }
        headerCell = row.createCell(11);
        //headerCell.setCellValue(membership.get.name());

        headerCell = row.createCell(12);
        //headerCell.setCellValue(membership.getStatus().name());

        if(invoice.getPayDate()!=null) {
            headerCell = row.createCell(13);
            headerCell.setCellValue(invoice.getPayDate().toLocalDate().toString());
        }
    }

    public void generateHeader(Workbook workbook, Row header , Date startDate, int months)
    {
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Member no");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Member Id");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("Member Prefix");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("Member Name");
        //headerCell.setCellStyle(headerStyle);


        headerCell = header.createCell(4);
        headerCell.setCellValue("Surname");
        //headerCell.setCellStyle(headerStyle);


        headerCell = header.createCell(5);
        headerCell.setCellValue("Original nationality");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(6);
        headerCell.setCellValue("Agent Type");
        //headerCell.setCellStyle(headerStyle);


        headerCell = header.createCell(7);
        headerCell.setCellValue("Agent name");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(8);
        headerCell.setCellValue("Activation date");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(9);
        headerCell.setCellValue("Fee");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(10);
        headerCell.setCellValue("Status");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(11);
        headerCell.setCellValue("Membership Type");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(12);
        headerCell.setCellValue("");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(13);
        headerCell.setCellValue("Pay date");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(14);
        headerCell.setCellValue("Year");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(15);
        headerCell.setCellValue("Perday");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(16);
        headerCell.setCellValue("Day");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(17);
        headerCell.setCellValue("Balance");
        //headerCell.setCellStyle(headerStyle);


        for(int i =0 ;i < months; i++)
        {
            headerCell = header.createCell(18+i);
            headerCell.setCellValue(startDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate().with(TemporalAdjusters.lastDayOfMonth()).toString());
            //headerCell.setCellStyle(headerStyle);

        }
    }

    public Member findById(String reqMemberId) {
        return memberRepository.findById(reqMemberId).orElseThrow(()-> new NotFoundException("Not found request member"));
    }

    public Member requestCreateChangePackageApplication(String memberId, String newPackageId, PackageAction packageAction){
        Member member = memberRepository.findById(memberId).orElseThrow(()-> new NotFoundException("Not found request member"));
        ApplicationResource.CreateApplicationReq req = new ApplicationResource.CreateApplicationReq(memberId, newPackageId, Boolean.TRUE, null);
        Application application = null;
        if ( PackageAction.UPGRADE.equals(packageAction) ) {
            application = applicationService.createUpgradeApplication(req);
        }else if( PackageAction.TYPECHANGE.equals(packageAction) ){
            application = applicationService.createTypeChangeApplication(req);
        }
        if(application != null) {
            application.setPackageAction(packageAction);
            applicationService.save(application);
        }
        return member;
    }

    public List<Member> getImmediateFamilyMember(String memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(()->new NotFoundException("Not found member"));
        //19,20,21,22
        List<String> cardIds = new ArrayList<>();
        cardIds.add("19");
        cardIds.add("20");
        cardIds.add("21");
        cardIds.add("22");
        List<Membership> membershipList = membershipService.findByCardIn(cardIds);
        List<Member> family = new ArrayList<>();
        for (Membership memberShip : membershipList) {
            Optional<Member> opt = memberRepository.findById(memberShip.getMemberId());
            if(opt.isPresent())
                family.add(opt.get());
        }
        return family;
    }
}
