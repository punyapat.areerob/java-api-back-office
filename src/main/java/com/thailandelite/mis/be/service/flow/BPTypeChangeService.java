package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.repository.MembershipRepository;
import com.thailandelite.mis.be.service.ApplicationService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.MemberService;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.model.domain.Member;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class BPTypeChangeService extends  BPService<Member> {
    private final MemberRepository memberRepository;
    private final ApplicationService applicationService;
    private final ApplicationRepository applicationRepository;

    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPTypeChangeService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , MemberRepository memberRepository
        , ApplicationRepository applicationRepository
        , ApplicationService applicationService
        , ApplicationContext applicationContext) {

        super(camundaService,
            new BPTypeChangeService.Pending_CRM_Check_Transfer_DOC(caseActivityRepository, memberRepository),
            new BPTypeChangeService.Pending_User_Resubmit(caseActivityRepository, memberRepository),
            new BPTypeChangeService.Pending_GR_create_E_Doc(caseActivityRepository, memberRepository),
            new BPTypeChangeService.GR_Process_to_IM(caseActivityRepository, memberRepository),
            new BPTypeChangeService.GR_Pending_IM(caseActivityRepository, memberRepository),
            new BPTypeChangeService.CRM_Approve_Close_Case(caseActivityRepository, memberRepository) ,
            new BPTypeChangeService.CRM_Reject_Close_Case(caseActivityRepository, memberRepository)
        );
        this.memberRepository = memberRepository;
        this.applicationRepository = applicationRepository;
        this.applicationService = applicationService;
    }

    @Override
    public Object create(String flowDefKey, Object object) {
        Application application = (Application) object;
        ProcessInstance process = super.startProcess(flowDefKey, application.getId());
        this.caseInfoService.createMemberTypeChangeCase(flowDefKey, BPTypeChangeService.Pending_CRM_Check_Transfer_DOC.taskDefKey, process, application);
        return  application;
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        Application application = applicationRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        //// TODO: 10/4/2021 AD call CardService
        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .application(application)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<Application> applications = applicationRepository.findByIdIn(entityIds);

        ImmutableMap<String, Application> idMap = Maps.uniqueIndex(applications, Application::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            Application application = idMap.get(caseInfo.getEntityId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .application(application)
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @RequiredArgsConstructor
    private static class Pending_CRM_Check_Transfer_DOC implements ITask {
        public static final String taskDefKey = "Pending_CRM_Check_Transfer_DOC";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,REJECT,SKIPTO}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.Pending_CRM_Check_Transfer_DOC.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.Pending_CRM_Check_Transfer_DOC.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E-Doc");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING USER RESUBMIT");
                    caseInfo.setTaskDefKey("Pending_User_Resubmit");
                    break;
                case SKIPTO:
                    caseInfo.setStatus("PENDING CRM APPROVE");
                    caseInfo.setTaskDefKey("CRM_Approve_Close_Case");
                    break;
            }


        }
    }

    @RequiredArgsConstructor
    private static class Pending_User_Resubmit implements ITask {
        public static final String taskDefKey = "Pending_User_Resubmit";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {RESUBMIT,CANCEL}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.Pending_User_Resubmit.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.Pending_User_Resubmit.Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("PENDING CRM CHECK DOC");
                    caseInfo.setTaskDefKey("Pending_CRM_Check_Transfer_DOC");
                    break;
                case CANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }


        }
    }

    @RequiredArgsConstructor
    private static class Pending_GR_create_E_Doc implements ITask {
        public static final String taskDefKey = "Pending_GR_create_E_Doc";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,REJECT,SKIPTO}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.Pending_GR_create_E_Doc.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.Pending_GR_create_E_Doc.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR TO IM");
                    caseInfo.setTaskDefKey("GR_Process_to_IM");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING USER RESUBMIT");
                    caseInfo.setTaskDefKey("Pending_User_Resubmit");
                    break;
                case SKIPTO:
                    caseInfo.setStatus("PENDING CRM APPROVE");
                    caseInfo.setTaskDefKey("CRM_Approve_Close_Case");
                    break;
            }

        }
    }

    @RequiredArgsConstructor
    private static class GR_Process_to_IM implements ITask {
        public static final String taskDefKey = "GR_Process_to_IM";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,MOVE}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.GR_Process_to_IM.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.GR_Process_to_IM.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING GR RESPONSE");
                    caseInfo.setTaskDefKey("GR_Pending_IM");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E-Doc");
                    break;
            }

        }
    }

    @RequiredArgsConstructor
    private static class GR_Pending_IM implements ITask {
        public static final String taskDefKey = "GR_Pending_IM";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {APPROVE,MOVE, REJECT}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.GR_Pending_IM.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.GR_Pending_IM.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING CRM APPROVE");
                    caseInfo.setTaskDefKey("CRM_Approve_Close_Case");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR TO IM");
                    caseInfo.setTaskDefKey("GR_Process_to_IM");
                    break;
                case REJECT:
                    caseInfo.setStatus("PENDING CRM REJECT");
                    caseInfo.setTaskDefKey("CRM_Reject_Close_Case");
                    break;
            }


        }
    }


    @RequiredArgsConstructor
    private static class CRM_Approve_Close_Case implements ITask {
        public static final String taskDefKey = "CRM_Approve_Close_Case";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {DONE,MOVE }

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.CRM_Approve_Close_Case.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.CRM_Approve_Close_Case.Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case MOVE:
                    caseInfo.setStatus("PENDING GR");
                    caseInfo.setTaskDefKey("Pending_GR_create_E-Doc");
                    break;
            }

        }
    }


    @RequiredArgsConstructor
    private static class CRM_Reject_Close_Case implements ITask {
        public static final String taskDefKey = "CRM_Reject_Close_Case";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;

        public enum Action {REJECT }

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPTypeChangeService.CRM_Reject_Close_Case.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPTypeChangeService.CRM_Reject_Close_Case.Action.valueOf(actionName)) {
                case REJECT:
                    caseInfo.setStatus("REJECT");
                    caseInfo.setTaskDefKey("");
                    break;
            }

        }
    }
}
