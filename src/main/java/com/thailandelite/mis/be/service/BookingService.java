package com.thailandelite.mis.be.service;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.service.model.CardInfo;
import com.thailandelite.mis.be.service.model.MembershipInfo;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.booking.*;
import com.thailandelite.mis.model.domain.booking.dao.*;
import com.thailandelite.mis.model.domain.booking.enums.BookingFlowDefinitionKey;
import com.thailandelite.mis.model.domain.booking.enums.BookingInfoType;
import com.thailandelite.mis.model.domain.booking.enums.BookingServiceType;
import com.thailandelite.mis.model.domain.booking.enums.MemberBookingInfoStatus;
import com.thailandelite.mis.model.domain.enumeration.BookingServiceMemberStatus;
import com.thailandelite.mis.model.domain.enumeration.BookingStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.hibernate.exception.DataException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.management.BadAttributeValueExpException;
import javax.ws.rs.BadRequestException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingService {
    private final BookingRepository bookingRepository;
    private final BookingServiceMemberRepository bookingServiceMemberRepository;
    private final MemberService memberService;
    private final ProductRepository productRepository;
    private final ProductService productService;
    private final ShopRepository shopRepository;
    private final VendorRepository vendorRepository;
    private final PrivilegeRepository privilegeRepository;
    private final ProductModelRepository productModelRepository;
    private final MemberPrivilegeService memberPrivilegeService;
    private final MembershipRepository membershipRepository;
    private final CardService cardService;
    private final CaseInfoRepository caseInfoRepository;

    public void saveBooking(BookingFrom bookingFrom) throws IOException, DocumentException {
        Booking booking = new Booking();
        booking.setRemark(bookingFrom.getBooking().getRemark());
        bookingRepository.save(booking);
    }
    public List<Booking> getBooking() {
        List<Booking> bookings = bookingRepository.findAll();
        bookings.stream().map(book->{
            List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findByBookingId(book.getId());
            book.setBookingServiceMembers(bookingServiceMembers);
            return book;
        }).collect(Collectors.toList());
        return bookings;
    }

    public List<Booking> getPendingBookingByMemberId(String memberId) {
        List<Booking> bookings = bookingRepository.findAllByRequestMemberIdAndBookingStatus(memberId, BookingStatus.WAITING_VERIFY);
        bookings.stream().map(book->{
            List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findByBookingId(book.getId());
            book.setBookingServiceMembers(bookingServiceMembers);
            return book;
        }).collect(Collectors.toList());
        return bookings;
    }

    public Booking getBookingById(String id) {
        Booking booking = bookingRepository.findById(id).orElseThrow(() -> new RuntimeException("Not found"));
        List<BookingServiceMember> members = bookingServiceMemberRepository.findByBookingId(booking.getId());
        booking.setBookingServiceMembers(members);
        return booking;
    }

    public Booking getBookingDetailById(String id) {
        return getBookingById(id);
    }

    public BookingResponse getWrapBookingDetailById(String id) {
        return wrapBooking(getBookingById(id));
    }

    public Booking createBooking(BookingFrom bookingFrom) {
        String reqMemberId = bookingFrom.getBooking().getRequestMemberId();
        Member reqMember = memberService.findById(reqMemberId);
        if(bookingFrom.getBooking() != null && StringUtils.hasText(bookingFrom.getBooking().getId()) ){
            return bookingRepository.findById(bookingFrom.getBooking().getId()).orElseThrow(()->new NotFoundException("Not found booking info"));
        }
//        isValidBooking(bookingFrom); //check Booking Valid data and time
        Booking newBooking =new Booking();
        newBooking.setNo(generateBookingNo(bookingFrom));

        newBooking.setRemark(bookingFrom.getBooking().getRemark());
        newBooking.setCreatedBy(bookingFrom.getCreateBy());
        newBooking.setCreatedDate(ZonedDateTime.now().toInstant());
        newBooking.setLastModifiedBy(bookingFrom.getCreateBy());
        newBooking.setLastModifiedDate(ZonedDateTime.now().toInstant());
        newBooking.setCreatedUserType("USER");
        newBooking.setLastUserType("USER");
        newBooking.setRecordStatus("N");
        newBooking.setBookingStatus(BookingStatus.CREATE);

        newBooking.setRequestMemberId(reqMemberId);
        newBooking.setRequestMember(reqMember);
        newBooking = bookingRepository.save(newBooking);
        BookingFlowDefinitionKey requestProductType = BookingFlowDefinitionKey.ALL;
        final String newBookingId = newBooking.getId();
        String bookingName = "-";
        if (bookingFrom.getBookingServiceMembers() != null && bookingFrom.getBookingServiceMembers().size() > 0) {
            String productId = bookingFrom.getBookingServiceMembers().get(0).getProductId();
            Optional<Product> product = productRepository.findById(productId);
            if (product.isPresent()) {
                bookingName = product.get().getNameEn();
                requestProductType = product.get().getFlowDefinitionKey();
            }

            bookingFrom.getBookingServiceMembers().stream().map(request -> {
                BookingServiceMember memberBooking = new BookingServiceMember();
                memberBooking.setMemberId(request.getMemberId());
                memberBooking.setStatus(BookingServiceMemberStatus.CREATE);
                memberBooking.setMemberName("-");
                memberBooking.setMemberEmail("-");
                memberBooking.setMemberCardId("-");

                try {
                    log.debug("Membership id, {}",request.getMemberId() );
                    MembershipInfo memberShip = memberService.getMembershipInfo(request.getMemberId());
                    log.debug("Membership: {}", memberShip);
                    Member mem = memberShip.getMember();
                    log.debug("Name: {}", mem.getGivenName() + " " + mem.getSurName());
                    String title = mem.getTitle() != null ? mem.getTitle().getName() + " " : "";
                    memberBooking.setMemberName(mem.getTitle().getName() + mem.getGivenName() + " " + mem.getSurName());
                    log.debug("Email: {}", mem.getEmail());
                    memberBooking.setMemberEmail(mem.getEmail());
                    log.debug("Card no.: {}", memberShip.getMembership().getMembershipIdNo());
                    memberBooking.setMemberCardId(memberShip.getMembership().getMembershipIdNo());
                } catch (HandlerException e) {
                    e.printStackTrace();
                }

                memberBooking.setBookingId(newBookingId);
                memberBooking.setProductId(request.getProductId());
                memberBooking.setModelId(request.getModelId());
                memberBooking.setVendorId(product.get().getVendorId());
                String modelName = "";
                Optional<ProductModel> pmm = productModelRepository.findById(request.getModelId());
                if(pmm.isPresent())
                    modelName = pmm.get().getName();
                memberBooking.setModelName(modelName);

                Optional<Product> productPerMem = productRepository.findById(productId);
                if(productPerMem.isPresent()){
                    Product pm = productPerMem.get();
                    memberBooking.setProductName(pm.getNameEn());
                    memberBooking.setFlowDefinitionKey(pm.getFlowDefinitionKey());
                    Optional<Shop> s = shopRepository.findById(pm.getShopId());
                    if(s.isPresent()){
                        memberBooking.setShopName(s.get().getNameEn());
                    }
                    Optional<Vendor> v = vendorRepository.findById(pm.getShopId());
                    if(v.isPresent()){
                        memberBooking.setVendorId(v.get().getId());
                        memberBooking.setVendorName(v.get().getNameEn());
                    }
                }
                //TODO Calculate price or quota.
                memberBooking.setPrice(request.getPrice());
                memberBooking.setQuota(request.getQuota());
                memberBooking.setRemark(request.getRemark());
                memberBooking.setStartTime(request.getStartTime());
                memberBooking.setEndTime(request.getEndTime());
                memberBooking.setImageUpload(request.getImageUpload());
                memberBooking.setBookingDetail(request.getBookingDetail());
                if(request.getBookingGuest() != null ) {
                    String modelNameGuest = modelName;
                    memberBooking.setBookingGuest(request.getBookingGuest().stream().map(bookingServiceMembersGuest -> {
                        BookingGuest bookingGuest = new BookingGuest();
                        bookingGuest.setName(bookingServiceMembersGuest.getName());
                        bookingGuest.setAmount(bookingServiceMembersGuest.getAmount());
                        bookingGuest.setProductModelId(request.getModelId());
                        bookingGuest.setProductModelName(modelNameGuest);
                        bookingGuest.setCreatedBy(bookingFrom.getCreateBy());
                        bookingGuest.setCreatedDate(ZonedDateTime.now().toInstant());
                        bookingGuest.setLastModifiedBy(bookingFrom.getCreateBy());
                        bookingGuest.setLastModifiedDate(ZonedDateTime.now().toInstant());
                        bookingGuest.setCreatedUserType("USER");
                        bookingGuest.setLastUserType("USER");
                        bookingGuest.setRecordStatus("N");
                        return bookingGuest;
                    }).collect(Collectors.toList()));
                }
                memberBooking.setCreatedBy(bookingFrom.getCreateBy());
                memberBooking.setCreatedDate(ZonedDateTime.now().toInstant());
                memberBooking.setLastModifiedBy(bookingFrom.getCreateBy());
                memberBooking.setLastModifiedDate(ZonedDateTime.now().toInstant());
                memberBooking.setCreatedUserType("USERS");
                memberBooking.setLastUserType("USERS");
                memberBooking.setRecordStatus("N");
                bookingServiceMemberRepository.save(memberBooking);
                return memberBooking;
            }).collect(Collectors.toList());
        }
        newBooking.setName(bookingName);
        newBooking.setRequestProductFlowDefinitionKey(requestProductType);
        newBooking = bookingRepository.save(newBooking);
        return  newBooking;

    }

    public void deductBooking(Booking booking) {
        bookingServiceMemberRepository.findByBookingId(booking.getId()).stream().map(b->{
            ProductModel productModel = productModelRepository.findById(b.getModelId()).orElseThrow(() -> new NotFoundException("productModel"));
            List<String> modelPrivilegeIds = productModel.getPrivilegesId();

            Membership membership = membershipRepository.findByMembershipIdNo(b.getMemberCardId()).orElseThrow(() -> new NotFoundException("membership"));
            Card card = cardService.getCard(membership.getCardId());
            CardInfo cardInfo = cardService.getCardInfo(card.getId());
            cardInfo.getCardPrivileges().stream().map(p -> {
                for (String privilegeId : modelPrivilegeIds) {
                    if(p.getPrivilege().getId().equals(privilegeId)) {
                        boolean deductQuota = memberPrivilegeService.deductQuota(membershipRepository.findByMemberId(b.getMemberId()).get(0).getId(),productModelRepository.findById(b.getModelId()).get().getPrivilegesId().get(0));
                        if(!deductQuota) {
                            b.setPrice(Float.valueOf(productModel.getNormalPrice()));
                            bookingServiceMemberRepository.save(b);
                        }
                    }else{
                        b.setPrice(Float.valueOf(productModel.getNormalPrice()));
                        bookingServiceMemberRepository.save(b);
                    }
                }
                return null;
            }).collect(Collectors.toList());
            return null;
        }).collect(Collectors.toList());
    }

    private void isValidBooking(BookingFrom bookingFrom) {
        List<BookingServiceMember> bookings = bookingFrom.getBookingServiceMembers();
        String memberId = bookingFrom.getBooking().getRequestMemberId();
//        AtomicBoolean yes = new AtomicBoolean(true);.takeWhile(value -> yes.get())
        bookings.stream().map(b->{
            ZonedDateTime endTime = b.getEndTime() == null ? b.getStartTime().plusMinutes(30) : b.getEndTime();
            bookingServiceMemberRepository.findByMemberId(memberId).stream().map(c->{
//            if(!((b.getStartTime().isBefore(c.getEndTime()) || b.getStartTime().equals(c.getEndTime())) &&
//                (b.getEndTime().isAfter(c.getStartTime()) || b.getEndTime().equals(c.getStartTime()))))
//                throw new EmptyStackException();
//                return 0;
                ZonedDateTime endTimeOld = c.getEndTime() == null ? c.getStartTime().plusMinutes(30) : c.getEndTime();
                if((b.getStartTime().isBefore(endTimeOld) && b.getStartTime().isAfter(c.getStartTime())) || b.getStartTime().equals(c.getStartTime()) ||
                    (endTime.isAfter(c.getStartTime()) && endTime.isBefore(endTimeOld)) || endTime.equals(endTimeOld))
                    throw new NotFoundException("Booking Overlap");
                    return 0;
            }).collect(Collectors.toList());
        return 0;
        }).collect(Collectors.toList());
        }

    private String generateBookingNo(BookingFrom bookingFrom) {
        //TODO Implement booking No.
        return "ABC001";
    }

    public BookingServiceMember changePriceAndQuota(String id, Float price, Integer quota) {
        BookingServiceMember bookingServiceMember = bookingServiceMemberRepository.findById(id).orElseThrow(() -> new NotFoundException("BookingServiceMember"));
        bookingServiceMember.setPrice(price);
        bookingServiceMember.setQuota(quota);
        bookingServiceMemberRepository.save(bookingServiceMember);
        return bookingServiceMember;
    }

    public List<Booking> fetchBookingByVendor(String id) {
        List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findByVendorId(id);
        List<Booking> bookings = findBookingsByMemberBooking(bookingServiceMembers);
        return bookings;
    }

    public List<Booking> fetchBookingByMember(String id) {
        List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findByMemberId(id);
        List<Booking> bookings = findBookingsByMemberBooking(bookingServiceMembers);
        return bookings;
    }

    public List<Booking> getBookingByMemberInCase(String memberId) {
        List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findByMemberId(memberId);
        List<Booking> bookings = findBookingsByMemberBooking(bookingServiceMembers);
        List<String> bookingId = new ArrayList<>();
        for (Booking booking : bookings) {
            bookingId.add(booking.getId());
        }
        List<CaseInfo> caseInfos = caseInfoRepository.findAllByEntityIdIn(bookingId);
        List<Booking> bookingCases = new ArrayList<>();
        for (CaseInfo caseInfo : caseInfos) {
            for (Booking booking : bookings) {
                if(caseInfo.getEntityId().equalsIgnoreCase(booking.getId())) {
                    bookingCases.add(booking);
                    break;
                }
            }
        }
        return bookingCases;
    }

    private List<Booking> findBookingsByMemberBooking(List<BookingServiceMember> bookingServiceMembers) {
        if(bookingServiceMembers == null || bookingServiceMembers.size() <= 0)
            return new ArrayList<>();
        List<String> bookingIds = bookingServiceMembers.stream().map((book)->book.getBookingId()).collect(Collectors.toList());
        List<Booking> bookings = bookingRepository.findByIdIn(bookingIds);
        if (bookings == null || bookings.size() <= 0 )
            return new ArrayList<>();
        for (Booking booking : bookings) {
            for (BookingServiceMember bsMember : bookingServiceMembers) {
                if(booking.getId().contains(bsMember.getBookingId())){
                    if( booking.getBookingServiceMembers() == null)
                        booking.setBookingServiceMembers(new ArrayList<>());

                    booking.getBookingServiceMembers().add(bsMember);
                }
            }
        }
        return bookings;
    }

    public List<BookingResponse> fetchWrapBookingByMember(String id) {
        List<Booking> bookings = fetchBookingByMember(id);
        List<BookingResponse> ret = new ArrayList<>();
        for (Booking booking : bookings) {
            ret.add(wrapBooking(booking));
        }
        return ret;
    }

    public List<BookingResponse> fetchWrapBookingByVendor(String id) {
        List<Booking> bookings = fetchBookingByVendor(id);
        List<BookingResponse> ret = new ArrayList<>();
        for (Booking booking : bookings) {
            ret.add(wrapBooking(booking));
        }
        return ret;
    }
    public Page<BookingResponse> fetchWrapBookingByVendor(String id, BookingStatus status, Pageable pageable) {
        List<Booking> bookings = fetchBookingByVendor(id);
        List<BookingResponse> ret = new ArrayList<>();
        for (Booking booking : bookings) {
            if(booking != null && status != null && status.equals(booking.getBookingStatus()))
                ret.add(wrapBooking(booking));
        }
        return PageableExecutionUtils.getPage(ret, pageable, (LongSupplier) ()-> ret.size());

    }

    public BookingResponse wrapBooking(Booking booking){
        BookingResponse ret = new BookingResponse();
        List<BookingServiceMember> members = booking.getBookingServiceMembers();
        if (BookingFlowDefinitionKey.TICKET.equals(booking.getRequestProductFlowDefinitionKey())){
            BookingResponse ticketBooking = new BookingResponse();
            ticketBooking.setBookingId(booking.getId());
            ticketBooking.setBookingNo(booking.getNo());
            ticketBooking.setName(booking.getName());
            ticketBooking.setStatus(booking.getBookingStatus());
            ticketBooking.setType(BookingInfoType.NORMAL);
            ticketBooking.setReserveDateTime(booking.getBookingDateTime());
            ticketBooking.setLocationName("");
            ticketBooking.setFlightNumber("");
            ticketBooking.setMeetingTime("");
            ticketBooking.setPersons(1);
            ticketBooking.setServiceType(BookingServiceType.NORMAL);

            BookingNormalInfo bookingNormalInfo = new BookingNormalInfo();
            List<NormalInfo> normalInfoList = new ArrayList<>();
            for (BookingServiceMember member : members) {
                NormalInfo normalInfo = new NormalInfo();
                normalInfo.setName(member.getMemberName());
                normalInfo.setDescription(member.getProductName()+" x 1");
                normalInfo.setPaymentMethod("Not support");
                normalInfoList.add(normalInfo);
                bookingNormalInfo.setNormalInfoList(normalInfoList);
            }
            ticketBooking.setBookingNormalInfo(bookingNormalInfo);

            List<MemberBookingInfo> memberBookingInfos = new ArrayList<>();
            for (int i = 0;i<members.size();i++) {
                BookingServiceMember member = members.get(i);
                MemberBookingInfo memberInfo = new MemberBookingInfo();
                memberInfo.setStatus(MemberBookingInfoStatus.PENDING_VENDOR);
                if (i == 0)
                    memberInfo.setTitle("LEAD MEMBER");
                else
                    memberInfo.setTitle("MEMBER");

                memberInfo.setName(member.getMemberName());
                memberInfo.setCardId(member.getMemberCardId());
                memberInfo.setEmail(member.getMemberEmail());
                Optional<Product> productTmp = productRepository.findById(member.getProductId());
                memberInfo.setProgram("- x 1" );
                if(productTmp.isPresent())
                    memberInfo.setProgram(productTmp.get().getNameEn()+ " x 1" );
                memberInfo.setCost("-");
                memberInfo.setRequireThaiVisa(false);
                memberInfo.setUsedThaiVisaService(false);
                memberInfo.setRequestNote(member.getRemark());
                memberBookingInfos.add(memberInfo);

            }
            ticketBooking.setMembers(memberBookingInfos);
            ticketBooking.setRemark(booking.getRemark());
            ticketBooking.setCreatedDate(booking.getCreatedDate().atZone(ZoneId.systemDefault()));
            ticketBooking.setFlowDefinitionKey(BookingFlowDefinitionKey.TICKET);
            return ticketBooking;
        }

        if(members != null && members.size() > 0) {
            BookingServiceMember firstMember = members.get(0);
            Product product = productRepository.findById(firstMember.getProductId()).orElseThrow(()->new NotFoundException("Not found Product"));
            ProductModel firstProductModel = productModelRepository.findById(firstMember.getModelId()).orElseThrow(()->new NotFoundException("Not foudn model"));
            Optional<Shop> shop = shopRepository.findById(product.getShopId());
            Optional<Privilege> firstPrivilege = privilegeRepository.findById(firstProductModel.getPrivilegesId().get(0));
            BookingDetail bookingDetail = firstMember.getBookingDetail();
            ret.setBookingId(firstMember.getBookingId());
            ret.setName(booking.getName());
            ret.setType(BookingInfoType.NORMAL);
            if(firstMember.getBookingDetail() != null){
                if( firstMember.getBookingDetail().isArrival() )
                    ret.setType(BookingInfoType.ARRIVAL);
                else
                    ret.setType(BookingInfoType.DEPARTURE);
            }
            ret.setCancelPolicy(firstProductModel.getPolicy());
            ret.setReserveDateTime(firstMember.getStartTime());
            ret.setLocationName("-");
            if(shop.isPresent())
                ret.setLocationName(shop.get().getNameEn());

            ret.setBookingNo(booking.getNo());
            ret.setStatus(booking.getBookingStatus());


            ret.setServiceType(BookingServiceType.NORMAL);
            ret.setFlowDefinitionKey(product.getFlowDefinitionKey());
            if(firstPrivilege.isPresent()){
                String privilegeName = firstPrivilege.get().getNameEN() != null ? firstPrivilege.get().getNameEN() : "";
            }
            if(BookingFlowDefinitionKey.AIRPORT_TRANSFER.equals(product.getFlowDefinitionKey())){
                ret.setServiceType(BookingServiceType.TRANSFER);
            }
            if( bookingDetail != null) {
                ret.setFlightNumber(firstMember.getBookingDetail().getFlightNumber());
                if (firstMember.getBookingDetail().getMeetingTime() != null)
                    ret.setMeetingTime(DateUtils.toDateString(firstMember.getBookingDetail().getMeetingTime(), "yyyy-MM-dd HH:MM"));
            }
            if (BookingServiceType.TRANSFER.equals(ret.getServiceType())) {
                BookingTransferInfo bookingTransferInfo = new BookingTransferInfo();
                bookingTransferInfo.setDistance("");
                bookingTransferInfo.setFromName("-");
                bookingTransferInfo.setFromLocation("-");
                bookingTransferInfo.setToName("-");
                bookingTransferInfo.setToLocation("-");
                bookingTransferInfo.setVehicle("-");
                if(bookingDetail.getPickupTime() != null){
                    bookingTransferInfo.setArrival(bookingDetail.isArrival());
                    bookingTransferInfo.setPickupTime(DateUtils.toDateString(bookingDetail.getPickupTime(),"yyyy-MM-dd (HH:MM)"));
                    bookingTransferInfo.setFromName(bookingDetail.getFromName());
                    bookingTransferInfo.setFromLocation("-");
                    bookingTransferInfo.setToName(bookingDetail.getToName());
                    bookingTransferInfo.setToLocation("-");
                    bookingTransferInfo.setVehicle(firstMember.getModelName());
                }

                ret.setBookingTransferInfo(bookingTransferInfo);
            }else {
                BookingNormalInfo bookingNormalInfo = new BookingNormalInfo();
                List<NormalInfo> normalInfoList = new ArrayList<>();
                for (BookingServiceMember member : members) {
                    NormalInfo normalInfo = new NormalInfo();
                    normalInfo.setName(member.getMemberName());
                    normalInfo.setDescription(member.getProductName()+" x 1");
                    normalInfo.setPaymentMethod("Not support");
                    normalInfoList.add(normalInfo);
                    bookingNormalInfo.setNormalInfoList(normalInfoList);
                }

                ret.setBookingNormalInfo(bookingNormalInfo);
            }
//            ret.setBookingEpaInfo();
//            ret.setEstimatedCost(null);

            int persons = 0;
            List<MemberBookingInfo> memberBookingInfos = new ArrayList<>();
            List<GuestBookingInfo> guestBookingInfos = new ArrayList<>();

            for (int i = 0;i<members.size();i++) {
                BookingServiceMember member = members.get(i);
                persons++;
                if(member.getBookingGuest() != null) {
                    persons += member.getBookingGuest().size();
                    for (BookingGuest bookingGuest : member.getBookingGuest()) {
                        GuestBookingInfo guestInfo = new GuestBookingInfo();
                        guestInfo.setStatus(MemberBookingInfoStatus.PENDING_VENDOR);
                        guestInfo.setName(bookingGuest.getName());
                        guestInfo.setAmount(bookingGuest.getAmount());
                        guestBookingInfos.add(guestInfo);
                    }
                }
                MemberBookingInfo memberInfo = new MemberBookingInfo();
                memberInfo.setStatus(MemberBookingInfoStatus.PENDING_VENDOR);
                if (i == 0)
                    memberInfo.setTitle("LEAD MEMBER");
                else
                    memberInfo.setTitle("MEMBER");

                memberInfo.setName(member.getMemberName());
                memberInfo.setCardId(member.getMemberCardId());
                memberInfo.setEmail(member.getMemberEmail());
                Optional<Product> productTmp = productRepository.findById(member.getProductId());
                memberInfo.setProgram("- x 1" );
                if(productTmp.isPresent())
                    memberInfo.setProgram(productTmp.get().getNameEn()+ " x 1" );

//                Optional<ProductModel> productModelTmp = productModelRepository.findById(member.getModelId());
                memberInfo.setCost("-");
                memberInfo.setRequireThaiVisa(false);
                memberInfo.setUsedThaiVisaService(false);
                if (BookingFlowDefinitionKey.EPA.equals(member.getFlowDefinitionKey())
                    && member.getModelName() != null && member.getModelName().toLowerCase().contains("in") ){
                    memberInfo.setRequireThaiVisa(true);
                }

                memberInfo.setRequestNote(member.getRemark());
                memberBookingInfos.add(memberInfo);
            }
            ret.setMembers(memberBookingInfos);
            ret.setGuests(guestBookingInfos);
            ret.setPersons(persons);
            ret.setRemark(booking.getRemark());
            ret.setCreatedDate(booking.getCreatedDate().atZone(ZoneId.systemDefault()));
        }

        return ret;
    }

    public BookingFlowDefinitionKey getFlowDefinitionKey(String bookingId) {

        List<BookingServiceMember> bookingServiceMembers = bookingServiceMemberRepository.findByBookingId(bookingId);
        if(bookingServiceMembers != null && bookingServiceMembers.size() > 0 ){
            return bookingServiceMembers.get(0).getFlowDefinitionKey();
//            Product product = productRepository.findById(bookingServiceMembers.get(0).getProductId()).orElseThrow(()-> new NotFoundException("Not found your product info."));
//            return product.getFlowDefinitionKey();
        }
        return BookingFlowDefinitionKey.GOLF;
    }

    public BookingResponse cancelBooking(String id, String reason) {
        return null;
    }

    public BookingFrom createBookingForm(String callCenter, BookingRequest request, String memberId) {
        BookingFrom bookingFrom = new BookingFrom();
        Booking booking = new Booking();
        booking.setRemark(request.getRemark());
        booking.setRequestMemberId(memberId);
        bookingFrom.setBooking(booking);

        List<BookingServiceMember> memberBookings = new ArrayList<>();
        for (BookingMemberRequest memberReq : request.getMembers()) {
            BookingServiceMember memBooking = new BookingServiceMember();
            memBooking.setMemberId(memberReq.getMemberId());
            memBooking.setProductId(memberReq.getProductId());
            memBooking.setModelId(memberReq.getModelId());
            memBooking.setRemark(memberReq.getRemark());
            //TODO IF reserve in range have to loop set multiple
            memBooking.setStartTime(request.getStartTime());
            memBooking.setEndTime(request.getEndTime());
            memBooking.setImageUpload(memberReq.getImageUpload());
            memBooking.setBookingDetail(memberReq.getBookingDetail());
            memBooking.setBookingGuest(memberReq.getBookingGuest());
            memberBookings.add(memBooking);
        }
        bookingFrom.setBookingServiceMembers(memberBookings);
        bookingFrom.setCreateBy(callCenter);
        return bookingFrom;
    }

    public void addKeyDefToBooking() {
        List<Booking> bookings = bookingRepository.findAll();
        for (Booking booking : bookings) {
            if(booking.getId() == null)
                continue;

            List<BookingServiceMember> bsm = bookingServiceMemberRepository.findByBookingId(booking.getId());
            if(bsm != null && bsm.size() > 0 ){
                booking.setRequestProductFlowDefinitionKey(bsm.get(0).getFlowDefinitionKey());
            }

            Member requestMember = booking.getRequestMember();
            if(requestMember != null){
                if(requestMember.getVisa() == null){
                    requestMember.setVisa(new MemberVisa());
                }
            }
        }

        bookingRepository.saveAll(bookings);
    }

    public Booking openTicket(String reqMemberId, TicketForm ticketForm) {

        Member reqMember = memberService.findById(reqMemberId);
//        isValidBooking(bookingFrom); //check Booking Valid data and time
        Booking newBooking =new Booking();
        newBooking.setNo("TKT0001");

        newBooking.setRemark(ticketForm.getTicketNote());
        newBooking.setCreatedBy(ticketForm.getCreateBy());
        newBooking.setCreatedDate(ZonedDateTime.now().toInstant());
        newBooking.setLastModifiedBy(ticketForm.getCreateBy());
        newBooking.setLastModifiedDate(ZonedDateTime.now().toInstant());
        newBooking.setCreatedUserType("USER");
        newBooking.setLastUserType("USER");
        newBooking.setRecordStatus("N");
        newBooking.setBookingStatus(BookingStatus.CREATE);

        newBooking.setRequestMemberId(reqMemberId);
        newBooking.setRequestMember(reqMember);
        newBooking = bookingRepository.save(newBooking);
        BookingFlowDefinitionKey requestProductType = BookingFlowDefinitionKey.TICKET;
        final String newBookingId = newBooking.getId();
        String bookingName = "-";
        BookingServiceMember memberBooking = new BookingServiceMember();
        memberBooking.setMemberId(reqMemberId);
        memberBooking.setStatus(BookingServiceMemberStatus.CREATE);
        memberBooking.setMemberName("-");
        memberBooking.setMemberEmail("-");
        memberBooking.setMemberCardId("-");
        try {
            MembershipInfo memberShip = memberService.getMembershipInfo(reqMemberId);
            log.debug("Membership: {}", memberShip);
            Member mem = memberShip.getMember();
            log.debug("Name: {}", mem.getGivenName() + " " + mem.getSurName());
            String title = mem.getTitle() != null ? mem.getTitle().getName() + " " : "";
            memberBooking.setMemberName(mem.getTitle().getName() + mem.getGivenName() + " " + mem.getSurName());
            log.debug("Email: {}", mem.getEmail());
            memberBooking.setMemberEmail(mem.getEmail());
            log.debug("Card no.: {}", memberShip.getMembership().getMembershipIdNo());
            memberBooking.setMemberCardId(memberShip.getMembership().getMembershipIdNo());
            memberBooking.setFlowDefinitionKey(BookingFlowDefinitionKey.TICKET);
        } catch (HandlerException e) {
            e.printStackTrace();
        }

        memberBooking.setBookingId(newBookingId);
        memberBooking.setProductId("");
        memberBooking.setModelId("");
        String modelName = "";

        memberBooking.setPrice(0.0f);
        memberBooking.setQuota(0);
        memberBooking.setRemark("");
        memberBooking.setStartTime(ZonedDateTime.now());
        memberBooking.setEndTime(ZonedDateTime.now());
        memberBooking.setImageUpload("");

        memberBooking.setCreatedBy(ticketForm.getCreateBy());
        memberBooking.setCreatedDate(ZonedDateTime.now().toInstant());
        memberBooking.setLastModifiedBy(ticketForm.getCreateBy());
        memberBooking.setLastModifiedDate(ZonedDateTime.now().toInstant());
        memberBooking.setCreatedUserType("USERS");
        memberBooking.setLastUserType("USERS");
        memberBooking.setRecordStatus("N");
        bookingServiceMemberRepository.save(memberBooking);

        newBooking.setName(bookingName);
        newBooking.setRequestProductFlowDefinitionKey(requestProductType);
        newBooking = bookingRepository.save(newBooking);
        return  newBooking;
    }

    public List<BookingResponse> fetchWrapMyBookingInCase(String memberId) {
        List<Booking> bookings = getBookingByMemberInCase(memberId);
        List<BookingResponse> ret = new ArrayList<>();
        for (Booking booking : bookings) {
            ret.add(wrapBooking(booking));
        }
        return ret;
    }
}
