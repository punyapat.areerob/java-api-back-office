package com.thailandelite.mis.be.service.dto.reports;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class RevenueReportResponse {
    private String name;
    private String description;
    private Float paidAmount;
    private ZonedDateTime paidDate;
    private String other1;
    private String other2;
    private String other3;
    private String other4;
}
