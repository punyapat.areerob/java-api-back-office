package com.thailandelite.mis.be.service.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class CaseInstanceListDto {
    private String caseId;

    private String caseName;

    private ArrayList<CaseInstanceDto> cases;

    public CaseInstanceListDto(ArrayList<CaseInstanceDto> cases, String caseId, String caseName) {
        this.cases = cases;
        this.caseId = caseId;
        this.caseName = caseName;
    }
}
