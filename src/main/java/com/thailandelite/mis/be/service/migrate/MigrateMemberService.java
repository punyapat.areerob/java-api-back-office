package com.thailandelite.mis.be.service.migrate;
import java.time.ZonedDateTime;

import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import com.thailandelite.mis.model.domain.master.Gender;
import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.model.domain.MemberVisa;
import com.thailandelite.mis.model.domain.Membership;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.migrate.domain.SmmMember;
import com.thailandelite.mis.migrate.repository.SmmMemberRepository;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.Member.Hobby;
import com.thailandelite.mis.model.domain.Member.MemberType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.thailandelite.mis.be.service.migrate.MigrateUtils.intToString;

@Profile("migrate")
@Slf4j
@Service
@RequiredArgsConstructor
public class MigrateMemberService {

    private final MigrateCacheService cache;
    private final MemberRepository memberRepository;
    private final MembershipRepository membershipRepository;
    private final MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository;

    private final SmmMemberRepository smmMemberRepository;

    public long member(Pageable page) {
        Page<SmmMember> pages = smmMemberRepository.findAllByMemberStatusAndRecordStatus("3", "N", page);

        List<SmmMember> srcList = pages.getContent();
        srcList.forEach(src -> {
            Member dest = new Member();
            dest.setId(src.getMemberId().toString());
//            dest.setUserId("");
            dest.setStatus(MemberStatus.MEMBER);
            dest.setTitle(cache.getTitle(src.getTitleId()));
            dest.setGivenName(src.getFNAME());
            dest.setMiddleName(src.getMNAME());
            dest.setSurName(src.getLNAME());

            Passport passport = new Passport();
            passport.setPassportNo(src.getPassportNo());
            passport.setIssueBy(src.getPassportIssueBy());
            passport.setIssueDate(src.getPassportIssueDate());
            passport.setExpireDate(src.getPassportExpiryDate());

            dest.setPassport(passport);
            dest.setPdpa(null);
            dest.setAllowContactMe(false);
            dest.setDateOfBirth(src.getBIRTHDATE());
//            dest.setCountryOfBirth(src.getBIRTHPLACE());
            dest.setPicture(src.getMemberPicture());
            dest.setGender("M".equalsIgnoreCase(src.getGenderId()) ? new Gender("0", "Male") : new Gender("1","Female"));
            dest.setBloodType(cache.getBloodType(src.getBloodType()));
            dest.setReligion(cache.getReligion(src.getReligionId()));
            dest.setNationality(cache.getNationality(src.getNationalityId()));
            dest.setOccupation(cache.getOccupation(src.getOccupationId()));
            dest.setBusinessTitle(src.getBusinessTitle());
            dest.setCompanyName(src.getCompanyName());
            dest.setNatureOfBusiness(cache.getNatureOfBusiness(src.getNatureOfBusinessId()));
            dest.setNatureOfBusinessOther(src.getNatureOfBusinessOther());
            dest.setOccupationOther(src.getOccupationOther());
            dest.setHobby(Hobby.INTERNET);
            dest.setApplicationId(intToString(src.getApplicationId()));
            dest.setApplicationNo(src.getApplicationNo());
//            dest.setCardId(intToString(src.getPackageId()));
//            dest.setMembershipNo(src.getMembershipNo());
            dest.setAgentId(src.getAgentId());
            dest.setApprovalNo(src.getApprovalNo());
            dest.setAllergic(src.getALLERGIC());

            MemberVisa memberVisa = new MemberVisa();
            memberVisa.setVisaNo(src.getVisaNo());
            memberVisa.setVisaAtIssue(src.getVisaAtIssue());
            memberVisa.setVisaIssueDate(src.getVisaIssueDate());
            memberVisa.setVisaExpireDate(src.getVisaExpiryDate());
            dest.setVisa(memberVisa);
            dest.setEmail(src.getEMAIL());
            dest.setMobileInHomeCountry(src.getMobileInHomeCountry());
            dest.setMobileInThai(src.getMobileInThai());
            dest.setCountry(cache.getCountry(src.getCountryId()));
            dest.setStatus(MemberStatus.MEMBER);
            dest.setRemark(src.getREMARK());
            dest.setMemberType(MemberType.CORE);
            dest.setMemberCoreId(src.getMemberCoreId());
            dest.setMemberGroupId(intToString(src.getMemberGroupId()));
            dest.setMemberFileApplication(src.getMemberFileApplication());
            dest.setMemberFileOther(src.getMemberFileOther());
            dest.setMemberFilePassport(src.getMemberFilePassport());
//            dest.setMarketing(new Marketing());
//            dest.setActiveDate(src.getActiveDate());
//            dest.setCoreMemberId(intToString(src.getCoreMemberId()));
//            dest.setMemberTransferId(intToString(src.getMemberTransferId()));
            dest.setMemberBlockServicesStatus(src.getMemberBlockServicesStatus());
            dest.setMemberBlockServicesRemark(src.getMemberBlockServicesRemark());
            dest.setMemberNext(src.getMemberNext());
            dest.setMemberBirthCertificate(src.getMemberBirthCertificate());
            dest.setMemberMarriageCertificate(src.getMemberMarriageCertificate());
            dest.setMemberProofPayment(src.getMemberProofPayment());
//            dest.setExpiryDate(src.getExpiryDate());
            dest.setImmigrationAttachFile(src.getImmigrationAttachFile());
            dest.setRemarkEdit(src.getRemarkEdit());
            dest.setCommissionPaymentStatus(!"F".equals(src.getCommissionPaymentStatus()));
//            dest.setMemberUpgradeId(intToString(src.getMemberUpgradeId()));
            dest.setCovid19Remedy1(!"F".equals(src.getCommissionPaymentStatus()));
//            dest.setExpiryDateBeforeCovid19(src.getExpiryDateBeforeCovid19());
//            dest.setExpiryDateAfterCovid19(src.getExpiryDateAfterCovid19());
            dest.setCreatedBy(src.getCreateUser());
//            dest.setCreatedDate(new Instant());
            dest.setLastModifiedBy(src.getLastUser());
//            dest.setLastModifiedDate(new Instant());
            dest.setCreatedUserType(src.getCreateUserType());
            dest.setLastUserType(src.getLastUserType());
            dest.setRecordStatus(src.getRecordStatus());

            memberRepository.save(dest);
            log.debug("Insert Member:{}, ", dest);

            //Insert New membership
            Membership membership = new Membership();
//            membership.setId("");
            membership.setCardId(intToString(src.getPackageId()));
            membership.setMembershipNo(src.getMembershipNo());
            membership.setMembershipIdNo(src.getMembershipIdNo());
            membership.setMemberId(dest.getId());

            membership.setStatus(getMembershipStatus(src));
//            membership.setTemporaryCardUrl("");
//            membership.setEmailWelcomeMessage("");
            membership.setStartDate(src.getActiveDate());
            membership.setEndDate(src.getExpiryDate());
            membership.setExpiryDateBeforeCovid19(src.getExpiryDateBeforeCovid19());
            membership.setExpiryDateAfterCovid19(src.getExpiryDateAfterCovid19());
            membership.setActive(true);
            membership.setRenewQuotaDate(ZonedDateTime.now());
            membership.setCoreMemberId(intToString(src.getCoreMemberId()));
            membership.setMemberTransferId(intToString(src.getMemberTransferId()));
            membership.setMemberUpgradeId(intToString(src.getMemberUpgradeId()));
            membership.setMaxTransfer(0);
//            membership.setDateToAnnualFee(ZonedDateTime.now());
//            membership.setStayReportDate(ZonedDateTime.now());
//            membership.setNinetyReportDate(ZonedDateTime.now());
            Membership.AnnualFee annualFee = new Membership.AnnualFee();
            annualFee.setStatus(Membership.AnnualFeeStatus.NO);
            membership.setAnnualFee(annualFee);
//            membership.setCardPrivileges(Lists.newArrayList());
            membershipRepository.save(membership);
            log.debug("Insert Membership:{}, ", membership);

            //Insert New Quota
            List<MemberPrivilegeQuota> quotaList = cache.getMemberPrivilegeQuota(src.getMemberGroupId()).stream().map(mpq -> {
                MemberPrivilegeQuota mp = new MemberPrivilegeQuota();
//                mp.setId(mpq.getMemberPrivilegeQuotaId().toString());
                mp.setPrivilegeId(mpq.getPrivilegeId().toString());
                mp.setYearQuota(mpq.getYearQuota());
                mp.setQuotaQuantity(mpq.getQuotaQuantity());
                mp.setQuotaAdjust(mpq.getQuotaAdjust());
                mp.setQuotaUsed(mpq.getQuotaUsed());
                mp.setMemberGroupId(mpq.getMemberGroupId().toString());
//                mp.setQuotaUsedBk(mpq.getQuotaUsedBk());
                mp.setCreatedBy(mpq.getCreateUser());
                mp.setRecordStatus(src.getRecordStatus());
                return mp;
            }).collect(Collectors.toList());

            memberPrivilegeQuotaRepository.saveAll(quotaList);
            log.debug("Insert MemberPrivilege:{}, ", quotaList);
        });

        return pages.getContent().size();
    }

    private Membership.MembershipStatus getMembershipStatus(SmmMember src) {
        Membership.MembershipStatus membershipStatus = null;
        if( "1".equalsIgnoreCase(src.getMemberStatus()) ){
            membershipStatus = Membership.MembershipStatus.CANCEL;
        }else if("3".equalsIgnoreCase(src.getMemberStatus())){
            membershipStatus = Membership.MembershipStatus.MEMBER;
        }else if("7".equalsIgnoreCase(src.getMemberStatus())){
            membershipStatus = Membership.MembershipStatus.UPGRADE;
        }else if("10".equalsIgnoreCase(src.getMemberStatus())){
            membershipStatus = Membership.MembershipStatus.PENDING;
        }else if("11".equalsIgnoreCase(src.getMemberStatus())){
            membershipStatus = Membership.MembershipStatus.TERMINATE;
        }else if("13".equalsIgnoreCase(src.getMemberStatus())){
            membershipStatus = Membership.MembershipStatus.TRANSFER;
        }else if("15".equalsIgnoreCase(src.getMemberStatus())){
            membershipStatus = Membership.MembershipStatus.INACTIVE;
        }
        return membershipStatus;
    }
}
