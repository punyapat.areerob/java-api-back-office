package com.thailandelite.mis.be.service.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.thailandelite.mis.be.domain.enumeration.StepState;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@JsonPropertyOrder({
	   "id",
	   "caseNo",
	   "status",
	   "currentStepId",
	   "remark",
	   "caseId",
		"date",
	   "createdBy",
	   "updatedBy",
	   "createdAt",
	   "updatedAt"
	})
public class CaseInstanceDto {
	private String id;
	private String caseNo;
	private StepState status;
	private String currentStepId;
	private String remark;
	private String caseId;
    private UserDTO createdBy;
    private UserDTO updatedBy;
    private Instant createdAt;
    private Instant updatedAt;
    private String statusDisplay;
    private Object data;
}
