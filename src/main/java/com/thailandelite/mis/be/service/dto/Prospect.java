package com.thailandelite.mis.be.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Prospect {
    private String title;
    private String givenName;
    private String middleName;
    private String surName;
    private String areaCode;
    private String contactNo;
    private String passportNo;
    private String passportExpireDate;
    private String passportDateOfBirth;
    private boolean isAgreePda;
    private boolean isAllowContactMe;

    @JsonProperty("isAgreePda")
    private void setAgreePda(boolean isAgreePda) {
        this.isAgreePda = isAgreePda;
    }

    @JsonProperty("isAllowContactMe")
    private void setAllowContactMe(boolean isAllowContactMe) {
        this.isAllowContactMe = isAllowContactMe;
    }
}
