package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.repository.AgentContractRepository;
import com.thailandelite.mis.be.repository.AgentRepository;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.AgentReportResponse;
import com.thailandelite.mis.be.service.dto.reports.BaseReportRequest;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentContract;
import com.thailandelite.mis.model.domain.enumeration.ApplicationStatus;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@RequiredArgsConstructor
public class AgentReportService {

    private final MongoTemplate mongoTemplate;
    private final ExcelService excelService;
    private final AgentRepository agentRepositiory;
    private static String contract_end_field = "contractDateEnd";
    private static String contract_start_field = "contractDateStart";

    public Page<AgentReportResponse> getAgentExpireContractReport(BaseReportRequest arg, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<AgentReportResponse> listResult = generateEntityList(arg, queryInfo, contract_end_field);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] generateExcelExpireReport(String sheetName, BaseReportRequest searchRequest) {
        List<String> headers = Arrays.asList("Start", "End", "No", "Agent", "Code", "Type", "email", "In Thai");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        generateExcel(searchRequest, rows, contract_end_field);
        return excelService.generateExcelReport(sheetName, headers, rows);
    }

    public Page<AgentReportResponse> getAgentRenewContractReport(BaseReportRequest arg, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<AgentReportResponse> listResult = generateEntityList(arg, queryInfo, contract_start_field);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }


    @Data
    public class AgentApplicationStatus{
        private String agentName;
        private ApplicationStatus status;

        public AgentApplicationStatus(String agentCode, ApplicationStatus status) {
            this.agentName = agentCode;
            this.status = status;
        }
    }


    @Data
    public class AgentApplicationReport{
        private String agentName;
        private long total;
        private long paid;

        public AgentApplicationReport(String agentName, long total, long paid) {
        this.agentName = agentName;
        this.total = total;
        this.paid = paid;
        }
    }


    public Page<AgentApplicationReport> getAgentApplicantReport(BaseReportRequest arg, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<Application> listResult = getApplications(arg,queryInfo ,"created_date");
        List <AgentApplicationStatus> result = listResult.stream().map( application ->
             new AgentApplicationStatus(agentRepositiory.findById(application.getRegisterAgentId()).get().getAgentCode(),
                application.getStatus()
                )
        ).collect(Collectors.toList());

        var report = result.stream().collect(Collectors.groupingBy(w -> w.agentName));

        List<AgentApplicationReport> count_report = new ArrayList<>();
        report.keySet().forEach( x->
            count_report.add( new AgentApplicationReport( x,
               report.get(x).stream().count() ,
               report.get(x).stream().filter(y->y.status == ApplicationStatus.PAYMENT).count()
               ))
        );
        return PageableExecutionUtils.getPage(count_report, pageable, (LongSupplier) ()-> count_report.size());
    }


    public byte[] generateExcelRenewReport(String sheetName, BaseReportRequest searchRequest) {
        List<String> headers = Arrays.asList("Start", "End", "No", "Agent", "Code", "Type", "email", "In Thai");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        generateExcel(searchRequest, rows, contract_start_field);
        return excelService.generateExcelReport(sheetName, headers, rows);
    }

    private List<AgentReportResponse> generateEntityList(BaseReportRequest arg, Query queryInfo, String field) {
        List<AgentReportResponse> listResult = new ArrayList<>();
        List<AgentContract> entityList = getEntityList(arg, queryInfo, field);
        HashMap<String, AgentContract> lastedContract = new HashMap<>();
        for (AgentContract entity : entityList) {
            if(entity.getAgent() == null)
                continue;
            String agentId = entity.getAgent().getId();
            AgentContract agentContract = lastedContract.get(agentId);
            if(agentContract != null && agentContract.getContractDateStart().isAfter(entity.getContractDateStart())){
                entity = agentContract;
            }
            lastedContract.put(agentId, entity);
        }
        for (String agentId : lastedContract.keySet()) {
            AgentReportResponse res = new AgentReportResponse();
            res.setAgentContract(lastedContract.get(agentId));
            listResult.add(res);
        }
        return listResult;
    }


    private List<Application> getApplications(BaseReportRequest arg, Query queryInfo, String field) {

        if ( arg.getFrom() !=null && arg.getTo() != null)  {
            queryInfo.addCriteria(where(field).gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where(field).gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where(field).lt(arg.getTo()));
        }

        queryInfo.addCriteria(where("registerAgentId").exists(true));


        List<Application> list = mongoTemplate.find(queryInfo, Application.class);
        return list;
    }

    private List<AgentContract> getEntityList(BaseReportRequest arg, Query queryInfo, String field) {

        if ( arg.getFrom() !=null && arg.getTo() != null)  {
            queryInfo.addCriteria(where(field).gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where(field).gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where(field).lt(arg.getTo()));
        }
        List<AgentContract> list = mongoTemplate.find(queryInfo, AgentContract.class);
        return list;
    }

    private void generateExcel(BaseReportRequest arg, List<List<ExcelService.ExcelValue>> rows, String field) {
        Query queryInfo = new Query();
        List<AgentReportResponse> listResult = generateEntityList(arg, queryInfo, field);

        for (int i=0; i<listResult.size(); i++) {
            AgentReportResponse cellValue = listResult.get(i);
            AgentContract agentContract = cellValue.getAgentContract();
            Agent agent = cellValue.getAgent();
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            String agentName = agent.getAgentType() != null ? agent.getAgentType().getNameEn() : "-" ;
            cells.add(new ExcelService.ExcelValue(agentContract.getContractDateStart()));
            cells.add(new ExcelService.ExcelValue(agentContract.getContractDateEnd()));
            cells.add(new ExcelService.ExcelValue(agent.getContractNo()));
            cells.add(new ExcelService.ExcelValue(agent.getSalesAgentEn()));
            cells.add(new ExcelService.ExcelValue(agent.getAgentCode()));
            cells.add(new ExcelService.ExcelValue(agentName));
            cells.add(new ExcelService.ExcelValue(agent.getAgentEmail()));
            cells.add(new ExcelService.ExcelValue(agent.getCompanyRegisInThai()));
            rows.add(cells);
        }
    }
}
