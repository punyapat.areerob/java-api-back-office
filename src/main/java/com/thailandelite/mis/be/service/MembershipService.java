package com.thailandelite.mis.be.service;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.thailandelite.mis.model.domain.Membership.AnnualFeeStatus;

import com.google.common.base.Verify;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.service.model.CardInfo;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.agent.Agent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.joda.time.Years;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class MembershipService {
    private final CardService cardService;
    private final MembershipRepository membershipRepository;
    private final MemberRepository memberRepository;
    private final ModelMapper modelMapper;
    private final InvoiceRepository invoiceRepository;
    private final IncomeReportRepository incomeReportRepository;
    private final AgentRepository agentRepository;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository;
    private final PrivilegeRepository privilegeRepository;

    public Membership newMembership(String memberId, String cardId, String invoiceId) {
        Card card = cardService.getCard(cardId);
        List<CardPrivilege> cardPrivileges = cardService.getPrivilege(cardId);

        Membership membership = new Membership();
        membership.setMemberId(memberId);
        membership.setMembershipNo("");
        membership.setMembershipIdNo(this.generateMembershipId(card.getAbbreviation()));
        membership.setCardId(cardId);
        membership.setInvoiceId(invoiceId);
        membership.setStatus(Membership.MembershipStatus.PENDING);
//        membership.setCardPrivileges(cardPrivileges);
        membership.setMaxTransfer(card.getTransferSetting().getTimes());

        Membership.AnnualFee annualFee = getAnnualFee(card);
        membership.setAnnualFee(annualFee);

        log.info("newMembership : {}", membership);
        membershipRepository.save(membership);
        return membership;
    }

    @NotNull
    public String generateMembershipNo() { //TODO: add database sequence running number of MembershipNo
        String year = String.valueOf(ZonedDateTime.now().getYear()).substring(2);
        String membershipYear = "MEMBER_NO_" + year;
        long num = sequenceGeneratorService.generateSequence(membershipYear);
        return year + "/" + num;
    }

    @NotNull
    public String generateMembershipId(String cardCode) {
        String seqName = "MEMBERSHIP_" + cardCode;
        long num = sequenceGeneratorService.generateSequence(seqName);
        return cardCode + num + getCheckSum(num);
    }

    private int getCheckSum(long id) {
        String temp = Long.toString(id);
        int[] checkArr = Stream.of(temp.split("")).mapToInt(Integer::parseInt).toArray();
        int sum = 0;
        for (int num : checkArr) {
            if(Ints.indexOf(checkArr, num) % 2 == 0) {
                num = num * 2;
                if(num > 9) {
                    num -= 9;
                    sum += num;
                }
                else {
                    sum += num;
                }
            }
            else {
                sum += num;
            }
        }
        int mod = sum % 10;
        return ((mod == 0) ? 0 : 10 - mod);
    }

    @NotNull
    private Membership.AnnualFee getAnnualFee(Card card) {
        boolean requireAnnualFee = card.getAnnualFeeSetting().isRequire();
        Membership.AnnualFee annualFee = new Membership.AnnualFee();
        if (requireAnnualFee) {
            annualFee.setStatus(AnnualFeeStatus.REQUIRE);
            annualFee.setTotal(card.getValidityYear() - 1);
            annualFee.setRemain(card.getValidityYear() - 1);
            annualFee.setNext(ZonedDateTime.now().plusYears(1));
        } else {
            annualFee.setStatus(AnnualFeeStatus.NO);
        }
        return annualFee;
    }

    public Membership activateMember(String membershipId, ZonedDateTime activateDate) {
        Membership membership = membershipRepository.findById(membershipId)
            .orElseThrow(() -> new NotFoundException("Membership", membershipId));
        Member member = memberRepository.findById(membership.getMemberId()).get();
        Card card = cardService.getCard(membership.getCardId());
        ZonedDateTime paymentDate = invoiceRepository.findById(membership.getInvoiceId()).get().getPayDate();
        Membership.AnnualFee annualFee = getAnnualFee(card);
        membership.setAnnualFee(annualFee);
        membership.setPaymentDate(paymentDate);

        // TODO: 27/4/2021 AD Implement 90 day report
//        membership.setNextStayReport(new ZonedDateTime());
//        membership.setNextNinetyReport(new ZonedDateTime());
        membership.setStartDate(activateDate);
        membership.setEndDate(activateDate.plusYears(card.getValidityYear()));
        membership.setStatus(Membership.MembershipStatus.MEMBER);


        IncomeReport incomeReport = new IncomeReport();
        ZonedDateTime lastDay = activateDate.plusYears(card.getValidityYear());
        incomeReport.setActivateDate(activateDate);
        Long days = ChronoUnit.DAYS.between(activateDate,lastDay);
        Long perDays = card.getMembershipFee().getTotal().longValue()/days;
        incomeReport.setDay(days);

        if(member.getApplicationNo() != null) {
            incomeReport.setMemberNo(member.getApplicationNo());
        }
        if(member.getRegisterAgentId() != null) {
            Agent agent = agentRepository.findById(member.getRegisterAgentId()).get();
            incomeReport.setTypeOfAgent(agent.getAgentType().getNameEn());
            incomeReport.setAgentName(agent.getAgentCode());
        }
        if(member.getNationality()!=null) {
            incomeReport.setNationality(member.getNationality().getName());
        }
        if(member.getMemberType() !=null) {
            incomeReport.setMemberType(member.getMemberType().name());
        }
        incomeReport.setPayDate(paymentDate);
        incomeReport.setMembershipId(membershipId);
        incomeReport.setPerDay(perDays);
        incomeReport.setMemberId(member.getId());
        incomeReport.setMemberName(member.getGivenName());
        incomeReport.setMemberSurname(member.getSurName());
        incomeReport.setYear(activateDate.getYear());
        incomeReport.setBalance( card.getMembershipFee().getTotal().toString());
        List<MonthlyPayment> monthlyPaymentList = new ArrayList<>();

        ZonedDateTime firstActivate = activateDate;
        MonthlyPayment firstMonthlyPayment = new MonthlyPayment();
        firstMonthlyPayment.setPaymentDate(firstActivate.with(TemporalAdjusters.lastDayOfMonth()));
        firstMonthlyPayment.setAmount(perDays*(firstActivate.toLocalDate().lengthOfMonth()-(firstActivate.getDayOfMonth()!=1? firstActivate.getDayOfMonth()-1:1)));
        monthlyPaymentList.add(firstMonthlyPayment);
        firstActivate = firstActivate.plusMonths(1);
        while( firstActivate.compareTo(lastDay) < 0){
            MonthlyPayment monthlyPayment = new MonthlyPayment();
            monthlyPayment.setPaymentDate(firstActivate.with(TemporalAdjusters.lastDayOfMonth()));
            monthlyPayment.setAmount(perDays*firstActivate.toLocalDate().lengthOfMonth());
            monthlyPaymentList.add(monthlyPayment);
            firstActivate = firstActivate.plusMonths(1);
        }
        if(firstActivate.toLocalDate().compareTo(firstActivate.with(TemporalAdjusters.firstDayOfMonth()).toLocalDate()) > 0)
        {
            firstMonthlyPayment.setPaymentDate(firstActivate.with(TemporalAdjusters.lastDayOfMonth()));
            firstMonthlyPayment.setAmount(perDays*(firstActivate.getDayOfMonth()));
            monthlyPaymentList.add(firstMonthlyPayment);
        }

        incomeReport.setMonthlyPaymentList(monthlyPaymentList);
        incomeReportRepository.save(incomeReport);

        log.info("firstActivateMember : {}", membership);
        membershipRepository.save(membership);
        return membership;
    }

    public Membership paidAnnualFee(String membershipId, int years) {
        Membership membership = membershipRepository.findById(membershipId).orElseThrow(() -> new NotFoundException("Membership"));
        Membership.AnnualFee annualFee = membership.getAnnualFee();

        log.debug("PaidAnnualFee: membershipId={}, year={}, from:{}", membershipId, years, annualFee);

        Verify.verify(annualFee.getStatus() == AnnualFeeStatus.REQUIRE, "Annual fee not require");
        Verify.verify(annualFee.getRemain() > 0, "Annual fee not require(fully paid)");
        Verify.verify(annualFee.getRemain() >= years, "Annual fee remain %s years", annualFee.getRemain());

        int remain = annualFee.getRemain() - years;
        if(remain > 0){
            annualFee.setNext(annualFee.getNext().plusYears(years));
        }else {
            annualFee.setNext(null);
            annualFee.setStatus(AnnualFeeStatus.ALL_PAID);
        }

        annualFee.setRemain(remain);
        membership.setStatus(Membership.MembershipStatus.MEMBER);

        log.debug("PaidAnnualFee: to:{}", annualFee);

        membershipRepository.save(membership);
        return membership;
    }

    public Membership upgradeMembership(String memberId, String upgradeCardId) {
        Membership membership = membershipRepository.findOneByMemberIdAndStatus(memberId, Membership.MembershipStatus.MEMBER)
            .orElseThrow(() -> new NotFoundException("Membership"));
        Member member = memberRepository.findById(memberId).orElseThrow(() -> new NotFoundException("Member"));

        Card card = cardService.getCard(membership.getCardId());
        cardService.isAllowToUpgrade(card, upgradeCardId);

        CardInfo upgradeCardInfo = cardService.getCardInfo(upgradeCardId);

        ZonedDateTime newEndDate = membership.getStartDate().plusYears(upgradeCardInfo.getCard().getValidityYear());
        List<CardPrivilege> newPrivileges = upgradeCardInfo.getCardPrivileges();
        List<MemberPrivilegeQuota> memberPrivilegeQuotas = memberPrivilegeQuotaRepository.findAllByMemberGroupId(member.getMemberGroupId()).stream().map(m -> {  // To be correct use find by membershipId
            Privilege privilege = privilegeRepository.findById(m.getPrivilegeId()).orElseThrow(() -> new NotFoundException("Privilege"));
            CardPrivilege cardPrivilege = newPrivileges.stream().filter(c -> c.getPrivilege().equals(privilege)).findAny().orElse(null);

            MemberPrivilegeQuota upgradePrivilegeQuota = modelMapper.map(m, MemberPrivilegeQuota.class);
            Integer upgradeYearLeft = (int)ChronoUnit.YEARS.between(ZonedDateTime.now(), newEndDate);
            Integer yearLeft = (int)ChronoUnit.YEARS.between(ZonedDateTime.now(), membership.getEndDate());
            Integer remain = (m.getQuotaQuantity() - m.getQuotaUsed()) + (m.getQuotaQuantity() * yearLeft);
            Integer remainPerYear = remain / upgradeYearLeft;
            Integer extraAddYear = remain % upgradeYearLeft;
            if(cardPrivilege != null) {
                if(isUnLimitQuota(cardPrivilege.getQuota())) {
                    upgradePrivilegeQuota.setQuotaQuantity(cardPrivilege.getQuota());
                    upgradePrivilegeQuota.setId(null);
                    memberPrivilegeQuotaRepository.save(upgradePrivilegeQuota);
                }else{
                    for(Integer year = 1; year <= upgradeYearLeft; year++) {
                        if(year == 1) {
                            upgradePrivilegeQuota.setQuotaQuantity(cardPrivilege.getQuota() + remainPerYear + 1);
                            upgradePrivilegeQuota.setId(null);
                            memberPrivilegeQuotaRepository.save(upgradePrivilegeQuota);
                        }else if(year > 1 && year <= extraAddYear) {
                            MemberPrivilegeQuota extraUpgradePrivilegeQuota = modelMapper.map(m, MemberPrivilegeQuota.class);
                            extraUpgradePrivilegeQuota.setQuotaQuantity(cardPrivilege.getQuota() + remainPerYear + 1);
                            Integer yearQuota = Integer.parseInt(m.getYearQuota()) + 1;
                            extraUpgradePrivilegeQuota.setYearQuota(String.valueOf(yearQuota));
                            extraUpgradePrivilegeQuota.setQuotaUsed(0);
                            extraUpgradePrivilegeQuota.setActive(false);
                            extraUpgradePrivilegeQuota.setId(null);
                            memberPrivilegeQuotaRepository.save(extraUpgradePrivilegeQuota);
                        }else{
                            MemberPrivilegeQuota extraUpgradePrivilegeQuota = modelMapper.map(m, MemberPrivilegeQuota.class);
                            extraUpgradePrivilegeQuota.setId(null);
                            extraUpgradePrivilegeQuota.setQuotaQuantity(cardPrivilege.getQuota() + remainPerYear);
                            Integer yearQuota = Integer.parseInt(m.getYearQuota()) + 1;
                            extraUpgradePrivilegeQuota.setYearQuota(String.valueOf(yearQuota));
                            extraUpgradePrivilegeQuota.setQuotaUsed(0);
                            extraUpgradePrivilegeQuota.setActive(false);
                            extraUpgradePrivilegeQuota.setId(null);
                            memberPrivilegeQuotaRepository.save(extraUpgradePrivilegeQuota);
                        }
                    }
                }
            }else{
                for(Integer year = 1; year <= upgradeYearLeft; year++) {
                    if(year == 1) {
                        upgradePrivilegeQuota.setQuotaQuantity(remainPerYear + 1);
                        upgradePrivilegeQuota.setId(null);
                        memberPrivilegeQuotaRepository.save(upgradePrivilegeQuota);
                    }else if(year > 1 && year <= extraAddYear) {
                        MemberPrivilegeQuota extraUpgradePrivilegeQuota = modelMapper.map(m, MemberPrivilegeQuota.class);
                        extraUpgradePrivilegeQuota.setQuotaQuantity(remainPerYear + 1);
                        Integer yearQuota = Integer.parseInt(m.getYearQuota()) + 1;
                        extraUpgradePrivilegeQuota.setYearQuota(String.valueOf(yearQuota));
                        extraUpgradePrivilegeQuota.setQuotaUsed(0);
                        extraUpgradePrivilegeQuota.setActive(false);
                        extraUpgradePrivilegeQuota.setId(null);
                        memberPrivilegeQuotaRepository.save(extraUpgradePrivilegeQuota);
                    }else{
                        MemberPrivilegeQuota extraUpgradePrivilegeQuota = modelMapper.map(m, MemberPrivilegeQuota.class);
                        extraUpgradePrivilegeQuota.setId(null);
                        extraUpgradePrivilegeQuota.setQuotaQuantity(remainPerYear);
                        Integer yearQuota = Integer.parseInt(m.getYearQuota()) + 1;
                        extraUpgradePrivilegeQuota.setYearQuota(String.valueOf(yearQuota));
                        extraUpgradePrivilegeQuota.setQuotaUsed(0);
                        extraUpgradePrivilegeQuota.setActive(false);
                        extraUpgradePrivilegeQuota.setId(null);
                        memberPrivilegeQuotaRepository.save(extraUpgradePrivilegeQuota);
                    }
                }
            }
            return upgradePrivilegeQuota;
        }).collect(Collectors.toList());

        Membership upgradeMembership = modelMapper.map(membership, Membership.class);
        membership.setStatus(Membership.MembershipStatus.UPGRADE);
        membershipRepository.save(membership);

        upgradeMembership.setId(null);
        upgradeMembership.setCardId(upgradeCardId);
        upgradeMembership.setEndDate(newEndDate);
        upgradeMembership.setStatus(Membership.MembershipStatus.MEMBER);
        Card upgradeCard = cardService.getCard(upgradeCardId);
        upgradeMembership.setMembershipIdNo(this.generateMembershipId(upgradeCard.getAbbreviation()));
//        upgradeMembership.setCardPrivileges(newPrivileges);
        log.info("upgradeMembership : from:{} , to:{}", membership, upgradeMembership);

        membershipRepository.save(upgradeMembership);
        return upgradeMembership;
    }

    public boolean isUnLimitQuota(Integer quotaQuantity) {
        return quotaQuantity == 9999;
    }

    public Membership typeChangeMembership(String memberId, String typeChangeCardId) {
        Membership membership = membershipRepository.findOneByMemberIdAndStatus(memberId, Membership.MembershipStatus.MEMBER)
            .orElseThrow(() -> new NotFoundException("Membership"));

        Card typeChangeCard = cardService.getCard(typeChangeCardId);
        cardService.isAllowToTypeChange(typeChangeCard, membership.getCardId());

        Membership NewMembership = new Membership();
        NewMembership.setMemberId(memberId);
        NewMembership.setMembershipNo("");//// TODO: 27/4/2021 AD Generate membership No ex.(21/3345)
        NewMembership.setMembershipIdNo(this.generateMembershipId(typeChangeCard.getAbbreviation()));
        NewMembership.setCardId(typeChangeCardId);
//        membership.setInvoiceId(invoiceId);
        NewMembership.setStatus(Membership.MembershipStatus.PENDING);
        NewMembership.setMaxTransfer(typeChangeCard.getTransferSetting().getTimes());

        Membership.AnnualFee annualFee = new Membership.AnnualFee();
        annualFee.setStatus(AnnualFeeStatus.NO);
        NewMembership.setAnnualFee(annualFee);

        membership.setStatus(Membership.MembershipStatus.TYPE_CHANGE);

        log.info("newMembership : {}", NewMembership);
        membershipRepository.save(membership);
        membershipRepository.save(NewMembership);

        return NewMembership;
    }

    public Membership newMembershipByTransfer(String memberId, String transferMemberId) {
        Membership p = new Membership();
        p.setMemberId(memberId);
        Example<Membership> match = Example.of(p);
        Membership membership = membershipRepository.findOne(match).orElseThrow(() -> new NotFoundException("Member"));

        Card card = cardService.getCard(membership.getCardId());

        Card.TransferSetting transferSetting = card.getTransferSetting();
        Verify.verify(membership.getMaxTransfer() > 0, "Transfer limit");

        Membership transferMembership = modelMapper.map(membership, Membership.class);
        transferMembership.setId(null);
        transferMembership.setMemberId(transferMemberId);
        transferMembership.setMaxTransfer(membership.getMaxTransfer() - 1);

        log.info("transferMembership : from:{} , to:{}", membership, transferMembership);

        membership.setStatus(Membership.MembershipStatus.TRANSFER);
        membershipRepository.save(membership);

        membershipRepository.save(transferMembership);
        return transferMembership;
    }

    public void taskAnnualFeeWaring(){
        ZonedDateTime oneMonthToAnnualDate = LocalDate.now().plusMonths(1).atStartOfDay(ZoneId.systemDefault());
        List<Membership> memberships = membershipRepository.findByAnnualFeetBetween(oneMonthToAnnualDate, oneMonthToAnnualDate.plusDays(1));
        //todo waning annual fee notification
    }

    public void taskMembershipExpireWaring(){
        ZonedDateTime oneMonthToAnnualDate = LocalDate.now().plusMonths(1).atStartOfDay(ZoneId.systemDefault());
        List<Membership> memberships = membershipRepository.findByEndDateBetween(oneMonthToAnnualDate, oneMonthToAnnualDate.plusDays(1));
        //todo waning expire notification
    }

    public Membership getActiveMembership(String memberId) {
        List<Membership> membershipList = membershipRepository.findByMemberIdAndStatusIn(memberId, Arrays.asList(Membership.MembershipStatus.MEMBER, Membership.MembershipStatus.TYPE_CHANGE), Sort.by("membershipNo"));
        if(membershipList == null || membershipList.size() <= 0 )
            throw new NotFoundException("Not found your active membership.");
        return membershipList.get(0);
    }

    public List<Membership> findByCardIn(List<String> cardIds) {
        return membershipRepository.findByCardIdIn(cardIds);
    }
}
