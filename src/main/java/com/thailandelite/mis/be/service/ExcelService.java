package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.JobPositionsRepository;
import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.JobPositions;
import com.thailandelite.mis.model.domain.hr.JobPositionsStatus;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExcelService {

    public byte[] generateExcelReport(String sheetName, List<String> header, List<List<ExcelValue>> rows)
    {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(sheetName);
        generateHeader(workbook, sheet.createRow(0), header);
        generateContent(sheet, rows);

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            return  outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  null;

    }
    public void generateHeader(Workbook workbook, Row header, List<String> content)
    {
        CellStyle headerStyle = workbook.createCellStyle();
        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFont(font);
        Cell headerCell;
        for (int i=0;i<content.size(); i++) {
            headerCell = header.createCell(i);
            headerCell.setCellValue(content.get(i));
        }
    }

    public  void generateContent(Sheet sheet, List<List<ExcelValue>> rows)
    {
        for (int rowNo=0; rowNo<rows.size(); rowNo++) {
            Row row = sheet.createRow(rowNo+1);
            Cell headerCell;
            for (int i=0; i<rows.get(rowNo).size(); i++) {
                ExcelValue cell = rows.get(rowNo).get(i);
                headerCell = row.createCell(i);
                headerCell.setCellValue(cell.getColumnValue());
            }
        }
    }

    @Data
    public static class ExcelValue {

        private Object value;

        public ExcelValue(Object value) {
            this.value = value;
        }

        private String getColumnValue() {
            if (value != null && value instanceof ZonedDateTime) {
                ZonedDateTime tmp = (ZonedDateTime) value;
                return tmp.toLocalDate().toString();
            }else if (value != null && value instanceof Instant) {
                Instant tmp = (Instant) value;
                return tmp.toString();
            }else if (value != null && value instanceof Date) {
                Date tmp = (Date) value;
                return tmp.toString();
            }else if (value != null && value instanceof Integer) {
                Integer tmp = (Integer) value;
                return tmp.toString();
            }else if (value != null && value instanceof Float) {
                Float tmp = (Float) value;
                return tmp.toString();
            }else if (value != null && value instanceof Double) {
                Double tmp = (Double) value;
                return tmp.toString();
            }else if (value != null){
                return value.toString();
            }
            return "-";
        }
    }
}
