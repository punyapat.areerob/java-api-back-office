package com.thailandelite.mis.be.service.dto;

import lombok.Data;
import org.camunda.bpm.engine.impl.persistence.entity.SuspensionState;
import org.camunda.bpm.engine.task.DelegationState;
import org.camunda.bpm.engine.task.Task;

import java.util.Date;


@Data
public class TaskDto implements Task {
    protected String id;

    protected String owner;
    protected String assignee;
    protected DelegationState delegationState;

    protected String parentTaskId;

    protected String name;
    protected String description;
    protected int priority = Task.PRIORITY_NORMAL;
    protected Date createTime; // The time when the task has been created
    protected Date dueDate;
    protected Date followUpDate;
    protected int suspensionState = SuspensionState.ACTIVE.getStateCode();
    protected String tenantId;

    // execution
    protected String executionId;

    protected String processInstanceId;

    protected String processDefinitionId;

    // caseExecution
    protected String caseExecutionId;

    protected String caseInstanceId;
    protected String caseDefinitionId;

    // taskDefinition
    protected String taskDefinitionKey;

    protected String formKey;

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int getPriority() {
        return this.priority;
    }

    @Override
    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String getOwner() {
        return this.owner;
    }

    @Override
    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String getAssignee() {
        return this.assignee;
    }

    @Override
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @Override
    public DelegationState getDelegationState() {
        return this.delegationState;
    }

    @Override
    public void setDelegationState(DelegationState delegationState) {
        this.delegationState = delegationState;
    }

    @Override
    public String getProcessInstanceId() {
        return this.processInstanceId;
    }

    @Override
    public String getExecutionId() {
        return this.executionId;
    }

    @Override
    public String getProcessDefinitionId() {
        return this.processDefinitionId;
    }

    @Override
    public String getCaseInstanceId() {
        return this.caseInstanceId;
    }

    @Override
    public void setCaseInstanceId(String caseInstanceId) {
        this.caseInstanceId = caseInstanceId;
    }

    @Override
    public String getCaseExecutionId() {
        return this.caseExecutionId;
    }

    @Override
    public String getCaseDefinitionId() {
        return this.caseDefinitionId;
    }

    @Override
    public Date getCreateTime() {
        return this.createTime;
    }

    @Override
    public String getTaskDefinitionKey() {
        return this.taskDefinitionKey;
    }

    @Override
    public Date getDueDate() {
        return this.dueDate;
    }

    @Override
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public Date getFollowUpDate() {
        return this.followUpDate;
    }

    @Override
    public void setFollowUpDate(Date followUpDate) {
        this.followUpDate = followUpDate;
    }

    @Override
    public void delegate(String userId) { }

    @Override
    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    @Override
    public String getParentTaskId() {
        return this.parentTaskId;
    }

    @Override
    public boolean isSuspended() {
        return suspensionState == SuspensionState.SUSPENDED.getStateCode();
    }

    @Override
    public String getFormKey() {
        return this.formKey;
    }

    @Override
    public String getTenantId() {
        return this.tenantId;
    }

    @Override
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
}
