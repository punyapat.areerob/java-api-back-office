package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.ExcelService;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.be.service.report.utils.Generater;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.LongSupplier;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
@RequiredArgsConstructor
public class ApplicationReportService {

    private final MemberRepository memberRepository;
    private final ExcelService excelService;
    private final MongoTemplate mongoTemplate;

    private static String fieldGroupPackage = "packageName";
    private static String fieldGroupNationality = "nationality";

    public Page<Application> getApplicationReport(BaseReportRequest arg, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<Application> listResult = getApplicationList(arg, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] getApplicationExcel(BaseReportRequest searchRequest, Pageable pageable) {
        List<String> headers = Arrays.asList("NO", "Card Name", "Name", "ByAgent", "Status", "Create at");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        final Query queryInfo = new Query();
        List<Application> listResult = getApplicationList(searchRequest, queryInfo);
        for (int i=0; i<listResult.size(); i++) {
            Application cellValue = listResult.get(i);
            if ( cellValue != null ) {
                String name = cellValue.getUserProfileForm().getTitleId().getName() + " " + cellValue.getUserProfileForm().getGivenName() + " " + cellValue.getUserProfileForm().getSurName();
                String cardName = cellValue.getCard() != null ? cellValue.getCard().getName() : "-";
                List<ExcelService.ExcelValue> cells = new ArrayList<>();
                cells.add(new ExcelService.ExcelValue(cellValue.getApplicationNo()));
                cells.add(new ExcelService.ExcelValue(cardName));
                cells.add(new ExcelService.ExcelValue(name));
                cells.add(new ExcelService.ExcelValue(cellValue.getAgentName()));
                cells.add(new ExcelService.ExcelValue(cellValue.getStatus().name()));
                cells.add(new ExcelService.ExcelValue(cellValue.getCreatedDate()));
                rows.add(cells);
            }
        }
        return excelService.generateExcelReport("MemberByPackage", headers, rows);
    }

    public Page<CountReportResponse> getApplicationByNationalReport(BaseReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<CountReportResponse> listResult = countApplicationByNational(searchRequest, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] getApplicationByNationalExcel(BaseReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query();
        List<CountReportResponse> listResult = countApplicationByNational(searchRequest, queryInfo);
        List<String> headers = Arrays.asList("Period", "Nationality", "Amount");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        for (int i=0; i<listResult.size(); i++) {
            CountReportResponse cellValue = listResult.get(i);
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(cellValue.getDateTimePeriod()));
            cells.add(new ExcelService.ExcelValue(cellValue.getName()));
            cells.add(new ExcelService.ExcelValue(cellValue.getAmount()));
            rows.add(cells);
        }
        return excelService.generateExcelReport("ApplicationByNation", headers, rows);
    }

    private  List<CountReportResponse> countApplicationByNational(BaseReportRequest searchRequest, Query queryInfo) {
        List<CountReportResponse> listResult = new ArrayList<>();
        List<Application> applications = getApplicationList(searchRequest, queryInfo);
        HashMap<String, HashMap<String, List<Application>>> groupTimePeriod = getGroupDataAndFiled(searchRequest, fieldGroupNationality, applications);
        for (String timePeriod : groupTimePeriod.keySet()) {
            HashMap<String, List<Application>> groupMemberShips = groupTimePeriod.get(timePeriod);
            for (String groupByFieldKey : groupMemberShips.keySet()) {
                List<Application> membershipList = groupMemberShips.get(groupByFieldKey);

                CountReportResponse countRecord = new CountReportResponse();
                countRecord.setAmount(membershipList.size());
                countRecord.setName(groupByFieldKey);
                countRecord.setDateTimePeriod(timePeriod);
                listResult.add(countRecord);
            }
        }
        return listResult;
    }

    public Page<ApplicationReportResponse> getApplicationTimePeriodReport(BaseReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query().with(pageable);
        List<ApplicationReportResponse> listResult = getApplicationTimePeriod(searchRequest, queryInfo);
        return PageableExecutionUtils.getPage(listResult, pageable, (LongSupplier) ()-> listResult.size());
    }

    public byte[] getApplicationTimePeriodExcel(BaseReportRequest searchRequest, Pageable pageable) {
        final Query queryInfo = new Query();
        List<ApplicationReportResponse> listResult = getApplicationTimePeriod(searchRequest, queryInfo);
        List<String> headers = Arrays.asList("Submit date", "Card Name", "Name", "ByAgent", "Nationality", "Country", "Status", "Updated");
        List<List<ExcelService.ExcelValue>> rows = new ArrayList<>();
        for (int i=0; i<listResult.size(); i++) {
            ApplicationReportResponse cellValue = listResult.get(i);
            Application application = cellValue.getApplication();
            String name = application.getUserProfileForm().getTitleId().getName() + " " + application.getUserProfileForm().getGivenName() + " " + application.getUserProfileForm().getSurName();
            String cardName = application.getCard() != null ? application.getCard().getName() : "-";
            String nationality = application.getPassportForm() != null && application.getPassportForm().getNationalityId() != null ? application.getPassportForm().getNationalityId().getName() : "-";
            String country = application.getAddressForm() != null && application.getAddressForm().getCountryId() != null ? application.getAddressForm().getCountryId().getName() : "-";
            List<ExcelService.ExcelValue> cells = new ArrayList<>();
            cells.add(new ExcelService.ExcelValue(cellValue.getDateTimePeriod()));
            cells.add(new ExcelService.ExcelValue(cardName));
            cells.add(new ExcelService.ExcelValue(name));
            cells.add(new ExcelService.ExcelValue(application.getAgentName()));
            cells.add(new ExcelService.ExcelValue(nationality));
            cells.add(new ExcelService.ExcelValue(country));
            cells.add(new ExcelService.ExcelValue(application.getStatus().name()));
            cells.add(new ExcelService.ExcelValue(application.getLastModifiedDate()));
            rows.add(cells);
        }
        return excelService.generateExcelReport("Application", headers, rows);
    }

    private List<ApplicationReportResponse> getApplicationTimePeriod(BaseReportRequest searchRequest, Query queryInfo) {
        List<ApplicationReportResponse> listResult = new ArrayList<>();
        List<Application> applicationQuery = getApplicationList(searchRequest, queryInfo);
        HashMap<String, List<Application>> groupTimePeriod = new HashMap<>();
        String groupByFormat = Generater.getGroupByFormat(searchRequest);
        for (Application application : applicationQuery) {
            ZonedDateTime zonedDateTime = application.getCreatedDate().atZone(ZoneId.systemDefault());
            String timePeriodKey = DateUtils.toDateString(zonedDateTime, groupByFormat);
            List<Application> applications = groupTimePeriod.get(timePeriodKey);
            if (applications == null)
                applications = new ArrayList<>();
            applications.add(application);
            groupTimePeriod.put(timePeriodKey, applications);
        }

        for (String timePeriodKey : groupTimePeriod.keySet()) {
            List<Application> applications = groupTimePeriod.get(timePeriodKey);
            for (Application application : applications) {
                ApplicationReportResponse appReport = new ApplicationReportResponse();
                appReport.setDateTimePeriod(timePeriodKey);
                appReport.setApplication(application);
                listResult.add(appReport);
            }
        }

        return listResult;
    }

    private List<Application> getApplicationList(BaseReportRequest arg, Query queryInfo) {
        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("created_date").gt(arg.getFrom()).lt(arg.getTo()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("created_date").gt(arg.getFrom()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("created_date").lt(arg.getTo()));
        }
        List<Application> memberships = mongoTemplate.find(queryInfo, Application.class);
        return memberships;
    }

    private HashMap<String, HashMap<String, List<Application>>> getGroupDataAndFiled(BaseReportRequest arg, String groupByField, List<Application> applications) {
        HashMap<String, HashMap<String, List<Application>>> groupTimePeriod = new HashMap<>();
        String groupByValue = Generater.getGroupByFormat(arg);
        for (Application application : applications) {
            ZonedDateTime zonedDateTime = application.getCreatedDate().atZone(ZoneId.systemDefault());
            String timePeriodKey = DateUtils.toDateString(zonedDateTime, groupByValue);
            String groupByFieldKey = getGroupByFieldKey(application, groupByField);

            HashMap<String, List<Application>> groupData = groupTimePeriod.get(timePeriodKey);
            if(groupData == null){
                groupData = new HashMap<>();
                groupData.put(groupByFieldKey, new ArrayList<>());
            }
            List<Application> entityList = groupData.get(groupByFieldKey);
            if(entityList == null)
                entityList = new ArrayList<>();
            entityList.add(application);

            groupData.put(groupByFieldKey, entityList);
            groupTimePeriod.put(timePeriodKey, groupData);
        }

        return groupTimePeriod;
    }

    private String getGroupByFieldKey(Application entity, String fieldGroupBy) {
        if ( fieldGroupNationality.equalsIgnoreCase(fieldGroupBy) ) {
            Optional<Member> member = memberRepository.findById(entity.getMemberId());
            if(member.isPresent())
                return member.get().getNationality().getName();
            else
                return "-";
        }else {
            return entity.getCard().getName();
        }
    }

}
