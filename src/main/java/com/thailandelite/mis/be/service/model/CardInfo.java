package com.thailandelite.mis.be.service.model;


import com.thailandelite.mis.model.domain.CardPrivilege;
import com.thailandelite.mis.model.domain.Card;
import lombok.Data;

import java.util.List;

@Data
public class CardInfo {
    private Card card;
    private List<CardPrivilege> cardPrivileges;
}
