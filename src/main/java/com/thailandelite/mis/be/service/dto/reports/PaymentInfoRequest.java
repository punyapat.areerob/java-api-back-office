package com.thailandelite.mis.be.service.dto.reports;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class PaymentInfoRequest {
    private ZonedDateTime from;
    private ZonedDateTime to;
}
