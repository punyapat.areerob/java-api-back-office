package com.thailandelite.mis.be.service.flow;

import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.BookingService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.InvoiceService;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import com.thailandelite.mis.model.domain.enumeration.IncidentType;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPIncidentService extends BPService<Booking> {

    private final MemberPaymentRepository memberPaymentRepository;
    private final MemberRepository memberRepository;
    private final CardRepository cardRepository;
    private final InvoiceRepository invoiceRepository;
    private final IncidentRepository incidentRepository;
    private final PaymentTransactionRepository paymentTransactionRepository;
    private final ProductModelRepository productModelRepository;
    private final BookingService bookingService;
    private final InvoiceService invoiceService;

    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPIncidentService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , MemberPaymentRepository memberPaymentRepository
        , MemberRepository memberRepository
        , CardRepository cardRepository
        , InvoiceRepository invoiceRepository
        , InvoiceService invoiceService
        , PaymentTransactionRepository paymentTransactionRepository
        , ApplicationContext applicationContext, BookingService bookingService, IncidentRepository incidentRepository, ProductModelRepository productModelRepository) {

        super(camundaService,
            new System_Evaluate_Incident_Type(caseActivityRepository),
            new MCC_record_issue(caseActivityRepository),
            new Supervisor_Check_Case(caseActivityRepository,invoiceService,incidentRepository ),
            new MCC_record_issue_compliment(caseActivityRepository),
            new Supervisor_Check_Case_compliment(caseActivityRepository ),
            new MCC_record_issue_complaint(caseActivityRepository),
            new Supervisor_Check_Case_complaint(caseActivityRepository ),
            new MCC_record_issue_record(caseActivityRepository),
            new Supervisor_Check_Case_record(caseActivityRepository )
        );

        this.memberPaymentRepository = memberPaymentRepository;
        this.memberRepository = memberRepository;
        this.cardRepository = cardRepository;
        this.invoiceRepository = invoiceRepository;
        this.paymentTransactionRepository = paymentTransactionRepository;
        this.bookingService = bookingService;
        this.incidentRepository  = incidentRepository;
        this.productModelRepository = productModelRepository;
        this.invoiceService = invoiceService;
    }

    @Override
    public String create(String processDefKey, Object id) {
        Booking booking = bookingService.getBookingById(id.toString());
        Incident incident = new Incident();
        incident.setIncidentType(IncidentType.BOOKING);
        incident.setMemberId(booking.getRequestMemberId());
        incident.setBookingId(booking.getId());
        incident.setBooking(booking);
        List<ProductModel> productModels = new ArrayList<>();
        for (BookingServiceMember bookingServiceMember : booking.getBookingServiceMembers()) {
            Optional<ProductModel> productModel = productModelRepository.findById(bookingServiceMember.getMemberId());
            if (productModel.isPresent())
                productModels.add(productModel.get());
        }
        incident.setProductModels(productModels);
        incidentRepository.save(incident);
        ProcessInstance process = super.startProcess(processDefKey, incident.getId());
        this.caseInfoService.createIncident(processDefKey, "System_Evaluate_Incident_Type", process,incident.getId());
        return "success";
    }

    public String createNoneBooking(String processDefKey, Incident incident) {
        incidentRepository.insert(incident);
        ProcessInstance process = super.startProcess(processDefKey,incident.getId());
        this.caseInfoService.createIncident(processDefKey, "System_Evaluate_Incident_Type", process,incident.getId());
        return "success";
    }

    public MemberPayment save(MemberPayment memberPayment) {
        memberPaymentRepository.save(memberPayment);
        return memberPayment;
    }

    public String uploadFile(String path, MultipartFile file) throws UploadFailException {
        FileService fileService = new FileService();
        return fileService.upload(path, file);
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);

            Incident incident = incidentRepository.findById(caseInfo.getEntityId()).get();
            Booking booking = null;
            if(incident.getBookingId()!=null) {
            booking =bookingService.getBookingById(incident.getBookingId());

            }
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .incident(incident).booking(booking)
                .build();
            dest.setData(data);
            return dest;
        });
    }
    @RequiredArgsConstructor
    private static class System_Evaluate_Incident_Type implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "System_Evaluate_Incident_Type";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {BOOKINGINCIDENT,COMPLIMENT,RECORD,COMPLAINT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case BOOKINGINCIDENT:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setIncidentType(IncidentType.BOOKING);
                    caseInfo.setTaskDefKey("MCC_record_issue");
                    break;
                case COMPLIMENT:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setIncidentType(IncidentType.COMPLIMENT);
                    caseInfo.setTaskDefKey("MCC_record_issue_compliment");
                    break;
                case RECORD:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setIncidentType(IncidentType.RECORD);
                    caseInfo.setTaskDefKey("MCC_record_issue_record");
                    break;
                case COMPLAINT:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setIncidentType(IncidentType.COMPLAINT);
                    caseInfo.setTaskDefKey("MCC_record_issue_complaint");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class MCC_record_issue implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "MCC_record_issue";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE,REMARK}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING_SUPERVISOR");
                    caseInfo.setTaskDefKey("Supervisor_Check_Case");
                    break;
                case REMARK:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Supervisor_Check_Case implements ITask {
        final CaseActivityRepository caseActivityRepository;
        final InvoiceService invoiceService;
        final IncidentRepository incidentRepository;
        public static final String taskDefKey = "Supervisor_Check_Case";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {MOVE,MEMBER_QUOTA,MEMBER_PENALTY,VENDOR,STAFF,CANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case MOVE:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue");
                    break;
                case MEMBER_QUOTA:
                    caseInfo.setStatus("MEMBER_QUOTA");
                    caseInfo.setTaskDefKey("");
                    break;
                case MEMBER_PENALTY:
                    caseInfo.setStatus("MEMBER_PENALTY");
                    caseInfo.setTaskDefKey("");
                    invoiceService.createPenaltyInvoice( incidentRepository.findById(caseInfo.getEntityId()).get().getBookingId());
                    break;
                case VENDOR:
                    caseInfo.setStatus("VENDOR");
                    caseInfo.setTaskDefKey("");
                    break;
                case STAFF:
                    caseInfo.setStatus("STAFF");
                    caseInfo.setTaskDefKey("");
                    break;
                case CANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class MCC_record_issue_compliment implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "MCC_record_issue_compliment";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE,REMARK}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case REMARK:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue_compliment");
                    break;
                case APPROVE:
                    caseInfo.setStatus("PENDING_SUPERVISOR");
                    caseInfo.setTaskDefKey("Supervisor_Check_Case_compliment");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Supervisor_Check_Case_compliment implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "Supervisor_Check_Case_compliment";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {MOVE,DONE,CANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case MOVE:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue_compliment");
                    break;
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case CANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }


    @RequiredArgsConstructor
    private static class MCC_record_issue_complaint implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "MCC_record_issue_complaint";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE,REMARK}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case REMARK:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue_complaint");
                    break;
                case APPROVE:
                    caseInfo.setStatus("PENDING_SUPERVISOR");
                    caseInfo.setTaskDefKey("Supervisor_Check_Case_complaint");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Supervisor_Check_Case_complaint implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "Supervisor_Check_Case_complaint";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {MOVE,DONE,CANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case MOVE:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue_complaint");
                    break;
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case CANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }




    @RequiredArgsConstructor
    private static class MCC_record_issue_record implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "MCC_record_issue_record";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE,REMARK}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case REMARK:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue_record");
                    break;
                case APPROVE:
                    caseInfo.setStatus("PENDING_SUPERVISOR");
                    caseInfo.setTaskDefKey("Supervisor_Check_Case_record");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

    @RequiredArgsConstructor
    private static class Supervisor_Check_Case_record implements ITask {
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "Supervisor_Check_Case_record";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {MOVE,DONE,CANCEL}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            switch (Action.valueOf(actionName)) {
                case MOVE:
                    caseInfo.setStatus("PENDING_MCC");
                    caseInfo.setTaskDefKey("MCC_record_issue_record");
                    break;
                case DONE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case CANCEL:
                    caseInfo.setStatus("CANCEL");
                    caseInfo.setTaskDefKey("");
                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }



    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);


        Incident incident = incidentRepository.findById(caseInfoDTO.getEntityId()).get();
        Booking booking = null;
        if(incident.getBookingId()!=null) {
            booking =bookingService.getBookingById(incident.getBookingId());

        }
        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .incident(incident).booking(booking)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }
}

