package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.repository.JobApplicationRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.ApplicationService;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.JobApplication;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class BPJobApplicationService extends  BPService<JobApplication> {

    private final JobApplicationRepository jobApplicationRepository;

    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPJobApplicationService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , JobApplicationRepository jobApplicationRepository
        , ApplicationContext applicationContext) {

        super(camundaService,
            new BPJobApplicationService.Pending_HR_Review(caseActivityRepository, jobApplicationRepository)
        );
        this.jobApplicationRepository = jobApplicationRepository;

    }

    @Override
    public Object create(String flowDefKey, Object object) {
        JobApplication application = (JobApplication) object;
        ProcessInstance process = super.startProcess(flowDefKey, application.getId());
        this.caseInfoService.createJobApplicationCase(flowDefKey, BPJobApplicationService.Pending_HR_Review.taskDefKey, process, application);
        return  application;
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        JobApplication jobApplication = jobApplicationRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .jobApplication(jobApplication)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<JobApplication> applications = jobApplicationRepository.findByIdIn(entityIds);

        ImmutableMap<String, JobApplication> idMap = Maps.uniqueIndex(applications, JobApplication::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            JobApplication application = idMap.get(caseInfo.getEntityId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .jobApplication(application)
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @RequiredArgsConstructor
    private static class Pending_HR_Review implements ITask {
        public static final String taskDefKey = "Pending_HR_Review";
        final CaseActivityRepository caseActivityRepository;
        final JobApplicationRepository jobApplicationRepository;

        public enum Action {APPROVE,REJECT}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPJobApplicationService.Pending_HR_Review.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPJobApplicationService.Pending_HR_Review.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("DONE");
                    caseInfo.setTaskDefKey("");
                    break;
                case REJECT:
                    caseInfo.setStatus("REJECT");
                    caseInfo.setTaskDefKey("");
                    break;

            }

        }
    }

}
