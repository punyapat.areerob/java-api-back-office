package com.thailandelite.mis.be.service;

import com.google.common.base.Verify;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.oned.EAN8Writer;
import com.thailandelite.mis.be.cdr.domain.Cdr;
import com.thailandelite.mis.be.cdr.domain.QueueLog;
import com.thailandelite.mis.be.cdr.repository.CdrRepository;
import com.thailandelite.mis.be.cdr.repository.QueueLogRepository;
import com.thailandelite.mis.be.domain.CallLog;
import com.thailandelite.mis.be.domain.CallLog.CallLogAction;
import com.thailandelite.mis.be.repository.MemberPhoneRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.dto.ReceiveCallStatus;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPhone;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

import static com.thailandelite.mis.be.service.migrate.MigrateUtils.zdt;

@Slf4j
@Service
@RequiredArgsConstructor
public class BarcodeService {

    public static byte[] generateCode128BarcodeImage(String barcodeText) throws Exception {
        Code128Writer barcodeWriter = new Code128Writer();
        BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.CODE_128, 500, 150);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "JPG", outputStream);
        return outputStream.toByteArray();
    }

    public byte[] generateBarcodeImage(String ref1, String ref2, String amount) throws Exception {
        String taxId = "0105553000237";
        String suffix = "00";
        String cr = "\r";
        String barcodeText = "|" + taxId + suffix + cr + ref1 + cr + ref2 + cr + amount;
        System.out.println("barcodeText: "+ barcodeText);
        System.out.println("length: "+ barcodeText.length());
        Verify.verify(barcodeText.length() <= 62, "barcodeText more than 62 digits");
        return generateCode128BarcodeImage(barcodeText);
    }
}
