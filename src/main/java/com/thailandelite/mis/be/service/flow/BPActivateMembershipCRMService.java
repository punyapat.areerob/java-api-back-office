package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.MemberService;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPayment;
import com.thailandelite.mis.model.domain.Membership;
import com.thailandelite.mis.model.domain.enumeration.PackageAction;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class BPActivateMembershipCRMService extends  BPService<Member> {

    private final MemberRepository memberRepository;
    private final MemberService memberService;
    private final MembershipRepository membershipRepository;
    private final CardRepository cardRepository;
    private final CardPrivilegeRepository cardPrivilegeRepository;
    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPActivateMembershipCRMService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , MemberRepository memberRepository
        , MembershipRepository membershipRepository
        , MemberService memberService
        , CardRepository cardRepository
        , CardPrivilegeRepository cardPrivilegeRepository
        , ApplicationContext applicationContext) {

        super(camundaService,
            new BPActivateMembershipCRMService.CRM_SET_ACTIVATE_DATE_DOC(caseActivityRepository, memberRepository , membershipRepository),
            new BPActivateMembershipCRMService.CRM_UPDATE_DOC_WELCOME_PACK(caseActivityRepository, memberRepository  , membershipRepository)
            );
        this.memberRepository = memberRepository;
        this.membershipRepository = membershipRepository;
        this.memberService = memberService;
        this.cardRepository = cardRepository;
        this.cardPrivilegeRepository = cardPrivilegeRepository;
    }


    @RequiredArgsConstructor
    private static class CRM_SET_ACTIVATE_DATE_DOC implements ITask {
        public static final String taskDefKey = "CRM_SET_ACTIVATE_DATE_DOC";
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;
        final MembershipRepository membershipRepository;
        public enum Action {DONE}

        public String name() {
            return taskDefKey;
        }

        public Enum<?>[] actions() {
            return BPActivateMembershipCRMService.CRM_SET_ACTIVATE_DATE_DOC.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPActivateMembershipCRMService.CRM_SET_ACTIVATE_DATE_DOC.Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setStatus("PENDING CRM UPDATE DOC");
                    caseInfo.setTaskDefKey("CRM_UPDATE_DOC_WELCOME_PACK");
                    break;
            }
        }
    }


    @RequiredArgsConstructor
    private static class CRM_UPDATE_DOC_WELCOME_PACK implements ITask {
        final CaseActivityRepository caseActivityRepository;
        final MemberRepository memberRepository;
        final MembershipRepository membershipRepository;

        public enum Action {DONE}

        public String name() {
            return "CRM_UPDATE_DOC_WELCOME_PACK";
        }

        public Enum<?>[] actions() {
            return BPActivateMembershipCRMService.CRM_UPDATE_DOC_WELCOME_PACK.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {

            switch (BPActivateMembershipCRMService.CRM_UPDATE_DOC_WELCOME_PACK.Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("COMPLETE");
                    break;
            }

        }
    }

    @Override
    public Object create(String flowDefKey, Object object ) {
        MemberPayment memberPayment = (MemberPayment) object;
        Membership membership = memberService.newMembership(memberPayment.getMemberId(),memberPayment.getCardId(),memberPayment.getInvoiceId());
        ProcessInstance process = super.startProcess(flowDefKey, membership.getId());
        this.caseInfoService.createMemberActivationCase(flowDefKey, BPActivateMembershipCRMService.CRM_SET_ACTIVATE_DATE_DOC.taskDefKey, process, membership
        ,"PENDING CRM SET ACTIVATE" ,memberPayment.getPackageAction()
        );
        return membership;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<Membership> members = membershipRepository.findByIdIn(entityIds);

        ImmutableMap<String, Membership> idMap = Maps.uniqueIndex(members, Membership::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            Membership membership = idMap.get(caseInfo.getEntityId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .membership(membership)
                .member(memberRepository.findById(membership.getMemberId()).get())
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        Membership membership = membershipRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        Member member = memberRepository.findById(membership.getMemberId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        //// TODO: 10/4/2021 AD call CardService
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder().member(member)
            .membership(membership).card(cardRepository.findById(membership.getCardId()).get())
            .privileges(cardPrivilegeRepository.findAllByCardId(membership.getCardId()))
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }


}
