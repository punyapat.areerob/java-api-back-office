package com.thailandelite.mis.be.service.flow;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.exception.UploadFailException;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.service.CaseInfoService;
import com.thailandelite.mis.be.service.core.FileService;
import com.thailandelite.mis.model.domain.enumeration.ApplicationStatus;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.model.domain.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPMembershipTransactionService extends BPService<MemberPayment> {

    private final MemberPaymentRepository memberPaymentRepository;
    private final MemberRepository memberRepository;
    private final CardRepository cardRepository;
    private final InvoiceRepository invoiceRepository;
    private final PaymentTransactionRepository paymentTransactionRepository;
    private final ApplicationRepository applicationRepository;
    private final BPActivateMembershipService bpActivateMembershipService;
    @Autowired
    protected CaseInfoService caseInfoService;

    private final ModelMapper modelMapper = new ModelMapper();

    public BPMembershipTransactionService(CamundaService camundaService
        , CaseActivityRepository caseActivityRepository
        , MemberPaymentRepository memberPaymentRepository
        , MemberRepository memberRepository
        , CardRepository cardRepository
        , InvoiceRepository invoiceRepository
        , PaymentTransactionRepository paymentTransactionRepository
        , BPActivateMembershipService bpActivateMembershipService
        , ApplicationContext applicationContext, ApplicationRepository applicationRepository) {

        super(camundaService,
            new FINverifyMembershiptransaction(caseActivityRepository, memberPaymentRepository),
            new SLSApproveMembershipTransaction(caseActivityRepository, memberPaymentRepository ,memberRepository ,bpActivateMembershipService, applicationRepository ),
            new UserSubmitVerificationPaymentReject(caseActivityRepository, memberPaymentRepository),
            new UserSubmitVerificationPaymentIncomplete(caseActivityRepository, memberPaymentRepository)
        );

        this.memberPaymentRepository = memberPaymentRepository;
        this.memberRepository = memberRepository;
        this.cardRepository = cardRepository;
        this.invoiceRepository = invoiceRepository;
        this.paymentTransactionRepository = paymentTransactionRepository;
        this.bpActivateMembershipService = bpActivateMembershipService;
        this.applicationRepository = applicationRepository;
    }

    @RequiredArgsConstructor
    private static class FINverifyMembershiptransaction implements ITask {
        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;

        public enum Action {APPROVE, REJECT, INCOMPLETE}

        public String name() {
            return "fin_verify_membership_transaction";
        }

        public Enum<?>[] actions() {
            return FINverifyMembershiptransaction.Action.values();
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (FINverifyMembershiptransaction.Action.valueOf(actionName)) {
                case APPROVE:
                    caseInfo.setStatus("PENDING SLS");
                    caseInfo.setTaskDefKey("sls_approve_membership_transaction");
                    break;
                case REJECT:
                    caseInfo.setStatus("PAYMENT REJECT");
                    caseInfo.setTaskDefKey("user_submit_verification_payment_reject");
                    break;
                case INCOMPLETE:
                    caseInfo.setStatus("PAYMENT INCOMPLETE");
                    caseInfo.setTaskDefKey("user_submit_verification_payment_incomplete");
                    break;
            }

            memberPaymentRepository.save(memberPayment);
        }
    }

    @RequiredArgsConstructor
    private static class UserSubmitVerificationPaymentReject implements ITask {
        public enum Action {RESUBMIT, CANCEL}

        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;


        public String name() {
            return "Pending_User_Resubmit";
        }


        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (UserSubmitVerificationPaymentReject.Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("PENDING FIN");
                    caseInfo.setTaskDefKey("fin_verify_membership_transaction");
                    break;
                case CANCEL:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("CANCELLED");
                    break;
            }


            memberPaymentRepository.save(memberPayment);
        }

        @Override
        public Enum<?>[] actions() {
            return UserSubmitVerificationPaymentReject.Action.values();
        }
    }

    @RequiredArgsConstructor
    private static class UserSubmitVerificationPaymentIncomplete implements ITask {
        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;

        public Application data;

        public enum Action {RESUBMIT, CANCEL}

        public String name() {
            return "user_submit_verification_payment_incomplete";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (UserSubmitVerificationPaymentIncomplete.Action.valueOf(actionName)) {
                case RESUBMIT:
                    caseInfo.setStatus("PENDING FIN");
                    caseInfo.setTaskDefKey("fin_verify_membership_transaction");
                    break;
                case CANCEL:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("CANCELLED");
                    break;
            }

            memberPaymentRepository.save(memberPayment);
        }

        @Override
        public Enum<?>[] actions() {
            return UserSubmitVerificationPaymentIncomplete.Action.values();
        }

    }

    @RequiredArgsConstructor
    private static class SLSApproveMembershipTransaction implements ITask {
        public Application data;
        final CaseActivityRepository caseActivityRepository;
        final MemberPaymentRepository memberPaymentRepository;
        final MemberRepository memberRepository;
        final BPActivateMembershipService bpActivateMembershipService;
        final ApplicationRepository applicationRepository;
        public enum Action {DONE}


        public String name() {
            return "sls_approve_membership_transaction";
        }

        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog) {
            MemberPayment memberPayment = memberPaymentRepository.findById(caseInfo.getEntityId()).orElseThrow(() -> new RuntimeException("Not found"));

            switch (SLSApproveMembershipTransaction.Action.valueOf(actionName)) {
                case DONE:
                    caseInfo.setTaskDefKey("");
                    caseInfo.setStatus("DONE");
                    Optional<Application> applicationOp = applicationRepository.findById(memberPayment.getApplicantId());
                    if(applicationOp.isPresent()) {
                        Application application = applicationOp.get();
                        application.setStatus(ApplicationStatus.ACTIVATE);
                        applicationRepository.save(application);
                    }

                    break;
            }

            bpActivateMembershipService.create("activate_membership_by_sales" , memberPayment);
            memberPaymentRepository.save(memberPayment);

        }

        @Override
        public Enum<?>[] actions() {
            return SLSApproveMembershipTransaction.Action.values();
        }
    }

    @Override
    public MemberPayment create(String processDefKey, Object object) {
        MemberPayment memberPayment = (MemberPayment) object;
        ProcessInstance process = super.startProcess(processDefKey, memberPayment.getId());
        memberPaymentRepository.insert(memberPayment);
        caseInfoService.createMemberPayment(processDefKey,process,memberPayment,"fin_verify_membership_transaction");

        return memberPayment;
    }

    public MemberPayment save(MemberPayment memberPayment) {
        memberPaymentRepository.save(memberPayment);
        return memberPayment;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        List<MemberPayment> applications = memberPaymentRepository.findByIdIn(entityIds);

        ImmutableMap<String, MemberPayment> idMap = Maps.uniqueIndex(applications, MemberPayment::getId);
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            MemberPayment memberPayment = idMap.get(caseInfo.getEntityId());
            Member member = memberRepository.findById(memberPayment.getMemberId())
                .orElseThrow(() -> new RuntimeException("Not found"));

            Invoice invoice = invoiceRepository.findById(memberPayment.getInvoiceId()).orElseThrow(() -> new RuntimeException("Not found"));
            Card card = cardRepository.findById(memberPayment.getCardId()).orElseThrow(() -> new RuntimeException("Not found"));
            List<PaymentTransaction> paymentTransactions = paymentTransactionRepository.findByInvoiceId(memberPayment.getInvoiceId());
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .memberPayment(memberPayment)
                .member(member)
                .invoice(invoice)
                .card(card)
                .paymentTransactions(paymentTransactions)
                .build();
            dest.setData(data);
            return dest;
        });
    }

    public byte[] downloadFile(String path) throws Exception {
        FileService fileService = new FileService();
        return fileService.getFile(path);
    }

    public String uploadFile(String path, MultipartFile file) throws UploadFailException {
        FileService fileService = new FileService();
        return fileService.upload(path, file);
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);

        MemberPayment memberPayment = memberPaymentRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        Member member = memberRepository.findById(memberPayment.getMemberId())
            .orElseThrow(() -> new RuntimeException("Not found"));

        Invoice invoice = invoiceRepository.findById(memberPayment.getInvoiceId()).orElseThrow(() -> new RuntimeException("Not found"));

        Card card  = cardRepository.findById(memberPayment.getCardId()).orElseThrow(() -> new RuntimeException("Not found"));

        List<PaymentTransaction> paymentTransactions = paymentTransactionRepository.findByInvoiceId(memberPayment.getInvoiceId());

        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .memberPayment(memberPayment)
            .member(member)
            .invoice(invoice)
            .card(card)
            .paymentTransactions(paymentTransactions)
            .build();

        caseInfoDTO.setData(data);

        return caseInfoDTO;
    }
}
