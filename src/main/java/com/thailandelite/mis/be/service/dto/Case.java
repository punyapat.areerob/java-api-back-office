package com.thailandelite.mis.be.service.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.thailandelite.mis.be.domain.enumeration.StepState;
import lombok.Data;

import java.time.Instant;

@Data
public class Case {
	private String id;
    private String processId;
    private String processDefKey;
    private String currentTaskId;
    private String currentTaskDefKey;
	private String status;
	private String remark;

}
