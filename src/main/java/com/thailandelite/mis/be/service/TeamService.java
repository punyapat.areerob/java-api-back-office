package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.domain.User;
import com.thailandelite.mis.be.repository.TeamRepository;
import com.thailandelite.mis.be.repository.UserRepository;
import com.thailandelite.mis.be.service.core.ADService;
import com.thailandelite.mis.model.domain.master.Team;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class TeamService {
    private final ADService adService;
    private final TeamRepository teamRepository;
    private final UserRepository userRepository;

    public List<User> fetchLdapUsers() {
        List<User> users = new ArrayList<>();

        List<Team> teams = teamRepository.findAll();
        for (Team team : teams) {
            try {
                List<ADService.MemberAD> adUsers = adService.getGroupUsers("CN=" + team.getAdPath());
                for (ADService.MemberAD groupUser : adUsers) {
                    if (userRepository.existsByEmail(groupUser.getEmail()).equals(false)) {
                        User user = new User();
                        user.setEmail(groupUser.getEmail());
                        user.setTeam(team.getId());
                        String dn = groupUser.getDn();
                        user.setFirstName(dn.substring(dn.indexOf("=") + 1, dn.indexOf(" ")));
                        user.setLastName(dn.substring(dn.indexOf(" ") + 1, dn.indexOf(",")));
                        user.setLoginType(User.LoginType.LDAP);

                        log.debug("Create new user : {}", user);
                        userRepository.save(user);
                        users.add(user);
                    }
                }
            } catch (Exception e) {
                log.warn("Search ldap fail", e);
            }
        }

        return users;
    }
}
