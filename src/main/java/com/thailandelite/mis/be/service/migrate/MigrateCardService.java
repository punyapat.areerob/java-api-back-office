package com.thailandelite.mis.be.service.migrate;

import com.thailandelite.mis.model.domain.Privilege;
import com.thailandelite.mis.be.repository.CardPrivilegeRepository;
import com.thailandelite.mis.be.repository.CardRepository;
import com.thailandelite.mis.be.repository.PrivilegeRepository;
import com.thailandelite.mis.migrate.PackageItemDB;
import com.thailandelite.mis.migrate.PrivilegeDB;
import com.thailandelite.mis.migrate.card.CardDB;
import com.thailandelite.mis.model.domain.Card;
import com.thailandelite.mis.model.domain.CardPrivilege;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class MigrateCardService {
    public final CardRepository cardRepository;
    public final CardPrivilegeRepository cardPrivilegeRepository;
    public final PrivilegeRepository privilegeRepository;

    public void card(List<CardDB> srclist){
        List<Card> list = srclist.stream().map(this::map).collect(Collectors.toList());
        cardRepository.deleteAll();
        cardRepository.saveAll(list);
    }

    public void cardPrivilege(List<PackageItemDB> srclist){
        List<CardPrivilege> list = srclist.stream().map(this::map).collect(Collectors.toList());
        cardPrivilegeRepository.deleteAll();
        cardPrivilegeRepository.saveAll(list);
    }

    public void privilege(List<PrivilegeDB> srclist){
        List<Privilege> list = srclist.stream().map(this::map).collect(Collectors.toList());
        privilegeRepository.deleteAll();
        privilegeRepository.saveAll(list);
    }


    private  Privilege map(PrivilegeDB src) {
        Privilege dest = new Privilege();
//        dest.setId(src.getPRIVILEGE_ID());
//        dest.setName(src.getPRIVILEGE_NAME_EN());
//        dest.setPrivilegeGroupId(src.getPRIVILEGE_GROUP_ID());
//        dest.setPrivilegeGroupName(src.getPRIVILEGE_GROUP_NAME_EN());
//        dest.setPrivilegeCode(src.getPRIVILEGE_CODE());
//        dest.setRemark(src.getPRIVILEGE_CODE());
//        dest.setRecordStatus(src.getRECORD_STATUS());
//        dest.setShowInDataJA(src.getSHOW_IN_ADD_JA());
//        dest.setCreatedBy(src.getCREATE_USER());
//        dest.setCreatedDate(src.getCREATE_DATE());
//        dest.setUpdatedBy(src.getLAST_USER());
//        dest.setUpdatedDate(src.getLAST_DATE());

        return dest;
    }


    private CardPrivilege map(PackageItemDB src) {
        CardPrivilege dest = new CardPrivilege();
//        dest.setId(String.valueOf(src.getPACKAGE_ITEM_ID()));
//        dest.setCardId(String.valueOf(src.getPACKAGE_ID()));
//        dest.setCardName(src.getPACKAGE_NAME_EN());
//        dest.setCardStartDate(src.getSTART_DATE());
//        dest.setCardEndDate(src.getEND_DATE());
//        dest.setCardTimesName(src.getPACKAGE_TIMES_NAME_EN());
//        dest.setPrivilegeId(String.valueOf(src.getPRIVILEGE_ID()));
//        dest.setPrivilegeName(src.getPRIVILEGE_NAME_EN());
//        dest.setCardPrivilegeQuantity(src.getPACKAGE_ITEM_QUANTITY());
//        dest.setQuota(Integer.valueOf(src.getPACKAGE_ITEM_POINT()));
//        dest.setCardPrivilegeOrder(Integer.valueOf(src.getPACKAGE_ITEM_ORDER()));
//        dest.setQuotaHide(src.getQUAOTA_HIDE());
//        dest.setShowQuotaStatus(src.getSHOW_QUOTA_STATUS());
////        dest.setValidityPeriod(CardPrivilegeValidity.YEARLY);
//        dest.setRemark(src.getREMARK());
//        dest.setRecordStatus(src.getRECORD_STATUS());
//        dest.setCreatedBy(src.getCREATE_USER());
//        dest.setCreatedDate(src.getCREATE_DATE().toInstant());
//        dest.setLastModifiedBy(src.getLAST_USER());
//        dest.setLastModifiedDate(src.getLAST_DATE().toInstant());

        return dest;
    }

    private Card map(CardDB src) {
        Card dest = new Card();
       /* dest.setId(src.getPACKAGE_ID());
        dest.setName(src.getPACKAGE_NAME_EN());
        dest.setAbbreviation(src.getID_PACKAGE());
        dest.setCode(src.getPACKAGE_CODE());
        dest.setStatus(src.getPUBLICE_STATUS().equals("T") ?  CardStatus.ACTIVE : CardStatus.INACTIVE);
        dest.setPhoto(src.getPACKAGE_LARGE_PICTURE());
        dest.setMemberContactCenter(src.getMEMBER_CONTACT_CENTER().equals("Y"));
        dest.setGovernmentConcierges(src.getGOVERNMENT_CONCIERGES().equals("Y"));
        dest.setNinetyReportDay(src.getNINETY_DAY_REPORT().equals("Y"));
        dest.setActiveStartDate(src.getSTART_DATE());
        dest.setActiveEndDate(src.getEND_DATE());
//        dest.setMemberInformationAgreement("");
//        dest.setMemberInformationAgreementFile("");
        dest.setRemark(src.getREMARK());
        dest.setRecordStatus(src.getRECORD_STATUS());
        dest.setCreatedBy(src.getCREATE_USER());
        dest.setCreatedDate(src.getCREATE_DATE());
        dest.setUpdatedBy(src.getLAST_USER());
        dest.setUpdatedDate(src.getLAST_DATE());
//        dest.setPhyCard("");
        dest.setMembershipType("");
        dest.setValidityYear(0);
        dest.setMembershipFee(new MembershipFeeSetting());
        dest.setTransferSetting(new TransferSetting());
        dest.setAdditionalFamilyMemberSetting(new AdditionalFamilyMemberSetting());
        dest.setAgeCondition(new AgeCondition());
        dest.setAnnualFeeSetting(new AnnualFeeSetting());
        dest.setVisaSetting(new VisaSetting());
        dest.setUpgradeSettings(Lists.newArrayList());*/

        return dest;
    }

}
