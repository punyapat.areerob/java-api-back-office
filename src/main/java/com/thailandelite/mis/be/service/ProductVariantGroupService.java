package com.thailandelite.mis.be.service;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.be.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductVariantGroupService {
    private final ProductVariantGroupRepository productVariantGroupRepository;
    private final ProductVariantValueRepository productVariantValueRepository;

    public void deleteVendor(String id ,String productVariantGroupId) {
        productVariantGroupRepository.deleteById(productVariantGroupId);
    }
    public ProductVariantGroup getById(String productVariantGroupId) {
        ProductVariantGroup group = productVariantGroupRepository.findById(productVariantGroupId).orElseThrow(() -> new NotFoundException("ProductVariantGroup"));
        List<ProductVariantValue> values = productVariantValueRepository.findByProductVariantGroupId(productVariantGroupId);
        group.setProductVariantValue(values);
        return group;
    }

}
