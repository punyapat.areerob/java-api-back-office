package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.PaymentTransaction;
import com.thailandelite.mis.model.domain.enumeration.PaymentMethod;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import lombok.Data;

@Data
public class PaymentInfoResponse {
    private PaymentTransaction paymentTransaction;
    private PaymentMethod paymentMethod;
    private PaymentType payType;
    private Invoice invoice;
    private Member member;
}
