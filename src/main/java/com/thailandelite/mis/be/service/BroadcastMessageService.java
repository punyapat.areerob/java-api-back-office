package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.domain.BroadcastMessage;
import com.thailandelite.mis.be.repository.BroadcastMessageRepository;
import com.thailandelite.mis.be.service.helper.QueryHelper;
import com.thailandelite.mis.be.web.rest.BroadcastMessageResource.BroadcastMessageQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;

import static com.thailandelite.mis.be.domain.enumeration.BroadcastMessageStatus.ACTIVE;
import static com.thailandelite.mis.be.domain.enumeration.BroadcastMessageStatus.EXPIRED;
import static com.thailandelite.mis.be.service.helper.QueryHelper.containing;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.util.StringUtils.hasText;

@Slf4j
@Service
@RequiredArgsConstructor
public class BroadcastMessageService {
    private final BroadcastMessageRepository broadcastMessageRepository;
    private final MongoTemplate mongoTemplate;

    public List<BroadcastMessage> getLiveMessage(String teamId) {
        return broadcastMessageRepository.findAllByActivePeriod(teamId, Instant.now(), ACTIVE);
    }

    public void taskExpireMessage() {
        broadcastMessageRepository.findAllByToBeforeAndStatus(ZonedDateTime.now(), ACTIVE)
            .forEach(broadcastMessage -> {
                broadcastMessage.setStatus(EXPIRED);
                broadcastMessageRepository.save(broadcastMessage);
            });
    }

    public List<BroadcastMessage> search(BroadcastMessageQuery example) {
        Query query = new QueryHelper()
            .and(example.getSubject(), (value) -> where("subject").regex(containing(value)))
            .and(example.getMessage(), (value) -> where("message").regex(containing(value)))
            .and(example.getStatus(), (value) -> where("status").is(value))
            .and(example.getCreatorTeamId(), (value) -> where("creatorTeamId").is(value))
            .build();

        return mongoTemplate.find(query, BroadcastMessage.class);
    }
}
