package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.booking.enums.BookingFlowDefinitionKey;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class BookingReportRequest {
    private static final long serialVersionUID = 1L;
    private ZonedDateTime from;
    private ZonedDateTime to;
    private TimePeriod timePeriod;
    private String vendorName;
    private BookingFlowDefinitionKey serviceType;

}
