package com.thailandelite.mis.be.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;

import static org.springframework.http.HttpMethod.POST;


@Slf4j
@Service
public class ElastixService {

    @Value("") // Elastix url
    public String url;

    @Value("") // generatedAuth-Token
    public String token;

    @Value("") // extension
    public String extension;

    @Value("") // queue (skill)
    public String queueSkill;

    private final RestTemplate rt = new RestTemplate();

    public String currentToken;

    @NotNull
    private HttpHeaders getHttpHeaders() {
        if(currentToken == null || currentToken.isEmpty())
            currentToken = this.token;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer " + currentToken);
        return headers;
    }

    public QueueExternal addQueue() {
        log.debug("Add Queue!!");
        HttpHeaders headers = getHttpHeaders();
        HashMap<String, String> formReq = new HashMap<>();
        formReq.put("extension", extension);
        formReq.put("queue", queueSkill);
        HttpEntity<?> httpEntity = new HttpEntity<>(formReq, headers);
        QueueExternal response = this.rt.exchange(url + "/api_elastix/index.php/queueExternals/add", POST, httpEntity, QueueExternal.class).getBody();
        assert response != null;
        log.debug("response is {}", response.elastixResponseDTO);
        currentToken = response.token;
        return response;
    }

    public QueueExternal removeQueue() {
        log.debug("Remove Queue!!");
        HttpHeaders headers = getHttpHeaders();
        HashMap<String, String> formReq = new HashMap<>();
        formReq.put("extension", extension);
        formReq.put("queue", queueSkill);
        HttpEntity<?> httpEntity = new HttpEntity<>(formReq, headers);
        QueueExternal response = this.rt.exchange(url + "/api_elastix/index.php/queueExternals/remove", POST, httpEntity, QueueExternal.class).getBody();
        assert response != null;
        log.debug("response is {}", response.elastixResponseDTO);
        currentToken = response.token;
        return response;
    }

    public QueueExternal pauseQueue() {
        log.debug("Pause Queue!!");
        HttpHeaders headers = getHttpHeaders();
        HashMap<String, String> formReq = new HashMap<>();
        formReq.put("extension", extension);
        formReq.put("queue", queueSkill);
        HttpEntity<?> httpEntity = new HttpEntity<>(formReq, headers);
        QueueExternal response = this.rt.exchange(url + "/api_elastix/index.php/queueExternals/pause", POST, httpEntity, QueueExternal.class).getBody();
        assert response != null;
        log.debug("response is {}", response.elastixResponseDTO);
        currentToken = response.token;
        return response;
    }

    public QueueExternal unpauseQueue() {
        log.debug("Unpause Queue!!");
        HttpHeaders headers = getHttpHeaders();
        HashMap<String, String> formReq = new HashMap<>();
        formReq.put("extension", extension);
        formReq.put("queue", queueSkill);
        HttpEntity<?> httpEntity = new HttpEntity<>(formReq, headers);
        QueueExternal response = this.rt.exchange(url + "/api_elastix/index.php/queueExternals/unpause", POST, httpEntity, QueueExternal.class).getBody();
        assert response != null;
        log.debug("response is {}", response.elastixResponseDTO);
        currentToken = response.token;
        return response;
    }


    public static class QueueExternal {

        private String elastixResponseDTO;

        private String token;
    }

}



