package com.thailandelite.mis.be.service.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class CardDTO implements Serializable {

    private String id;
}
