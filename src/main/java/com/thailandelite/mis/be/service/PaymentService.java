package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.PaymentTransactionRepository;
import com.thailandelite.mis.be.service.exception.NotFoundException;
import com.thailandelite.mis.be.web.rest.flows.MemberPaymentBySLSResource;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.Invoice;
import com.thailandelite.mis.model.domain.MemberPayment;
import com.thailandelite.mis.model.domain.PaymentTransaction;
import com.thailandelite.mis.be.repository.InvoiceRepository;
import com.thailandelite.mis.model.domain.enumeration.InvoiceStatus;
import com.thailandelite.mis.model.domain.enumeration.PaymentMethod;
import com.thailandelite.mis.model.domain.enumeration.PaymentTransactionStatus;
import com.thailandelite.mis.model.domain.enumeration.PaymentType;
import com.thailandelite.mis.model.dto.request.OnlinePaymentRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentService {
    private final InvoiceRepository invoiceRepository;
    private final PaymentTransactionRepository paymentTransactionRepository;
    private final MemberPaymentBySLSResource memberPaymentBySLSResource;
    private final ApplicationRepository applicationRepository;

    public void createPayment() {

    }

    public void onPaymentReceived() {
    }

    public void onPaymentConfirmed() {
    }

    public void onPaymentRejected() {
    }

    public List<PaymentTransaction> createPaymentTransaction(String invoiceId, OnlinePaymentRequest request) {
        List<PaymentTransaction> paymentTransactions = new ArrayList<>();
        Invoice invoice = invoiceRepository.findById(invoiceId).orElseThrow(() -> new NotFoundException("Not found invoices:" + invoiceId));
        if(!request.getIsSplit()){
            Float amount = invoice.getTotalAmountIncVat();
            paymentTransactions.add(createPaymentTransaction(invoiceId, PaymentMethod.ONLINE, amount, ""));
        }else{
            int numberSplit = request.getSplitAmount();
            Float amount = invoice.getTotalAmountIncVat()/numberSplit;
            for (int i=0;i<numberSplit;i++){
                paymentTransactions.add(createPaymentTransaction(invoiceId, PaymentMethod.ONLINE, amount, ""));
            }
        }
        return paymentTransactions;
    }

    public PaymentTransaction updatePaymentTransaction(String invoiceId, PaymentTransaction paymentTransaction) {
        PaymentTransaction paymentTransactionDB = paymentTransactionRepository.findById(paymentTransaction.getId()).orElseThrow(()->new NotFoundException("Not found payment transaction."));
        paymentTransactionDB.setStatus(paymentTransaction.getStatus());
        return paymentTransactionRepository.save(paymentTransactionDB);
    }

    public Invoice uploadPayslip(String id, String path) {
        Invoice invoice = invoiceRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found invoices:" + id));
        createPaymentTransaction(id, PaymentMethod.OFFLINE, 0.0f, path);

        List<PaymentTransaction> oldTransactions = paymentTransactionRepository.findByInvoiceId(id);
        if(PaymentType.MEMBERSHIP.equals(invoice.getPayType()) ) {
            if(oldTransactions != null || oldTransactions.size() <= 0 ){
                MemberPayment memberPayment = new MemberPayment();
                memberPayment.setMemberId(invoice.getMemberId());
                memberPayment.setApplicantId(invoice.getDocumentId());
                Application application = applicationRepository.findById(invoice.getDocumentId()).orElseThrow(() -> new NotFoundException("Not found Application:" + invoice.getDocumentId()));
                memberPayment.setCardId(application.getCard().getId());
                memberPayment.setInvoiceId(invoice.getId());
                memberPaymentBySLSResource.createMemberPayment(memberPayment);

                invoice.setStatus(InvoiceStatus.VERIFY);
                invoiceRepository.save(invoice);
            }else{

            }

        }else if(PaymentType.ANNUALFEE.equals(invoice.getPayType())){

        }else if(PaymentType.PENALTY.equals(invoice.getPayType())){

        }
        return invoice;
    }
    public Invoice completeOnlineCharge(String transId) {
        PaymentTransaction transaction = paymentTransactionRepository.findById(transId).orElseThrow(() -> new NotFoundException("Not found transaction"));
        Invoice invoice = invoiceRepository.findById(transaction.getInvoiceId()).orElseThrow(() -> new NotFoundException("Not found invoices:" + transaction.getInvoiceId()));
        if(PaymentType.MEMBERSHIP.equals(invoice.getPayType()) ) {
            MemberPayment memberPayment = new MemberPayment();
            memberPayment.setMemberId(invoice.getMemberId());
            memberPayment.setApplicantId(invoice.getDocumentId());
            Application application = applicationRepository.findById(invoice.getDocumentId()).orElseThrow(() -> new NotFoundException("Not found Application:" + invoice.getDocumentId()));
            memberPayment.setCardId(application.getCard().getId());
            memberPayment.setInvoiceId(invoice.getId());
            memberPaymentBySLSResource.createMemberPayment(memberPayment);
            invoice.setStatus(InvoiceStatus.VERIFY);
            invoiceRepository.save(invoice);
        }else if(PaymentType.ANNUALFEE.equals(invoice.getPayType())){

        }else if(PaymentType.PENALTY.equals(invoice.getPayType())){

        }
        return invoice;
    }
    private PaymentTransaction createPaymentTransaction(String id,  PaymentMethod paymentMethod, Float amount, String path) {
        PaymentTransaction newTransaction = new PaymentTransaction();
        String transactionNo = this.generateTransactionNo();
        newTransaction.setInvoiceId(id);
        newTransaction.setTransactionNo(transactionNo);
        newTransaction.setStatus(PaymentTransactionStatus.UNPAID);
        newTransaction.setPaymentMethod(paymentMethod);
        newTransaction.setAmount(amount);
        newTransaction.setSlipPath(path);
        newTransaction.setGatewayTransactionNo("GW_"+transactionNo);
        newTransaction.setCreatedDate(ZonedDateTime.now().toInstant());
        newTransaction.setLastModifiedDate(ZonedDateTime.now().toInstant());
        paymentTransactionRepository.save(newTransaction);
        return newTransaction;
    }

    private String generateTransactionNo(){
        return String.valueOf(System.currentTimeMillis()).substring(1, 13);
    }

}
