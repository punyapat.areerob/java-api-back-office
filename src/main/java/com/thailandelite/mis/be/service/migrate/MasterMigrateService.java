package com.thailandelite.mis.be.service.migrate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.web.rest.MISResource;
import com.thailandelite.mis.migrate.master.*;
import com.thailandelite.mis.model.domain.BusinessIndustry;
import com.thailandelite.mis.model.domain.Nationality;
import com.thailandelite.mis.model.domain.Occupation;
import com.thailandelite.mis.model.domain.master.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class MasterMigrateService {
    private final TitleRepository titleRepository;
    private final BloodTypeRepository bloodTypeRepository;
    private final ReligionRepository religionRepository;
    private final NationalitiesRepository nationalitiesRepository;
    private final OccupationRepository occupationRepository;
    private final BusinessIndustryRepository businessIndustryRepository;
    private final AccommodationTypeRepository accommodationTypeRepository;
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;
    private final ProvinceRepository provinceRepository;
    private final DistrictRepository districtRepository;
    private final SubDistrictRepository subDistrictRepository;
    private final RelationshipRepository relationshipRepository;
    private final StateRepository stateRepository;

    public void title(List<TitleDB> srclist) {
        List<Title> list = srclist.stream().map(this::map).collect(Collectors.toList());
        titleRepository.deleteAll();
        titleRepository.saveAll(list);
    }

    public void bloodType(List<BloodTypeDB> srclist) {
        List<BloodType> list = srclist.stream().map(this::map).collect(Collectors.toList());
        bloodTypeRepository.deleteAll();
        bloodTypeRepository.saveAll(list);
    }

    public void religion(List<ReligionDB> srclist) {
        List<Religion> list = srclist.stream().map(this::map).collect(Collectors.toList());
        religionRepository.deleteAll();
        religionRepository.saveAll(list);
    }

    public void nationalities(List<NationalitiesDB> srclist) {
        List<Nationality> list = srclist.stream().map(this::map).collect(Collectors.toList());
        nationalitiesRepository.deleteAll();
        nationalitiesRepository.saveAll(list);
    }

    public void occupation(List<OccupationDB> srclist) {
        List<Occupation> list = srclist.stream().map(this::map).collect(Collectors.toList());
        occupationRepository.deleteAll();
        occupationRepository.saveAll(list);
    }

    public void businessIndustry(List<BusinessIndustryDB> srclist) {
        List<BusinessIndustry> list = srclist.stream().map(this::map).collect(Collectors.toList());
        businessIndustryRepository.deleteAll();
        businessIndustryRepository.saveAll(list);
    }

    public void accommodation(List<AccommodationTypeDB> srclist) {
        List<AccommodationType> list = srclist.stream().map(this::map).collect(Collectors.toList());
        accommodationTypeRepository.deleteAll();
        accommodationTypeRepository.saveAll(list);
    }

    public void country(List<CountryDB> srclist) {
        List<Country> list = srclist.stream().map(this::map).collect(Collectors.toList());
        countryRepository.deleteAll();
        countryRepository.saveAll(list);
    }

    @Async
    public void city(InputStream targetStream) throws IOException {
        AtomicInteger i = new AtomicInteger();
        cityRepository.deleteAll();
        List<City> list = new ArrayList<>(200);
        MISResource.readFileLargeFile(targetStream, (mapper, parser) -> {
            CityDB cityDB = mapper.readValue(parser, CityDB.class);
            list.add(map(cityDB));
            if(list.size() == 4000){
                log.info("Saving chunk: {}" , i.incrementAndGet());
                cityRepository.saveAll(list);
                list.clear();
            }
        });
    }

    public void province(List<ProvinceDB> srclist) {
        List<Province> list = srclist.stream().map(this::map).collect(Collectors.toList());
        provinceRepository.deleteAll();
        provinceRepository.saveAll(list);
    }

    public void district(List<DistrictDB> srclist) {
        List<District> list = srclist.stream().map(this::map).collect(Collectors.toList());
        districtRepository.deleteAll();
        districtRepository.saveAll(list);
    }

    public void subDistrict(List<SubDistrictDB> srclist) {
        List<SubDistrict> list = srclist.stream().map(this::map).collect(Collectors.toList());
        subDistrictRepository.deleteAll();
        subDistrictRepository.saveAll(list);
    }

    public void relationship(List<RelationshipDB> srclist) {
        List<Relationship> list = srclist.stream().map(this::map).collect(Collectors.toList());
        relationshipRepository.deleteAll();
        relationshipRepository.saveAll(list);
    }

    public void state(List<StateDB> srclist) {
        List<State> list = srclist.stream().map(this::map).collect(Collectors.toList());
        stateRepository.deleteAll();
        stateRepository.saveAll(list);
    }

    private Title map(TitleDB src) {
        Title title = new Title();
        title.setId(src.getTITLEID().toString());
        title.setName(src.getTITLENAMEEN());
        title.setPublicStatus(src.getPUBLICESTATUS().equals("T"));
        title.setActive(src.rECORDSTATUS.equals("N"));
        return title;
    }

    private BloodType map(BloodTypeDB src) {
        BloodType bloodType = new BloodType();
        bloodType.setId(src.getBLOODTYPEID().toString());
        bloodType.setName(src.bLOODTYPENAME);
        bloodType.setActive(src.rECORDSTATUS.equals("N"));
        return bloodType;
    }

    private Religion map(ReligionDB src) {
        Religion dest = new Religion();
        dest.setId(src.getRELIGIONID().toString());
        dest.setName(src.getRELIGIONNAMEEN());
        dest.setPublicStatus(src.getPUBLICESTATUS().equals("T"));
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private Nationality map(NationalitiesDB src) {
        Nationality dest = new Nationality();
        dest.setId(src.getNATIONALITYID().toString());
        dest.setName(src.getNATIONALITYNAMEEN());
        dest.setCountyCode(src.getCOUNTRYCODE());
        dest.setCountyName(src.getCOUNTRYNAME());
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private Occupation map(OccupationDB src) {
        Occupation dest = new Occupation();
        dest.setId(src.getOCCUPATIONID().toString());
        dest.setName(src.getOCCUPATIONNAMEEN());
        dest.setPublicStatus(src.getPUBLICESTATUS().equals("T"));
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private BusinessIndustry map(BusinessIndustryDB src) {
        BusinessIndustry dest = new BusinessIndustry();
        dest.setId(src.getNATUREOFBUSINESSID().toString());
        dest.setName(src.nATUREOFBUSINESSNAMEEN);
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private AccommodationType map(AccommodationTypeDB src) {
        AccommodationType dest = new AccommodationType();
        dest.setId(src.getADDRESSTYPEID().toString());
        dest.setName(src.aDDRESSTYPENAMEEN);
        dest.setNameTH(src.aDDRESSTYPENAMETH);
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private Country map(CountryDB src) {
        Country dest = new Country();
        dest.setId(src.getCOUNTRYID().toString());
        dest.setName(src.getCOUNTRYNAMEEN());
        dest.setCountryCode(src.getCOUNTRYCODE());
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private City map(CityDB src) {
        City dest = new City();
        dest.setId(src.getCITYID().toString());
        dest.setStateId(src.sTATEID != null ? src.sTATEID.toString() : "");
        dest.setCountryId(src.cOUNTRYID != null ? src.cOUNTRYID.toString() : "");
        dest.setCountryCode(src.cITYCODE);
        dest.setCityCode(src.cITYCODE);
        dest.setCityNameEn(src.cITYNAMEEN);
        dest.setCityNameTH(src.cITYNAMETH);
        dest.setProvinceId(src.pROVINCEID != null ? src.pROVINCEID.toString() : "");
        dest.setDistrictId(src.dISTRICTID != null ? src.dISTRICTID.toString() : "");

        dest.setActive(src.getRECORDSTATUS().equals("N"));
        return dest;
    }

    private Province map(ProvinceDB src) {
        Province dest = new Province();
        dest.setId(src.getPROVINCEID().toString());
        dest.setCountryId(src.getCOUNTRYID().toString());
        dest.setCode(src.cOUNTRYID.toString());
        dest.setNameEN(src.pROVINCENAMEEN);
        dest.setNameTH(src.pROVINCENAMETH);
        dest.setActive(src.rECORDSTATUS.equals("N"));
        return dest;
    }

    private District map(DistrictDB src) {
        District dest = new District();
        dest.setId(src.getDISTRICTID().toString());
        dest.setNameEN(src.getDISTRICTNAMEEN());
        dest.setNameTH(src.getDISTRICTNAMETH());
        dest.setProvinceID(src.getPROVINCEID());
        dest.setFullId(src.getFULLID());
        return dest;
    }

    private SubDistrict map(SubDistrictDB src) {
        SubDistrict dest = new SubDistrict();
        dest.setId(src.getSUBDISTRICTID().toString());
        dest.setNameEN(src.getSUBDISTRICTNAMEEN());
        dest.setNameTH(src.getSUBDISTRICTNAMETH());
        dest.setDistrictID(src.getDISTRICTID());
        dest.setProvinceID(src.getSUBDISTRICTNAMETH());
        dest.setFullId(src.getFULLID());
        return dest;
    }

    private Relationship map(RelationshipDB src) {
        Relationship dest = new Relationship();
        dest.setId(src.getRelationshipId().toString());
        dest.setNameEN(src.getRelationshipNameEn());
        dest.setNameTH(src.getRelationshipNameTh());
        return dest;
    }

    private State map(StateDB src) {
        State dest = new State();
        dest.setId(src.getStateId().toString());
        dest.setCountryId(src.getCountryId().toString());
        dest.setCountryCode(src.getCountryCode());
        dest.setNameEn(src.getStateNameEn());
        dest.setNameTh(src.getStateNameTh());
        dest.setActive(src.getRecordStatus().equals("N"));
        return dest;
    }

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonParser parser = mapper.getFactory().createParser(new File("/Users/golfdigg05/MAS_CITY.json"));
        if(parser.nextToken() != JsonToken.START_ARRAY) {
            throw new IllegalStateException("Expected an array");
        }
        while(parser.nextToken() == JsonToken.START_OBJECT) {
            // read everything from this START_OBJECT to the matching END_OBJECT
            // and return it as a tree model ObjectNode
            CityDB newJsonNode = mapper.readValue(parser, CityDB.class);
            System.out.println(newJsonNode);
            // do whatever you need to do with this object
        }

        parser.close();
    }
}
