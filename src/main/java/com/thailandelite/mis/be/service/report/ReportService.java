package com.thailandelite.mis.be.service.report;

import com.thailandelite.mis.be.common.DateUtils;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.be.service.dto.reports.*;
import com.thailandelite.mis.model.domain.*;
import com.thailandelite.mis.model.domain.enumeration.InvoiceStatus;
import com.thailandelite.mis.model.domain.enumeration.MemberStatus;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.byExample;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
public class ReportService  {

    private final MongoTemplate mongoTemplate;
    private final MemberRepository memberRepository;
    private final CardRepository cardRepository;


    private final ModelMapper modelMapper = new ModelMapper();

    public ReportService(MongoTemplate mongoTemplate, MemberRepository memberRepository, CardRepository cardRepository) {
        this.mongoTemplate = mongoTemplate;
        this.memberRepository = memberRepository;
        this.cardRepository = cardRepository;
    }

    public List<Invoice> queryInvoiceNotComplete(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo().toString()).toDate()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }
        queryInfo.addCriteria(where("status").ne(InvoiceStatus.SUCCESS));

        return mongoTemplate.find(queryInfo, Invoice.class);
    }


    public Page<Invoice> queryInvoiceNotCompletePage(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        List< Invoice> listResult = queryInvoiceNotComplete(arg,pageable);

        return PageableExecutionUtils.getPage(listResult, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), Invoice.class));
    }


    public byte[] queryInvoiceNotCompleteExcel(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        List<Invoice> listResult = queryInvoiceNotComplete(arg,pageable);

        return generateInvoiceheader(listResult);
    }


    public byte[] queryInvoiceExcel(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);
        List< Invoice> listResult = queryInvoice(arg,pageable);
        return generateInvoiceheader(listResult);
    }


    public Page<Invoice> queryInvoicePage(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);
        List< Invoice> listResult = queryInvoice(arg,pageable);

        return PageableExecutionUtils.getPage(listResult, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), Invoice.class));
    }


    public  List< Invoice>  queryInvoice(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo().toString()).toDate()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }

        return  mongoTemplate.find(queryInfo, Invoice.class);
    }


    public void generateInvoiceheader(Workbook workbook, Row header )
    {
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("ID");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Status");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("Name");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("Surname");
        //headerCell.setCellStyle(headerStyle);


        headerCell = header.createCell(4);
        headerCell.setCellValue("Total");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(5);
        headerCell.setCellValue("Paid");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(6);
        headerCell.setCellValue("Remain");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(7);
        headerCell.setCellValue("CS Pay");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(8);
        headerCell.setCellValue("Create");
        //headerCell.setCellStyle(headerStyle);
        headerCell = header.createCell(9);
        headerCell.setCellValue("Expiry");
        //headerCell.setCellStyle(headerStyle);
        headerCell = header.createCell(10);
        headerCell.setCellValue("Update");
        //headerCell.setCellStyle(headerStyle);

    }


    public void generateIncomeReportheader(Workbook workbook, Row header , ZonedDateTime date , int month)
    {
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("MemberID");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("MemberName");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("MemberSurname");
        ////headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("nationality");
        //headerCell.setCellStyle(headerStyle);


        headerCell = header.createCell(4);
        headerCell.setCellValue("activate Date");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(5);
        headerCell.setCellValue("Year");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(6);
        headerCell.setCellValue("perDay");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(7);
        headerCell.setCellValue("day");
        //headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(8);
        headerCell.setCellValue("Balance");
        //headerCell.setCellStyle(headerStyle);


        for(int i =0 ;i < month;i++){

            headerCell = header.createCell(9+i);
            headerCell.setCellValue(date.plusMonths(i).with(TemporalAdjusters.lastDayOfMonth()).toLocalDate().toString());
            //headerCell.setCellStyle(headerStyle);

        }

    }

    public  void generateIncomeReport(IncomeReport incomeReport, Sheet sheet, int i ,ZonedDateTime date, int month)
    {
        Row row = sheet.createRow(i+1);
        Cell headerCell = row.createCell(0);
        headerCell.setCellValue(incomeReport.getMemberId());

        headerCell = row.createCell(1);
        headerCell.setCellValue(incomeReport.getMemberName());

        headerCell = row.createCell(2);
        headerCell.setCellValue(incomeReport.getMemberSurname());


        headerCell = row.createCell(3);
        headerCell.setCellValue(incomeReport.getNationality());
        //headerCell.setCellStyle(headerStyle);


        headerCell = row.createCell(4);
        headerCell.setCellValue(incomeReport.getActivateDate().toLocalDate().toString());
        //headerCell.setCellStyle(headerStyle);

        headerCell = row.createCell(5);
        headerCell.setCellValue(incomeReport.getYear());
        //headerCell.setCellStyle(headerStyle);

        headerCell = row.createCell(6);
        headerCell.setCellValue(incomeReport.getPerDay());
        //headerCell.setCellStyle(headerStyle);

        headerCell = row.createCell(7);
        headerCell.setCellValue(incomeReport.getDay());
        //headerCell.setCellStyle(headerStyle);

        headerCell = row.createCell(8);
        headerCell.setCellValue(incomeReport.getBalance());

        for(int d =0 ;d < month;d++){
            headerCell = row.createCell(9+d);
            ZonedDateTime first = date.plusMonths(d).with(TemporalAdjusters.firstDayOfMonth());
            ZonedDateTime last = date.plusMonths(d).with(TemporalAdjusters.firstDayOfMonth());

            if(incomeReport.getMonthlyPaymentList().stream().filter(x->(x.getPaymentDate().with(TemporalAdjusters.firstDayOfMonth()).toLocalDate().equals(first.toLocalDate())) || (x.getPaymentDate().with(TemporalAdjusters.lastDayOfMonth()).toLocalDate().equals(last.toLocalDate()))).count() > 0) {
                headerCell.setCellValue(incomeReport.getMonthlyPaymentList().stream().filter(x->(x.getPaymentDate().with(TemporalAdjusters.firstDayOfMonth()).toLocalDate().equals(first.toLocalDate())) || (x.getPaymentDate().with(TemporalAdjusters.lastDayOfMonth()).toLocalDate().equals(last.toLocalDate()))).findFirst().get().getAmount());
            }
            //headerCell.setCellStyle(headerStyle);

        }

    }


    public  void generateInvoice(Invoice invoice, Sheet sheet, int i )
    {
        Row row = sheet.createRow(i+1);
         Cell headerCell = row.createCell(0);
        headerCell.setCellValue(invoice.getId());

        headerCell = row.createCell(1);
        headerCell.setCellValue(invoice.getStatus().name());

        headerCell = row.createCell(2);
        headerCell.setCellValue(invoice.getMember().getGivenName());

        headerCell = row.createCell(3);
        headerCell.setCellValue(invoice.getMember().getSurName());

        headerCell = row.createCell(4);
        headerCell.setCellValue(invoice.getTotalAmount());

        headerCell = row.createCell(5);
        headerCell.setCellValue(invoice.getPaidAmount());

        headerCell = row.createCell(6);
        headerCell.setCellValue(invoice.getTotalAmount() - invoice.getPaidAmount());


        headerCell = row.createCell(7);
        if(invoice.getPayDate() !=null) {
            headerCell.setCellValue(invoice.getPayDate().toLocalDate().toString());
        }

        headerCell = row.createCell(8);
        if(invoice.getExpire() !=null) {
            headerCell.setCellValue(invoice.getExpire().toLocalDate().toString());
        }

        headerCell = row.createCell(9);
        if(invoice.getLastModifiedDate() !=null) {
            headerCell.setCellValue(invoice.getLastModifiedDate().toString());
        }
    }

    public byte[] generateInvoiceheader(List<Invoice> invoices)
    {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("income");
        Row header = sheet.createRow(0);
        generateInvoiceheader(workbook,header);

        for (var i =0 ;i< invoices.size();i++ ) {
            generateInvoice(invoices.get(i),sheet,i);
        }

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            return  outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

      return  null;

    }


    public byte[] generateIncomeHeader(List<IncomeReport> invoices)
    {
        ZonedDateTime activate = invoices.stream().map(x-> x.getActivateDate()).min(ZonedDateTime::compareTo).get().with(TemporalAdjusters.firstDayOfMonth());
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("income");
        Row header = sheet.createRow(0);
        generateIncomeReportheader(workbook,header,activate,2000);

        for (var i =0 ;i< invoices.size();i++ ) {
            generateIncomeReport(invoices.get(i),sheet,i,activate,2000);
        }

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            workbook.write(outputStream);
            return  outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  null;

    }


    public List<Invoice> queryNotActivateMember(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo().toString()).toDate()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }
        List<String> members =  memberRepository.findAllByStatus(MemberStatus.REGISTER).stream().map(x -> x.getId()).collect(Collectors.toList());
        return mongoTemplate.find(queryInfo, Invoice.class).stream().filter(x->members.contains(x.getMemberId())).collect(Collectors.toList());
    }



    public byte[] queryNotActivateMemberExcel(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);
         List<Invoice> listResult = queryNotActivateMember(arg,pageable);
        return generateInvoiceheader(listResult);
    }

    public Page<Invoice> queryNotActivateMemberPage(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);
         List< Invoice> listResult = queryNotActivateMember(arg,pageable);
         return PageableExecutionUtils.getPage(listResult, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), Invoice.class));
    }



    public byte[] queryActivateMemberExcel(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);
        List<Invoice> listResult = queryInvoice(arg,pageable);

        return generateInvoiceheader(listResult);
    }

    public Page<Invoice> queryActivateMemberPage(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        List< Invoice> listResult = queryActivateMember(arg,pageable);
        return PageableExecutionUtils.getPage(listResult, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), Invoice.class));
    }

    public List<Invoice> queryActivateMember(FinanceRequest arg , Pageable pageable){
        final Query queryInfo = new Query().with(pageable);

        if (arg.getFrom() !=null && arg.getTo()!=null)  {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo().toString()).toDate()));
        }
        else if (arg.getFrom() !=null) {
            queryInfo.addCriteria(where("last_modified_date").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        }
        else if (arg.getTo()!=null) {
            queryInfo.addCriteria(where("last_modified_date").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }

        List<String> members =  memberRepository.findAllByStatus(MemberStatus.MEMBER).stream().map(x -> x.getId()).collect(Collectors.toList());
        return mongoTemplate.find(queryInfo, Invoice.class).stream().filter(x->members.contains(x.getMemberId())).collect(Collectors.toList());
    }



    public byte[] getIncomeReportExcel(FinanceRequest arg, Pageable pageable) {

        Query queryInfo = new Query().with(pageable);
        if (arg.getFrom() !=null && arg.getTo() !=null)  {
            queryInfo.addCriteria(where("activateDate").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo() .toString()).toDate()));
        }
        else if (arg.getFrom()!= null) {
            queryInfo.addCriteria(where("activateDate").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        }
        else if ( arg.getTo() != null) {
            queryInfo.addCriteria(where("activateDate").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }

        List<IncomeReport> invoices = mongoTemplate.find(queryInfo, IncomeReport.class);


        return generateIncomeHeader(invoices);
     }

    public Page<IncomeReport> getIncomeReport(FinanceRequest arg, Pageable pageable) {

        Query queryInfo = new Query().with(pageable);
        if (arg.getFrom() !=null && arg.getTo() !=null)  {
            queryInfo.addCriteria(where("activateDate").gt(Instant.parse(arg.getFrom().toString()).toDate()).lt(Instant.parse(arg.getTo() .toString()).toDate()));
        }
        else if (arg.getFrom()!= null) {
            queryInfo.addCriteria(where("activateDate").gt(Instant.parse(arg.getFrom().toString()).toDate()));
        }
        else if ( arg.getTo() != null) {
            queryInfo.addCriteria(where("activateDate").lt(Instant.parse(arg.getTo().toString()).toDate()));
        }

        List<IncomeReport> invoices = mongoTemplate.find(queryInfo, IncomeReport.class);

        return PageableExecutionUtils.getPage(invoices, pageable, () -> mongoTemplate.count(Query.of(queryInfo).limit(-1).skip(-1), IncomeReport.class));
    }

    private String getFieldGroupByKey(Membership membership, String fieldGroupBy) {
        if ("nationality".equalsIgnoreCase(fieldGroupBy)) {
            Optional<Member> member = memberRepository.findById(membership.getMemberId());
            if(member.isPresent())
                return member.get().getNationality().getName();
            else
                return "-";
        }else {
            Optional<Card> card = cardRepository.findById(membership.getCardId());
            if(card.isPresent())
                return card.get().getName();
            else
                return "-";
        }
    }

}
