package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.Product;
import com.thailandelite.mis.model.domain.Shop;
import com.thailandelite.mis.model.domain.Vendor;
import lombok.Data;

@Data
public class ProductReportResponse{
    private Vendor vendor;
    private Shop shop;
    private Product product;
}
