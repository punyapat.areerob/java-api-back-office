package com.thailandelite.mis.be.service.flow;

import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.repository.VendorProdileUpdateRepository;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.model.domain.VendorRequest;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import com.thailandelite.mis.model.dto.request.VendorProfileUpdateDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
@Slf4j
@Service
public class BPEditVendorService extends BPService<VendorProfileUpdateDao>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BPIncidentService bpIncidentService;
    private final VendorProdileUpdateRepository vendorProdileUpdateRepository;
    public BPEditVendorService(
        CamundaService camundaService,
        VendorProdileUpdateRepository vendorProdileUpdateRepository,
        CaseActivityRepository caseActivityRepository,
        BPIncidentService bpIncidentService1){
        super(camundaService,
            new BPEditVendorService.Register_Vendor(vendorProdileUpdateRepository, caseActivityRepository),
            new BPEditVendorService.Ally_verify(vendorProdileUpdateRepository, caseActivityRepository)

        );
        this.vendorProdileUpdateRepository =vendorProdileUpdateRepository;
        this.bpIncidentService = bpIncidentService1;
    }
    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) {
        VendorProfileUpdateDao vendorRequest = (VendorProfileUpdateDao) object;
        VendorProfileUpdateDao result = vendorProdileUpdateRepository.save(vendorRequest);
        ProcessInstance process = super.startProcess(processDefKey,result.getId());
        this.caseInfoService.EditVendorRequest(processDefKey, Register_Vendor.taskDefKey, process,result);
        return result;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
                .build();
            dest.setData(data);
            return dest;
        });
    }
    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        VendorProfileUpdateDao vendorProfileUpdateDao = vendorProdileUpdateRepository.findById(caseInfoDTO.getEntityId())
            .orElseThrow(() -> new RuntimeException("Not found"));
        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder()
            .vendorProfileUpdateDao(vendorProfileUpdateDao)
            .build();

        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }
    @RequiredArgsConstructor
    private static class Register_Vendor implements ITask {
        final VendorProdileUpdateRepository vendorProdileUpdateRepository;
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "Register_Vendor";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {APPROVE}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog)  {
            switch (Action.valueOf(actionName)) {
                case APPROVE:

                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }
    @RequiredArgsConstructor
    private static class Ally_verify implements ITask {
        final VendorProdileUpdateRepository vendorProdileUpdateRepository;
        final CaseActivityRepository caseActivityRepository;
        public static final String taskDefKey = "Register_Vendor";
        public String name() {
            return taskDefKey ;
        }
        public enum Action {DONE,RESUBMIT}
        @Override
        public void process(String actionName, Map<String, Object> vars, String taskId, String user, String remark, CaseInfo caseInfo, CaseActivity caseLog)  {
            switch (Action.valueOf(actionName)) {
                case DONE:

                    break;
                case RESUBMIT:

                    break;
            }
        }
        @Override
        public Enum<?>[] actions() { return Action.values();}
    }

}
