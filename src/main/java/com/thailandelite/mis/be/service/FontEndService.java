package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.web.rest.UserJWTController;
import com.thailandelite.mis.be.web.rest.errors.BadRequestAlertException;
import com.thailandelite.mis.model.domain.Member;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;


@Slf4j
@Service
public class FontEndService {
    //    http://ebapi-dev.golfdigg.com/
    @Value("${mis.url}")
    public String url;

    @Value("${mis.token}")
    public String token;

    @Value("${mis.username}")
    public String userName;

    @Value("${mis.password}")
    public String password;

    private final RestTemplate rt = new RestTemplate();

    public String currentToken;

    @NotNull
    private HttpHeaders getHttpHeaders() {
        if(currentToken == null || currentToken.isEmpty())
            currentToken = this.token;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer " + currentToken);
        return headers;
    }

    public UserJWTController.JWTToken refreshToken() {
        log.debug("Refresh Token!!");
        HttpHeaders headers = getHttpHeaders();
        HashMap<String, String> formReq = new HashMap<>();
        formReq.put("username", userName);
        formReq.put("password", password);
        HttpEntity<?> httpEntity = new HttpEntity<>(formReq, headers);
        UserJWTController.JWTToken token = this.rt.exchange(url + "/api/authenticate", POST, httpEntity, UserJWTController.JWTToken.class).getBody();
        log.debug("new token is {}", token.getIdToken());
        currentToken = token.getIdToken();
        return token;
    }

    public ResponseEntity<Object> forward(String path) {
        HttpHeaders httpHeaders = getHttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        try {
            return rt.exchange(url + path, HttpMethod.GET, httpEntity, Object.class);
        }catch (HttpStatusCodeException e){
            if(e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)){
                refreshToken();
                httpHeaders = getHttpHeaders();
                httpEntity = new HttpEntity<>(httpHeaders);
                return rt.exchange(url + path, HttpMethod.GET, httpEntity, Object.class);
            }
            throw e;
        }
    }

    public <T> T forwardRequest(String path, ParameterizedTypeReference<T> paramsTypeRef) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        return this.rt.exchange(url + path, HttpMethod.GET, httpEntity, paramsTypeRef).getBody();
    }

    public <T> T forwardPostRequest(String path, ParameterizedTypeReference<T> paramsTypeRef, Object body) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(body, headers);
        return this.rt.exchange(url + path, POST, httpEntity, paramsTypeRef).getBody();
    }

    public boolean changePassword(String email, String newPassword) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        try {
            this.rt.exchange(url + "/api/v1/be/member/password/change/force?email=" + email + "&password=" + newPassword, HttpMethod.GET, httpEntity, Member.class);
            return true;
        }catch (HttpStatusCodeException e){
            if(e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)){
                refreshToken();
                headers = getHttpHeaders();
                httpEntity = new HttpEntity<>(headers);
                this.rt.exchange(url + "/api/v1/be/member/password/change/force?email=" + email + "&password=" + newPassword, HttpMethod.GET, httpEntity, Member.class);
                return true;
            }
            throw e;
        }

    }

    public boolean changeEmail(String email, String newEmail) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        try {
            this.rt.exchange(url + "/api/v1/be/member/email?emailOld=" + email + "&emailNew=" + newEmail, HttpMethod.GET, httpEntity, Member.class);
            return true;
        }catch (HttpStatusCodeException e){
            if(e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)){
                refreshToken();
                headers = getHttpHeaders();
                httpEntity = new HttpEntity<>(headers);
                this.rt.exchange(url + "/api/v1/be/member/email?emailOld=" + email + "&emailNew=" + newEmail, HttpMethod.GET, httpEntity, Member.class);
                return true;
            }
            throw e;
        }
    }
}



