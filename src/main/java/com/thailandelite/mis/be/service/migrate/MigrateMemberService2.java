package com.thailandelite.mis.be.service.migrate;

import com.thailandelite.mis.model.domain.MemberPrivilegeQuota;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.migrate.domain.SmmMemberPassport;
import com.thailandelite.mis.migrate.domain.SmmMemberPrivilegeQuota;
import com.thailandelite.mis.migrate.repository.SmmMemberPassportRepository;
import com.thailandelite.mis.migrate.repository.SmmMemberPrivilegeQuotaRepository;
import com.thailandelite.mis.model.domain.Passport;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Profile("migrate")
@Slf4j
@Service
@RequiredArgsConstructor
public class MigrateMemberService2 {
    public final CardRepository cardRepository;
    public final CardPrivilegeRepository cardPrivilegeRepository;
    public final PrivilegeRepository privilegeRepository;
    public final MemberPrivilegeQuotaRepository memberPrivilegeQuotaRepository;
    public final PassportRepository passportRepository;

    private final SmmMemberPrivilegeQuotaRepository smmMemberPrivilegeQuotaRepository;
    private final SmmMemberPassportRepository smmMemberPassportRepository;

    public long memberPrivilege() {
        List<SmmMemberPrivilegeQuota> srcList = smmMemberPrivilegeQuotaRepository.findAll();
        List<MemberPrivilegeQuota> list = srcList.stream().map(src -> {
            MemberPrivilegeQuota dest = new MemberPrivilegeQuota();
            dest.setId(src.getMemberPrivilegeQuotaId().toString());
            dest.setPrivilegeId(src.getPrivilegeId().toString());
            dest.setYearQuota(src.getYearQuota());
            dest.setQuotaQuantity(src.getQuotaQuantity());
            dest.setQuotaAdjust(src.getQuotaAdjust());
            dest.setQuotaUsed(src.getQuotaUsed());
            dest.setMemberGroupId(src.getMemberGroupId().toString());
//            dest.setQuotaUsedBk(src.getQuotaUsedBk());
            dest.setCreatedBy(src.getCreateUser());
            /*dest.setCreatedDate(src.getCreateDate());
            dest.setUpdatedBy(src.getLastUser());
            dest.setUpdatedDate(src.getLastDate());*/
            dest.setRecordStatus(src.getRecordStatus());
            return dest;
        }).collect(Collectors.toList());

        memberPrivilegeQuotaRepository.deleteAll();
        return memberPrivilegeQuotaRepository.saveAll(list).size();
    }

    public long passport() {
        List<SmmMemberPassport> srcList = smmMemberPassportRepository.findAll();
        List<Passport> list = srcList.stream().map( src -> {
            Passport dest = new Passport();
            dest.setId(src.getPassportId().toString());
            dest.setMemberId(src.getMemberId().toString());
            dest.setPassportNo(src.getPassportNo());
            dest.setIssueBy(src.getPassportIssueBy());
            dest.setIssueDate(src.getPassportIssueDate());
            dest.setExpireDate(src.getPassportExpiryDate());
//            dest.setNationality(src.getNationalityId());
//            dest.setNationalityPassportId(src.getNationalityPassportId());
            dest.setFilePassport(src.getFilePassport());
            dest.setRemark(src.getREMARK());
            dest.setRecordStatus(src.getRecordStatus());
            dest.setCreatedBy(src.getCreateUser());
            dest.setCreatedDate(src.getCreateDate());
            dest.setUpdatedBy(src.getLastUser());
            dest.setUpdatedDate(src.getLastDate());
            return dest;
        }).collect(Collectors.toList());

        passportRepository.deleteAll();
        return passportRepository.saveAll(list).size();
    }
}
