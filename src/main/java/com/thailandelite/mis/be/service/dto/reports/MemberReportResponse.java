package com.thailandelite.mis.be.service.dto.reports;

import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.Membership;
import lombok.Data;

@Data
public class MemberReportResponse {
    private String dateTimePeriod;
    private Member member;
    private Membership membership;
}
