package com.thailandelite.mis.be.service.flow;

import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.domain.CaseInfo;
import com.thailandelite.mis.be.repository.ApplicationRepository;
import com.thailandelite.mis.be.repository.BookingRepository;
import com.thailandelite.mis.be.repository.CaseActivityRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.service.CamundaService;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQuery;
import com.thailandelite.mis.be.web.rest.flows.model.CaseQueryRequest;
import com.thailandelite.mis.model.domain.Application;
import com.thailandelite.mis.model.domain.booking.Booking;
import com.thailandelite.mis.model.dto.CaseInfoDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.jsonwebtoken.lang.Strings.hasText;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class BPBooking extends BPService<Booking>{
    private final ModelMapper modelMapper = new ModelMapper();
    private final BookingRepository bookingRepository;
    private final MemberRepository memberRepository;
    private final ApplicationRepository applicationRepository;
    public BPBooking(CamundaService camundaService,
                     BookingRepository bookingRepository,
                     CaseActivityRepository caseActivityRepository,
                     MemberRepository memberRepository,
                     ApplicationRepository applicationRepository
    ) {
        super(camundaService);
        this.bookingRepository = bookingRepository;
        this.memberRepository = memberRepository;
        this.applicationRepository = applicationRepository;
    }

    public Enum<?>[] getTaskByTaskDefKey(String taskDefKey) {
        ITask iTask = taskDefs.stream()
            .filter(taskDef -> isNameEquals(taskDefKey, taskDef))
            .findAny().orElseThrow(() -> new RuntimeException("TaskDefinitionKey not match"));
        return iTask.actions();
    }

    @Override
    public Object create(String processDefKey, Object object) {
        return null;
    }

    @Override
    protected Page<CaseInfoDTO> mapToDTO(Page<CaseInfo> caseList) {
        List<String> entityIds = caseList.stream().map(CaseInfo::getEntityId).collect(toList());
        return caseList.map(caseInfo -> {
            CaseInfoDTO dest = modelMapper.map(caseInfo, CaseInfoDTO.class);
            Booking booking = bookingRepository.findById(caseInfo.getEntityId()).get();
            CaseInfoDTO.Data data = CaseInfoDTO.Data.builder().booking(booking)
                .build();
            dest.setData(data);
            return dest;
        });
    }

    @Override
    public CaseInfoDTO getDetail(String caseId) {
        CaseInfoDTO caseInfoDTO = super.getCaseDetail(caseId);
        //// TODO: 10/4/2021 AD call CardService
        Booking booking = bookingRepository.findById(caseInfoDTO.getEntityId()).get();
        CaseInfoDTO.Data data = CaseInfoDTO.Data.builder().booking(booking)
            .build();
        caseInfoDTO.setData(data);
        return caseInfoDTO;
    }



    public String getSignature(String bookingId) {

        Booking booking = bookingRepository.findById(bookingId).get();
        Application application = applicationRepository.findById(memberRepository.findById (booking.getRequestMemberId()).get().getApplicationId()).get();
        return application.getDocumentForm().getSignature();
    }


    @Override
    public Page<CaseInfoDTO> queryCaseWithEntity(CaseQueryRequest request, Class c){
        CaseQuery caseQuery = modelMapper.map(request, CaseQuery.class);
        Page<CaseInfo> caseInfoPage = caseInfoService.queryByEntityByEntity(caseQuery);
        return this.mapToDTO(caseInfoPage);
    }

}
