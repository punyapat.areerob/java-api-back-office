package com.thailandelite.mis.be.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String _90Days_Close = "90Days-Close";
    public static final String _90Days_EPL = "90Days-EPL";
    public static final String _90Days_VENDOR = "90Days-VENDOR";
    public static final String Activate_CRM = "Activate-CRM";
    public static final String Activate_SLS = "Activate-SLS";
    public static final String Agent_Applicant_SLS = "Agent-Applicant-SLS";
    public static final String Agent_Applicant_SLS_Risk = "Agent-Applicant-SLS-Risk";
    public static final String Agent_Upgrade_CRM = "Agent-Upgrade-CRM";
    public static final String AgentDetails_SLS = "AgentDetails-SLS";
    public static final String AirportEPA_Close = "AirportEPA-Close";
    public static final String AirportEPA_EPA = "AirportEPA-EPA";
    public static final String AirportEPA_VENDOR = "AirportEPA-VENDOR";
    public static final String AnnualPayment_FIN = "AnnualPayment-FIN";
    public static final String Applicant_GR = "Applicant-GR";
    public static final String Applicant_GR_Risk = "Applicant-GR-Risk";
    public static final String Applicant_SLS = "Applicant-SLS";
    public static final String Applicant_SLS_Risk = "Applicant-SLS-Risk";
    public static final String BOI_Close = "BOI-Close";
    public static final String BOI_EPL = "BOI-EPL";
    public static final String BOI_RSVN = "BOI-RSVN";
    public static final String Banking_Close = "Banking-Close";
    public static final String Banking_EPL = "Banking-EPL";
    public static final String Banking_RSVN = "Banking-RSVN";
    public static final String CashReceipt_FIN = "CashReceipt-FIN";
    public static final String DrivingLicense_Close = "DrivingLicense-Close";
    public static final String DrivingLicense_EPL = "DrivingLicense-EPL";
    public static final String DrivingLicense_GR = "DrivingLicense-GR";
    public static final String DrivingLicense_RSVN = "DrivingLicense-RSVN";
    public static final String GRCheck_CRM = "GRCheck-CRM";
    public static final String GRCheck_GR = "GRCheck-GR";
    public static final String Golf_Manager = "Golf-Manager";
    public static final String Golf_Close = "Golf-Close";
    public static final String Golf_Golfdigg = "Golf-Golfdigg";
    public static final String Golf_MCC = "Golf-MCC";
    public static final String Golf_VENDOR = "Golf-VENDOR";
    public static final String HealthCheck_Close = "HealthCheck-Close";
    public static final String HealthCheck_RSVN = "HealthCheck-RSVN";
    public static final String HealthCheck_VENDOR = "HealthCheck-VENDOR";
    public static final String Incident_CRM = "Incident-CRM";
    public static final String Incident_CRMManager = "Incident-CRMManager";
    public static final String Incident_EPA = "Incident-EPA";
    public static final String Incident_EPAManager = "Incident-EPAManager";
    public static final String Incident_MCC = "Incident-MCC";
    public static final String Incident_Manager = "Incident-Manager";
    public static final String JPA_HR = "JPA-HR";
    public static final String Lounge_Close = "Lounge-Close";
    public static final String Lounge_VENDOR = "Lounge-VENDOR";
    public static final String MembershipPayment_CRM = "MembershipPayment-CRM";
    public static final String MembershipPayment_FIN = "MembershipPayment-FIN";
    public static final String MembershipPayment_SLS = "MembershipPayment-SLS";
    public static final String NameChange_GR = "NameChange-GR";
    public static final String NameChangeRisk_GR = "NameChangeRisk-GR";
    public static final String NameNationalityChange_CRM = "NameNationalityChange-CRM";
    public static final String NameNationalityChangeRisk_CRM = "NameNationalityChangeRisk-CRM";
    public static final String OtherBooking_RSVN = "OtherBooking-RSVN";
    public static final String PassportUpdate_CRM = "PassportUpdate-CRM";
    public static final String PenaltyPayment_FIN = "PenaltyPayment-FIN";
    public static final String Renew_SLS = "Renew-SLS";
    public static final String Service_RSVN = "Service-RSVN";
    public static final String Spa_Close = "Spa-Close";
    public static final String Spa_VENDOR = "Spa-VENDOR";
    public static final String Stay_VENDOR = "Stay-VENDOR";
    public static final String Stay_Close = "Stay-Close";
    public static final String Stay_EPL = "Stay-EPL";
    public static final String Stay_GR = "Stay-GR";
    public static final String Stay_RSVN = "Stay-RSVN";
    public static final String Transfer_Close = "Transfer-Close";
    public static final String Transfer_GR = "Transfer-GR";
    public static final String Transfer_GR_Risk = "Transfer-GR-Risk";
    public static final String Transfer_RSVN = "Transfer-RSVN";
    public static final String TransferMember_CRM = "TransferMember-CRM";
    public static final String TransferMember_CRM_Risk = "TransferMember-CRM-Risk";
    public static final String TransferPayment_CRM = "TransferPayment-CRM";
    public static final String TransferPayment_FIN = "TransferPayment-FIN";
    public static final String TypeChange_GR_Risk = "Type change-GR-Risk";
    public static final String TypeChange_CRM = "TypeChange-CRM";
    public static final String TypeChange_CRM_Risk = "TypeChange-CRM-Risk";
    public static final String TypeChange_GR = "TypeChange-GR";
    public static final String Upgrade_CRM = "Upgrade-CRM";
    public static final String UpgradePayment_CRM = "UpgradePayment-CRM";
    public static final String UpgradePayment_FIN = "UpgradePayment-FIN";
    public static final String VISAEmbassy_Close = "VISA Embassy-Close";
    public static final String VISAEmbassy_GR = "VISA Embassy-GR";
    public static final String VISAEmbassy_RSVN = "VISA Embassy-RSVN";
    public static final String VISAImmi_VENDOR = "VISAImmi-VENDOR";
    public static final String VISAImmi_Close = "VISAImmi-Close";
    public static final String VISAImmi_EPA = "VISAImmi-EPA";
    public static final String VISAImmi_EPL = "VISAImmi-EPL";
    public static final String VISAImmi_GR = "VISAImmi-GR";
    public static final String VISAImmi_RSVN = "VISAImmi-RSVN";
    public static final String VendorDetail_ALY = "VendorDetail-ALY";

    private AuthoritiesConstants() {
    }
}
