package io.easycm.framework.form.generator;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Fernando on 16/12/2014.
 */
@Data
public class RadioOptions implements Serializable {

    private String label;
    private Object value;
    private String description;

    public RadioOptions() {
    }

    public RadioOptions(String label, String value) {
        this.label = label;
        this.value = value;
    }
}
