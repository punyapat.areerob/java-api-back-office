package io.easycm.framework.form.generator;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FieldGroup {
    private List<FormlyFieldConfig> fieldGroup = new ArrayList<>();
}
