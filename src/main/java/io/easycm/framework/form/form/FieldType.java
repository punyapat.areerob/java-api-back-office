package io.easycm.framework.form.form;

/**
 * Created by Fernando on 06/01/2015.
 */
public enum FieldType {
    AUTO("auto"),
    PASSSWORD("password"),
    INPUT("input"),
    TEXTAREA("textarea"),
    CHECKBOX("checkbox"),
    TOGGLE("toggle"),
    RADIO("radio"),
    SELECT("select"),
//    SELECT_MULTI("select"),
    NUMBER("number"),
    DATEPICKER("datepicker");
    private String type = "auto";

    FieldType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
