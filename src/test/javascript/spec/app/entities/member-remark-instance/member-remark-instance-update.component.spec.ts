import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { MemberRemarkInstanceUpdateComponent } from 'app/entities/member-remark-instance/member-remark-instance-update.component';
import { MemberRemarkInstanceService } from 'app/entities/member-remark-instance/member-remark-instance.service';
import { MemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';

describe('Component Tests', () => {
  describe('MemberRemarkInstance Management Update Component', () => {
    let comp: MemberRemarkInstanceUpdateComponent;
    let fixture: ComponentFixture<MemberRemarkInstanceUpdateComponent>;
    let service: MemberRemarkInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [MemberRemarkInstanceUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MemberRemarkInstanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MemberRemarkInstanceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MemberRemarkInstanceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MemberRemarkInstance('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MemberRemarkInstance();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
