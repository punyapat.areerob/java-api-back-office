import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { MemberRemarkInstanceComponent } from 'app/entities/member-remark-instance/member-remark-instance.component';
import { MemberRemarkInstanceService } from 'app/entities/member-remark-instance/member-remark-instance.service';
import { MemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';

describe('Component Tests', () => {
  describe('MemberRemarkInstance Management Component', () => {
    let comp: MemberRemarkInstanceComponent;
    let fixture: ComponentFixture<MemberRemarkInstanceComponent>;
    let service: MemberRemarkInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [MemberRemarkInstanceComponent],
      })
        .overrideTemplate(MemberRemarkInstanceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MemberRemarkInstanceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MemberRemarkInstanceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MemberRemarkInstance('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.memberRemarkInstances && comp.memberRemarkInstances[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
