import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { MemberRemarkInstanceDetailComponent } from 'app/entities/member-remark-instance/member-remark-instance-detail.component';
import { MemberRemarkInstance } from 'app/shared/model/member-remark-instance.model';

describe('Component Tests', () => {
  describe('MemberRemarkInstance Management Detail Component', () => {
    let comp: MemberRemarkInstanceDetailComponent;
    let fixture: ComponentFixture<MemberRemarkInstanceDetailComponent>;
    const route = ({ data: of({ memberRemarkInstance: new MemberRemarkInstance('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [MemberRemarkInstanceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MemberRemarkInstanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MemberRemarkInstanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load memberRemarkInstance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.memberRemarkInstance).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
