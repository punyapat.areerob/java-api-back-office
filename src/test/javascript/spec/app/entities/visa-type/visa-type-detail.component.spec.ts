import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VisaTypeDetailComponent } from 'app/entities/visa-type/visa-type-detail.component';
import { VisaType } from 'app/shared/model/visa-type.model';

describe('Component Tests', () => {
  describe('VisaType Management Detail Component', () => {
    let comp: VisaTypeDetailComponent;
    let fixture: ComponentFixture<VisaTypeDetailComponent>;
    const route = ({ data: of({ visaType: new VisaType('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VisaTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VisaTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VisaTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load visaType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.visaType).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
