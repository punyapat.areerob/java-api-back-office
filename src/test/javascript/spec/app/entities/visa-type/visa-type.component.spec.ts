import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { VisaTypeComponent } from 'app/entities/visa-type/visa-type.component';
import { VisaTypeService } from 'app/entities/visa-type/visa-type.service';
import { VisaType } from 'app/shared/model/visa-type.model';

describe('Component Tests', () => {
  describe('VisaType Management Component', () => {
    let comp: VisaTypeComponent;
    let fixture: ComponentFixture<VisaTypeComponent>;
    let service: VisaTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VisaTypeComponent],
      })
        .overrideTemplate(VisaTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VisaTypeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VisaTypeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new VisaType('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.visaTypes && comp.visaTypes[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
