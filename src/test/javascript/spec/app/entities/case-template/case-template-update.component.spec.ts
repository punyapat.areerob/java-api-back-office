import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseTemplateUpdateComponent } from 'app/entities/case-template/case-template-update.component';
import { CaseTemplateService } from 'app/entities/case-template/case-template.service';
import { CaseTemplate } from 'app/shared/model/case-template.model';

describe('Component Tests', () => {
  describe('CaseTemplate Management Update Component', () => {
    let comp: CaseTemplateUpdateComponent;
    let fixture: ComponentFixture<CaseTemplateUpdateComponent>;
    let service: CaseTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseTemplateUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseTemplateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseTemplateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseTemplateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseTemplate('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseTemplate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
