import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseTemplateDetailComponent } from 'app/entities/case-template/case-template-detail.component';
import { CaseTemplate } from 'app/shared/model/case-template.model';

describe('Component Tests', () => {
  describe('CaseTemplate Management Detail Component', () => {
    let comp: CaseTemplateDetailComponent;
    let fixture: ComponentFixture<CaseTemplateDetailComponent>;
    const route = ({ data: of({ caseTemplate: new CaseTemplate('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseTemplateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseTemplateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseTemplateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseTemplate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseTemplate).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
