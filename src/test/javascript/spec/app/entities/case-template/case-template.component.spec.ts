import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseTemplateComponent } from 'app/entities/case-template/case-template.component';
import { CaseTemplateService } from 'app/entities/case-template/case-template.service';
import { CaseTemplate } from 'app/shared/model/case-template.model';

describe('Component Tests', () => {
  describe('CaseTemplate Management Component', () => {
    let comp: CaseTemplateComponent;
    let fixture: ComponentFixture<CaseTemplateComponent>;
    let service: CaseTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseTemplateComponent],
      })
        .overrideTemplate(CaseTemplateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseTemplateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseTemplateService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseTemplate('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseTemplates && comp.caseTemplates[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
