import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingGuestDetailComponent } from 'app/entities/booking-guest/booking-guest-detail.component';
import { BookingGuest } from 'app/shared/model/booking-guest.model';

describe('Component Tests', () => {
  describe('BookingGuest Management Detail Component', () => {
    let comp: BookingGuestDetailComponent;
    let fixture: ComponentFixture<BookingGuestDetailComponent>;
    const route = ({ data: of({ bookingGuest: new BookingGuest('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingGuestDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookingGuestDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookingGuestDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bookingGuest on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bookingGuest).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
