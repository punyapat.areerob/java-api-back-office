import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingGuestUpdateComponent } from 'app/entities/booking-guest/booking-guest-update.component';
import { BookingGuestService } from 'app/entities/booking-guest/booking-guest.service';
import { BookingGuest } from 'app/shared/model/booking-guest.model';

describe('Component Tests', () => {
  describe('BookingGuest Management Update Component', () => {
    let comp: BookingGuestUpdateComponent;
    let fixture: ComponentFixture<BookingGuestUpdateComponent>;
    let service: BookingGuestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingGuestUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BookingGuestUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingGuestUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingGuestService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingGuest('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingGuest();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
