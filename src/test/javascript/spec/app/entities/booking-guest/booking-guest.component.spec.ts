import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { BookingGuestComponent } from 'app/entities/booking-guest/booking-guest.component';
import { BookingGuestService } from 'app/entities/booking-guest/booking-guest.service';
import { BookingGuest } from 'app/shared/model/booking-guest.model';

describe('Component Tests', () => {
  describe('BookingGuest Management Component', () => {
    let comp: BookingGuestComponent;
    let fixture: ComponentFixture<BookingGuestComponent>;
    let service: BookingGuestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingGuestComponent],
      })
        .overrideTemplate(BookingGuestComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingGuestComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingGuestService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BookingGuest('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bookingGuests && comp.bookingGuests[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
