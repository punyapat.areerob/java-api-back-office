import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AgentCommissionRateService } from 'app/entities/agent-commission-rate/agent-commission-rate.service';
import { IAgentCommissionRate, AgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';
import { AgentRateType } from 'app/shared/model/enumerations/agent-rate-type.model';

describe('Service Tests', () => {
  describe('AgentCommissionRate Service', () => {
    let injector: TestBed;
    let service: AgentCommissionRateService;
    let httpMock: HttpTestingController;
    let elemDefault: IAgentCommissionRate;
    let expectedResult: IAgentCommissionRate | IAgentCommissionRate[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(AgentCommissionRateService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new AgentCommissionRate('ID', 'AAAAAAA', 0, AgentRateType.STEP, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a AgentCommissionRate', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new AgentCommissionRate()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a AgentCommissionRate', () => {
        const returnedFromService = Object.assign(
          {
            constraint: 'BBBBBB',
            commissionRate: 1,
            type: 'BBBBBB',
            comment: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of AgentCommissionRate', () => {
        const returnedFromService = Object.assign(
          {
            constraint: 'BBBBBB',
            commissionRate: 1,
            type: 'BBBBBB',
            comment: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a AgentCommissionRate', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
