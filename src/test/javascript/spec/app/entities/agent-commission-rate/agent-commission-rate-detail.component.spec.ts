import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentCommissionRateDetailComponent } from 'app/entities/agent-commission-rate/agent-commission-rate-detail.component';
import { AgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';

describe('Component Tests', () => {
  describe('AgentCommissionRate Management Detail Component', () => {
    let comp: AgentCommissionRateDetailComponent;
    let fixture: ComponentFixture<AgentCommissionRateDetailComponent>;
    const route = ({ data: of({ agentCommissionRate: new AgentCommissionRate('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentCommissionRateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AgentCommissionRateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentCommissionRateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load agentCommissionRate on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentCommissionRate).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
