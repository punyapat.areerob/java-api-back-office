import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentCommissionRateUpdateComponent } from 'app/entities/agent-commission-rate/agent-commission-rate-update.component';
import { AgentCommissionRateService } from 'app/entities/agent-commission-rate/agent-commission-rate.service';
import { AgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';

describe('Component Tests', () => {
  describe('AgentCommissionRate Management Update Component', () => {
    let comp: AgentCommissionRateUpdateComponent;
    let fixture: ComponentFixture<AgentCommissionRateUpdateComponent>;
    let service: AgentCommissionRateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentCommissionRateUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AgentCommissionRateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentCommissionRateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentCommissionRateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentCommissionRate('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentCommissionRate();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
