import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { AgentCommissionRateComponent } from 'app/entities/agent-commission-rate/agent-commission-rate.component';
import { AgentCommissionRateService } from 'app/entities/agent-commission-rate/agent-commission-rate.service';
import { AgentCommissionRate } from 'app/shared/model/agent-commission-rate.model';

describe('Component Tests', () => {
  describe('AgentCommissionRate Management Component', () => {
    let comp: AgentCommissionRateComponent;
    let fixture: ComponentFixture<AgentCommissionRateComponent>;
    let service: AgentCommissionRateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentCommissionRateComponent],
      })
        .overrideTemplate(AgentCommissionRateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentCommissionRateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentCommissionRateService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AgentCommissionRate('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.agentCommissionRates && comp.agentCommissionRates[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
