import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { SubProductCategoryComponent } from 'app/entities/sub-product-category/sub-product-category.component';
import { SubProductCategoryService } from 'app/entities/sub-product-category/sub-product-category.service';
import { SubProductCategory } from 'app/shared/model/sub-product-category.model';

describe('Component Tests', () => {
  describe('SubProductCategory Management Component', () => {
    let comp: SubProductCategoryComponent;
    let fixture: ComponentFixture<SubProductCategoryComponent>;
    let service: SubProductCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [SubProductCategoryComponent],
      })
        .overrideTemplate(SubProductCategoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SubProductCategoryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SubProductCategoryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new SubProductCategory('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.subProductCategories && comp.subProductCategories[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
