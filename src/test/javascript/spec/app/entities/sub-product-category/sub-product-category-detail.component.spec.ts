import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { SubProductCategoryDetailComponent } from 'app/entities/sub-product-category/sub-product-category-detail.component';
import { SubProductCategory } from 'app/shared/model/sub-product-category.model';

describe('Component Tests', () => {
  describe('SubProductCategory Management Detail Component', () => {
    let comp: SubProductCategoryDetailComponent;
    let fixture: ComponentFixture<SubProductCategoryDetailComponent>;
    const route = ({ data: of({ subProductCategory: new SubProductCategory('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [SubProductCategoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(SubProductCategoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SubProductCategoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load subProductCategory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.subProductCategory).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
