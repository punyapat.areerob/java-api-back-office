import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { SubProductCategoryUpdateComponent } from 'app/entities/sub-product-category/sub-product-category-update.component';
import { SubProductCategoryService } from 'app/entities/sub-product-category/sub-product-category.service';
import { SubProductCategory } from 'app/shared/model/sub-product-category.model';

describe('Component Tests', () => {
  describe('SubProductCategory Management Update Component', () => {
    let comp: SubProductCategoryUpdateComponent;
    let fixture: ComponentFixture<SubProductCategoryUpdateComponent>;
    let service: SubProductCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [SubProductCategoryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(SubProductCategoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SubProductCategoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SubProductCategoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SubProductCategory('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SubProductCategory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
