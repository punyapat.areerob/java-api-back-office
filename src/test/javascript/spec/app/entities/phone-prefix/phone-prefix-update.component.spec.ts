import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { PhonePrefixUpdateComponent } from 'app/entities/phone-prefix/phone-prefix-update.component';
import { PhonePrefixService } from 'app/entities/phone-prefix/phone-prefix.service';
import { PhonePrefix } from 'app/shared/model/phone-prefix.model';

describe('Component Tests', () => {
  describe('PhonePrefix Management Update Component', () => {
    let comp: PhonePrefixUpdateComponent;
    let fixture: ComponentFixture<PhonePrefixUpdateComponent>;
    let service: PhonePrefixService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [PhonePrefixUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(PhonePrefixUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhonePrefixUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PhonePrefixService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PhonePrefix('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PhonePrefix();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
