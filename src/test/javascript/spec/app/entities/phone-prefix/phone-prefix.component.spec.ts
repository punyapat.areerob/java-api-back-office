import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { PhonePrefixComponent } from 'app/entities/phone-prefix/phone-prefix.component';
import { PhonePrefixService } from 'app/entities/phone-prefix/phone-prefix.service';
import { PhonePrefix } from 'app/shared/model/phone-prefix.model';

describe('Component Tests', () => {
  describe('PhonePrefix Management Component', () => {
    let comp: PhonePrefixComponent;
    let fixture: ComponentFixture<PhonePrefixComponent>;
    let service: PhonePrefixService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [PhonePrefixComponent],
      })
        .overrideTemplate(PhonePrefixComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PhonePrefixComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PhonePrefixService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PhonePrefix('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.phonePrefixes && comp.phonePrefixes[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
