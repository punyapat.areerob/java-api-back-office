import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { PhonePrefixDetailComponent } from 'app/entities/phone-prefix/phone-prefix-detail.component';
import { PhonePrefix } from 'app/shared/model/phone-prefix.model';

describe('Component Tests', () => {
  describe('PhonePrefix Management Detail Component', () => {
    let comp: PhonePrefixDetailComponent;
    let fixture: ComponentFixture<PhonePrefixDetailComponent>;
    const route = ({ data: of({ phonePrefix: new PhonePrefix('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [PhonePrefixDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(PhonePrefixDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PhonePrefixDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load phonePrefix on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.phonePrefix).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
