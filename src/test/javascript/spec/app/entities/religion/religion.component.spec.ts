import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ReligionComponent } from 'app/entities/religion/religion.component';
import { ReligionService } from 'app/entities/religion/religion.service';
import { Religion } from 'app/shared/model/religion.model';

describe('Component Tests', () => {
  describe('Religion Management Component', () => {
    let comp: ReligionComponent;
    let fixture: ComponentFixture<ReligionComponent>;
    let service: ReligionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ReligionComponent],
      })
        .overrideTemplate(ReligionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ReligionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ReligionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Religion('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.religions && comp.religions[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
