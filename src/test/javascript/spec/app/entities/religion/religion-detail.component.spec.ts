import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ReligionDetailComponent } from 'app/entities/religion/religion-detail.component';
import { Religion } from 'app/shared/model/religion.model';

describe('Component Tests', () => {
  describe('Religion Management Detail Component', () => {
    let comp: ReligionDetailComponent;
    let fixture: ComponentFixture<ReligionDetailComponent>;
    const route = ({ data: of({ religion: new Religion('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ReligionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ReligionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ReligionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load religion on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.religion).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
