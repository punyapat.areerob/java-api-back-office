import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ReligionUpdateComponent } from 'app/entities/religion/religion-update.component';
import { ReligionService } from 'app/entities/religion/religion.service';
import { Religion } from 'app/shared/model/religion.model';

describe('Component Tests', () => {
  describe('Religion Management Update Component', () => {
    let comp: ReligionUpdateComponent;
    let fixture: ComponentFixture<ReligionUpdateComponent>;
    let service: ReligionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ReligionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ReligionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ReligionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ReligionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Religion('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Religion();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
