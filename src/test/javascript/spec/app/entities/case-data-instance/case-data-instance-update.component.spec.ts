import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseDataInstanceUpdateComponent } from 'app/entities/case-data-instance/case-data-instance-update.component';
import { CaseDataInstanceService } from 'app/entities/case-data-instance/case-data-instance.service';
import { CaseDataInstance } from 'app/shared/model/case-data-instance.model';

describe('Component Tests', () => {
  describe('CaseDataInstance Management Update Component', () => {
    let comp: CaseDataInstanceUpdateComponent;
    let fixture: ComponentFixture<CaseDataInstanceUpdateComponent>;
    let service: CaseDataInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseDataInstanceUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseDataInstanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseDataInstanceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseDataInstanceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseDataInstance('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseDataInstance();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
