import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseDataInstanceDetailComponent } from 'app/entities/case-data-instance/case-data-instance-detail.component';
import { CaseDataInstance } from 'app/shared/model/case-data-instance.model';

describe('Component Tests', () => {
  describe('CaseDataInstance Management Detail Component', () => {
    let comp: CaseDataInstanceDetailComponent;
    let fixture: ComponentFixture<CaseDataInstanceDetailComponent>;
    const route = ({ data: of({ caseDataInstance: new CaseDataInstance('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseDataInstanceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseDataInstanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseDataInstanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseDataInstance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseDataInstance).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
