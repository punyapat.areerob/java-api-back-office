import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseDataInstanceComponent } from 'app/entities/case-data-instance/case-data-instance.component';
import { CaseDataInstanceService } from 'app/entities/case-data-instance/case-data-instance.service';
import { CaseDataInstance } from 'app/shared/model/case-data-instance.model';

describe('Component Tests', () => {
  describe('CaseDataInstance Management Component', () => {
    let comp: CaseDataInstanceComponent;
    let fixture: ComponentFixture<CaseDataInstanceComponent>;
    let service: CaseDataInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseDataInstanceComponent],
      })
        .overrideTemplate(CaseDataInstanceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseDataInstanceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseDataInstanceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseDataInstance('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseDataInstances && comp.caseDataInstances[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
