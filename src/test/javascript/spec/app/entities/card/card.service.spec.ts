import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { CardService } from 'app/entities/card/card.service';
import { ICard, Card } from 'app/shared/model/card.model';
import { CardStatus } from 'app/shared/model/enumerations/card-status.model';
import { CardType } from 'app/shared/model/enumerations/card-type.model';

describe('Service Tests', () => {
  describe('Card Service', () => {
    let injector: TestBed;
    let service: CardService;
    let httpMock: HttpTestingController;
    let elemDefault: ICard;
    let expectedResult: ICard | ICard[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CardService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Card(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        CardStatus.ACTIVE,
        CardType.REGULAR,
        'AAAAAAA',
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            activeStartDate: currentDate.format(DATE_TIME_FORMAT),
            activeEndDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Card', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            activeStartDate: currentDate.format(DATE_TIME_FORMAT),
            activeEndDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            activeStartDate: currentDate,
            activeEndDate: currentDate,
          },
          returnedFromService
        );

        service.create(new Card()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Card', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            abbreviation: 'BBBBBB',
            description: 'BBBBBB',
            status: 'BBBBBB',
            type: 'BBBBBB',
            photo: 'BBBBBB',
            requireApplicant: 1,
            memberValidity: 1,
            memberFee: 1,
            memberFeeVat: 1,
            additionalMember: 1,
            additionalMemberFee: 1,
            numberOfTransfer: 1,
            transferFee: 1,
            annualFee: 1,
            annualFeeVat: 1,
            ageMin: 1,
            ageMax: 1,
            visaPeriod: 1,
            activeStartDate: currentDate.format(DATE_TIME_FORMAT),
            activeEndDate: currentDate.format(DATE_TIME_FORMAT),
            memberInformationAgreement: 'BBBBBB',
            memberPackageAgreement: 'BBBBBB',
            remark: 'BBBBBB',
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            activeStartDate: currentDate,
            activeEndDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Card', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            abbreviation: 'BBBBBB',
            description: 'BBBBBB',
            status: 'BBBBBB',
            type: 'BBBBBB',
            photo: 'BBBBBB',
            requireApplicant: 1,
            memberValidity: 1,
            memberFee: 1,
            memberFeeVat: 1,
            additionalMember: 1,
            additionalMemberFee: 1,
            numberOfTransfer: 1,
            transferFee: 1,
            annualFee: 1,
            annualFeeVat: 1,
            ageMin: 1,
            ageMax: 1,
            visaPeriod: 1,
            activeStartDate: currentDate.format(DATE_TIME_FORMAT),
            activeEndDate: currentDate.format(DATE_TIME_FORMAT),
            memberInformationAgreement: 'BBBBBB',
            memberPackageAgreement: 'BBBBBB',
            remark: 'BBBBBB',
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            activeStartDate: currentDate,
            activeEndDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Card', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
