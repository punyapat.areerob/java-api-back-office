import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BusinessIndustryUpdateComponent } from 'app/entities/business-industry/business-industry-update.component';
import { BusinessIndustryService } from 'app/entities/business-industry/business-industry.service';
import { BusinessIndustry } from 'app/shared/model/business-industry.model';

describe('Component Tests', () => {
  describe('BusinessIndustry Management Update Component', () => {
    let comp: BusinessIndustryUpdateComponent;
    let fixture: ComponentFixture<BusinessIndustryUpdateComponent>;
    let service: BusinessIndustryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BusinessIndustryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BusinessIndustryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BusinessIndustryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BusinessIndustryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BusinessIndustry('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BusinessIndustry();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
