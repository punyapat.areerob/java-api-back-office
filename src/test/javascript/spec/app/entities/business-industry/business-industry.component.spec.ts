import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { BusinessIndustryComponent } from 'app/entities/business-industry/business-industry.component';
import { BusinessIndustryService } from 'app/entities/business-industry/business-industry.service';
import { BusinessIndustry } from 'app/shared/model/business-industry.model';

describe('Component Tests', () => {
  describe('BusinessIndustry Management Component', () => {
    let comp: BusinessIndustryComponent;
    let fixture: ComponentFixture<BusinessIndustryComponent>;
    let service: BusinessIndustryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BusinessIndustryComponent],
      })
        .overrideTemplate(BusinessIndustryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BusinessIndustryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BusinessIndustryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BusinessIndustry('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.businessIndustries && comp.businessIndustries[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
