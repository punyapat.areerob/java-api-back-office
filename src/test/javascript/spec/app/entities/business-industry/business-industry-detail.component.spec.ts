import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BusinessIndustryDetailComponent } from 'app/entities/business-industry/business-industry-detail.component';
import { BusinessIndustry } from 'app/shared/model/business-industry.model';

describe('Component Tests', () => {
  describe('BusinessIndustry Management Detail Component', () => {
    let comp: BusinessIndustryDetailComponent;
    let fixture: ComponentFixture<BusinessIndustryDetailComponent>;
    const route = ({ data: of({ businessIndustry: new BusinessIndustry('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BusinessIndustryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BusinessIndustryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BusinessIndustryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load businessIndustry on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.businessIndustry).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
