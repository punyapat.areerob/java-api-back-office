import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { AccommodationTypeComponent } from 'app/entities/accommodation-type/accommodation-type.component';
import { AccommodationTypeService } from 'app/entities/accommodation-type/accommodation-type.service';
import { AccommodationType } from 'app/shared/model/accommodation-type.model';

describe('Component Tests', () => {
  describe('AccommodationType Management Component', () => {
    let comp: AccommodationTypeComponent;
    let fixture: ComponentFixture<AccommodationTypeComponent>;
    let service: AccommodationTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AccommodationTypeComponent],
      })
        .overrideTemplate(AccommodationTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AccommodationTypeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AccommodationTypeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AccommodationType('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.accommodationTypes && comp.accommodationTypes[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
