import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AccommodationTypeDetailComponent } from 'app/entities/accommodation-type/accommodation-type-detail.component';
import { AccommodationType } from 'app/shared/model/accommodation-type.model';

describe('Component Tests', () => {
  describe('AccommodationType Management Detail Component', () => {
    let comp: AccommodationTypeDetailComponent;
    let fixture: ComponentFixture<AccommodationTypeDetailComponent>;
    const route = ({ data: of({ accommodationType: new AccommodationType('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AccommodationTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AccommodationTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AccommodationTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load accommodationType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.accommodationType).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
