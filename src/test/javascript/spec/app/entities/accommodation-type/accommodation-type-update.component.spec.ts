import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AccommodationTypeUpdateComponent } from 'app/entities/accommodation-type/accommodation-type-update.component';
import { AccommodationTypeService } from 'app/entities/accommodation-type/accommodation-type.service';
import { AccommodationType } from 'app/shared/model/accommodation-type.model';

describe('Component Tests', () => {
  describe('AccommodationType Management Update Component', () => {
    let comp: AccommodationTypeUpdateComponent;
    let fixture: ComponentFixture<AccommodationTypeUpdateComponent>;
    let service: AccommodationTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AccommodationTypeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AccommodationTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AccommodationTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AccommodationTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AccommodationType('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AccommodationType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
