import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BroadcastMessageUpdateComponent } from 'app/entities/broadcast-message/broadcast-message-update.component';
import { BroadcastMessageService } from 'app/entities/broadcast-message/broadcast-message.service';
import { BroadcastMessage } from 'app/shared/model/broadcast-message.model';

describe('Component Tests', () => {
  describe('BroadcastMessage Management Update Component', () => {
    let comp: BroadcastMessageUpdateComponent;
    let fixture: ComponentFixture<BroadcastMessageUpdateComponent>;
    let service: BroadcastMessageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BroadcastMessageUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BroadcastMessageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BroadcastMessageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BroadcastMessageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BroadcastMessage('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BroadcastMessage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
