import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BroadcastMessageService } from 'app/entities/broadcast-message/broadcast-message.service';
import { IBroadcastMessage, BroadcastMessage } from 'app/shared/model/broadcast-message.model';
import { BroadcastMessageStatus } from 'app/shared/model/enumerations/broadcast-message-status.model';
import { BroadcastMessageVisible } from 'app/shared/model/enumerations/broadcast-message-visible.model';

describe('Service Tests', () => {
  describe('BroadcastMessage Service', () => {
    let injector: TestBed;
    let service: BroadcastMessageService;
    let httpMock: HttpTestingController;
    let elemDefault: IBroadcastMessage;
    let expectedResult: IBroadcastMessage | IBroadcastMessage[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BroadcastMessageService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BroadcastMessage(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        BroadcastMessageStatus.DRAFT,
        BroadcastMessageVisible.TEAM,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BroadcastMessage', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            from: currentDate,
            to: currentDate,
          },
          returnedFromService
        );

        service.create(new BroadcastMessage()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BroadcastMessage', () => {
        const returnedFromService = Object.assign(
          {
            subject: 'BBBBBB',
            message: 'BBBBBB',
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            visible: 'BBBBBB',
            creatorTeamId: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            from: currentDate,
            to: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BroadcastMessage', () => {
        const returnedFromService = Object.assign(
          {
            subject: 'BBBBBB',
            message: 'BBBBBB',
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            visible: 'BBBBBB',
            creatorTeamId: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            from: currentDate,
            to: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BroadcastMessage', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
