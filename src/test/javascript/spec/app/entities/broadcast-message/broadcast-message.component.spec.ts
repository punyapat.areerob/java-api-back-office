import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { BroadcastMessageComponent } from 'app/entities/broadcast-message/broadcast-message.component';
import { BroadcastMessageService } from 'app/entities/broadcast-message/broadcast-message.service';
import { BroadcastMessage } from 'app/shared/model/broadcast-message.model';

describe('Component Tests', () => {
  describe('BroadcastMessage Management Component', () => {
    let comp: BroadcastMessageComponent;
    let fixture: ComponentFixture<BroadcastMessageComponent>;
    let service: BroadcastMessageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BroadcastMessageComponent],
      })
        .overrideTemplate(BroadcastMessageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BroadcastMessageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BroadcastMessageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BroadcastMessage('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.broadcastMessages && comp.broadcastMessages[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
