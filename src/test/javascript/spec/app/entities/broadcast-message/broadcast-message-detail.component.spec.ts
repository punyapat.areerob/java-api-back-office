import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BroadcastMessageDetailComponent } from 'app/entities/broadcast-message/broadcast-message-detail.component';
import { BroadcastMessage } from 'app/shared/model/broadcast-message.model';

describe('Component Tests', () => {
  describe('BroadcastMessage Management Detail Component', () => {
    let comp: BroadcastMessageDetailComponent;
    let fixture: ComponentFixture<BroadcastMessageDetailComponent>;
    const route = ({ data: of({ broadcastMessage: new BroadcastMessage('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BroadcastMessageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BroadcastMessageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BroadcastMessageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load broadcastMessage on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.broadcastMessage).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
