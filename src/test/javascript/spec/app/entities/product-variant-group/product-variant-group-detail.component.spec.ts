import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProductVariantGroupDetailComponent } from 'app/entities/product-variant-group/product-variant-group-detail.component';
import { ProductVariantGroup } from 'app/shared/model/product-variant-group.model';

describe('Component Tests', () => {
  describe('ProductVariantGroup Management Detail Component', () => {
    let comp: ProductVariantGroupDetailComponent;
    let fixture: ComponentFixture<ProductVariantGroupDetailComponent>;
    const route = ({ data: of({ productVariantGroup: new ProductVariantGroup('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductVariantGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProductVariantGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductVariantGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load productVariantGroup on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.productVariantGroup).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
