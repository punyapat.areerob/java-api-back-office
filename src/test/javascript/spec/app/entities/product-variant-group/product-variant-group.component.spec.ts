import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ProductVariantGroupComponent } from 'app/entities/product-variant-group/product-variant-group.component';
import { ProductVariantGroupService } from 'app/entities/product-variant-group/product-variant-group.service';
import { ProductVariantGroup } from 'app/shared/model/product-variant-group.model';

describe('Component Tests', () => {
  describe('ProductVariantGroup Management Component', () => {
    let comp: ProductVariantGroupComponent;
    let fixture: ComponentFixture<ProductVariantGroupComponent>;
    let service: ProductVariantGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductVariantGroupComponent],
      })
        .overrideTemplate(ProductVariantGroupComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductVariantGroupComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductVariantGroupService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProductVariantGroup('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.productVariantGroups && comp.productVariantGroups[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
