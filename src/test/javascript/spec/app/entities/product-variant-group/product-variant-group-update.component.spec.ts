import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProductVariantGroupUpdateComponent } from 'app/entities/product-variant-group/product-variant-group-update.component';
import { ProductVariantGroupService } from 'app/entities/product-variant-group/product-variant-group.service';
import { ProductVariantGroup } from 'app/shared/model/product-variant-group.model';

describe('Component Tests', () => {
  describe('ProductVariantGroup Management Update Component', () => {
    let comp: ProductVariantGroupUpdateComponent;
    let fixture: ComponentFixture<ProductVariantGroupUpdateComponent>;
    let service: ProductVariantGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductVariantGroupUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProductVariantGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductVariantGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductVariantGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductVariantGroup('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductVariantGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
