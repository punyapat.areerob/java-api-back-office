import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { VerifyFieldComponent } from 'app/entities/verify-field/verify-field.component';
import { VerifyFieldService } from 'app/entities/verify-field/verify-field.service';
import { VerifyField } from 'app/shared/model/verify-field.model';

describe('Component Tests', () => {
  describe('VerifyField Management Component', () => {
    let comp: VerifyFieldComponent;
    let fixture: ComponentFixture<VerifyFieldComponent>;
    let service: VerifyFieldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VerifyFieldComponent],
      })
        .overrideTemplate(VerifyFieldComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VerifyFieldComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VerifyFieldService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new VerifyField('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.verifyFields && comp.verifyFields[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
