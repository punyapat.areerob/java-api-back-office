import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VerifyFieldUpdateComponent } from 'app/entities/verify-field/verify-field-update.component';
import { VerifyFieldService } from 'app/entities/verify-field/verify-field.service';
import { VerifyField } from 'app/shared/model/verify-field.model';

describe('Component Tests', () => {
  describe('VerifyField Management Update Component', () => {
    let comp: VerifyFieldUpdateComponent;
    let fixture: ComponentFixture<VerifyFieldUpdateComponent>;
    let service: VerifyFieldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VerifyFieldUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(VerifyFieldUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VerifyFieldUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VerifyFieldService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VerifyField('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VerifyField();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
