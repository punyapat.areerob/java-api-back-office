import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VerifyFieldDetailComponent } from 'app/entities/verify-field/verify-field-detail.component';
import { VerifyField } from 'app/shared/model/verify-field.model';

describe('Component Tests', () => {
  describe('VerifyField Management Detail Component', () => {
    let comp: VerifyFieldDetailComponent;
    let fixture: ComponentFixture<VerifyFieldDetailComponent>;
    const route = ({ data: of({ verifyField: new VerifyField('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VerifyFieldDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VerifyFieldDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VerifyFieldDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load verifyField on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.verifyField).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
