import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepComponent } from 'app/entities/case-step/case-step.component';
import { CaseStepService } from 'app/entities/case-step/case-step.service';
import { CaseStep } from 'app/shared/model/case-step.model';

describe('Component Tests', () => {
  describe('CaseStep Management Component', () => {
    let comp: CaseStepComponent;
    let fixture: ComponentFixture<CaseStepComponent>;
    let service: CaseStepService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepComponent],
      })
        .overrideTemplate(CaseStepComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStepComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStepService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseStep('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseSteps && comp.caseSteps[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
