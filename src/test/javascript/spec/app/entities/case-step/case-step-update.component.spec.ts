import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepUpdateComponent } from 'app/entities/case-step/case-step-update.component';
import { CaseStepService } from 'app/entities/case-step/case-step.service';
import { CaseStep } from 'app/shared/model/case-step.model';

describe('Component Tests', () => {
  describe('CaseStep Management Update Component', () => {
    let comp: CaseStepUpdateComponent;
    let fixture: ComponentFixture<CaseStepUpdateComponent>;
    let service: CaseStepService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseStepUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStepUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStepService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStep('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStep();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
