import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepDetailComponent } from 'app/entities/case-step/case-step-detail.component';
import { CaseStep } from 'app/shared/model/case-step.model';

describe('Component Tests', () => {
  describe('CaseStep Management Detail Component', () => {
    let comp: CaseStepDetailComponent;
    let fixture: ComponentFixture<CaseStepDetailComponent>;
    const route = ({ data: of({ caseStep: new CaseStep('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseStepDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseStepDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseStep on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseStep).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
