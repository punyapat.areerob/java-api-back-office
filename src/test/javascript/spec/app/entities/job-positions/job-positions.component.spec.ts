import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { JobPositionsComponent } from 'app/entities/job-positions/job-positions.component';
import { JobPositionsService } from 'app/entities/job-positions/job-positions.service';
import { JobPositions } from 'app/shared/model/job-positions.model';

describe('Component Tests', () => {
  describe('JobPositions Management Component', () => {
    let comp: JobPositionsComponent;
    let fixture: ComponentFixture<JobPositionsComponent>;
    let service: JobPositionsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobPositionsComponent],
      })
        .overrideTemplate(JobPositionsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JobPositionsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JobPositionsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new JobPositions('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.jobPositions && comp.jobPositions[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
