import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JobPositionsUpdateComponent } from 'app/entities/job-positions/job-positions-update.component';
import { JobPositionsService } from 'app/entities/job-positions/job-positions.service';
import { JobPositions } from 'app/shared/model/job-positions.model';

describe('Component Tests', () => {
  describe('JobPositions Management Update Component', () => {
    let comp: JobPositionsUpdateComponent;
    let fixture: ComponentFixture<JobPositionsUpdateComponent>;
    let service: JobPositionsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobPositionsUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(JobPositionsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JobPositionsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JobPositionsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new JobPositions('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new JobPositions();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
