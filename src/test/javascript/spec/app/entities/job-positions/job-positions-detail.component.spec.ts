import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JobPositionsDetailComponent } from 'app/entities/job-positions/job-positions-detail.component';
import { JobPositions } from 'app/shared/model/job-positions.model';

describe('Component Tests', () => {
  describe('JobPositions Management Detail Component', () => {
    let comp: JobPositionsDetailComponent;
    let fixture: ComponentFixture<JobPositionsDetailComponent>;
    const route = ({ data: of({ jobPositions: new JobPositions('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobPositionsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(JobPositionsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JobPositionsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jobPositions on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jobPositions).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
