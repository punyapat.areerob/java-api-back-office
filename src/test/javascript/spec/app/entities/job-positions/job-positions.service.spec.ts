import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JobPositionsService } from 'app/entities/job-positions/job-positions.service';
import { IJobPositions, JobPositions } from 'app/shared/model/job-positions.model';
import { JobPositionsStatus } from 'app/shared/model/enumerations/job-positions-status.model';

describe('Service Tests', () => {
  describe('JobPositions Service', () => {
    let injector: TestBed;
    let service: JobPositionsService;
    let httpMock: HttpTestingController;
    let elemDefault: IJobPositions;
    let expectedResult: IJobPositions | IJobPositions[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(JobPositionsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new JobPositions(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        currentDate,
        currentDate,
        JobPositionsStatus.ACTIVE
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a JobPositions', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            from: currentDate,
            to: currentDate,
          },
          returnedFromService
        );

        service.create(new JobPositions()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a JobPositions', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            teamId: 'BBBBBB',
            teamName: 'BBBBBB',
            jobDescriptionTemplateId: 'BBBBBB',
            jobDescriptionTemplateName: 'BBBBBB',
            jobTitle: 'BBBBBB',
            lineOfWork: 'BBBBBB',
            department: 'BBBBBB',
            educationBackground: 'BBBBBB',
            salary: 1,
            location: 'BBBBBB',
            summary: 'BBBBBB',
            mainReponsibilities: 'BBBBBB',
            jobDescription: 'BBBBBB',
            jobQualifications: 'BBBBBB',
            profilePhoto: true,
            militaryPass: true,
            enLanguageScore: true,
            educationCertificate: true,
            driverLicense: true,
            chLanguageScore: true,
            educationQualification: true,
            citizenID: true,
            jpLanguageScore: true,
            workPermit: true,
            marriageCertificate: true,
            houseRegistry: true,
            birthCertificate: true,
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            from: currentDate,
            to: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of JobPositions', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            teamId: 'BBBBBB',
            teamName: 'BBBBBB',
            jobDescriptionTemplateId: 'BBBBBB',
            jobDescriptionTemplateName: 'BBBBBB',
            jobTitle: 'BBBBBB',
            lineOfWork: 'BBBBBB',
            department: 'BBBBBB',
            educationBackground: 'BBBBBB',
            salary: 1,
            location: 'BBBBBB',
            summary: 'BBBBBB',
            mainReponsibilities: 'BBBBBB',
            jobDescription: 'BBBBBB',
            jobQualifications: 'BBBBBB',
            profilePhoto: true,
            militaryPass: true,
            enLanguageScore: true,
            educationCertificate: true,
            driverLicense: true,
            chLanguageScore: true,
            educationQualification: true,
            citizenID: true,
            jpLanguageScore: true,
            workPermit: true,
            marriageCertificate: true,
            houseRegistry: true,
            birthCertificate: true,
            from: currentDate.format(DATE_TIME_FORMAT),
            to: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            from: currentDate,
            to: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a JobPositions', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
