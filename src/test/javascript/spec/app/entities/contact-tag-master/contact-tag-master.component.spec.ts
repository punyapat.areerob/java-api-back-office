import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ContactTagMasterComponent } from 'app/entities/contact-tag-master/contact-tag-master.component';
import { ContactTagMasterService } from 'app/entities/contact-tag-master/contact-tag-master.service';
import { ContactTagMaster } from 'app/shared/model/contact-tag-master.model';

describe('Component Tests', () => {
  describe('ContactTagMaster Management Component', () => {
    let comp: ContactTagMasterComponent;
    let fixture: ComponentFixture<ContactTagMasterComponent>;
    let service: ContactTagMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ContactTagMasterComponent],
      })
        .overrideTemplate(ContactTagMasterComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContactTagMasterComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactTagMasterService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ContactTagMaster('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.contactTagMasters && comp.contactTagMasters[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
