import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ContactTagMasterUpdateComponent } from 'app/entities/contact-tag-master/contact-tag-master-update.component';
import { ContactTagMasterService } from 'app/entities/contact-tag-master/contact-tag-master.service';
import { ContactTagMaster } from 'app/shared/model/contact-tag-master.model';

describe('Component Tests', () => {
  describe('ContactTagMaster Management Update Component', () => {
    let comp: ContactTagMasterUpdateComponent;
    let fixture: ComponentFixture<ContactTagMasterUpdateComponent>;
    let service: ContactTagMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ContactTagMasterUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ContactTagMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContactTagMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactTagMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContactTagMaster('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContactTagMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
