import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ContactTagMasterDetailComponent } from 'app/entities/contact-tag-master/contact-tag-master-detail.component';
import { ContactTagMaster } from 'app/shared/model/contact-tag-master.model';

describe('Component Tests', () => {
  describe('ContactTagMaster Management Detail Component', () => {
    let comp: ContactTagMasterDetailComponent;
    let fixture: ComponentFixture<ContactTagMasterDetailComponent>;
    const route = ({ data: of({ contactTagMaster: new ContactTagMaster('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ContactTagMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ContactTagMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContactTagMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load contactTagMaster on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.contactTagMaster).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
