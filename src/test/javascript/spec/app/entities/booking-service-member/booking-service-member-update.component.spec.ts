import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingServiceMemberUpdateComponent } from 'app/entities/booking-service-member/booking-service-member-update.component';
import { BookingServiceMemberService } from 'app/entities/booking-service-member/booking-service-member.service';
import { BookingServiceMember } from 'app/shared/model/booking-service-member.model';

describe('Component Tests', () => {
  describe('BookingServiceMember Management Update Component', () => {
    let comp: BookingServiceMemberUpdateComponent;
    let fixture: ComponentFixture<BookingServiceMemberUpdateComponent>;
    let service: BookingServiceMemberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingServiceMemberUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BookingServiceMemberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingServiceMemberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingServiceMemberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingServiceMember('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingServiceMember();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
