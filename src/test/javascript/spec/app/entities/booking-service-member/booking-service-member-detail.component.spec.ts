import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingServiceMemberDetailComponent } from 'app/entities/booking-service-member/booking-service-member-detail.component';
import { BookingServiceMember } from 'app/shared/model/booking-service-member.model';

describe('Component Tests', () => {
  describe('BookingServiceMember Management Detail Component', () => {
    let comp: BookingServiceMemberDetailComponent;
    let fixture: ComponentFixture<BookingServiceMemberDetailComponent>;
    const route = ({ data: of({ bookingServiceMember: new BookingServiceMember('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingServiceMemberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookingServiceMemberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookingServiceMemberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bookingServiceMember on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bookingServiceMember).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
