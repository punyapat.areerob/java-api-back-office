import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { BookingServiceMemberComponent } from 'app/entities/booking-service-member/booking-service-member.component';
import { BookingServiceMemberService } from 'app/entities/booking-service-member/booking-service-member.service';
import { BookingServiceMember } from 'app/shared/model/booking-service-member.model';

describe('Component Tests', () => {
  describe('BookingServiceMember Management Component', () => {
    let comp: BookingServiceMemberComponent;
    let fixture: ComponentFixture<BookingServiceMemberComponent>;
    let service: BookingServiceMemberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingServiceMemberComponent],
      })
        .overrideTemplate(BookingServiceMemberComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingServiceMemberComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingServiceMemberService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BookingServiceMember('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bookingServiceMembers && comp.bookingServiceMembers[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
