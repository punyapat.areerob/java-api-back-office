import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CardPrivilegeComponent } from 'app/entities/card-privilege/card-privilege.component';
import { CardPrivilegeService } from 'app/entities/card-privilege/card-privilege.service';
import { CardPrivilege } from 'app/shared/model/card-privilege.model';

describe('Component Tests', () => {
  describe('CardPrivilege Management Component', () => {
    let comp: CardPrivilegeComponent;
    let fixture: ComponentFixture<CardPrivilegeComponent>;
    let service: CardPrivilegeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CardPrivilegeComponent],
      })
        .overrideTemplate(CardPrivilegeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CardPrivilegeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CardPrivilegeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CardPrivilege('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cardPrivileges && comp.cardPrivileges[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
