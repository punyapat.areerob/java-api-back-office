import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CardPrivilegeService } from 'app/entities/card-privilege/card-privilege.service';
import { ICardPrivilege, CardPrivilege } from 'app/shared/model/card-privilege.model';
import { CardPrivilegeValidity } from 'app/shared/model/enumerations/card-privilege-validity.model';

describe('Service Tests', () => {
  describe('CardPrivilege Service', () => {
    let injector: TestBed;
    let service: CardPrivilegeService;
    let httpMock: HttpTestingController;
    let elemDefault: ICardPrivilege;
    let expectedResult: ICardPrivilege | ICardPrivilege[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CardPrivilegeService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new CardPrivilege('ID', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0, CardPrivilegeValidity.YEARLY, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CardPrivilege', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new CardPrivilege()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CardPrivilege', () => {
        const returnedFromService = Object.assign(
          {
            cardId: 'BBBBBB',
            cardName: 'BBBBBB',
            privilegeId: 'BBBBBB',
            privilegeName: 'BBBBBB',
            quota: 1,
            validityPeriod: 'BBBBBB',
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CardPrivilege', () => {
        const returnedFromService = Object.assign(
          {
            cardId: 'BBBBBB',
            cardName: 'BBBBBB',
            privilegeId: 'BBBBBB',
            privilegeName: 'BBBBBB',
            quota: 1,
            validityPeriod: 'BBBBBB',
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CardPrivilege', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
