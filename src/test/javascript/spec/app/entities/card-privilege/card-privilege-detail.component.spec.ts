import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CardPrivilegeDetailComponent } from 'app/entities/card-privilege/card-privilege-detail.component';
import { CardPrivilege } from 'app/shared/model/card-privilege.model';

describe('Component Tests', () => {
  describe('CardPrivilege Management Detail Component', () => {
    let comp: CardPrivilegeDetailComponent;
    let fixture: ComponentFixture<CardPrivilegeDetailComponent>;
    const route = ({ data: of({ cardPrivilege: new CardPrivilege('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CardPrivilegeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CardPrivilegeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CardPrivilegeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cardPrivilege on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cardPrivilege).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
