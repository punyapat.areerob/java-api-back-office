import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CardPrivilegeUpdateComponent } from 'app/entities/card-privilege/card-privilege-update.component';
import { CardPrivilegeService } from 'app/entities/card-privilege/card-privilege.service';
import { CardPrivilege } from 'app/shared/model/card-privilege.model';

describe('Component Tests', () => {
  describe('CardPrivilege Management Update Component', () => {
    let comp: CardPrivilegeUpdateComponent;
    let fixture: ComponentFixture<CardPrivilegeUpdateComponent>;
    let service: CardPrivilegeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CardPrivilegeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CardPrivilegeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CardPrivilegeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CardPrivilegeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CardPrivilege('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CardPrivilege();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
