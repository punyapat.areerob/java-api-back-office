import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { AgentCommissionRateConfigComponent } from 'app/entities/agent-commission-rate-config/agent-commission-rate-config.component';
import { AgentCommissionRateConfigService } from 'app/entities/agent-commission-rate-config/agent-commission-rate-config.service';
import { AgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';

describe('Component Tests', () => {
  describe('AgentCommissionRateConfig Management Component', () => {
    let comp: AgentCommissionRateConfigComponent;
    let fixture: ComponentFixture<AgentCommissionRateConfigComponent>;
    let service: AgentCommissionRateConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentCommissionRateConfigComponent],
      })
        .overrideTemplate(AgentCommissionRateConfigComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentCommissionRateConfigComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentCommissionRateConfigService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AgentCommissionRateConfig('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.agentCommissionRateConfigs && comp.agentCommissionRateConfigs[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
