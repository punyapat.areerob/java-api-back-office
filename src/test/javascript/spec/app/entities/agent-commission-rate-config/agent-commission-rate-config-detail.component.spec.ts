import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentCommissionRateConfigDetailComponent } from 'app/entities/agent-commission-rate-config/agent-commission-rate-config-detail.component';
import { AgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';

describe('Component Tests', () => {
  describe('AgentCommissionRateConfig Management Detail Component', () => {
    let comp: AgentCommissionRateConfigDetailComponent;
    let fixture: ComponentFixture<AgentCommissionRateConfigDetailComponent>;
    const route = ({ data: of({ agentCommissionRateConfig: new AgentCommissionRateConfig('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentCommissionRateConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AgentCommissionRateConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentCommissionRateConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load agentCommissionRateConfig on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentCommissionRateConfig).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
