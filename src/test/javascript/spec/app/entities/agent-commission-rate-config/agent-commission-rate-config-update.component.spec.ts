import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentCommissionRateConfigUpdateComponent } from 'app/entities/agent-commission-rate-config/agent-commission-rate-config-update.component';
import { AgentCommissionRateConfigService } from 'app/entities/agent-commission-rate-config/agent-commission-rate-config.service';
import { AgentCommissionRateConfig } from 'app/shared/model/agent-commission-rate-config.model';

describe('Component Tests', () => {
  describe('AgentCommissionRateConfig Management Update Component', () => {
    let comp: AgentCommissionRateConfigUpdateComponent;
    let fixture: ComponentFixture<AgentCommissionRateConfigUpdateComponent>;
    let service: AgentCommissionRateConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentCommissionRateConfigUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AgentCommissionRateConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentCommissionRateConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentCommissionRateConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentCommissionRateConfig('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentCommissionRateConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
