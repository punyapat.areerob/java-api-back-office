import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingStatusDetailComponent } from 'app/entities/booking-status/booking-status-detail.component';
import { BookingStatus } from 'app/shared/model/booking-status.model';

describe('Component Tests', () => {
  describe('BookingStatus Management Detail Component', () => {
    let comp: BookingStatusDetailComponent;
    let fixture: ComponentFixture<BookingStatusDetailComponent>;
    const route = ({ data: of({ bookingStatus: new BookingStatus('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingStatusDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookingStatusDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookingStatusDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bookingStatus on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bookingStatus).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
