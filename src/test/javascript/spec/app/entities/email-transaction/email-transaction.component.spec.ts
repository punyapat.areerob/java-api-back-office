import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { EmailTransactionComponent } from 'app/entities/email-transaction/email-transaction.component';
import { EmailTransactionService } from 'app/entities/email-transaction/email-transaction.service';
import { EmailTransaction } from 'app/shared/model/email-transaction.model';

describe('Component Tests', () => {
  describe('EmailTransaction Management Component', () => {
    let comp: EmailTransactionComponent;
    let fixture: ComponentFixture<EmailTransactionComponent>;
    let service: EmailTransactionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [EmailTransactionComponent],
      })
        .overrideTemplate(EmailTransactionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmailTransactionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmailTransactionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new EmailTransaction('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.emailTransactions && comp.emailTransactions[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
