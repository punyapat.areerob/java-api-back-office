import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { EmailTransactionDetailComponent } from 'app/entities/email-transaction/email-transaction-detail.component';
import { EmailTransaction } from 'app/shared/model/email-transaction.model';

describe('Component Tests', () => {
  describe('EmailTransaction Management Detail Component', () => {
    let comp: EmailTransactionDetailComponent;
    let fixture: ComponentFixture<EmailTransactionDetailComponent>;
    const route = ({ data: of({ emailTransaction: new EmailTransaction('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [EmailTransactionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(EmailTransactionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EmailTransactionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load emailTransaction on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.emailTransaction).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
