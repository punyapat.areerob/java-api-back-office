import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { EmailTransactionService } from 'app/entities/email-transaction/email-transaction.service';
import { IEmailTransaction, EmailTransaction } from 'app/shared/model/email-transaction.model';
import { EmailTransactionStatus } from 'app/shared/model/enumerations/email-transaction-status.model';

describe('Service Tests', () => {
  describe('EmailTransaction Service', () => {
    let injector: TestBed;
    let service: EmailTransactionService;
    let httpMock: HttpTestingController;
    let elemDefault: IEmailTransaction;
    let expectedResult: IEmailTransaction | IEmailTransaction[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(EmailTransactionService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new EmailTransaction(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        EmailTransactionStatus.SUCCESS,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            sendDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a EmailTransaction', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            sendDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            sendDate: currentDate,
          },
          returnedFromService
        );

        service.create(new EmailTransaction()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a EmailTransaction', () => {
        const returnedFromService = Object.assign(
          {
            templateName: 'BBBBBB',
            to: 'BBBBBB',
            subject: 'BBBBBB',
            body: 'BBBBBB',
            sendDate: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            error: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            sendDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of EmailTransaction', () => {
        const returnedFromService = Object.assign(
          {
            templateName: 'BBBBBB',
            to: 'BBBBBB',
            subject: 'BBBBBB',
            body: 'BBBBBB',
            sendDate: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            error: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            sendDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a EmailTransaction', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
