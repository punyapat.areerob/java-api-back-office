import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { EmailTransactionUpdateComponent } from 'app/entities/email-transaction/email-transaction-update.component';
import { EmailTransactionService } from 'app/entities/email-transaction/email-transaction.service';
import { EmailTransaction } from 'app/shared/model/email-transaction.model';

describe('Component Tests', () => {
  describe('EmailTransaction Management Update Component', () => {
    let comp: EmailTransactionUpdateComponent;
    let fixture: ComponentFixture<EmailTransactionUpdateComponent>;
    let service: EmailTransactionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [EmailTransactionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(EmailTransactionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmailTransactionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmailTransactionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EmailTransaction('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EmailTransaction();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
