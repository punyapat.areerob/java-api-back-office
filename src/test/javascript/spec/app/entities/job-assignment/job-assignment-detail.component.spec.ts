import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JobAssignmentDetailComponent } from 'app/entities/job-assignment/job-assignment-detail.component';
import { JobAssignment } from 'app/shared/model/job-assignment.model';

describe('Component Tests', () => {
  describe('JobAssignment Management Detail Component', () => {
    let comp: JobAssignmentDetailComponent;
    let fixture: ComponentFixture<JobAssignmentDetailComponent>;
    const route = ({ data: of({ jobAssignment: new JobAssignment('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobAssignmentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(JobAssignmentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JobAssignmentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jobAssignment on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jobAssignment).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
