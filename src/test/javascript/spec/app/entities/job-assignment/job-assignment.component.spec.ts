import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { JobAssignmentComponent } from 'app/entities/job-assignment/job-assignment.component';
import { JobAssignmentService } from 'app/entities/job-assignment/job-assignment.service';
import { JobAssignment } from 'app/shared/model/job-assignment.model';

describe('Component Tests', () => {
  describe('JobAssignment Management Component', () => {
    let comp: JobAssignmentComponent;
    let fixture: ComponentFixture<JobAssignmentComponent>;
    let service: JobAssignmentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobAssignmentComponent],
      })
        .overrideTemplate(JobAssignmentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JobAssignmentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JobAssignmentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new JobAssignment('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.jobAssignments && comp.jobAssignments[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
