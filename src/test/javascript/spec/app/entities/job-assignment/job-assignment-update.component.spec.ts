import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JobAssignmentUpdateComponent } from 'app/entities/job-assignment/job-assignment-update.component';
import { JobAssignmentService } from 'app/entities/job-assignment/job-assignment.service';
import { JobAssignment } from 'app/shared/model/job-assignment.model';

describe('Component Tests', () => {
  describe('JobAssignment Management Update Component', () => {
    let comp: JobAssignmentUpdateComponent;
    let fixture: ComponentFixture<JobAssignmentUpdateComponent>;
    let service: JobAssignmentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobAssignmentUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(JobAssignmentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JobAssignmentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JobAssignmentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new JobAssignment('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new JobAssignment();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
