import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepStateUpdateComponent } from 'app/entities/case-step-state/case-step-state-update.component';
import { CaseStepStateService } from 'app/entities/case-step-state/case-step-state.service';
import { CaseStepState } from 'app/shared/model/case-step-state.model';

describe('Component Tests', () => {
  describe('CaseStepState Management Update Component', () => {
    let comp: CaseStepStateUpdateComponent;
    let fixture: ComponentFixture<CaseStepStateUpdateComponent>;
    let service: CaseStepStateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepStateUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseStepStateUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStepStateUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStepStateService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStepState('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStepState();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
