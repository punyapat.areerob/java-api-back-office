import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepStateDetailComponent } from 'app/entities/case-step-state/case-step-state-detail.component';
import { CaseStepState } from 'app/shared/model/case-step-state.model';

describe('Component Tests', () => {
  describe('CaseStepState Management Detail Component', () => {
    let comp: CaseStepStateDetailComponent;
    let fixture: ComponentFixture<CaseStepStateDetailComponent>;
    const route = ({ data: of({ caseStepState: new CaseStepState('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepStateDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseStepStateDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseStepStateDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseStepState on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseStepState).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
