import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepStateComponent } from 'app/entities/case-step-state/case-step-state.component';
import { CaseStepStateService } from 'app/entities/case-step-state/case-step-state.service';
import { CaseStepState } from 'app/shared/model/case-step-state.model';

describe('Component Tests', () => {
  describe('CaseStepState Management Component', () => {
    let comp: CaseStepStateComponent;
    let fixture: ComponentFixture<CaseStepStateComponent>;
    let service: CaseStepStateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepStateComponent],
      })
        .overrideTemplate(CaseStepStateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStepStateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStepStateService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseStepState('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseStepStates && comp.caseStepStates[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
