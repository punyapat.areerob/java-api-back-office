import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProductVariantValueUpdateComponent } from 'app/entities/product-variant-value/product-variant-value-update.component';
import { ProductVariantValueService } from 'app/entities/product-variant-value/product-variant-value.service';
import { ProductVariantValue } from 'app/shared/model/product-variant-value.model';

describe('Component Tests', () => {
  describe('ProductVariantValue Management Update Component', () => {
    let comp: ProductVariantValueUpdateComponent;
    let fixture: ComponentFixture<ProductVariantValueUpdateComponent>;
    let service: ProductVariantValueService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductVariantValueUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProductVariantValueUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductVariantValueUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductVariantValueService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductVariantValue('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductVariantValue();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
