import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProductVariantValueDetailComponent } from 'app/entities/product-variant-value/product-variant-value-detail.component';
import { ProductVariantValue } from 'app/shared/model/product-variant-value.model';

describe('Component Tests', () => {
  describe('ProductVariantValue Management Detail Component', () => {
    let comp: ProductVariantValueDetailComponent;
    let fixture: ComponentFixture<ProductVariantValueDetailComponent>;
    const route = ({ data: of({ productVariantValue: new ProductVariantValue('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductVariantValueDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProductVariantValueDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductVariantValueDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load productVariantValue on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.productVariantValue).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
