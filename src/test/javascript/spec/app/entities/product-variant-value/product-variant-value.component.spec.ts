import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ProductVariantValueComponent } from 'app/entities/product-variant-value/product-variant-value.component';
import { ProductVariantValueService } from 'app/entities/product-variant-value/product-variant-value.service';
import { ProductVariantValue } from 'app/shared/model/product-variant-value.model';

describe('Component Tests', () => {
  describe('ProductVariantValue Management Component', () => {
    let comp: ProductVariantValueComponent;
    let fixture: ComponentFixture<ProductVariantValueComponent>;
    let service: ProductVariantValueService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductVariantValueComponent],
      })
        .overrideTemplate(ProductVariantValueComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductVariantValueComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductVariantValueService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProductVariantValue('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.productVariantValues && comp.productVariantValues[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
