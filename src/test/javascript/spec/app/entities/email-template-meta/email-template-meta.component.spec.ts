import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { EmailTemplateMetaComponent } from 'app/entities/email-template-meta/email-template-meta.component';
import { EmailTemplateMetaService } from 'app/entities/email-template-meta/email-template-meta.service';
import { EmailTemplateMeta } from 'app/shared/model/email-template-meta.model';

describe('Component Tests', () => {
  describe('EmailTemplateMeta Management Component', () => {
    let comp: EmailTemplateMetaComponent;
    let fixture: ComponentFixture<EmailTemplateMetaComponent>;
    let service: EmailTemplateMetaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [EmailTemplateMetaComponent],
      })
        .overrideTemplate(EmailTemplateMetaComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmailTemplateMetaComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmailTemplateMetaService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new EmailTemplateMeta('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.emailTemplateMetas && comp.emailTemplateMetas[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
