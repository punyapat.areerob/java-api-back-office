import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { EmailTemplateMetaDetailComponent } from 'app/entities/email-template-meta/email-template-meta-detail.component';
import { EmailTemplateMeta } from 'app/shared/model/email-template-meta.model';

describe('Component Tests', () => {
  describe('EmailTemplateMeta Management Detail Component', () => {
    let comp: EmailTemplateMetaDetailComponent;
    let fixture: ComponentFixture<EmailTemplateMetaDetailComponent>;
    const route = ({ data: of({ emailTemplateMeta: new EmailTemplateMeta('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [EmailTemplateMetaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(EmailTemplateMetaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EmailTemplateMetaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load emailTemplateMeta on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.emailTemplateMeta).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
