import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { EmailTemplateMetaUpdateComponent } from 'app/entities/email-template-meta/email-template-meta-update.component';
import { EmailTemplateMetaService } from 'app/entities/email-template-meta/email-template-meta.service';
import { EmailTemplateMeta } from 'app/shared/model/email-template-meta.model';

describe('Component Tests', () => {
  describe('EmailTemplateMeta Management Update Component', () => {
    let comp: EmailTemplateMetaUpdateComponent;
    let fixture: ComponentFixture<EmailTemplateMetaUpdateComponent>;
    let service: EmailTemplateMetaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [EmailTemplateMetaUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(EmailTemplateMetaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmailTemplateMetaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EmailTemplateMetaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EmailTemplateMeta('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EmailTemplateMeta();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
