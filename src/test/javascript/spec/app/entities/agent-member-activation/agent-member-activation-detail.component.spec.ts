import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentMemberActivationDetailComponent } from 'app/entities/agent-member-activation/agent-member-activation-detail.component';
import { AgentMemberActivation } from 'app/shared/model/agent-member-activation.model';

describe('Component Tests', () => {
  describe('AgentMemberActivation Management Detail Component', () => {
    let comp: AgentMemberActivationDetailComponent;
    let fixture: ComponentFixture<AgentMemberActivationDetailComponent>;
    const route = ({ data: of({ agentMemberActivation: new AgentMemberActivation('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentMemberActivationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AgentMemberActivationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentMemberActivationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load agentMemberActivation on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentMemberActivation).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
