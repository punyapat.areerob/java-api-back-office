import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { AgentMemberActivationComponent } from 'app/entities/agent-member-activation/agent-member-activation.component';
import { AgentMemberActivationService } from 'app/entities/agent-member-activation/agent-member-activation.service';
import { AgentMemberActivation } from 'app/shared/model/agent-member-activation.model';

describe('Component Tests', () => {
  describe('AgentMemberActivation Management Component', () => {
    let comp: AgentMemberActivationComponent;
    let fixture: ComponentFixture<AgentMemberActivationComponent>;
    let service: AgentMemberActivationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentMemberActivationComponent],
      })
        .overrideTemplate(AgentMemberActivationComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentMemberActivationComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentMemberActivationService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AgentMemberActivation('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.agentMemberActivations && comp.agentMemberActivations[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
