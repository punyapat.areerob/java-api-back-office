import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JsonIntegerUpdateComponent } from 'app/entities/json-integer/json-integer-update.component';
import { JsonIntegerService } from 'app/entities/json-integer/json-integer.service';
import { JsonInteger } from 'app/shared/model/json-integer.model';

describe('Component Tests', () => {
  describe('JsonInteger Management Update Component', () => {
    let comp: JsonIntegerUpdateComponent;
    let fixture: ComponentFixture<JsonIntegerUpdateComponent>;
    let service: JsonIntegerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JsonIntegerUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(JsonIntegerUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JsonIntegerUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JsonIntegerService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new JsonInteger('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new JsonInteger();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
