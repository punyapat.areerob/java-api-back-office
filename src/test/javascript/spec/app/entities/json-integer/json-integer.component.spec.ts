import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { JsonIntegerComponent } from 'app/entities/json-integer/json-integer.component';
import { JsonIntegerService } from 'app/entities/json-integer/json-integer.service';
import { JsonInteger } from 'app/shared/model/json-integer.model';

describe('Component Tests', () => {
  describe('JsonInteger Management Component', () => {
    let comp: JsonIntegerComponent;
    let fixture: ComponentFixture<JsonIntegerComponent>;
    let service: JsonIntegerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JsonIntegerComponent],
      })
        .overrideTemplate(JsonIntegerComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JsonIntegerComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JsonIntegerService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new JsonInteger('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.jsonIntegers && comp.jsonIntegers[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
