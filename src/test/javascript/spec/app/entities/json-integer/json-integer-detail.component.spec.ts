import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JsonIntegerDetailComponent } from 'app/entities/json-integer/json-integer-detail.component';
import { JsonInteger } from 'app/shared/model/json-integer.model';

describe('Component Tests', () => {
  describe('JsonInteger Management Detail Component', () => {
    let comp: JsonIntegerDetailComponent;
    let fixture: ComponentFixture<JsonIntegerDetailComponent>;
    const route = ({ data: of({ jsonInteger: new JsonInteger('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JsonIntegerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(JsonIntegerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JsonIntegerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jsonInteger on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jsonInteger).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
