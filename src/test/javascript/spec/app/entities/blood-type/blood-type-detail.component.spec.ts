import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BloodTypeDetailComponent } from 'app/entities/blood-type/blood-type-detail.component';
import { BloodType } from 'app/shared/model/blood-type.model';

describe('Component Tests', () => {
  describe('BloodType Management Detail Component', () => {
    let comp: BloodTypeDetailComponent;
    let fixture: ComponentFixture<BloodTypeDetailComponent>;
    const route = ({ data: of({ bloodType: new BloodType('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BloodTypeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BloodTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BloodTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bloodType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bloodType).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
