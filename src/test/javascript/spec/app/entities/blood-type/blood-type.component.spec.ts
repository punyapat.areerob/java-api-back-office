import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { BloodTypeComponent } from 'app/entities/blood-type/blood-type.component';
import { BloodTypeService } from 'app/entities/blood-type/blood-type.service';
import { BloodType } from 'app/shared/model/blood-type.model';

describe('Component Tests', () => {
  describe('BloodType Management Component', () => {
    let comp: BloodTypeComponent;
    let fixture: ComponentFixture<BloodTypeComponent>;
    let service: BloodTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BloodTypeComponent],
      })
        .overrideTemplate(BloodTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BloodTypeComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BloodTypeService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BloodType('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bloodTypes && comp.bloodTypes[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
