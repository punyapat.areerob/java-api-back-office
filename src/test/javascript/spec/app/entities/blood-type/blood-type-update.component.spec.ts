import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BloodTypeUpdateComponent } from 'app/entities/blood-type/blood-type-update.component';
import { BloodTypeService } from 'app/entities/blood-type/blood-type.service';
import { BloodType } from 'app/shared/model/blood-type.model';

describe('Component Tests', () => {
  describe('BloodType Management Update Component', () => {
    let comp: BloodTypeUpdateComponent;
    let fixture: ComponentFixture<BloodTypeUpdateComponent>;
    let service: BloodTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BloodTypeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BloodTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BloodTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BloodTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BloodType('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BloodType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
