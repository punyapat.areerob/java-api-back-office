import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingServicesDetailComponent } from 'app/entities/booking-services/booking-services-detail.component';
import { BookingServices } from 'app/shared/model/booking-services.model';

describe('Component Tests', () => {
  describe('BookingServices Management Detail Component', () => {
    let comp: BookingServicesDetailComponent;
    let fixture: ComponentFixture<BookingServicesDetailComponent>;
    const route = ({ data: of({ bookingServices: new BookingServices('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingServicesDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BookingServicesDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BookingServicesDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bookingServices on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bookingServices).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
