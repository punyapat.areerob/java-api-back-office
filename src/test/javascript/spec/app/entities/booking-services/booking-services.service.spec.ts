import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BookingServicesService } from 'app/entities/booking-services/booking-services.service';
import { IBookingServices, BookingServices } from 'app/shared/model/booking-services.model';

describe('Service Tests', () => {
  describe('BookingServices Service', () => {
    let injector: TestBed;
    let service: BookingServicesService;
    let httpMock: HttpTestingController;
    let elemDefault: IBookingServices;
    let expectedResult: IBookingServices | IBookingServices[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BookingServicesService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BookingServices(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        currentDate,
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdBy: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: currentDate.format(DATE_TIME_FORMAT),
            createdAt: currentDate.format(DATE_TIME_FORMAT),
            updatedAt: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BookingServices', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            createdBy: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: currentDate.format(DATE_TIME_FORMAT),
            createdAt: currentDate.format(DATE_TIME_FORMAT),
            updatedAt: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdBy: currentDate,
            updatedBy: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );

        service.create(new BookingServices()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BookingServices', () => {
        const returnedFromService = Object.assign(
          {
            shopCategoryId: 'BBBBBB',
            shopId: 'BBBBBB',
            remark: 'BBBBBB',
            isActive: true,
            serviceName: 'BBBBBB',
            serviceId: 'BBBBBB',
            fromLatitude: 'BBBBBB',
            fromLongitude: 'BBBBBB',
            toLatitude: 'BBBBBB',
            toLongitude: 'BBBBBB',
            createdBy: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: currentDate.format(DATE_TIME_FORMAT),
            createdAt: currentDate.format(DATE_TIME_FORMAT),
            updatedAt: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdBy: currentDate,
            updatedBy: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BookingServices', () => {
        const returnedFromService = Object.assign(
          {
            shopCategoryId: 'BBBBBB',
            shopId: 'BBBBBB',
            remark: 'BBBBBB',
            isActive: true,
            serviceName: 'BBBBBB',
            serviceId: 'BBBBBB',
            fromLatitude: 'BBBBBB',
            fromLongitude: 'BBBBBB',
            toLatitude: 'BBBBBB',
            toLongitude: 'BBBBBB',
            createdBy: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: currentDate.format(DATE_TIME_FORMAT),
            createdAt: currentDate.format(DATE_TIME_FORMAT),
            updatedAt: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdBy: currentDate,
            updatedBy: currentDate,
            createdAt: currentDate,
            updatedAt: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BookingServices', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
