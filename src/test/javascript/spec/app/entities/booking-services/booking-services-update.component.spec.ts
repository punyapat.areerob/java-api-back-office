import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { BookingServicesUpdateComponent } from 'app/entities/booking-services/booking-services-update.component';
import { BookingServicesService } from 'app/entities/booking-services/booking-services.service';
import { BookingServices } from 'app/shared/model/booking-services.model';

describe('Component Tests', () => {
  describe('BookingServices Management Update Component', () => {
    let comp: BookingServicesUpdateComponent;
    let fixture: ComponentFixture<BookingServicesUpdateComponent>;
    let service: BookingServicesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingServicesUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BookingServicesUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingServicesUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingServicesService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingServices('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BookingServices();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
