import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { BookingServicesComponent } from 'app/entities/booking-services/booking-services.component';
import { BookingServicesService } from 'app/entities/booking-services/booking-services.service';
import { BookingServices } from 'app/shared/model/booking-services.model';

describe('Component Tests', () => {
  describe('BookingServices Management Component', () => {
    let comp: BookingServicesComponent;
    let fixture: ComponentFixture<BookingServicesComponent>;
    let service: BookingServicesService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [BookingServicesComponent],
      })
        .overrideTemplate(BookingServicesComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingServicesComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingServicesService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BookingServices('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bookingServices && comp.bookingServices[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
