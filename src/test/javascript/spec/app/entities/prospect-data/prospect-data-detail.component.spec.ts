import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProspectDataDetailComponent } from 'app/entities/prospect-data/prospect-data-detail.component';
import { ProspectData } from 'app/shared/model/prospect-data.model';

describe('Component Tests', () => {
  describe('ProspectData Management Detail Component', () => {
    let comp: ProspectDataDetailComponent;
    let fixture: ComponentFixture<ProspectDataDetailComponent>;
    const route = ({ data: of({ prospectData: new ProspectData('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProspectDataDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProspectDataDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProspectDataDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load prospectData on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.prospectData).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
