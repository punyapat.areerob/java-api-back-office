import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ProspectDataComponent } from 'app/entities/prospect-data/prospect-data.component';
import { ProspectDataService } from 'app/entities/prospect-data/prospect-data.service';
import { ProspectData } from 'app/shared/model/prospect-data.model';

describe('Component Tests', () => {
  describe('ProspectData Management Component', () => {
    let comp: ProspectDataComponent;
    let fixture: ComponentFixture<ProspectDataComponent>;
    let service: ProspectDataService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProspectDataComponent],
      })
        .overrideTemplate(ProspectDataComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProspectDataComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProspectDataService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProspectData('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.prospectData && comp.prospectData[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
