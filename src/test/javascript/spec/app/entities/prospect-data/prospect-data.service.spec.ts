import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ProspectDataService } from 'app/entities/prospect-data/prospect-data.service';
import { IProspectData, ProspectData } from 'app/shared/model/prospect-data.model';

describe('Service Tests', () => {
  describe('ProspectData Service', () => {
    let injector: TestBed;
    let service: ProspectDataService;
    let httpMock: HttpTestingController;
    let elemDefault: IProspectData;
    let expectedResult: IProspectData | IProspectData[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ProspectDataService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new ProspectData(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        false
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ProspectData', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ProspectData()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ProspectData', () => {
        const returnedFromService = Object.assign(
          {
            title: 'BBBBBB',
            givenName: 'BBBBBB',
            middleName: 'BBBBBB',
            surName: 'BBBBBB',
            areaCode: 'BBBBBB',
            contactNo: 'BBBBBB',
            passportNo: 'BBBBBB',
            passportExpireDate: 'BBBBBB',
            passportDateOfBirth: 'BBBBBB',
            agreePda: true,
            allowContactMe: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ProspectData', () => {
        const returnedFromService = Object.assign(
          {
            title: 'BBBBBB',
            givenName: 'BBBBBB',
            middleName: 'BBBBBB',
            surName: 'BBBBBB',
            areaCode: 'BBBBBB',
            contactNo: 'BBBBBB',
            passportNo: 'BBBBBB',
            passportExpireDate: 'BBBBBB',
            passportDateOfBirth: 'BBBBBB',
            agreePda: true,
            allowContactMe: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ProspectData', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
