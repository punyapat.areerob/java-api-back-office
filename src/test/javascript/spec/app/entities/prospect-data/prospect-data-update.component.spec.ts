import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProspectDataUpdateComponent } from 'app/entities/prospect-data/prospect-data-update.component';
import { ProspectDataService } from 'app/entities/prospect-data/prospect-data.service';
import { ProspectData } from 'app/shared/model/prospect-data.model';

describe('Component Tests', () => {
  describe('ProspectData Management Update Component', () => {
    let comp: ProspectDataUpdateComponent;
    let fixture: ComponentFixture<ProspectDataUpdateComponent>;
    let service: ProspectDataService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProspectDataUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProspectDataUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProspectDataUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProspectDataService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProspectData('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProspectData();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
