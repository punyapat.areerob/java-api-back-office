import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { MemberRemarkTagComponent } from 'app/entities/member-remark-tag/member-remark-tag.component';
import { MemberRemarkTagService } from 'app/entities/member-remark-tag/member-remark-tag.service';
import { MemberRemarkTag } from 'app/shared/model/member-remark-tag.model';

describe('Component Tests', () => {
  describe('MemberRemarkTag Management Component', () => {
    let comp: MemberRemarkTagComponent;
    let fixture: ComponentFixture<MemberRemarkTagComponent>;
    let service: MemberRemarkTagService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [MemberRemarkTagComponent],
      })
        .overrideTemplate(MemberRemarkTagComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MemberRemarkTagComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MemberRemarkTagService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MemberRemarkTag('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.memberRemarkTags && comp.memberRemarkTags[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
