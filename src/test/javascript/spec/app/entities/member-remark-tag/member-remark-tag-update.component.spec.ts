import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { MemberRemarkTagUpdateComponent } from 'app/entities/member-remark-tag/member-remark-tag-update.component';
import { MemberRemarkTagService } from 'app/entities/member-remark-tag/member-remark-tag.service';
import { MemberRemarkTag } from 'app/shared/model/member-remark-tag.model';

describe('Component Tests', () => {
  describe('MemberRemarkTag Management Update Component', () => {
    let comp: MemberRemarkTagUpdateComponent;
    let fixture: ComponentFixture<MemberRemarkTagUpdateComponent>;
    let service: MemberRemarkTagService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [MemberRemarkTagUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MemberRemarkTagUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MemberRemarkTagUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MemberRemarkTagService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MemberRemarkTag('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MemberRemarkTag();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
