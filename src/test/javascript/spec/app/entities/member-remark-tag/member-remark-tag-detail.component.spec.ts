import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { MemberRemarkTagDetailComponent } from 'app/entities/member-remark-tag/member-remark-tag-detail.component';
import { MemberRemarkTag } from 'app/shared/model/member-remark-tag.model';

describe('Component Tests', () => {
  describe('MemberRemarkTag Management Detail Component', () => {
    let comp: MemberRemarkTagDetailComponent;
    let fixture: ComponentFixture<MemberRemarkTagDetailComponent>;
    const route = ({ data: of({ memberRemarkTag: new MemberRemarkTag('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [MemberRemarkTagDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MemberRemarkTagDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MemberRemarkTagDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load memberRemarkTag on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.memberRemarkTag).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
