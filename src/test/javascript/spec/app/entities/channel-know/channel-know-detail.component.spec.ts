import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ChannelKnowDetailComponent } from 'app/entities/channel-know/channel-know-detail.component';
import { ChannelKnow } from 'app/shared/model/channel-know.model';

describe('Component Tests', () => {
  describe('ChannelKnow Management Detail Component', () => {
    let comp: ChannelKnowDetailComponent;
    let fixture: ComponentFixture<ChannelKnowDetailComponent>;
    const route = ({ data: of({ channelKnow: new ChannelKnow('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ChannelKnowDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ChannelKnowDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ChannelKnowDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load channelKnow on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.channelKnow).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
