import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ChannelKnowComponent } from 'app/entities/channel-know/channel-know.component';
import { ChannelKnowService } from 'app/entities/channel-know/channel-know.service';
import { ChannelKnow } from 'app/shared/model/channel-know.model';

describe('Component Tests', () => {
  describe('ChannelKnow Management Component', () => {
    let comp: ChannelKnowComponent;
    let fixture: ComponentFixture<ChannelKnowComponent>;
    let service: ChannelKnowService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ChannelKnowComponent],
      })
        .overrideTemplate(ChannelKnowComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChannelKnowComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ChannelKnowService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ChannelKnow('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.channelKnows && comp.channelKnows[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
