import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ChannelKnowUpdateComponent } from 'app/entities/channel-know/channel-know-update.component';
import { ChannelKnowService } from 'app/entities/channel-know/channel-know.service';
import { ChannelKnow } from 'app/shared/model/channel-know.model';

describe('Component Tests', () => {
  describe('ChannelKnow Management Update Component', () => {
    let comp: ChannelKnowUpdateComponent;
    let fixture: ComponentFixture<ChannelKnowUpdateComponent>;
    let service: ChannelKnowService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ChannelKnowUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ChannelKnowUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ChannelKnowUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ChannelKnowService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ChannelKnow('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ChannelKnow();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
