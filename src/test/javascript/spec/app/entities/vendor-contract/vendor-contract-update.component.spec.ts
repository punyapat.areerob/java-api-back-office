import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VendorContractUpdateComponent } from 'app/entities/vendor-contract/vendor-contract-update.component';
import { VendorContractService } from 'app/entities/vendor-contract/vendor-contract.service';
import { VendorContract } from 'app/shared/model/vendor-contract.model';

describe('Component Tests', () => {
  describe('VendorContract Management Update Component', () => {
    let comp: VendorContractUpdateComponent;
    let fixture: ComponentFixture<VendorContractUpdateComponent>;
    let service: VendorContractService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VendorContractUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(VendorContractUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VendorContractUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VendorContractService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VendorContract('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VendorContract();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
