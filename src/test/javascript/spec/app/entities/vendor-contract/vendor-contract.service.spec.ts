import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { VendorContractService } from 'app/entities/vendor-contract/vendor-contract.service';
import { IVendorContract, VendorContract } from 'app/shared/model/vendor-contract.model';

describe('Service Tests', () => {
  describe('VendorContract Service', () => {
    let injector: TestBed;
    let service: VendorContractService;
    let httpMock: HttpTestingController;
    let elemDefault: IVendorContract;
    let expectedResult: IVendorContract | IVendorContract[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(VendorContractService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new VendorContract('ID', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a VendorContract', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new VendorContract()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a VendorContract', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            vendorId: 'BBBBBB',
            vendorContractStartDate: 'BBBBBB',
            vendorContractEndDate: 'BBBBBB',
            vendorContractFile: 'BBBBBB',
            remark: 'BBBBBB',
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of VendorContract', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            vendorId: 'BBBBBB',
            vendorContractStartDate: 'BBBBBB',
            vendorContractEndDate: 'BBBBBB',
            vendorContractFile: 'BBBBBB',
            remark: 'BBBBBB',
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a VendorContract', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
