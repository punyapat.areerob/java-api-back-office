import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { VendorContractComponent } from 'app/entities/vendor-contract/vendor-contract.component';
import { VendorContractService } from 'app/entities/vendor-contract/vendor-contract.service';
import { VendorContract } from 'app/shared/model/vendor-contract.model';

describe('Component Tests', () => {
  describe('VendorContract Management Component', () => {
    let comp: VendorContractComponent;
    let fixture: ComponentFixture<VendorContractComponent>;
    let service: VendorContractService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VendorContractComponent],
      })
        .overrideTemplate(VendorContractComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VendorContractComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VendorContractService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new VendorContract('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.vendorContracts && comp.vendorContracts[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
