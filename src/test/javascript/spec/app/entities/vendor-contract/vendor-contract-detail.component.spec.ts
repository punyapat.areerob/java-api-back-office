import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VendorContractDetailComponent } from 'app/entities/vendor-contract/vendor-contract-detail.component';
import { VendorContract } from 'app/shared/model/vendor-contract.model';

describe('Component Tests', () => {
  describe('VendorContract Management Detail Component', () => {
    let comp: VendorContractDetailComponent;
    let fixture: ComponentFixture<VendorContractDetailComponent>;
    const route = ({ data: of({ vendorContract: new VendorContract('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VendorContractDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VendorContractDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VendorContractDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load vendorContract on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vendorContract).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
