import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepInstanceUpdateComponent } from 'app/entities/case-step-instance/case-step-instance-update.component';
import { CaseStepInstanceService } from 'app/entities/case-step-instance/case-step-instance.service';
import { CaseStepInstance } from 'app/shared/model/case-step-instance.model';

describe('Component Tests', () => {
  describe('CaseStepInstance Management Update Component', () => {
    let comp: CaseStepInstanceUpdateComponent;
    let fixture: ComponentFixture<CaseStepInstanceUpdateComponent>;
    let service: CaseStepInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepInstanceUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseStepInstanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStepInstanceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStepInstanceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStepInstance('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseStepInstance();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
