import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CaseStepInstanceService } from 'app/entities/case-step-instance/case-step-instance.service';
import { ICaseStepInstance, CaseStepInstance } from 'app/shared/model/case-step-instance.model';

describe('Service Tests', () => {
  describe('CaseStepInstance Service', () => {
    let injector: TestBed;
    let service: CaseStepInstanceService;
    let httpMock: HttpTestingController;
    let elemDefault: ICaseStepInstance;
    let expectedResult: ICaseStepInstance | ICaseStepInstance[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CaseStepInstanceService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new CaseStepInstance('ID', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CaseStepInstance', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new CaseStepInstance()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CaseStepInstance', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            stepId: 'BBBBBB',
            caseInstanceId: 'BBBBBB',
            begin: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CaseStepInstance', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            stepId: 'BBBBBB',
            caseInstanceId: 'BBBBBB',
            begin: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CaseStepInstance', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
