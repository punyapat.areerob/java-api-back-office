import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepInstanceDetailComponent } from 'app/entities/case-step-instance/case-step-instance-detail.component';
import { CaseStepInstance } from 'app/shared/model/case-step-instance.model';

describe('Component Tests', () => {
  describe('CaseStepInstance Management Detail Component', () => {
    let comp: CaseStepInstanceDetailComponent;
    let fixture: ComponentFixture<CaseStepInstanceDetailComponent>;
    const route = ({ data: of({ caseStepInstance: new CaseStepInstance('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepInstanceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseStepInstanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseStepInstanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseStepInstance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseStepInstance).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
