import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseStepInstanceComponent } from 'app/entities/case-step-instance/case-step-instance.component';
import { CaseStepInstanceService } from 'app/entities/case-step-instance/case-step-instance.service';
import { CaseStepInstance } from 'app/shared/model/case-step-instance.model';

describe('Component Tests', () => {
  describe('CaseStepInstance Management Component', () => {
    let comp: CaseStepInstanceComponent;
    let fixture: ComponentFixture<CaseStepInstanceComponent>;
    let service: CaseStepInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseStepInstanceComponent],
      })
        .overrideTemplate(CaseStepInstanceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseStepInstanceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseStepInstanceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseStepInstance('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseStepInstances && comp.caseStepInstances[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
