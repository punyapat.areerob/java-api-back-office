import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { JsonStringComponent } from 'app/entities/json-string/json-string.component';
import { JsonStringService } from 'app/entities/json-string/json-string.service';
import { JsonString } from 'app/shared/model/json-string.model';

describe('Component Tests', () => {
  describe('JsonString Management Component', () => {
    let comp: JsonStringComponent;
    let fixture: ComponentFixture<JsonStringComponent>;
    let service: JsonStringService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JsonStringComponent],
      })
        .overrideTemplate(JsonStringComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JsonStringComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JsonStringService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new JsonString('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.jsonStrings && comp.jsonStrings[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
