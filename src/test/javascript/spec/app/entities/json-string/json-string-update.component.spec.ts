import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JsonStringUpdateComponent } from 'app/entities/json-string/json-string-update.component';
import { JsonStringService } from 'app/entities/json-string/json-string.service';
import { JsonString } from 'app/shared/model/json-string.model';

describe('Component Tests', () => {
  describe('JsonString Management Update Component', () => {
    let comp: JsonStringUpdateComponent;
    let fixture: ComponentFixture<JsonStringUpdateComponent>;
    let service: JsonStringService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JsonStringUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(JsonStringUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JsonStringUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JsonStringService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new JsonString('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new JsonString();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
