import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JsonStringDetailComponent } from 'app/entities/json-string/json-string-detail.component';
import { JsonString } from 'app/shared/model/json-string.model';

describe('Component Tests', () => {
  describe('JsonString Management Detail Component', () => {
    let comp: JsonStringDetailComponent;
    let fixture: ComponentFixture<JsonStringDetailComponent>;
    const route = ({ data: of({ jsonString: new JsonString('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JsonStringDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(JsonStringDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JsonStringDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jsonString on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jsonString).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
