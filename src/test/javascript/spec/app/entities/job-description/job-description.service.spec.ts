import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { JobDescriptionService } from 'app/entities/job-description/job-description.service';
import { IJobDescription, JobDescription } from 'app/shared/model/job-description.model';

describe('Service Tests', () => {
  describe('JobDescription Service', () => {
    let injector: TestBed;
    let service: JobDescriptionService;
    let httpMock: HttpTestingController;
    let elemDefault: IJobDescription;
    let expectedResult: IJobDescription | IJobDescription[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(JobDescriptionService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new JobDescription(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a JobDescription', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new JobDescription()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a JobDescription', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            teamId: 'BBBBBB',
            teamName: 'BBBBBB',
            jobTitle: 'BBBBBB',
            lineOfWork: 'BBBBBB',
            department: 'BBBBBB',
            educationBackground: 'BBBBBB',
            salary: 1,
            location: 'BBBBBB',
            summary: 'BBBBBB',
            mainResponsibilities: 'BBBBBB',
            jobDescription: 'BBBBBB',
            jobQualifications: 'BBBBBB',
            profilePhoto: true,
            militaryPass: true,
            enLanguageScore: true,
            educationCertificate: true,
            driverLicense: true,
            chLanguageScore: true,
            educationQualification: true,
            citizenID: true,
            jpLanguageScore: true,
            workPermit: true,
            marriageCertificate: true,
            houseRegistry: true,
            birthCertificate: true,
            remark: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of JobDescription', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            teamId: 'BBBBBB',
            teamName: 'BBBBBB',
            jobTitle: 'BBBBBB',
            lineOfWork: 'BBBBBB',
            department: 'BBBBBB',
            educationBackground: 'BBBBBB',
            salary: 1,
            location: 'BBBBBB',
            summary: 'BBBBBB',
            mainResponsibilities: 'BBBBBB',
            jobDescription: 'BBBBBB',
            jobQualifications: 'BBBBBB',
            profilePhoto: true,
            militaryPass: true,
            enLanguageScore: true,
            educationCertificate: true,
            driverLicense: true,
            chLanguageScore: true,
            educationQualification: true,
            citizenID: true,
            jpLanguageScore: true,
            workPermit: true,
            marriageCertificate: true,
            houseRegistry: true,
            birthCertificate: true,
            remark: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a JobDescription', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
