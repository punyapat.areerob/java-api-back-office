import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { JobDescriptionComponent } from 'app/entities/job-description/job-description.component';
import { JobDescriptionService } from 'app/entities/job-description/job-description.service';
import { JobDescription } from 'app/shared/model/job-description.model';

describe('Component Tests', () => {
  describe('JobDescription Management Component', () => {
    let comp: JobDescriptionComponent;
    let fixture: ComponentFixture<JobDescriptionComponent>;
    let service: JobDescriptionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobDescriptionComponent],
      })
        .overrideTemplate(JobDescriptionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JobDescriptionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JobDescriptionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new JobDescription('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.jobDescriptions && comp.jobDescriptions[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
