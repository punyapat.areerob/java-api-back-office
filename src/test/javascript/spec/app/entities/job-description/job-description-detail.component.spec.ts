import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JobDescriptionDetailComponent } from 'app/entities/job-description/job-description-detail.component';
import { JobDescription } from 'app/shared/model/job-description.model';

describe('Component Tests', () => {
  describe('JobDescription Management Detail Component', () => {
    let comp: JobDescriptionDetailComponent;
    let fixture: ComponentFixture<JobDescriptionDetailComponent>;
    const route = ({ data: of({ jobDescription: new JobDescription('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobDescriptionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(JobDescriptionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(JobDescriptionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load jobDescription on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.jobDescription).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
