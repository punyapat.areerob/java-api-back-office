import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { JobDescriptionUpdateComponent } from 'app/entities/job-description/job-description-update.component';
import { JobDescriptionService } from 'app/entities/job-description/job-description.service';
import { JobDescription } from 'app/shared/model/job-description.model';

describe('Component Tests', () => {
  describe('JobDescription Management Update Component', () => {
    let comp: JobDescriptionUpdateComponent;
    let fixture: ComponentFixture<JobDescriptionUpdateComponent>;
    let service: JobDescriptionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [JobDescriptionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(JobDescriptionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JobDescriptionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JobDescriptionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new JobDescription('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new JobDescription();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
