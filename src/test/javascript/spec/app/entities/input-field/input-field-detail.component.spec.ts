import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { InputFieldDetailComponent } from 'app/entities/input-field/input-field-detail.component';
import { InputField } from 'app/shared/model/input-field.model';

describe('Component Tests', () => {
  describe('InputField Management Detail Component', () => {
    let comp: InputFieldDetailComponent;
    let fixture: ComponentFixture<InputFieldDetailComponent>;
    const route = ({ data: of({ inputField: new InputField('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [InputFieldDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(InputFieldDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InputFieldDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load inputField on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.inputField).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
