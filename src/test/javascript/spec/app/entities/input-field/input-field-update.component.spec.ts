import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { InputFieldUpdateComponent } from 'app/entities/input-field/input-field-update.component';
import { InputFieldService } from 'app/entities/input-field/input-field.service';
import { InputField } from 'app/shared/model/input-field.model';

describe('Component Tests', () => {
  describe('InputField Management Update Component', () => {
    let comp: InputFieldUpdateComponent;
    let fixture: ComponentFixture<InputFieldUpdateComponent>;
    let service: InputFieldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [InputFieldUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(InputFieldUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InputFieldUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InputFieldService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new InputField('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new InputField();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
