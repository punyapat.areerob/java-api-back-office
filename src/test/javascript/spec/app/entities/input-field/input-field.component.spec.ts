import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { InputFieldComponent } from 'app/entities/input-field/input-field.component';
import { InputFieldService } from 'app/entities/input-field/input-field.service';
import { InputField } from 'app/shared/model/input-field.model';

describe('Component Tests', () => {
  describe('InputField Management Component', () => {
    let comp: InputFieldComponent;
    let fixture: ComponentFixture<InputFieldComponent>;
    let service: InputFieldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [InputFieldComponent],
      })
        .overrideTemplate(InputFieldComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InputFieldComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InputFieldService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new InputField('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.inputFields && comp.inputFields[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
