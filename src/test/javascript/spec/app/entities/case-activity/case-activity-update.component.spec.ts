import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseActivityUpdateComponent } from 'app/entities/case-activity/case-activity-update.component';
import { CaseActivityService } from 'app/entities/case-activity/case-activity.service';
import { CaseActivity } from 'app/shared/model/case-activity.model';

describe('Component Tests', () => {
  describe('CaseActivity Management Update Component', () => {
    let comp: CaseActivityUpdateComponent;
    let fixture: ComponentFixture<CaseActivityUpdateComponent>;
    let service: CaseActivityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseActivityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseActivityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseActivityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseActivityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseActivity('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseActivity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
