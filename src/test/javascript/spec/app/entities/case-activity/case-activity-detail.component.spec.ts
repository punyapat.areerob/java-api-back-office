import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseActivityDetailComponent } from 'app/entities/case-activity/case-activity-detail.component';
import { CaseActivity } from 'app/shared/model/case-activity.model';

describe('Component Tests', () => {
  describe('CaseActivity Management Detail Component', () => {
    let comp: CaseActivityDetailComponent;
    let fixture: ComponentFixture<CaseActivityDetailComponent>;
    const route = ({ data: of({ caseActivity: new CaseActivity('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseActivityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseActivityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseActivityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseActivity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseActivity).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
