import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseActivityComponent } from 'app/entities/case-activity/case-activity.component';
import { CaseActivityService } from 'app/entities/case-activity/case-activity.service';
import { CaseActivity } from 'app/shared/model/case-activity.model';

describe('Component Tests', () => {
  describe('CaseActivity Management Component', () => {
    let comp: CaseActivityComponent;
    let fixture: ComponentFixture<CaseActivityComponent>;
    let service: CaseActivityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseActivityComponent],
      })
        .overrideTemplate(CaseActivityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseActivityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseActivityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseActivity('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseActivities && comp.caseActivities[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
