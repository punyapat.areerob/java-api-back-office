import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ShopService } from 'app/entities/shop/shop.service';
import { IShop, Shop } from 'app/shared/model/shop.model';

describe('Service Tests', () => {
  describe('Shop Service', () => {
    let injector: TestBed;
    let service: ShopService;
    let httpMock: HttpTestingController;
    let elemDefault: IShop;
    let expectedResult: IShop | IShop[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ShopService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Shop(
        'ID',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        0,
        false
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            openTime: currentDate.format(DATE_FORMAT),
            closeTime: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Shop', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            openTime: currentDate.format(DATE_FORMAT),
            closeTime: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            openTime: currentDate,
            closeTime: currentDate,
          },
          returnedFromService
        );

        service.create(new Shop()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Shop', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            vendorId: 'BBBBBB',
            email: 'BBBBBB',
            openTime: currentDate.format(DATE_FORMAT),
            closeTime: currentDate.format(DATE_FORMAT),
            mobile: 'BBBBBB',
            address: 'BBBBBB',
            subDistrict: 'BBBBBB',
            district: 'BBBBBB',
            province: 'BBBBBB',
            country: 'BBBBBB',
            postCode: 'BBBBBB',
            latitude: 1,
            longitude: 1,
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            openTime: currentDate,
            closeTime: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Shop', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            vendorId: 'BBBBBB',
            email: 'BBBBBB',
            openTime: currentDate.format(DATE_FORMAT),
            closeTime: currentDate.format(DATE_FORMAT),
            mobile: 'BBBBBB',
            address: 'BBBBBB',
            subDistrict: 'BBBBBB',
            district: 'BBBBBB',
            province: 'BBBBBB',
            country: 'BBBBBB',
            postCode: 'BBBBBB',
            latitude: 1,
            longitude: 1,
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            openTime: currentDate,
            closeTime: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Shop', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
