import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VendorsContactUpdateComponent } from 'app/entities/vendors-contact/vendors-contact-update.component';
import { VendorsContactService } from 'app/entities/vendors-contact/vendors-contact.service';
import { VendorsContact } from 'app/shared/model/vendors-contact.model';

describe('Component Tests', () => {
  describe('VendorsContact Management Update Component', () => {
    let comp: VendorsContactUpdateComponent;
    let fixture: ComponentFixture<VendorsContactUpdateComponent>;
    let service: VendorsContactService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VendorsContactUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(VendorsContactUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VendorsContactUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VendorsContactService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new VendorsContact('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new VendorsContact();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
