import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { VendorsContactComponent } from 'app/entities/vendors-contact/vendors-contact.component';
import { VendorsContactService } from 'app/entities/vendors-contact/vendors-contact.service';
import { VendorsContact } from 'app/shared/model/vendors-contact.model';

describe('Component Tests', () => {
  describe('VendorsContact Management Component', () => {
    let comp: VendorsContactComponent;
    let fixture: ComponentFixture<VendorsContactComponent>;
    let service: VendorsContactService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VendorsContactComponent],
      })
        .overrideTemplate(VendorsContactComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(VendorsContactComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VendorsContactService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new VendorsContact('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.vendorsContacts && comp.vendorsContacts[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
