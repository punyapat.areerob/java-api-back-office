import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { VendorsContactDetailComponent } from 'app/entities/vendors-contact/vendors-contact-detail.component';
import { VendorsContact } from 'app/shared/model/vendors-contact.model';

describe('Component Tests', () => {
  describe('VendorsContact Management Detail Component', () => {
    let comp: VendorsContactDetailComponent;
    let fixture: ComponentFixture<VendorsContactDetailComponent>;
    const route = ({ data: of({ vendorsContact: new VendorsContact('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [VendorsContactDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(VendorsContactDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VendorsContactDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load vendorsContact on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vendorsContact).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
