import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { StepOwnerUpdateComponent } from 'app/entities/step-owner/step-owner-update.component';
import { StepOwnerService } from 'app/entities/step-owner/step-owner.service';
import { StepOwner } from 'app/shared/model/step-owner.model';

describe('Component Tests', () => {
  describe('StepOwner Management Update Component', () => {
    let comp: StepOwnerUpdateComponent;
    let fixture: ComponentFixture<StepOwnerUpdateComponent>;
    let service: StepOwnerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [StepOwnerUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(StepOwnerUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StepOwnerUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StepOwnerService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new StepOwner('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new StepOwner();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
