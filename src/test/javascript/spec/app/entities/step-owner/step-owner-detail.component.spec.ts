import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { StepOwnerDetailComponent } from 'app/entities/step-owner/step-owner-detail.component';
import { StepOwner } from 'app/shared/model/step-owner.model';

describe('Component Tests', () => {
  describe('StepOwner Management Detail Component', () => {
    let comp: StepOwnerDetailComponent;
    let fixture: ComponentFixture<StepOwnerDetailComponent>;
    const route = ({ data: of({ stepOwner: new StepOwner('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [StepOwnerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(StepOwnerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(StepOwnerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load stepOwner on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.stepOwner).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
