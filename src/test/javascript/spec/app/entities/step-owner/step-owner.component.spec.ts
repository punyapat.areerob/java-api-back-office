import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { StepOwnerComponent } from 'app/entities/step-owner/step-owner.component';
import { StepOwnerService } from 'app/entities/step-owner/step-owner.service';
import { StepOwner } from 'app/shared/model/step-owner.model';

describe('Component Tests', () => {
  describe('StepOwner Management Component', () => {
    let comp: StepOwnerComponent;
    let fixture: ComponentFixture<StepOwnerComponent>;
    let service: StepOwnerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [StepOwnerComponent],
      })
        .overrideTemplate(StepOwnerComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StepOwnerComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StepOwnerService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new StepOwner('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.stepOwners && comp.stepOwners[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
