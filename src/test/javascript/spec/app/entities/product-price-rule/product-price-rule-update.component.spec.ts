import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProductPriceRuleUpdateComponent } from 'app/entities/product-price-rule/product-price-rule-update.component';
import { ProductPriceRuleService } from 'app/entities/product-price-rule/product-price-rule.service';
import { ProductPriceRule } from 'app/shared/model/product-price-rule.model';

describe('Component Tests', () => {
  describe('ProductPriceRule Management Update Component', () => {
    let comp: ProductPriceRuleUpdateComponent;
    let fixture: ComponentFixture<ProductPriceRuleUpdateComponent>;
    let service: ProductPriceRuleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductPriceRuleUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProductPriceRuleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductPriceRuleUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductPriceRuleService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductPriceRule('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductPriceRule();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
