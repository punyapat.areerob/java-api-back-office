import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ProductPriceRuleService } from 'app/entities/product-price-rule/product-price-rule.service';
import { IProductPriceRule, ProductPriceRule } from 'app/shared/model/product-price-rule.model';

describe('Service Tests', () => {
  describe('ProductPriceRule Service', () => {
    let injector: TestBed;
    let service: ProductPriceRuleService;
    let httpMock: HttpTestingController;
    let elemDefault: IProductPriceRule;
    let expectedResult: IProductPriceRule | IProductPriceRule[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ProductPriceRuleService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ProductPriceRule('ID', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, currentDate, false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            activeStartDate: currentDate.format(DATE_FORMAT),
            activeEndDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ProductPriceRule', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            activeStartDate: currentDate.format(DATE_FORMAT),
            activeEndDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            activeStartDate: currentDate,
            activeEndDate: currentDate,
          },
          returnedFromService
        );

        service.create(new ProductPriceRule()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ProductPriceRule', () => {
        const returnedFromService = Object.assign(
          {
            productId: 'BBBBBB',
            usedQuota: 'BBBBBB',
            priceSale: 'BBBBBB',
            priceBase: 'BBBBBB',
            status: 'BBBBBB',
            activeStartDate: currentDate.format(DATE_FORMAT),
            activeEndDate: currentDate.format(DATE_FORMAT),
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            activeStartDate: currentDate,
            activeEndDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ProductPriceRule', () => {
        const returnedFromService = Object.assign(
          {
            productId: 'BBBBBB',
            usedQuota: 'BBBBBB',
            priceSale: 'BBBBBB',
            priceBase: 'BBBBBB',
            status: 'BBBBBB',
            activeStartDate: currentDate.format(DATE_FORMAT),
            activeEndDate: currentDate.format(DATE_FORMAT),
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            activeStartDate: currentDate,
            activeEndDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ProductPriceRule', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
