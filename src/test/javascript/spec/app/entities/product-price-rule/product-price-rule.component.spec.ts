import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ProductPriceRuleComponent } from 'app/entities/product-price-rule/product-price-rule.component';
import { ProductPriceRuleService } from 'app/entities/product-price-rule/product-price-rule.service';
import { ProductPriceRule } from 'app/shared/model/product-price-rule.model';

describe('Component Tests', () => {
  describe('ProductPriceRule Management Component', () => {
    let comp: ProductPriceRuleComponent;
    let fixture: ComponentFixture<ProductPriceRuleComponent>;
    let service: ProductPriceRuleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductPriceRuleComponent],
      })
        .overrideTemplate(ProductPriceRuleComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductPriceRuleComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductPriceRuleService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProductPriceRule('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.productPriceRules && comp.productPriceRules[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
