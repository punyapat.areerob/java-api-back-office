import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ProductPriceRuleDetailComponent } from 'app/entities/product-price-rule/product-price-rule-detail.component';
import { ProductPriceRule } from 'app/shared/model/product-price-rule.model';

describe('Component Tests', () => {
  describe('ProductPriceRule Management Detail Component', () => {
    let comp: ProductPriceRuleDetailComponent;
    let fixture: ComponentFixture<ProductPriceRuleDetailComponent>;
    const route = ({ data: of({ productPriceRule: new ProductPriceRule('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ProductPriceRuleDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProductPriceRuleDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProductPriceRuleDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load productPriceRule on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.productPriceRule).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
