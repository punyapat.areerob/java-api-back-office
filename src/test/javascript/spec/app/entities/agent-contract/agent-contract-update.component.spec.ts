import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentContractUpdateComponent } from 'app/entities/agent-contract/agent-contract-update.component';
import { AgentContractService } from 'app/entities/agent-contract/agent-contract.service';
import { AgentContract } from 'app/shared/model/agent-contract.model';

describe('Component Tests', () => {
  describe('AgentContract Management Update Component', () => {
    let comp: AgentContractUpdateComponent;
    let fixture: ComponentFixture<AgentContractUpdateComponent>;
    let service: AgentContractService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentContractUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AgentContractUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentContractUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentContractService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentContract('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AgentContract();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
