import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { AgentContractComponent } from 'app/entities/agent-contract/agent-contract.component';
import { AgentContractService } from 'app/entities/agent-contract/agent-contract.service';
import { AgentContract } from 'app/shared/model/agent-contract.model';

describe('Component Tests', () => {
  describe('AgentContract Management Component', () => {
    let comp: AgentContractComponent;
    let fixture: ComponentFixture<AgentContractComponent>;
    let service: AgentContractService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentContractComponent],
      })
        .overrideTemplate(AgentContractComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AgentContractComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AgentContractService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AgentContract('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.agentContracts && comp.agentContracts[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
