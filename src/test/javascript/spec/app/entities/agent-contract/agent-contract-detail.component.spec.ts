import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { AgentContractDetailComponent } from 'app/entities/agent-contract/agent-contract-detail.component';
import { AgentContract } from 'app/shared/model/agent-contract.model';

describe('Component Tests', () => {
  describe('AgentContract Management Detail Component', () => {
    let comp: AgentContractDetailComponent;
    let fixture: ComponentFixture<AgentContractDetailComponent>;
    const route = ({ data: of({ agentContract: new AgentContract('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [AgentContractDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AgentContractDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentContractDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load agentContract on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentContract).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
