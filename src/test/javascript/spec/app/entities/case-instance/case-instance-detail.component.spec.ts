import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseInstanceDetailComponent } from 'app/entities/case-instance/case-instance-detail.component';
import { CaseInstance } from 'app/shared/model/case-instance.model';

describe('Component Tests', () => {
  describe('CaseInstance Management Detail Component', () => {
    let comp: CaseInstanceDetailComponent;
    let fixture: ComponentFixture<CaseInstanceDetailComponent>;
    const route = ({ data: of({ caseInstance: new CaseInstance('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseInstanceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaseInstanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaseInstanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caseInstance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caseInstance).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
