import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { CaseInstanceComponent } from 'app/entities/case-instance/case-instance.component';
import { CaseInstanceService } from 'app/entities/case-instance/case-instance.service';
import { CaseInstance } from 'app/shared/model/case-instance.model';

describe('Component Tests', () => {
  describe('CaseInstance Management Component', () => {
    let comp: CaseInstanceComponent;
    let fixture: ComponentFixture<CaseInstanceComponent>;
    let service: CaseInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseInstanceComponent],
      })
        .overrideTemplate(CaseInstanceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseInstanceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseInstanceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CaseInstance('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caseInstances && comp.caseInstances[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
