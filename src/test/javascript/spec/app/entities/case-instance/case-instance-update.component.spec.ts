import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { CaseInstanceUpdateComponent } from 'app/entities/case-instance/case-instance-update.component';
import { CaseInstanceService } from 'app/entities/case-instance/case-instance.service';
import { CaseInstance } from 'app/shared/model/case-instance.model';

describe('Component Tests', () => {
  describe('CaseInstance Management Update Component', () => {
    let comp: CaseInstanceUpdateComponent;
    let fixture: ComponentFixture<CaseInstanceUpdateComponent>;
    let service: CaseInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [CaseInstanceUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaseInstanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaseInstanceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaseInstanceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseInstance('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CaseInstance();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
