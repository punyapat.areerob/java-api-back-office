import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ShopBillInfoComponent } from 'app/entities/shop-bill-info/shop-bill-info.component';
import { ShopBillInfoService } from 'app/entities/shop-bill-info/shop-bill-info.service';
import { ShopBillInfo } from 'app/shared/model/shop-bill-info.model';

describe('Component Tests', () => {
  describe('ShopBillInfo Management Component', () => {
    let comp: ShopBillInfoComponent;
    let fixture: ComponentFixture<ShopBillInfoComponent>;
    let service: ShopBillInfoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ShopBillInfoComponent],
      })
        .overrideTemplate(ShopBillInfoComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShopBillInfoComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShopBillInfoService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ShopBillInfo('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.shopBillInfos && comp.shopBillInfos[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
