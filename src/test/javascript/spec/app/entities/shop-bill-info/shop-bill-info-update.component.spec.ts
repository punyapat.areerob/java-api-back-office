import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ShopBillInfoUpdateComponent } from 'app/entities/shop-bill-info/shop-bill-info-update.component';
import { ShopBillInfoService } from 'app/entities/shop-bill-info/shop-bill-info.service';
import { ShopBillInfo } from 'app/shared/model/shop-bill-info.model';

describe('Component Tests', () => {
  describe('ShopBillInfo Management Update Component', () => {
    let comp: ShopBillInfoUpdateComponent;
    let fixture: ComponentFixture<ShopBillInfoUpdateComponent>;
    let service: ShopBillInfoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ShopBillInfoUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ShopBillInfoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShopBillInfoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShopBillInfoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShopBillInfo('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShopBillInfo();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
