import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ShopBillInfoService } from 'app/entities/shop-bill-info/shop-bill-info.service';
import { IShopBillInfo, ShopBillInfo } from 'app/shared/model/shop-bill-info.model';

describe('Service Tests', () => {
  describe('ShopBillInfo Service', () => {
    let injector: TestBed;
    let service: ShopBillInfoService;
    let httpMock: HttpTestingController;
    let elemDefault: IShopBillInfo;
    let expectedResult: IShopBillInfo | IShopBillInfo[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ShopBillInfoService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new ShopBillInfo('ID', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', false);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('123').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ShopBillInfo', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ShopBillInfo()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ShopBillInfo', () => {
        const returnedFromService = Object.assign(
          {
            vendorId: 'BBBBBB',
            companyName: 'BBBBBB',
            taxId: 'BBBBBB',
            address: 'BBBBBB',
            status: 'BBBBBB',
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ShopBillInfo', () => {
        const returnedFromService = Object.assign(
          {
            vendorId: 'BBBBBB',
            companyName: 'BBBBBB',
            taxId: 'BBBBBB',
            address: 'BBBBBB',
            status: 'BBBBBB',
            active: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ShopBillInfo', () => {
        service.delete('123').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
