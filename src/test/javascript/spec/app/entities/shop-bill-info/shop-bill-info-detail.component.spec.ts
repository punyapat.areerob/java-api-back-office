import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ShopBillInfoDetailComponent } from 'app/entities/shop-bill-info/shop-bill-info-detail.component';
import { ShopBillInfo } from 'app/shared/model/shop-bill-info.model';

describe('Component Tests', () => {
  describe('ShopBillInfo Management Detail Component', () => {
    let comp: ShopBillInfoDetailComponent;
    let fixture: ComponentFixture<ShopBillInfoDetailComponent>;
    const route = ({ data: of({ shopBillInfo: new ShopBillInfo('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ShopBillInfoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ShopBillInfoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShopBillInfoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shopBillInfo on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shopBillInfo).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
