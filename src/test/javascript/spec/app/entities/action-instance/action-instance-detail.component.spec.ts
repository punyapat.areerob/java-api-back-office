import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ActionInstanceDetailComponent } from 'app/entities/action-instance/action-instance-detail.component';
import { ActionInstance } from 'app/shared/model/action-instance.model';

describe('Component Tests', () => {
  describe('ActionInstance Management Detail Component', () => {
    let comp: ActionInstanceDetailComponent;
    let fixture: ComponentFixture<ActionInstanceDetailComponent>;
    const route = ({ data: of({ actionInstance: new ActionInstance('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ActionInstanceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ActionInstanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ActionInstanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load actionInstance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.actionInstance).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
