import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MisbeTestModule } from '../../../test.module';
import { ActionInstanceUpdateComponent } from 'app/entities/action-instance/action-instance-update.component';
import { ActionInstanceService } from 'app/entities/action-instance/action-instance.service';
import { ActionInstance } from 'app/shared/model/action-instance.model';

describe('Component Tests', () => {
  describe('ActionInstance Management Update Component', () => {
    let comp: ActionInstanceUpdateComponent;
    let fixture: ComponentFixture<ActionInstanceUpdateComponent>;
    let service: ActionInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ActionInstanceUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ActionInstanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ActionInstanceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ActionInstanceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ActionInstance('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ActionInstance();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
