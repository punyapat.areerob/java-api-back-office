import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MisbeTestModule } from '../../../test.module';
import { ActionInstanceComponent } from 'app/entities/action-instance/action-instance.component';
import { ActionInstanceService } from 'app/entities/action-instance/action-instance.service';
import { ActionInstance } from 'app/shared/model/action-instance.model';

describe('Component Tests', () => {
  describe('ActionInstance Management Component', () => {
    let comp: ActionInstanceComponent;
    let fixture: ComponentFixture<ActionInstanceComponent>;
    let service: ActionInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MisbeTestModule],
        declarations: [ActionInstanceComponent],
      })
        .overrideTemplate(ActionInstanceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ActionInstanceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ActionInstanceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ActionInstance('123')],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.actionInstances && comp.actionInstances[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
