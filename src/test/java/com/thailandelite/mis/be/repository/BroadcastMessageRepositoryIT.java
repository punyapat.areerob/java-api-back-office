package com.thailandelite.mis.be.repository;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import com.thailandelite.mis.be.common.Utils;
import com.thailandelite.mis.be.domain.enumeration.BroadcastMessageStatus;
import com.thailandelite.mis.be.domain.enumeration.BroadcastMessageVisible;

import com.google.common.collect.Lists;
import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.BroadcastMessage;
import com.thailandelite.mis.be.domain.MemberRemarkInstance;
import com.thailandelite.mis.model.domain.Nationality;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.query.UntypedExampleMatcher;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link CustomAuditEventRepository}.
 */
@SpringBootTest(classes = MisbeApp.class)
public class BroadcastMessageRepositoryIT {


    public static final String MESSAGE_1 = "message_1";
    public static final String TEAM_ID = "team_id";
    @Autowired
    private BroadcastMessageRepository repository;

    @BeforeEach
    public void setup() {
        repository.deleteAll();
    }

    @Test
    public void findByExample() {
        BroadcastMessage message = getBroadcastMessage();
        message.setCreatorTeamId("team1");

        BroadcastMessage message2 = getBroadcastMessage();
        message2.setCreatorTeamId("team2");

        repository.save(message);
        repository.save(message2);

        ExampleMatcher matcher = Utils.matcher();

        BroadcastMessage message3 = new BroadcastMessage();
        message3.setCreatorTeamId("TEAM1");

        Example<BroadcastMessage> of = Example.of(message3, matcher);

        List<BroadcastMessage> list = repository.findAll(of);
        assertThat(list).size().isEqualTo(1);
    }

    @Test
    public void findByCreateByMyTeam() {
        BroadcastMessage message = getBroadcastMessage();
        message.setCreatorTeamId("team1");

        BroadcastMessage message2 = getBroadcastMessage();
        message2.setCreatorTeamId("team2");

        repository.save(message);
        repository.save(message2);

        List<BroadcastMessage> list = repository.findAllByCreatorTeamId("team1");
        assertThat(list).size().isEqualTo(1);
    }

    @Test
    public void canActiveMessage() {
        BroadcastMessage message = getBroadcastMessage();
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 2, 7, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(1);
    }

    @Test
    public void canNotFindDraftMessage() {
        BroadcastMessage message = getBroadcastMessage();
        message.setStatus(BroadcastMessageStatus.DRAFT);
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 2, 7, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(0);
    }

    @Test
    public void canNotFindComingMessage() {
        BroadcastMessage message = getBroadcastMessage();
        message.setFrom(ZonedDateTime.of(2021, 1, 3, 7, 0, 0, 0, ZoneId.systemDefault()));
        message.setStatus(BroadcastMessageStatus.DRAFT);
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 2, 7, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(0);
    }

    @Test
    public void canNotFindExpireMessage() {
        BroadcastMessage message = getBroadcastMessage();
        message.setTo(ZonedDateTime.of(2021, 1, 9, 7, 0, 0, 0, ZoneId.systemDefault()));
        message.setStatus(BroadcastMessageStatus.DRAFT);
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 10, 7, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(0);
    }

    @Test
    public void canFindMessageAtFromDate() {
        BroadcastMessage message = getBroadcastMessage();
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 1, 10, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(1);
    }

    @Test
    public void canFindMessageAtToDate() {
        BroadcastMessage message = getBroadcastMessage();
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 15, 5, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(1);
    }

    @Test
    public void canFindMessageFromAndToInSameDate() {
        BroadcastMessage message = getBroadcastMessage();
        message.setFrom(ZonedDateTime.of(2021, 1, 3, 7, 0, 0, 0, ZoneId.systemDefault()));
        message.setTo(ZonedDateTime.of(2021, 1, 3, 23, 0, 0, 0, ZoneId.systemDefault()));
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 3, 10, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByActivePeriod(TEAM_ID, from.toInstant(), BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(1);
    }

    @Test
    public void canFindExpireMessage() {
        BroadcastMessage message = getBroadcastMessage();
        repository.save(message);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 16, 10, 0, 0, 0, ZoneId.systemDefault());
        List<BroadcastMessage> list = repository.findAllByToBeforeAndStatus(from, BroadcastMessageStatus.ACTIVE);
        assertThat(list).size().isEqualTo(1);
    }

    private BroadcastMessage getBroadcastMessage() {
        BroadcastMessage message = new BroadcastMessage();
        message.setSubject("subject");
        message.setMessage(MESSAGE_1);

        ZonedDateTime from = ZonedDateTime.of(2021, 1, 1, 7, 0, 0, 0, ZoneId.systemDefault());
        message.setFrom(from);

        ZonedDateTime to = ZonedDateTime.of(2021, 1, 15, 7, 0, 0, 0, ZoneId.systemDefault());
        message.setTo(to);

        message.setStatus(BroadcastMessageStatus.ACTIVE);
        message.setCreatorTeamId(TEAM_ID);
        message.setTeamList(Lists.newArrayList(TEAM_ID));
        return message;
    }

}
