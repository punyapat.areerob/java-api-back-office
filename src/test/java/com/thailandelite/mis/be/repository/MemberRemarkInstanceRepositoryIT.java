package com.thailandelite.mis.be.repository;
import com.google.common.collect.Lists;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.MemberRemarkInstance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link CustomAuditEventRepository}.
 */
@SpringBootTest(classes = MisbeApp.class)
public class MemberRemarkInstanceRepositoryIT {

    public static final String TEAM_ID = "team_id";
    public static final String TAG_ID = "tag_id";
    public static final String MEMBER_ID = "member_id";
    public static final String REMARK_TEXT = "remark_text_1";
    @Autowired
    private MemberRemarkInstanceRepository repository;

    @BeforeEach
    public void setup() {
        repository.deleteAll();
    }

    private MemberRemarkInstance createRemark() {
        MemberRemarkInstance memberRemark = new MemberRemarkInstance();
        memberRemark.setMemberId(MEMBER_ID);
        memberRemark.setText(REMARK_TEXT);
//        memberRemark.setTagId(TAG_ID);
//        memberRemark.setTagName("tag_name");
        memberRemark.setAccessTeams(Lists.newArrayList(TEAM_ID));
        memberRemark.setActive(true);
        return memberRemark;
    }

    @Test
    public void canAddAndSearch() {
        MemberRemarkInstance memberRemark = createRemark();
        repository.save(memberRemark);

        List<MemberRemarkInstance> list = repository.findByMemberIdAndAccessTeamsAndTag(MEMBER_ID, TEAM_ID, TAG_ID);
        assertThat(list).size().isEqualTo(1);
        assertThat(list.get(0).getMemberId()).isEqualTo(MEMBER_ID);
        assertThat(list.get(0).getText()).isEqualTo(REMARK_TEXT);
    }

    @Test
    public void notValidTeamId() {
        MemberRemarkInstance memberRemark = createRemark();
        repository.save(memberRemark);

        List<MemberRemarkInstance> list = repository.findByMemberIdAndAccessTeamsAndTag(MEMBER_ID, "AA", TAG_ID);
        assertThat(list).size().isEqualTo(0);
    }

    @Test
    public void notValidMemberId() {
        MemberRemarkInstance memberRemark = createRemark();
        repository.save(memberRemark);

        List<MemberRemarkInstance> list = repository.findByMemberIdAndAccessTeamsAndTag("111", TEAM_ID, TAG_ID);
        assertThat(list).size().isEqualTo(0);
    }

    @Test
    public void canAddTwoTeam() {
        MemberRemarkInstance memberRemark = createRemark();
        memberRemark.setAccessTeams(Lists.newArrayList("SLS", "CC"));
        repository.save(memberRemark);

        List<MemberRemarkInstance> list = repository.findByMemberIdAndAccessTeamsAndTag(MEMBER_ID, "SLS", TAG_ID);
        assertThat(list).size().isEqualTo(1);
        assertThat(list.get(0).getMemberId()).isEqualTo(MEMBER_ID);
        assertThat(list.get(0).getText()).isEqualTo(REMARK_TEXT);
    }
}
