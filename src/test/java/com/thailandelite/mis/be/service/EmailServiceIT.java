package com.thailandelite.mis.be.service;
import com.google.common.collect.Maps;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.EmailTemplate;
import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.domain.SendEmailRequest;
import com.thailandelite.mis.be.repository.EmailTemplateRepository;
import com.thailandelite.mis.be.repository.EmailTransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

/**
 * Integration tests for {@link MailService}.
 */
@SpringBootTest(classes = MisbeApp.class)
public class EmailServiceIT {
    public static final String TEMPLATE_NAME = "test-template";
    public static final String SUBJECT = "Test subject";
    public static final String TO = "sedtawut@yahoo.com";
    @Autowired
    private EmailTemplateRepository emailTemplateRepository;
    @Autowired
    private EmailTransactionRepository emailTransactionRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private MailService mailService;

    @BeforeEach
    public void setup() {
        emailTemplateRepository.deleteAll();
        emailTransactionRepository.deleteAll();

        EmailTemplate template = createTemplate();
        emailTemplateRepository.save(template);
    }

    @Test
    public void canSendEmail() throws Exception {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(TO);
        request.setSubject(SUBJECT);
        request.setTemplateName(TEMPLATE_NAME);
        HashMap<String, Object> vars = Maps.newHashMap();
        vars.put("firstName", "FIRST");
        vars.put("lastName", "LAST");
        request.setVars(vars);

        emailService.sendEmailTemplate(request);

        List<EmailTransaction> all = emailTransactionRepository.findAll();
        assertThat(all).size().isEqualTo(1);

        EmailTransaction message = all.get(0);
        assertThat(message.getSubject()).isEqualTo(SUBJECT);
        assertThat(message.getTo()).isEqualTo(TO);
        assertThat(message.getBody()).isEqualTo("Hello FIRST LAST");
    }

    @Test
    public void throwIfSendWrongTemplateName() throws Exception {
        SendEmailRequest request = new SendEmailRequest();
        request.setTo(TO);
        request.setSubject(SUBJECT);
        request.setTemplateName("Wrong Template");
        HashMap<String, Object> vars = Maps.newHashMap();
        vars.put("firstName", "FIRST");
        vars.put("lastName", "LAST");
        request.setVars(vars);

        assertThrows(RuntimeException.class, () -> emailService.sendEmailTemplate(request));
    }

    @Test
    public void throwIfNameContainSpaceBar() throws Exception {
        EmailTemplate template = createTemplate();
        template.setName("Hello ");

        assertThrows(RuntimeException.class, () -> emailService.save(template));
    }

    @Test
    public void throwIfNameExist() throws Exception {
        EmailTemplate template = createTemplate();
        emailService.save(template);

        assertThrows(RuntimeException.class, () -> emailService.save(template));
    }

    private EmailTemplate createTemplate() {
        EmailTemplate template = new EmailTemplate();
        template.setName(TEMPLATE_NAME);
        template.setDescription("");
        template.setContent("Hello {{firstName}} {{lastName}}");

        HashMap<String, String> metaData = Maps.newHashMap();
        metaData.put("firstName", "Customer firstname");
        metaData.put("lastName", "Customer lastname");
        return template;
    }
}
