package com.thailandelite.mis.be.service;
import com.thailandelite.mis.model.domain.Card.AdditionalFamilyMemberType;
import com.thailandelite.mis.model.domain.Card.TransferType;
import com.thailandelite.mis.model.domain.Card.VatType;
import com.thailandelite.mis.model.domain.Card.AgeConditionType;
import com.thailandelite.mis.model.domain.Card.CardStatus;
import com.thailandelite.mis.model.domain.Card.AdditionalFamilyMemberSetting;
import com.thailandelite.mis.model.domain.Card.MembershipType;
import com.thailandelite.mis.model.domain.Card.VisaSetting;
import com.google.common.collect.Lists;
import com.thailandelite.mis.model.domain.Card.PublicStatus;
import java.time.ZonedDateTime;
import com.thailandelite.mis.model.domain.Card.AnnualFeeSetting;
import com.thailandelite.mis.model.domain.Card.TransferSetting;
import com.thailandelite.mis.model.domain.Card.AgeCondition;
import com.thailandelite.mis.model.domain.Card.MembershipFeeSetting;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.repository.*;
import com.thailandelite.mis.model.domain.Card;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link MailService}.
 */
@SpringBootTest(classes = MisbeApp.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class CardServiceIT {
    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private PrivilegeRepository privilegeRepository;
    @Autowired
    private CardPrivilegeRepository cardPrivilegeRepository;

    private Card mockEUPCardInfoNoneAnnualFee() {
        Card card = new Card();
        card.setId("12");
        card.setName("Elite Ultimate Privilege-renew Jan-Mar 21");
        card.setAbbreviation("UP");
        card.setCode("");
        card.setDescription("");
        card.setPublicStatus(PublicStatus.ACTIVE);
        card.setStatus(CardStatus.ACTIVE);
        card.setPhoto("");

        AgeCondition ageCondition = new AgeCondition();
        ageCondition.setType(AgeConditionType.MINIMUM);
        ageCondition.setAge(20);
        card.setAgeCondition(ageCondition);

        card.setMemberContactCenter(true);
        card.setGovernmentConcierges(true);
        card.setNinetyReportDay(true);
        card.setActiveStartDate(ZonedDateTime.now());
        card.setActiveEndDate(ZonedDateTime.now());
        card.setMemberInformationAgreement("");
        card.setMemberInformationAgreementFile("");
        card.setMemberCondition("");
        card.setRemark("");
        card.setMembershipType(MembershipType.REGULAR);
        card.setValidityYear(20);

        MembershipFeeSetting membershipFee = new MembershipFeeSetting();
        membershipFee.setAmount(2000000.0F);
        membershipFee.setVatType(VatType.EXCLUDE);
        card.setMembershipFee(membershipFee);

        TransferSetting transferSetting = new TransferSetting();
        transferSetting.setType(TransferType.YES);
        transferSetting.setTimes(0);
        transferSetting.setFeePercent(0);

        card.setTransferSetting(transferSetting);

        AdditionalFamilyMemberSetting additionalFamilyMemberSetting = new AdditionalFamilyMemberSetting();
        additionalFamilyMemberSetting.setType(AdditionalFamilyMemberType.YES);
        additionalFamilyMemberSetting.setAmount(0.0F);
        additionalFamilyMemberSetting.setVatType(VatType.INCLUDE);
        card.setAdditionalFamilyMemberSetting(additionalFamilyMemberSetting);

        AnnualFeeSetting annualFeeSetting = new AnnualFeeSetting();
        annualFeeSetting.setAmount(20000.0F);
        annualFeeSetting.setVatType(VatType.EXCLUDE);
        card.setAnnualFeeSetting(annualFeeSetting);

        VisaSetting visaSetting = new VisaSetting();
        visaSetting.setYear(5);
        visaSetting.setRenewAble(true);

        card.setVisaSetting(visaSetting);
        card.setUpgradeSettings(Lists.newArrayList());

        return card;
    }
}
