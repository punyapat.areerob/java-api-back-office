package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.repository.MembershipRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = MisbeApp.class)
public class MemberShipServiceIT {
    public static final String CARD_CODE = "EA";

    @Autowired()
    private MembershipRepository membershipRepository;
    @Autowired()
    private MembershipService membershipService;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @BeforeEach
    public void setup() {
        membershipRepository.deleteAll();
    }

    @Test
    public void set() {
        long initValue = sequenceGeneratorService.setSequence("TEST-SEQ", 100);
        assertThat(initValue).isEqualTo(100);

        long nextValue = sequenceGeneratorService.generateSequence("TEST-SEQ");
        assertThat(nextValue).isEqualTo(101);
    }

    @Test
    public void generateNewMemberShipId() {
        long initValue = sequenceGeneratorService.setSequence("MEMBERSHIP_EA", 4107273);
        assertThat(initValue).isEqualTo(4107273);

        String id = membershipService.generateMembershipId(CARD_CODE);
        assertThat(id).isEqualTo("EA41072745");
    }

    @Test
    public void generateNewMembershipNo() {
        long initValue = sequenceGeneratorService.setSequence("MEMBER_NO_21", 1234);
        assertThat(initValue).isEqualTo(1234);

        String membershipNo = membershipService.generateMembershipNo();
        assertThat(membershipNo).isEqualTo("21/1235");
    }
}
