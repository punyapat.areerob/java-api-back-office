package com.thailandelite.mis.be.service;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.repository.AgentCommissionRateRepository;
import com.thailandelite.mis.be.repository.AgentContractRepository;
import com.thailandelite.mis.be.repository.AgentRepository;
import com.thailandelite.mis.model.domain.agent.Agent;
import com.thailandelite.mis.model.domain.agent.AgentCommissionRate;
import com.thailandelite.mis.model.domain.agent.AgentContract;
import com.thailandelite.mis.model.domain.agent.AgentCommissionRate.Rate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = MisbeApp.class)
public class AgentServiceIT {
    public static final String AGENT_NAME = "agent_name";
    public static final String COMMISSION_ID = "100";
    public static final String AGENT_ID = "100";
    public static final String CONTRACT_ID = "100";

    @Autowired()
    private AgentRepository agentRepository;
    @Autowired()
    private AgentContractRepository agentContractRepository;
    @Autowired()
    private AgentCommissionRateRepository agentCommissionRateRepository;
    @Autowired()
    private AgentService agentService;

    @BeforeEach
    public void setup() {
        agentRepository.deleteAll();
        agentContractRepository.deleteAll();
    }

    @Test
    public void doDoSomethingShouldReturnHello() {
        String value = agentService.doDoSomething();
        assertThat(value).isEqualTo("Hello");
    }

    @Test
    public void getCommissionByAgent_5_10() {
        Agent agent = getAgent();
        agentRepository.save(agent);

        AgentContract agentContract = getAgentContract();
        agentContractRepository.save(agentContract);

        AgentCommissionRate agentCommissionRate = getAgentCommissionRate();
        agentCommissionRateRepository.save(agentCommissionRate);

        Float percent = agentService.getCommissionPercentByAgent(AGENT_ID, 5);

        assertThat(percent).isEqualTo(10);
    }

    @Test
    public void calCommissionFromMockData() {
        Agent agent = getAgent();
        agentRepository.save(agent);

        AgentContract agentContract = getAgentContract();
        agentContractRepository.save(agentContract);

        AgentCommissionRate agentCommissionRate = getAgentCommissionRate();
        agentCommissionRateRepository.save(agentCommissionRate);

        List<AgentService.MemberActivation> result = getResultMemberActivation();

        List<AgentService.MemberActivation> list = agentService.calCommisstion(AGENT_ID, agentService.getActivateMemberListByAgent(AGENT_ID));

        assertThat(list).isEqualTo(result);
    }

    private Agent getAgent() {
        Agent agent = new Agent();
        agent.setId(AGENT_ID);
        return agent;
    }

    private AgentCommissionRate getAgentCommissionRate() {
        AgentCommissionRate agentCommissionRate = new AgentCommissionRate();
        List<Rate> rates = new ArrayList<Rate>();
        Rate r1 = new Rate();
        Rate r2 = new Rate();
        Rate r3 = new Rate();

        agentCommissionRate.setId(COMMISSION_ID);

        r1.setQuantityStart(1);
        r1.setQuantityEnd(30);
        r1.setCommissionRatePercent(10f);
        rates.add(r1);
        
        r2.setQuantityStart(31);
        r2.setQuantityEnd(60);
        r2.setCommissionRatePercent(12.5f);
        rates.add(r2);
        
        r3.setQuantityStart(61);
        r3.setQuantityEnd(9999);
        r3.setCommissionRatePercent(15f);
        rates.add(r3);

        agentCommissionRate.setRates(rates);

        return agentCommissionRate;
    }

    private AgentContract getAgentContract() {
        Agent agent = getAgent();
        AgentCommissionRate agentCommissionRate = getAgentCommissionRate();
        AgentContract agentContract = new AgentContract();

        agentContract.setId(CONTRACT_ID);

        agentContract.setAgentCommissionRate(agentCommissionRate);

        agentContract.setAgent(agent);

        return agentContract;
    }

    private List<AgentService.MemberActivation> getResultMemberActivation() {
        List<AgentService.MemberActivation> lists = new ArrayList<>();
        lists.add(new AgentService.MemberActivation("1", "A", "A", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 1000000f, 65420.56f, 934579.44f, 61));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(140186.92f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(140186.92f);

        lists.add(new AgentService.MemberActivation("2", "B", "B", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 62));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("3", "C", "C", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 63));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("4", "D", "D", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 64));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("5", "E", "E", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 65));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("6", "F", "F", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 1000000f, 65420.56f, 934579.44f, 66));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(140186.92f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(140186.92f);

        lists.add(new AgentService.MemberActivation("7", "G", "G", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 67));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("8", "H", "H", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 1000000f, 65420.56f, 934579.44f, 68));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(140186.92f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(140186.92f);

        lists.add(new AgentService.MemberActivation("9", "I", "I", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 69));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("10", "J", "J", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 70));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("11", "K", "K", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 800000f, 52336.45f, 747663.55f, 71));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(112149.53f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(112149.53f);

        lists.add(new AgentService.MemberActivation("12", "L", "L", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 0f, 0f, 0f, 72));
        lists.get(lists.size()-1).setCommissionPercent(15f);

        lists.add(new AgentService.MemberActivation("13", "M", "M", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 73));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);

        lists.add(new AgentService.MemberActivation("14", "N", "N", ZonedDateTime.parse("2021-05-19T23:22:00.000+00:00[Asia/Bangkok]"), 500000f, 32710.28f, 467289.72f, 74));
        lists.get(lists.size()-1).setCommissionPercent(15f);
        lists.get(lists.size()-1).setCommissionAmount(70093.46f);
        lists.get(lists.size()-1).setCommissionVat(0f);
        lists.get(lists.size()-1).setCommissionNet(70093.46f);
        return lists;
    }
}
