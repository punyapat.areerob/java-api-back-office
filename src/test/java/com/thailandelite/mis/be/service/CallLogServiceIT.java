package com.thailandelite.mis.be.service;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.cdr.domain.Cdr;
import com.thailandelite.mis.be.cdr.domain.QueueLog;
import com.thailandelite.mis.be.cdr.repository.CdrRepository;
import com.thailandelite.mis.be.cdr.repository.QueueLogRepository;
import com.thailandelite.mis.be.repository.MemberPhoneRepository;
import com.thailandelite.mis.be.repository.MemberRepository;
import com.thailandelite.mis.be.repository.TitleRepository;
import com.thailandelite.mis.model.domain.Member;
import com.thailandelite.mis.model.domain.MemberPhone;
import com.thailandelite.mis.model.domain.master.Title;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link UserService}.
 */
@SpringBootTest(classes = MisbeApp.class)
public class CallLogServiceIT {

    @Autowired
    private CdrRepository cdrRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private TitleRepository titleRepository;
    @Autowired
    private MemberPhoneRepository memberPhoneRepository;
    @Autowired
    private CallLogService callLogService;
    @Autowired
    private QueueLogRepository queueLogRepository;

    private Cdr receiveCall;
    private Cdr abandonCall;
    private QueueLog queueLog;
    private MemberPhone memberPhone;
    private MemberPhone abandonMemberPhone;
    private Member member;
    private Member abandonMember;
    private Title title;


    @BeforeEach
    public void init() {
        // ReceiveCall
        cdrRepository.deleteAll();
        receiveCall = new Cdr();
        receiveCall.setCdrId(11);
        receiveCall.setCalldate(new Date(2021,06,22,8,30));
        receiveCall.setSrc("012345678");
        receiveCall.setDst("4444");
        receiveCall.setDisposition("ANSWERED");
        receiveCall.setUniqueid("16161616");

        abandonCall = new Cdr();
        abandonCall.setCdrId(12);
        abandonCall.setCalldate(new Date(2021,06,22,8,31));
        abandonCall.setSrc("012345679");
        abandonCall.setDst("4444");
        abandonCall.setDisposition("NO-ANSWERED");
        abandonCall.setUniqueid("17171717");

        // QueueLog
        queueLog = new QueueLog();
        queueLog.setId(13);
        queueLog.setCallid("17171717");
        queueLog.setEvent("ABANDON");


        // MemberPhone
        memberPhoneRepository.deleteAll();
        memberPhone = new MemberPhone();
        memberPhone.setId("1");
        memberPhone.setMemberId("1234");
        memberPhone.setPhoneNo("012345678");

        abandonMemberPhone = new MemberPhone();
        abandonMemberPhone.setId("2");
        abandonMemberPhone.setMemberId("1235");
        abandonMemberPhone.setPhoneNo("012345679");

        // Title
        titleRepository.deleteAll();
        title = new Title();
        title.setId("06");
        title.setName("Mr.");

        // Member
        memberRepository.deleteAll();
        member = new Member();
        member.setId("1234");
        member.setGivenName("Pitawat");
        member.setSurName("Kulkraijak");
        member.setTitle(title);

        abandonMember = new Member();
        abandonMember.setId("1235");
        abandonMember.setGivenName("Phumipat");
        abandonMember.setSurName("Palanakawong");
        abandonMember.setTitle(title);
    }

    @Test
    public void getReceiveCall() {
        CallLogService.CallingLog callingLog = new CallLogService.CallingLog();
        callingLog.setPhone("012345678");
        List<CallLogService.CallingMemberName> callingMemberNames = new ArrayList<>();
        CallLogService.CallingMemberName memberName = new CallLogService.CallingMemberName();
        memberName.setMemberId("1234");
        memberName.setTitle("Mr.");
        memberName.setFirstName("Pitawat");
        memberName.setLastName("Kulkraijak");
        callingMemberNames.add(memberName);
        callingLog.setCallingMemberNames(callingMemberNames);

        cdrRepository.save(receiveCall);
        memberPhoneRepository.save(memberPhone);
        titleRepository.save(title);
        memberRepository.save(member);

        CallLogService.CallingLog result = callLogService.ReceiveCall("4444");
        assertThat(result).isEqualTo(callingLog);
    }

    @Test
    public void getAbandonCall() {
        List<CallLogService.CallingLog> callingLogs = new ArrayList<>();
        CallLogService.CallingLog callingLog = new CallLogService.CallingLog();
        callingLog.setPhone("012345679");
        List<CallLogService.CallingMemberName> callingMemberNames = new ArrayList<>();
        CallLogService.CallingMemberName memberName = new CallLogService.CallingMemberName();
        memberName.setMemberId("1235");
        memberName.setTitle("Mr.");
        memberName.setFirstName("Phumipat");
        memberName.setLastName("Palanakawong");
        callingMemberNames.add(memberName);
        callingLog.setCallingMemberNames(callingMemberNames);
        callingLogs.add(callingLog);

        cdrRepository.save(receiveCall);
        cdrRepository.save(abandonCall);
        queueLogRepository.save(queueLog);
        memberPhoneRepository.save(memberPhone);
        memberPhoneRepository.save(abandonMemberPhone);
        titleRepository.save(title);
        memberRepository.save(member);
        memberRepository.save(abandonMember);

        List<CallLogService.CallingLog> result = callLogService.AbandonCall("4444");
        assertThat(result).isEqualTo(callingLogs);
    }

}
