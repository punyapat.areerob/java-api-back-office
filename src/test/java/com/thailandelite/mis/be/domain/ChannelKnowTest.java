package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.ChannelKnow;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class ChannelKnowTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChannelKnow.class);
        ChannelKnow channelKnow1 = new ChannelKnow();
        channelKnow1.setId("id1");
        ChannelKnow channelKnow2 = new ChannelKnow();
        channelKnow2.setId(channelKnow1.getId());
        assertThat(channelKnow1).isEqualTo(channelKnow2);
        channelKnow2.setId("id2");
        assertThat(channelKnow1).isNotEqualTo(channelKnow2);
        channelKnow1.setId(null);
        assertThat(channelKnow1).isNotEqualTo(channelKnow2);
    }
}
