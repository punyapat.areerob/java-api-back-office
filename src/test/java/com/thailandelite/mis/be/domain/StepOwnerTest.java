package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class StepOwnerTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StepOwner.class);
        StepOwner stepOwner1 = new StepOwner();
        stepOwner1.setId("id1");
        StepOwner stepOwner2 = new StepOwner();
        stepOwner2.setId(stepOwner1.getId());
        assertThat(stepOwner1).isEqualTo(stepOwner2);
        stepOwner2.setId("id2");
        assertThat(stepOwner1).isNotEqualTo(stepOwner2);
        stepOwner1.setId(null);
        assertThat(stepOwner1).isNotEqualTo(stepOwner2);
    }
}
