package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class SubProductCategoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubProductCategory.class);
        SubProductCategory subProductCategory1 = new SubProductCategory();
        subProductCategory1.setId("id1");
        SubProductCategory subProductCategory2 = new SubProductCategory();
        subProductCategory2.setId(subProductCategory1.getId());
        assertThat(subProductCategory1).isEqualTo(subProductCategory2);
        subProductCategory2.setId("id2");
        assertThat(subProductCategory1).isNotEqualTo(subProductCategory2);
        subProductCategory1.setId(null);
        assertThat(subProductCategory1).isNotEqualTo(subProductCategory2);
    }
}
