package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class EmailTemplateMetaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailTemplateMeta.class);
        EmailTemplateMeta emailTemplateMeta1 = new EmailTemplateMeta();
        emailTemplateMeta1.setId("id1");
        EmailTemplateMeta emailTemplateMeta2 = new EmailTemplateMeta();
        emailTemplateMeta2.setId(emailTemplateMeta1.getId());
        assertThat(emailTemplateMeta1).isEqualTo(emailTemplateMeta2);
        emailTemplateMeta2.setId("id2");
        assertThat(emailTemplateMeta1).isNotEqualTo(emailTemplateMeta2);
        emailTemplateMeta1.setId(null);
        assertThat(emailTemplateMeta1).isNotEqualTo(emailTemplateMeta2);
    }
}
