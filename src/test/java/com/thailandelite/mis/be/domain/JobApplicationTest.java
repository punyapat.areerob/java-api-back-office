package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.JobApplication;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class JobApplicationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobApplication.class);
        JobApplication jobApplication1 = new JobApplication();
        jobApplication1.setId("id1");
        JobApplication jobApplication2 = new JobApplication();
        jobApplication2.setId(jobApplication1.getId());
        assertThat(jobApplication1).isEqualTo(jobApplication2);
        jobApplication2.setId("id2");
        assertThat(jobApplication1).isNotEqualTo(jobApplication2);
        jobApplication1.setId(null);
        assertThat(jobApplication1).isNotEqualTo(jobApplication2);
    }
}
