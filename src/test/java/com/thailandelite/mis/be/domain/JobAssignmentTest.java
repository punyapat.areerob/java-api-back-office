package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.JobAssignment;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class JobAssignmentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobAssignment.class);
        JobAssignment jobAssignment1 = new JobAssignment();
        jobAssignment1.setId("id1");
        JobAssignment jobAssignment2 = new JobAssignment();
        jobAssignment2.setId(jobAssignment1.getId());
        assertThat(jobAssignment1).isEqualTo(jobAssignment2);
        jobAssignment2.setId("id2");
        assertThat(jobAssignment1).isNotEqualTo(jobAssignment2);
        jobAssignment1.setId(null);
        assertThat(jobAssignment1).isNotEqualTo(jobAssignment2);
    }
}
