package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class MemberRemarkTagTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MemberRemarkTag.class);
        MemberRemarkTag memberRemarkTag1 = new MemberRemarkTag();
        memberRemarkTag1.setId("id1");
        MemberRemarkTag memberRemarkTag2 = new MemberRemarkTag();
        memberRemarkTag2.setId(memberRemarkTag1.getId());
        assertThat(memberRemarkTag1).isEqualTo(memberRemarkTag2);
        memberRemarkTag2.setId("id2");
        assertThat(memberRemarkTag1).isNotEqualTo(memberRemarkTag2);
        memberRemarkTag1.setId(null);
        assertThat(memberRemarkTag1).isNotEqualTo(memberRemarkTag2);
    }
}
