package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.JobDescription;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class JobDescriptionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobDescription.class);
        JobDescription jobDescription1 = new JobDescription();
        jobDescription1.setId("id1");
        JobDescription jobDescription2 = new JobDescription();
        jobDescription2.setId(jobDescription1.getId());
        assertThat(jobDescription1).isEqualTo(jobDescription2);
        jobDescription2.setId("id2");
        assertThat(jobDescription1).isNotEqualTo(jobDescription2);
        jobDescription1.setId(null);
        assertThat(jobDescription1).isNotEqualTo(jobDescription2);
    }
}
