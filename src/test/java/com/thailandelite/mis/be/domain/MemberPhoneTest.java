package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.MemberPhone;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class MemberPhoneTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MemberPhone.class);
        MemberPhone memberPhone1 = new MemberPhone();
        memberPhone1.setId("id1");
        MemberPhone memberPhone2 = new MemberPhone();
        memberPhone2.setId(memberPhone1.getId());
        assertThat(memberPhone1).isEqualTo(memberPhone2);
        memberPhone2.setId("id2");
        assertThat(memberPhone1).isNotEqualTo(memberPhone2);
        memberPhone1.setId(null);
        assertThat(memberPhone1).isNotEqualTo(memberPhone2);
    }
}
