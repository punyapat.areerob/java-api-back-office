package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class ActionInstanceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActionInstance.class);
        ActionInstance actionInstance1 = new ActionInstance();
        actionInstance1.setId("id1");
        ActionInstance actionInstance2 = new ActionInstance();
        actionInstance2.setId(actionInstance1.getId());
        assertThat(actionInstance1).isEqualTo(actionInstance2);
        actionInstance2.setId("id2");
        assertThat(actionInstance1).isNotEqualTo(actionInstance2);
        actionInstance1.setId(null);
        assertThat(actionInstance1).isNotEqualTo(actionInstance2);
    }
}
