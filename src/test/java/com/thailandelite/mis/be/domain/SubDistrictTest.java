package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.SubDistrict;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class SubDistrictTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubDistrict.class);
        SubDistrict subDistrict1 = new SubDistrict();
        subDistrict1.setId("id1");
        SubDistrict subDistrict2 = new SubDistrict();
        subDistrict2.setId(subDistrict1.getId());
        assertThat(subDistrict1).isEqualTo(subDistrict2);
        subDistrict2.setId("id2");
        assertThat(subDistrict1).isNotEqualTo(subDistrict2);
        subDistrict1.setId(null);
        assertThat(subDistrict1).isNotEqualTo(subDistrict2);
    }
}
