package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CaseStepInstanceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseStepInstance.class);
        CaseStepInstance caseStepInstance1 = new CaseStepInstance();
        caseStepInstance1.setId("id1");
        CaseStepInstance caseStepInstance2 = new CaseStepInstance();
        caseStepInstance2.setId(caseStepInstance1.getId());
        assertThat(caseStepInstance1).isEqualTo(caseStepInstance2);
        caseStepInstance2.setId("id2");
        assertThat(caseStepInstance1).isNotEqualTo(caseStepInstance2);
        caseStepInstance1.setId(null);
        assertThat(caseStepInstance1).isNotEqualTo(caseStepInstance2);
    }
}
