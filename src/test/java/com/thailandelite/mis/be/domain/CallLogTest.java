package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CallLogTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CallLog.class);
        CallLog callLog1 = new CallLog();
        callLog1.setId("id1");
        CallLog callLog2 = new CallLog();
        callLog2.setId(callLog1.getId());
        assertThat(callLog1).isEqualTo(callLog2);
        callLog2.setId("id2");
        assertThat(callLog1).isNotEqualTo(callLog2);
        callLog1.setId(null);
        assertThat(callLog1).isNotEqualTo(callLog2);
    }
}
