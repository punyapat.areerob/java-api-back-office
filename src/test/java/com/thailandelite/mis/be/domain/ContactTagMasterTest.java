package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class ContactTagMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactTagMaster.class);
        ContactTagMaster contactTagMaster1 = new ContactTagMaster();
        contactTagMaster1.setId("id1");
        ContactTagMaster contactTagMaster2 = new ContactTagMaster();
        contactTagMaster2.setId(contactTagMaster1.getId());
        assertThat(contactTagMaster1).isEqualTo(contactTagMaster2);
        contactTagMaster2.setId("id2");
        assertThat(contactTagMaster1).isNotEqualTo(contactTagMaster2);
        contactTagMaster1.setId(null);
        assertThat(contactTagMaster1).isNotEqualTo(contactTagMaster2);
    }
}
