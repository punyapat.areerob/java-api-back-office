package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.VisaType;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class VisaTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VisaType.class);
        VisaType visaType1 = new VisaType();
        visaType1.setId("id1");
        VisaType visaType2 = new VisaType();
        visaType2.setId(visaType1.getId());
        assertThat(visaType1).isEqualTo(visaType2);
        visaType2.setId("id2");
        assertThat(visaType1).isNotEqualTo(visaType2);
        visaType1.setId(null);
        assertThat(visaType1).isNotEqualTo(visaType2);
    }
}
