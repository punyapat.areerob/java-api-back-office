package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CaseActivityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseActivity.class);
        CaseActivity caseActivity1 = new CaseActivity();
        caseActivity1.setId("id1");
        CaseActivity caseActivity2 = new CaseActivity();
        caseActivity2.setId(caseActivity1.getId());
        assertThat(caseActivity1).isEqualTo(caseActivity2);
        caseActivity2.setId("id2");
        assertThat(caseActivity1).isNotEqualTo(caseActivity2);
        caseActivity1.setId(null);
        assertThat(caseActivity1).isNotEqualTo(caseActivity2);
    }
}
