package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CaseStepStateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseStepState.class);
        CaseStepState caseStepState1 = new CaseStepState();
        caseStepState1.setId("id1");
        CaseStepState caseStepState2 = new CaseStepState();
        caseStepState2.setId(caseStepState1.getId());
        assertThat(caseStepState1).isEqualTo(caseStepState2);
        caseStepState2.setId("id2");
        assertThat(caseStepState1).isNotEqualTo(caseStepState2);
        caseStepState1.setId(null);
        assertThat(caseStepState1).isNotEqualTo(caseStepState2);
    }
}
