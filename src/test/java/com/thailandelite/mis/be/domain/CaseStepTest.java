package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CaseStepTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseStep.class);
        CaseStep caseStep1 = new CaseStep();
        caseStep1.setId("id1");
        CaseStep caseStep2 = new CaseStep();
        caseStep2.setId(caseStep1.getId());
        assertThat(caseStep1).isEqualTo(caseStep2);
        caseStep2.setId("id2");
        assertThat(caseStep1).isNotEqualTo(caseStep2);
        caseStep1.setId(null);
        assertThat(caseStep1).isNotEqualTo(caseStep2);
    }
}
