package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.agent.AgentCommissionRate;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class AgentCommissionRateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentCommissionRate.class);
        AgentCommissionRate agentCommissionRate1 = new AgentCommissionRate();
        agentCommissionRate1.setId("id1");
        AgentCommissionRate agentCommissionRate2 = new AgentCommissionRate();
        agentCommissionRate2.setId(agentCommissionRate1.getId());
        assertThat(agentCommissionRate1).isEqualTo(agentCommissionRate2);
        agentCommissionRate2.setId("id2");
        assertThat(agentCommissionRate1).isNotEqualTo(agentCommissionRate2);
        agentCommissionRate1.setId(null);
        assertThat(agentCommissionRate1).isNotEqualTo(agentCommissionRate2);
    }
}
