package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.AccommodationType;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class AccommodationTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccommodationType.class);
        AccommodationType accommodationType1 = new AccommodationType();
        accommodationType1.setId("id1");
        AccommodationType accommodationType2 = new AccommodationType();
        accommodationType2.setId(accommodationType1.getId());
        assertThat(accommodationType1).isEqualTo(accommodationType2);
        accommodationType2.setId("id2");
        assertThat(accommodationType1).isNotEqualTo(accommodationType2);
        accommodationType1.setId(null);
        assertThat(accommodationType1).isNotEqualTo(accommodationType2);
    }
}
