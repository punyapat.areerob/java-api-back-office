package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.agent.AgentContract;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class AgentContractTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentContract.class);
        AgentContract agentContract1 = new AgentContract();
        agentContract1.setId("id1");
        AgentContract agentContract2 = new AgentContract();
        agentContract2.setId(agentContract1.getId());
        assertThat(agentContract1).isEqualTo(agentContract2);
        agentContract2.setId("id2");
        assertThat(agentContract1).isNotEqualTo(agentContract2);
        agentContract1.setId(null);
        assertThat(agentContract1).isNotEqualTo(agentContract2);
    }
}
