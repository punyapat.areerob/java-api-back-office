package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CaseTemplateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseTemplate.class);
        CaseTemplate caseTemplate1 = new CaseTemplate();
        caseTemplate1.setId("id1");
        CaseTemplate caseTemplate2 = new CaseTemplate();
        caseTemplate2.setId(caseTemplate1.getId());
        assertThat(caseTemplate1).isEqualTo(caseTemplate2);
        caseTemplate2.setId("id2");
        assertThat(caseTemplate1).isNotEqualTo(caseTemplate2);
        caseTemplate1.setId(null);
        assertThat(caseTemplate1).isNotEqualTo(caseTemplate2);
    }
}
