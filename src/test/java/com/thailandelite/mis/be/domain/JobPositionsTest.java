package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.JobPositions;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class JobPositionsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobPositions.class);
        JobPositions jobPositions1 = new JobPositions();
        jobPositions1.setId("id1");
        JobPositions jobPositions2 = new JobPositions();
        jobPositions2.setId(jobPositions1.getId());
        assertThat(jobPositions1).isEqualTo(jobPositions2);
        jobPositions2.setId("id2");
        assertThat(jobPositions1).isNotEqualTo(jobPositions2);
        jobPositions1.setId(null);
        assertThat(jobPositions1).isNotEqualTo(jobPositions2);
    }
}
