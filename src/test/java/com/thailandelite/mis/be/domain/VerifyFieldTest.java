package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class VerifyFieldTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VerifyField.class);
        VerifyField verifyField1 = new VerifyField();
        verifyField1.setId("id1");
        VerifyField verifyField2 = new VerifyField();
        verifyField2.setId(verifyField1.getId());
        assertThat(verifyField1).isEqualTo(verifyField2);
        verifyField2.setId("id2");
        assertThat(verifyField1).isNotEqualTo(verifyField2);
        verifyField1.setId(null);
        assertThat(verifyField1).isNotEqualTo(verifyField2);
    }
}
