package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.PhonePrefix;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class PhonePrefixTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PhonePrefix.class);
        PhonePrefix phonePrefix1 = new PhonePrefix();
        phonePrefix1.setId("id1");
        PhonePrefix phonePrefix2 = new PhonePrefix();
        phonePrefix2.setId(phonePrefix1.getId());
        assertThat(phonePrefix1).isEqualTo(phonePrefix2);
        phonePrefix2.setId("id2");
        assertThat(phonePrefix1).isNotEqualTo(phonePrefix2);
        phonePrefix1.setId(null);
        assertThat(phonePrefix1).isNotEqualTo(phonePrefix2);
    }
}
