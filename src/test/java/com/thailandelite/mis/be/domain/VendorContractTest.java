package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.VendorContract;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class VendorContractTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VendorContract.class);
        VendorContract vendorContract1 = new VendorContract();
        vendorContract1.setId("id1");
        VendorContract vendorContract2 = new VendorContract();
        vendorContract2.setId(vendorContract1.getId());
        assertThat(vendorContract1).isEqualTo(vendorContract2);
        vendorContract2.setId("id2");
        assertThat(vendorContract1).isNotEqualTo(vendorContract2);
        vendorContract1.setId(null);
        assertThat(vendorContract1).isNotEqualTo(vendorContract2);
    }
}
