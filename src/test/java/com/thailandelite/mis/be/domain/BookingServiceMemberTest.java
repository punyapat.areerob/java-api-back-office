package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.booking.BookingServiceMember;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class BookingServiceMemberTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingServiceMember.class);
        BookingServiceMember bookingServiceMember1 = new BookingServiceMember();
        bookingServiceMember1.setId("id1");
        BookingServiceMember bookingServiceMember2 = new BookingServiceMember();
        bookingServiceMember2.setId(bookingServiceMember1.getId());
        assertThat(bookingServiceMember1).isEqualTo(bookingServiceMember2);
        bookingServiceMember2.setId("id2");
        assertThat(bookingServiceMember1).isNotEqualTo(bookingServiceMember2);
        bookingServiceMember1.setId(null);
        assertThat(bookingServiceMember1).isNotEqualTo(bookingServiceMember2);
    }
}
