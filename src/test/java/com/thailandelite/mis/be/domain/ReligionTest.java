package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.Religion;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class ReligionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Religion.class);
        Religion religion1 = new Religion();
        religion1.setId("id1");
        Religion religion2 = new Religion();
        religion2.setId(religion1.getId());
        assertThat(religion1).isEqualTo(religion2);
        religion2.setId("id2");
        assertThat(religion1).isNotEqualTo(religion2);
        religion1.setId(null);
        assertThat(religion1).isNotEqualTo(religion2);
    }
}
