package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CaseInstanceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaseInstance.class);
        CaseInstance caseInstance1 = new CaseInstance();
        caseInstance1.setId("id1");
        CaseInstance caseInstance2 = new CaseInstance();
        caseInstance2.setId(caseInstance1.getId());
        assertThat(caseInstance1).isEqualTo(caseInstance2);
        caseInstance2.setId("id2");
        assertThat(caseInstance1).isNotEqualTo(caseInstance2);
        caseInstance1.setId(null);
        assertThat(caseInstance1).isNotEqualTo(caseInstance2);
    }
}
