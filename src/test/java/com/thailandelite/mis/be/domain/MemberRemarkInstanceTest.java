package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class MemberRemarkInstanceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MemberRemarkInstance.class);
        MemberRemarkInstance memberRemarkInstance1 = new MemberRemarkInstance();
        memberRemarkInstance1.setId("id1");
        MemberRemarkInstance memberRemarkInstance2 = new MemberRemarkInstance();
        memberRemarkInstance2.setId(memberRemarkInstance1.getId());
        assertThat(memberRemarkInstance1).isEqualTo(memberRemarkInstance2);
        memberRemarkInstance2.setId("id2");
        assertThat(memberRemarkInstance1).isNotEqualTo(memberRemarkInstance2);
        memberRemarkInstance1.setId(null);
        assertThat(memberRemarkInstance1).isNotEqualTo(memberRemarkInstance2);
    }
}
