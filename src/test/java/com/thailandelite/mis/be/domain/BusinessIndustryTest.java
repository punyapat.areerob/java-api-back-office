package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.BusinessIndustry;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class BusinessIndustryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BusinessIndustry.class);
        BusinessIndustry businessIndustry1 = new BusinessIndustry();
        businessIndustry1.setId("id1");
        BusinessIndustry businessIndustry2 = new BusinessIndustry();
        businessIndustry2.setId(businessIndustry1.getId());
        assertThat(businessIndustry1).isEqualTo(businessIndustry2);
        businessIndustry2.setId("id2");
        assertThat(businessIndustry1).isNotEqualTo(businessIndustry2);
        businessIndustry1.setId(null);
        assertThat(businessIndustry1).isNotEqualTo(businessIndustry2);
    }
}
