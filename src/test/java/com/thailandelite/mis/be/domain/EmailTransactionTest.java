package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class EmailTransactionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmailTransaction.class);
        EmailTransaction emailTransaction1 = new EmailTransaction();
        emailTransaction1.setId("id1");
        EmailTransaction emailTransaction2 = new EmailTransaction();
        emailTransaction2.setId(emailTransaction1.getId());
        assertThat(emailTransaction1).isEqualTo(emailTransaction2);
        emailTransaction2.setId("id2");
        assertThat(emailTransaction1).isNotEqualTo(emailTransaction2);
        emailTransaction1.setId(null);
        assertThat(emailTransaction1).isNotEqualTo(emailTransaction2);
    }
}
