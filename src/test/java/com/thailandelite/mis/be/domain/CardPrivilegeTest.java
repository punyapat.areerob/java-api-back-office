package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.CardPrivilege;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class CardPrivilegeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardPrivilege.class);
        CardPrivilege cardPrivilege1 = new CardPrivilege();
        cardPrivilege1.setId("id1");
        CardPrivilege cardPrivilege2 = new CardPrivilege();
        cardPrivilege2.setId(cardPrivilege1.getId());
        assertThat(cardPrivilege1).isEqualTo(cardPrivilege2);
        cardPrivilege2.setId("id2");
        assertThat(cardPrivilege1).isNotEqualTo(cardPrivilege2);
        cardPrivilege1.setId(null);
        assertThat(cardPrivilege1).isNotEqualTo(cardPrivilege2);
    }
}
