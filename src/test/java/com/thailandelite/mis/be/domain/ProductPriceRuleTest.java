package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class ProductPriceRuleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductPriceRule.class);
        ProductPriceRule productPriceRule1 = new ProductPriceRule();
        productPriceRule1.setId("id1");
        ProductPriceRule productPriceRule2 = new ProductPriceRule();
        productPriceRule2.setId(productPriceRule1.getId());
        assertThat(productPriceRule1).isEqualTo(productPriceRule2);
        productPriceRule2.setId("id2");
        assertThat(productPriceRule1).isNotEqualTo(productPriceRule2);
        productPriceRule1.setId(null);
        assertThat(productPriceRule1).isNotEqualTo(productPriceRule2);
    }
}
