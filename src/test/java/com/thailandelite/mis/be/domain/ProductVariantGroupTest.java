package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.ProductVariantGroup;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class ProductVariantGroupTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductVariantGroup.class);
        ProductVariantGroup productVariantGroup1 = new ProductVariantGroup();
        productVariantGroup1.setId("id1");
        ProductVariantGroup productVariantGroup2 = new ProductVariantGroup();
        productVariantGroup2.setId(productVariantGroup1.getId());
        assertThat(productVariantGroup1).isEqualTo(productVariantGroup2);
        productVariantGroup2.setId("id2");
        assertThat(productVariantGroup1).isNotEqualTo(productVariantGroup2);
        productVariantGroup1.setId(null);
        assertThat(productVariantGroup1).isNotEqualTo(productVariantGroup2);
    }
}
