package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.agent.AgentMemberActivation;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class AgentMemberActivationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentMemberActivation.class);
        AgentMemberActivation agentMemberActivation1 = new AgentMemberActivation();
        agentMemberActivation1.setId("id1");
        AgentMemberActivation agentMemberActivation2 = new AgentMemberActivation();
        agentMemberActivation2.setId(agentMemberActivation1.getId());
        assertThat(agentMemberActivation1).isEqualTo(agentMemberActivation2);
        agentMemberActivation2.setId("id2");
        assertThat(agentMemberActivation1).isNotEqualTo(agentMemberActivation2);
        agentMemberActivation1.setId(null);
        assertThat(agentMemberActivation1).isNotEqualTo(agentMemberActivation2);
    }
}
