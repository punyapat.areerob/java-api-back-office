package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class BroadcastMessageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BroadcastMessage.class);
        BroadcastMessage broadcastMessage1 = new BroadcastMessage();
        broadcastMessage1.setId("id1");
        BroadcastMessage broadcastMessage2 = new BroadcastMessage();
        broadcastMessage2.setId(broadcastMessage1.getId());
        assertThat(broadcastMessage1).isEqualTo(broadcastMessage2);
        broadcastMessage2.setId("id2");
        assertThat(broadcastMessage1).isNotEqualTo(broadcastMessage2);
        broadcastMessage1.setId(null);
        assertThat(broadcastMessage1).isNotEqualTo(broadcastMessage2);
    }
}
