package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.master.BloodType;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class BloodTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BloodType.class);
        BloodType bloodType1 = new BloodType();
        bloodType1.setId("id1");
        BloodType bloodType2 = new BloodType();
        bloodType2.setId(bloodType1.getId());
        assertThat(bloodType1).isEqualTo(bloodType2);
        bloodType2.setId("id2");
        assertThat(bloodType1).isNotEqualTo(bloodType2);
        bloodType1.setId(null);
        assertThat(bloodType1).isNotEqualTo(bloodType2);
    }
}
