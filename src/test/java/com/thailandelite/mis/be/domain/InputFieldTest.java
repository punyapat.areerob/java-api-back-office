package com.thailandelite.mis.be.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class InputFieldTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InputField.class);
        InputField inputField1 = new InputField();
        inputField1.setId("id1");
        InputField inputField2 = new InputField();
        inputField2.setId(inputField1.getId());
        assertThat(inputField1).isEqualTo(inputField2);
        inputField2.setId("id2");
        assertThat(inputField1).isNotEqualTo(inputField2);
        inputField1.setId(null);
        assertThat(inputField1).isNotEqualTo(inputField2);
    }
}
