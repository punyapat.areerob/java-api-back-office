package com.thailandelite.mis.be.domain;

import com.thailandelite.mis.model.domain.agent.AgentLog;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.thailandelite.mis.be.web.rest.TestUtil;

public class AgentLogTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentLog.class);
        AgentLog agentLog1 = new AgentLog();
        agentLog1.setId("id1");
        AgentLog agentLog2 = new AgentLog();
        agentLog2.setId(agentLog1.getId());
        assertThat(agentLog1).isEqualTo(agentLog2);
        agentLog2.setId("id2");
        assertThat(agentLog1).isNotEqualTo(agentLog2);
        agentLog1.setId(null);
        assertThat(agentLog1).isNotEqualTo(agentLog2);
    }
}
