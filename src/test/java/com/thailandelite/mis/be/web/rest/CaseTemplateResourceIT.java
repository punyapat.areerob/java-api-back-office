package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CaseTemplate;
import com.thailandelite.mis.be.repository.CaseTemplateRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.thailandelite.mis.model.domain.enumeration.CaseStatus;
/**
 * Integration tests for the {@link CaseTemplateResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseTemplateResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final CaseStatus DEFAULT_STATUS = CaseStatus.ACTIVE;
    private static final CaseStatus UPDATED_STATUS = CaseStatus.INACTIVE;

    @Autowired
    private CaseTemplateRepository caseTemplateRepository;

    @Autowired
    private MockMvc restCaseTemplateMockMvc;

    private CaseTemplate caseTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseTemplate createEntity() {
        CaseTemplate caseTemplate = new CaseTemplate()
            .name(DEFAULT_NAME)
            .status(DEFAULT_STATUS);
        return caseTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseTemplate createUpdatedEntity() {
        CaseTemplate caseTemplate = new CaseTemplate()
            .name(UPDATED_NAME)
            .status(UPDATED_STATUS);
        return caseTemplate;
    }

    @BeforeEach
    public void initTest() {
        caseTemplateRepository.deleteAll();
        caseTemplate = createEntity();
    }

    @Test
    public void createCaseTemplate() throws Exception {
        int databaseSizeBeforeCreate = caseTemplateRepository.findAll().size();
        // Create the CaseTemplate
        restCaseTemplateMockMvc.perform(post("/api/case-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseTemplate)))
            .andExpect(status().isCreated());

        // Validate the CaseTemplate in the database
        List<CaseTemplate> caseTemplateList = caseTemplateRepository.findAll();
        assertThat(caseTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        CaseTemplate testCaseTemplate = caseTemplateList.get(caseTemplateList.size() - 1);
        assertThat(testCaseTemplate.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCaseTemplate.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    public void createCaseTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseTemplateRepository.findAll().size();

        // Create the CaseTemplate with an existing ID
        caseTemplate.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseTemplateMockMvc.perform(post("/api/case-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the CaseTemplate in the database
        List<CaseTemplate> caseTemplateList = caseTemplateRepository.findAll();
        assertThat(caseTemplateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseTemplates() throws Exception {
        // Initialize the database
        caseTemplateRepository.save(caseTemplate);

        // Get all the caseTemplateList
        restCaseTemplateMockMvc.perform(get("/api/case-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseTemplate.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    public void getCaseTemplate() throws Exception {
        // Initialize the database
        caseTemplateRepository.save(caseTemplate);

        // Get the caseTemplate
        restCaseTemplateMockMvc.perform(get("/api/case-templates/{id}", caseTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseTemplate.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }
    @Test
    public void getNonExistingCaseTemplate() throws Exception {
        // Get the caseTemplate
        restCaseTemplateMockMvc.perform(get("/api/case-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseTemplate() throws Exception {
        // Initialize the database
        caseTemplateRepository.save(caseTemplate);

        int databaseSizeBeforeUpdate = caseTemplateRepository.findAll().size();

        // Update the caseTemplate
        CaseTemplate updatedCaseTemplate = caseTemplateRepository.findById(caseTemplate.getId()).get();
        updatedCaseTemplate
            .name(UPDATED_NAME)
            .status(UPDATED_STATUS);

        restCaseTemplateMockMvc.perform(put("/api/case-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseTemplate)))
            .andExpect(status().isOk());

        // Validate the CaseTemplate in the database
        List<CaseTemplate> caseTemplateList = caseTemplateRepository.findAll();
        assertThat(caseTemplateList).hasSize(databaseSizeBeforeUpdate);
        CaseTemplate testCaseTemplate = caseTemplateList.get(caseTemplateList.size() - 1);
        assertThat(testCaseTemplate.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCaseTemplate.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    public void updateNonExistingCaseTemplate() throws Exception {
        int databaseSizeBeforeUpdate = caseTemplateRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseTemplateMockMvc.perform(put("/api/case-templates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the CaseTemplate in the database
        List<CaseTemplate> caseTemplateList = caseTemplateRepository.findAll();
        assertThat(caseTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseTemplate() throws Exception {
        // Initialize the database
        caseTemplateRepository.save(caseTemplate);

        int databaseSizeBeforeDelete = caseTemplateRepository.findAll().size();

        // Delete the caseTemplate
        restCaseTemplateMockMvc.perform(delete("/api/case-templates/{id}", caseTemplate.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseTemplate> caseTemplateList = caseTemplateRepository.findAll();
        assertThat(caseTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
