package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CaseDataInstanceResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseDataInstanceResourceIT {

  /*  @Autowired
    private CaseDataInstanceRepository caseDataInstanceRepository;

    @Autowired
    private MockMvc restCaseDataInstanceMockMvc;

    private CaseDataInstance caseDataInstance;

    *//**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static CaseDataInstance createEntity() {
        CaseDataInstance caseDataInstance = new CaseDataInstance();
        return caseDataInstance;
    }
    *//**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static CaseDataInstance createUpdatedEntity() {
        CaseDataInstance caseDataInstance = new CaseDataInstance();
        return caseDataInstance;
    }

    @BeforeEach
    public void initTest() {
        caseDataInstanceRepository.deleteAll();
        caseDataInstance = createEntity();
    }

    @Test
    public void createCaseDataInstance() throws Exception {
        int databaseSizeBeforeCreate = caseDataInstanceRepository.findAll().size();
        // Create the CaseDataInstance
        restCaseDataInstanceMockMvc.perform(post("/api/case-data-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseDataInstance)))
            .andExpect(status().isCreated());

        // Validate the CaseDataInstance in the database
        List<CaseDataInstance> caseDataInstanceList = caseDataInstanceRepository.findAll();
        assertThat(caseDataInstanceList).hasSize(databaseSizeBeforeCreate + 1);
        CaseDataInstance testCaseDataInstance = caseDataInstanceList.get(caseDataInstanceList.size() - 1);
    }

    @Test
    public void createCaseDataInstanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseDataInstanceRepository.findAll().size();

        // Create the CaseDataInstance with an existing ID
        caseDataInstance.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseDataInstanceMockMvc.perform(post("/api/case-data-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseDataInstance)))
            .andExpect(status().isBadRequest());

        // Validate the CaseDataInstance in the database
        List<CaseDataInstance> caseDataInstanceList = caseDataInstanceRepository.findAll();
        assertThat(caseDataInstanceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseDataInstances() throws Exception {
        // Initialize the database
        caseDataInstanceRepository.save(caseDataInstance);

        // Get all the caseDataInstanceList
        restCaseDataInstanceMockMvc.perform(get("/api/case-data-instances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseDataInstance.getId())));
    }

    @Test
    public void getCaseDataInstance() throws Exception {
        // Initialize the database
        caseDataInstanceRepository.save(caseDataInstance);

        // Get the caseDataInstance
        restCaseDataInstanceMockMvc.perform(get("/api/case-data-instances/{id}", caseDataInstance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseDataInstance.getId()));
    }
    @Test
    public void getNonExistingCaseDataInstance() throws Exception {
        // Get the caseDataInstance
        restCaseDataInstanceMockMvc.perform(get("/api/case-data-instances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseDataInstance() throws Exception {
        // Initialize the database
        caseDataInstanceRepository.save(caseDataInstance);

        int databaseSizeBeforeUpdate = caseDataInstanceRepository.findAll().size();

        // Update the caseDataInstance
        CaseDataInstance updatedCaseDataInstance = caseDataInstanceRepository.findById(caseDataInstance.getId()).get();

        restCaseDataInstanceMockMvc.perform(put("/api/case-data-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseDataInstance)))
            .andExpect(status().isOk());

        // Validate the CaseDataInstance in the database
        List<CaseDataInstance> caseDataInstanceList = caseDataInstanceRepository.findAll();
        assertThat(caseDataInstanceList).hasSize(databaseSizeBeforeUpdate);
        CaseDataInstance testCaseDataInstance = caseDataInstanceList.get(caseDataInstanceList.size() - 1);
    }

    @Test
    public void updateNonExistingCaseDataInstance() throws Exception {
        int databaseSizeBeforeUpdate = caseDataInstanceRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseDataInstanceMockMvc.perform(put("/api/case-data-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseDataInstance)))
            .andExpect(status().isBadRequest());

        // Validate the CaseDataInstance in the database
        List<CaseDataInstance> caseDataInstanceList = caseDataInstanceRepository.findAll();
        assertThat(caseDataInstanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseDataInstance() throws Exception {
        // Initialize the database
        caseDataInstanceRepository.save(caseDataInstance);

        int databaseSizeBeforeDelete = caseDataInstanceRepository.findAll().size();

        // Delete the caseDataInstance
        restCaseDataInstanceMockMvc.perform(delete("/api/case-data-instances/{id}", caseDataInstance.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseDataInstance> caseDataInstanceList = caseDataInstanceRepository.findAll();
        assertThat(caseDataInstanceList).hasSize(databaseSizeBeforeDelete - 1);
    }*/
}
