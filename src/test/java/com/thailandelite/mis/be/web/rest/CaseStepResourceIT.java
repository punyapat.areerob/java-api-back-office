package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CaseStep;
import com.thailandelite.mis.be.repository.CaseStepRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CaseStepResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseStepResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CASE_ID = "AAAAAAAAAA";
    private static final String UPDATED_CASE_ID = "BBBBBBBBBB";

    private static final Boolean DEFAULT_BEGIN = false;
    private static final Boolean UPDATED_BEGIN = true;

    @Autowired
    private CaseStepRepository caseStepRepository;

    @Autowired
    private MockMvc restCaseStepMockMvc;

    private CaseStep caseStep;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStep createEntity() {
        CaseStep caseStep = new CaseStep()
            .name(DEFAULT_NAME)
            .caseId(DEFAULT_CASE_ID)
            .begin(DEFAULT_BEGIN);
        return caseStep;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStep createUpdatedEntity() {
        CaseStep caseStep = new CaseStep()
            .name(UPDATED_NAME)
            .caseId(UPDATED_CASE_ID)
            .begin(UPDATED_BEGIN);
        return caseStep;
    }

    @BeforeEach
    public void initTest() {
        caseStepRepository.deleteAll();
        caseStep = createEntity();
    }

    @Test
    public void createCaseStep() throws Exception {
        int databaseSizeBeforeCreate = caseStepRepository.findAll().size();
        // Create the CaseStep
        restCaseStepMockMvc.perform(post("/api/case-steps")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStep)))
            .andExpect(status().isCreated());

        // Validate the CaseStep in the database
        List<CaseStep> caseStepList = caseStepRepository.findAll();
        assertThat(caseStepList).hasSize(databaseSizeBeforeCreate + 1);
        CaseStep testCaseStep = caseStepList.get(caseStepList.size() - 1);
        assertThat(testCaseStep.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCaseStep.getCaseId()).isEqualTo(DEFAULT_CASE_ID);
        assertThat(testCaseStep.isBegin()).isEqualTo(DEFAULT_BEGIN);
    }

    @Test
    public void createCaseStepWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseStepRepository.findAll().size();

        // Create the CaseStep with an existing ID
        caseStep.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseStepMockMvc.perform(post("/api/case-steps")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStep)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStep in the database
        List<CaseStep> caseStepList = caseStepRepository.findAll();
        assertThat(caseStepList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseSteps() throws Exception {
        // Initialize the database
        caseStepRepository.save(caseStep);

        // Get all the caseStepList
        restCaseStepMockMvc.perform(get("/api/case-steps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseStep.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].caseId").value(hasItem(DEFAULT_CASE_ID)))
            .andExpect(jsonPath("$.[*].begin").value(hasItem(DEFAULT_BEGIN.booleanValue())));
    }
    
    @Test
    public void getCaseStep() throws Exception {
        // Initialize the database
        caseStepRepository.save(caseStep);

        // Get the caseStep
        restCaseStepMockMvc.perform(get("/api/case-steps/{id}", caseStep.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseStep.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.caseId").value(DEFAULT_CASE_ID))
            .andExpect(jsonPath("$.begin").value(DEFAULT_BEGIN.booleanValue()));
    }
    @Test
    public void getNonExistingCaseStep() throws Exception {
        // Get the caseStep
        restCaseStepMockMvc.perform(get("/api/case-steps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseStep() throws Exception {
        // Initialize the database
        caseStepRepository.save(caseStep);

        int databaseSizeBeforeUpdate = caseStepRepository.findAll().size();

        // Update the caseStep
        CaseStep updatedCaseStep = caseStepRepository.findById(caseStep.getId()).get();
        updatedCaseStep
            .name(UPDATED_NAME)
            .caseId(UPDATED_CASE_ID)
            .begin(UPDATED_BEGIN);

        restCaseStepMockMvc.perform(put("/api/case-steps")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseStep)))
            .andExpect(status().isOk());

        // Validate the CaseStep in the database
        List<CaseStep> caseStepList = caseStepRepository.findAll();
        assertThat(caseStepList).hasSize(databaseSizeBeforeUpdate);
        CaseStep testCaseStep = caseStepList.get(caseStepList.size() - 1);
        assertThat(testCaseStep.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCaseStep.getCaseId()).isEqualTo(UPDATED_CASE_ID);
        assertThat(testCaseStep.isBegin()).isEqualTo(UPDATED_BEGIN);
    }

    @Test
    public void updateNonExistingCaseStep() throws Exception {
        int databaseSizeBeforeUpdate = caseStepRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseStepMockMvc.perform(put("/api/case-steps")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStep)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStep in the database
        List<CaseStep> caseStepList = caseStepRepository.findAll();
        assertThat(caseStepList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseStep() throws Exception {
        // Initialize the database
        caseStepRepository.save(caseStep);

        int databaseSizeBeforeDelete = caseStepRepository.findAll().size();

        // Delete the caseStep
        restCaseStepMockMvc.perform(delete("/api/case-steps/{id}", caseStep.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseStep> caseStepList = caseStepRepository.findAll();
        assertThat(caseStepList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
