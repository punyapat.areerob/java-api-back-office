package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.master.VisaType;
import com.thailandelite.mis.be.repository.VisaTypeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VisaTypeResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VisaTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private VisaTypeRepository visaTypeRepository;

    @Autowired
    private MockMvc restVisaTypeMockMvc;

    private VisaType visaType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VisaType createEntity() {
        VisaType visaType = new VisaType()
            .name(DEFAULT_NAME)
            .active(DEFAULT_ACTIVE);
        return visaType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VisaType createUpdatedEntity() {
        VisaType visaType = new VisaType()
            .name(UPDATED_NAME)
            .active(UPDATED_ACTIVE);
        return visaType;
    }

    @BeforeEach
    public void initTest() {
        visaTypeRepository.deleteAll();
        visaType = createEntity();
    }

    @Test
    public void createVisaType() throws Exception {
        int databaseSizeBeforeCreate = visaTypeRepository.findAll().size();
        // Create the VisaType
        restVisaTypeMockMvc.perform(post("/api/visa-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visaType)))
            .andExpect(status().isCreated());

        // Validate the VisaType in the database
        List<VisaType> visaTypeList = visaTypeRepository.findAll();
        assertThat(visaTypeList).hasSize(databaseSizeBeforeCreate + 1);
        VisaType testVisaType = visaTypeList.get(visaTypeList.size() - 1);
        assertThat(testVisaType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVisaType.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    public void createVisaTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = visaTypeRepository.findAll().size();

        // Create the VisaType with an existing ID
        visaType.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restVisaTypeMockMvc.perform(post("/api/visa-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visaType)))
            .andExpect(status().isBadRequest());

        // Validate the VisaType in the database
        List<VisaType> visaTypeList = visaTypeRepository.findAll();
        assertThat(visaTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllVisaTypes() throws Exception {
        // Initialize the database
        visaTypeRepository.save(visaType);

        // Get all the visaTypeList
        restVisaTypeMockMvc.perform(get("/api/visa-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(visaType.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    public void getVisaType() throws Exception {
        // Initialize the database
        visaTypeRepository.save(visaType);

        // Get the visaType
        restVisaTypeMockMvc.perform(get("/api/visa-types/{id}", visaType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(visaType.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }
    @Test
    public void getNonExistingVisaType() throws Exception {
        // Get the visaType
        restVisaTypeMockMvc.perform(get("/api/visa-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVisaType() throws Exception {
        // Initialize the database
        visaTypeRepository.save(visaType);

        int databaseSizeBeforeUpdate = visaTypeRepository.findAll().size();

        // Update the visaType
        VisaType updatedVisaType = visaTypeRepository.findById(visaType.getId()).get();
        updatedVisaType
            .name(UPDATED_NAME)
            .active(UPDATED_ACTIVE);

        restVisaTypeMockMvc.perform(put("/api/visa-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVisaType)))
            .andExpect(status().isOk());

        // Validate the VisaType in the database
        List<VisaType> visaTypeList = visaTypeRepository.findAll();
        assertThat(visaTypeList).hasSize(databaseSizeBeforeUpdate);
        VisaType testVisaType = visaTypeList.get(visaTypeList.size() - 1);
        assertThat(testVisaType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVisaType.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    public void updateNonExistingVisaType() throws Exception {
        int databaseSizeBeforeUpdate = visaTypeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVisaTypeMockMvc.perform(put("/api/visa-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visaType)))
            .andExpect(status().isBadRequest());

        // Validate the VisaType in the database
        List<VisaType> visaTypeList = visaTypeRepository.findAll();
        assertThat(visaTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteVisaType() throws Exception {
        // Initialize the database
        visaTypeRepository.save(visaType);

        int databaseSizeBeforeDelete = visaTypeRepository.findAll().size();

        // Delete the visaType
        restVisaTypeMockMvc.perform(delete("/api/visa-types/{id}", visaType.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VisaType> visaTypeList = visaTypeRepository.findAll();
        assertThat(visaTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
