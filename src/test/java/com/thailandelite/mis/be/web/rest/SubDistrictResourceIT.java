package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.master.SubDistrict;
import com.thailandelite.mis.be.repository.SubDistrictRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SubDistrictResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class SubDistrictResourceIT {

    @Autowired
    private SubDistrictRepository subDistrictRepository;

    @Autowired
    private MockMvc restSubDistrictMockMvc;

    private SubDistrict subDistrict;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubDistrict createEntity() {
        SubDistrict subDistrict = new SubDistrict();
        return subDistrict;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubDistrict createUpdatedEntity() {
        SubDistrict subDistrict = new SubDistrict();
        return subDistrict;
    }

    @BeforeEach
    public void initTest() {
        subDistrictRepository.deleteAll();
        subDistrict = createEntity();
    }

    @Test
    public void createSubDistrict() throws Exception {
        int databaseSizeBeforeCreate = subDistrictRepository.findAll().size();
        // Create the SubDistrict
        restSubDistrictMockMvc.perform(post("/api/sub-districts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subDistrict)))
            .andExpect(status().isCreated());

        // Validate the SubDistrict in the database
        List<SubDistrict> subDistrictList = subDistrictRepository.findAll();
        assertThat(subDistrictList).hasSize(databaseSizeBeforeCreate + 1);
        SubDistrict testSubDistrict = subDistrictList.get(subDistrictList.size() - 1);
    }

    @Test
    public void createSubDistrictWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subDistrictRepository.findAll().size();

        // Create the SubDistrict with an existing ID
        subDistrict.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubDistrictMockMvc.perform(post("/api/sub-districts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subDistrict)))
            .andExpect(status().isBadRequest());

        // Validate the SubDistrict in the database
        List<SubDistrict> subDistrictList = subDistrictRepository.findAll();
        assertThat(subDistrictList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllSubDistricts() throws Exception {
        // Initialize the database
        subDistrictRepository.save(subDistrict);

        // Get all the subDistrictList
        restSubDistrictMockMvc.perform(get("/api/sub-districts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subDistrict.getId())));
    }

    @Test
    public void getSubDistrict() throws Exception {
        // Initialize the database
        subDistrictRepository.save(subDistrict);

        // Get the subDistrict
        restSubDistrictMockMvc.perform(get("/api/sub-districts/{id}", subDistrict.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(subDistrict.getId()));
    }
    @Test
    public void getNonExistingSubDistrict() throws Exception {
        // Get the subDistrict
        restSubDistrictMockMvc.perform(get("/api/sub-districts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSubDistrict() throws Exception {
        // Initialize the database
        subDistrictRepository.save(subDistrict);

        int databaseSizeBeforeUpdate = subDistrictRepository.findAll().size();

        // Update the subDistrict
        SubDistrict updatedSubDistrict = subDistrictRepository.findById(subDistrict.getId()).get();

        restSubDistrictMockMvc.perform(put("/api/sub-districts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSubDistrict)))
            .andExpect(status().isOk());

        // Validate the SubDistrict in the database
        List<SubDistrict> subDistrictList = subDistrictRepository.findAll();
        assertThat(subDistrictList).hasSize(databaseSizeBeforeUpdate);
        SubDistrict testSubDistrict = subDistrictList.get(subDistrictList.size() - 1);
    }

    @Test
    public void updateNonExistingSubDistrict() throws Exception {
        int databaseSizeBeforeUpdate = subDistrictRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubDistrictMockMvc.perform(put("/api/sub-districts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subDistrict)))
            .andExpect(status().isBadRequest());

        // Validate the SubDistrict in the database
        List<SubDistrict> subDistrictList = subDistrictRepository.findAll();
        assertThat(subDistrictList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteSubDistrict() throws Exception {
        // Initialize the database
        subDistrictRepository.save(subDistrict);

        int databaseSizeBeforeDelete = subDistrictRepository.findAll().size();

        // Delete the subDistrict
        restSubDistrictMockMvc.perform(delete("/api/sub-districts/{id}", subDistrict.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubDistrict> subDistrictList = subDistrictRepository.findAll();
        assertThat(subDistrictList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
