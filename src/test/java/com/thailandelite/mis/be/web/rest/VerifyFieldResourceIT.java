package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.VerifyField;
import com.thailandelite.mis.be.repository.VerifyFieldRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VerifyFieldResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VerifyFieldResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    private static final Boolean DEFAULT_VERIFY = false;
    private static final Boolean UPDATED_VERIFY = true;

    @Autowired
    private VerifyFieldRepository verifyFieldRepository;

    @Autowired
    private MockMvc restVerifyFieldMockMvc;

    private VerifyField verifyField;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VerifyField createEntity() {
        VerifyField verifyField = new VerifyField()
            .name(DEFAULT_NAME)
            .label(DEFAULT_LABEL)
            .link(DEFAULT_LINK)
            .verify(DEFAULT_VERIFY);
        return verifyField;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VerifyField createUpdatedEntity() {
        VerifyField verifyField = new VerifyField()
            .name(UPDATED_NAME)
            .label(UPDATED_LABEL)
            .link(UPDATED_LINK)
            .verify(UPDATED_VERIFY);
        return verifyField;
    }

    @BeforeEach
    public void initTest() {
        verifyFieldRepository.deleteAll();
        verifyField = createEntity();
    }

    @Test
    public void createVerifyField() throws Exception {
        int databaseSizeBeforeCreate = verifyFieldRepository.findAll().size();
        // Create the VerifyField
        restVerifyFieldMockMvc.perform(post("/api/verify-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(verifyField)))
            .andExpect(status().isCreated());

        // Validate the VerifyField in the database
        List<VerifyField> verifyFieldList = verifyFieldRepository.findAll();
        assertThat(verifyFieldList).hasSize(databaseSizeBeforeCreate + 1);
        VerifyField testVerifyField = verifyFieldList.get(verifyFieldList.size() - 1);
        assertThat(testVerifyField.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVerifyField.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testVerifyField.getLink()).isEqualTo(DEFAULT_LINK);
        assertThat(testVerifyField.isVerify()).isEqualTo(DEFAULT_VERIFY);
    }

    @Test
    public void createVerifyFieldWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = verifyFieldRepository.findAll().size();

        // Create the VerifyField with an existing ID
        verifyField.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restVerifyFieldMockMvc.perform(post("/api/verify-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(verifyField)))
            .andExpect(status().isBadRequest());

        // Validate the VerifyField in the database
        List<VerifyField> verifyFieldList = verifyFieldRepository.findAll();
        assertThat(verifyFieldList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllVerifyFields() throws Exception {
        // Initialize the database
        verifyFieldRepository.save(verifyField);

        // Get all the verifyFieldList
        restVerifyFieldMockMvc.perform(get("/api/verify-fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(verifyField.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK)))
            .andExpect(jsonPath("$.[*].verify").value(hasItem(DEFAULT_VERIFY.booleanValue())));
    }
    
    @Test
    public void getVerifyField() throws Exception {
        // Initialize the database
        verifyFieldRepository.save(verifyField);

        // Get the verifyField
        restVerifyFieldMockMvc.perform(get("/api/verify-fields/{id}", verifyField.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(verifyField.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK))
            .andExpect(jsonPath("$.verify").value(DEFAULT_VERIFY.booleanValue()));
    }
    @Test
    public void getNonExistingVerifyField() throws Exception {
        // Get the verifyField
        restVerifyFieldMockMvc.perform(get("/api/verify-fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVerifyField() throws Exception {
        // Initialize the database
        verifyFieldRepository.save(verifyField);

        int databaseSizeBeforeUpdate = verifyFieldRepository.findAll().size();

        // Update the verifyField
        VerifyField updatedVerifyField = verifyFieldRepository.findById(verifyField.getId()).get();
        updatedVerifyField
            .name(UPDATED_NAME)
            .label(UPDATED_LABEL)
            .link(UPDATED_LINK)
            .verify(UPDATED_VERIFY);

        restVerifyFieldMockMvc.perform(put("/api/verify-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVerifyField)))
            .andExpect(status().isOk());

        // Validate the VerifyField in the database
        List<VerifyField> verifyFieldList = verifyFieldRepository.findAll();
        assertThat(verifyFieldList).hasSize(databaseSizeBeforeUpdate);
        VerifyField testVerifyField = verifyFieldList.get(verifyFieldList.size() - 1);
        assertThat(testVerifyField.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVerifyField.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testVerifyField.getLink()).isEqualTo(UPDATED_LINK);
        assertThat(testVerifyField.isVerify()).isEqualTo(UPDATED_VERIFY);
    }

    @Test
    public void updateNonExistingVerifyField() throws Exception {
        int databaseSizeBeforeUpdate = verifyFieldRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVerifyFieldMockMvc.perform(put("/api/verify-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(verifyField)))
            .andExpect(status().isBadRequest());

        // Validate the VerifyField in the database
        List<VerifyField> verifyFieldList = verifyFieldRepository.findAll();
        assertThat(verifyFieldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteVerifyField() throws Exception {
        // Initialize the database
        verifyFieldRepository.save(verifyField);

        int databaseSizeBeforeDelete = verifyFieldRepository.findAll().size();

        // Delete the verifyField
        restVerifyFieldMockMvc.perform(delete("/api/verify-fields/{id}", verifyField.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VerifyField> verifyFieldList = verifyFieldRepository.findAll();
        assertThat(verifyFieldList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
