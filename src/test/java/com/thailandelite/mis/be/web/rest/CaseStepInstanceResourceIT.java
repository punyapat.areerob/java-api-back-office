package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CaseStepInstance;
import com.thailandelite.mis.be.repository.CaseStepInstanceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CaseStepInstanceResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseStepInstanceResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STEP_ID = "AAAAAAAAAA";
    private static final String UPDATED_STEP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CASE_INSTANCE_ID = "AAAAAAAAAA";
    private static final String UPDATED_CASE_INSTANCE_ID = "BBBBBBBBBB";

    private static final Boolean DEFAULT_BEGIN = false;
    private static final Boolean UPDATED_BEGIN = true;

    @Autowired
    private CaseStepInstanceRepository caseStepInstanceRepository;

    @Autowired
    private MockMvc restCaseStepInstanceMockMvc;

    private CaseStepInstance caseStepInstance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStepInstance createEntity() {
        CaseStepInstance caseStepInstance = new CaseStepInstance()
            .name(DEFAULT_NAME)
            .stepId(DEFAULT_STEP_ID)
            .caseInstanceId(DEFAULT_CASE_INSTANCE_ID)
            .begin(DEFAULT_BEGIN);
        return caseStepInstance;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStepInstance createUpdatedEntity() {
        CaseStepInstance caseStepInstance = new CaseStepInstance()
            .name(UPDATED_NAME)
            .stepId(UPDATED_STEP_ID)
            .caseInstanceId(UPDATED_CASE_INSTANCE_ID)
            .begin(UPDATED_BEGIN);
        return caseStepInstance;
    }

    @BeforeEach
    public void initTest() {
        caseStepInstanceRepository.deleteAll();
        caseStepInstance = createEntity();
    }

    @Test
    public void createCaseStepInstance() throws Exception {
        int databaseSizeBeforeCreate = caseStepInstanceRepository.findAll().size();
        // Create the CaseStepInstance
        restCaseStepInstanceMockMvc.perform(post("/api/case-step-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStepInstance)))
            .andExpect(status().isCreated());

        // Validate the CaseStepInstance in the database
        List<CaseStepInstance> caseStepInstanceList = caseStepInstanceRepository.findAll();
        assertThat(caseStepInstanceList).hasSize(databaseSizeBeforeCreate + 1);
        CaseStepInstance testCaseStepInstance = caseStepInstanceList.get(caseStepInstanceList.size() - 1);
        assertThat(testCaseStepInstance.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCaseStepInstance.getStepId()).isEqualTo(DEFAULT_STEP_ID);
        assertThat(testCaseStepInstance.getCaseInstanceId()).isEqualTo(DEFAULT_CASE_INSTANCE_ID);
        assertThat(testCaseStepInstance.isBegin()).isEqualTo(DEFAULT_BEGIN);
    }

    @Test
    public void createCaseStepInstanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseStepInstanceRepository.findAll().size();

        // Create the CaseStepInstance with an existing ID
        caseStepInstance.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseStepInstanceMockMvc.perform(post("/api/case-step-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStepInstance)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStepInstance in the database
        List<CaseStepInstance> caseStepInstanceList = caseStepInstanceRepository.findAll();
        assertThat(caseStepInstanceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseStepInstances() throws Exception {
        // Initialize the database
        caseStepInstanceRepository.save(caseStepInstance);

        // Get all the caseStepInstanceList
        restCaseStepInstanceMockMvc.perform(get("/api/case-step-instances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseStepInstance.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].stepId").value(hasItem(DEFAULT_STEP_ID)))
            .andExpect(jsonPath("$.[*].caseInstanceId").value(hasItem(DEFAULT_CASE_INSTANCE_ID)))
            .andExpect(jsonPath("$.[*].begin").value(hasItem(DEFAULT_BEGIN.booleanValue())));
    }
    
    @Test
    public void getCaseStepInstance() throws Exception {
        // Initialize the database
        caseStepInstanceRepository.save(caseStepInstance);

        // Get the caseStepInstance
        restCaseStepInstanceMockMvc.perform(get("/api/case-step-instances/{id}", caseStepInstance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseStepInstance.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.stepId").value(DEFAULT_STEP_ID))
            .andExpect(jsonPath("$.caseInstanceId").value(DEFAULT_CASE_INSTANCE_ID))
            .andExpect(jsonPath("$.begin").value(DEFAULT_BEGIN.booleanValue()));
    }
    @Test
    public void getNonExistingCaseStepInstance() throws Exception {
        // Get the caseStepInstance
        restCaseStepInstanceMockMvc.perform(get("/api/case-step-instances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseStepInstance() throws Exception {
        // Initialize the database
        caseStepInstanceRepository.save(caseStepInstance);

        int databaseSizeBeforeUpdate = caseStepInstanceRepository.findAll().size();

        // Update the caseStepInstance
        CaseStepInstance updatedCaseStepInstance = caseStepInstanceRepository.findById(caseStepInstance.getId()).get();
        updatedCaseStepInstance
            .name(UPDATED_NAME)
            .stepId(UPDATED_STEP_ID)
            .caseInstanceId(UPDATED_CASE_INSTANCE_ID)
            .begin(UPDATED_BEGIN);

        restCaseStepInstanceMockMvc.perform(put("/api/case-step-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseStepInstance)))
            .andExpect(status().isOk());

        // Validate the CaseStepInstance in the database
        List<CaseStepInstance> caseStepInstanceList = caseStepInstanceRepository.findAll();
        assertThat(caseStepInstanceList).hasSize(databaseSizeBeforeUpdate);
        CaseStepInstance testCaseStepInstance = caseStepInstanceList.get(caseStepInstanceList.size() - 1);
        assertThat(testCaseStepInstance.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCaseStepInstance.getStepId()).isEqualTo(UPDATED_STEP_ID);
        assertThat(testCaseStepInstance.getCaseInstanceId()).isEqualTo(UPDATED_CASE_INSTANCE_ID);
        assertThat(testCaseStepInstance.isBegin()).isEqualTo(UPDATED_BEGIN);
    }

    @Test
    public void updateNonExistingCaseStepInstance() throws Exception {
        int databaseSizeBeforeUpdate = caseStepInstanceRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseStepInstanceMockMvc.perform(put("/api/case-step-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStepInstance)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStepInstance in the database
        List<CaseStepInstance> caseStepInstanceList = caseStepInstanceRepository.findAll();
        assertThat(caseStepInstanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseStepInstance() throws Exception {
        // Initialize the database
        caseStepInstanceRepository.save(caseStepInstance);

        int databaseSizeBeforeDelete = caseStepInstanceRepository.findAll().size();

        // Delete the caseStepInstance
        restCaseStepInstanceMockMvc.perform(delete("/api/case-step-instances/{id}", caseStepInstance.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseStepInstance> caseStepInstanceList = caseStepInstanceRepository.findAll();
        assertThat(caseStepInstanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
