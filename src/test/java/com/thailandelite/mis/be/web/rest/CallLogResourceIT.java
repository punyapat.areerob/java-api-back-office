package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CallLog;
import com.thailandelite.mis.be.repository.CallLogRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CallLogResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CallLogResourceIT {

    @Autowired
    private CallLogRepository callLogRepository;

    @Autowired
    private MockMvc restCallLogMockMvc;

    private CallLog callLog;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CallLog createEntity() {
        CallLog callLog = new CallLog();
        return callLog;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CallLog createUpdatedEntity() {
        CallLog callLog = new CallLog();
        return callLog;
    }

    @BeforeEach
    public void initTest() {
        callLogRepository.deleteAll();
        callLog = createEntity();
    }

    @Test
    public void createCallLog() throws Exception {
        int databaseSizeBeforeCreate = callLogRepository.findAll().size();
        // Create the CallLog
        restCallLogMockMvc.perform(post("/api/call-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(callLog)))
            .andExpect(status().isCreated());

        // Validate the CallLog in the database
        List<CallLog> callLogList = callLogRepository.findAll();
        assertThat(callLogList).hasSize(databaseSizeBeforeCreate + 1);
        CallLog testCallLog = callLogList.get(callLogList.size() - 1);
    }

    @Test
    public void createCallLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = callLogRepository.findAll().size();

        // Create the CallLog with an existing ID
        callLog.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCallLogMockMvc.perform(post("/api/call-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(callLog)))
            .andExpect(status().isBadRequest());

        // Validate the CallLog in the database
        List<CallLog> callLogList = callLogRepository.findAll();
        assertThat(callLogList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCallLogs() throws Exception {
        // Initialize the database
        callLogRepository.save(callLog);

        // Get all the callLogList
        restCallLogMockMvc.perform(get("/api/call-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(callLog.getId())));
    }
    
    @Test
    public void getCallLog() throws Exception {
        // Initialize the database
        callLogRepository.save(callLog);

        // Get the callLog
        restCallLogMockMvc.perform(get("/api/call-logs/{id}", callLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(callLog.getId()));
    }
    @Test
    public void getNonExistingCallLog() throws Exception {
        // Get the callLog
        restCallLogMockMvc.perform(get("/api/call-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCallLog() throws Exception {
        // Initialize the database
        callLogRepository.save(callLog);

        int databaseSizeBeforeUpdate = callLogRepository.findAll().size();

        // Update the callLog
        CallLog updatedCallLog = callLogRepository.findById(callLog.getId()).get();

        restCallLogMockMvc.perform(put("/api/call-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCallLog)))
            .andExpect(status().isOk());

        // Validate the CallLog in the database
        List<CallLog> callLogList = callLogRepository.findAll();
        assertThat(callLogList).hasSize(databaseSizeBeforeUpdate);
        CallLog testCallLog = callLogList.get(callLogList.size() - 1);
    }

    @Test
    public void updateNonExistingCallLog() throws Exception {
        int databaseSizeBeforeUpdate = callLogRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCallLogMockMvc.perform(put("/api/call-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(callLog)))
            .andExpect(status().isBadRequest());

        // Validate the CallLog in the database
        List<CallLog> callLogList = callLogRepository.findAll();
        assertThat(callLogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCallLog() throws Exception {
        // Initialize the database
        callLogRepository.save(callLog);

        int databaseSizeBeforeDelete = callLogRepository.findAll().size();

        // Delete the callLog
        restCallLogMockMvc.perform(delete("/api/call-logs/{id}", callLog.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CallLog> callLogList = callLogRepository.findAll();
        assertThat(callLogList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
