package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CaseActivity;
import com.thailandelite.mis.be.repository.CaseActivityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.thailandelite.mis.model.domain.enumeration.CaseActivityEvent;
/**
 * Integration tests for the {@link CaseActivityResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseActivityResourceIT {

    private static final String DEFAULT_CASE_INSTANCE_ID = "AAAAAAAAAA";
    private static final String UPDATED_CASE_INSTANCE_ID = "BBBBBBBBBB";

    private static final CaseActivityEvent DEFAULT_EVENT = CaseActivityEvent.CREATED;
    private static final CaseActivityEvent UPDATED_EVENT = CaseActivityEvent.COMMENT;

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_UPDATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private CaseActivityRepository caseActivityRepository;

    @Autowired
    private MockMvc restCaseActivityMockMvc;

    private CaseActivity caseActivity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseActivity createEntity() {
        CaseActivity caseActivity = new CaseActivity()
//            .caseInstanceId(DEFAULT_CASE_INSTANCE_ID)
//            .event(DEFAULT_EVENT)
//            .remark(DEFAULT_REMARK)
//            .createdBy(DEFAULT_CREATED_BY)
//            .updatedBy(DEFAULT_UPDATED_BY)
//            .createdAt(DEFAULT_CREATED_AT)
//            .updatedAt(DEFAULT_UPDATED_AT)
            ;
        return caseActivity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseActivity createUpdatedEntity() {
        CaseActivity caseActivity = new CaseActivity()
//            .caseInstanceId(UPDATED_CASE_INSTANCE_ID)
//            .event(UPDATED_EVENT)
//            .remark(UPDATED_REMARK)
//            .createdBy(UPDATED_CREATED_BY)
//            .updatedBy(UPDATED_UPDATED_BY)
//            .createdAt(UPDATED_CREATED_AT)
//            .updatedAt(UPDATED_UPDATED_AT)
            ;
        return caseActivity;
    }

    @BeforeEach
    public void initTest() {
        caseActivityRepository.deleteAll();
        caseActivity = createEntity();
    }

    @Test
    public void createCaseActivity() throws Exception {
        int databaseSizeBeforeCreate = caseActivityRepository.findAll().size();
        // Create the CaseActivity
        restCaseActivityMockMvc.perform(post("/api/case-activities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseActivity)))
            .andExpect(status().isCreated());

        // Validate the CaseActivity in the database
        List<CaseActivity> caseActivityList = caseActivityRepository.findAll();
        assertThat(caseActivityList).hasSize(databaseSizeBeforeCreate + 1);
        CaseActivity testCaseActivity = caseActivityList.get(caseActivityList.size() - 1);
        assertThat(testCaseActivity.getCaseInstanceId()).isEqualTo(DEFAULT_CASE_INSTANCE_ID);
        assertThat(testCaseActivity.getEvent()).isEqualTo(DEFAULT_EVENT);
        assertThat(testCaseActivity.getRemark()).isEqualTo(DEFAULT_REMARK);
//        assertThat(testCaseActivity.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
//        assertThat(testCaseActivity.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
//        assertThat(testCaseActivity.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
//        assertThat(testCaseActivity.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createCaseActivityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseActivityRepository.findAll().size();

        // Create the CaseActivity with an existing ID
        caseActivity.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseActivityMockMvc.perform(post("/api/case-activities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseActivity)))
            .andExpect(status().isBadRequest());

        // Validate the CaseActivity in the database
        List<CaseActivity> caseActivityList = caseActivityRepository.findAll();
        assertThat(caseActivityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseActivities() throws Exception {
        // Initialize the database
        caseActivityRepository.save(caseActivity);

        // Get all the caseActivityList
        restCaseActivityMockMvc.perform(get("/api/case-activities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseActivity.getId())))
            .andExpect(jsonPath("$.[*].caseInstanceId").value(hasItem(DEFAULT_CASE_INSTANCE_ID)))
            .andExpect(jsonPath("$.[*].event").value(hasItem(DEFAULT_EVENT.toString())))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }

    @Test
    public void getCaseActivity() throws Exception {
        // Initialize the database
        caseActivityRepository.save(caseActivity);

        // Get the caseActivity
        restCaseActivityMockMvc.perform(get("/api/case-activities/{id}", caseActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseActivity.getId()))
            .andExpect(jsonPath("$.caseInstanceId").value(DEFAULT_CASE_INSTANCE_ID))
            .andExpect(jsonPath("$.event").value(DEFAULT_EVENT.toString()))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }
    @Test
    public void getNonExistingCaseActivity() throws Exception {
        // Get the caseActivity
        restCaseActivityMockMvc.perform(get("/api/case-activities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseActivity() throws Exception {
        // Initialize the database
        caseActivityRepository.save(caseActivity);

        int databaseSizeBeforeUpdate = caseActivityRepository.findAll().size();

        // Update the caseActivity
        CaseActivity updatedCaseActivity = caseActivityRepository.findById(caseActivity.getId()).get();
//        updatedCaseActivity
//            .caseInstanceId(UPDATED_CASE_INSTANCE_ID)
//            .event(UPDATED_EVENT)
//            .remark(UPDATED_REMARK)
//            .createdBy(UPDATED_CREATED_BY)
//            .updatedBy(UPDATED_UPDATED_BY)
//            .createdAt(UPDATED_CREATED_AT)
//            .updatedAt(UPDATED_UPDATED_AT);

        restCaseActivityMockMvc.perform(put("/api/case-activities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseActivity)))
            .andExpect(status().isOk());

        // Validate the CaseActivity in the database
        List<CaseActivity> caseActivityList = caseActivityRepository.findAll();
        assertThat(caseActivityList).hasSize(databaseSizeBeforeUpdate);
        CaseActivity testCaseActivity = caseActivityList.get(caseActivityList.size() - 1);
        assertThat(testCaseActivity.getCaseInstanceId()).isEqualTo(UPDATED_CASE_INSTANCE_ID);
        assertThat(testCaseActivity.getEvent()).isEqualTo(UPDATED_EVENT);
        assertThat(testCaseActivity.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testCaseActivity.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
//        assertThat(testCaseActivity.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
//        assertThat(testCaseActivity.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
//        assertThat(testCaseActivity.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingCaseActivity() throws Exception {
        int databaseSizeBeforeUpdate = caseActivityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseActivityMockMvc.perform(put("/api/case-activities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseActivity)))
            .andExpect(status().isBadRequest());

        // Validate the CaseActivity in the database
        List<CaseActivity> caseActivityList = caseActivityRepository.findAll();
        assertThat(caseActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseActivity() throws Exception {
        // Initialize the database
        caseActivityRepository.save(caseActivity);

        int databaseSizeBeforeDelete = caseActivityRepository.findAll().size();

        // Delete the caseActivity
        restCaseActivityMockMvc.perform(delete("/api/case-activities/{id}", caseActivity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseActivity> caseActivityList = caseActivityRepository.findAll();
        assertThat(caseActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
