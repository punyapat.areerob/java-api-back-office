package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.ContactTagMaster;
import com.thailandelite.mis.be.repository.ContactTagMasterRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactTagMasterResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactTagMasterResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_COLOR = "BBBBBBBBBB";

    private static final String DEFAULT_CREATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATE_BY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private ContactTagMasterRepository contactTagMasterRepository;

    @Autowired
    private MockMvc restContactTagMasterMockMvc;

    private ContactTagMaster contactTagMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactTagMaster createEntity() {
        ContactTagMaster contactTagMaster = new ContactTagMaster()
            .name(DEFAULT_NAME)
            .color(DEFAULT_COLOR)
//            .createBy(DEFAULT_CREATE_BY)
            .active(DEFAULT_ACTIVE);
        return contactTagMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactTagMaster createUpdatedEntity() {
        ContactTagMaster contactTagMaster = new ContactTagMaster()
            .name(UPDATED_NAME)
            .color(UPDATED_COLOR)
//            .createBy(UPDATED_CREATE_BY)
            .active(UPDATED_ACTIVE);
        return contactTagMaster;
    }

    @BeforeEach
    public void initTest() {
        contactTagMasterRepository.deleteAll();
        contactTagMaster = createEntity();
    }

    @Test
    public void createContactTagMaster() throws Exception {
        int databaseSizeBeforeCreate = contactTagMasterRepository.findAll().size();
        // Create the ContactTagMaster
        restContactTagMasterMockMvc.perform(post("/api/contact-tag-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactTagMaster)))
            .andExpect(status().isCreated());

        // Validate the ContactTagMaster in the database
        List<ContactTagMaster> contactTagMasterList = contactTagMasterRepository.findAll();
        assertThat(contactTagMasterList).hasSize(databaseSizeBeforeCreate + 1);
        ContactTagMaster testContactTagMaster = contactTagMasterList.get(contactTagMasterList.size() - 1);
        assertThat(testContactTagMaster.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testContactTagMaster.getColor()).isEqualTo(DEFAULT_COLOR);
//        assertThat(testContactTagMaster.getCreateBy()).isEqualTo(DEFAULT_CREATE_BY);
        assertThat(testContactTagMaster.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    public void createContactTagMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactTagMasterRepository.findAll().size();

        // Create the ContactTagMaster with an existing ID
        contactTagMaster.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactTagMasterMockMvc.perform(post("/api/contact-tag-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactTagMaster)))
            .andExpect(status().isBadRequest());

        // Validate the ContactTagMaster in the database
        List<ContactTagMaster> contactTagMasterList = contactTagMasterRepository.findAll();
        assertThat(contactTagMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllContactTagMasters() throws Exception {
        // Initialize the database
        contactTagMasterRepository.save(contactTagMaster);

        // Get all the contactTagMasterList
        restContactTagMasterMockMvc.perform(get("/api/contact-tag-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactTagMaster.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR)))
            .andExpect(jsonPath("$.[*].createBy").value(hasItem(DEFAULT_CREATE_BY)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    public void getContactTagMaster() throws Exception {
        // Initialize the database
        contactTagMasterRepository.save(contactTagMaster);

        // Get the contactTagMaster
        restContactTagMasterMockMvc.perform(get("/api/contact-tag-masters/{id}", contactTagMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactTagMaster.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.color").value(DEFAULT_COLOR))
            .andExpect(jsonPath("$.createBy").value(DEFAULT_CREATE_BY))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }
    @Test
    public void getNonExistingContactTagMaster() throws Exception {
        // Get the contactTagMaster
        restContactTagMasterMockMvc.perform(get("/api/contact-tag-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateContactTagMaster() throws Exception {
        // Initialize the database
        contactTagMasterRepository.save(contactTagMaster);

        int databaseSizeBeforeUpdate = contactTagMasterRepository.findAll().size();

        // Update the contactTagMaster
        ContactTagMaster updatedContactTagMaster = contactTagMasterRepository.findById(contactTagMaster.getId()).get();
        updatedContactTagMaster
            .name(UPDATED_NAME)
            .color(UPDATED_COLOR)
//            .createBy(UPDATED_CREATE_BY)
            .active(UPDATED_ACTIVE);

        restContactTagMasterMockMvc.perform(put("/api/contact-tag-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedContactTagMaster)))
            .andExpect(status().isOk());

        // Validate the ContactTagMaster in the database
        List<ContactTagMaster> contactTagMasterList = contactTagMasterRepository.findAll();
        assertThat(contactTagMasterList).hasSize(databaseSizeBeforeUpdate);
        ContactTagMaster testContactTagMaster = contactTagMasterList.get(contactTagMasterList.size() - 1);
        assertThat(testContactTagMaster.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testContactTagMaster.getColor()).isEqualTo(UPDATED_COLOR);
//        assertThat(testContactTagMaster.getCreateBy()).isEqualTo(UPDATED_CREATE_BY);
        assertThat(testContactTagMaster.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    public void updateNonExistingContactTagMaster() throws Exception {
        int databaseSizeBeforeUpdate = contactTagMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactTagMasterMockMvc.perform(put("/api/contact-tag-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactTagMaster)))
            .andExpect(status().isBadRequest());

        // Validate the ContactTagMaster in the database
        List<ContactTagMaster> contactTagMasterList = contactTagMasterRepository.findAll();
        assertThat(contactTagMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteContactTagMaster() throws Exception {
        // Initialize the database
        contactTagMasterRepository.save(contactTagMaster);

        int databaseSizeBeforeDelete = contactTagMasterRepository.findAll().size();

        // Delete the contactTagMaster
        restContactTagMasterMockMvc.perform(delete("/api/contact-tag-masters/{id}", contactTagMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactTagMaster> contactTagMasterList = contactTagMasterRepository.findAll();
        assertThat(contactTagMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
