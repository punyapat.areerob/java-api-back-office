package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.master.ChannelKnow;
import com.thailandelite.mis.be.repository.ChannelKnowRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ChannelKnowResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ChannelKnowResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private ChannelKnowRepository channelKnowRepository;

    @Autowired
    private MockMvc restChannelKnowMockMvc;

    private ChannelKnow channelKnow;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChannelKnow createEntity() {
        ChannelKnow channelKnow = new ChannelKnow()
            .name(DEFAULT_NAME)
            .active(DEFAULT_ACTIVE);
        return channelKnow;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChannelKnow createUpdatedEntity() {
        ChannelKnow channelKnow = new ChannelKnow()
            .name(UPDATED_NAME)
            .active(UPDATED_ACTIVE);
        return channelKnow;
    }

    @BeforeEach
    public void initTest() {
        channelKnowRepository.deleteAll();
        channelKnow = createEntity();
    }

    @Test
    public void createChannelKnow() throws Exception {
        int databaseSizeBeforeCreate = channelKnowRepository.findAll().size();
        // Create the ChannelKnow
        restChannelKnowMockMvc.perform(post("/api/channel-knows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(channelKnow)))
            .andExpect(status().isCreated());

        // Validate the ChannelKnow in the database
        List<ChannelKnow> channelKnowList = channelKnowRepository.findAll();
        assertThat(channelKnowList).hasSize(databaseSizeBeforeCreate + 1);
        ChannelKnow testChannelKnow = channelKnowList.get(channelKnowList.size() - 1);
        assertThat(testChannelKnow.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testChannelKnow.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    public void createChannelKnowWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = channelKnowRepository.findAll().size();

        // Create the ChannelKnow with an existing ID
        channelKnow.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restChannelKnowMockMvc.perform(post("/api/channel-knows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(channelKnow)))
            .andExpect(status().isBadRequest());

        // Validate the ChannelKnow in the database
        List<ChannelKnow> channelKnowList = channelKnowRepository.findAll();
        assertThat(channelKnowList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllChannelKnows() throws Exception {
        // Initialize the database
        channelKnowRepository.save(channelKnow);

        // Get all the channelKnowList
        restChannelKnowMockMvc.perform(get("/api/channel-knows?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(channelKnow.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    public void getChannelKnow() throws Exception {
        // Initialize the database
        channelKnowRepository.save(channelKnow);

        // Get the channelKnow
        restChannelKnowMockMvc.perform(get("/api/channel-knows/{id}", channelKnow.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(channelKnow.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }
    @Test
    public void getNonExistingChannelKnow() throws Exception {
        // Get the channelKnow
        restChannelKnowMockMvc.perform(get("/api/channel-knows/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateChannelKnow() throws Exception {
        // Initialize the database
        channelKnowRepository.save(channelKnow);

        int databaseSizeBeforeUpdate = channelKnowRepository.findAll().size();

        // Update the channelKnow
        ChannelKnow updatedChannelKnow = channelKnowRepository.findById(channelKnow.getId()).get();
        updatedChannelKnow
            .name(UPDATED_NAME)
            .active(UPDATED_ACTIVE);

        restChannelKnowMockMvc.perform(put("/api/channel-knows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedChannelKnow)))
            .andExpect(status().isOk());

        // Validate the ChannelKnow in the database
        List<ChannelKnow> channelKnowList = channelKnowRepository.findAll();
        assertThat(channelKnowList).hasSize(databaseSizeBeforeUpdate);
        ChannelKnow testChannelKnow = channelKnowList.get(channelKnowList.size() - 1);
        assertThat(testChannelKnow.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testChannelKnow.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    public void updateNonExistingChannelKnow() throws Exception {
        int databaseSizeBeforeUpdate = channelKnowRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChannelKnowMockMvc.perform(put("/api/channel-knows")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(channelKnow)))
            .andExpect(status().isBadRequest());

        // Validate the ChannelKnow in the database
        List<ChannelKnow> channelKnowList = channelKnowRepository.findAll();
        assertThat(channelKnowList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteChannelKnow() throws Exception {
        // Initialize the database
        channelKnowRepository.save(channelKnow);

        int databaseSizeBeforeDelete = channelKnowRepository.findAll().size();

        // Delete the channelKnow
        restChannelKnowMockMvc.perform(delete("/api/channel-knows/{id}", channelKnow.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChannelKnow> channelKnowList = channelKnowRepository.findAll();
        assertThat(channelKnowList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
