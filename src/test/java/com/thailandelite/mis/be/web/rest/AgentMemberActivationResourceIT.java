package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.agent.AgentMemberActivation;
import com.thailandelite.mis.be.repository.AgentMemberActivationRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.thailandelite.mis.be.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AgentMemberActivationResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AgentMemberActivationResourceIT {

    private static final String DEFAULT_AGENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_INVOICE = "AAAAAAAAAA";
    private static final String UPDATED_INVOICE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_ACTIVATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_ACTIVATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENCY = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY = "BBBBBBBBBB";

    private static final Integer DEFAULT_MONEY = 1;
    private static final Integer UPDATED_MONEY = 2;

    @Autowired
    private AgentMemberActivationRepository agentMemberActivationRepository;

    @Autowired
    private MockMvc restAgentMemberActivationMockMvc;

    private AgentMemberActivation agentMemberActivation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentMemberActivation createEntity() {
        AgentMemberActivation agentMemberActivation = new AgentMemberActivation()
            .agentId(DEFAULT_AGENT_ID)
            .invoice(DEFAULT_INVOICE)
            .date(DEFAULT_DATE)
            .activate(DEFAULT_ACTIVATE)
            .remark(DEFAULT_REMARK)
            .currency(DEFAULT_CURRENCY)
            .money(DEFAULT_MONEY);
        return agentMemberActivation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentMemberActivation createUpdatedEntity() {
        AgentMemberActivation agentMemberActivation = new AgentMemberActivation()
            .agentId(UPDATED_AGENT_ID)
            .invoice(UPDATED_INVOICE)
            .date(UPDATED_DATE)
            .activate(UPDATED_ACTIVATE)
            .remark(UPDATED_REMARK)
            .currency(UPDATED_CURRENCY)
            .money(UPDATED_MONEY);
        return agentMemberActivation;
    }

    @BeforeEach
    public void initTest() {
        agentMemberActivationRepository.deleteAll();
        agentMemberActivation = createEntity();
    }

    @Test
    public void createAgentMemberActivation() throws Exception {
        int databaseSizeBeforeCreate = agentMemberActivationRepository.findAll().size();
        // Create the AgentMemberActivation
        restAgentMemberActivationMockMvc.perform(post("/api/agent-member-activations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentMemberActivation)))
            .andExpect(status().isCreated());

        // Validate the AgentMemberActivation in the database
        List<AgentMemberActivation> agentMemberActivationList = agentMemberActivationRepository.findAll();
        assertThat(agentMemberActivationList).hasSize(databaseSizeBeforeCreate + 1);
        AgentMemberActivation testAgentMemberActivation = agentMemberActivationList.get(agentMemberActivationList.size() - 1);
        assertThat(testAgentMemberActivation.getAgentId()).isEqualTo(DEFAULT_AGENT_ID);
        assertThat(testAgentMemberActivation.getInvoice()).isEqualTo(DEFAULT_INVOICE);
        assertThat(testAgentMemberActivation.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testAgentMemberActivation.getActivate()).isEqualTo(DEFAULT_ACTIVATE);
        assertThat(testAgentMemberActivation.getRemark()).isEqualTo(DEFAULT_REMARK);
        assertThat(testAgentMemberActivation.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(testAgentMemberActivation.getMoney()).isEqualTo(DEFAULT_MONEY);
    }

    @Test
    public void createAgentMemberActivationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentMemberActivationRepository.findAll().size();

        // Create the AgentMemberActivation with an existing ID
        agentMemberActivation.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentMemberActivationMockMvc.perform(post("/api/agent-member-activations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentMemberActivation)))
            .andExpect(status().isBadRequest());

        // Validate the AgentMemberActivation in the database
        List<AgentMemberActivation> agentMemberActivationList = agentMemberActivationRepository.findAll();
        assertThat(agentMemberActivationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllAgentMemberActivations() throws Exception {
        // Initialize the database
        agentMemberActivationRepository.save(agentMemberActivation);

        // Get all the agentMemberActivationList
        restAgentMemberActivationMockMvc.perform(get("/api/agent-member-activations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentMemberActivation.getId())))
            .andExpect(jsonPath("$.[*].agentId").value(hasItem(DEFAULT_AGENT_ID)))
            .andExpect(jsonPath("$.[*].invoice").value(hasItem(DEFAULT_INVOICE)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(sameInstant(DEFAULT_DATE))))
            .andExpect(jsonPath("$.[*].activate").value(hasItem(sameInstant(DEFAULT_ACTIVATE))))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK)))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY)))
            .andExpect(jsonPath("$.[*].money").value(hasItem(DEFAULT_MONEY)));
    }

    @Test
    public void getAgentMemberActivation() throws Exception {
        // Initialize the database
        agentMemberActivationRepository.save(agentMemberActivation);

        // Get the agentMemberActivation
        restAgentMemberActivationMockMvc.perform(get("/api/agent-member-activations/{id}", agentMemberActivation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agentMemberActivation.getId()))
            .andExpect(jsonPath("$.agentId").value(DEFAULT_AGENT_ID))
            .andExpect(jsonPath("$.invoice").value(DEFAULT_INVOICE))
            .andExpect(jsonPath("$.date").value(sameInstant(DEFAULT_DATE)))
            .andExpect(jsonPath("$.activate").value(sameInstant(DEFAULT_ACTIVATE)))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY))
            .andExpect(jsonPath("$.money").value(DEFAULT_MONEY));
    }
    @Test
    public void getNonExistingAgentMemberActivation() throws Exception {
        // Get the agentMemberActivation
        restAgentMemberActivationMockMvc.perform(get("/api/agent-member-activations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateAgentMemberActivation() throws Exception {
        // Initialize the database
        agentMemberActivationRepository.save(agentMemberActivation);

        int databaseSizeBeforeUpdate = agentMemberActivationRepository.findAll().size();

        // Update the agentMemberActivation
        AgentMemberActivation updatedAgentMemberActivation = agentMemberActivationRepository.findById(agentMemberActivation.getId()).get();
        updatedAgentMemberActivation
            .agentId(UPDATED_AGENT_ID)
            .invoice(UPDATED_INVOICE)
            .date(UPDATED_DATE)
            .activate(UPDATED_ACTIVATE)
            .remark(UPDATED_REMARK)
            .currency(UPDATED_CURRENCY)
            .money(UPDATED_MONEY);

        restAgentMemberActivationMockMvc.perform(put("/api/agent-member-activations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgentMemberActivation)))
            .andExpect(status().isOk());

        // Validate the AgentMemberActivation in the database
        List<AgentMemberActivation> agentMemberActivationList = agentMemberActivationRepository.findAll();
        assertThat(agentMemberActivationList).hasSize(databaseSizeBeforeUpdate);
        AgentMemberActivation testAgentMemberActivation = agentMemberActivationList.get(agentMemberActivationList.size() - 1);
        assertThat(testAgentMemberActivation.getAgentId()).isEqualTo(UPDATED_AGENT_ID);
        assertThat(testAgentMemberActivation.getInvoice()).isEqualTo(UPDATED_INVOICE);
        assertThat(testAgentMemberActivation.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testAgentMemberActivation.getActivate()).isEqualTo(UPDATED_ACTIVATE);
        assertThat(testAgentMemberActivation.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testAgentMemberActivation.getCurrency()).isEqualTo(UPDATED_CURRENCY);
        assertThat(testAgentMemberActivation.getMoney()).isEqualTo(UPDATED_MONEY);
    }

    @Test
    public void updateNonExistingAgentMemberActivation() throws Exception {
        int databaseSizeBeforeUpdate = agentMemberActivationRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentMemberActivationMockMvc.perform(put("/api/agent-member-activations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentMemberActivation)))
            .andExpect(status().isBadRequest());

        // Validate the AgentMemberActivation in the database
        List<AgentMemberActivation> agentMemberActivationList = agentMemberActivationRepository.findAll();
        assertThat(agentMemberActivationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteAgentMemberActivation() throws Exception {
        // Initialize the database
        agentMemberActivationRepository.save(agentMemberActivation);

        int databaseSizeBeforeDelete = agentMemberActivationRepository.findAll().size();

        // Delete the agentMemberActivation
        restAgentMemberActivationMockMvc.perform(delete("/api/agent-member-activations/{id}", agentMemberActivation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AgentMemberActivation> agentMemberActivationList = agentMemberActivationRepository.findAll();
        assertThat(agentMemberActivationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
