package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.agent.AgentLog;
import com.thailandelite.mis.be.repository.AgentLogRepository;
import com.thailandelite.mis.be.service.AgentLogService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AgentLogResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AgentLogResourceIT {

    @Autowired
    private AgentLogRepository agentLogRepository;

    @Autowired
    private AgentLogService agentLogService;

    @Autowired
    private MockMvc restAgentLogMockMvc;

    private AgentLog agentLog;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentLog createEntity() {
        AgentLog agentLog = new AgentLog();
        return agentLog;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentLog createUpdatedEntity() {
        AgentLog agentLog = new AgentLog();
        return agentLog;
    }

    @BeforeEach
    public void initTest() {
        agentLogRepository.deleteAll();
        agentLog = createEntity();
    }

    @Test
    public void createAgentLog() throws Exception {
        int databaseSizeBeforeCreate = agentLogRepository.findAll().size();
        // Create the AgentLog
        restAgentLogMockMvc.perform(post("/api/agent-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentLog)))
            .andExpect(status().isCreated());

        // Validate the AgentLog in the database
        List<AgentLog> agentLogList = agentLogRepository.findAll();
        assertThat(agentLogList).hasSize(databaseSizeBeforeCreate + 1);
        AgentLog testAgentLog = agentLogList.get(agentLogList.size() - 1);
    }

    @Test
    public void createAgentLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentLogRepository.findAll().size();

        // Create the AgentLog with an existing ID
        agentLog.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentLogMockMvc.perform(post("/api/agent-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentLog)))
            .andExpect(status().isBadRequest());

        // Validate the AgentLog in the database
        List<AgentLog> agentLogList = agentLogRepository.findAll();
        assertThat(agentLogList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllAgentLogs() throws Exception {
        // Initialize the database
        agentLogRepository.save(agentLog);

        // Get all the agentLogList
        restAgentLogMockMvc.perform(get("/api/agent-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentLog.getId())));
    }

    @Test
    public void getAgentLog() throws Exception {
        // Initialize the database
        agentLogRepository.save(agentLog);

        // Get the agentLog
        restAgentLogMockMvc.perform(get("/api/agent-logs/{id}", agentLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agentLog.getId()));
    }
    @Test
    public void getNonExistingAgentLog() throws Exception {
        // Get the agentLog
        restAgentLogMockMvc.perform(get("/api/agent-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateAgentLog() throws Exception {
        // Initialize the database
//        agentLogService.save(agentLog);

        int databaseSizeBeforeUpdate = agentLogRepository.findAll().size();

        // Update the agentLog
        AgentLog updatedAgentLog = agentLogRepository.findById(agentLog.getId()).get();

        restAgentLogMockMvc.perform(put("/api/agent-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgentLog)))
            .andExpect(status().isOk());

        // Validate the AgentLog in the database
        List<AgentLog> agentLogList = agentLogRepository.findAll();
        assertThat(agentLogList).hasSize(databaseSizeBeforeUpdate);
        AgentLog testAgentLog = agentLogList.get(agentLogList.size() - 1);
    }

    @Test
    public void updateNonExistingAgentLog() throws Exception {
        int databaseSizeBeforeUpdate = agentLogRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentLogMockMvc.perform(put("/api/agent-logs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentLog)))
            .andExpect(status().isBadRequest());

        // Validate the AgentLog in the database
        List<AgentLog> agentLogList = agentLogRepository.findAll();
        assertThat(agentLogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteAgentLog() throws Exception {
        // Initialize the database
//        agentLogService.save(agentLog);

        int databaseSizeBeforeDelete = agentLogRepository.findAll().size();

        // Delete the agentLog
        restAgentLogMockMvc.perform(delete("/api/agent-logs/{id}", agentLog.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AgentLog> agentLogList = agentLogRepository.findAll();
        assertThat(agentLogList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
