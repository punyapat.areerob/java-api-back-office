package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.ActionInstance;
import com.thailandelite.mis.be.repository.ActionInstanceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.thailandelite.mis.be.domain.enumeration.ActionType;
/**
 * Integration tests for the {@link ActionInstanceResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ActionInstanceResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NEXT = "AAAAAAAAAA";
    private static final String UPDATED_NEXT = "BBBBBBBBBB";

    private static final ActionType DEFAULT_TYPE = ActionType.START;
    private static final ActionType UPDATED_TYPE = ActionType.DONE;

    @Autowired
    private ActionInstanceRepository actionInstanceRepository;

    @Autowired
    private MockMvc restActionInstanceMockMvc;

    private ActionInstance actionInstance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActionInstance createEntity() {
        ActionInstance actionInstance = new ActionInstance()
            .name(DEFAULT_NAME)
            .next(DEFAULT_NEXT)
            .type(DEFAULT_TYPE);
        return actionInstance;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActionInstance createUpdatedEntity() {
        ActionInstance actionInstance = new ActionInstance()
            .name(UPDATED_NAME)
            .next(UPDATED_NEXT)
            .type(UPDATED_TYPE);
        return actionInstance;
    }

    @BeforeEach
    public void initTest() {
        actionInstanceRepository.deleteAll();
        actionInstance = createEntity();
    }

    @Test
    public void createActionInstance() throws Exception {
        int databaseSizeBeforeCreate = actionInstanceRepository.findAll().size();
        // Create the ActionInstance
        restActionInstanceMockMvc.perform(post("/api/action-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(actionInstance)))
            .andExpect(status().isCreated());

        // Validate the ActionInstance in the database
        List<ActionInstance> actionInstanceList = actionInstanceRepository.findAll();
        assertThat(actionInstanceList).hasSize(databaseSizeBeforeCreate + 1);
        ActionInstance testActionInstance = actionInstanceList.get(actionInstanceList.size() - 1);
        assertThat(testActionInstance.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testActionInstance.getNext()).isEqualTo(DEFAULT_NEXT);
        assertThat(testActionInstance.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    public void createActionInstanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = actionInstanceRepository.findAll().size();

        // Create the ActionInstance with an existing ID
        actionInstance.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restActionInstanceMockMvc.perform(post("/api/action-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(actionInstance)))
            .andExpect(status().isBadRequest());

        // Validate the ActionInstance in the database
        List<ActionInstance> actionInstanceList = actionInstanceRepository.findAll();
        assertThat(actionInstanceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllActionInstances() throws Exception {
        // Initialize the database
        actionInstanceRepository.save(actionInstance);

        // Get all the actionInstanceList
        restActionInstanceMockMvc.perform(get("/api/action-instances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(actionInstance.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].next").value(hasItem(DEFAULT_NEXT)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    public void getActionInstance() throws Exception {
        // Initialize the database
        actionInstanceRepository.save(actionInstance);

        // Get the actionInstance
        restActionInstanceMockMvc.perform(get("/api/action-instances/{id}", actionInstance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(actionInstance.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.next").value(DEFAULT_NEXT))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }
    @Test
    public void getNonExistingActionInstance() throws Exception {
        // Get the actionInstance
        restActionInstanceMockMvc.perform(get("/api/action-instances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateActionInstance() throws Exception {
        // Initialize the database
        actionInstanceRepository.save(actionInstance);

        int databaseSizeBeforeUpdate = actionInstanceRepository.findAll().size();

        // Update the actionInstance
        ActionInstance updatedActionInstance = actionInstanceRepository.findById(actionInstance.getId()).get();
        updatedActionInstance
            .name(UPDATED_NAME)
            .next(UPDATED_NEXT)
            .type(UPDATED_TYPE);

        restActionInstanceMockMvc.perform(put("/api/action-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedActionInstance)))
            .andExpect(status().isOk());

        // Validate the ActionInstance in the database
        List<ActionInstance> actionInstanceList = actionInstanceRepository.findAll();
        assertThat(actionInstanceList).hasSize(databaseSizeBeforeUpdate);
        ActionInstance testActionInstance = actionInstanceList.get(actionInstanceList.size() - 1);
        assertThat(testActionInstance.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testActionInstance.getNext()).isEqualTo(UPDATED_NEXT);
        assertThat(testActionInstance.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    public void updateNonExistingActionInstance() throws Exception {
        int databaseSizeBeforeUpdate = actionInstanceRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restActionInstanceMockMvc.perform(put("/api/action-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(actionInstance)))
            .andExpect(status().isBadRequest());

        // Validate the ActionInstance in the database
        List<ActionInstance> actionInstanceList = actionInstanceRepository.findAll();
        assertThat(actionInstanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteActionInstance() throws Exception {
        // Initialize the database
        actionInstanceRepository.save(actionInstance);

        int databaseSizeBeforeDelete = actionInstanceRepository.findAll().size();

        // Delete the actionInstance
        restActionInstanceMockMvc.perform(delete("/api/action-instances/{id}", actionInstance.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ActionInstance> actionInstanceList = actionInstanceRepository.findAll();
        assertThat(actionInstanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
