package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.StepOwner;
import com.thailandelite.mis.be.repository.StepOwnerRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StepOwnerResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class StepOwnerResourceIT {

    private static final String DEFAULT_STAFF_ID = "AAAAAAAAAA";
    private static final String UPDATED_STAFF_ID = "BBBBBBBBBB";

    private static final String DEFAULT_GIVEN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GIVEN_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SUR_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUR_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private StepOwnerRepository stepOwnerRepository;

    @Autowired
    private MockMvc restStepOwnerMockMvc;

    private StepOwner stepOwner;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StepOwner createEntity() {
        StepOwner stepOwner = new StepOwner()
            .staffId(DEFAULT_STAFF_ID)
            .givenName(DEFAULT_GIVEN_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .surName(DEFAULT_SUR_NAME)
            .email(DEFAULT_EMAIL);
        return stepOwner;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StepOwner createUpdatedEntity() {
        StepOwner stepOwner = new StepOwner()
            .staffId(UPDATED_STAFF_ID)
            .givenName(UPDATED_GIVEN_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .surName(UPDATED_SUR_NAME)
            .email(UPDATED_EMAIL);
        return stepOwner;
    }

    @BeforeEach
    public void initTest() {
        stepOwnerRepository.deleteAll();
        stepOwner = createEntity();
    }

    @Test
    public void createStepOwner() throws Exception {
        int databaseSizeBeforeCreate = stepOwnerRepository.findAll().size();
        // Create the StepOwner
        restStepOwnerMockMvc.perform(post("/api/step-owners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(stepOwner)))
            .andExpect(status().isCreated());

        // Validate the StepOwner in the database
        List<StepOwner> stepOwnerList = stepOwnerRepository.findAll();
        assertThat(stepOwnerList).hasSize(databaseSizeBeforeCreate + 1);
        StepOwner testStepOwner = stepOwnerList.get(stepOwnerList.size() - 1);
        assertThat(testStepOwner.getStaffId()).isEqualTo(DEFAULT_STAFF_ID);
        assertThat(testStepOwner.getGivenName()).isEqualTo(DEFAULT_GIVEN_NAME);
        assertThat(testStepOwner.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testStepOwner.getSurName()).isEqualTo(DEFAULT_SUR_NAME);
        assertThat(testStepOwner.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    public void createStepOwnerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = stepOwnerRepository.findAll().size();

        // Create the StepOwner with an existing ID
        stepOwner.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restStepOwnerMockMvc.perform(post("/api/step-owners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(stepOwner)))
            .andExpect(status().isBadRequest());

        // Validate the StepOwner in the database
        List<StepOwner> stepOwnerList = stepOwnerRepository.findAll();
        assertThat(stepOwnerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllStepOwners() throws Exception {
        // Initialize the database
        stepOwnerRepository.save(stepOwner);

        // Get all the stepOwnerList
        restStepOwnerMockMvc.perform(get("/api/step-owners?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stepOwner.getId())))
            .andExpect(jsonPath("$.[*].staffId").value(hasItem(DEFAULT_STAFF_ID)))
            .andExpect(jsonPath("$.[*].givenName").value(hasItem(DEFAULT_GIVEN_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].surName").value(hasItem(DEFAULT_SUR_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }
    
    @Test
    public void getStepOwner() throws Exception {
        // Initialize the database
        stepOwnerRepository.save(stepOwner);

        // Get the stepOwner
        restStepOwnerMockMvc.perform(get("/api/step-owners/{id}", stepOwner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(stepOwner.getId()))
            .andExpect(jsonPath("$.staffId").value(DEFAULT_STAFF_ID))
            .andExpect(jsonPath("$.givenName").value(DEFAULT_GIVEN_NAME))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME))
            .andExpect(jsonPath("$.surName").value(DEFAULT_SUR_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL));
    }
    @Test
    public void getNonExistingStepOwner() throws Exception {
        // Get the stepOwner
        restStepOwnerMockMvc.perform(get("/api/step-owners/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateStepOwner() throws Exception {
        // Initialize the database
        stepOwnerRepository.save(stepOwner);

        int databaseSizeBeforeUpdate = stepOwnerRepository.findAll().size();

        // Update the stepOwner
        StepOwner updatedStepOwner = stepOwnerRepository.findById(stepOwner.getId()).get();
        updatedStepOwner
            .staffId(UPDATED_STAFF_ID)
            .givenName(UPDATED_GIVEN_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .surName(UPDATED_SUR_NAME)
            .email(UPDATED_EMAIL);

        restStepOwnerMockMvc.perform(put("/api/step-owners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedStepOwner)))
            .andExpect(status().isOk());

        // Validate the StepOwner in the database
        List<StepOwner> stepOwnerList = stepOwnerRepository.findAll();
        assertThat(stepOwnerList).hasSize(databaseSizeBeforeUpdate);
        StepOwner testStepOwner = stepOwnerList.get(stepOwnerList.size() - 1);
        assertThat(testStepOwner.getStaffId()).isEqualTo(UPDATED_STAFF_ID);
        assertThat(testStepOwner.getGivenName()).isEqualTo(UPDATED_GIVEN_NAME);
        assertThat(testStepOwner.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testStepOwner.getSurName()).isEqualTo(UPDATED_SUR_NAME);
        assertThat(testStepOwner.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    public void updateNonExistingStepOwner() throws Exception {
        int databaseSizeBeforeUpdate = stepOwnerRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStepOwnerMockMvc.perform(put("/api/step-owners")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(stepOwner)))
            .andExpect(status().isBadRequest());

        // Validate the StepOwner in the database
        List<StepOwner> stepOwnerList = stepOwnerRepository.findAll();
        assertThat(stepOwnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteStepOwner() throws Exception {
        // Initialize the database
        stepOwnerRepository.save(stepOwner);

        int databaseSizeBeforeDelete = stepOwnerRepository.findAll().size();

        // Delete the stepOwner
        restStepOwnerMockMvc.perform(delete("/api/step-owners/{id}", stepOwner.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<StepOwner> stepOwnerList = stepOwnerRepository.findAll();
        assertThat(stepOwnerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
