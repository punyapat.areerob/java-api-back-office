package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.MemberPhone;
import com.thailandelite.mis.be.repository.MemberPhoneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MemberPhoneResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MemberPhoneResourceIT {

    @Autowired
    private MemberPhoneRepository memberPhoneRepository;

    @Autowired
    private MockMvc restMemberPhoneMockMvc;

    private MemberPhone memberPhone;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MemberPhone createEntity() {
        MemberPhone memberPhone = new MemberPhone();
        return memberPhone;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MemberPhone createUpdatedEntity() {
        MemberPhone memberPhone = new MemberPhone();
        return memberPhone;
    }

    @BeforeEach
    public void initTest() {
        memberPhoneRepository.deleteAll();
        memberPhone = createEntity();
    }

    @Test
    public void createMemberPhone() throws Exception {
        int databaseSizeBeforeCreate = memberPhoneRepository.findAll().size();
        // Create the MemberPhone
        restMemberPhoneMockMvc.perform(post("/api/member-phones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(memberPhone)))
            .andExpect(status().isCreated());

        // Validate the MemberPhone in the database
        List<MemberPhone> memberPhoneList = memberPhoneRepository.findAll();
        assertThat(memberPhoneList).hasSize(databaseSizeBeforeCreate + 1);
        MemberPhone testMemberPhone = memberPhoneList.get(memberPhoneList.size() - 1);
    }

    @Test
    public void createMemberPhoneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = memberPhoneRepository.findAll().size();

        // Create the MemberPhone with an existing ID
        memberPhone.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restMemberPhoneMockMvc.perform(post("/api/member-phones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(memberPhone)))
            .andExpect(status().isBadRequest());

        // Validate the MemberPhone in the database
        List<MemberPhone> memberPhoneList = memberPhoneRepository.findAll();
        assertThat(memberPhoneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllMemberPhones() throws Exception {
        // Initialize the database
        memberPhoneRepository.save(memberPhone);

        // Get all the memberPhoneList
        restMemberPhoneMockMvc.perform(get("/api/member-phones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(memberPhone.getId())));
    }

    @Test
    public void getMemberPhone() throws Exception {
        // Initialize the database
        memberPhoneRepository.save(memberPhone);

        // Get the memberPhone
        restMemberPhoneMockMvc.perform(get("/api/member-phones/{id}", memberPhone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(memberPhone.getId()));
    }
    @Test
    public void getNonExistingMemberPhone() throws Exception {
        // Get the memberPhone
        restMemberPhoneMockMvc.perform(get("/api/member-phones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateMemberPhone() throws Exception {
        // Initialize the database
        memberPhoneRepository.save(memberPhone);

        int databaseSizeBeforeUpdate = memberPhoneRepository.findAll().size();

        // Update the memberPhone
        MemberPhone updatedMemberPhone = memberPhoneRepository.findById(memberPhone.getId()).get();

        restMemberPhoneMockMvc.perform(put("/api/member-phones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedMemberPhone)))
            .andExpect(status().isOk());

        // Validate the MemberPhone in the database
        List<MemberPhone> memberPhoneList = memberPhoneRepository.findAll();
        assertThat(memberPhoneList).hasSize(databaseSizeBeforeUpdate);
        MemberPhone testMemberPhone = memberPhoneList.get(memberPhoneList.size() - 1);
    }

    @Test
    public void updateNonExistingMemberPhone() throws Exception {
        int databaseSizeBeforeUpdate = memberPhoneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMemberPhoneMockMvc.perform(put("/api/member-phones")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(memberPhone)))
            .andExpect(status().isBadRequest());

        // Validate the MemberPhone in the database
        List<MemberPhone> memberPhoneList = memberPhoneRepository.findAll();
        assertThat(memberPhoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteMemberPhone() throws Exception {
        // Initialize the database
        memberPhoneRepository.save(memberPhone);

        int databaseSizeBeforeDelete = memberPhoneRepository.findAll().size();

        // Delete the memberPhone
        restMemberPhoneMockMvc.perform(delete("/api/member-phones/{id}", memberPhone.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MemberPhone> memberPhoneList = memberPhoneRepository.findAll();
        assertThat(memberPhoneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
