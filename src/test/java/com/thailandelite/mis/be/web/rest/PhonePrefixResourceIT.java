package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.model.domain.master.PhonePrefix;
import com.thailandelite.mis.be.repository.PhonePrefixRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhonePrefixResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PhonePrefixResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DIAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DIAL_CODE = "BBBBBBBBBB";

    @Autowired
    private PhonePrefixRepository phonePrefixRepository;

    @Autowired
    private MockMvc restPhonePrefixMockMvc;

    private PhonePrefix phonePrefix;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhonePrefix createEntity() {
        PhonePrefix phonePrefix = new PhonePrefix()
            .name(DEFAULT_NAME)
            .dialCode(DEFAULT_DIAL_CODE);
        return phonePrefix;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PhonePrefix createUpdatedEntity() {
        PhonePrefix phonePrefix = new PhonePrefix()
            .name(UPDATED_NAME)
            .dialCode(UPDATED_DIAL_CODE);
        return phonePrefix;
    }

    @BeforeEach
    public void initTest() {
        phonePrefixRepository.deleteAll();
        phonePrefix = createEntity();
    }

    @Test
    public void createPhonePrefix() throws Exception {
        int databaseSizeBeforeCreate = phonePrefixRepository.findAll().size();
        // Create the PhonePrefix
        restPhonePrefixMockMvc.perform(post("/api/phone-prefixes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phonePrefix)))
            .andExpect(status().isCreated());

        // Validate the PhonePrefix in the database
        List<PhonePrefix> phonePrefixList = phonePrefixRepository.findAll();
        assertThat(phonePrefixList).hasSize(databaseSizeBeforeCreate + 1);
        PhonePrefix testPhonePrefix = phonePrefixList.get(phonePrefixList.size() - 1);
        assertThat(testPhonePrefix.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPhonePrefix.getDialCode()).isEqualTo(DEFAULT_DIAL_CODE);
    }

    @Test
    public void createPhonePrefixWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phonePrefixRepository.findAll().size();

        // Create the PhonePrefix with an existing ID
        phonePrefix.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhonePrefixMockMvc.perform(post("/api/phone-prefixes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phonePrefix)))
            .andExpect(status().isBadRequest());

        // Validate the PhonePrefix in the database
        List<PhonePrefix> phonePrefixList = phonePrefixRepository.findAll();
        assertThat(phonePrefixList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllPhonePrefixes() throws Exception {
        // Initialize the database
        phonePrefixRepository.save(phonePrefix);

        // Get all the phonePrefixList
        restPhonePrefixMockMvc.perform(get("/api/phone-prefixes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phonePrefix.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].dialCode").value(hasItem(DEFAULT_DIAL_CODE)));
    }

    @Test
    public void getPhonePrefix() throws Exception {
        // Initialize the database
        phonePrefixRepository.save(phonePrefix);

        // Get the phonePrefix
        restPhonePrefixMockMvc.perform(get("/api/phone-prefixes/{id}", phonePrefix.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(phonePrefix.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.dialCode").value(DEFAULT_DIAL_CODE));
    }
    @Test
    public void getNonExistingPhonePrefix() throws Exception {
        // Get the phonePrefix
        restPhonePrefixMockMvc.perform(get("/api/phone-prefixes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePhonePrefix() throws Exception {
        // Initialize the database
        phonePrefixRepository.save(phonePrefix);

        int databaseSizeBeforeUpdate = phonePrefixRepository.findAll().size();

        // Update the phonePrefix
        PhonePrefix updatedPhonePrefix = phonePrefixRepository.findById(phonePrefix.getId()).get();
        updatedPhonePrefix
            .name(UPDATED_NAME)
            .dialCode(UPDATED_DIAL_CODE);

        restPhonePrefixMockMvc.perform(put("/api/phone-prefixes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhonePrefix)))
            .andExpect(status().isOk());

        // Validate the PhonePrefix in the database
        List<PhonePrefix> phonePrefixList = phonePrefixRepository.findAll();
        assertThat(phonePrefixList).hasSize(databaseSizeBeforeUpdate);
        PhonePrefix testPhonePrefix = phonePrefixList.get(phonePrefixList.size() - 1);
        assertThat(testPhonePrefix.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPhonePrefix.getDialCode()).isEqualTo(UPDATED_DIAL_CODE);
    }

    @Test
    public void updateNonExistingPhonePrefix() throws Exception {
        int databaseSizeBeforeUpdate = phonePrefixRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhonePrefixMockMvc.perform(put("/api/phone-prefixes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(phonePrefix)))
            .andExpect(status().isBadRequest());

        // Validate the PhonePrefix in the database
        List<PhonePrefix> phonePrefixList = phonePrefixRepository.findAll();
        assertThat(phonePrefixList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deletePhonePrefix() throws Exception {
        // Initialize the database
        phonePrefixRepository.save(phonePrefix);

        int databaseSizeBeforeDelete = phonePrefixRepository.findAll().size();

        // Delete the phonePrefix
        restPhonePrefixMockMvc.perform(delete("/api/phone-prefixes/{id}", phonePrefix.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PhonePrefix> phonePrefixList = phonePrefixRepository.findAll();
        assertThat(phonePrefixList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
