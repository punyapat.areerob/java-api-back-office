package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.EmailTransaction;
import com.thailandelite.mis.be.repository.EmailTransactionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.thailandelite.mis.be.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.thailandelite.mis.be.domain.enumeration.EmailTransactionStatus;
/**
 * Integration tests for the {@link EmailTransactionResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class EmailTransactionResourceIT {

    private static final String DEFAULT_TEMPLATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TEMPLATE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TO = "AAAAAAAAAA";
    private static final String UPDATED_TO = "BBBBBBBBBB";

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_BODY = "AAAAAAAAAA";
    private static final String UPDATED_BODY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_SEND_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SEND_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final EmailTransactionStatus DEFAULT_STATUS = EmailTransactionStatus.SUCCESS;
    private static final EmailTransactionStatus UPDATED_STATUS = EmailTransactionStatus.FAIL;

    private static final String DEFAULT_ERROR = "AAAAAAAAAA";
    private static final String UPDATED_ERROR = "BBBBBBBBBB";

    @Autowired
    private EmailTransactionRepository emailTransactionRepository;

    @Autowired
    private MockMvc restEmailTransactionMockMvc;

    private EmailTransaction emailTransaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmailTransaction createEntity() {
        EmailTransaction emailTransaction = new EmailTransaction()
            .templateName(DEFAULT_TEMPLATE_NAME)
            .to(DEFAULT_TO)
            .subject(DEFAULT_SUBJECT)
            .body(DEFAULT_BODY)
            .sendDate(DEFAULT_SEND_DATE)
            .status(DEFAULT_STATUS)
            .error(DEFAULT_ERROR);
        return emailTransaction;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmailTransaction createUpdatedEntity() {
        EmailTransaction emailTransaction = new EmailTransaction()
            .templateName(UPDATED_TEMPLATE_NAME)
            .to(UPDATED_TO)
            .subject(UPDATED_SUBJECT)
            .body(UPDATED_BODY)
            .sendDate(UPDATED_SEND_DATE)
            .status(UPDATED_STATUS)
            .error(UPDATED_ERROR);
        return emailTransaction;
    }

    @BeforeEach
    public void initTest() {
        emailTransactionRepository.deleteAll();
        emailTransaction = createEntity();
    }

    @Test
    public void createEmailTransaction() throws Exception {
        int databaseSizeBeforeCreate = emailTransactionRepository.findAll().size();
        // Create the EmailTransaction
        restEmailTransactionMockMvc.perform(post("/api/email-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(emailTransaction)))
            .andExpect(status().isCreated());

        // Validate the EmailTransaction in the database
        List<EmailTransaction> emailTransactionList = emailTransactionRepository.findAll();
        assertThat(emailTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        EmailTransaction testEmailTransaction = emailTransactionList.get(emailTransactionList.size() - 1);
        assertThat(testEmailTransaction.getTemplateName()).isEqualTo(DEFAULT_TEMPLATE_NAME);
        assertThat(testEmailTransaction.getTo()).isEqualTo(DEFAULT_TO);
        assertThat(testEmailTransaction.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testEmailTransaction.getBody()).isEqualTo(DEFAULT_BODY);
        assertThat(testEmailTransaction.getSendDate()).isEqualTo(DEFAULT_SEND_DATE);
        assertThat(testEmailTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testEmailTransaction.getError()).isEqualTo(DEFAULT_ERROR);
    }

    @Test
    public void createEmailTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = emailTransactionRepository.findAll().size();

        // Create the EmailTransaction with an existing ID
        emailTransaction.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmailTransactionMockMvc.perform(post("/api/email-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(emailTransaction)))
            .andExpect(status().isBadRequest());

        // Validate the EmailTransaction in the database
        List<EmailTransaction> emailTransactionList = emailTransactionRepository.findAll();
        assertThat(emailTransactionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllEmailTransactions() throws Exception {
        // Initialize the database
        emailTransactionRepository.save(emailTransaction);

        // Get all the emailTransactionList
        restEmailTransactionMockMvc.perform(get("/api/email-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailTransaction.getId())))
            .andExpect(jsonPath("$.[*].templateName").value(hasItem(DEFAULT_TEMPLATE_NAME)))
            .andExpect(jsonPath("$.[*].to").value(hasItem(DEFAULT_TO)))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].body").value(hasItem(DEFAULT_BODY)))
            .andExpect(jsonPath("$.[*].sendDate").value(hasItem(sameInstant(DEFAULT_SEND_DATE))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].error").value(hasItem(DEFAULT_ERROR)));
    }

    @Test
    public void getEmailTransaction() throws Exception {
        // Initialize the database
        emailTransactionRepository.save(emailTransaction);

        // Get the emailTransaction
        restEmailTransactionMockMvc.perform(get("/api/email-transactions/{id}", emailTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(emailTransaction.getId()))
            .andExpect(jsonPath("$.templateName").value(DEFAULT_TEMPLATE_NAME))
            .andExpect(jsonPath("$.to").value(DEFAULT_TO))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT))
            .andExpect(jsonPath("$.body").value(DEFAULT_BODY))
            .andExpect(jsonPath("$.sendDate").value(sameInstant(DEFAULT_SEND_DATE)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.error").value(DEFAULT_ERROR));
    }
    @Test
    public void getNonExistingEmailTransaction() throws Exception {
        // Get the emailTransaction
        restEmailTransactionMockMvc.perform(get("/api/email-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateEmailTransaction() throws Exception {
        // Initialize the database
        emailTransactionRepository.save(emailTransaction);

        int databaseSizeBeforeUpdate = emailTransactionRepository.findAll().size();

        // Update the emailTransaction
        EmailTransaction updatedEmailTransaction = emailTransactionRepository.findById(emailTransaction.getId()).get();
        updatedEmailTransaction
            .templateName(UPDATED_TEMPLATE_NAME)
            .to(UPDATED_TO)
            .subject(UPDATED_SUBJECT)
            .body(UPDATED_BODY)
            .sendDate(UPDATED_SEND_DATE)
            .status(UPDATED_STATUS)
            .error(UPDATED_ERROR);

        restEmailTransactionMockMvc.perform(put("/api/email-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEmailTransaction)))
            .andExpect(status().isOk());

        // Validate the EmailTransaction in the database
        List<EmailTransaction> emailTransactionList = emailTransactionRepository.findAll();
        assertThat(emailTransactionList).hasSize(databaseSizeBeforeUpdate);
        EmailTransaction testEmailTransaction = emailTransactionList.get(emailTransactionList.size() - 1);
        assertThat(testEmailTransaction.getTemplateName()).isEqualTo(UPDATED_TEMPLATE_NAME);
        assertThat(testEmailTransaction.getTo()).isEqualTo(UPDATED_TO);
        assertThat(testEmailTransaction.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testEmailTransaction.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testEmailTransaction.getSendDate()).isEqualTo(UPDATED_SEND_DATE);
        assertThat(testEmailTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testEmailTransaction.getError()).isEqualTo(UPDATED_ERROR);
    }

    @Test
    public void updateNonExistingEmailTransaction() throws Exception {
        int databaseSizeBeforeUpdate = emailTransactionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmailTransactionMockMvc.perform(put("/api/email-transactions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(emailTransaction)))
            .andExpect(status().isBadRequest());

        // Validate the EmailTransaction in the database
        List<EmailTransaction> emailTransactionList = emailTransactionRepository.findAll();
        assertThat(emailTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteEmailTransaction() throws Exception {
        // Initialize the database
        emailTransactionRepository.save(emailTransaction);

        int databaseSizeBeforeDelete = emailTransactionRepository.findAll().size();

        // Delete the emailTransaction
        restEmailTransactionMockMvc.perform(delete("/api/email-transactions/{id}", emailTransaction.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EmailTransaction> emailTransactionList = emailTransactionRepository.findAll();
        assertThat(emailTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchEmailTransaction() throws Exception {
        // Initialize the database
        emailTransactionRepository.save(emailTransaction);

        String path = String.format("/api/email-transactions?sort=id,desc&to=%s&sendDate=%s&status=%s",
            DEFAULT_TO,
            DEFAULT_SEND_DATE.toLocalDate(),
            DEFAULT_STATUS.toString());

        // Get all the emailTransactionList
        restEmailTransactionMockMvc.perform(get(path))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emailTransaction.getId())))
            .andExpect(jsonPath("$.[*].templateName").value(hasItem(DEFAULT_TEMPLATE_NAME)))
            .andExpect(jsonPath("$.[*].to").value(hasItem(DEFAULT_TO)))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].body").value(hasItem(DEFAULT_BODY)))
            .andExpect(jsonPath("$.[*].sendDate").value(hasItem(sameInstant(DEFAULT_SEND_DATE))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].error").value(hasItem(DEFAULT_ERROR)));
    }
}
