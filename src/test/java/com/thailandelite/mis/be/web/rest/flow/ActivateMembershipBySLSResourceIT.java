package com.thailandelite.mis.be.web.rest.flow;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.config.Constants;
import com.thailandelite.mis.be.domain.User;
import com.thailandelite.mis.be.repository.AuthorityRepository;
import com.thailandelite.mis.be.repository.UserRepository;
import com.thailandelite.mis.be.security.AuthoritiesConstants;
import com.thailandelite.mis.be.service.UserService;
import com.thailandelite.mis.be.service.dto.PasswordChangeDTO;
import com.thailandelite.mis.be.service.dto.UserDTO;
import com.thailandelite.mis.be.web.rest.AccountResource;
import com.thailandelite.mis.be.web.rest.TestUtil;
import com.thailandelite.mis.be.web.rest.WithUnauthenticatedMockUser;
import com.thailandelite.mis.be.web.rest.vm.KeyAndPasswordVM;
import com.thailandelite.mis.be.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.thailandelite.mis.be.web.rest.flow.ActivateMembershipBySLSResourceIT.TEST_USER_LOGIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AccountResource} REST controller.
 */
@AutoConfigureMockMvc
@WithMockUser(value = TEST_USER_LOGIN)
@SpringBootTest(classes = MisbeApp.class)
public class ActivateMembershipBySLSResourceIT {
    static final String TEST_USER_LOGIN = "test";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MockMvc restAccountMockMvc;

    @BeforeEach
    public void setup() {
        userRepository.deleteAll();
    }

    @Test
    @WithUnauthenticatedMockUser
    public void testNonAuthenticatedUser() throws Exception {
        restAccountMockMvc.perform(get("/api/authenticate")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string(""));
    }

    @Test
    public void testAuthenticatedUser() throws Exception {
        restAccountMockMvc.perform(get("/api/authenticate")
            .with(request -> {
                request.setRemoteUser(TEST_USER_LOGIN);
                return request;
            })
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string(TEST_USER_LOGIN));
    }


}
