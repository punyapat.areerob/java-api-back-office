package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CaseInstance;
import com.thailandelite.mis.be.repository.CaseInstanceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.thailandelite.mis.be.domain.enumeration.StepState;
/**
 * Integration tests for the {@link CaseInstanceResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseInstanceResourceIT {

    private static final String DEFAULT_CASE_NO = "AAAAAAAAAA";
    private static final String UPDATED_CASE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_CASE_ID = "AAAAAAAAAA";
    private static final String UPDATED_CASE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_STEP_ID = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_STEP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    private static final String DEFAULT_MEMBER_ID = "AAAAAAAAAA";
    private static final String UPDATED_MEMBER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CASE_DATA_ID = "AAAAAAAAAA";
    private static final String UPDATED_CASE_DATA_ID = "BBBBBBBBBB";

    private static final StepState DEFAULT_STEP_STATUS = StepState.NEW;
    private static final StepState UPDATED_STEP_STATUS = StepState.IN_PROGRESS;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_UPDATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private CaseInstanceRepository caseInstanceRepository;

    @Autowired
    private MockMvc restCaseInstanceMockMvc;

    private CaseInstance caseInstance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseInstance createEntity() {
        CaseInstance caseInstance = new CaseInstance()
            .caseNo(DEFAULT_CASE_NO)
            .caseId(DEFAULT_CASE_ID)
            .currentStepId(DEFAULT_CURRENT_STEP_ID)
            .remark(DEFAULT_REMARK)
            .memberId(DEFAULT_MEMBER_ID)
            .caseDataId(DEFAULT_CASE_DATA_ID)
            .stepStatus(DEFAULT_STEP_STATUS)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedBy(DEFAULT_UPDATED_BY)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return caseInstance;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseInstance createUpdatedEntity() {
        CaseInstance caseInstance = new CaseInstance()
            .caseNo(UPDATED_CASE_NO)
            .caseId(UPDATED_CASE_ID)
            .currentStepId(UPDATED_CURRENT_STEP_ID)
            .remark(UPDATED_REMARK)
            .memberId(UPDATED_MEMBER_ID)
            .caseDataId(UPDATED_CASE_DATA_ID)
            .stepStatus(UPDATED_STEP_STATUS)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return caseInstance;
    }

    @BeforeEach
    public void initTest() {
        caseInstanceRepository.deleteAll();
        caseInstance = createEntity();
    }

    @Test
    public void createCaseInstance() throws Exception {
        int databaseSizeBeforeCreate = caseInstanceRepository.findAll().size();
        // Create the CaseInstance
        restCaseInstanceMockMvc.perform(post("/api/case-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseInstance)))
            .andExpect(status().isCreated());

        // Validate the CaseInstance in the database
        List<CaseInstance> caseInstanceList = caseInstanceRepository.findAll();
        assertThat(caseInstanceList).hasSize(databaseSizeBeforeCreate + 1);
        CaseInstance testCaseInstance = caseInstanceList.get(caseInstanceList.size() - 1);
        assertThat(testCaseInstance.getCaseNo()).isEqualTo(DEFAULT_CASE_NO);
        assertThat(testCaseInstance.getCaseId()).isEqualTo(DEFAULT_CASE_ID);
        assertThat(testCaseInstance.getCurrentStepId()).isEqualTo(DEFAULT_CURRENT_STEP_ID);
        assertThat(testCaseInstance.getRemark()).isEqualTo(DEFAULT_REMARK);
        assertThat(testCaseInstance.getMemberId()).isEqualTo(DEFAULT_MEMBER_ID);
        assertThat(testCaseInstance.getCaseDataId()).isEqualTo(DEFAULT_CASE_DATA_ID);
        assertThat(testCaseInstance.getStepStatus()).isEqualTo(DEFAULT_STEP_STATUS);
        assertThat(testCaseInstance.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCaseInstance.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testCaseInstance.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCaseInstance.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createCaseInstanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseInstanceRepository.findAll().size();

        // Create the CaseInstance with an existing ID
        caseInstance.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseInstanceMockMvc.perform(post("/api/case-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseInstance)))
            .andExpect(status().isBadRequest());

        // Validate the CaseInstance in the database
        List<CaseInstance> caseInstanceList = caseInstanceRepository.findAll();
        assertThat(caseInstanceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseInstances() throws Exception {
        // Initialize the database
        caseInstanceRepository.save(caseInstance);

        // Get all the caseInstanceList
        restCaseInstanceMockMvc.perform(get("/api/case-instances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseInstance.getId())))
            .andExpect(jsonPath("$.[*].caseNo").value(hasItem(DEFAULT_CASE_NO)))
            .andExpect(jsonPath("$.[*].caseId").value(hasItem(DEFAULT_CASE_ID)))
            .andExpect(jsonPath("$.[*].currentStepId").value(hasItem(DEFAULT_CURRENT_STEP_ID)))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK)))
            .andExpect(jsonPath("$.[*].memberId").value(hasItem(DEFAULT_MEMBER_ID)))
            .andExpect(jsonPath("$.[*].caseDataId").value(hasItem(DEFAULT_CASE_DATA_ID)))
            .andExpect(jsonPath("$.[*].stepStatus").value(hasItem(DEFAULT_STEP_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }
    
    @Test
    public void getCaseInstance() throws Exception {
        // Initialize the database
        caseInstanceRepository.save(caseInstance);

        // Get the caseInstance
        restCaseInstanceMockMvc.perform(get("/api/case-instances/{id}", caseInstance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseInstance.getId()))
            .andExpect(jsonPath("$.caseNo").value(DEFAULT_CASE_NO))
            .andExpect(jsonPath("$.caseId").value(DEFAULT_CASE_ID))
            .andExpect(jsonPath("$.currentStepId").value(DEFAULT_CURRENT_STEP_ID))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK))
            .andExpect(jsonPath("$.memberId").value(DEFAULT_MEMBER_ID))
            .andExpect(jsonPath("$.caseDataId").value(DEFAULT_CASE_DATA_ID))
            .andExpect(jsonPath("$.stepStatus").value(DEFAULT_STEP_STATUS.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }
    @Test
    public void getNonExistingCaseInstance() throws Exception {
        // Get the caseInstance
        restCaseInstanceMockMvc.perform(get("/api/case-instances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseInstance() throws Exception {
        // Initialize the database
        caseInstanceRepository.save(caseInstance);

        int databaseSizeBeforeUpdate = caseInstanceRepository.findAll().size();

        // Update the caseInstance
        CaseInstance updatedCaseInstance = caseInstanceRepository.findById(caseInstance.getId()).get();
        updatedCaseInstance
            .caseNo(UPDATED_CASE_NO)
            .caseId(UPDATED_CASE_ID)
            .currentStepId(UPDATED_CURRENT_STEP_ID)
            .remark(UPDATED_REMARK)
            .memberId(UPDATED_MEMBER_ID)
            .caseDataId(UPDATED_CASE_DATA_ID)
            .stepStatus(UPDATED_STEP_STATUS)
            .createdBy(UPDATED_CREATED_BY)
            .updatedBy(UPDATED_UPDATED_BY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restCaseInstanceMockMvc.perform(put("/api/case-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseInstance)))
            .andExpect(status().isOk());

        // Validate the CaseInstance in the database
        List<CaseInstance> caseInstanceList = caseInstanceRepository.findAll();
        assertThat(caseInstanceList).hasSize(databaseSizeBeforeUpdate);
        CaseInstance testCaseInstance = caseInstanceList.get(caseInstanceList.size() - 1);
        assertThat(testCaseInstance.getCaseNo()).isEqualTo(UPDATED_CASE_NO);
        assertThat(testCaseInstance.getCaseId()).isEqualTo(UPDATED_CASE_ID);
        assertThat(testCaseInstance.getCurrentStepId()).isEqualTo(UPDATED_CURRENT_STEP_ID);
        assertThat(testCaseInstance.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testCaseInstance.getMemberId()).isEqualTo(UPDATED_MEMBER_ID);
        assertThat(testCaseInstance.getCaseDataId()).isEqualTo(UPDATED_CASE_DATA_ID);
        assertThat(testCaseInstance.getStepStatus()).isEqualTo(UPDATED_STEP_STATUS);
        assertThat(testCaseInstance.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCaseInstance.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testCaseInstance.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCaseInstance.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingCaseInstance() throws Exception {
        int databaseSizeBeforeUpdate = caseInstanceRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseInstanceMockMvc.perform(put("/api/case-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseInstance)))
            .andExpect(status().isBadRequest());

        // Validate the CaseInstance in the database
        List<CaseInstance> caseInstanceList = caseInstanceRepository.findAll();
        assertThat(caseInstanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseInstance() throws Exception {
        // Initialize the database
        caseInstanceRepository.save(caseInstance);

        int databaseSizeBeforeDelete = caseInstanceRepository.findAll().size();

        // Delete the caseInstance
        restCaseInstanceMockMvc.perform(delete("/api/case-instances/{id}", caseInstance.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseInstance> caseInstanceList = caseInstanceRepository.findAll();
        assertThat(caseInstanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
