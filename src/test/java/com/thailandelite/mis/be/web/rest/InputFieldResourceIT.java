package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.InputField;
import com.thailandelite.mis.be.repository.InputFieldRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link InputFieldResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class InputFieldResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    @Autowired
    private InputFieldRepository inputFieldRepository;

    @Autowired
    private MockMvc restInputFieldMockMvc;

    private InputField inputField;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InputField createEntity() {
        InputField inputField = new InputField()
            .name(DEFAULT_NAME)
            .label(DEFAULT_LABEL)
            .link(DEFAULT_LINK);
        return inputField;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InputField createUpdatedEntity() {
        InputField inputField = new InputField()
            .name(UPDATED_NAME)
            .label(UPDATED_LABEL)
            .link(UPDATED_LINK);
        return inputField;
    }

    @BeforeEach
    public void initTest() {
        inputFieldRepository.deleteAll();
        inputField = createEntity();
    }

    @Test
    public void createInputField() throws Exception {
        int databaseSizeBeforeCreate = inputFieldRepository.findAll().size();
        // Create the InputField
        restInputFieldMockMvc.perform(post("/api/input-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inputField)))
            .andExpect(status().isCreated());

        // Validate the InputField in the database
        List<InputField> inputFieldList = inputFieldRepository.findAll();
        assertThat(inputFieldList).hasSize(databaseSizeBeforeCreate + 1);
        InputField testInputField = inputFieldList.get(inputFieldList.size() - 1);
        assertThat(testInputField.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInputField.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testInputField.getLink()).isEqualTo(DEFAULT_LINK);
    }

    @Test
    public void createInputFieldWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inputFieldRepository.findAll().size();

        // Create the InputField with an existing ID
        inputField.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restInputFieldMockMvc.perform(post("/api/input-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inputField)))
            .andExpect(status().isBadRequest());

        // Validate the InputField in the database
        List<InputField> inputFieldList = inputFieldRepository.findAll();
        assertThat(inputFieldList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllInputFields() throws Exception {
        // Initialize the database
        inputFieldRepository.save(inputField);

        // Get all the inputFieldList
        restInputFieldMockMvc.perform(get("/api/input-fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inputField.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK)));
    }
    
    @Test
    public void getInputField() throws Exception {
        // Initialize the database
        inputFieldRepository.save(inputField);

        // Get the inputField
        restInputFieldMockMvc.perform(get("/api/input-fields/{id}", inputField.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(inputField.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK));
    }
    @Test
    public void getNonExistingInputField() throws Exception {
        // Get the inputField
        restInputFieldMockMvc.perform(get("/api/input-fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateInputField() throws Exception {
        // Initialize the database
        inputFieldRepository.save(inputField);

        int databaseSizeBeforeUpdate = inputFieldRepository.findAll().size();

        // Update the inputField
        InputField updatedInputField = inputFieldRepository.findById(inputField.getId()).get();
        updatedInputField
            .name(UPDATED_NAME)
            .label(UPDATED_LABEL)
            .link(UPDATED_LINK);

        restInputFieldMockMvc.perform(put("/api/input-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedInputField)))
            .andExpect(status().isOk());

        // Validate the InputField in the database
        List<InputField> inputFieldList = inputFieldRepository.findAll();
        assertThat(inputFieldList).hasSize(databaseSizeBeforeUpdate);
        InputField testInputField = inputFieldList.get(inputFieldList.size() - 1);
        assertThat(testInputField.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInputField.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testInputField.getLink()).isEqualTo(UPDATED_LINK);
    }

    @Test
    public void updateNonExistingInputField() throws Exception {
        int databaseSizeBeforeUpdate = inputFieldRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInputFieldMockMvc.perform(put("/api/input-fields")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inputField)))
            .andExpect(status().isBadRequest());

        // Validate the InputField in the database
        List<InputField> inputFieldList = inputFieldRepository.findAll();
        assertThat(inputFieldList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteInputField() throws Exception {
        // Initialize the database
        inputFieldRepository.save(inputField);

        int databaseSizeBeforeDelete = inputFieldRepository.findAll().size();

        // Delete the inputField
        restInputFieldMockMvc.perform(delete("/api/input-fields/{id}", inputField.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<InputField> inputFieldList = inputFieldRepository.findAll();
        assertThat(inputFieldList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
