package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.CaseStepState;
import com.thailandelite.mis.be.repository.CaseStepStateRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.thailandelite.mis.be.domain.enumeration.StepState;
/**
 * Integration tests for the {@link CaseStepStateResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaseStepStateResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final StepState DEFAULT_STATE = StepState.NEW;
    private static final StepState UPDATED_STATE = StepState.IN_PROGRESS;

    @Autowired
    private CaseStepStateRepository caseStepStateRepository;

    @Autowired
    private MockMvc restCaseStepStateMockMvc;

    private CaseStepState caseStepState;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStepState createEntity() {
        CaseStepState caseStepState = new CaseStepState()
            .name(DEFAULT_NAME)
            .state(DEFAULT_STATE);
        return caseStepState;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaseStepState createUpdatedEntity() {
        CaseStepState caseStepState = new CaseStepState()
            .name(UPDATED_NAME)
            .state(UPDATED_STATE);
        return caseStepState;
    }

    @BeforeEach
    public void initTest() {
        caseStepStateRepository.deleteAll();
        caseStepState = createEntity();
    }

    @Test
    public void createCaseStepState() throws Exception {
        int databaseSizeBeforeCreate = caseStepStateRepository.findAll().size();
        // Create the CaseStepState
        restCaseStepStateMockMvc.perform(post("/api/case-step-states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStepState)))
            .andExpect(status().isCreated());

        // Validate the CaseStepState in the database
        List<CaseStepState> caseStepStateList = caseStepStateRepository.findAll();
        assertThat(caseStepStateList).hasSize(databaseSizeBeforeCreate + 1);
        CaseStepState testCaseStepState = caseStepStateList.get(caseStepStateList.size() - 1);
        assertThat(testCaseStepState.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCaseStepState.getState()).isEqualTo(DEFAULT_STATE);
    }

    @Test
    public void createCaseStepStateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caseStepStateRepository.findAll().size();

        // Create the CaseStepState with an existing ID
        caseStepState.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaseStepStateMockMvc.perform(post("/api/case-step-states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStepState)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStepState in the database
        List<CaseStepState> caseStepStateList = caseStepStateRepository.findAll();
        assertThat(caseStepStateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllCaseStepStates() throws Exception {
        // Initialize the database
        caseStepStateRepository.save(caseStepState);

        // Get all the caseStepStateList
        restCaseStepStateMockMvc.perform(get("/api/case-step-states?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caseStepState.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())));
    }
    
    @Test
    public void getCaseStepState() throws Exception {
        // Initialize the database
        caseStepStateRepository.save(caseStepState);

        // Get the caseStepState
        restCaseStepStateMockMvc.perform(get("/api/case-step-states/{id}", caseStepState.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caseStepState.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()));
    }
    @Test
    public void getNonExistingCaseStepState() throws Exception {
        // Get the caseStepState
        restCaseStepStateMockMvc.perform(get("/api/case-step-states/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCaseStepState() throws Exception {
        // Initialize the database
        caseStepStateRepository.save(caseStepState);

        int databaseSizeBeforeUpdate = caseStepStateRepository.findAll().size();

        // Update the caseStepState
        CaseStepState updatedCaseStepState = caseStepStateRepository.findById(caseStepState.getId()).get();
        updatedCaseStepState
            .name(UPDATED_NAME)
            .state(UPDATED_STATE);

        restCaseStepStateMockMvc.perform(put("/api/case-step-states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaseStepState)))
            .andExpect(status().isOk());

        // Validate the CaseStepState in the database
        List<CaseStepState> caseStepStateList = caseStepStateRepository.findAll();
        assertThat(caseStepStateList).hasSize(databaseSizeBeforeUpdate);
        CaseStepState testCaseStepState = caseStepStateList.get(caseStepStateList.size() - 1);
        assertThat(testCaseStepState.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCaseStepState.getState()).isEqualTo(UPDATED_STATE);
    }

    @Test
    public void updateNonExistingCaseStepState() throws Exception {
        int databaseSizeBeforeUpdate = caseStepStateRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaseStepStateMockMvc.perform(put("/api/case-step-states")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caseStepState)))
            .andExpect(status().isBadRequest());

        // Validate the CaseStepState in the database
        List<CaseStepState> caseStepStateList = caseStepStateRepository.findAll();
        assertThat(caseStepStateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteCaseStepState() throws Exception {
        // Initialize the database
        caseStepStateRepository.save(caseStepState);

        int databaseSizeBeforeDelete = caseStepStateRepository.findAll().size();

        // Delete the caseStepState
        restCaseStepStateMockMvc.perform(delete("/api/case-step-states/{id}", caseStepState.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaseStepState> caseStepStateList = caseStepStateRepository.findAll();
        assertThat(caseStepStateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
