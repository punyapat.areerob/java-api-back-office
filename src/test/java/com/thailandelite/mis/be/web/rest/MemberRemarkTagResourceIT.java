package com.thailandelite.mis.be.web.rest;

import com.thailandelite.mis.be.MisbeApp;
import com.thailandelite.mis.be.domain.MemberRemarkTag;
import com.thailandelite.mis.be.repository.MemberRemarkTagRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MemberRemarkTagResource} REST controller.
 */
@SpringBootTest(classes = MisbeApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MemberRemarkTagResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Autowired
    private MemberRemarkTagRepository memberRemarkTagRepository;

    @Autowired
    private MockMvc restMemberRemarkTagMockMvc;

    private MemberRemarkTag memberRemarkTag;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MemberRemarkTag createEntity() {
        MemberRemarkTag memberRemarkTag = new MemberRemarkTag()
            .name(DEFAULT_NAME)
            .active(DEFAULT_ACTIVE);
        return memberRemarkTag;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MemberRemarkTag createUpdatedEntity() {
        MemberRemarkTag memberRemarkTag = new MemberRemarkTag()
            .name(UPDATED_NAME)
            .active(UPDATED_ACTIVE);
        return memberRemarkTag;
    }

    @BeforeEach
    public void initTest() {
        memberRemarkTagRepository.deleteAll();
        memberRemarkTag = createEntity();
    }

    @Test
    public void createMemberRemarkTag() throws Exception {
        int databaseSizeBeforeCreate = memberRemarkTagRepository.findAll().size();
        // Create the MemberRemarkTag
        restMemberRemarkTagMockMvc.perform(post("/api/member-remark-tags")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(memberRemarkTag)))
            .andExpect(status().isCreated());

        // Validate the MemberRemarkTag in the database
        List<MemberRemarkTag> memberRemarkTagList = memberRemarkTagRepository.findAll();
        assertThat(memberRemarkTagList).hasSize(databaseSizeBeforeCreate + 1);
        MemberRemarkTag testMemberRemarkTag = memberRemarkTagList.get(memberRemarkTagList.size() - 1);
        assertThat(testMemberRemarkTag.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMemberRemarkTag.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    public void createMemberRemarkTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = memberRemarkTagRepository.findAll().size();

        // Create the MemberRemarkTag with an existing ID
        memberRemarkTag.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restMemberRemarkTagMockMvc.perform(post("/api/member-remark-tags")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(memberRemarkTag)))
            .andExpect(status().isBadRequest());

        // Validate the MemberRemarkTag in the database
        List<MemberRemarkTag> memberRemarkTagList = memberRemarkTagRepository.findAll();
        assertThat(memberRemarkTagList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllMemberRemarkTags() throws Exception {
        // Initialize the database
        memberRemarkTagRepository.save(memberRemarkTag);

        // Get all the memberRemarkTagList
        restMemberRemarkTagMockMvc.perform(get("/api/member-remark-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(memberRemarkTag.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }
    
    @Test
    public void getMemberRemarkTag() throws Exception {
        // Initialize the database
        memberRemarkTagRepository.save(memberRemarkTag);

        // Get the memberRemarkTag
        restMemberRemarkTagMockMvc.perform(get("/api/member-remark-tags/{id}", memberRemarkTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(memberRemarkTag.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }
    @Test
    public void getNonExistingMemberRemarkTag() throws Exception {
        // Get the memberRemarkTag
        restMemberRemarkTagMockMvc.perform(get("/api/member-remark-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateMemberRemarkTag() throws Exception {
        // Initialize the database
        memberRemarkTagRepository.save(memberRemarkTag);

        int databaseSizeBeforeUpdate = memberRemarkTagRepository.findAll().size();

        // Update the memberRemarkTag
        MemberRemarkTag updatedMemberRemarkTag = memberRemarkTagRepository.findById(memberRemarkTag.getId()).get();
        updatedMemberRemarkTag
            .name(UPDATED_NAME)
            .active(UPDATED_ACTIVE);

        restMemberRemarkTagMockMvc.perform(put("/api/member-remark-tags")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedMemberRemarkTag)))
            .andExpect(status().isOk());

        // Validate the MemberRemarkTag in the database
        List<MemberRemarkTag> memberRemarkTagList = memberRemarkTagRepository.findAll();
        assertThat(memberRemarkTagList).hasSize(databaseSizeBeforeUpdate);
        MemberRemarkTag testMemberRemarkTag = memberRemarkTagList.get(memberRemarkTagList.size() - 1);
        assertThat(testMemberRemarkTag.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMemberRemarkTag.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    public void updateNonExistingMemberRemarkTag() throws Exception {
        int databaseSizeBeforeUpdate = memberRemarkTagRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMemberRemarkTagMockMvc.perform(put("/api/member-remark-tags")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(memberRemarkTag)))
            .andExpect(status().isBadRequest());

        // Validate the MemberRemarkTag in the database
        List<MemberRemarkTag> memberRemarkTagList = memberRemarkTagRepository.findAll();
        assertThat(memberRemarkTagList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteMemberRemarkTag() throws Exception {
        // Initialize the database
        memberRemarkTagRepository.save(memberRemarkTag);

        int databaseSizeBeforeDelete = memberRemarkTagRepository.findAll().size();

        // Delete the memberRemarkTag
        restMemberRemarkTagMockMvc.perform(delete("/api/member-remark-tags/{id}", memberRemarkTag.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MemberRemarkTag> memberRemarkTagList = memberRemarkTagRepository.findAll();
        assertThat(memberRemarkTagList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
